Открытая версия ЯОС
===================

Примечание про открытую версию ЯОС. Перевод разработки в закрытый режим
находится в процессе. Очевидно, что большая часть модулей будет оставаться
открытой. На данный момент, в открытой версии есть следующие отличия:

* не все новые изменения сразу попадают в открытую версию
* отсутствует ветка с англоязычными ключевыми словами и англоязычными именами сущностей в коде 

Дату версии можно определить по истории коммитов. 

[Общий файл README.normal.md](README.normal.md)


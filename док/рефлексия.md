# Рефлексия

Речь идёт о новом формате объектных файлов (сейчас смотрим яос0 от 2019-12-28)

Соответствующий код находится в модуле Reflection, в файле Generic.Reflection.Mod

Reflection.ReportModule и пр. нужны для вывода структуры модуля.

Reflection.WriteVariables и пр. - для вывода состояния модуля.

```
MODULE Proba ; IMPORT Commands , Modules , Reflection2 , Streams ;  
  
TYPE myRecordType =RECORD stringField :ARRAY 128 OF CHAR ; integerField :INTEGER END ;  
  
VAR m : Modules .Module ;  
VAR glob :INTEGER ;  
VAR myRecordVar : myRecordType ;  
  
PROCEDURE z *(context : Commands .Context );  
VAR res :LONGINT ; msg :ARRAY 4096 OF CHAR ; o : Streams .Writer ;  
BEGIN  
myRecordVar .integerField :=1 ;  
myRecordVar .stringField := "ABCDEF";  
INC (glob );  
o :=context .out ;  
o .String ("Loading the module"); o .Ln ;  
m :=Modules .ThisModule ("Proba", res , msg );  
IF (m =NIL )OR (res # 0 )THEN  
o .String ("z : Failed to load module : "); o .String (msg ); o .Ln ;  
RETURN END ;  
o .String ("=========Reflection .Report =========== "); o .Ln ;  
Reflection2 .Report (o , m .refs ,0 );  
o .String ("=========Reflection .ModuleState ======= "); o .Ln ;  
Reflection2 .ModuleState (o , m );  
o .String ("=========Done ==================== "); o .Ln ;  
END z ;  
  
END Proba .z ~  
```

Получается вот что:

```
Loading the module  
 ========= Reflection.Report ===========  
MODULE Proba12: Scope  
12:VAR m[@06A2BB70]:POINTER TO RECORD  
25:VAR glob[@06A2BB74]:INTEGER  
41:VAR myRecordVar[@06A2BB78]:RECORD 06A2BCA0  
68:PROCEDURE @Body[@06A2B8F0 - 06A2B906](  
):no type  
93: Scope  
END  
94:PROCEDURE z[@06A2B906 - 06A2BB23](  
113:VAR context[@12]:POINTER TO RECORD  
):no type  
134: Scope  
134:VAR res[@-4]:LONGINT  
149:VAR msg[@-4100]:ARRAY 4096 OF CHAR  
169:VAR o[@-4104]:POINTER TO RECORD  
END  
183:TYPE myRecordType 06A2BCA0  
206: Scope  
206:VAR stringField[@0]:ARRAY 128 OF CHAR  
234:VAR integerField[@128]:INTEGER  
END  
END  
 ========= Reflection.ModuleState =======  
State Proba:  
m=06A2BD48 (Modules.Module)  
glob=2  
myRecordVar=...(Proba.myRecordType)  
```

Формат Refs-ов, как он описан в исходнике:

```
References section format:   
Scope = sfScopeBegin {variable:Variable} {procedure:Procedure} {typeDecl:TypeDeclaration} sfScopeEnd.  
Module = sfModule prevSymbolOffset:SIZE name:String Scope.  
Procedure = sfProcedure prevSymbolOffset:SIZE name:String start:ADR end:ADR flags:SET {parameter:Variable} returnType:Type Scope.  
Variable = sfVariable prevSymbolOffset:SIZE name:String (sfRelative offset: SIZE | sfIndirec offset: SIZE | sfAbsolute address:ADDRESS) type:Type.  
TypeDeclaration = sfTypeDeclaration prevSymbolOffset:SIZE name:String typeInfo:ADR Scope.  
Type =   
sfTypePointerToRecord   
| sfTypePointerToArray Type  
| sfTypeOpenArray Type  
| sfTypeDynamicArray Type  
| sfTypeStaticArray length:SIZE Type  
| sfTypeMathOpenArray Type   
| sfTypeMathStaticArray length:SIZE Type  
| sfTypeMathTensor Type  
| sfTypeRecord tdAdr:ADDRESS   
| sfTypeDelegate {Parameter} return:Type  
| sfTypePort (sfIN | sfOUT)  
| sfTypeBOOLEAN  
| sfTypeCHAR | sfTypeCHAR8 | sfTypeCHAR16 | sfTypeCHAR32  
| sfTypeSHORTINT | sfTypeINTEGER | sfTypeLONGINT | sfTypeHUGEINT  
| sfTypeSIGNED8 | sfTypeSIGNED16 | sfTypeSIGNED32 | sfTypeSIGNED64  
| sfTypeUNSIGNED8 | sfTypeUNSIGNED16 | sfTypeUNSIGNED32 | sfTypeUNSIGNED64  
| sfTypeWORD | sfTypeLONGWORD  
| sfTypeREAL | sfTypeLONGREAL  
| sfTypeCOMPLEX | sfTypeLONGCOMPLEX  
| sfTypeSET | sfTypeANY | sfTypeOBJECT | sfTypeBYTE | sfTypeADDRESS | sfTypeSIZE  
| sfTypeIndirect offset:SIZE.  
```

См. также [яп-активный-оберон/рефлексия-2.md](яп-активный-оберон/рефлексия-2.md)
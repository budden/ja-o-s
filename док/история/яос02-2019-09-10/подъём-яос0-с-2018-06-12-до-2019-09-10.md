Поднимаем яос0 с 2018-06-12 до 2019-09-10
=========================================

Выбираем версию
---------------

Выбираем 2019-09-10 прямо перед цифровой революцией.

Отводим промежуточную ветку и пробно сливаем
--------------------------------------------
Отвели от мастера промежуточная-ветка-2019-09-10
и кое-что починили, а именно, TFParser полностью работает. И ещё 
не помню, что. 

Тогда отводим от яос0 ветку яос01-похожая-на-2019-09-10 и копируем в неё 
для начала все изменённые исходные файлы 
(а потом нужно ещё все бинарные скопировать).

* HotKeys.XML - легко прошло, видимо, уже залил раньше. 
* Configuration.XML - переправил опции по умолчанию на -p=win32
* Heaps.Mod - исчезло поле TypeInfoDesc.sentinel , а также
ArrayDataBlockDesc.current. Вроде всё поправил, но поменялись обе стороны

Мои изменения сводились к открытию следующего:
TypeInfoDesc: descSize-, sentinel-, tag-, flags-, mod-
StaticTypeDesc*: info-, recSize-
SystemBlockDesc*
ArrayBlockDesc*

* Release.Tool - добавил только PodrobnajaPechatq.Mod
* `.gitignore` - сделал старое похожим на новое
* reflection.mod - не трогаем

От промежуточной ветки отводим пробная-ветка-2019-09-10-1 и фиксируем в неё
то, что изменилось. В яос01-похожая-на-2019-09-10 фиксируем прямо так. 

Копируем все бинарные файлы из пробная-ветка... в яос01-похожая-на. 
auto.dsk смотрим, чтобы не пропало (сохраним его в сторонке)

Пробему слить (без фиксации) - получаем конфликты в нескольких файлах, см.
чуть ниже. 

Откатываем: git merge --abort && git clean -x -d -f && get checkout -f .

И обрабатываем руками конфликты в директориях. 
* Tutorial.Text - берём из яос01
* TFStringPool.Mod - хм, выбранные файлы не имеют отличий, уладим любым способом. 
* PETTrees.Mod - берём от промежуточной ветки
* FoxAMDBackend.Mod - копируем BohdanPC и комментируем его вызов (пока что)
* Compiler.Mod - вроде уже всё сделано, копируем файл из пробной ветки в яос01.

Теперь зальём в промежуточную ветку и в яос0.

git.exe merge --no-commit --strategy=resolve яос01-похожая-на-2019-09-10
 
Остался один конфликт в TFStringPool, и отличия есть. Я ничего не понял
(потом понял, что конлфикт реально есть).

В итоге всё залилось и у нас теперь бинарные файлы от master, а исходники
слиты. Делаем рез. копию и пробуем пересобрать полувручную по мотивам wsl-build.

Подробная печать не собирается - отключаем её и пытаемся собрать без неё.

192.168.5.137
den73-u16.echelon.lan

Подробную печать собрали, теперь её чиним. 
ptr0 пытается интерпретировать размер как указатель (massivObmanka) и падает. 
В 2018-06-12 было так:

((Massiv takov: 
0003 0000 0000 0000 CCD8 016F 0003 0000 CD24 016F 0000 0000 CD64 016F 0000 0000 )

А стало так:
((Massiv takov: 
0007 0000 C050 0A64 0007 0000 0000 0000 C0A0 0A64 C0A0 0A64 C0E0 0A64 0000 0000 )

Вот как изменился MarkArray:
Было:
```
PROCEDURE AssignArray*(dest: ADDRESS; tag: StaticTypeBlockU;  numElems: SIZE; src: ADDRESS);
VAR i, j: LONGINT; sval: ADDRESS; 
BEGIN
	FOR j := 0 TO LEN(tag.pointerOffsets)-1 DO
	FOR i := 0 TO numElems-1 DO
		SYSTEM.GET(src+tag.pointerOffsets[i] + i * tag.recSize + tag.pointerOffsets[j], sval);
		CheckAssignment(dest+ i * tag.recSize + tag.pointerOffsets[j], sval);
	END;
	END;
	SYSTEM.MOVE(src,dest,tag.recSize * numElems);
	INC(assigns);	
END AssignArray;

```

Стало:
```
PROCEDURE MarkArray*(adr: ADDRESS; tag: StaticTypeBlockU; numElems: SIZE);
VAR i, j: SIZE; offset: ADDRESS; sval {UNTRACED}: ANY;
BEGIN
	IF CAS(marking,FALSE,FALSE) THEN
		FOR j := 0 TO LEN(tag.pointerOffsets)-1 DO
			FOR i := 0 TO numElems-1 DO
				offset := i * tag.recSize + tag.pointerOffsets[j];
				SYSTEM.GET(adr+offset, sval);
				Mark(sval);
			END;
		END;
	END;
END MarkArray;
```
Невзирая на этот код, похоже, что структура ARRAY N OF ANY и ARRAY OF ANY теперь отличаются, а раньше они не отличались, или я этого раньше не замечал. 

VAR ptr4 : POINTER TO ARRAY 3 OF ANY;
VAR ptr4d : POINTER TO ARRAY OF ANY;

======= PodrobnajaPechatq.PechVKontektse ptr4 ========: 
Izuchaju...0B2A3140...(DataBlockStaticTypeDesc = 0B2A0B68) (heapBlockDescAdr = 0B2A3128((#StaticTypeDesc(:info {#TypeInfoDesc(:descSize 52
     :tag 0B2A0B68
     :flags {}
     :mod 0B2A0648
     :name @HdPtrDesc)} :recSize 4 :pointerOffsetsLength 1
))IzuchiMassivIzvestnyjjKakMassiv: ehto vidObjqekta3DTMassivUkazatelejjNaANY
(r.Pech &0B2A3140~)(DT[3] : ((Massiv takov: 
0003 0000 3148 0B2A 30C0 0B2A 30C0 0B2A 3100 0B2A 0000 0000 0000 0000 0000 0000 )

))(r.Pech &0B2A3100~)( :vidObjqekta 6 0B2A3100 PrimerPodrobnajaPechatq.myRecType(
    f text (r.STz 0B2A3100 PrimerPodrobnajaPechatq:42B (1)~) "SSS"
    f pointerToArrayOfAny (r.STz 0B2A3104 PrimerPodrobnajaPechatq:44F (1)~) NIL)))
======= PodrobnajaPechatq.PechVKontektse ptr4d ========: 
Izuchaju...0B2A3180...(DataBlockStaticTypeDesc = 0B2A0B68) (heapBlockDescAdr = 0B2A3168((#StaticTypeDesc(:info {#TypeInfoDesc(:descSize 52
     :tag 0B2A0B68
     :flags {}
     :mod 0B2A0648
     :name @HdPtrDesc)} :recSize 4 :pointerOffsetsLength 1
))IzuchiMassivIzvestnyjjKakMassiv: ehto vidObjqekta3DTMassivUkazatelejjNaANY
(r.Pech &0B2A3180~)(DT[3] : ((Massiv takov: 
0003 0000 3190 0B2A 0003 0000 0000 0000 30C0 0B2A 30C0 0B2A 3100 0B2A 0000 0000 System.DoCommands: Command: 'PrimerPodrobnajaPechatq.z', parameters: None failed: Exception during command execution (res: 3904)

В версии Яос0 раскладка была одинаковой, а теперь поменялась. 

Кое-как залепил, похоже, что функционал не уменьшился, хотя всего не проверял. 

Чиним отладчик теперь:

Там были проблемы с типом и позицией - поправил. 

Вроде работает, возможно хуже, чем раньше, но как-то ходит в целом. 

Теперь пересобираем релиз, делаем из этой ветки ветку яос02, отправляем, назначаем её веткой по умолчанию
в gitlab. 
### Вызов функций с переменным числом аргументов

[источник](https://forum.oberoncore.ru/viewtopic.php?f=22&t=6383&start=100#p107721)
```
MODULE TestOpen; (** AUTHOR ""; PURPOSE ""; *)

IMPORT
   Commands, Strings;

PROCEDURE Proc(
   context: Commands.Context;
   CONST p1: ARRAY [*] OF LONGINT;
   CONST p2: ARRAY [*] OF LONGREAL;
   CONST p3: ARRAY [*] OF Strings.String
);
BEGIN
   context.out.Int(LEN(p1, 0), 0); context.out.Ln;
   context.out.Int(LEN(p2, 0), 0); context.out.Ln;
   context.out.Int(LEN(p3, 0), 0); context.out.Ln;
END Proc;

PROCEDURE Test*(context: Commands.Context);
BEGIN
   Proc(context, [1], [1, 2],
      [Strings.NewString("one"), Strings.NewString("two"), Strings.NewString("three")]
   );
END Test;

END TestOpen.
```
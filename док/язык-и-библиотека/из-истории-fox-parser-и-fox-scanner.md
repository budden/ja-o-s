Здесь рассматриваем журнал изменений для FoxParser и FoxScanner с конца 2012 года, а также их поддержку в TF* 
SHA указаны по моему репозиторию jaos.


```
2015-09-08 SHA-1: 3cea997c26bbb470c1d92fc0ff262e3a804d5845
 - исправлена определение размера при чтении целых чисел (была ошибка)

2015-08-20 SHA-1: 915fac92817a6c6215ffaa6edfb94422cc4f052e
 - отправка и получение с помощью <<? и >>?
 В тот же день исправлена ошибка!

2014-06-04 SHA-1: 6b7c5dd4030684d4bd5072b548b9d42f9703cd09
 - минимальная поддержка внешних символов (Extern)

2014-05-22 SHA-1: 127fc44640876e8a039aa2b8bad58152918e0956
 - FoxScanner добавлена поддержка 16-ричных литералов 0xABCD в стиле Си (сделано для TF)

2014-02-19 SHA-1: cb55b4d61e65aa174c828088e6e6657a5c7f95662
 - FoxScanner добавлен «^» для ассемблера ARM

2013-12-11 SHA-1: 8a117c3fff8ffc9a1096a1a5341d3a55d72daa43
 - FoxScanner добавлен «!» для ассемблера ARM

2013-07-11 SHA-1: 2bf5495ada893e893689ad861fa757de4ad23258
 - поддержка новых видов литералов строк:

String literals can now be defined in three ways:
(a) starting and ending with ":  "String. No commands. No escapes. EOS:"
(b) starting and ending with \" and "\ respectivey: \"String. With commands such as newline \n. "Substrings" possible. EOS escapable with \"\\ EOS:"\
(c) starting and ending with \<esc>" and "<esc>\, respectivly, where <esc> denotes a non-whitespace character : \+"String. No commands. No escapes. "Substring" possible. \"Substring"\ possible. \X"Substring"X\ possible. EOS:"+\
Multi string literals possible : Log.String("first string" "second string" \"Third string\n"\
Escaped strings also handled by command region selector in WMTextView => "~" can be escaped by strings [end of command otherwise].


2013-03-06 3cc149cac3354b557b - добавлена поддержка для «?» и «!» для получения и отправки через порты
Изменение в лексере: добавлены символы Questionmarks = «??» и ExclamationMarks = «!!»
```
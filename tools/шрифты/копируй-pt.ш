set -xe

P_S=$(cd $(dirname $0) && pwd);
# Нормализируем путь, чтобы в нём не было .. 
export PAPKA_SKRIPTA=`readlink -f $P_S`

IST="$PAPKA_SKRIPTA/истинные-шрифты"
PRIJOMN="$PAPKA_SKRIPTA/../../source"
rm $PRIJOMN/*.ttf || echo "Не смог удалить старые шрифты"
cd $PRIJOMN

# Обретаем шрифты
cp "$IST/PTMono-Regular-i-Cyrillic-Old.ttf" DejaVuSans.ttf

# Чтобы не усложнять анализ имён шрифтов, мы переименовываем
# файлы так, чтобы Яос их понимал. 

# в оригинале был DejaVuSans_bd.ttf
cp "$IST/PTMono-Bold-i-Cyrillic-Old.ttf" DejaVuSans_bd.ttf

# в оригинале был DejaVuSans-BoldOblique.ttf
cp "$IST/PTMono-Bold-i-Cyrillic-Old.ttf" DejaVuSans_bi.ttf

# в оригинале был DejaVuSans--Oblique.ttf
cp "$IST/PTMono-Regular-i-Cyrillic-Old.ttf" DejaVuSans_i.ttf

# Это модифицированный шрифт PTMono Bold, в котором
# латиница взята от condensed!
# И он по метрике наверняка несовместим с DejaVu. Ожидаем проблем!
cp "DejaVuSans.ttf" DejaVuSansMono.ttf

# Делаем хитрый финт ушами и вместо жирного шрифта подставляем такой же. 
# Поэтому жирность для данного шрифта работать не будет, она уже занята
# под отличение кир/лат. Зато хотя бы не будет кривой исходный текст.

cp DejaVuSansMono.ttf DejaVuSansMono_bd.ttf

# А раньше было так:
# cp "$IST/DejaVuSansMono_bd.ttf" DejaVuSansMono_bd.ttf


# Чтобы проблем не было, нужно всё переделать на базе
# одного шрифта, чтобы i и bi были того же размера. 
# но сейчас пора спать.

# В оригинале DejaVuSansMono-BoldOblique
cp "$IST/DejaVuSansMono_bi.ttf" DejaVuSansMono_bi.ttf

# В оригинале DejaVuSansMono-Oblique
cp "$IST/DejaVuSansMono_i.ttf" DejaVuSansMono_i.ttf


# И так далее - везде заменяем префиксы
cp "$IST/DejaVuSansCondensed.ttf" DejaVuSansCondensed.ttf
cp "$IST/DejaVuSansCondensed_bd.ttf" DejaVuSansCondensed_bd.ttf
cp "$IST/DejaVuSansCondensed_bi.ttf" DejaVuSansCondensed_bi.ttf
cp "$IST/DejaVuSansCondensed_i.ttf" DejaVuSansCondensed_i.ttf

# Делаем вид, что у нас много шрифтов, 
# чтобы меньше ломать совместимость и править код.
# Например, какие-то шрифты упомянуты в сборке под ARM, 
# какие-то упомянуты по имени прямо в исходнике. 

cp DejaVuSans.ttf Oberon.ttf
cp DejaVuSans_bd.ttf Oberon_bd.ttf
cp DejaVuSans_bi.ttf Oberon_bi.ttf
cp DejaVuSans_i.ttf Oberon_i.ttf

cp DejaVuSansMono.ttf VeraMo.ttf
cp DejaVuSansMono_bd.ttf VeraMo_bd.ttf
cp DejaVuSansMono_bi.ttf VeraMo_bi.ttf
cp DejaVuSansMono_i.ttf VeraMo_i.ttf

cp VeraMo.ttf Courier.ttf
cp VeraMo_bd.ttf Courier_bd.ttf
cp VeraMo_bi.ttf Courier_bi.ttf
cp VeraMo_i.ttf Courier_i.ttf

cp Oberon.ttf Vera.ttf
cp Oberon_bd.ttf Vera_bd.ttf
cp Oberon_bi.ttf Vera_bi.ttf
cp Oberon_i.ttf Vera_i.ttf

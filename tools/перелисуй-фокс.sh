#!/bin/bash


doIt() {
  # Проверяем, существует ли файл X
  if [ ! -f "$1" ]; then
    echo "File $1 does not exist"
  # Проверяем, существует ли файл Y
  elif [ ! -f "$2" ]; then
    echo "File $2 does not exist"
  else
    doIt2 "$1" "$2"
  fi
}

doIt2() {
  # Заменяем строку "Lis" на "Fox" в файле X и записываем результат во временный файл
  sed 's/Lis/Fox/g' "$1" > /tmp/X1

  # Заменяем строку "Lis" на "Fox" в файле Y и записываем результат во временный файл
  sed 's/Lis/Fox/g' "$2" > /tmp/Y1

  # Сравниваем строки в файлах X1 и Y1
  if cmp -s /tmp/X1 /tmp/Y1; then
    echo "$1 - изолисуально"
  else
    # Если строки не совпадают, вызываем meld
    meld "$1" "$2"
  fi
}

cd ../source
doIt LisScanner.перев.XML FoxScanner.перев.XML
doIt LisSyntaxTree.перев.XML FoxSyntaxTree.перев.XML
doIt LisSections.Mod FoxSections.Mod
doIt LisBinaryCode.Mod FoxBinaryCode.Mod
doIt LisFingerprinter.Mod FoxFingerprinter.Mod
doIt LisTextCompiler.Mod FoxTextCompiler.Mod
doIt LisInterfaceComparison.Mod FoxInterfaceComparison.Mod
doIt LisCodeGenerators.Mod FoxCodeGenerators.Mod
doIt LisAMD64Assembler.Mod FoxAMD64Assembler.Mod
doIt LisGenericObjectFile.Mod FoxGenericObjectFile.Mod
doIt LisAssembler.Mod FoxAssembler.Mod
doIt LisAMDBackend.Mod FoxAMDBackend.Mod
doIt LisARMInstructionSet.Mod FoxARMInstructionSet.Mod
doIt LisARMAssembler.Mod FoxARMAssembler.Mod
doIt LisARMBackend.Mod FoxARMBackend.Mod
doIt LisОтлПеч.Mod FoxОтлПеч.Mod
doIt LisГенЗагСлов.Mod FoxГенЗагСлов.Mod
doIt LisCompiler.Mod Compiler.Mod
doIt Macro.LisOberonFrontend.Mod Macro.FoxOberonFrontend.Mod
echo таких файлов нет, а надо сделать!
doIt LisMacroИПП.Mod FoxMacroИПП.Mod
doIt LisIntermediateBackend.Mod FoxIntermediateBackend.Mod
doIt LisIntermediateCode.Mod FoxIntermediateCode.Mod
doIt LisSemanticChecker.Mod FoxSemanticChecker.Mod
doIt LisPrintout.Mod FoxPrintout.Mod
doIt LisGlobal.Mod FoxGlobal.Mod
doIt LisПереводчик.Mod FoxПереводчик.Mod
doIt LisУтилитыДляПереводаКода.Mod FoxУтилитыДляПереводаКода.Mod
doIt LisTextualSymbolFile.Mod FoxTextualSymbolFile.Mod
doIt LisSyntaxTree.Mod FoxSyntaxTree.Mod
doIt LisScanner.Mod FoxScanner.Mod
doIt LisParser.Mod FoxParser.Mod
doIt LisBackend.Mod FoxBackend.Mod
doIt LisFormats.Mod FoxFormats.Mod
doIt LisFrontend.Mod FoxFrontend.Mod

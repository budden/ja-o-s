модуль WMPerfMonPluginCpu; (** AUTHOR "staubesv"; PURPOSE "Performance Monitor CPU load plugin"; *)
(**
 * History:
 *
 *	16.02.2006	First Release (staubesv)
 *	23.06.2006	Adapted to WMPerfMonPlugins (staubesv)
 *	12.03.2007	Moved CPU number detection to WMPerfMonPlugins.Mod (staubesv)
 *)

использует
	WMPerfMonPlugins, Строки8, Objects, ЭВМ, Modules;

конст

	CpuLoadAll = 9999;

	ModuleName = "WMPerfMonPluginCpu";

тип

	CpuLoadHelper = окласс(WMPerfMonPlugins.Helper);
	перем
		cpuLoad : массив ЭВМ.МаксКвоПроцессоров из вещ32; (* in % *)
		nbrOfCpus : цел32;

		lastTimeStamp, timestamp : цел64;
		lastCycles : массив ЭВМ.МаксКвоПроцессоров из цел64;

		проц {перекрыта}Update;
		перем cpuNbr : цел32; dCycles, total : вещ64;
		нач
			timestamp := ЭВМ.ДайКвоТактовПроцессораСМоментаПерезапуска();

			если lastTimeStamp # 0 то
				total := timestamp - lastTimeStamp;
				нцДля cpuNbr := 0 до ЭВМ.МаксКвоПроцессоров-1 делай
					если Objects.idleCycles[cpuNbr] # 0 то
						dCycles := Objects.idleCycles[cpuNbr] - lastCycles[cpuNbr];
						cpuLoad[cpuNbr] := 100.0 - устарПреобразуйКБолееУзкомуЦел(100.0 * (dCycles / total));
					всё;
				кц;
			всё;

			нцДля cpuNbr := 0 до ЭВМ.МаксКвоПроцессоров-1 делай lastCycles[cpuNbr] := Objects.idleCycles[cpuNbr]; кц;
			lastTimeStamp := timestamp;
		кон Update;

		(** Return the number of CPUs *)
		проц GetNbrOfCpus() : цел32;
		перем nbrOfCpus, cpuNbr : цел32;
		нач
			(* Determine number of CPUs *)
			nbrOfCpus := 0;
			нцДля cpuNbr := 0 до ЭВМ.МаксКвоПроцессоров-1 делай
				если Objects.idleCycles[cpuNbr] # 0 то
					(* TODO: Improve number of CPU detection *)
					увел(nbrOfCpus);
				всё;
			кц;
			возврат nbrOfCpus;
		кон GetNbrOfCpus;

		проц &New*;
		нач
			nbrOfCpus := GetNbrOfCpus();
		кон New;

	кон CpuLoadHelper;

тип

	CpuParameter* = укль на запись (WMPerfMonPlugins.Parameter);
		processorID* : размерМЗ;
	кон;

	CpuLoad* = окласс(WMPerfMonPlugins.Plugin)
	перем
		processorID : размерМЗ;
		h : CpuLoadHelper;

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		перем nbr : массив 4 из симв8;
		нач
			p.name := "CPU Load"; p.description := "100% - % the idle thread is running";
			p.modulename := ModuleName;
			p.min := 0; p.max := 100; p.unit := "%"; p.minDigits := 6; p.fraction := 2;

			processorID := p(CpuParameter).processorID;
			если processorID = CpuLoadAll то
				p.devicename := "All Processors";
			иначе
				p.devicename := "";
				Строки8.ПишиЦел64_вСтроку(processorID, nbr);
				Строки8.ПодклейВСтрокуХвост(p.devicename, "Processor P"); Строки8.ПодклейВСтрокуХвост(p.devicename, nbr);
			всё;

			p.helper := cpuLoadHelper; h := cpuLoadHelper;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		перем cpu : цел32; sum : вещ32;
		нач
			если processorID = CpuLoadAll то
				нцДля cpu := 0 до ЭВМ.МаксКвоПроцессоров-1 делай
					sum := sum + h.cpuLoad[cpu]
				кц;
				dataset[0] := sum / h.nbrOfCpus;
			иначе
				dataset[0] :=  h.cpuLoad[processorID];
			всё;
		кон UpdateDataset;

	кон CpuLoad;

тип

	ReadyCounter = окласс(WMPerfMonPlugins.Plugin);

		проц {перекрыта}Init(p : WMPerfMonPlugins.Parameter);
		нач
			p.name := "NumReady"; p.description := "Number of processes in ready queue";
			p.modulename := ModuleName;
			p.autoMax := истина; p.minDigits := 2;
		кон Init;

		проц {перекрыта}UpdateDataset;
		нач
			dataset[0] := Objects.NumReady();
		кон UpdateDataset;

	кон ReadyCounter;

перем
	(* Facilitates integration into Performance Monitor application *)
	nbrOfCpus- : цел32;
	cpuLoadHelper : CpuLoadHelper;

проц InitPlugins;
перем
	readyCounter : ReadyCounter; par : WMPerfMonPlugins.Parameter;
	c : CpuLoad; cpar : CpuParameter; proc : цел32;
нач
	нов(par); нов(readyCounter, par);
	нов(cpar); cpar.processorID := 0; нов(c, cpar);
	если cpuLoadHelper.nbrOfCpus > 1 то
		нцДля proc := 1 до cpuLoadHelper.nbrOfCpus-1 делай
			нов(cpar); cpar.processorID := proc; нов(c, cpar);
		кц;
		нов(cpar); cpar.processorID := CpuLoadAll; нов(c, cpar);
	всё;
кон InitPlugins;

проц Install*;
кон Install;

проц Cleanup;
нач
	WMPerfMonPlugins.updater.RemoveByModuleName(ModuleName);
кон Cleanup;

нач
	нов(cpuLoadHelper); nbrOfCpus := cpuLoadHelper.nbrOfCpus;
	InitPlugins;
	Modules.InstallTermHandler(Cleanup);
кон WMPerfMonPluginCpu.

WMPerfMonPluginProcessors.Install ~ 	System.Free WMPerfMonPluginProcessors ~

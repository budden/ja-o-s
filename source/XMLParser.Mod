модуль XMLParser;	(** AUTHOR "swalthert"; PURPOSE "XML parser"; *)

использует
	Строки8, ЛогЯдра, DynamicStrings, Потоки, Scanner := XMLScanner, XML;

конст
	Ok* = XML.Ok;
	UnknownError* = -1;
тип
	String = Строки8.уСтрока;

	Parser* = окласс
	перем
		scanner: Scanner.Scanner;
		dtd: XML.DocTypeDecl;
		elemReg*: XML.ElementRegistry;
		reportError*: проц {делегат} (pos, line, row: Потоки.ТипМестоВПотоке; конст msg: массив из симв8);
		res*: цел32; (* result success / error code *)
		ds1, ds2 : DynamicStrings.DynamicString; (** utility string, { (ds1 # NIL) & (ds2 # NIL) } *)

		проц &Init*(s: Scanner.Scanner);
		нач
			reportError := DefaultReportError;
			scanner := s;
			res := Ok;
			нов(ds1); нов(ds2);
		кон Init;

		проц Error(конст msg: массив из симв8);
		нач
			reportError(scanner.GetPos(), scanner.line, scanner.col, msg);
			res := UnknownError;
		кон Error;

		проц CheckSymbol(expectedSymbols: мнвоНаБитахМЗ; конст errormsg: массив из симв8): булево;
		нач
			если ~(scanner.sym в expectedSymbols) то
				Error(errormsg); возврат ложь
			иначе
				возврат истина
			всё
		кон CheckSymbol;

		проц ExpandCharacterRef(num: цел32): симв8;
		нач
			возврат симв8ИзКода(устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(num)))
		кон ExpandCharacterRef;

		проц ExpandEntityRef(конст name: массив из симв8; type: цел8): String;
		перем generalEntity: XML.EntityDecl;
		нач
			если dtd # НУЛЬ то
				generalEntity := dtd.GetEntityDecl(name, type);
				если generalEntity # НУЛЬ то
					возврат generalEntity.GetValue()
				иначе
					возврат НУЛЬ;
				всё
			иначе
				возврат НУЛЬ;
			всё;
		кон ExpandEntityRef;

		проц Parse*(): XML.Document;
		перем doc: XML.Document; e : XML.Element; s: String;
		нач
			нов(doc); doc.SetPos(scanner.GetPos()); dtd := doc.GetDocTypeDecl();
			scanner.ScanContent();	(* prolog *)
			если scanner.sym = Scanner.TagXMLDeclOpen то	(* XMLDecl? *)
				doc.AddContent(ParseXMLDecl());
				scanner.ScanContent()
			всё;
			нцПока (scanner.sym # Scanner.TagDeclOpen) и (scanner.sym # Scanner.TagElemStartOpen) делай	(* Misc* *)
				просей scanner.sym из
				| Scanner.TagPIOpen: doc.AddContent(ParseProcessingInstruction())
				| Scanner.Comment: doc.AddContent(ParseComment())
				иначе
					Error("unknown XML content (Document Type Declaration, Processing Instruction, Comment or Root Element expected)");
					возврат doc
				всё;
				scanner.ScanContent()
			кц;
			если scanner.sym = Scanner.TagDeclOpen то	(* (doctypedecl Misc* )? *)
				s := scanner.GetString(Scanner.Str_Other);				(* doctypedecl .. *)
				если s^ = 'DOCTYPE' то
					ParseDocTypeDecl(); doc.AddContent(dtd)
				иначе
					Error("'<!DOCTYPE' expected"); возврат doc
				всё;
				scanner.ScanContent();
				нцПока (scanner.sym # Scanner.TagElemStartOpen) делай	(* .. Misc* *)
					просей scanner.sym из
					| Scanner.TagPIOpen: doc.AddContent(ParseProcessingInstruction())
					| Scanner.Comment: doc.AddContent(ParseComment())
					| Scanner.TagElemStartOpen: (* do nothing *)
					иначе Error("unknown XML content (Processing Instruction, Comment or Root Element expected)"); возврат doc
					всё;
					scanner.ScanContent()
				кц
			всё;
			e := ParseElement();
			если e = НУЛЬ то возврат НУЛЬ всё;
			doc.AddContent(e);	(* element *)
			scanner.ScanContent();
			нцПока scanner.sym # Scanner.Eof делай	(* Misc* *)
				просей scanner.sym из
				| Scanner.TagPIOpen: doc.AddContent(ParseProcessingInstruction())
				| Scanner.Comment: doc.AddContent(ParseComment())
				| Scanner.Eof: (* do nothing *)
				иначе Error("unknown XML content (Processing Instruction, Comment or End of file expected)"); возврат doc
				всё;
				scanner.ScanContent()
			кц;
			возврат doc
		кон Parse;

		проц ParseExtGenEntity*(extEntityRef: XML.ExternalEntityRef);
		нач
			scanner.ScanContent();
			если scanner.sym = Scanner.TagXMLDeclOpen то
				extEntityRef.AddContent(ParseTextDecl());
				scanner.ScanContent()
			всё;
			нцДо
				просей scanner.sym из
				| Scanner.CharData: extEntityRef.AddContent(ParseCharData())
				| Scanner.TagElemStartOpen: extEntityRef.AddContent(ParseElement())
				| Scanner.CharRef: extEntityRef.AddContent(ParseCharRef())
				| Scanner.EntityRef: extEntityRef.AddContent(ParseEntityRef())
				| Scanner.CDataSect: extEntityRef.AddContent(ParseCDataSect())
				| Scanner.Comment: extEntityRef.AddContent(ParseComment())
				| Scanner.TagPIOpen: extEntityRef.AddContent(ParseProcessingInstruction())
				| Scanner.TagElemEndOpen: (* do nothing *)
				| Scanner.Eof: Error("element not closed"); возврат
				иначе
					Error("unknown Element Content"); возврат
				всё;
				scanner.ScanContent()
			кцПри scanner.sym = Scanner.Eof
		кон ParseExtGenEntity;

		проц ParseXMLDecl(): XML.XMLDecl;
		перем decl: XML.XMLDecl; s: String;
		нач
			нов(decl); decl.SetPos(scanner.GetPos());
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Name}, "'version' expected") то возврат decl всё;
			s := scanner.GetString(Scanner.Str_Other);
			если s^ # "version" то Error("'version' expected"); возврат decl всё;
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Equal}, "'=' expected") то возврат decl всё;
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Literal}, "Version Number expected") то возврат decl всё;
			s := scanner.GetString(Scanner.Str_Other);
			decl.SetVersion(s^);
			scanner.ScanMarkup(); s := scanner.GetString(Scanner.Str_Other);
			если (scanner.sym = Scanner.Name) и (s^ = "encoding") то
				scanner.ScanMarkup();
				если ~CheckSymbol({Scanner.Equal}, "'=' expected") то возврат decl всё;
				scanner.ScanMarkup();
				если ~CheckSymbol({Scanner.Literal}, "Encoding Name expected") то возврат decl всё;
				s := scanner.GetString(Scanner.Str_Other);
				decl.SetEncoding(s^);
				scanner.ScanMarkup(); s := scanner.GetString(Scanner.Str_Other)
			всё;
			если (scanner.sym = Scanner.Name) и (s^ = "standalone") то
				scanner.ScanMarkup();
				если ~CheckSymbol({Scanner.Equal}, "'=' expected") то возврат decl всё;
				scanner.ScanMarkup();
				если ~CheckSymbol({Scanner.Literal}, '"yes" or "no" expected') то возврат decl всё;
				s := scanner.GetString(Scanner.Str_Other);
				если s^ = "yes" то decl.SetStandalone(истина)
				аесли s^ = "no" то decl.SetStandalone(ложь)
				иначе Error('"yes" or "no" expected'); возврат decl
				всё;
				scanner.ScanMarkup()
			всё;
			если ~CheckSymbol({Scanner.TagPIClose}, "'?>' expected") то возврат decl всё;
			возврат decl
		кон ParseXMLDecl;

		проц ParseTextDecl(): XML.TextDecl;
		перем decl: XML.TextDecl; s: String;
		нач
			нов(decl); decl.SetPos(scanner.GetPos());
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Name}, "'version' expected") то возврат decl всё;
			s := scanner.GetString(Scanner.Str_Other);
			если s^ # "version" то Error("'version' expected"); возврат decl всё;
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Equal}, "'=' expected") то возврат decl всё;
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Literal}, "Version Number expected") то возврат decl всё;
			s := scanner.GetString(Scanner.Str_Other);
			decl.SetVersion(s^);
			scanner.ScanMarkup(); s := scanner.GetString(Scanner.Str_Other);
			если (scanner.sym = Scanner.Name) и (s^ = "encoding") то
				scanner.ScanMarkup();
				если ~CheckSymbol({Scanner.Equal}, "'=' expected") то возврат decl всё;
				scanner.ScanMarkup();
				если ~CheckSymbol({Scanner.Literal}, "Encoding Name expected") то возврат decl всё;
				s := scanner.GetString(Scanner.Str_Other);
				decl.SetEncoding(s^);
				scanner.ScanMarkup(); s := scanner.GetString(Scanner.Str_Other)
			всё;
			если ~CheckSymbol({Scanner.TagPIClose}, "'?>' expected") то возврат decl всё;
			возврат decl
		кон ParseTextDecl;

		проц ParseComment(): XML.Comment;
		перем comment: XML.Comment; s: String;
		нач
			нов(comment); comment.SetPos(scanner.GetPos());
			s := scanner.GetString(Scanner.Str_Comment);
			comment.SetStrAsString(s);
			возврат comment
		кон ParseComment;

		проц ParseProcessingInstruction(): XML.ProcessingInstruction;
		перем pi: XML.ProcessingInstruction; s: String;
		нач
			нов(pi); pi.SetPos(scanner.GetPos());
			s := scanner.GetString(Scanner.Str_ProcessingInstruction);
			pi.SetTarget(s^);
			scanner.ScanPInstruction();
			если ~CheckSymbol({Scanner.TagPIClose}, "'?>' expected") то возврат pi всё;
			s := scanner.GetString(Scanner.Str_ProcessingInstruction);
			pi.SetInstruction(s^);
			возврат pi
		кон ParseProcessingInstruction;

		проц ParseDocTypeDecl;
		перем externalSubset: XML.EntityDecl; s: String;
		нач
			нов(dtd); dtd.SetPos(scanner.GetPos());
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Name}, "DTD name expected") то возврат всё;
			s := scanner.GetString(Scanner.Str_Other); dtd.SetNameAsString(s);
			scanner.ScanMarkup();
			если scanner.sym = Scanner.Name то	(* DTD points to external subset *)
				нов(externalSubset); externalSubset.SetPos(scanner.GetPos());
				s := scanner.GetString(Scanner.Str_Other);
				если s^ = 'SYSTEM' то
					s := ParseSystemLiteral();
					externalSubset.SetSystemId(s^)
				аесли s^ = 'PUBLIC' то
					s := ParsePubidLiteral();
					externalSubset.SetPublicId(s^);
					s := ParseSystemLiteral();
					externalSubset.SetSystemId(s^)
				иначе
					Error("'SYSTEM' or 'PUBLIC' expected"); возврат
				всё;
				dtd.SetExternalSubset(externalSubset);
				scanner.ScanMarkup()
			всё;
			если scanner.sym = Scanner.BracketOpen то	(* markupdecl *)
				ParseMarkupDecls()
			всё;
			если ~CheckSymbol({Scanner.TagClose}, "'>' expected") то возврат всё;
		кон ParseDocTypeDecl;

		проц ParseMarkupDecls;
		перем s: String; (* oldscanner: Scanner.Scanner; *)
		нач
			нцДо
				scanner.ScanMarkup();
				просей scanner.sym из
				| Scanner.TagDeclOpen:
						s := scanner.GetString(Scanner.Str_Other);
						если s^ = 'ELEMENT' то
							ParseElementDecl(dtd)
						аесли s^ = 'ATTLIST' то
							ParseAttListDecl(dtd)
						аесли s^ = 'ENTITY' то
							ParseEntityDecl(dtd)
						аесли s^ = 'NOTATION' то
							ParseNotationDecl(dtd)
						иначе
							Error("'ELEMENT', 'ATTLIST' or 'NOTATION' expected"); возврат
						всё
				|Scanner.TagPIOpen: dtd.AddMarkupDecl(ParseProcessingInstruction())
				| Scanner.Comment: dtd.AddMarkupDecl(ParseComment())
			(*	| Scanner.ParamEntityRef:
					s := scanner.GetStr();
					s := ExpandEntityRef(s^, XML.ParameterEntity);
					f := Files.New(""); Files.OpenWriter(w, f, 0); w.Bytes(s^, 0, LEN(s^) - 1); w.Update;
					oldscanner := scanner;
					NEW(scanner, f);
					ParseMarkupDecls();
					scanner := oldscanner *)
				| Scanner.BracketClose: (* end of markupdecl *)
				| Scanner.Eof, Scanner.Invalid: возврат
				иначе
					Error("unknown markup declaration"); возврат
				всё
			кцПри scanner.sym = Scanner.BracketClose;
			scanner.ScanMarkup()
		кон ParseMarkupDecls;

		(*
			elementdecl ::=	'<!ELEMENT' S Name S contentspec S? '>"
			contentspec ::=	'EMPTY' | 'ANY' | Mixed | children
			S ::=			(#x20 | #x9 | #xD | #xA)+
		*)
		проц ParseElementDecl(dtd: XML.DocTypeDecl);
		перем ed: XML.ElementDecl; ccp: XML.CollectionCP; s: String;
			contentType: цел8;
		нач
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Name}, "Element name expected") то возврат всё;
			s := scanner.GetString(Scanner.Str_ElementName);
			ed := dtd.GetElementDecl(s^);
			если ed = НУЛЬ то	(* Attribute List Declaration not occured yet -> create new element declaration and add it to the DTD *)
				нов(ed); ed.SetPos(scanner.GetPos());
				ed.SetNameAsString(s);
				dtd.AddMarkupDecl(ed)
			всё;
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Name, Scanner.ParenOpen}, "'EMPTY', 'ANY', Mixed or Element Content expected") то
				возврат всё;
			если scanner.sym = Scanner.Name то
				s := scanner.GetString(Scanner.Str_Other);
				если s^ = 'EMPTY' то
					ed.SetContentType(XML.Empty)
				аесли s^ = 'ANY' то
					ed.SetContentType(XML.Any)
				иначе
					Error("'EMPTY' or 'ANY' expected"); возврат
				всё;
				scanner.ScanMarkup();
				если ~CheckSymbol({Scanner.TagClose}, "'>' expected") то возврат всё;
			аесли scanner.sym = Scanner.ParenOpen то	(* Mixed or children element content *)
				ccp := ParseContentParticle(contentType);
				ed.SetContent(ccp);
				ed.SetContentType(contentType)
			всё
		кон ParseElementDecl;

		проц ParseAttListDecl(dtd: XML.DocTypeDecl);
		перем ed: XML.ElementDecl; ad: XML.AttributeDecl; s: String;
		нач
			scanner.ScanMarkup();	(* parse element name *)
			если ~CheckSymbol({Scanner.Name}, "Element name expected") то возврат всё;
			s := scanner.GetString(Scanner.Str_AttributeName);
			ed := dtd.GetElementDecl(s^);
			если ed = НУЛЬ то	(* Element Declaration not occured yet -> create new element declaration and add it to the DTD *)
				нов(ed); ed.SetPos(scanner.GetPos());
				ed.SetNameAsString(s);
				dtd.AddMarkupDecl(ed)
			всё;
			scanner.ScanMarkup();
			нцПока (scanner.sym # Scanner.TagClose)  делай	(* parse AttDefs *)
				если ~CheckSymbol({Scanner.Name}, "Attribute Name expected") то возврат всё;
				s := scanner.GetString(Scanner.Str_AttributeName); нов(ad); ad.SetPos(scanner.GetPos());
				ad.SetNameAsString(s);
				scanner.ScanMarkup();
				если ~CheckSymbol({Scanner.Name, Scanner.ParenOpen}, "Attribute Type expected") то возврат всё;
				если scanner.sym = Scanner.Name то
					s := scanner.GetString(Scanner.Str_Other);
					если s^ = 'CDATA' то ad.SetType(XML.CData)
					аесли s^ = 'ID' то ad.SetType(XML.Id)
					аесли s^ = 'IDREF' то ad.SetType(XML.IdRef)
					аесли s^ = 'IDREFS' то ad.SetType(XML.IdRefs)
					аесли s^ = 'ENTITY' то ad.SetType(XML.Entity)
					аесли s^ = 'ENTITIES' то ad.SetType(XML.Entities)
					аесли s^ = 'NMTOKEN' то ad.SetType(XML.NmToken)
					аесли s^ = 'NMTOKENS' то ad.SetType(XML.NmTokens)
					аесли s^ = 'NOTATION' то
						ad.SetType(XML.Notation);
						scanner.ScanMarkup();
						если ~CheckSymbol({Scanner.ParenOpen}, "'(' expected") то возврат всё;
						scanner.ScanMarkup();
						если ~CheckSymbol({Scanner.Name}, "Notation Name expected") то возврат всё;
						scanner.ScanMarkup()
					иначе Error("Attribute Type expected"); возврат
					всё
				аесли scanner.sym = Scanner.ParenOpen то
					ad.SetType(XML.Enumeration);
					scanner.ScanMarkup();
					если ~CheckSymbol({Scanner.Name, Scanner.Nmtoken}, "Value Nmtoken expected") то возврат всё;
				всё;
				если (ad.GetType() = XML.Notation) или (ad.GetType() = XML.Enumeration) то
					нцПока (scanner.sym = Scanner.Name) или
							((scanner.sym = Scanner.Nmtoken) и (ad.GetType() = XML.Enumeration)) делай
						s := scanner.GetString(Scanner.Str_Other);
						ad.AddAllowedValue(s^);
						scanner.ScanMarkup();
						если scanner.sym = Scanner.Or то
							scanner.ScanMarkup()
						всё
					кц;
					если ~CheckSymbol({Scanner.ParenClose}, "')' expected") то возврат всё;
				всё;
				scanner.ScanMarkup();
				s := scanner.GetString(Scanner.Str_Other);	(* parse DefaultDecl *)
				если ~CheckSymbol({Scanner.PoundName, Scanner.Literal},
					"'#REQUIRED', '#IMPLIED', '#FIXED' or AttValue expected") то возврат всё;
				если scanner.sym =  Scanner.PoundName то
					если (s^ = '#REQUIRED') то
						ad.SetRequired(истина)
					аесли (s^ = '#FIXED') то
						ad.SetRequired(истина);
						scanner.ScanMarkup();
						если ~CheckSymbol({Scanner.Literal}, "AttValue expected") то возврат всё
					аесли (s^ = '#IMPLIED') то
						ad.SetRequired(ложь)
					иначе
						Error("'#REQUIRED', '#IMPLIED' or '#FIXED' expected"); возврат
					всё
				аесли scanner.sym = Scanner.Literal то
					ad.SetRequired(ложь)
				всё;
				если (scanner.sym = Scanner.Literal) то
					s := ParseAttributeValue();
					ad.SetDefaultValue(s^)
				всё;
				scanner.ScanMarkup();
				ed.AddAttributeDecl(ad);
			кц;
			если ~CheckSymbol({Scanner.TagClose}, "'>' expected") то возврат всё;
		кон ParseAttListDecl;

		(*
			Mixed		::=	'(' S? '#PCDATA' (S? '|' S? Name)* S? ')*'
							| '(' S? '#PCDATA' S? ')'

			children		::=	(choise | seq) ('?' | '*' | '+')?
			cp			::=	(Name | choise | seq) ('?' | '*' | '+')?
			choice		::=	'(' S? cp (S? '|' S? cp)+ S? ')'
			seq			::=	'(' S? cp (S? ',' S? cp)* S? ')'
		*)
		проц ParseContentParticle(перем contentType: цел8): XML.CollectionCP;
		перем cp: XML.ContentParticle; ncp: XML.NameContentParticle; ccp: XML.CollectionCP; s: String;
		нач
			если ~CheckSymbol({Scanner.ParenOpen}, "'(' expected") то возврат ccp всё;
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Name, Scanner.PoundName, Scanner.ParenOpen},
					"Element Name, '#PCDATA' or '(' expected") то возврат ccp всё;
			если scanner.sym = Scanner.PoundName то
				contentType := XML.MixedContent;
				s := scanner.GetString(Scanner.Str_Other);
				если s^ = '#PCDATA' то
					нов(ncp); ncp.SetPos(scanner.GetPos()); ncp.SetNameAsString(s); ncp.SetOccurence(XML.Once);
					нов(ccp); ccp.SetType(XML.Choice); ccp.AddChild(ncp);
					scanner.ScanMarkup();
					если ~CheckSymbol({Scanner.ParenClose, Scanner.Or}, "')' or '|' expected") то возврат ccp всё;
					если scanner.sym = Scanner.ParenClose то
						scanner.ScanMarkup();
						если ~CheckSymbol({Scanner.Asterisk, Scanner.TagClose}, "'*' or '>' expected") то возврат ccp всё;
						если scanner.sym = Scanner.Asterisk то
							ccp.SetOccurence(XML.ZeroOrMore);
							scanner.ScanMarkup();
							если ~CheckSymbol({Scanner.TagClose}, "'>' expected") то возврат ccp всё
						аесли scanner.sym = Scanner.TagClose то
							ccp.SetOccurence(XML.Once)
						всё;
						cp := ccp
					аесли scanner.sym = Scanner.Or то
						нцПока scanner.sym = Scanner.Or делай
							scanner.ScanMarkup();
							если ~CheckSymbol({Scanner.Name}, "Element Name expected") то возврат ccp всё;
							s := scanner.GetString(Scanner.Str_Other); нов(ncp); ncp.SetPos(scanner.GetPos());
							ncp.SetNameAsString(s); ncp.SetOccurence(XML.Once);
							ccp.AddChild(ncp);
							scanner.ScanMarkup();
							если ~CheckSymbol({Scanner.ParenClose, Scanner.Or}, "')' or '|' expected") то возврат ccp всё
						кц;
						scanner.ScanMarkup();
						если ~CheckSymbol({Scanner.Asterisk}, "'*' expected") то возврат ccp всё;
						ccp.SetOccurence(XML.ZeroOrMore);
						scanner.ScanMarkup();
						если ~CheckSymbol({Scanner.TagClose}, "'>' expected") то возврат ccp всё;
						cp := ccp
					всё
				иначе
					Error('"#PCDATA" expected'); возврат ccp
				всё
			иначе
				cp := ParseElementContent();
				если ~CheckSymbol({Scanner.Or, Scanner.Comma, Scanner.ParenClose}, "'|' or ',' expected") то возврат ccp всё;
				если scanner.sym = Scanner.Or то
					нов(ccp);
					ccp.SetType(XML.Choice); ccp.AddChild(cp);
					нцДо
						scanner.ScanMarkup();
						ccp.AddChild(ParseElementContent());
						если ~CheckSymbol({Scanner.Or, Scanner.ParenClose}, "'|' or ')' expected") то возврат ccp всё;
					кцПри scanner.sym = Scanner.ParenClose;
					cp := ccp
				аесли scanner.sym = Scanner.Comma то
					нов(ccp);
					ccp.SetType(XML.Sequence); ccp.AddChild(cp);
					нцДо
						scanner.ScanMarkup();
						ccp.AddChild(ParseElementContent());
						если ~CheckSymbol({Scanner.Comma, Scanner.ParenClose}, "',' or ')' expected") то возврат ccp всё;
					кцПри scanner.sym = Scanner.ParenClose;
					cp := ccp
				аесли scanner.sym = Scanner.ParenClose то
					нов(ccp);
					ccp.SetType(XML.Sequence); ccp.AddChild(cp);
					cp := ccp;
				всё;
				scanner.ScanMarkup();
				просей scanner.sym из
				| Scanner.Question: cp.SetOccurence(XML.ZeroOrOnce);
					scanner.ScanMarkup(); если ~CheckSymbol({Scanner.TagClose}, "'>' expected") то возврат ccp всё
				| Scanner.TagPIClose: cp.SetOccurence(XML.ZeroOrOnce)
				| Scanner.Asterisk: cp.SetOccurence(XML.ZeroOrMore);
					scanner.ScanMarkup(); если ~CheckSymbol({Scanner.TagClose}, "'>' expected") то возврат ccp всё
				| Scanner.Plus: cp.SetOccurence(XML.OnceOrMore);
					scanner.ScanMarkup(); если ~CheckSymbol({Scanner.TagClose}, "'>' expected") то возврат ccp всё
				иначе cp.SetOccurence(XML.Once);
					если ~CheckSymbol({Scanner.TagClose}, "'>' expected") то возврат ccp всё
				всё
			всё;
			возврат cp(XML.CollectionCP)
		кон ParseContentParticle;

		проц ParseElementContent(): XML.ContentParticle;
		перем cp: XML.ContentParticle; ncp: XML.NameContentParticle; ccp: XML.CollectionCP; s: String;
		нач
			если ~CheckSymbol({Scanner.Name, Scanner.ParenOpen}, "Element Name or '(' expected") то возврат cp всё;
			если scanner.sym = Scanner.Name то
				нов(ncp); ncp.SetPos(scanner.GetPos()); s := scanner.GetString(Scanner.Str_Other);
				ncp.SetNameAsString(s); cp := ncp
			аесли scanner.sym = Scanner.ParenOpen то
				scanner.ScanMarkup();
				cp := ParseElementContent();
				если ~CheckSymbol({Scanner.Or, Scanner.Comma}, "'|' or ',' expected") то возврат cp всё;
				если scanner.sym = Scanner.Or то
					нов(ccp); ccp.SetPos(scanner.GetPos());
					ccp.SetType(XML.Choice); ccp.AddChild(cp);
					нцДо
						scanner.ScanMarkup();
						ccp.AddChild(ParseElementContent());
						если ~CheckSymbol({Scanner.Or, Scanner.ParenClose}, "'|' or ')' expected") то возврат cp всё;
					кцПри scanner.sym = Scanner.ParenClose;
					cp := ccp
				аесли scanner.sym = Scanner.Comma то
					нов(ccp); ccp.SetPos(scanner.GetPos());
					ccp.SetType(XML.Sequence); ccp.AddChild(cp);
					нцДо
						scanner.ScanMarkup();
						ccp.AddChild(ParseElementContent());
						если ~CheckSymbol({Scanner.Comma, Scanner.ParenClose}, "',' or ')' expected") то возврат cp всё
					кцПри scanner.sym = Scanner.ParenClose;
					cp := ccp
				всё
			всё;
			scanner.ScanMarkup();
			просей scanner.sym из
			| Scanner.Question: cp.SetOccurence(XML.ZeroOrOnce); scanner.ScanMarkup()
			| Scanner.Asterisk: cp.SetOccurence(XML.ZeroOrMore); scanner.ScanMarkup()
			| Scanner.Plus: cp.SetOccurence(XML.OnceOrMore); scanner.ScanMarkup()
			иначе cp.SetOccurence(XML.Once)
			всё;
			возврат cp
		кон ParseElementContent;

		проц ParseEntityDecl(dtd: XML.DocTypeDecl);
		перем ed: XML.EntityDecl; s: String;
		нач
			нов(ed);
			ed.SetPos(scanner.GetPos());
			scanner.ScanMarkup();
			если scanner.sym = Scanner.Percent то	(* Parameter Entity Decl *)
				ed.SetType(XML.ParameterEntity);
				scanner.ScanMarkup()
			иначе	(* General Entity Declaration *)
				ed.SetType(XML.GeneralEntity);
			всё;
			если ~CheckSymbol({Scanner.Name}, "Entity Declaration Name expected") то возврат всё;
			s := scanner.GetString(Scanner.Str_Other);
			ed.SetNameAsString(s);
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Literal, Scanner.Name}, "EntityValue, 'SYSTEM' or 'PUBLIC' expected") то возврат всё;
			если scanner.sym = Scanner.Literal то	(* EntityValue *)
				s := ParseEntityValue();
				ed.SetValue(s^);
				scanner.ScanMarkup()
			аесли scanner.sym = Scanner.Name то	(* ExternalID *)
				s := scanner.GetString(Scanner.Str_Other);
				если s^ = 'SYSTEM' то
					s := ParseSystemLiteral();
					ed.SetSystemId(s^);
					scanner.ScanMarkup()
				аесли s^ = 'PUBLIC' то
					s := ParsePubidLiteral();
					ed.SetPublicId(s^);
					s := ParseSystemLiteral();
					ed.SetSystemId(s^);
					scanner.ScanMarkup()
				иначе
					Error("'SYSTEM' or 'PUBLIC' expected"); возврат
				всё;
				если (scanner.sym = Scanner.Name) и (ed.GetType() = XML.GeneralEntity) то
					s := scanner.GetString(Scanner.Str_Other);
					если s^ = 'NDATA' то	(* NDataDecl *)
						scanner.ScanMarkup();
						если ~CheckSymbol({Scanner.Name}, "Notation Name expected") то возврат всё;
						s := scanner.GetString(Scanner.Str_Other);
						ed.SetNotationName(s^);
						scanner.ScanMarkup()
					иначе
						Error("'NDATA' expected"); возврат
					всё
				всё
			иначе
				Error("EntityValue or SystemId expected"); возврат
			всё;
			если ~CheckSymbol({Scanner.TagClose}, "'>' expected") то возврат всё;
			dtd.AddMarkupDecl(ed)
		кон ParseEntityDecl;

		проц ParseNotationDecl(dtd: XML.DocTypeDecl);
		перем nd: XML.NotationDecl; s: String;
		нач
			нов(nd); nd.SetPos(scanner.GetPos());
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Name}, "Notation Name expected") то возврат всё;
			s := scanner.GetString(Scanner.Str_Other);
			nd.SetNameAsString(s);
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Name}, "'PUBLIC' or 'SYSTEM' expected") то возврат всё;
			s := scanner.GetString(Scanner.Str_Other);
			если s^ = 'PUBLIC' то
				s := ParsePubidLiteral();
				nd.SetPublicId(s^);
				scanner.ScanMarkup();
				если scanner.sym = Scanner.Literal то	(* ExternalID 1 *)
					s := scanner.GetString(Scanner.Str_Other);
					nd.SetSystemId(s^);
					scanner.ScanMarkup()
				иначе	(* PublicID, nothing more *)
				всё
			аесли s^ = 'SYSTEM' то	(* ExternalID 2 *)
				s := ParseSystemLiteral();
				nd.SetSystemId(s^);
				scanner.ScanMarkup()
			всё;
			если ~CheckSymbol({Scanner.TagClose}, "'>' expected") то возврат всё;
			dtd.AddMarkupDecl(nd)
		кон ParseNotationDecl;

		проц ParseSystemLiteral(): String;
		перем systemLiteral: String;
		нач
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Literal}, "System Literal expected") то возврат systemLiteral всё;
			systemLiteral := scanner.GetString(Scanner.Str_SystemLiteral);
			возврат systemLiteral
		кон ParseSystemLiteral;

		проц ParsePubidLiteral(): String;
		перем pubidLiteral: String;
		нач
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Literal}, "PubidLiteral expected") то возврат pubidLiteral всё;
			pubidLiteral := scanner.GetString(Scanner.Str_PublicLiteral);
			если ~IsPubidLiteral(pubidLiteral^) то Error("not a correct Pubid Literal"); возврат pubidLiteral всё;
			возврат pubidLiteral
		кон ParsePubidLiteral;

		проц ParseCDataSect(): XML.CDataSect;
		перем cds: XML.CDataSect; s: String;
		нач
			нов(cds); cds.SetPos(scanner.GetPos());
			s := scanner.GetString(Scanner.Str_CDataSection);
			cds.SetStrAsString(s);
			возврат cds
		кон ParseCDataSect;

		проц ParseCharData(): XML.ArrayChars;
		перем cd: XML.ArrayChars; oldpos: цел32; s,s2: String;
		нач
			oldpos := scanner.GetOldPos();
			нов(cd); (* cd.SetFilePos(scanner.GetFile(), scanner.GetOldPos()); cd.SetLen(scanner.GetPos() - oldpos); *)
			cd.SetPos(scanner.GetPos());
			s := scanner.GetString(Scanner.Str_CharData);
			s := ExpandCharacterRefs(s);
			cd.SetStrAsString(s);
			возврат cd
		кон ParseCharData;

		проц ParseElement(): XML.Element;
		перем e: XML.Element; c: XML.Content; empty: булево;
		нач
			ParseStartTag(e, empty);
			если e = НУЛЬ то возврат НУЛЬ всё;
			если ~empty то
				нцДо
					scanner.ScanContent();
					просей scanner.sym из
					| Scanner.CharData: c := ParseCharData();
					| Scanner.TagElemStartOpen: c := ParseElement();
					| Scanner.CharRef: c := ParseCharRef();
					| Scanner.EntityRef: c := ParseEntityRef();
					| Scanner.CDataSect: c := ParseCDataSect();
					| Scanner.Comment: c := ParseComment();
					| Scanner.TagPIOpen: c := ParseProcessingInstruction();
					| Scanner.TagElemEndOpen: c := НУЛЬ; (* do nothing *)
					| Scanner.Eof: Error("element not closed"); возврат e
					иначе
						Error("unknown Element Content"); возврат e
					всё;
					если c # НУЛЬ то e.AddContent(c) всё;
				кцПри scanner.sym = Scanner.TagElemEndOpen;
				ParseEndTag(e);
			всё;
			возврат e
		кон ParseElement;

		проц ParseStartTag(перем e: XML.Element; перем empty: булево);
		перем s: String; pos: цел32; firstInstantiationFailed: булево;
		нач
			pos := scanner.GetOldPos();
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Name}, "Element Name expected") то возврат всё;
			s := scanner.GetString(Scanner.Str_ElementName);
			если elemReg # НУЛЬ то
				e := elemReg.InstantiateElement(s^)
			всё;
			если e = НУЛЬ то
				firstInstantiationFailed := истина; нов(e)
			иначе
				firstInstantiationFailed := ложь;
			всё;
			e.SetPos(scanner.GetPos());
			e.SetNameAsString(s);
			scanner.ScanMarkup();
			нцПока scanner.sym = Scanner.Name делай
				e.AddAttribute(ParseAttribute());
				scanner.ScanMarkup();
			кц;
			если (elemReg # НУЛЬ) и (firstInstantiationFailed) то
				e := elemReg.InstantiateLate(e);
				e.SetNameAsString(s);
			всё;
			если ~CheckSymbol({Scanner.TagEmptyElemClose, Scanner.TagClose}, "'/>' or '>' expected") то возврат всё;
			если scanner.sym = Scanner.TagEmptyElemClose то
				empty := истина
			аесли scanner.sym = Scanner.TagClose то
				empty := ложь
			всё
		кон ParseStartTag;

		проц ParseAttribute(): XML.Attribute;
		перем a: XML.Attribute; s: String;
		нач
			нов(a); a.SetPos(scanner.GetPos());
			s := scanner.GetString(Scanner.Str_AttributeName);
			a.SetNameAsString(s);
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Equal}, "'=' expected") то возврат a всё;
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Literal}, "Attribute Value expected") то возврат a всё;
			s := ParseAttributeValue();
			a.SetValueAsString(s);
			возврат a
		кон ParseAttribute;

		проц ParseEndTag(e: XML.Element);
		перем ds: DynamicStrings.DynamicString; s1, s2: String; msg: массив 12 из симв8;
		нач
			scanner.ScanMarkup();
			s1 := scanner.GetString(Scanner.Str_ElementName); s2 := e.GetName();
			если (scanner.sym = Scanner.Name) и (s1^ = s2^) то
				scanner.ScanMarkup();
				если ~CheckSymbol({Scanner.TagClose}, "'>' expected") то возврат всё;
			иначе
				нов(ds);
				msg := "'</'"; ds.Append(msg); ds.Append(s2^);
				msg := ">' expected"; ds.Append(msg); s1 := ds.ToArrOfChar();
				Error(s1^); возврат
			всё
		кон ParseEndTag;

		проц ExpandCharacterRefs(s: String): String;
		перем
			from, to: цел32;
			ch : симв8;

			проц ReplaceEntity(конст source: массив из симв8; перем srcPos: цел32; перем dest: массив из симв8; перем destPos: цел32);
			перем ch: симв8; name: массив 32 из симв8; sp, dp: цел32; string: Строки8.уСтрока; pos: цел32;
			нач
				утв(source[srcPos] = "&");
				sp := srcPos+1;
				нцДо
					ch := source[sp];
					name[dp] := ch;
					увел(sp); увел(dp);
				кцПри (ch = ";") или (ch = 0X) или (dp >= длинаМассива(name));
				name[dp-1] := 0X;

				если ch = ";" то
					string := ExpandPredefinedEntity(name);
					если string # НУЛЬ то
						pos := 0;
						нцДо
							ch := string[pos];
							dest[destPos] := ch;
							увел(pos); увел(destPos);
						кцПри ch = 0X;
						srcPos := sp -1;
						умень(destPos);
					всё;
				всё;
				увел(srcPos);
			кон ReplaceEntity;

		нач
			(* we make use of the fact that the "expansion" is actually always a shrinkage and therefore make change in place ! *)
			to := 0; from := 0;

			нцПока (s[from] # "&") и (s[from] # 0X) делай
				увел(from); увел(to);
			кц;

			нцДо
				ch := s[from];
				если ch = "&" то
					ReplaceEntity(s^, from, s^, to);
				иначе
					s[to] := ch;
					увел(from); увел(to);
				всё;
			кцПри ch = 0X;

			возврат s

			(*
			END;
			s[to] := 0X;

			s := scanner.GetString(Scanner.Str_AttributeValue);
			ds1.Clear; ds1.Append(s^);
			start := 0; len := ds1.Length(); expanded := FALSE;
			WHILE start < len DO
				WHILE (start < len) & (ds1.Get(start) # '&') DO
					INC(start)
				END;
				IF ds1.Get(start) = '&' THEN
					expanded := TRUE;
					end := start + 1;
					WHILE (end < len) & (ds1.Get(end) # ';') DO
						INC(end)
					END;
					IF ds1.Get(end) = ';' THEN
						ds2.Clear;
						s := ds1.Extract(0, start);	(* literal before reference *)
						ds2.Append(s^);
						IF ds1.Get(start + 1) = '#' THEN	(* character reference *)
							s := ds1.Extract(start + 2, end - start - 1);
							val := StrToInt(s^);
							msg[0] := ExpandCharacterRef(val);
							msg[1] := 0X;
							ds2.Append(msg);
							start := start + 1;
						ELSE	(* predefined entity or general entity reference *)
							s := ds1.Extract(start + 1, end - start - 1);	(* reference name *)
							es := ExpandPredefinedEntity(s^);
							IF (es # NIL) THEN
								start := start + 1; (* don't expand reference again *)
							ELSE
								es := ExpandEntityRef(s^, XML.GeneralEntity);	(* reference value *)
							END;
							IF es = NIL THEN
								NEW(ds2);
								msg := 'unknown entity "'; ds2.Append(msg);
								es := ds1.Extract(start + 1, end - start - 1); ds2.Append(es^);
								msg := '"'; ds2.Append(msg);
								es := ds2.ToArrOfChar();
								Error(es^); RETURN ds1.ToArrOfChar()
							END;
							ds2.Append(es^);
						END;

						s := ds1.Extract(end + 1, len - end -1);	(* literal after reference *)
						ds2.Append(s^);
						ds1.CopyFrom(ds2, 0, ds2.Length());
						len := ds1.Length()
					ELSE
						Error("';' expected (unclosed reference)"); RETURN ds1.ToArrOfChar()
					END
				END
			END;
			IF expanded THEN
				RETURN ds1.ToArrOfChar();
			ELSE
				RETURN s;
			END;
			*)
		кон ExpandCharacterRefs;

		проц ParseEntityValue(): String;
		перем s, es: String; start, end, len: размерМЗ; val: цел32; msg: массив 17 из симв8;
		нач
			ds1.Clear; ds1.Append(s^);
			start := 0; len := ds1.Length();
			нцПока start < len делай
				нцПока (start < len) и ((ds1.Get(start) # '&') или (ds1.Get(start + 1) # '#')) и (ds1.Get(start) # '%') делай
					увел(start)
				кц;
				если ((ds1.Get(start) = '&') и (ds1.Get(start + 1) = '#')) или (ds1.Get(start) = '%') то
					end := start + 1;
					нцПока (end < len) и (ds1.Get(end) # ';') делай
						увел(end)
					кц;
					если ds1.Get(end) = ';' то
						ds2.Clear;
						s := ds1.Extract(0, start);	(* literal before reference *)
						ds2.Append(s^);
						если (ds1.Get(start) = '&') и (ds1.Get(start + 1) = '#') то	(* character reference *)
							s := ds1.Extract(start + 2, end - start - 1);
							val := StrToInt(s^);
							msg[0] := ExpandCharacterRef(val);
							msg[1] := 0X;
							ds2.Append(msg);
							start := start + 1;
						иначе	(* predefined entity or parameter entity reference *)
							s := ds1.Extract(start + 1, end - start - 1);	(* reference name *)
							es := ExpandPredefinedEntity(s^);
							если (es # НУЛЬ) то
								start := start + 1; (* don't expand reference again *)
							иначе
								es := ExpandEntityRef(s^, XML.ParameterEntity);	(* reference value *)
							всё;
							если es = НУЛЬ то
								нов(ds2);
								msg := 'unknown entity "'; ds2.Append(msg);
								es := ds1.Extract(start + 1, end - start - 1); ds2.Append(es^);
								msg := '"'; ds2.Append(msg);
								es := ds2.ToArrOfChar();
								Error(es^); возврат ds1.ToArrOfChar()
							всё;
							ds2.Append(es^);
						всё;
						s := ds1.Extract(end + 1, len - end -1);	(* literal after reference *)
						ds2.Append(s^);
						ds1.CopyFrom(ds2, 0, ds2.Length());
						len := ds1.Length()
					иначе
						Error("';' expected (unclosed reference)"); возврат ds1.ToArrOfChar()
					всё
				всё
			кц;
			возврат ds1.ToArrOfChar()
		кон ParseEntityValue;

		проц ParseAttributeValue(): String;
		перем
			s, es: String; start, end, len, val: цел32; msg: массив 17 из симв8;
			expanded : булево;
		нач
			s := scanner.GetString(Scanner.Str_AttributeValue);
			возврат ExpandCharacterRefs(s);
		кон ParseAttributeValue;

		проц ParseCharRef(): XML.CharReference;
		перем cRef: XML.CharReference; code: цел32; res: целМЗ; s: String;
		нач
			s := scanner.GetString(Scanner.CharRef);
			если s[0] = 'x' то	(* hexadecimal *)
				Строки8.УдалиПодстроку˛неПроверяя0(s^, 0, 1);
				Строки8.ПрочтиЦел32_16_ричноИзСтроки(s^, code, res);
			иначе	(* decimal *)
				Строки8.ПрочтиЦел32_изСтроки(s^, code);
			всё;
			нов(cRef); cRef.SetPos(scanner.GetPos());
			cRef.SetCode(code);
			возврат cRef;
		кон ParseCharRef;

		проц ParseEntityRef(): XML.EntityRef;
		перем ext: XML.ExternalEntityRef; int: XML.InternalEntityRef; s1, s2: String; ent: XML.EntityDecl;
		нач
			s1 := scanner.GetString(Scanner.Str_EntityRef);
			ent := dtd.GetEntityDecl(s1^, XML.GeneralEntity);
			если ent # НУЛЬ то
				s2 := ent.GetValue();
				если s2 # НУЛЬ то
					нов(int); int.SetPos(scanner.GetPos());
					int.SetNameAsString(s1);
					возврат int
				иначе
					нов(ext); ext.SetPos(scanner.GetPos());
					ext.SetNameAsString(s1);
					возврат ext
				всё
			иначе
				возврат НУЛЬ
			всё
		кон ParseEntityRef;

	кон Parser;

перем
	(* read-only *)
	predefinedEntities : массив 5 из запись name : массив 5 из симв8; expanded : Строки8.уСтрока; кон;

проц IsPubidLiteral(конст str: массив из симв8): булево;
перем i, len: размерМЗ; ch: симв8;
нач
	i := 0; len := длинаМассива(str); ch := str[0];
	нцДо
		ch := str[i]; увел(i)
	кцПри ((ch # 20X) и (ch # 0DX) и (ch # 0AX) и ((ch < 'a') или ('z' < ch)) и ((ch < 'A') и ('Z' < ch))
			и ((ch < '0') и ('9' < ch)) и (ch # '(') и (ch # ')') и (ch # '+') и (ch # ',') и (ch # '.')
			и (ch # '/') и (ch # ':') и (ch # '=') и (ch # '?') и (ch # ';') и (ch # '!') и (ch # '*') и (ch # '#')
			и (ch # '@') и (ch # '$') и (ch # '_') и (ch # '%')) или (i >= len);
	возврат i = len
кон IsPubidLiteral;

проц StrToInt(перем str: массив из симв8): цел32;
нач
	если str[0] = 'x' то	(* str in hexadecimal form *)
		str[0] := ' ';
		возврат DynamicStrings.HexStrToInt(str)
	иначе
		возврат DynamicStrings.StrToInt(str)
	всё
кон StrToInt;

проц DefaultReportError(pos, line, col: Потоки.ТипМестоВПотоке; конст msg: массив из симв8);
нач
	ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСимв8(симв8ИзКода(9H)); ЛогЯдра.пСимв8(симв8ИзКода(9H)); ЛогЯдра.пСтроку8("pos "); ЛогЯдра.пЦел64(pos, 6);
	ЛогЯдра.пСтроку8(", line "); ЛогЯдра.пЦел64(line, 0); ЛогЯдра.пСтроку8(", column "); ЛогЯдра.пЦел64(col, 0);
	ЛогЯдра.пСтроку8("    "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
кон DefaultReportError;

проц ExpandPredefinedEntity(конст name : массив из симв8) : Строки8.уСтрока;
перем i : цел32;
нач
	нцДля i := 0 до длинаМассива(predefinedEntities)-1 делай
		если (name = predefinedEntities[i].name) то
			возврат predefinedEntities[i].expanded;
		всё;
	кц;
	возврат НУЛЬ;
кон ExpandPredefinedEntity;

проц Init;
нач
	predefinedEntities[0].name := "lt"; predefinedEntities[0].expanded := Строки8.ЯвиУСтроку("<");
	predefinedEntities[1].name := "gt"; predefinedEntities[1].expanded := Строки8.ЯвиУСтроку(">");
	predefinedEntities[2].name := "amp"; predefinedEntities[2].expanded := Строки8.ЯвиУСтроку("&");
	predefinedEntities[3].name := "apos"; predefinedEntities[3].expanded := Строки8.ЯвиУСтроку("'");
	predefinedEntities[4].name := "quot"; predefinedEntities[4].expanded := Строки8.ЯвиУСтроку('"');
кон Init;

нач
	Init;
кон XMLParser.

# Постройка образа ЯОС, с которого можно загзурить компьютер IBM PC (проверено на ASUS G771JW)
# https://forum.oberoncore.ru/viewtopic.php?f=22&t=63867
# Порядок выполнения:
#   - выполнить 4 команды - появится образ ЯОС:BIOS32/JA-O-S-USB.img
#   - запуск в QEMU (Win): qemu-20200221\qemu-system-x86_64 -m 1025 -machine pc -serial stdio -net nic,model=pcnet -net user -drive if=none,id=usbstick,format=raw,file=JA-O-S-USB.img -usb -device usb-ehci,id=ehci -device usb-storage,bus=ehci.0,drive=usbstick
#   - запуск в QEMU (Linux): ЯОС:BIOS32/a2-usb.bash
#   - записать образ на флешку, например, с помощью Win32 disk imager
#   - выключить питание компьютера
#   - загрузиться с флешки (для ASUS G771JW - отключить внешний монитор и перевключить питание - del, boot overrides, выбрать флешку)


# 1. Создаём директорию
FSTools.CreateDirectory ЯОС:NewBios32USB ~

# 2. Стираем старое и собираем
System.DoCommands
	FSTools.DeleteFiles -i ЯОС:NewBios32USB/* ~
	СборщикВыпускаЯОС.Скомпилируй --скомпилируй --лис --путьКОбъектнымФайлам="ЯОС:NewBios32USB/" --порождаяАрхивы --генерируяФайлXMLДляУстановщика Bios32 ~~

# 3. удаление лишнего из архива (не всегда работает)

ZipTool.Delete --ignore ЯОС:NewBios32USB/Applications.zip InstallerPackages.XML ~ 


# 4. Подготовка образа диска

System.DoCommands 

System.Timer start ~

ZipTool.Add --nopath ЯОС:NewBios32USB/Applications.zip ЯОС:NewBios32USB/InstallerPackages.XML ~
FSTools.DeleteFiles -i ЯОС:NewBios32USB/JA-O-S-USB.img ~

PCAAMD64.Assemble BootManager.Asm ~
BootManager.Split BootManager.Bin ~

PCAAMD64.Assemble OBLUnreal.Asm ~


Linker.Link --path=ЯОС:NewBios32USB/ 
	--displacement=100000H 
	--fileName=ЯОС:NewBios32USB/USB.Bin
	Kernel Traps ATADisks
	UsbHubDriver UsbEhci UsbEhciPCI UsbStorageBoot
	DiskVolumes 
	DiskFS Loader BootConsole ~


VirtualDisks.Create ЯОС:NewBios32USB/JA-O-S-USB.img 480000 512 ~
VirtualDisks.Install -b=512 VDISK0 ЯОС:NewBios32USB/JA-O-S-USB.img ~


Partitions.WriteMBR VDISK0#0 OBEMBR.BIN ~
Partitions.InstallBootManager VDISK0#0 BootManagerMBR.Bin BootManagerTail.Bin ~
Partitions.Create VDISK0#1 76 180 ~

PartitionsLib.SetBootLoaderFile OBLUnreal.Bin ~
Partitions.Format VDISK0#1 AosFS -1 ЯОС:NewBios32USB/USB.Bin ~ (* -1 makes sure that actual boot file size is taken as offset for AosFS *)
FSTools.Mount TEMP AosFS VDISK0#1 ~


ZipTool.ExtractAll --prefix=TEMP: --sourcePath=ЯОС:NewBios32USB/ --overwrite --silent
Kernel.zip System.zip Drivers.zip ApplicationsMini.zip Applications.zip Compiler.zip 
GuiApplicationsMini.zip GuiApplications.zip Build.zip EFI.zip
TrueTypeFonts.zip BuildSrc.zip KernelSrc.zip SystemSrc.zip DriversSrc.zip ApplicationsMiniSrc.zip ApplicationsSrc.zip GuiApplicationsMiniSrc.zip GuiApplicationsSrc.zip 
ScreenFonts.zip TrueTypeFonts.zip ~


FSTools.CopyFiles ЯОС:NewBios32USB/*.zip => TEMP:*.zip ~
Linker.Link --path=ЯОС:NewBios32CD/ --displacement=100000H --fileName=ЯОС:NewBios32CD/IDE.Bin Kernel Traps ATADisks DiskVolumes DiskFS Loader BootConsole ~

FSTools.CopyFiles ЯОС:NewBios32CD/IDE.Bin => TEMP:IDE.Bin ~
FSTools.CopyFiles OBLUnreal.Bin => TEMP:OBLUnreal.Bin ~
FSTools.DeleteFiles TEMP:OBL.Bin ~

FSTools.DeleteFiles -i TEMP:Configuration.XML ~
FSTools.CopyFiles BIOS.Configuration.XML => TEMP:Configuration.XML ~

FSTools.Unsafe ~
FSTools.DeleteFiles -i TEMP:*.перев.XML ~
FSTools.CopyFiles *.перев.XML => TEMP:/*.XML ~
FSTools.Safe ~

FSTools.Watch TEMP ~
FSTools.Unmount TEMP ~

Partitions.SetConfig VDISK0#1
TraceMode="4" TracePort="1" TraceBPS="115200"
BootVol1="AOS AosFS USB0#1"
AosFS="DiskVolumes.New DiskFS.NewFS"
CacheSize="1000"
ExtMemSize="512"
MaxProcs="-1"
ATADetect="legacy"
Init="114"
Boot="DisplayLinear.Install"
Boot1="Keyboard.Install;MousePS2.Install"
Boot2="DriverDatabase.Enable;UsbHubDriver.Install;UsbEhciPCI.Install;UsbUhci.Install;UsbOhci.Install"
Boot3="WindowManager.Install"
Boot4="ЗагрузиПереводыЭлементовКода.ИзВсехФайлов"
Boot5="Autostart.Run"
Boot6="WMInstaller.Open"
~
VirtualDisks.Uninstall VDISK0 ~

System.Show время подготовки образа USB: ~ System.Timer elapsed ~

FSTools.CloseFiles ЯОС:NewBios32USB/JA-O-S-USB.img ~
FSTools.DeleteFiles -i ЯОС:BIOS32/JA-O-S-USB.img ~
FSTools.CopyFiles ЯОС:NewBios32USB/JA-O-S-USB.img => ЯОС:BIOS32/JA-O-S-USB.img ~

~

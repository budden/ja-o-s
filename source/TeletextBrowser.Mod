модуль TeletextBrowser;	(** AUTHOR "oljeger@student.ethz.ch"; PURPOSE "Browsing Interface for Aos Teletext" *)

использует
	TeletextDecoder, ЛогЯдра;

конст
	VbiUndefined = TeletextDecoder.VbiUndefined;

тип
	(** Container for a teletext page in markup form. Concrete implementation will contain
		  Texts.Text or HTML content *)
	TeletextPage* = окласс
	кон TeletextPage;

	(** Procedure that creates markup content and returns it *)
	LoadProc* = проц{делегат}() : TeletextPage;

	(** Generic teletext browser *)
	TeletextBrowser* = окласс
	перем
		page-, subPage: размерМЗ;
		suite-: TeletextDecoder.TeletextSuite;
		pgSet: TeletextDecoder.TeletextPageSet;
		pgData*: TeletextDecoder.TeletextPage;
		content*: TeletextPage;
		channel-: массив 33 из симв8;
		refreshInterval*: цел32;
		rotating*: булево;
		transparent*: булево;
		loadProc*: LoadProc;

		проц &Init*(suite: TeletextDecoder.TeletextSuite);
		нач
			если suite = НУЛЬ то
				ЛогЯдра.пСтроку8("{TeletextBrowser.Init} Parameter 'suite' = NIL"); ЛогЯдра.пВК_ПС;
				возврат
			всё;
			сам.suite := suite;
			копируйСтрокуДо0(suite.channel.name, channel);
			page := 0;
			pgSet := suite.pages[page];
			pgData := pgSet.data;
			если pgData # НУЛЬ то
				subPage := pgData.subPageNo
			всё
		кон Init;

		(** Does the current page have any subpages? *)
		проц HasSubpages*() : булево;
		нач
			если pgData = НУЛЬ то
				возврат ложь
			всё;
			возврат pgData # pgData.nextSub
		кон HasSubpages;

		(** Select another teletext suite for browsing *)
		проц SetSuite*(suite: TeletextDecoder.TeletextSuite);
		нач
			Init(suite)
		кон SetSuite;

		(** Select another teletext suite for browsing according to the given TV frequency *)
		проц SetSuiteFromFreq*(freq: цел32);
		нач
			Init(TeletextDecoder.SelectTeletextSuite(freq))
		кон SetSuiteFromFreq;

		(** Reload the current teletext page *)
		проц ReloadPage*() : TeletextPage;
		перем
			sub: размерМЗ;
			tmp: TeletextPage;
		нач
			sub := subPage;
			нач {единолично}
				pgData := pgSet.data
			кон;
			нцПока (pgData # НУЛЬ) и (sub # pgData.subPageNo) делай
				tmp := GetNextSubpage()
			кц;
			возврат loadProc()
		кон ReloadPage;

		(** Get the next valid teletext page *)
		проц GetNextPage*() : TeletextPage;
		нач
			page := pgSet.next;
			если page = VbiUndefined то
				возврат НУЛЬ
			всё;
			pgSet := suite.pages[page];
			нач {единолично}
				pgData := pgSet.data
			кон;
			если pgData # НУЛЬ то
				subPage := pgData.subPageNo
			всё;
			возврат loadProc()
		кон GetNextPage;

		(** Get the previous valid teletext page *)
		проц GetPreviousPage*() : TeletextPage;
		нач
			page := pgSet.prev;
			если page = VbiUndefined то
				возврат НУЛЬ
			всё;
			pgSet := suite.pages[page];
			нач {единолично}
				pgData := pgSet.data
			кон;
			если pgData # НУЛЬ то
				subPage := pgData.subPageNo
			всё;
			возврат loadProc()
		кон GetPreviousPage;

		(** Get the next subpage *)
		проц GetNextSubpage*() : TeletextPage;
		нач
			если pgData = НУЛЬ то
				возврат НУЛЬ
			всё;
			нач {единолично}
				pgData := pgData.nextSub
			кон;
			subPage := pgData.subPageNo;
			возврат loadProc()
		кон GetNextSubpage;

		(** Get the previous subpage *)
		проц GetPreviousSubpage*() : TeletextPage;
		нач
			если pgData = НУЛЬ то
				возврат НУЛЬ
			всё;
			нач {единолично}
				pgData := pgData.prevSub
			кон;
			subPage := pgData.subPageNo;
			возврат loadProc()
		кон GetPreviousSubpage;

		(** Get a teletext page by number. page is in range 100-899 *)
		проц GetPage*(page: размерМЗ) : TeletextPage;
		нач
			page := page - 100;
			сам.page := page;
			если (page < 0) или (page >= 800) или (suite = НУЛЬ) или (suite.pages[page].data = НУЛЬ) то
				нач {единолично}
					pgData := НУЛЬ
				кон
			иначе
				pgSet := suite.pages[page];
				нач {единолично}
					pgData := pgSet.data
				кон;
				subPage := pgData.subPageNo
			всё;
			возврат loadProc()
		кон GetPage;

	кон TeletextBrowser;

кон TeletextBrowser.


System.Free TeletextBrowser ~

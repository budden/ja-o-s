(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль ISO9660Files;	(** AUTHOR "?/be"; PURPOSE "ISO 9660 File System (ported from Native Oberon)"; *)

	использует НИЗКОУР, Modules, Files, ЛогЯдра, Строки8;

	конст
		debug = ложь; nameDebug = ложь;

		SS = 2048;
		MaxBufs = 4;

		Directory = 1;

		eFileDoesNotExist = 8903;
		eCannotOpenSubDir = 8916;
		eInvalidFirstCluster = 8917;
		eNameIsWild = 8927;
		eInvalidFileName = 8941;
		eInvalidVolume = 9000;

	тип
		Filename = массив 256 из симв8;

		VolDesc = укль на запись
			root, rootDirSize: цел32 (* sector number of root directory and root directory size *)
		кон;

		Buffer = укль на запись (Files.Hint)
			pos, lim: цел32;
			next: Buffer;
			data: укль на массив из симв8
		кон;

		FileSystem = окласс(Files.FileSystem)
			перем
				pri, sup, cur: VolDesc;
				jolietLevel: цел32;

			(** Open an existing file. The same file descriptor is returned if a file is opened multiple times.  End users use Files.Old instead. *)
			проц {перекрыта}Old0*(конст name: массив из симв8): Files.File;
			перем f: File; namebuf: Filename; dircl, dirpos, time, date, filecl, len: цел32; attr: мнвоНаБитахМЗ; res: цел16;
			нач {единолично}
				res := 0; f := НУЛЬ;
				Check(name, namebuf, res);
				если res = 0 то
					LocateFile(namebuf, сам, dircl, dirpos, time, date, filecl, len, attr, res);
					если debug то LogString("Old0; filecl: "); LogInt(filecl); LogLn всё;
					если res = 0 то
						если Directory в attr то res := eCannotOpenSubDir
						аесли filecl < 16 то res := eInvalidFirstCluster
						иначе f := OpenFile(namebuf, сам, dircl, dirpos, time, date, filecl, len, attr)
						всё
					всё
				всё;
				возврат f
			кон Old0;

			(** Enumerate canonical file names. mask may contain * wildcards.  For internal use only.  End users use Enumerator instead. *)
			проц {перекрыта}Enumerate0*(конст mask: массив из симв8; flags: мнвоНаБитахМЗ; enum: Files.Enumerator);
			перем
				fname, name, mmask, pname, fullname: Filename;
				f: Files.File; R: Files.Rider;
				attr: мнвоНаБитахМЗ; bAddToEnum: булево;
				pos, time, date, len, cl: цел32; res: цел16;
			нач {единолично}
				Check(mask, name, res);
				если (res = 0) или (res = eNameIsWild) то
					SeparateName(name, name, mmask); если (mmask = "") то копируйСтрокуДо0("*", mmask) всё;
					Files.JoinName(prefix, name, pname);
					если nameDebug то LogString("Enumerate; dir name: "); LogString(pname); LogLn всё;
					f := OldDir(сам, name);
					если f # НУЛЬ то
						f.Set(R, 0); pos := 0;
						нц
							MatchFile(R, mmask, fname, pos, cl, time, date, len, attr, res);
							если res # 0 то прервиЦикл всё;
							копируйСтрокуДо0(pname, fullname);
							если name[0] # 0X то Files.AppendStr("/", fullname) всё;
							Files.AppendStr(fname, fullname);

							bAddToEnum := истина;
							если Directory в attr то
								если (fname = ".") или (fname = "..") то
									bAddToEnum := ложь;
								всё;
								flags := { Files.Directory };
								len := 0;
							иначе
								flags := {};
							всё;

							если bAddToEnum то
								enum.PutEntry(fullname, flags, time, date, len)
							всё;
						кц;
					иначе res := eFileDoesNotExist
					всё
				всё
			кон Enumerate0;

			(** Return the unique non-zero key of the named file, if it exists. *)
			проц {перекрыта}FileKey*(конст name: массив из симв8): цел32;
			перем res: цел16; namebuf: Filename; t, key, filecl: цел32; attr: мнвоНаБитахМЗ;
			нач {единолично}
				если nameDebug то LogString("OFSFATFiles.FileKey: "); LogString(name); LogLn всё;
				key := 0;
				Check(name, namebuf, res);
				если res = 0 то
					LocateFile(namebuf, сам, t, t, t, t, filecl, t, attr, res);
					если res = 0 то key := filecl всё
				всё;
				возврат key
			кон FileKey;

			(** Finalize the file system. *)
			проц {перекрыта}Finalize*;
			нач {единолично}
				vol.Finalize;
				Finalize^
			кон Finalize;
		кон FileSystem;

		File = окласс (Files.File)
		перем
			len,
			time, date,
			filecl: цел32;	(* first cluster *)
			attr: мнвоНаБитахМЗ ;	(* ISO file attributes *)
			(* directory info *)
			name: Filename;
			dircl,	(* start cluster of dir. that contains entry for file *)
			dirpos: цел32;		(* position in cluster of dir. in which entry lies *)
			nofbufs: цел16;
			firstbuf: Buffer;

			проц {перекрыта}Set*(перем r: Files.Rider; pos: Files.Position);
			нач {единолично}
				r.eof := ложь; r.res := 0; r.file := сам; r.fs := fs;
				если (pos < 0) то r.apos := 0
				аесли (pos < len) то r.apos := pos
				иначе r.apos := len
				всё;
				r.hint := firstbuf
			кон Set;

			проц {перекрыта}Pos*(перем r: Files.Rider): Files.Position;
			нач {единолично}
				возврат r.apos
			кон Pos;

			проц FindBuf(pos: цел32; hint: Buffer): Buffer;
			перем buf: Buffer;
			нач
				buf := hint;
				нц
					если (pos >= buf.pos) и (pos < buf.pos+buf.lim) то прервиЦикл всё;
					buf := buf.next;
					если buf = hint то buf := НУЛЬ;  прервиЦикл всё
				кц;
				возврат buf
			кон FindBuf;

			проц ReadBuf(buf: Buffer; pos: цел32);
			нач
				утв(pos <= len, 100);
				buf.pos := pos - pos остОтДеленияНа fs.vol.blockSize;
				pos := pos DIV fs.vol.blockSize;
				если pos = len DIV fs.vol.blockSize то buf.lim := len остОтДеленияНа fs.vol.blockSize
				иначе buf.lim := fs.vol.blockSize
				всё;
				если debug то LogString("ReadBuf; block: "); LogInt(filecl+pos); LogLn всё;
				fs.vol.GetBlock(filecl+pos, buf.data^)
			кон ReadBuf;

			проц GetBuf(pos: цел32; hint: Buffer): Buffer;
			перем buf: Buffer;
			нач
				buf := FindBuf(pos, hint);
				если buf = НУЛЬ то
					если nofbufs < MaxBufs то (*allocate new buffer*)
						нов(buf);  нов(buf.data, fs.vol.blockSize);
						buf.next := firstbuf.next;  firstbuf.next := buf;
						увел(nofbufs)
					иначе (*reuse one of the buffers; round robin *)
						buf := firstbuf; firstbuf := buf.next;
					всё;
					ReadBuf(buf, pos);
				всё;
				возврат buf
			кон GetBuf;

			проц {перекрыта}Read*(перем r: Files.Rider; перем x: симв8);
			перем buf: Buffer;
			нач {единолично}
				r.res := 0;
				если (r.apos < len) то
					buf := GetBuf(цел32(r.apos), r.hint(Buffer));
					x := buf.data[цел32(r.apos)-buf.pos];
					увел(r.apos)
				иначе
					x := 0X; r.eof := истина
				всё
			кон Read;

			проц {перекрыта}ReadBytes*(перем r: Files.Rider; перем x: массив из симв8; ofs, len: размерМЗ);
			перем m: размерМЗ; pos: цел32; src: адресВПамяти; buf: Buffer;
			нач {единолично}
				если длинаМассива(x) < len то НИЗКОУР.СТОП(19) всё;
				если len <= 0 то возврат всё;
				buf := r.hint(Buffer);
				m := сам.len - цел32(r.apos);
				если len <= m то r.res := 0 иначе r.eof := истина; r.res := len-m; len := m всё;
				нцПока len > 0 делай
					buf := GetBuf(цел32(r.apos), buf);
					pos := цел32(r.apos) - buf.pos;
					src := адресОт(buf.data[pos]);  m := buf.lim-pos;
					если m > len то m := len всё;
					НИЗКОУР.копируйПамять(src, адресОт(x[ofs]), m);
					умень(len, m); увел(ofs, m); увел(r.apos, m);
				кц;
				r.hint := buf
			кон ReadBytes;

			проц {перекрыта}Length*(): Files.Size;
			нач возврат len
			кон Length;

			проц {перекрыта}GetDate*(перем t, d: цел32);
			нач t := time; d := date
			кон GetDate;

			проц {перекрыта}GetName*(перем name: массив из симв8);
			нач копируйСтрокуДо0(сам.name, name)
			кон GetName;

			проц {перекрыта}Update*;
			кон Update; (* nothing *)
		кон File;

	перем	(* svr *)
		ExtractNameProc: проц(перем dir, name: массив из симв8);

	(* debug procedures *)

	проц LogString(s: массив из симв8);
	нач
		ЛогЯдра.пСтроку8(s)
	кон LogString;

	проц LogInt(i: цел32);
	нач
		ЛогЯдра.пЦел64(i, 0)
	кон LogInt;

	проц LogLn;
	нач
		ЛогЯдра.пВК_ПС
	кон LogLn;

	(* help procedures *)

	(* Get733 - 32 bit, both byte orders *)
	проц Get733(перем s: массив из симв8; first: цел32; перем d: цел32);
	нач
		d := устарПреобразуйКБолееШирокомуЦел(кодСимв8(s[first]));
		d := d + устарПреобразуйКБолееШирокомуЦел(кодСимв8(s[first+1]))*100H;
		d := d + устарПреобразуйКБолееШирокомуЦел(кодСимв8(s[first+2]))*10000H;
		d := d + устарПреобразуйКБолееШирокомуЦел(кодСимв8(s[first+3]))*1000000H
	кон Get733;

	(* Check - check filename.  Return correct name, or empty name if incorrect. *)
	проц Check(s: массив из симв8;  перем name: Filename; перем res: цел16);
	перем i, j: цел32;  ch: симв8;
	нач
		если nameDebug то LogString("Check: "); LogString(s) всё;
		res := 0; i := 0;
		если (s[0] = "/") или (s[0] = "\") то j := 1 иначе j := 0 всё;		(* remove leading / or \ *)
		нц
			ch := s[j];
			если ch = 0X то прервиЦикл всё;
			если ch = "\" то ch := "/" всё;
			если (ch < " ") или (ch >= 07FX) то res := eInvalidFileName; i := 0; прервиЦикл всё;
			если (ch = "?") или (ch = "*") то res := eNameIsWild всё;
			name[i] := ch;
			увел(i); увел(j)
		кц;
		name[i] := 0X;
		если nameDebug то LogString(" => "); LogString(name); LogLn всё;
	кон Check;

	проц GetVolumeDescriptors(fs: FileSystem; res: цел16);
	перем b: массив SS из симв8; i: цел32; vol: Files.Volume;
	нач
		vol := fs.vol;
		i := 16; fs.pri := НУЛЬ; fs.sup := НУЛЬ; fs.cur := НУЛЬ; fs.jolietLevel := 0;
		нцДо
			vol.GetBlock(i, b);	(* read boot sector *)
			просей b[0] из
				1X: (* Primary volume descriptor *)
					утв(fs.pri = НУЛЬ, 102);	(* exactly one primary volume desc *)
					нов(fs.pri);
					Get733(b, 158, fs.pri.root);	(* location of root directory *)
					Get733(b, 166, fs.pri.rootDirSize)	(* size of root directory in bytes *)
			|	2X: (* Supplementary volume descriptor  *)
					утв(fs.sup = НУЛЬ, 103);	(* 0 or 1 supplementary volume descriptor *)
					утв((b[88] = 25X) и (b[89] = 2FX) и ((b[90] = 40X) или (b[90] = 43X) или (b[90] = 45X)), 104);
					если b[90] = 40X то fs.jolietLevel := 1
					аесли b[90] = 43X то fs.jolietLevel := 2
					аесли b[90] = 45X то fs.jolietLevel := 3
					всё;
					нов(fs.sup);
					Get733(b, 158, fs.sup.root);	(* location of root directory *)
					Get733(b, 166, fs.sup.rootDirSize)	(* size of root directory in bytes *)
			иначе утв((b[0] = 0X) или (b[0] = 2X) или (b[0] = 0FFX), 100)	(* boot or end *)
			всё;
			увел(i)
		кцПри (res # 0) или (b[0] = 0FFX);
		если res = 0 то
			если fs.pri = НУЛЬ то res := eInvalidVolume
			аесли fs.sup # НУЛЬ то fs.cur := fs.sup; ExtractNameProc := ExtractLongName
			иначе fs.cur := fs.pri; ExtractNameProc := ExtractShortName
			всё
		всё;
	кон GetVolumeDescriptors;

	(* GetDir - Get information from a directory entry *)
	проц GetDir(перем dir, fname: массив из симв8; перем time, date, cl, len: цел32; перем attr: мнвоНаБитахМЗ);
	перем t: цел32;
	нач
		ExtractName(dir, fname);
		t (*attr*) := кодСимв8(dir[24]); attr := мнвоНаБитахМЗ(t);
		time := устарПреобразуйКБолееШирокомуЦел(кодСимв8(dir[20]))*64*64 + устарПреобразуйКБолееШирокомуЦел(кодСимв8(dir[21]))*64 + устарПреобразуйКБолееШирокомуЦел(кодСимв8(dir[22]));
		date := устарПреобразуйКБолееШирокомуЦел(кодСимв8(dir[17]))*32*16 + устарПреобразуйКБолееШирокомуЦел(кодСимв8(dir[18]))*16 + устарПреобразуйКБолееШирокомуЦел(кодСимв8(dir[19]));
		Get733(dir, 1, cl);
		Get733(dir, 9, len)
	кон GetDir;

	проц SplitName(str: массив из симв8; перем prefix, name: массив из симв8);
	перем i, j: цел32;
	нач
		если nameDebug то LogString("SplitName: "); LogString(str) всё;
		i := -1; j := -1;
		нцДо увел(i); увел(j); prefix[j] := str[i] кцПри (str[i] = 0X) или (str[i] = "/");
		если str[i] = "/" то
			prefix[j] := 0X; j := -1;
			нцДо увел(i); увел(j); name[j] := str[i] кцПри name[j] = 0X
		иначе name[0] := 0X
		всё;
		если nameDebug то LogString(" => "); LogString(prefix); LogString(", "); LogString(name); LogLn всё
	кон SplitName;

	(* SeparateName - separate str into a prefix and a name. *)
	проц SeparateName(str: массив из симв8; перем prefix: массив из симв8; перем name: Filename);
	перем i, j : цел32;
	нач
	(* Pre: str is result of a Check operation; all "\"s have been changed to "/" *)
		i := 0;  j := -1;
		нцПока str[i] # 0X делай
			если str[i] = "/" то j := i всё;
			увел(i)
		кц;
		если j >= 0 то
			копируйСтрокуДо0(str, prefix); prefix[j] := 0X;
			i := -1;
			нцДо увел(i); увел(j); name[i] := str[j] кцПри name[i] = 0X
		иначе копируйСтрокуДо0(str, name); prefix[0] := 0X
		всё
	кон SeparateName;

	проц ExtractShortName(перем dir, name: массив из симв8);
	перем i, j, len: цел32;
	нач
		len := кодСимв8(dir[31]);
		i := 0;  j := 32;
		нцПока (i < len) и (dir[j] # ";") делай name[i] := dir[j];  увел(i); увел(j) кц;
		name[i] := 0X;
	кон ExtractShortName;

	проц ExtractLongName(перем dir, name: массив из симв8);
	перем i, j, end: цел32;
	нач
		end := 33+кодСимв8(dir[31]);
		i := 0;  j := 33;
		нцПока (j < end) и (dir[j] # ";") делай name[i] := dir[j];  увел(i); увел(j, 2) кц;
		name[i] := 0X;
	кон ExtractLongName;

	проц ExtractName(перем dir, name: массив из симв8);
	перем len: цел32;
	нач
		len := кодСимв8(dir[31]);
		если len = 1 то
			если dir[33] = 0X то копируйСтрокуДо0(".", name)
			аесли dir[33] = 1X то копируйСтрокуДо0("..", name)
			всё
		иначе ExtractNameProc(dir, name)
		всё
	кон ExtractName;

	проц MatchFile(перем R: Files.Rider; name: массив из симв8; перем fname: массив из симв8;
		перем pos, cl, time, date, len: цел32; перем attr: мнвоНаБитахМЗ; перем res: цел16);
	перем
		found: булево; f: File; fs: FileSystem; buf: массив 256 из симв8; entryLen, padding: цел32;
	нач
		f := R.file(File); fs := R.fs(FileSystem);
		found := ложь;
		нц
			pos := цел32(R.file.Pos(R));
			если debug то LogString("MatchFile; pos: "); LogInt(pos); LogLn всё;
			R.file.Read(R, buf[0]); entryLen := кодСимв8(buf[0]);
			если debug то LogString("MatchFile; entryLen: "); LogInt(entryLen); LogLn всё;
			если R.eof то
				если debug то LogString("MatchFile; eof"); LogLn всё;
				прервиЦикл
			всё;
			если entryLen > 0 то
				R.file.ReadBytes(R, buf, 0, entryLen-1);
				GetDir(buf, fname, time, date, cl, len, attr);
				found := Строки8.ПодходитЛиПодМаскуИмениФайла¿(name, fname);
			иначе (* Directory entries may not cross sectors. Skip padding bytes *)
				padding := SS - (цел32(R.file.Pos(R)) остОтДеленияНа SS);
				если (padding > 0) и (padding <= 256) то
					если debug то LogString("MatchFile; skip padding: "); LogInt(padding); LogLn; всё;
					R.file.ReadBytes(R, buf, 0, padding); (* Skip padding *)
				иначе
					если debug то LogString("MatchFile; Confusing directory structure... givin' up"); всё;
					прервиЦикл;
				всё;
			всё;
			если found то прервиЦикл всё;
		кц;
		если found то res := 0 иначе res := eInvalidFileName всё;
	кон MatchFile;

	проц FindFile(fs: FileSystem; name: массив из симв8; dircl, dirlen: цел32;
		перем dirpos, time, date, filecl, len: цел32; перем attr: мнвоНаБитахМЗ; перем res: цел16);
	перем f: File; R: Files.Rider; fname: Filename;
	нач
		утв(name # "", 100);
		f := OpenFile("", fs, -1, -1, -1, -1, dircl, dirlen, {});
		f.Set(R, 0);
		MatchFile(R, name, fname, dirpos, filecl, time, date, len, attr, res);
	кон FindFile;

	проц LocateFile(name: массив из симв8; fs: FileSystem;
		перем dircl, dirpos, time, date, filecl, len: цел32; перем attr: мнвоНаБитахМЗ; перем res: цел16);
	перем cur: Filename; dirlen: цел32;
	нач
		res := 0;
		dircl := fs.cur.root; dirlen := fs.cur.rootDirSize;	(* start in root directory *)
		если name[0] = 0X то (* root dir *)
			filecl := dircl; attr := {Directory};
			len := dirlen; dirpos := -1;
		иначе
			нц
				SplitName(name, cur, name);
				FindFile(fs, cur, dircl, dirlen, dirpos, time, date, filecl, len, attr, res);
				если (res = 0) и (name # "") и ~(Directory в attr) то res := eInvalidFileName всё;
				если (res # 0) или (name = "") то прервиЦикл всё;
				dircl := filecl; dirlen := len
			кц
		всё
	кон LocateFile;

	проц OpenFile(name: массив из симв8; fs: FileSystem; dircl, dirpos, time, date, filecl, len: цел32; attr: мнвоНаБитахМЗ): File;
	перем f: File; buf: Buffer;
	нач
		нов(f); копируйСтрокуДо0(name, f.name); f.fs := fs; f.key := filecl;
		f.dircl := dircl; f.dirpos := dirpos; f.time := time; f.date := date;
		f.filecl := filecl; f.len := len; f.attr := attr;
		нов(buf); buf.next := buf;
		нов(buf.data, fs.vol.blockSize);
		если f.len = 0 то buf.pos := 0; buf.lim := 0
		иначе f.ReadBuf(buf, 0) (* file is not empty *)
		всё;
		f.firstbuf := buf;  f.nofbufs := 1;
		возврат f
	кон OpenFile;

	проц OldDir(fs: Files.FileSystem; name: массив из симв8): Files.File;
	перем f: File; dircl, dirpos, time, date, filecl, len: цел32; attr: мнвоНаБитахМЗ; res: цел16;
	нач
		res := 0; f := НУЛЬ;
		LocateFile(name, fs(FileSystem), dircl, dirpos, time, date, filecl, len, attr, res);
		если res = 0 то
			если ~(Directory в attr) то res := eCannotOpenSubDir
			аесли filecl < 16 то res := eInvalidFirstCluster
			иначе f := OpenFile(name, fs(FileSystem), dircl, dirpos, time, date, filecl, len, attr)
			всё
		всё;
		возврат f
	кон OldDir;

(** Generate a new file system object.  OFS.NewVol has volume parameter, OFS.Par has mount prefix. *)
проц NewFS*(context : Files.Parameters);
перем fs: FileSystem; res: цел16;
нач
	если Files.This(context.prefix) = НУЛЬ то
		нов(fs);  fs.vol := context.vol;
		GetVolumeDescriptors(fs, res);
		если res = 0 то
			если debug то
				LogString("  primary root: "); LogInt(fs.pri.root); LogLn;
				LogString("  primary root dir size: "); LogInt(fs.pri.rootDirSize); LogLn
			всё;
			fs.desc[0] := 0X;  Files.AppendStr(context.vol.name, fs.desc);  Files.AppendStr(" / ISO9660FS", fs.desc);
			Files.Add(fs, context.prefix)
		иначе context.error.пСтроку8("No ISO9660 file system found on "); context.error.пСтроку8(context.vol.name); context.error.пВК_ПС;
		всё
	иначе context.error.пСтроку8(context.prefix); context.error.пСтроку8(" already in use"); context.error.пВК_ПС;
	всё;
кон NewFS;

(* Clean up when module freed. *)
проц Cleanup;
перем ft: Files.FileSystemTable; i: размерМЗ;
нач
	если Modules.shutdown = Modules.None то
		Files.GetList(ft);
		если ft # НУЛЬ то
			нцДля i := 0 до длинаМассива(ft^)-1 делай
				если ft[i] суть FileSystem то Files.Remove(ft[i]) всё
			кц
		всё
	всё
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон ISO9660Files.

ISO9660Volumes.Mod

Partitions.Show

OFSTools.Mount CD IsoFS  IDE2 ~
OFSTools.Unmount CD0 ~

System.Free ISO9660Files  ISO9660Volumes ~

System.Directory CD:*.* ~

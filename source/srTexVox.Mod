модуль srTexVox;
использует srBase, Raster, Graphics := WMGraphics, Random, Math, srMath;

тип SREAL=srBase.SREAL;
тип Ray = srBase.Ray;
тип Voxel = srBase.Voxel;
тип Name = srBase.Name;

тип TexVox* = окласс(Voxel);
перем
	img*: Raster.Image;
	fmt: Raster.Format;
	copy : Raster.Mode;
	W,H, bpr,adr: размерМЗ;
	transparent*: булево;

проц & init*(n: Name);
нач
	Raster.InitMode(copy, Raster.srcCopy);
	img :=Graphics.LoadImage(n, истина);
	если img#НУЛЬ то W := img.width-1; H:= img.height-1; всё;
кон init;

проц {перекрыта}Shade*(перем ray: Ray);
перем
	p: Raster.Pixel;
	xi,yj: размерМЗ;
	X,Y: SREAL;
	r,g,b: SREAL;
	a, lx, ly: SREAL;
	nx, ny, nz: цел16;
	dot: SREAL;
	inside: булево;
нач
	если img#НУЛЬ то
		просей ray.face из
			0: inside := истина
			|1: nx := -1
			|2: ny := -1
			|3: nz := -1
			|4: nx := 1
			|5: ny := 1
			|6: nz := 1
		иначе
		всё;
		если ~inside то
			dot := матМодуль(nx*ray.dxyz.x + ny*ray.dxyz.y+ nz*ray.dxyz.z);
	(*		CASE ray.face OF
				1: lx := ray.lxyz.y; ly :=  ray.lxyz.z;
				| 2:  lx := ray.lxyz.x; ly := ray.lxyz.z;
				| 3: lx := ray.lxyz.x; ly := ray.lxyz.y;
				| 4: lx := ray.lxyz.y; ly := ray.lxyz.z;
				| 5: lx := ray.lxyz.x; ly := ray.lxyz.z;
				| 6: lx := ray.lxyz.x; ly := ray.lxyz.y;
			ELSE
			END; *)
			X:=(1-lx)*W; Y:=(1-ly)*H;
			xi:=округлиВниз(X)остОтДеленияНа W; yj:=H - (округлиВниз(Y) остОтДеленияНа H);
			Raster.Get(img,xi,yj,p,copy);
			a:= кодСимв8(p[3])/255; r := кодСимв8(p[2])/255; g := кодСимв8(p[1])/255; b := кодСимв8(p[0])/255;
		       ray.r := ray.r + r*ray.ra*dot;
			ray.g := ray.g + g*ray.ga *dot;
			ray.b := ray.b + b*ray.ba*dot;
			ray.ra := ray.ra-(b+g);
			ray.ga := ray.ga-(r+b);
			ray.ba := ray.ba-(r+g);
			srBase.clamp3(ray.ra,ray.ga, ray.ba);
			ray.a := (ray.ra+ray.ga+ray.ba)/3;
		(*	ray.ra := 0; ray.ga := 0; ray.ba := 0; ray.a := 0; *)
		всё
	всё;
кон Shade;

кон TexVox;

тип texmirrorVox* = окласс(TexVox);
перем
	r, g, b, a, red, blue, green: SREAL;

проц SetColor* (R, G, B, alpha: SREAL);
нач
	red := R; green := G; blue := B;
	r := srBase.clamp(red * alpha);
	g := srBase.clamp(green * alpha);
	b := srBase.clamp(blue * alpha);
	a := srBase.clamp(alpha);
кон SetColor;

проц {перекрыта}Shade*(перем ray: Ray);
перем
	p: Raster.Pixel;
	xi,yj: размерМЗ;
	X,Y: SREAL;
	r,g,b: SREAL;
	dr,dg,db: SREAL;
	lx, ly: SREAL;
	nx, ny, nz: цел16;
	dot: SREAL;
	inside: булево;
нач
	если img#НУЛЬ то
		просей ray.face из
			0: inside := истина
			|1: nx := -1
			|2: ny := -1
			|3: nz := -1
			|4: nx := 1
			|5: ny := 1
			|6: nz := 1
		иначе
		всё;
		если ~inside то
			dot := матМодуль(nx*ray.dxyz.x + ny*ray.dxyz.y+ nz*ray.dxyz.z);
			просей ray.face из
				1: lx := ray.lxyz.y; ly :=  ray.lxyz.z;
				| 2:  lx := ray.lxyz.x; ly := ray.lxyz.z;
				| 3: lx := ray.lxyz.x; ly := ray.lxyz.y;
				| 4: lx := ray.lxyz.y; ly := ray.lxyz.z;
				| 5: lx := ray.lxyz.x; ly := ray.lxyz.z;
				| 6: lx := ray.lxyz.x; ly := ray.lxyz.y;
			иначе
			всё;
			X:=(1-lx)*W; Y:=(1-ly)*H;
			xi:=округлиВниз(X) остОтДеленияНа W; yj:=округлиВниз(Y) остОтДеленияНа H;
			Raster.Get(img,xi,yj,p,copy);
			r := кодСимв8(p[2])/255; g := кодСимв8(p[1])/255; b := кодСимв8(p[0])/255;
			dr := r*ray.ra*dot;
			dg := g*ray.ga*dot;
			db := b*ray.ba*dot;
			ray.r := ray.r + dr;
			ray.g := ray.g + dg;
			ray.b := ray.b + db;
			ray.ra := ray.ra - dr;
			ray.ga := ray.ga - dg;
			ray.ba := ray.ba- db;
			ray.a := (ray.ra+ray.ga+ray.ba)/3;
			если ray.a > 0.25 то
				mirror(ray)
			иначе
				ray.a := 0
			всё
		всё
	всё;
кон Shade;

кон texmirrorVox;

тип scrollVox* = окласс(Voxel);
перем
	img*: Raster.Image;
	fmt: Raster.Format;
	copy : Raster.Mode;
	w,h, bpr,adr: размерМЗ;
	transparent: булево;
	off: цел16;

проц & init*(n: Name);
нач
	img :=Graphics.LoadImage(n, истина);
	w := img.width-1; h := img.height-1;
	Raster.InitMode(copy, Raster.srcCopy);
	register;
кон init;

проц {перекрыта}Shade*(перем ray: Ray);
перем
	p: Raster.Pixel;
	x,y: размерМЗ;
	r,g,b: SREAL;
	lx, ly: SREAL;
	nx, ny, nz: цел16;
	dot: SREAL;
	inside: булево;
нач
	если img#НУЛЬ то
	просей ray.face из
		0: inside := истина
		|1: nx := -1
		|2: ny := -1
		|3: nz := -1
		|4: nx := 1
		|5: ny := 1
		|6: nz := 1
	иначе
	всё;
	если ~inside то
		dot := матМодуль(nx*ray.dxyz.x + ny*ray.dxyz.y+ nz*ray.dxyz.z);
			просей ray.face из
				1: lx := ray.lxyz.y; ly :=  ray.lxyz.z;
				| 2:  lx := ray.lxyz.x; ly := ray.lxyz.z;
				| 3: lx := ray.lxyz.x; ly := ray.lxyz.y;
				| 4: lx := ray.lxyz.y; ly := ray.lxyz.z;
				| 5: lx := ray.lxyz.x; ly := ray.lxyz.z;
				| 6: lx := ray.lxyz.x; ly := ray.lxyz.y;
			иначе
			всё;
		x :=  округлиВниз((1-lx)*w); y :=  округлиВниз((1-ly)*h);
		x := x остОтДеленияНа w;
		y := y остОтДеленияНа h;
		Raster.Get(img,(x+off) остОтДеленияНа w,(y+off) остОтДеленияНа h,p,copy);
		r := кодСимв8(p[2])/255; g := кодСимв8(p[1])/255; b := кодСимв8(p[0])/255;
		ray.r := ray.r + r*ray.ra*dot;
		ray.g := ray.g + g*ray.ga*dot;
		ray.b := ray.b + b*ray.ba*dot;
		ray.a := 0;
		всё
	всё
кон Shade;

(*PROCEDURE tick;
BEGIN
	off := SHORT((off+1) MOD w);
END tick;*)

кон scrollVox;

тип interfereVox* = окласс(Voxel);
перем
	imgn, imgm*: Raster.Image;
	fmt: Raster.Format;
	copy : Raster.Mode;
	w,h, bpr,adr: размерМЗ;
	done: булево;
	off: цел16;

проц & init*(n,m: Name);
нач
	imgn:=Graphics.LoadImage(n, истина);
	w := imgn.width-1; h := imgn.height-1;
	imgm:=Graphics.LoadImage(n, истина);
	w := imgm.width-1; h := imgm.height-1;
	Raster.InitMode(copy, Raster.srcCopy);
	register;
кон init;

проц {перекрыта}Shade*(перем ray: Ray);
перем
	p: Raster.Pixel;
	x,y: размерМЗ;
	r,g,b: SREAL;
	lx, ly: SREAL;
нач
	просей ray.face из
		1: lx := ray.lxyz.y; ly :=  ray.lxyz.z;
		| 2:  lx := ray.lxyz.x; ly := ray.lxyz.z;
		| 3: lx := ray.lxyz.x; ly := ray.lxyz.y;
		| 4: lx := ray.lxyz.y; ly := ray.lxyz.z;
		| 5: lx := ray.lxyz.x; ly := ray.lxyz.z;
		| 6: lx := ray.lxyz.x; ly := ray.lxyz.y;
	иначе
	всё; 	x :=  округлиВниз((lx)*w); y :=  округлиВниз((1-ly)*h);
	если x < 0 то x := 0 аесли x > w то x := w всё;
	если y < 0 то y := 0 аесли y > h то y := h всё;
	Raster.Get(imgn,(x+off) остОтДеленияНа w,y,p,copy);
	r := кодСимв8(p[2])/255; g := кодСимв8(p[1])/255; b := кодСимв8(p[0])/255;
	ray.r := ray.r + r ;
	ray.g := ray.g + g ;
	ray.b := ray.b + b ;
	Raster.Get(imgm,x,y,p,copy);
	r := кодСимв8(p[0])/255; g := кодСимв8(p[1])/255; b := кодСимв8(p[0])/255;
	ray.r := ray.r + r ;
	ray.g := ray.g + g ;
	ray.b := ray.b + b ;
	ray.a := 0;
кон Shade;

проц {перекрыта}tick*;
нач
	off := цел16((off+1) остОтДеленияНа w);
кон tick;

кон interfereVox;

(*
NOT WORKING NOW.
TYPE mirVox *= OBJECT(Voxel);
VAR
	fmt: Raster.Format;
	w,h, bpr,adr: SIGNED32;
	done: BOOLEAN;

PROCEDURE & init;
BEGIN
	w := 100;  h := 100;
END init;

PROCEDURE Shade (VAR ray: Ray);
VAR
	p: Raster.Pixel;
	x,y: SIGNED32;
	red,green,blue, alpha: SREAL;
	a,b,c,lx,ly: SREAL;
BEGIN
	a := ABS(1/2 - ray.lx); b := ABS(1/2 - ray.ly); c := ABS(1/2 - ray.lz);
	IF (a > b) & (a > c ) THEN
		 lx := ray.ly; ly := ray.lz;
	ELSIF (b > a) & (b > c ) THEN
		lx := ray.lx; ly := ray.lz;
	ELSIF (c > a) & (c > b ) THEN
		lx := ray.lx; ly := ray.ly;
	END;
	x :=  ENTIER(lx*w); y :=  ENTIER(ly*h);
	IF x < 0 THEN x := (w DIV 2) ELSIF x > w THEN x := (w DIV 2) END;
	IF y < 0 THEN y := (h DIV 2) ELSIF y > h THEN y := (h DIV 2) END;
	Raster.Get(srBase.img,w-x,y,p,srBase.copy);
	red := ORD(p[2])/255; green := ORD(p[1])/255; blue := ORD(p[0])/255;
	ray.r := ray.r + red;
	ray.g := ray.g + green;
	ray.b := ray.b + blue;
	ray.a :=  0;
END Shade;

END mirVox; *)

тип TexSph*= окласс(TexVox);
перем
	cx, cy, cz: SREAL;
	D2: SREAL;

проц & {перекрыта}init*(n: Name);
нач
	img :=Graphics.LoadImage(n, истина);
	W := img.width-1; H:= img.height-1;
	Raster.InitMode(copy, Raster.srcCopy);
	D2 := 1/4;
	cx := 1/2; cy := 1/2; cz := 1/2;
кон init;

проц ctop(x,y,z: SREAL; перем th,ph,d: SREAL);
нач
	d := Math.sqrt((cx-x)*(cx-x) + (cy-y)*(cy-y) + (cz-z)*(cz-z));
	th := 6.28*srMath.sin((x-cx)/d);
	ph :=  6.28*srMath.cos((y-cy)/d);
кон ctop;

проц {перекрыта}Shade*(перем ray: Ray);
перем
	x,y,z, th,ph,r2, radius, r,g,b: SREAL;
	ax, ay, az, bx, by, bz : SREAL;
	i: цел16;
	p: Raster.Pixel;
	X,Y: размерМЗ;
нач
	если img#НУЛЬ то
		ax := ray.lxyz.x; ay := ray.lxyz.y; az := ray.lxyz.z;
		bx := ray.lxyz.x + ray.dxyz.x; by := ray.lxyz.y+ ray.dxyz.y; bz := ray.lxyz.z+ ray.dxyz.z;
		x := (ax+bx)/2; y := (ay+by)/2; z := (az + bz)/2;
		нцДля i := 0 до 12 делай
			r2 := (cx-x)*(cx-x) + (cy-y)*(cy-y) + (cz-z)*(cz-z);
			если r2 < D2 то
				bx := x; by := y; bz := z
			иначе
				ax := x; ay := y; az := z
			всё;
			x := (ax+bx)/2; y := (ay+by)/2; z := (az + bz)/2;
		кц;
		если (r2-D2) < 0.01 то
			ctop(x,y,z, th, ph, radius);
			X := (округлиВниз(th * 100)) остОтДеленияНа W;
			Y := (округлиВниз(ph * 100)) остОтДеленияНа H;
			Raster.Get(img, X, Y, p,copy);
			r := кодСимв8(p[2])/255; g := кодСимв8(p[1])/255; b := кодСимв8(p[0])/255;
			ray.r := ray.r + r*ray.a;
			ray.g := ray.g + g*ray.a;
			ray.b := ray.b + b *ray.a;
			ray.a := 0;
		всё
	всё;
кон Shade;

кон TexSph;

перем
	rand: Random.Generator;

проц mirror(перем ray: Ray);
нач
	просей ray.face из
		1: 	 ray.dxyz.x:= -ray.dxyz.x;
		|2:	ray.dxyz.y:= -ray.dxyz.y;
		|3:	ray.dxyz.z:= -ray.dxyz.z;
		|4: 	ray.dxyz.x:= -ray.dxyz.x;
		|5:	ray.dxyz.y:= -ray.dxyz.y;
		|6:	ray.dxyz.z:= -ray.dxyz.z;
	иначе
	всё;
	ray.changed := истина;
кон mirror;


нач
	rand:=srBase.rand;
кон srTexVox.

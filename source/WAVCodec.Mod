модуль WAVCodec; (** AUTHOR "MVT, PL"; PURPOSE "WAV audio format Codec"; *)

использует
	Codecs, SoundDevices, Потоки, ЛогЯдра,НИЗКОУР;

конст
	MAXBUF = 4096;

тип
	Chunk = массив 5 из симв8; 								(* type of wave header part *)

	(* Header of a wave file *)
	WaveHeader* = запись
		chunkRIFF: Chunk; 									(* must be "RIFF" *)
		chunkWAVE: Chunk; 									(* must be "WAVE" *)
		chunkfmt: Chunk; 									(* must be "fmt " *)
		waveFormatSize: цел32; 							(* must be 16 for PCM wave *)
		formatTag: цел16; 								(* must be 1 for PCM wave *)
		nofCh: цел16; 									(* number of channels *)
		sRate: цел32; 									(* sampling rate *)
		bRate: цел32; 									(* byte rate *)
		blockAlign: цел16; 								(* bytes per sample *)
		bitsPerSample: цел16; 							(* sampling resolution = bits per sample for 1 channel *)
		chunkdata: Chunk; 									(* must be "data" *)
		fileSize: цел32;									(* size of the whole file minus 8 byte *)
		dataSize: цел32; 									(* size of PCM data in byte = file size minus header size *)
	кон;

	(* Audio Wave PCM Encoder *)
	WAVEncoder* = окласс(Codecs.AudioEncoder)
	перем
		out: Потоки.Писарь;
		h: WaveHeader;

		проц {перекрыта}Open*(out: Потоки.Писарь; sRate, sRes, nofCh: цел32; перем res : целМЗ);
		нач
			res := -1;
			если out = НУЛЬ то
				ЛогЯдра.пСтроку8("WAVEncoder - Writer is NIL"); ЛогЯдра.пВК_ПС;
				возврат;
			всё;
			сам.out := out;

			(* Write wave header *)
			h.chunkRIFF[0] := "R"; h.chunkRIFF[1] := "I"; h.chunkRIFF[2] := "F"; h.chunkRIFF[3] := "F";
			out.пБайты(h.chunkRIFF, 0, 4);

			h.fileSize := размер16_от(WaveHeader)-8; (* for wave file with zero-length sound - will be updated later *)
			WriteRawBELongInt(out, h.fileSize);

			h.chunkWAVE[0] := "W"; h.chunkWAVE[1] := "A"; h.chunkWAVE[2] := "V"; h.chunkWAVE[3] := "E";
			out.пБайты(h.chunkWAVE, 0, 4);

			h.chunkfmt[0] := "f"; h.chunkfmt[1] := "m"; h.chunkfmt[2] := "t"; h.chunkfmt[3] := " ";
			out.пБайты(h.chunkfmt, 0, 4);

			h.waveFormatSize := 16;
			WriteRawBELongInt(out, h.waveFormatSize);

			h.formatTag := 1;
			WriteRawBEInteger(out, h.formatTag);

			h.nofCh := устарПреобразуйКБолееУзкомуЦел(nofCh);
			WriteRawBEInteger(out, h.nofCh);

			h.sRate := sRate;
			WriteRawBELongInt(out, h.sRate);

			h.blockAlign := устарПреобразуйКБолееУзкомуЦел(nofCh * (sRes DIV 8));
			h.bRate := sRate * h.blockAlign;
			WriteRawBELongInt(out, h.bRate);
			WriteRawBEInteger(out, h.blockAlign);

			h.bitsPerSample := устарПреобразуйКБолееУзкомуЦел(sRes);
			WriteRawBEInteger(out, h.bitsPerSample);

			h.chunkdata[0] := "d"; h.chunkdata[1] := "a"; h.chunkdata[2] := "t"; h.chunkdata[3] := "a";
			out.пБайты(h.chunkdata, 0, 4);

			h.dataSize := 0; (* for wave file with zero-length sound - will be updated later *)
			WriteRawBELongInt(out, h.dataSize);

			out.ПротолкниБуферВПоток;
			res := 0
		кон Open;

		проц {перекрыта}Write*(buffer : SoundDevices.Buffer; перем res : целМЗ);
		нач
			out.пБайты(buffer.data^, 0, buffer.len);
			out.ПротолкниБуферВПоток
		кон Write;

	кон WAVEncoder;

	(* Audio Wave PCM Decoder *)
	WAVDecoder* = окласс(Codecs.AudioDecoder)
	перем in: Потоки.Чтец;
		h: WaveHeader;
		hasMoreBytes : булево;

		проц {перекрыта}Open*(in : Потоки.Чтец; перем res : целМЗ);
		перем len: цел32; inLen: размерМЗ; c: симв8;
		нач
			res := -1; inLen := 0;
			если in = НУЛЬ то
				ЛогЯдра.пСтроку8("WAVDecoder - InputStream is NIL"); ЛогЯдра.пВК_ПС;
				возврат;
			всё;
			сам.in := in;

			(* Read header and check for correctness *)
			in.чБайты(h.chunkRIFF, 0, 4, inLen);
			если (inLen # 4) или (h.chunkRIFF # "RIFF") то
				ЛогЯдра.пСтроку8("WAVDecoder - RIFF header ID not found"); ЛогЯдра.пВК_ПС;
				возврат;
			всё;

			ReadRawBELongInt(in, h.fileSize);

			in.чБайты(h.chunkWAVE, 0, 4, inLen);
			если (inLen # 4) или (h.chunkWAVE # "WAVE") то
				ЛогЯдра.пСтроку8("WAVDecoder - WAVE header ID not found"); ЛогЯдра.пВК_ПС;
				возврат;
			всё;

			in.чБайты(h.chunkfmt, 0, 4, inLen);
			если (inLen # 4) или (h.chunkfmt # "fmt ") то
				ЛогЯдра.пСтроку8("WAVDecoder - fmt header ID not found"); ЛогЯдра.пВК_ПС;
				возврат;
			всё;

			ReadRawBELongInt(in, h.waveFormatSize);
			если (h.waveFormatSize < 16) то
				ЛогЯдра.пСтроку8("WAVDecoder - Wrong header size"); ЛогЯдра.пВК_ПС;
				возврат;
			всё;

			in.чЦел16_мз(h.formatTag);
			если (h.formatTag # 1) то
				ЛогЯдра.пСтроку8("WAVDecoder - Wrong wave format (must be PCM)"); ЛогЯдра.пВК_ПС;
				возврат;
			всё;

			ReadRawBEInteger(in, h.nofCh);
			ReadRawBELongInt(in, h.sRate);
			ReadRawBELongInt(in, h.bRate);
			ReadRawBEInteger(in, h.blockAlign);
			ReadRawBEInteger(in, h.bitsPerSample);

			если (h.blockAlign*h.sRate # h.bRate) или (h.nofCh*(h.bitsPerSample DIV 8) # h.blockAlign) то
				ЛогЯдра.пСтроку8("WAVDecoder - Inconsistent header info"); ЛогЯдра.пВК_ПС;
				возврат;
			всё;

			len := h.waveFormatSize - 16;
			нцПока len > 0 делай c := in.чИДайСимв8(); умень(len) кц;

			нцДо
				in.чБайты(h.chunkdata, 0, 4, inLen);
			кцПри (inLen = 4) и (h.chunkdata = "data") или (in.МестоВПотоке() >= (h.fileSize + 8));

			если (inLen # 4) или (h.chunkdata # "data") то
				ЛогЯдра.пСтроку8("WAVDecoder - data header ID not found"); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("res= "); ЛогЯдра.пЦел64(inLen, 0);
				ЛогЯдра.пСтроку8("h.chunkdata= "); ЛогЯдра.пСтроку8(h.chunkdata);
				возврат;
			всё;

			ReadRawBELongInt(in, h.dataSize);

			hasMoreBytes := истина;

			res := 0
		кон Open;

		проц {перекрыта}HasMoreData*():булево;
		нач
			возврат hasMoreBytes
		кон HasMoreData;

		проц {перекрыта}GetAudioInfo*(перем nofChannels, samplesPerSecond, bitsPerSample : цел32);
		нач
			nofChannels := h.nofCh;
			bitsPerSample := h.bitsPerSample;
			samplesPerSecond := h.sRate
		кон GetAudioInfo;

		(* Dumps part of the header *)
		проц DumpHeader;
		нач
			ЛогЯдра.пСтроку8("-- WAV Header Data --"); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("h.nofCh= "); ЛогЯдра.пЦел64(h.nofCh, 0); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("h.sRate= "); ЛогЯдра.пЦел64(h.sRate, 0); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("h.bitsPerSample= "); ЛогЯдра.пЦел64(h.bitsPerSample, 0); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("h.bRate= "); ЛогЯдра.пЦел64(h.bRate, 0); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("h.blockAlign= "); ЛогЯдра.пЦел64(h.blockAlign, 0); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("h.fileSize= "); ЛогЯдра.пЦел64(h.fileSize, 0);
			ЛогЯдра.пСтроку8("h.dataSize= "); ЛогЯдра.пЦел64(h.dataSize, 0)
		кон DumpHeader;

		проц {перекрыта}CanSeek*() : булево;
		нач
			ЛогЯдра.пСтроку8("Not Implemented");
			возврат ложь;
		кон CanSeek;

		проц {перекрыта}GetCurrentSample*() : цел32;
		нач
			возврат округлиВниз((in.МестоВПотоке() - (h.fileSize - h.dataSize)) / h.bRate * h.sRate)
		кон GetCurrentSample;

		проц {перекрыта}GetTotalSamples*() : цел32;
		нач
			возврат округлиВниз(h.dataSize / h.bRate * h.sRate)
		кон GetTotalSamples;

		(* Returns the current time in 1/10 sec *)
		проц {перекрыта}GetCurrentTime*() : цел32;
		нач
			возврат округлиВниз((in.МестоВПотоке() - (h.fileSize - h.dataSize)) / h.bRate * 10)
		кон GetCurrentTime;

		проц {перекрыта}SetStreamLength*(length : цел32);
		нач
			h.fileSize := length-8;
			h.dataSize := length-размер16_от(WaveHeader);
		кон SetStreamLength;

		проц {перекрыта}SeekSample*(sample: цел32; goKeySample : булево; перем res : целМЗ);
		перем seekType: цел32;
		нач
			seekType := Codecs.SeekByte;
			(* in.Seek(seekType, h.fileSize - h.dataSize + ENTIER(sample / h.sRate * h.bRate), itemSize, res); *)
			in.ПерейдиКМестуВПотоке(h.fileSize - h.dataSize + округлиВниз(sample / h.sRate * h.bRate))
		кон SeekSample;

		проц {перекрыта}SeekMillisecond*(millisecond : цел32; goKeySample : булево; перем res : целМЗ);
		нач
			SeekSample(округлиВниз(millisecond / 1000 * h.sRate), goKeySample, res)
		кон SeekMillisecond;

		(** Prepare the next audio bytes not yet filled into a buffer *)
		проц {перекрыта}Next*;
		кон Next;

		проц {перекрыта}FillBuffer*(buffer : SoundDevices.Buffer);
		нач
			in.чБайты(buffer.data^, 0, длинаМассива(buffer.data^), buffer.len);
			если (in.кодВозвратаПоследнейОперации = Потоки.КонецФайла) или (buffer.len < длинаМассива(buffer.data)) то
				hasMoreBytes := ложь;
				возврат;
			всё;
		кон FillBuffer;
	кон WAVDecoder;

	(* Audio PCM Decoder (WAV without header) *)
	PCMDecoder* = окласс(Codecs.AudioDecoder)
	перем
		in: Потоки.Чтец;
		h : WaveHeader;
		hasMoreBytes : булево;

		проц {перекрыта}Open*(in : Потоки.Чтец; перем res : целМЗ);
		нач
			res := -1;
			если in = НУЛЬ то
				ЛогЯдра.пСтроку8("PCMDecoder - InputStream is NIL"); ЛогЯдра.пВК_ПС;
				возврат;
			всё;
			сам.in := in;

			hasMoreBytes := истина;
			res := 0
		кон Open;

		проц {перекрыта}HasMoreData*():булево;
		нач
			возврат hasMoreBytes
		кон HasMoreData;

		проц {перекрыта}GetAudioInfo*(перем nofChannels, samplesPerSecond, bitsPerSample : цел32);
		нач
			nofChannels := h.nofCh;
			bitsPerSample := h.bitsPerSample;
			samplesPerSecond := h.sRate
		кон GetAudioInfo;

		проц {перекрыта}SetAudioInfo*(nofChannels, samplesPerSecond, bitsPerSample : цел32);
		нач
			h.nofCh := устарПреобразуйКБолееУзкомуЦел(nofChannels);
			h.bitsPerSample := устарПреобразуйКБолееУзкомуЦел(bitsPerSample);
			h.sRate := samplesPerSecond;
			(* calc the others *)
			h.bRate := h.nofCh * h.sRate * h.bitsPerSample DIV 8;
			h.blockAlign := h.nofCh * h.bitsPerSample DIV 8
		кон SetAudioInfo;

		проц {перекрыта}CanSeek*() : булево;
		нач
			ЛогЯдра.пСтроку8("Not Implemented");
			возврат ложь;
		кон CanSeek;

		проц {перекрыта}GetCurrentSample*() : цел32;
		нач
			ЛогЯдра.пСтроку8("pi= ");
			возврат округлиВниз(8 * in.МестоВПотоке() / h.bitsPerSample / h.nofCh)
		кон GetCurrentSample;

		проц {перекрыта}GetTotalSamples*() : цел32;
		нач
			ЛогЯдра.пСтроку8("pa= ");
			возврат округлиВниз(8 * h.dataSize / h.bitsPerSample / h.nofCh)
		кон GetTotalSamples;

		(* Returns the current time in 1/10 sec *)
		проц {перекрыта}GetCurrentTime*() : цел32;
		нач
			ЛогЯдра.пСтроку8("po= ");
			возврат округлиВниз(8 * in.МестоВПотоке() / h.bitsPerSample / h.nofCh / h.sRate * 10)
		кон GetCurrentTime;

		проц {перекрыта}SetStreamLength*(length : цел32);
		нач
			h.fileSize := length+размер16_от(WaveHeader)-8;
			h.dataSize := length
		кон SetStreamLength;

		проц {перекрыта}SeekSample*(sample: цел32; goKeySample : булево; перем res : целМЗ);
		перем seekType: цел32;
		нач
			ЛогЯдра.пСтроку8("pu= "); ЛогЯдра.пЦел64(sample, 0);
			ЛогЯдра.пСтроку8("bi= "); ЛогЯдра.пЦел64(h.bitsPerSample, 0);
			seekType := Codecs.SeekByte;
			(* in.Seek(seekType, ENTIER(sample * h.bitsPerSample / 8 * h.nofCh), itemSize, res); *)
			in.ПерейдиКМестуВПотоке(округлиВниз(sample * h.bitsPerSample / 8 * h.nofCh))
		кон SeekSample;

		проц {перекрыта}SeekMillisecond*(millisecond : цел32; goKeySample : булево; перем res : целМЗ);
		нач
			SeekSample(округлиВниз(millisecond / 1000 * h.sRate), goKeySample, res)
		кон SeekMillisecond;

		(** Prepare the next audio bytes not yet filled into a buffer *)
		проц {перекрыта}Next*;
		кон Next;

		проц {перекрыта}FillBuffer*(buffer : SoundDevices.Buffer);
		нач
			in.чБайты(buffer.data^, 0, длинаМассива(buffer.data^), buffer.len);
			если (in.кодВозвратаПоследнейОперации = Потоки.КонецФайла) или (buffer.len < длинаМассива(buffer.data)) то
				hasMoreBytes := ложь; ЛогЯдра.пСтроку8("BOOOOM!!");
				возврат;
			всё;
		кон FillBuffer;
	кон PCMDecoder;

(* Routines for reading and writing numbers in Intel's big endian format *)
проц ReadRawBEInteger(перем r: Потоки.Чтец; перем value: цел16);
нач
	value := кодСимв8(r.чИДайСимв8()) + 100H *кодСимв8(r.чИДайСимв8());
кон ReadRawBEInteger;

проц ReadRawBELongInt(перем r: Потоки.Чтец; перем value: цел32);
нач
	value := устарПреобразуйКБолееШирокомуЦел(кодСимв8(r.чИДайСимв8())) + 100H * устарПреобразуйКБолееШирокомуЦел(кодСимв8(r.чИДайСимв8()))
		+ 10000H * устарПреобразуйКБолееШирокомуЦел(кодСимв8(r.чИДайСимв8())) + 1000000H * устарПреобразуйКБолееШирокомуЦел(кодСимв8(r.чИДайСимв8()));
кон ReadRawBELongInt;

проц WriteRawBEInteger(перем w: Потоки.Писарь; value: цел16);
нач
	w.пСимв8(симв8ИзКода(value остОтДеленияНа 100H));
	w.пСимв8(симв8ИзКода(value DIV 100H));
кон WriteRawBEInteger;

проц WriteRawBELongInt(перем w: Потоки.Писарь; value: цел32);
нач
	w.пСимв8(симв8ИзКода(value остОтДеленияНа 100H));
	value := value DIV 100H;
	w.пСимв8(симв8ИзКода(value остОтДеленияНа 100H));
	value := value DIV 100H;
	w.пСимв8(симв8ИзКода(value остОтДеленияНа 100H));
	w.пСимв8(симв8ИзКода(value DIV 100H));
кон WriteRawBELongInt;

(* -- Factories -- *)
проц EncoderFactory*() : Codecs.AudioEncoder;
перем p : WAVEncoder;
нач
	нов(p);
	возврат p
кон EncoderFactory;

проц DecoderFactory*() : Codecs.AudioDecoder;
перем p : WAVDecoder;
нач
	нов(p);
	возврат p
кон DecoderFactory;

проц PCMDecoderFactory*() : Codecs.AudioDecoder;
перем p : PCMDecoder;
нач
	нов(p);
	возврат p
кон PCMDecoderFactory

кон WAVCodec.

------------------------------------------------------------------------------

System.Free WAVCodec;

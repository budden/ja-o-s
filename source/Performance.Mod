(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Performance; (** AUTHOR "pjm"; PURPOSE "Performance measurements"; *)

использует ЛогЯдра, ЭВМ, Modules, Objects, Kernel, Random, Math;

конст
	LoadPeriod = 5;	(* load sample interval in seconds *)
	IdlePeriod = 10;	(* idle sample interval in seconds *)

	Time0 = 1; Time1 = 5; Time2 = 15;	(* average load accumulates in minutes *)

	Trace = ложь;

тип
	Monitor = окласс
		перем
			period: цел32; rand: Random.Generator; timer: Kernel.Timer;
			loops: цел32; state: цел8;

		проц &Init*(period: цел32; randomized: булево);
		нач
			сам.period := period; state := 0;
			если randomized то нов(rand) иначе rand := НУЛЬ всё
		кон Init;

		проц Compute(loops: цел32);	(* abstract *)
		кон Compute;

		проц Stop;
		нач {единолично}
			если state = 0 то увел(state) всё;
			timer.Wakeup;
			дождись(state = 2)	(* body terminated *)
		кон Stop;

	нач {активное, приоритет(Objects.High)}
		нов(timer); loops := 0;
		нцПока state = 0 делай
			увел(loops); Compute(loops);
			если rand # НУЛЬ то
				timer.Sleep(округлиВниз(period*(1000*2)*rand.Uniform()))
			иначе
				timer.Sleep(period*1000)
			всё
		кц;
		нач {единолично} увел(state) кон
	кон Monitor;

тип
	LoadMonitor = окласс (Monitor)	(* singleton *)
		перем exp: массив 3 из вещ32;

		проц &{перекрыта}Init*(period: цел32; randomized: булево);
		нач
			Init^(period, randomized);
			load[0] := 0; load[1] := 0; load[2] := 0;
			exp[0] := 1/Math.exp(period/(Time0*60));
			exp[1] := 1/Math.exp(period/(Time1*60));
			exp[2] := 1/Math.exp(period/(Time2*60))
		кон Init;

		проц {перекрыта}Compute(loops: цел32);
		перем n: цел32;
		нач
			n := Objects.NumReady() - 1;	(* subtract one for fridge light *)
			UpdateLoad(load[0], exp[0], n);
			UpdateLoad(load[1], exp[1], n);
			UpdateLoad(load[2], exp[2], n);
			если Trace то
				ЛогЯдра.пЦел64(loops, 8); ЛогЯдра.пЦел64(n, 3);
				WriteLoad(load[0]); WriteLoad(load[1]); WriteLoad(load[2]);
				ЛогЯдра.пВК_ПС
			всё
		кон Compute;

	кон LoadMonitor;

тип
	IdleMonitor = окласс (Monitor)	(* singleton *)
		перем milliTimer: Kernel.MilliTimer; idlecount: массив ЭВМ.МаксКвоПроцессоров из цел32;

		проц &{перекрыта}Init*(period: цел32; randomized: булево);
		перем i: цел32;
		нач
			Init^(period, randomized);
			нцДля i := 0 до ЭВМ.МаксКвоПроцессоров-1 делай
				idlecount[i] := Objects.idlecount[i]; idle[i] := -1
			кц;
			Kernel.SetTimer(milliTimer, 0);
		кон Init;

		проц {перекрыта}Compute(loops: цел32);
		перем i, ic, id, td: цел32;
		нач
			td := Kernel.Elapsed(milliTimer); Kernel.SetTimer(milliTimer, 0);
			если td = 0 то td := 1 всё;	(* avoid divide by 0 *)
			нцДля i := 0 до ЭВМ.МаксКвоПроцессоров-1 делай
				ic := Objects.idlecount[i];
				id := ic - idlecount[i]; idlecount[i] := ic;
				если ic # 0 то	(* processor alive, ignore wrap *)
					idle[i] := id*100 DIV td	(* SIGNED32 assignments are atomic *)
				всё
			кц;
			если Trace то
				ЛогЯдра.пЦел64(loops, 8);
				нцДля i := 0 до ЭВМ.МаксКвоПроцессоров-1 делай ЛогЯдра.пЦел64(idle[i], 5) кц;
				ЛогЯдра.пВК_ПС
			всё
		кон Compute;

	кон IdleMonitor;

перем
	load*: массив 3 из вещ32;	(** load estimates *)
	idle*: массив ЭВМ.МаксКвоПроцессоров из цел32;	(** idle percentage estimates *)
	loadmon: LoadMonitor;
	idlemon: IdleMonitor;

проц UpdateLoad(перем load: вещ32; exp: вещ32; n: цел32);
нач
	load := load*exp + (1-exp)*n	(* FLOAT32 assigments are atomic *)
кон UpdateLoad;

проц WriteLoad(load: вещ32);
перем x: цел32;
нач
	если Trace то
		x := округлиВниз(load*100 + 0.5);
		ЛогЯдра.пЦел64(x DIV 100, 3); ЛогЯдра.пСимв8(".");
		ЛогЯдра.пЦел64(x DIV 10 остОтДеленияНа 10, 1); ЛогЯдра.пЦел64(x остОтДеленияНа 10, 1)
	всё
кон WriteLoad;

проц Cleanup;
нач
	если loadmon # НУЛЬ то loadmon.Stop; loadmon := НУЛЬ всё;
	если idlemon # НУЛЬ то idlemon.Stop; idlemon := НУЛЬ всё
	(* race with object bodies on Free! *)
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
	если loadmon = НУЛЬ то нов(loadmon, LoadPeriod, истина) всё;
	если idlemon = НУЛЬ то нов(idlemon, IdlePeriod, ложь) всё
кон Performance.

(**
Notes:
o "load" is a Unix-like estimate of the average number of ready and running processes over the past 1, 5 and 15 minutes.
o "idle" is an estimate of the percentage of idle time per processor over the last 10 seconds.
o When a processor is not available, its idle estimate is -1.
*)

(*
to do:
o fix idle on single-processor
o adjust idle computation to timeslice rate (currently assumes timeslice rate = AosTimer rate)
o Kernel.GetTimer and Objects.idlecount effects can give 101% idle
*)

System.Free Performance ~

System.ShowCommands Performance

System.State Performance

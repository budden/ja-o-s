модуль EventsKernelLog; (** AUTHOR "staubesv"; PURPOSE "Log system events to kernel log"; *)
(**
 * Log system events to kernel log
 *
 * History:
 *
 *	07.03.2007	First release (staubesv)
 *)

использует
	ЛогЯдра, Modules, Events, EventsUtils, Потоки;

конст
	Verbose = истина;

	(* System event classification (AosEventClasses.XML) *)
	Class = 1;		(* Events *)
	Subclass = 2;	(* Logging *)

	ModuleName = "EventsKernelLog";

тип

	EventLogger = окласс(Events.Sink);
	перем w : Потоки.Писарь;

		проц {перекрыта}Handle*(event : Events.Event);
		нач
			EventsUtils.ToStream(w, event);
		кон Handle;

		проц &Init*;
		нач
			name := ModuleName;
			нов(w, ЛогЯдра.ЗапишиВПоток, 1024);
		кон Init;

	кон EventLogger;

перем
	eventLogger- : EventLogger;

проц Install*;
кон Install;

проц Cleanup;
нач
	Events.AddEvent(ModuleName, Events.Information, Class, Subclass, 0, "Kernel log event logger shut down", Verbose);
	Events.Unregister(eventLogger);
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
	нов(eventLogger);
	Events.Register(eventLogger);
	Events.AddEvent(ModuleName, Events.Information, Class, Subclass, 0, "Started kernel log event logger", Verbose);
кон EventsKernelLog.

EventsKernelLog.Install ~		System.Free EventsKernelLog ~

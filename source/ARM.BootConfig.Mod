модуль (*Dyn*)BootConfig; (** AUTHOR "Timothée Martiel"; PURPOSE "Boot configuration from memory area"; *)

использует
	НИЗКОУР,
	Initializer(*,
	Commands*);

конст
	ConfigSize = 4096;
	CR = 0DX;
	LF = 0AX;

перем
	config: укль {опасныйДоступКПамяти,неОтслСборщиком} на массив ConfigSize из симв8;
	size: цел32;

	проц GetValue * (конст key: массив из симв8; перем value: массив из симв8);
	перем
		i, len: цел32;
	нач
		i := 0;
		len := 0;
		нц
			если i = size то прервиЦикл всё;
			(* is that the key? *)
			если MatchKey(config^, i, key) то
				нцДо увел(i) кцПри config[i] = '=';
				увел(i);
				нцПока (config[i + len] # CR) и (config[i + len] # LF) делай увел(len) кц;
				утв(len <= длинаМассива(value), 7);
				НИЗКОУР.копируйПамять(адресОт(config[i]), адресОт(value[0]), len);
				прервиЦикл
			иначе
				нцДо увел(i) кцПри (i = size) или (config[i] = LF);
				если i = size то прервиЦикл всё;
				увел(i)
			всё;
		кц;
		value[len] := 0X;
	кон GetValue;

	проц GetIntValue * (конст key: массив из симв8): цел32;
	перем
		str: массив 128 из симв8;
	нач
		GetValue(key, str);
		возврат StrToInt(str)
	кон GetIntValue;

	проц GetBoolValue * (конст key: массив из симв8): булево;
	перем
		str: массив 128 из симв8;
	нач
		GetValue(key, str);
		возврат StrToBool(str)
	кон GetBoolValue;

	проц Init *;
	нач
		НИЗКОУР.копируйПамять(Initializer.configBase, 1FFFF000H, Initializer.configSize);
		Initializer.configBase := 1FFFF000H;
		config := Initializer.configBase;
		size := Initializer.configSize;
		если size >= ConfigSize то size := ConfigSize-1; всё;
		config[size] := CR;
	кон Init;

	проц StrToInt (конст str: массив из симв8): цел32;
	перем
		i, value: цел32;
	нач
		i := 0;
		value := 0;
		нц
			если (i = длинаМассива(str)) или (str[i] = 0X) то возврат value всё;
			если (str[i] > '9') или (str[i] < '0') то возврат 0 всё;
			value := value * 10;
			увел(value, кодСимв8(str[i]) - кодСимв8('0'));
			увел(i)
		кц
	кон StrToInt;

	проц StrToBool (конст str: массив из симв8): булево;
	нач
		если str = '1' то возврат истина всё;
		возврат ложь
	кон StrToBool;

	(** return TRUE if str1 from ofs1 until the first '=' (excluded) matches str2 *)
	проц MatchKey (конст str1: массив из симв8; ofs1: цел32; конст str2: массив из симв8): булево;
	перем
		i: цел32;
	нач
		i := 0;
		нцПока (str1[ofs1 + i] # '=') и (str2[i] # 0X) и (str1[ofs1 + i] = str2[i]) делай увел(i) кц;
		возврат (str1[ofs1 + i] = '=') и (str2[i] = 0X)
	кон MatchKey;

(*	PROCEDURE Get * (context: Commands.Context);
	VAR
		key, val: ARRAY 128 OF CHAR;
	BEGIN
		IF ~context.arg.GetString(key) THEN RETURN END;
		context.out.String(key);
		context.out.String(": ");
		IF context.arg.GetString(val) THEN
			IF val = "int" THEN
				context.out.Int(GetIntValue(key), 0)
			ELSIF val = "bool" THEN
				IF GetBoolValue(key) THEN
					context.out.String("TRUE")
				ELSE
					context.out.String("FALSE")
				END
			ELSE
				GetValue(key, val);
				context.out.String("'");
				context.out.String(val);
				context.out.String("'");
			END
		ELSE
			GetValue(key, val);
			context.out.String("'");
			context.out.String(val);
			context.out.String("'");
		END;
		context.out.Ln
	END Get;

	PROCEDURE Print * (context: Commands.Context);
	VAR
		i: SIGNED32;
	BEGIN
		FOR i := 0 TO size - 1 DO
			context.out.Char(config[i])
		END
	END Print;*)
(*BEGIN
	cfg := "CpuClockHz=666666666
UartInputClockHz=50000000
KernelOutputUart=1
PsRefClockHz=50000000
KernelOutputUart=1
CpuNb=2
UsbPhyRstGpio0=46
UsbPhyRstGpio1=-1
UsbEnable0=1
UsbEnable1=0
UsbViewportInit=1
SdClock0=50000000
SdClock1=50000000
SdEnable0=1
SdEnable1=0
CpuNb=2
DDRSize=536870912
ConfigSize=4096;
HeapSize=469762048
KernelLoadAdr=1048576
StackSize=262144
EnableCaching=1
EnableKernelWatchdog=1
EnableFreeLists=0
EnableReturnBlocks=0
TraceHeaps=0
TraceModules=0
TraceInterrupts=0
";
config := ADDRESSOF(cfg[0]);
size := Strings.Length(cfg)*)
кон (*Dyn*)BootConfig.

модуль HierarchicalProfiler0; (** AUTHOR "staubesv"; PURPOSE "Platform-specific part of the hierarchical profiler"; *)

использует
	НИЗКОУР, ЭВМ, Objects, Modules, Kernel;

конст
	HandlerNotInstalled = 0;
	HandlerInstalled = 1;

	(* 	Time to wait when unloading the module before it is actually unloaded. This avoids that a pending interrupt
		calls 'HandleTimer' when the module is already unloaded. *)
	WaitTime = 100;	(* ms *)

тип
	Callback = проц (id : цел32; process : Objects.Process; pc, bp, lowAdr, highAdr : адресВПамяти);

перем
	callback : Callback;
	state : цел32;

(* First level interrupt handler. Called 1000 times per second by each processor *)
проц HandleTimer(id: цел32; конст state: ЭВМ.СостояниеПроцессора);
перем process : Objects.Process;
нач
	process := Objects.running[id];
	callback(id, process, state.PC, state.BP, state.SP, цел32(0FFFFFFFFH));
кон HandleTimer;

(** Start profiling. If the profiler is already running, it is stopped and the sample data is discarded before re-starting it *)
проц Enable*(proc : Callback);
нач {единолично}
	утв(proc # НУЛЬ);
	утв(state = HandlerNotInstalled);
	ЭВМ.InstallEventHandler(HandleTimer);
	state := HandlerInstalled;
	callback := proc;
кон Enable;

(** Stop profiling. The profile data is not discarded. It can be retrieved using the procedure 'GetProfile' *)
проц Disable*;
перем timer : Kernel.Timer;
нач {единолично}
	утв(state = HandlerInstalled);
	ЭВМ.InstallEventHandler(НУЛЬ);
	нов(timer); timer.Sleep(WaitTime);
кон Disable;

проц Cleanup;
нач
	ЭВМ.InstallEventHandler(НУЛЬ);
кон Cleanup;

нач
	callback := НУЛЬ;
	state := HandlerNotInstalled;
	Modules.InstallTermHandler(Cleanup);
кон HierarchicalProfiler0.

модуль WMMenus;	(** AUTHOR "TF/staubesv"; PURPOSE "Menu support"; *)
(*

	Vertical menu entry layout:

	| HMenuDistance | image.width OR MinImageWidth | HMenuDistance | TextImageDistance | TextWidth | HMenuDistance |

*)
использует
	Inputs, Strings, Raster, WMRectangles, WMGraphics, WMGraphicUtilities, WMComponents,
	WMWindowManager, WMProperties, WMEvents, WMDropTarget, WMTrees;

конст
	OpenDefault* = OpenDownRight;
	OpenUpLeft* = 1;
	OpenUpRight* = 2;
	OpenDownLeft* = 3;
	OpenDownRight* = 4;

	(* ShadowWindow.type *)
	Right = 0;
	Bottom = 1;

	ShadowWidth = 5;
	ShadowHeight = 5;
	ShadowOffsetVertical = 5;
	ShadowOffsetHorizontal = 5;

	LightGrey = цел32(0C0C0C0FFH);
	LightGreyDrag = цел32(0C0C0C0C0H);
	WhiteDrag = цел32(0FFFFFFC0H);

	TextImageDistance = 4;
	MinImageWidth = 4;

	HMenuDistance = 8;
	VMenuDistance = 4;

	SeparatorCaption = "---";
	SeparatorWidth = 9;
	SeparatorHeight = 5;

	DragDist = 10;

тип

	Separator* = окласс(WMTrees.TreeNode)
	кон Separator;

тип

	DragWrapper* = окласс
	кон DragWrapper;

тип

	MenuPanel*= окласс(WMComponents.VisualComponent)
	перем
		horizontal- : WMProperties.BooleanProperty;
		horizontalI : булево;

		openDirection- : WMProperties.Int32Property;
		openDirectionI : цел32;

		clSelected : WMProperties.ColorProperty;

		onSelect- : WMEvents.EventSource;

		menu : WMTrees.Tree;
		root, selection, hover : WMTrees.TreeNode;

		subMenuIndicatorImg : WMGraphics.Image;

		subMenu, parentWindow : MenuWindow;

		parentMenuPanel, focusPanel, rootMenuPanel : MenuPanel;

		greyBoxWidth : размерМЗ;

		dragNode : WMTrees.TreeNode;
		dragObject : динамическиТипизированныйУкль;

		(* pointer handling *)
		leftClick, dragPossible : булево;
		downX, downY : цел32;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrMenuPanel);
			нов(horizontal, НУЛЬ, НУЛЬ, НУЛЬ); properties.Add(horizontal);
			horizontalI := horizontal.Get();
			нов(openDirection, НУЛЬ, НУЛЬ, НУЛЬ); properties.Add(openDirection);
			нов(clSelected, НУЛЬ, НУЛЬ, НУЛЬ); properties.Add(clSelected);
			clSelected.Set(WMGraphics.Blue);
			нов(onSelect, сам, НУЛЬ, НУЛЬ, НУЛЬ);
			openDirectionI := OpenDefault;
			openDirection.Set(openDirectionI);
			menu := НУЛЬ;
			root := НУЛЬ; selection := НУЛЬ; hover := НУЛЬ;
			subMenuIndicatorImg := НУЛЬ;
			subMenu := НУЛЬ; parentWindow := НУЛЬ;
			greyBoxWidth := 2 * HMenuDistance + MinImageWidth;
			dragObject := НУЛЬ;
			parentMenuPanel := НУЛЬ; focusPanel := сам; rootMenuPanel := сам;
			takesFocus.Set(истина);
		кон Init;

		проц {перекрыта}PropertyChanged*(sender, property : динамическиТипизированныйУкль);
		нач
			если (property = clSelected) то
				Invalidate;
			аесли (property = horizontal) то
				horizontalI := horizontal.Get();
				Invalidate;
			аесли (property = openDirection) то
				openDirectionI := openDirection.Get();
				Invalidate;
			аесли property=properties то
				RecacheProperties; Invalidate
			иначе
				PropertyChanged^(sender, property);
			всё;
		кон PropertyChanged;

		проц {перекрыта}RecacheProperties*;
		нач
			RecacheProperties^;
			horizontalI := horizontal.Get();
			openDirectionI := openDirection.Get();
			(*Invalidate;*)
		кон RecacheProperties;

		проц SetParent(parentMenuPanel : MenuPanel);
		нач
			сам.parentMenuPanel := parentMenuPanel;
			если (parentMenuPanel # НУЛЬ) то
				rootMenuPanel := parentMenuPanel.rootMenuPanel;
			всё;
		кон SetParent;

		(* If menus are used as popup menus, the initial window most be registered here so it can be closed when an item has been selected *)
		проц SetParentWindow(parentWindow : MenuWindow);
		нач
			утв(parentWindow # НУЛЬ);
			сам.parentWindow := parentWindow;
		кон SetParentWindow;

		проц SetMenu*(menu : WMTrees.Tree; root : WMTrees.TreeNode);
		нач
			утв((menu # НУЛЬ) и (root # НУЛЬ));
			Acquire;
			сам.menu := menu; сам.root := root; hover := НУЛЬ;
			greyBoxWidth := матМаксимум(MinImageWidth + 2 * HMenuDistance , MaxImageWidth() + 2 * HMenuDistance);
			Invalidate;
			Release
		кон SetMenu;

		проц Measure(перем width, height : размерМЗ);
		перем child : WMTrees.TreeNode;
		нач
			утв((menu # НУЛЬ) и (root # НУЛЬ));
			width := 0; height := 0;
			если horizontal.Get() то
				menu.Acquire;
				child := menu.GetChildren(root);
				нцПока (child # НУЛЬ) делай
					width := width + ItemWidth(child, истина);
					child := menu.GetNextSibling(child);
				кц;
				menu.Release;
			иначе
				menu.Acquire;
				child := menu.GetChildren(root);
				нцПока (child # НУЛЬ) делай
					height := height + ItemHeight(child);
					width := матМаксимум(width, ItemWidth(child, ложь));
					child := menu.GetNextSibling(child);
				кц;
				menu.Release;
			всё;
		кон Measure;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		перем
			child : WMTrees.TreeNode;
			x, y, dx, dy, t, textY : размерМЗ;
			font : WMGraphics.Font;
			caption : Strings.String;
			image : Raster.Image;
		нач
			DrawBackground^(canvas);
			если (menu = НУЛЬ) или (root = НУЛЬ) то возврат; всё;
			font := GetFont();
			canvas.SetFont(font);
			canvas.SetColor(WMGraphics.Black);
			если horizontalI то
				x := 0;
				menu.Acquire;
				child := menu.GetChildren(root);
				нцПока (child # НУЛЬ) делай
					если ~(child суть Separator) то
						если (child = hover) то
							canvas.Fill(WMRectangles.MakeRect(x, 0, x + ItemWidth(child, horizontalI), bounds.GetHeight()), цел32(0FFFF00FFH), WMGraphics.ModeCopy);
						аесли (child = selection) то
							canvas.Fill(WMRectangles.MakeRect(x, 0, x + ItemWidth(child, horizontalI), bounds.GetHeight()), clSelected.Get(), WMGraphics.ModeCopy);
						всё;
						x := x + HMenuDistance;
						image := menu.GetNodeImage(child);
						если (image # НУЛЬ) то
							canvas.DrawImage(x, 0, image, WMGraphics.ModeSrcOverDst);
							x := x + image.width + HMenuDistance + TextImageDistance;
						всё;

						caption := menu.GetNodeCaption(child);
						если (caption # НУЛЬ) то
							font.GetStringSize(caption^, dx, dy); canvas.DrawString(x, dy, caption^);
							x := x + dx;
						всё;
						увел(x, HMenuDistance);
					иначе
						увел(x, HMenuDistance);
						canvas.Line(x + (SeparatorWidth DIV 2) + 1, 2, x + (SeparatorWidth DIV 2) + 1, bounds.GetHeight() - 2, WMGraphics.Black, WMGraphics.ModeCopy);
						x := x + SeparatorWidth + HMenuDistance;
					всё;
					child := menu.GetNextSibling(child)
				кц;
				menu.Release;
			иначе
				y := 0;
				menu.Acquire;
				если (openDirectionI = OpenDownLeft) или (openDirectionI = OpenUpLeft) то
					canvas.Fill(WMRectangles.MakeRect(bounds.GetWidth() - greyBoxWidth, 0, bounds.GetWidth() - greyBoxWidth, bounds.GetHeight()), LightGrey, WMGraphics.ModeCopy);
				иначе
					canvas.Fill(WMRectangles.MakeRect(0, 0, greyBoxWidth, bounds.GetHeight()), LightGrey, WMGraphics.ModeCopy);
				всё;
				child := menu.GetChildren(root);
				нцПока (child # НУЛЬ) делай
					x := HMenuDistance;
					если ~(child суть Separator) то
						если (child = hover) то
							canvas.Fill(WMRectangles.MakeRect(0, y, bounds.GetWidth(), y + ItemHeight(child)), цел32(0FFFF00FFH), WMGraphics.ModeCopy);
						аесли (child = selection) то
							canvas.Fill(WMRectangles.MakeRect(0, y, bounds.GetWidth(), y + ItemHeight(child)), clSelected.Get(), WMGraphics.ModeCopy);
						всё;
						увел(y, VMenuDistance);
						dy := 0;
						image := menu.GetNodeImage(child);
						если (image # НУЛЬ) то
							canvas.DrawImage(x, y, image, WMGraphics.ModeSrcOverDst);
							x := x + image.width + HMenuDistance + TextImageDistance;
							dy := image.height;
						иначе
							x := x + MinImageWidth + HMenuDistance + TextImageDistance;
						всё;

						caption := menu.GetNodeCaption(child);
						если (caption # НУЛЬ) то
							font.GetStringSize(caption^, dx, t);
							если (image # НУЛЬ) и (image.height > t) то
								textY := y + ((image.height + t - font.GetDescent()) DIV 2);
							иначе
								textY := y + font.ascent;
								dy := t;
							всё;
							canvas.DrawString(x, textY, caption^);
						всё;

						если menu.GetChildren(child) # НУЛЬ то
							если subMenuIndicatorImg # НУЛЬ то
								canvas.DrawImage(bounds.GetWidth() - subMenuIndicatorImg.width, 0, subMenuIndicatorImg, WMGraphics.ModeSrcOverDst)
							иначе
								canvas.DrawString(bounds.GetWidth() - 10, textY, "...")
							всё
						всё;
						y := y + dy + VMenuDistance;
					иначе
						y := y + VMenuDistance;
						canvas.Line(greyBoxWidth + 4, y + (SeparatorHeight DIV 2) + 1, bounds.GetWidth(), y + (SeparatorHeight DIV 2) + 1, LightGrey, WMGraphics.ModeCopy);
						y := y + SeparatorHeight + VMenuDistance;
					всё;
					child := menu.GetNextSibling(child)
				кц;
				menu.Release;
			всё;
		кон DrawBackground;

		(* caller must hold tree lock *)
		проц ItemWidth(item : WMTrees.TreeNode; isHorizontal : булево) : размерМЗ;
		перем
			width, dx, dy : размерМЗ;
			font : WMGraphics.Font;
			caption : Strings.String;
			image : Raster.Image;
		нач
			утв(menu.HasLock());
			width := 0;
			если ~(item суть Separator) то

				image := menu.GetNodeImage(item);
				если (image # НУЛЬ) то
					width := image.width + HMenuDistance + TextImageDistance;
				аесли ~(isHorizontal) то
					width := width + MinImageWidth + HMenuDistance + TextImageDistance;
				всё;

				caption := menu.GetNodeCaption(item);
				если (caption # НУЛЬ) то
					font := GetFont(); font.GetStringSize(caption^, dx, dy);
					width := width + dx;
				всё;

			иначе
				width := SeparatorWidth;
			всё;
			width := width + 2*HMenuDistance;
			возврат width;
		кон ItemWidth;

		(* caller must hold tree lock *)
		проц ItemHeight(item : WMTrees.TreeNode) : размерМЗ;
		перем
			height, dx, dy : размерМЗ;
			font : WMGraphics.Font;
			caption : Strings.String;
			image : Raster.Image;
		нач
			height := 0;
			если ~(item суть Separator) то

				caption := menu.GetNodeCaption(item);
				если (caption # НУЛЬ) то
					font := GetFont(); font.GetStringSize(caption^, dx, dy);
					height := dy;
				всё;

				image := menu.GetNodeImage(item);
				если (image # НУЛЬ) то
					если (image.height  > height) то
						height := image.height;
					всё;
				всё;
			иначе
				height := SeparatorHeight;
			всё;
			height := height + 2 * VMenuDistance;
			возврат height
		кон ItemHeight;

		проц MaxImageWidth() : размерМЗ;
		перем child : WMTrees.TreeNode; image : WMGraphics.Image; maxWidth : размерМЗ;
		нач
			maxWidth := 0;
			menu.Acquire;
			child := menu.GetChildren(root);
			нцПока (child # НУЛЬ) делай
				image := menu.GetNodeImage(child);
				если (image # НУЛЬ) и (image.width > maxWidth) то
					maxWidth := image.width;
				всё;
				child := menu.GetNextSibling(child);
			кц;
			menu.Release;
			возврат maxWidth;
		кон MaxImageWidth;

		проц IsSelectable(node : WMTrees.TreeNode) : булево;
		нач
			утв(node # НУЛЬ);
			возврат ~(node суть Separator);
		кон IsSelectable;

		проц FindHorizontal(x : размерМЗ) : WMTrees.TreeNode;
		перем p : размерМЗ; child : WMTrees.TreeNode;
		нач
			p := 0;
			menu.Acquire;
			child := menu.GetChildren(root);
			если (child # НУЛЬ) то
				нцДо
					p := p + ItemWidth(child, horizontalI);
					если p < x то child := menu.GetNextSibling(child); всё;
				кцПри (child = НУЛЬ) или (p >= x);
			всё;
			menu.Release;
			возврат child;
		кон FindHorizontal;

		проц FindVertical(y : размерМЗ) : WMTrees.TreeNode;
		перем p : размерМЗ; child : WMTrees.TreeNode;
		нач
			p := 0;
			menu.Acquire;
			child := menu.GetChildren(root);
			если (child # НУЛЬ) то
				нцДо
					p := p + ItemHeight(child);
					если p < y то child := menu.GetNextSibling(child); всё;
				кцПри (child = НУЛЬ) или (p >= y);
			всё;
			menu.Release;
			возврат child;
		кон FindVertical;

		проц GetItemRect(i : WMTrees.TreeNode; перем r : WMRectangles.Rectangle);
		перем child : WMTrees.TreeNode;
		нач
			r.l := 0; r.t := 0;
			menu.Acquire;
			child := menu.GetChildren(root);
			нцПока (child # НУЛЬ) и (child # i) делай
				если horizontal.Get() то
					увел(r.l, ItemWidth(child, horizontalI));
				иначе
					увел(r.t, ItemHeight(child));
				всё;
				child := menu.GetNextSibling(child);
			кц;
			если (child # НУЛЬ) то r.r := r.l + ItemWidth(child, horizontalI); r.b := r.t + ItemHeight(child) всё;
			menu.Release
		кон GetItemRect;

		проц LeafSelect(item : WMTrees.TreeNode);
		перем data : динамическиТипизированныйУкль;
		нач
			если parentMenuPanel = НУЛЬ то
				CloseSubMenu(ложь);
				menu.Acquire;
				data := menu.GetNodeData(item);
				menu.Release;
				если (data # НУЛЬ) то
					onSelect.Call(data);
				иначе
					onSelect.Call(item);
				всё;
				если (parentWindow # НУЛЬ) то
					parentWindow.CloseMenu(сам, НУЛЬ); parentWindow := НУЛЬ;
				всё;
			иначе
				parentMenuPanel.LeafSelect(item);
			всё
		кон LeafSelect;

		проц SetSelection(node : WMTrees.TreeNode);
		нач
			если (selection # node) то
				selection := node;
				Invalidate;
			всё;
		кон SetSelection;

		проц SelectNode(node : WMTrees.TreeNode; indicateLast : булево);
		перем child : WMTrees.TreeNode; r : WMRectangles.Rectangle; x, y : размерМЗ;
		нач
			утв(node # НУЛЬ);
			menu.Acquire;
			child := menu.GetChildren(node);
			если (child # НУЛЬ) то
				GetItemRect(node, r);
				если horizontal.Get() то
					если openDirection.Get() в {OpenUpLeft, OpenUpRight} то ToWMCoordinates(r.l, r.t, x, y);
					иначе ToWMCoordinates(r.l, r.b, x, y);
					всё
				иначе
					просей openDirection.Get() из
						|OpenUpLeft : ToWMCoordinates(r.l, r.b, x, y);
						|OpenUpRight : ToWMCoordinates(r.r, r.b, x, y);
						|OpenDownLeft : ToWMCoordinates(r.l, r.t, x, y);
						|OpenDownRight : ToWMCoordinates(r.r, r.t, x, y);
					иначе
						ToWMCoordinates(r.r, r.t, x, y);
					всё;
				всё;
				CloseSubMenu(indicateLast);
				SetSelection(node);
				нов(subMenu, x, y, openDirection.Get(), menu, node, сам, ложь, истина);
				rootMenuPanel.focusPanel := subMenu.menuPanel;
				PointerLeave;
			иначе
				LeafSelect(node)
			всё;
			menu.Release;
		кон SelectNode;

		проц CloseSubMenu(indicateLast : булево);
		нач
			если (subMenu # НУЛЬ) то
				subMenu.CloseMenu(НУЛЬ, НУЛЬ); subMenu := НУЛЬ;
				если (selection # НУЛЬ) то
					если indicateLast то hover := selection; всё;
					selection := НУЛЬ;
					Invalidate;
				всё;
				rootMenuPanel.focusPanel := сам;
			всё;
		кон CloseSubMenu;

		проц {перекрыта}PointerDown*(x, y : размерМЗ; keys : мнвоНаБитахМЗ);
		перем node : WMTrees.TreeNode;
		нач
			если horizontal.Get() то
				node := FindHorizontal(x);
			иначе
				node := FindVertical(y);
			всё;
			leftClick := (0 в keys);
			если leftClick и (node # НУЛЬ) и IsSelectable(node) то
				dragObject := GetDragWrapper(node, menu);
				если (dragObject # НУЛЬ) то
					dragPossible := истина;
					dragNode := node;
				всё;
			иначе
				CloseSubMenu(ложь);
			всё;
		кон PointerDown;

		проц {перекрыта}PointerUp*(x, y : размерМЗ; keys : мнвоНаБитахМЗ);
		перем node : WMTrees.TreeNode;
		нач
			если leftClick то
				если horizontal.Get() то
					node := FindHorizontal(x);
				иначе
					node := FindVertical(y);
				всё;
				если (node # НУЛЬ) то
					если IsSelectable(node) то
						SelectNode(node, ложь);
					всё;
				иначе
					CloseSubMenu(ложь);
				всё;
			всё;
			dragPossible := ложь;
		кон PointerUp;

		проц {перекрыта}PointerMove*(x, y : размерМЗ; keys : мнвоНаБитахМЗ);
		перем node : WMTrees.TreeNode;
		нач
			если dragPossible то
				если (матМодуль(x - downX) > DragDist) или (матМодуль(y - downY) > DragDist) то
					dragPossible := ложь;
					если (dragObject # НУЛЬ) то
						leftClick := ложь;
						MyStartDrag(dragNode, dragObject);
					всё;
				всё;
			иначе
				если horizontal.Get() то
					node := FindHorizontal(x);
				иначе
					node := FindVertical(y);
				всё;
				если (node # НУЛЬ) и ~IsSelectable(node) то node := НУЛЬ; всё;
				если (node # hover) то hover := node; Invalidate; всё;
			всё;
		кон PointerMove;

		проц {перекрыта}PointerLeave*;
		нач
			если hover # НУЛЬ то hover := НУЛЬ; Invalidate; всё;
		кон PointerLeave;

		проц MyStartDrag(node : WMTrees.TreeNode; object : динамическиТипизированныйУкль);
		перем
			image, canvasImage : WMGraphics.Image; перем caption : Strings.String;
			canvas : WMGraphics.BufferCanvas;
			width, height, x : размерМЗ;
		нач
			утв((node # НУЛЬ) и (object # НУЛЬ));
			menu.Acquire;
			image := menu.GetNodeImage(node);
			caption := menu.GetNodeCaption(node);
			height := ItemHeight(node);
			menu.Release;
			width := bounds.GetWidth();
			нов(canvasImage); Raster.Create(canvasImage, width, height, Raster.BGRA8888);
			нов(canvas, canvasImage);
			(* actually should factor out node rendering code in DrawBackground and re-use it here... *)
			x := HMenuDistance;
			canvas.Fill(WMRectangles.MakeRect(0, 0, greyBoxWidth, height), LightGreyDrag, WMGraphics.ModeSrcOverDst);
			canvas.Fill(WMRectangles.MakeRect(greyBoxWidth, 0, width, height), WhiteDrag, WMGraphics.ModeSrcOverDst);
			если (image # НУЛЬ) то
				canvas.DrawImage(x, VMenuDistance, image, WMGraphics.ModeSrcOverDst);
				x := x + image.width + HMenuDistance + TextImageDistance;
			всё;
			если (caption # НУЛЬ) то
				canvas.SetColor(WMGraphics.Black);
				WMGraphics.DrawStringInRect(canvas, WMRectangles.MakeRect(x, 0, width, height), ложь, WMGraphics.AlignLeft, WMGraphics.AlignCenter, caption^);
			всё;
			если ~StartDrag(object, canvasImage, 0,0,DragWasAccepted, НУЛЬ) то dragNode := НУЛЬ; dragObject := НУЛЬ; всё;
		кон MyStartDrag;

		проц DragWasAccepted(sender, data : динамическиТипизированныйУкль);
		перем di : WMWindowManager.DragInfo; itf : WMDropTarget.DropInterface; ignoreRes : целМЗ;
		нач
			если (data # НУЛЬ) и (data суть WMWindowManager.DragInfo) то
				di := data(WMWindowManager.DragInfo);
				если (di.data # НУЛЬ) и (di.data суть WMDropTarget.DropTarget) то
					itf := di.data(WMDropTarget.DropTarget).GetInterface(WMDropTarget.TypeObject);
					если (itf # НУЛЬ) и (itf суть WMDropTarget.DropObject) то
						itf(WMDropTarget.DropObject).Set(dragObject, ignoreRes);
					всё;
				всё;
			всё;
			если (rootMenuPanel.parentWindow # НУЛЬ) то
				rootMenuPanel.parentWindow.Close;
			иначе
				rootMenuPanel.CloseSubMenu(ложь);
			всё;
		кон DragWasAccepted;

		проц CursorUp;
		нач
			если horizontal.Get() то
				если (openDirectionI = OpenUpLeft) или (openDirectionI = OpenUpRight) то
					если (hover # НУЛЬ) и HasChildren(hover, menu) то
						SelectNode(hover, истина);
					всё;
				всё;
			иначе
				MoveToPrevious;
			всё;
		кон CursorUp;

		проц CursorDown;
		нач
			если horizontal.Get() то
				если (openDirectionI = OpenDownLeft) или (openDirectionI = OpenDownRight) то
					если (hover # НУЛЬ) и HasChildren(hover, menu) то
						SelectNode(hover, истина);
					всё;
				всё;
			иначе
				MoveToNext;
			всё;
		кон CursorDown;

		проц CursorLeft;
		нач
			если horizontal.Get() то
				MoveToPrevious;
			иначе
				если (openDirectionI = OpenUpLeft) или (openDirectionI = OpenDownLeft) то
					Acquire;
					если (hover # НУЛЬ) и HasChildren(hover, menu) то
						SelectNode(hover, истина);
					всё;
					Release;
				иначе
					если (parentMenuPanel # НУЛЬ) то
						parentMenuPanel.CloseSubMenu(истина);
					всё;
				всё;
			всё;
		кон CursorLeft;

		проц CursorRight;
		нач
			если horizontal.Get() то
				MoveToNext;
			иначе
				если (openDirectionI = OpenUpRight) или (openDirectionI = OpenDownRight) то
					Acquire;
					если (hover # НУЛЬ) и HasChildren(hover, menu) то
						SelectNode(hover, истина);
					всё;
					Release;
				иначе
					если (parentMenuPanel # НУЛЬ) то
						parentMenuPanel.CloseSubMenu(истина);
					всё;
				всё;
			всё;
		кон CursorRight;

		проц MoveToPrevious;
		нач
			Acquire;
			menu.Acquire;
			если (hover # НУЛЬ) то
				hover := menu.GetPrevSibling(hover);
				если (hover = НУЛЬ) то
					hover := menu.GetLastChild(root);
				всё;
			иначе
				hover := menu.GetLastChild(root);
			всё;
			menu.Release;
			Release;
			Invalidate;
		кон MoveToPrevious;

		проц MoveToNext;
		нач
			Acquire;
			menu.Acquire;
			если (hover # НУЛЬ) то
				hover := menu.GetNextSibling(hover);
				если (hover = НУЛЬ) то
					hover := menu.GetChildren(root);
				всё;
			иначе
				hover := menu.GetChildren(root);
			всё;
			menu.Release;
			Release;
			Invalidate;
		кон MoveToNext;

		проц SelectCurrent;
		нач
			Acquire;
			если (hover # НУЛЬ) то
				SelectNode(hover, истина);
			всё;
			Release;
		кон SelectCurrent;

		проц {перекрыта}KeyEvent*(ucs : размерМЗ; flags: мнвоНаБитахМЗ; перем keySym: размерМЗ); (** PROTECTED *)
		перем focusPanel : MenuPanel;
		нач
			утв(IsCallFromSequencer());
			если (Inputs.Release в flags) то возврат; всё;
			focusPanel := rootMenuPanel.focusPanel;
			если (focusPanel # НУЛЬ) то
				если (keySym = Inputs.KsUp) то focusPanel.CursorUp;
				аесли (keySym = Inputs.KsDown) то focusPanel.CursorDown;
				аесли (keySym = Inputs.KsLeft) то focusPanel.CursorLeft;
				аесли (keySym = Inputs.KsRight) то focusPanel.CursorRight;
				аесли (ucs = 20H) или (keySym = Inputs.KsReturn) то focusPanel.SelectCurrent;
				аесли (keySym = Inputs.KsEscape) то
					если (focusPanel.parentMenuPanel # НУЛЬ) то
						focusPanel.parentMenuPanel.CloseSubMenu(истина);
					аесли (focusPanel.parentWindow # НУЛЬ) то
						focusPanel.parentWindow.CloseMenu(НУЛЬ, НУЛЬ);
					всё;
				иначе
				всё;
			всё;
		кон KeyEvent;

		проц {перекрыта}FocusLost*;
		нач
			FocusLost^;
			CloseSubMenu(ложь);
			если (selection # НУЛЬ) или (hover # НУЛЬ) то
				selection := НУЛЬ; hover := НУЛЬ;
				Invalidate;
			всё;
		кон FocusLost;

		проц {перекрыта}Finalize*;
		нач
			Finalize^;
			CloseSubMenu(ложь);
		кон Finalize;

	кон MenuPanel;

тип

	ShadowWindow = окласс(WMWindowManager.Window)
	перем
		type, color : цел32;

		проц &New(type : цел32);
		нач
			утв((type = Right) или (type = Bottom));
			сам.type := type;
			Init(0, 0, истина);
			color := 04FH;
		кон New;

		проц {перекрыта}Draw*(canvas : WMGraphics.Canvas; w, h : размерМЗ; q : цел32);
		нач
			canvas.Fill(WMRectangles.MakeRect(0, 0, w, h), color, WMGraphics.ModeSrcOverDst);
		кон Draw;

	кон ShadowWindow;

тип

	MenuWindow= окласс(WMComponents.FormWindow)
	перем
		menuPanel : MenuPanel;
		takesFocus : булево;

		проц &Open*(x, y : размерМЗ; openDirection : цел32; menu : WMTrees.Tree; root : WMTrees.TreeNode; parent : MenuPanel; takesFocus, indicate : булево);
		перем width, height, dx, dy : размерМЗ; ignore : булево; flags : мнвоНаБитахМЗ;
		нач
			нов(menuPanel);
			menuPanel.openDirection.Set(openDirection);
			menuPanel.SetMenu(menu, root);
			menuPanel.SetParent(parent);
			если (indicate) то
				menu.Acquire;
				menuPanel.hover := menu.GetChildren(root);
				menu.Release;
			всё;
			сам.takesFocus := takesFocus;

			menuPanel.Measure(width, height);
			если (height < 5) то height := 5; всё;
			если (width < 5) то width := 5; всё;

			просей openDirection из
				|OpenUpLeft : dx := -width; dy := -height;
				|OpenUpRight : dy := -height;
				|OpenDownLeft : dx := -width;
			иначе
				dx := 0; dy := 0;
			всё;
			menuPanel.bounds.SetExtents(width, height);
			menuPanel.fillColor.Set(WMGraphics.White);

			Init(menuPanel.bounds.GetWidth(), menuPanel.bounds.GetHeight(), ложь);
			SetContent(menuPanel);
			flags := {WMWindowManager.FlagFrame, WMWindowManager.FlagHidden, WMWindowManager.FlagStayOnTop};
			если ~takesFocus то flags := flags + {WMWindowManager.FlagNoFocus}; всё;
			AddWindow(сам, x + dx, y + dy, flags);
			ignore := manager.TransferPointer(сам);
			manager.SetFocus(сам);
		кон Open;

		проц CloseMenu(sender, data : динамическиТипизированныйУкль);
		нач
			если ~sequencer.IsCallFromSequencer() то
				sequencer.ScheduleEvent(сам.CloseMenu, НУЛЬ, НУЛЬ);
			иначе
				Close;
			всё
		кон CloseMenu;

		проц {перекрыта}FocusLost*;
		нач
			FocusLost^;
			если takesFocus то
				Close;
			всё;
		кон FocusLost;

		проц {перекрыта}Draw*(canvas : WMGraphics.Canvas; w, h : размерМЗ; q : цел32); (** override *)
		нач
			Draw^(canvas, w, h, q);
			WMGraphicUtilities.DrawRect(canvas, WMRectangles.MakeRect(0, 0, w, h), WMGraphics.Black, WMGraphics.ModeCopy);
		кон Draw;

	кон MenuWindow;

перем
	StrMenuPanel : Strings.String;

проц AddWindow(window : WMWindowManager.Window; x, y : размерМЗ; flags : мнвоНаБитахМЗ);
перем
	manager : WMWindowManager.WindowManager;
	view : WMWindowManager.ViewPort;
	oldDecorator : WMWindowManager.Decorator;
нач
	утв(window # НУЛЬ);
	manager := WMWindowManager.GetDefaultManager();
	view := WMWindowManager.GetDefaultView();
	утв((manager # НУЛЬ) и (view # НУЛЬ));
	manager.lock.AcquireWrite;
	oldDecorator := manager.decorate;
	manager.decorate := ShadowDecorator;
	manager.Add((*ENTIER(view.range.l) +*) x, (*ENTIER(view.range.t) + *)y, window, flags);
	manager.decorate := oldDecorator;
	manager.lock.ReleaseWrite;
кон AddWindow;

проц ShadowDecorator(window : WMWindowManager.Window);
перем shadow : ShadowWindow; l, r, t, b : размерМЗ;

	проц InsertAfter(old, new : WMWindowManager.Window);
	нач
		new.next := old.next;
		new.prev := old;
		old.next := new;
		new.next.prev := new
	кон InsertAfter;

	проц InitShadow(shadow : ShadowWindow);
	нач
		shadow.manager := window.manager;
		shadow.flags := {WMWindowManager.FlagNoFocus, WMWindowManager.FlagHidden};
		если WMWindowManager.FlagStayOnBottom в window.flags то включиВоМнвоНаБитах(shadow.flags, WMWindowManager.FlagStayOnBottom); всё;
		если WMWindowManager.FlagNoResizing в window.flags то включиВоМнвоНаБитах(shadow.flags, WMWindowManager.FlagNoResizing); всё;
		если WMWindowManager.FlagNavigation в window.flags то
			shadow.view := window.view;
			включиВоМнвоНаБитах(shadow.flags, WMWindowManager.FlagNavigation);
		всё;
		InsertAfter(window, shadow);

		shadow.manager.AddDecorWindow(window, shadow);
		shadow.manager.AddVisibleDirty(shadow, shadow.bounds);
	кон InitShadow;

нач
	утв((window.manager # НУЛЬ) и (window.manager.lock.HasWriteLock()));
	l := window.bounds.l; r := window.bounds.r; t := window.bounds.t; b := window.bounds.b;

	нов(shadow, Right); window.rightW := shadow;
	shadow.bounds := WMRectangles.MakeRect(r, t + ShadowOffsetVertical, r + ShadowWidth, b + ShadowHeight);
	InitShadow(shadow);

	нов(shadow, Bottom); window.bottomW := shadow;
	shadow.bounds := WMRectangles.MakeRect(l + ShadowOffsetHorizontal, b, r, b + ShadowHeight);
	InitShadow(shadow);
кон ShadowDecorator;

проц HasChildren(parent : WMTrees.TreeNode; tree : WMTrees.Tree) : булево;
перем hasChildren : булево;
нач
	утв(tree # НУЛЬ);
	если (parent # НУЛЬ) то
		tree.Acquire;
		hasChildren := tree.GetChildren(parent) # НУЛЬ;
		tree.Release;
	иначе
		hasChildren := ложь;
	всё;
	возврат hasChildren;
кон HasChildren;

проц GetCaption*(data : динамическиТипизированныйУкль; menu : WMTrees.Tree) : Strings.String;
перем caption : Strings.String;
нач
	утв(menu # НУЛЬ);
	если (data # НУЛЬ) и (data суть WMTrees.TreeNode) то
		menu.Acquire;
		caption := menu.GetNodeCaption(data(WMTrees.TreeNode));
		menu.Release;
	иначе
		caption := НУЛЬ;
	всё;
	возврат caption;
кон GetCaption;

проц GetDragWrapper*(node : WMTrees.TreeNode; menu : WMTrees.Tree) : DragWrapper;
перем data: динамическиТипизированныйУкль; drag : DragWrapper;
нач
	утв(menu # НУЛЬ);
	drag := НУЛЬ;
	если (node # НУЛЬ) то
		menu.Acquire;
		data := menu.GetNodeData(node);
		menu.Release;
		если (data # НУЛЬ) и (data суть DragWrapper) то
			drag := data(DragWrapper);
		всё;
	всё;
	возврат drag;
кон GetDragWrapper;

проц FindChild(конст caption : массив из симв8; parent : WMTrees.TreeNode; tree : WMTrees.Tree) : WMTrees.TreeNode;
перем child : WMTrees.TreeNode; string : Strings.String; found : булево;
нач
	утв((parent # НУЛЬ) и (tree # НУЛЬ) и (tree.HasLock()));
	found := ложь;
	child := tree.GetChildren(parent);
	нцПока (child # НУЛЬ) и ~found делай
		string := tree.GetNodeCaption(child);
		found := (string # НУЛЬ) и (string^ = caption);
		если ~found то
			child := tree.GetNextSibling(child);
		всё;
	кц;
	возврат child;
кон FindChild;

проц AddChild*(конст caption : массив из симв8; parent : WMTrees.TreeNode; tree : WMTrees.Tree) : WMTrees.TreeNode;
перем node : WMTrees.TreeNode; separator : Separator;
нач
	утв((parent # НУЛЬ) и (tree # НУЛЬ) и (tree.HasLock()));
	если (caption # SeparatorCaption) то
		нов(node);
		tree.SetNodeCaption(node, Strings.NewString(caption));
	иначе
		нов(separator);
		node := separator;
	всё;
	tree.AddChildNode(parent, node);
	возврат node;
кон AddChild;

проц Find*(конст path : массив из симв8; menu : WMTrees.Tree) : WMTrees.TreeNode;
перем caption : массив 256 из симв8; child, node, parent : WMTrees.TreeNode; i, j : цел32;
нач
	утв(menu # НУЛЬ);
	node := НУЛЬ;
	menu.Acquire;
	parent := menu.GetRoot();
	если (parent # НУЛЬ) то
		caption := "";
		i := 0; j := 0;
		нц
			если (i >= длинаМассива(path)) то
				прервиЦикл;
			аесли (path[i] = ".") или (path[i] = 0X) то
				caption[j] := 0X;
				child := FindChild(caption, parent, menu);
				если (child = НУЛЬ) то
					прервиЦикл;
				всё;
				parent := child;
				если (path[i] = 0X) то
					node := child;
					прервиЦикл;
				иначе
					caption := ""; j := 0;
				всё;
			аесли (j < длинаМассива(caption) - 1) то
				caption[j] := path[i];
				увел(j);
			всё;
			увел(i);
		кц;
	всё;
	menu.Release;
	возврат node;
кон Find;

проц AddItemNode*(конст path : массив из симв8; menu : WMTrees.Tree) : WMTrees.TreeNode;
перем caption : массив 256 из симв8; node, parent : WMTrees.TreeNode; i, j : цел32;
нач
	утв(menu # НУЛЬ);
	menu.Acquire;
	если (menu.GetRoot() = НУЛЬ) то
		нов(node); menu.SetRoot(node)
	всё;
	i := 0; j := 0;
	caption := ""; parent := menu.GetRoot();
	нц
		если (i >= длинаМассива(path)) то
			прервиЦикл;
		аесли (path[i] = ".") или (path[i] = 0X) то
			caption[j] := 0X;
			node := FindChild(caption, parent, menu);
			если (node = НУЛЬ) то
				node := AddChild(caption, parent, menu);
			всё;
			parent := node;
			caption := ""; j := 0;
			если (path[i] = 0X) то прервиЦикл; всё;
		аесли (j < длинаМассива(caption) - 1) то
			caption[j] := path[i];
			увел(j);
		всё;
		увел(i);
	кц;
	menu.Release;
	утв(node # НУЛЬ);
	возврат node;
кон AddItemNode;

проц AddItem*(конст path : массив из симв8; menu : WMTrees.Tree);
перем ignore : WMTrees.TreeNode;
нач
	ignore := AddItemNode(path, menu);
кон AddItem;

проц Show*(menu : WMTrees.Tree; x, y : размерМЗ; handler : WMEvents.EventListener);
перем window : MenuWindow; root : WMTrees.TreeNode;
нач
	утв((menu # НУЛЬ) и (handler # НУЛЬ));
	menu.Acquire;
	root := menu.GetRoot();
	menu.Release;
	если (root # НУЛЬ) то
		нов(window, x, y, OpenDefault, menu, root, НУЛЬ, истина, ложь);
		window.menuPanel.SetParentWindow(window);
		window.menuPanel.onSelect.Add(handler);
	всё;
кон Show;

нач
	StrMenuPanel := Strings.NewString("MenuPanel");
кон WMMenus.

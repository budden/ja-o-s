(**
	AUTHOR: Timothee Martiel, Alexey Morozov
	PURPOSE: A2 Serials interface for Zynq PS UARTs
*)
модуль PsSerials;

использует Platform, BootConfig, Modules, Строки8, PsUartMin, PsUart, Serials, Objects, ЭВМ;

тип
	Port = окласс (Serials.Port)
	перем
		uart: PsUart.UartController;

		recvLocked := ложь : булево;
		sendLocked := ложь : булево;

		проц & Init (id: PsUart.UartId);
		нач
			uart := PsUart.GetUart(id);
			утв(uart # НУЛЬ);
		кон Init;

		проц {перекрыта}Open (bps, data, parity, stop : цел32; перем res: целМЗ);
		нач{единолично}
			PsUart.Open(uart, bps, data, parity, stop, res);
		кон Open;

		проц {перекрыта}Закрой;
		нач{единолично}
			PsUart.Close(uart);
		кон Закрой;

		проц BusyLoopCallback(перем res: целМЗ): булево;
		нач
			если uart.open то
				Objects.Yield;
				возврат истина;
			иначе
				res := Serials.Closed;
				возврат ложь;
			всё;
		кон BusyLoopCallback;

		проц BusyLoopCallback0(перем res: целМЗ): булево;
		нач
			если uart.open то
				возврат истина;
			иначе
				res := Serials.Closed;
				возврат ложь;
			всё;
		кон BusyLoopCallback0;

		(* This method can be safely used for trace output *)
		проц {перекрыта}SendChar(char: симв8; перем res: целМЗ);
		нач
			(*! use BusyLoopCallback0 method to make sure no low-level lock is acquired here - required when used as trace output *)
			PsUart.SendChar(uart, char, истина, BusyLoopCallback0, res);
		кон SendChar;

		(*! This method must not be used for trace output - danger of a dead lock! *)
		проц {перекрыта}ЗапишиВПоток(конст buf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);
		нач
			ЭВМ.ЗапросиБлокировкуАктивногоОбъекта(sendLocked);
			PsUart.Send(uart, buf, ofs, len, propagate, BusyLoopCallback, res);
		выходя
			ЭВМ.ОтпустиБлокировкуАктивногоОбъекта(sendLocked);
		кон ЗапишиВПоток;

		проц {перекрыта}ReceiveChar(перем char: симв8; перем res: целМЗ);
		нач
			ЭВМ.ЗапросиБлокировкуАктивногоОбъекта(recvLocked);
			char := PsUart.ReceiveChar(uart, BusyLoopCallback, res);
		выходя
			ЭВМ.ОтпустиБлокировкуАктивногоОбъекта(recvLocked);
		кон ReceiveChar;

		проц {перекрыта}ПрочтиИзПотока(перем buf: массив из симв8; ofs, size, min: размерМЗ; перем len: размерМЗ; перем res: целМЗ);
		нач
			ЭВМ.ЗапросиБлокировкуАктивногоОбъекта(recvLocked);
			PsUart.Receive(uart, buf, ofs, size, min, len, BusyLoopCallback, res);
		выходя
			ЭВМ.ОтпустиБлокировкуАктивногоОбъекта(recvLocked);
		кон ПрочтиИзПотока;

		проц {перекрыта}Available(): размерМЗ;
		нач
			возврат PsUart.Available(uart);
		кон Available;

		проц {перекрыта}GetPortState(перем openstat : булево; перем bps, data, parity, stop : цел32);
		нач{единолично}
			openstat := uart.open;
			bps := uart.bps;
			data := uart.data;
			parity := uart.parity;
			stop := uart.stop;
		кон GetPortState;

	кон Port;

перем
	ports: массив 2 из Port;

	проц Init;
	перем
		i: размерМЗ;
		clk: PsUart.ClockFrequency; res: целМЗ;
		name, desc: массив 32 из симв8;
		tracePort: размерМЗ;
	нач
		clk := BootConfig.GetIntValue("UartInputClockHz");
		нцДля i := 0 до длинаМассива(Platform.UartBase,0)-1 делай
			PsUart.Install(i, Platform.UartBase[i], clk, res);
		кц;

		(* if one of PS UART's is used for tracing, make the corresponding port open to allow trace output via PsUartMin *)
		tracePort := BootConfig.GetIntValue("TracePort");
		если (tracePort >= 1) и (tracePort <= длинаМассива(Platform.UartBase,0)) то
			PsUart.Open(PsUart.GetUart(tracePort-1), BootConfig.GetIntValue("TraceBPS"),PsUartMin.DefaultDataBits,PsUartMin.DefaultParity,PsUartMin.DefaultStop, res);
		иначе tracePort := -1;
		всё;

		нцДля i := 0 до длинаМассива(ports) - 1 делай
			нов(ports[i], i);
			name := "UART";
			Строки8.ПодклейВСтрокуРаспечаткуЦел64(name, i+1);
			desc := "Zynq PS UART";
			Строки8.ПодклейВСтрокуРаспечаткуЦел64(desc, i);
			Serials.RegisterOnboardPort(i+1, ports[i], name, desc);
		кц;

		если tracePort >= 0 то
			(* switch from PsUartMin to port-based trace output *)
			Serials.SetTracePort(tracePort, BootConfig.GetIntValue("TraceBPS"),PsUartMin.DefaultDataBits,PsUartMin.DefaultParity,PsUartMin.DefaultStop, res);
		всё;
		Modules.InstallTermHandler(Cleanup)
	кон Init;

	проц Cleanup;
	перем i: размерМЗ;
	нач
		нцДля i := 0 до длинаМассива(ports) - 1 делай
			если ports[i] # НУЛЬ то Serials.UnRegisterPort(ports[i]) всё
		кц
	кон Cleanup;

нач
	Init
кон PsSerials.

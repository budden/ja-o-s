(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль Data;   (** AUTHOR "adf"; PURPOSE "Sortable data objects (via a key)"; *)

использует NbrInt64, NbrInt, DataErrors, DataIO;

конст
	(*  Version number used when reading/writing an instance of Datum to file. *)
	VERSION* = 1;
	(* The maximum that base can be is 78 and still retain a 10 character string when based on a 64-bit integer. *)
	BASE = 78;
	(** Maximum number of characters allowed in a Word. *)
	CHARACTERS* = 10;

тип
	Key* = запись
		k: NbrInt64.Integer
	кон;

	(** Class Datum has been DataIO registered, and therefore, any instance of it can be made persistent
		by using the DataIO Reader and Writer, or more simply, by calling procedures Load and Store below. *)

	Datum* = окласс
	перем key: Key;

		(** Sets the key to its sentinel value. *)
		проц & Initialize*;
		нач
			key.k := Sentinel.k
		кон Initialize;

	(** Returns a deep copy of the data; shallow copies are obtained via assignment, i.e., :=. *)
		проц Copy*( перем copy: Datum );
		нач
			если copy = НУЛЬ то нов( copy ) всё;
			copy.key := сам.key
		кон Copy;

	(** Used internally to read data from a file. *)
		проц Read*( R: DataIO.Reader );
		нач {единолично}
			NbrInt64.Load( R, key.k )
		кон Read;

	(** Used internally to write data to a file. *)
		проц Write*( W: DataIO.Writer );
		нач
			NbrInt64.Store( W, key.k )
		кон Write;

	(** Obtain the key held by an instance of Datum. *)
		проц GetKey*( перем k: Key );
		нач
			k.k := key.k
		кон GetKey;

	(** A datum's key can only be changed when its key = Sentinel.  Once set, it cannot be reset. *)
		проц SetKey*( k: Key );
		нач
			если key.k = Sentinel.k то
				нач {единолично}
					key.k := k.k
				кон
			всё
		кон SetKey;

	кон Datum;


	(** A word is case sensitive.  Admissible characters include:
						word		=	char { char }.
						char			=	letter | digit | punctuation | arithmetic | alien.
						letter		=	"A" ... "Z" | "a" ... "z".
						digit		=	"0" ... "9".
						punctuation	=	" " | "." | ":" | ";" | "," | "_" | "~" | " ' " | ' " '.
						arithmetic	=	"+" | "-" | "*" | "/" | "^" | "=".
						alien		=	"?".
			Yes, white space is an admissible character.  However, leading and trailing white space is ignored.
			All unknown characters are assigned the alien glyph "?". *)
	Word* = массив CHARACTERS + 1 из симв8;

перем
	intToChar: укль на массив из симв8;
	charToInt: укль на массив из цел32;
	(** The initial key assigned to a Data.Datum.  It is the only key that can be overwritten via SetKey. *)
	Sentinel-: Key;


	(** Assignment operators for Keys. *)
	операция ":="*( перем l: Key;  r: NbrInt.Integer );
	нач
		l.k := r
	кон ":=";

	операция ":="*( перем l: Key;  r: NbrInt64.Integer );
	нач
		l.k := r
	кон ":=";

(** Comparison Operators between Keys. *)
	операция "="*( l, r: Key ): булево;
	нач
		возврат l.k = r.k
	кон "=";

	операция "#"*( l, r: Key ): булево;
	нач
		возврат l.k # r.k
	кон "#";

	операция "<"*( l, r: Key ): булево;
	нач
		возврат l.k < r.k
	кон "<";

	операция ">"*( l, r: Key ): булево;
	нач
		возврат l.k > r.k
	кон ">";

	операция "<="*( l, r: Key ): булево;
	нач
		возврат l.k <= r.k
	кон "<=";

	операция ">="*( l, r: Key ): булево;
	нач
		возврат l.k >= r.k
	кон ">=";

(** Comparison Operators between Data. *)
	операция "="*( l, r: Datum ): булево;
	нач
		возврат l.key = r.key
	кон "=";

	операция "#"*( l, r: Datum ): булево;
	нач
		возврат l.key # r.key
	кон "#";

	операция "<"*( l, r: Datum ): булево;
	нач
		возврат l.key < r.key
	кон "<";

	операция ">"*( l, r: Datum ): булево;
	нач
		возврат l.key > r.key
	кон ">";

	операция "<="*( l, r: Datum ): булево;
	нач
		возврат l.key <= r.key
	кон "<=";

	операция ">="*( l, r: Datum ): булево;
	нач
		возврат l.key >= r.key
	кон ">=";

(** Conversion procedure. *)
	проц KeyToInt64*( key: Key;  перем x: NbrInt64.Integer );
	нач
		x := key.k
	кон KeyToInt64;

(** Instead of using integers for sorting against, sometimes it is useful to use words for this purpose,
	like entries in a dictionary.  KetToWord  and  WordToKey  are mappings that allow for this.  *)
	проц KeyToWord*( key: Key;  перем word: Word );
	перем i, k, len: NbrInt.Integer;  base1, base2, base3, base4, base5, base6, base7, base8, base9: NbrInt64.Integer;
		int: массив CHARACTERS из NbrInt64.Integer;
		string: Word;
	нач
		(* Extract the string of integers from the single compressed integer. *)
		base1 := BASE;  base2 := base1 * base1;  base3 := base2 * base1;  base4 := base3 * base1;  base5 := base4 * base1;
		base6 := base5 * base1;  base7 := base6 * base1;  base8 := base7 * base1;  base9 := base8 * base1;
		(* The following algorithm is for CHARACTERS = 10. *)
		int[0] := key.k DIV base9;  int[1] := (key.k DIV base8) остОтДеленияНа base1;  int[2] := (key.k DIV base7) остОтДеленияНа base1;
		int[3] := (key.k DIV base6) остОтДеленияНа base1;  int[4] := (key.k DIV base5) остОтДеленияНа base1;  int[5] := (key.k DIV base4) остОтДеленияНа base1;
		int[6] := (key.k DIV base3) остОтДеленияНа base1;  int[7] := (key.k DIV base2) остОтДеленияНа base1;  int[8] := (key.k DIV base1) остОтДеленияНа base1;
		int[9] := key.k остОтДеленияНа base1;
		(* Convert this integer string into a character string. *)
		нцДля i := 0 до CHARACTERS - 1 делай k := NbrInt64.Short( int[i] );  string[i] := intToChar[k] кц;
		string[CHARACTERS] := 0X;
		(* Remove trailing white space recreating the compressed string. *)
		len := CHARACTERS;
		нцПока (len > 0) и (string[len - 1] = 20X) делай умень( len ) кц;
		нцДля i := 0 до len - 1 делай word[i] := string[i] кц;
		word[len] := 0X
	кон KeyToWord;

	проц WordToKey*( word: Word;  перем key: Key );
	перем i, k, len: NbrInt.Integer;
		int: массив CHARACTERS из NbrInt.Integer;
		string: Word;
	нач
		len := -1;
		нцДо увел( len )
		кцПри word[len] = 0X;
		(* Remove leading white space. *)
		k := 0;
		нцПока word[k] = 20X делай увел( k ) кц;
		(* Convert the passed string into a local string. *)
		нцДля i := 0 до len - k - 1 делай string[i] := word[i + k] кц;
		(* Add trailing white space, as required, to the local string. *)
		нцДля i := len - k до CHARACTERS - 1 делай string[i] := 20X кц;
		string[CHARACTERS] := 0X;
		(* Convert each character in the local string to a corresponding integer. *)
		нцДля i := 0 до CHARACTERS - 1 делай int[i] := charToInt[кодСимв8( string[i] )] кц;
		(* Compress this integer string into a single integer. *)
		key.k := 0;
		нцДля i := 0 до CHARACTERS - 2 делай key.k := (key.k + int[i]) * BASE кц;
		key.k := key.k + int[CHARACTERS - 1]
	кон WordToKey;

(* Create the local variables  charToInt  and  intToChar.  These procedures are courtesy of Patrik Reali. *)
	проц MakeCharToInt;
	перем i: NbrInt.Integer;
	нач
		нцДля i := 0 до 255 делай
			charToInt[i] := BASE - 1 (* Default - set all entries to the alien's value. *)
		кц;
		charToInt[20H] := 0;   (* white space *)
		charToInt[кодСимв8( "." )] := 1;  charToInt[кодСимв8( ":" )] := 2;  charToInt[кодСимв8( ";" )] := 3;  charToInt[кодСимв8( "," )] := 4;  charToInt[кодСимв8( "_" )] := 5;
		charToInt[кодСимв8( "~" )] := 6;  charToInt[кодСимв8( "'" )] := 7;  charToInt[кодСимв8( '"' )] := 8;  charToInt[кодСимв8( "+" )] := 9;  charToInt[кодСимв8( "-" )] := 10;
		charToInt[кодСимв8( "*" )] := 11;  charToInt[кодСимв8( "/" )] := 12;  charToInt[кодСимв8( "^" )] := 13;  charToInt[кодСимв8( "=" )] := 14;  charToInt[кодСимв8( "0" )] := 15;
		charToInt[кодСимв8( "1" )] := 16;  charToInt[кодСимв8( "2" )] := 17;  charToInt[кодСимв8( "3" )] := 18;  charToInt[кодСимв8( "4" )] := 19;  charToInt[кодСимв8( "5" )] := 20;
		charToInt[кодСимв8( "6" )] := 21;  charToInt[кодСимв8( "7" )] := 22;  charToInt[кодСимв8( "8" )] := 23;  charToInt[кодСимв8( "9" )] := 24;  charToInt[кодСимв8( "A" )] := 25;
		charToInt[кодСимв8( "a" )] := 26;  charToInt[кодСимв8( "B" )] := 27;  charToInt[кодСимв8( "b" )] := 28;  charToInt[кодСимв8( "C" )] := 29;  charToInt[кодСимв8( "c" )] := 30;
		charToInt[кодСимв8( "D" )] := 31;  charToInt[кодСимв8( "d" )] := 32;  charToInt[кодСимв8( "E" )] := 33;  charToInt[кодСимв8( "e" )] := 34;  charToInt[кодСимв8( "F" )] := 35;
		charToInt[кодСимв8( "f" )] := 36;  charToInt[кодСимв8( "G" )] := 37;  charToInt[кодСимв8( "g" )] := 38;  charToInt[кодСимв8( "H" )] := 39;  charToInt[кодСимв8( "h" )] := 40;
		charToInt[кодСимв8( "I" )] := 41;  charToInt[кодСимв8( "i" )] := 42;  charToInt[кодСимв8( "J" )] := 43;  charToInt[кодСимв8( "j" )] := 44;  charToInt[кодСимв8( "K" )] := 45;
		charToInt[кодСимв8( "k" )] := 46;  charToInt[кодСимв8( "L" )] := 47;  charToInt[кодСимв8( "l" )] := 48;  charToInt[кодСимв8( "M" )] := 49;  charToInt[кодСимв8( "m" )] := 50;
		charToInt[кодСимв8( "N" )] := 51;  charToInt[кодСимв8( "n" )] := 52;  charToInt[кодСимв8( "O" )] := 53;  charToInt[кодСимв8( "o" )] := 54;  charToInt[кодСимв8( "P" )] := 55;
		charToInt[кодСимв8( "p" )] := 56;  charToInt[кодСимв8( "Q" )] := 57;  charToInt[кодСимв8( "q" )] := 58;  charToInt[кодСимв8( "R" )] := 59;  charToInt[кодСимв8( "r" )] := 60;
		charToInt[кодСимв8( "S" )] := 61;  charToInt[кодСимв8( "s" )] := 62;  charToInt[кодСимв8( "T" )] := 63;  charToInt[кодСимв8( "t" )] := 64;  charToInt[кодСимв8( "U" )] := 65;
		charToInt[кодСимв8( "u" )] := 66;  charToInt[кодСимв8( "V" )] := 67;  charToInt[кодСимв8( "v" )] := 68;  charToInt[кодСимв8( "W" )] := 69;  charToInt[кодСимв8( "w" )] := 70;
		charToInt[кодСимв8( "X" )] := 71;  charToInt[кодСимв8( "x" )] := 72;  charToInt[кодСимв8( "Y" )] := 73;  charToInt[кодСимв8( "y" )] := 74;  charToInt[кодСимв8( "Z" )] := 75;
		charToInt[кодСимв8( "z" )] := 76;
		(* Assign the alien character. *)
		charToInt[кодСимв8( "?" )] := 77
	кон MakeCharToInt;

	проц MakeIntToChar;
	перем i, k: NbrInt.Integer;
	нач
		(* Assigns all elements the alien character. *)
		нцДля i := 0 до BASE - 1 делай intToChar[i] := "?" кц;
		(* Overwrite with the correct character. *)
		нцДля i := 0 до 255 делай k := charToInt[i];  intToChar[k] := симв8ИзКода( i ) кц
	кон MakeIntToChar;

(* The procedures needed to register type Datum so that its instances can be made persistent. *)
	проц LoadObj( R: DataIO.Reader;  перем obj: окласс );
	перем version: цел8;  ver: NbrInt.Integer;  new: Datum;
	нач
		R.чЦел8_мз( version );
		если version = -1 то
			obj := НУЛЬ  (* Version tag is -1 for NIL. *)
		иначе
			если version = VERSION то нов( new );  new.Read( R );  obj := new
					иначе  (* Encountered an unknown version number. *)
				ver := version;  DataErrors.IntError( ver, "Alien version number encountered." );  СТОП( 1000 )
			всё
		всё
	кон LoadObj;

	проц StoreObj( W: DataIO.Writer;  obj: окласс );
	перем old: Datum;
	нач
		если obj = НУЛЬ то W.пЦел8_мз( -1 ) иначе W.пЦел8_мз( VERSION );  old := obj( Datum );  old.Write( W ) всё
	кон StoreObj;

	проц Register;
	перем anInstanceOf: Datum;
	нач
		нов( anInstanceOf );  DataIO.PlugIn( anInstanceOf, LoadObj, StoreObj )
	кон Register;

(** Load and Store are procedures for external use that read/write an instance of Datum from/to a file. *)
	проц Load*( R: DataIO.Reader;  перем obj: Datum );
	перем ptr: окласс;
	нач
		R.Object( ptr );  obj := ptr( Datum )
	кон Load;

	проц Store*( W: DataIO.Writer;  obj: Datum );
	нач
		W.Object( obj )
	кон Store;

нач
	Sentinel.k := NbrInt64.MinNbr;  нов( charToInt, 256 );  MakeCharToInt;  нов( intToChar, BASE );  MakeIntToChar;  Register
кон Data.

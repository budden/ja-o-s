модуль WebBimbodot;

использует
	Dates, Строки8, Modules, Kernel,
	XML, XMLObjects, XMLScanner, XMLParser,

	Потоки, ЛогЯдра, Archives, Files,
	WebHTTP, WebCGI, HTTPSupport, Commands;

конст
	MaxAuthor = 16;
	ArticleListFile = "BimboArticles.txt";
	CategoryFile = "BimbodotCategories.txt";
	AuthorDataFile = "BimbodotAuthors.txt";
	BimbodotConfigFile = "BimbodotConfig.dat";


тип
	String = Строки8.уСтрока;

	HTMLWriter= окласс
	перем w* : Потоки.Писарь;

		проц &New*(w : Потоки.Писарь);
		нач сам.w := w;
		кон New;

		проц Head*(конст title : массив из симв8);
		нач
			w.пСтроку8('<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>');
			w.пСтроку8(title);
			w.пСтроку8("</title></head>");
			w.пСтроку8("<body>");
		кон Head;

		проц Br*;
		нач
			w.пСтроку8("<br/>");
		кон Br;

		проц Nbsp*;
		нач
			w.пСтроку8("&nbsp;");
		кон Nbsp;

		проц InputText*(конст name : массив из симв8; value : String);
		нач
			w.пСтроку8('<input type="text" name="'); w.пСтроку8(name);	w.пСтроку8('" ');
			если value # НУЛЬ то w.пСтроку8('value="'); HTMLString(value^); w.пСтроку8('" ') всё;
			w.пСтроку8('/>');
		кон InputText;

		проц Hide*(конст name, value : массив из симв8);
		нач
			w.пСтроку8('<input type="hidden" name="'); w.пСтроку8(name);	w.пСтроку8('" ');
			w.пСтроку8('value="'); HTMLString(value); w.пСтроку8('" ');
			w.пСтроку8('/>');
		кон Hide;

		проц BeginOptionField*(конст name, value: массив из симв8);
		нач
			w.пСтроку8('<select  name="'); w.пСтроку8(name); w.пСтроку8('" ');
			если value # "" то w.пСтроку8(' value="'); w.пСтроку8(value); w.пСтроку8('"')	всё;
			w.пСтроку8('>');
		кон BeginOptionField;

		проц Option*(конст text : массив из симв8);
		нач
			w.пСтроку8('<option>'); HTMLString(text); w.пСтроку8('</option>');
		кон Option;

		проц EndOptionField*;
		нач
			w.пСтроку8('</select>');
		кон EndOptionField;


		проц Submit(конст text : массив из симв8);
		нач
			w.пСтроку8('<input type="submit" value="');
			w.пСтроку8(text);
			w.пСтроку8('" />');
		кон Submit;

		проц InputArea*(конст name : массив из симв8; value : String);
		нач
			w.пСтроку8('<textarea cols="80" rows="10" name="'); w.пСтроку8(name);	w.пСтроку8('"> ');
			если value # НУЛЬ то TAHTMLString(value^); всё;
			w.пСтроку8('</textarea>');
		кон InputArea;

		проц TextLink*(конст text, target : массив из симв8);
		нач
			w.пСтроку8('<a href="'); w.пСтроку8(target); w.пСтроку8('">'); w.пСтроку8(text); w.пСтроку8("</a>")
		кон TextLink;

		проц Tail*;
		нач
			w.пСтроку8("</body></html>");
		кон Tail;

		проц TAHTMLString(конст s : массив из симв8);
		перем i : цел32;
		нач
			i := 0;
			нцПока s[i] # 0X делай
				просей s[i] из
					|"<" : w.пСтроку8("&lt;");
					|">" : w.пСтроку8("&gt;");
					|"&" : w.пСтроку8("&amp;");
					|'"' : w.пСтроку8("&quot;");
				иначе w.пСимв8(s[i])
				всё;
				увел(i)
			кц
		кон TAHTMLString;

		проц HTMLString(конст s : массив из симв8);
		перем i : цел32;
		нач
			i := 0;
			нцПока s[i] # 0X делай
				просей s[i] из
					|"<" : w.пСтроку8("&lt;");
					|">" : w.пСтроку8("&gt;");
					|"&" : w.пСтроку8("&amp;");
					|'"' : w.пСтроку8("&quot;");
					|0DX : w.пСтроку8("<br/>");
				иначе w.пСимв8(s[i])
				всё;
				увел(i)
			кц
		кон HTMLString;

		проц URIString(конст s : массив из симв8);
		перем i : цел32;
		нач
			i := 0;
			нцПока s[i] # 0X делай
				если uriLiteral[кодСимв8(s[i])] то w.пСимв8(s[i])
				иначе w.пСимв8("%"); w.п16ричное(кодСимв8(s[i]), -2)
				всё;
				увел(i)
			кц
		кон URIString;


	кон HTMLWriter;

	Version = окласс
	перем title, department, category, author, email, posterUID, date, text, cache  : String;
		articleID : String;
		authorUID : цел32;
		errors : булево;

		проц &New*;
		нач
			title := empty; department := empty; category := empty; author := empty; email := empty; posterUID := empty; date := empty; text := empty; cache := НУЛЬ;
		кон New;

		проц Store(w : Потоки.Писарь);
		перем d : XML.Document;
			r, e : XML.Element;
			c : XML.CDataSect;
		нач
			нов(d);
			нов(r); r.SetName("Version"); d.AddContent(r);

			нов(e); e.SetName("Title"); r.AddContent(e);
			нов(c); c.SetStr(title^); e.AddContent(c);

			нов(e); e.SetName("Department"); r.AddContent(e);
			нов(c); c.SetStr(department^); e.AddContent(c);

			нов(e); e.SetName("PosterUID"); r.AddContent(e);
			нов(c); c.SetStr(posterUID^); e.AddContent(c);

			нов(e); e.SetName("Email"); r.AddContent(e);
			нов(c); c.SetStr(email^); e.AddContent(c);

			нов(e); e.SetName("Author"); r.AddContent(e);
			нов(c); c.SetStr(author^); e.AddContent(c);

			нов(e); e.SetName("Category"); r.AddContent(e);
			нов(c); c.SetStr(category^); e.AddContent(c);

			нов(e); e.SetName("Date"); r.AddContent(e);
			нов(c); c.SetStr(date^); e.AddContent(c);

			нов(e); e.SetName("Text"); r.AddContent(e);
			нов(c); c.SetStr(text^); e.AddContent(c);

			d.Write(w, НУЛЬ, 0);
			w.ПротолкниБуферВПоток;
		кон Store;

		проц Fail(pos, line, row: Потоки.ТипМестоВПотоке; конст msg: массив из симв8);
		нач
			errors := истина;
			ЛогЯдра.пСтроку8("Version load failed : "); ЛогЯдра.пСтроку8("pos= "); ЛогЯдра.пЦел64(pos, 0); ЛогЯдра.пСтроку8("msg= "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС;
		кон Fail;

		проц Load(r : Потоки.Чтец) : булево;
		перем s : XMLScanner.Scanner;
			p : XMLParser.Parser;
			d : XML.Document;
			enum: XMLObjects.Enumerator; obj : динамическиТипизированныйУкль;
			root, e: XML.Element; str : String;

			проц GetCDataContent(e : XML.Element) : String;
			перем en : XMLObjects.Enumerator;
				p : динамическиТипизированныйУкль;
			нач
				en := e.GetContents();
				p := en.GetNext();
				если p # НУЛЬ то
					если p суть XML.CDataSect то
						возврат p(XML.CDataSect).GetStr()
					всё
				всё;
				возврат НУЛЬ
			кон GetCDataContent;

		нач
			нов(s, r); нов(p, s); p.reportError := Fail;
			errors := ложь;
			d := p.Parse();
			если errors то возврат ложь всё;

			root := d.GetRoot();

			enum := root.GetContents();
			нцПока enum.HasMoreElements() делай
				obj := enum.GetNext();
				если obj суть XML.Element то
					e := obj(XML.Element); str := e.GetName();
					если str^ = "Title" то title := GetCDataContent(e) всё;
					если str^ = "Department" то department := GetCDataContent(e) всё;
					если str^ = "Author" то author := GetCDataContent(e) всё;
					если str^ = "Email" то email := GetCDataContent(e) всё;
					если str^ = "PosterUID" то posterUID := GetCDataContent(e) всё;
					если str^ = "Category" то category := GetCDataContent(e) всё;
					если str^ = "Date" то date:= GetCDataContent(e) всё;
					если str^ = "Text" то text := GetCDataContent(e) всё;
				всё
			кц;
			возврат истина
		кон Load;

	кон Version;

	Article* = окласс
	перем uid, path, filename, stateFileName : String;
		nofVersions : цел32;
		current : Version;
		archive : Archives.Archive;
		site : Archive;


		проц Load(конст path, uid : массив из симв8) : булево;
		перем fn : массив 1024 из симв8;
		нач
			сам.uid := Строки8.ЯвиУСтроку(uid);
			сам.path := Строки8.ЯвиУСтроку(path);
			копируйСтрокуДо0(path, fn); Строки8.ПодклейВСтрокуХвост(fn, uid); Строки8.ПодклейВСтрокуХвост(fn, ".tar");
			сам.filename := Строки8.ЯвиУСтроку(fn);
			archive := Archives.Old(filename^, "tar");
			если archive = НУЛЬ то возврат ложь всё;

			копируйСтрокуДо0(path, fn); Строки8.ПодклейВСтрокуХвост(fn, uid); Строки8.ПодклейВСтрокуХвост(fn, ".art");
			stateFileName := Строки8.ЯвиУСтроку(fn);
			LoadState;

			current := GetVersion(nofVersions - 1);
			возврат истина
		кон Load;

		проц Create(конст path, uid : массив из симв8; version : Version);
		перем fn : массив 1024 из симв8;
		нач
			nofVersions := 0;
			сам.uid := Строки8.ЯвиУСтроку(uid);
			сам.path := Строки8.ЯвиУСтроку(path);
			копируйСтрокуДо0(path, fn); Строки8.ПодклейВСтрокуХвост(fn, uid); Строки8.ПодклейВСтрокуХвост(fn, ".tar");
			сам.filename := Строки8.ЯвиУСтроку(fn);
			archive := Archives.New(filename^, "tar");

			копируйСтрокуДо0(path, fn); Строки8.ПодклейВСтрокуХвост(fn, uid); Строки8.ПодклейВСтрокуХвост(fn, ".art");
			stateFileName := Строки8.ЯвиУСтроку(fn);
			InternalAdd(version)
		кон Create;


		(* count the number of article versions in the archive file *)
		проц CountVersions;
		перем index : Archives.Index;
			i: размерМЗ; count : цел32;
			n : String;
		нач
			archive.Acquire;
			index := archive.GetIndex();
			count := 0;
			нцДля i := 0 до длинаМассива(index) - 1 делай
				n := index[i].GetName();
				если n[0] = "V" то увел(count) всё
			кц;
			archive.Release;
			nofVersions := count
		кон CountVersions;

		(* load the info about the number of article version from a cache file *)
		проц LoadState;
		перем f : Files.File;
			r : Files.Reader;
		нач
			f := Files.Old(stateFileName^);
			если f = НУЛЬ то
				CountVersions;
				StoreState
			иначе
				Files.OpenReader(r, f, 0);
				r.чЦел32(nofVersions, ложь);
			всё;
			если nofVersions = 0 то CountVersions; StoreState всё;
		кон LoadState;

		(* store the number of article versions in the archive file *)
		проц StoreState;
		перем f : Files.File;
			w : Files.Writer;
		нач
			f := Files.New(stateFileName^);
			Files.OpenWriter(w, f, 0);
			w.пЦел64(nofVersions, 0);
			w.ПротолкниБуферВПоток;
			Files.Register(f)
		кон StoreState;

		проц GetVersion*(nr : цел32) : Version;
		перем v : Version;
			receiver : Потоки.Делегат˛реализующийЧтениеИзПотока;
			r : Потоки.Чтец;
			t, vName : массив 8 из симв8;
		нач {единолично}
			если (nr < 0) или (nr >= nofVersions) то nr := nofVersions - 1 всё;
			Строки8.ПишиЦел64_вСтроку(nr, t);
			vName := "V";
			Строки8.ПодклейВСтрокуХвост(vName, t);
			нов(v);
			archive.Acquire;
			receiver := archive.OpenReceiver(vName);
			если receiver # НУЛЬ то
				нов(r, receiver, 128);
				если ~v.Load(r) то v := НУЛЬ всё;
			иначе v := НУЛЬ
			всё;
			archive.Release;
			если v # НУЛЬ то v.articleID := uid всё;
			возврат v
		кон GetVersion;

		проц InternalAdd(v : Version);
		перем t, vName : массив 8 из симв8;
			w : Потоки.Писарь;
			s : Потоки.Делегат˛реализующийЗаписьВПоток;
		нач
			v.articleID := uid;
			Строки8.ПишиЦел64_вСтроку(nofVersions, t);
			vName := "V";
			Строки8.ПодклейВСтрокуХвост(vName, t);
			archive.Acquire;
			s := archive.OpenSender(vName);
			нов(w, s, 128);
			v.Store(w);
			archive.Release;
			current := v;
			увел(nofVersions);
			StoreState;
		кон InternalAdd;

		проц AddVersion*(v : Version);
		нач {единолично}
			InternalAdd(v);
			если site # НУЛЬ то site.ArticleUpdated всё;
		кон AddVersion;

	кон Article;

	Articles = укль на массив из Article;

	ArticleList = укль на запись
		uid : String;
		prev, next: ArticleList;
	кон;

	Author = запись
		uid : массив 16 из симв8;
		pwd : массив 16 из симв8;
		name : массив 64 из симв8;
		email : массив 64 из симв8;
		dept : массив 64 из симв8;
	кон;

	CategoryStrings = укль на массив из Строки8.уСтрока;

	Archive= окласс
	перем articles : Articles;
		nofLoadedArticles : цел32;
		aList, path, title : массив 256 из симв8;
		id : массив 32 из симв8;
		frontPage : Строки8.Строкодел;

		first, last : ArticleList;
		needUpdate : булево;
		alive : булево;

		authorList : массив MaxAuthor из Author;
		nofAuthor : цел32;

		nofCategories : цел32;
		categoryStrings : CategoryStrings;

		проц LoadCategories;
		перем
			f : Files.File;
			r : Files.Reader;
			s : массив 64 из симв8;
			fn : массив 256 из симв8;
		нач
			копируйСтрокуДо0(path, fn);
			Строки8.ПодклейВСтрокуХвост(fn, CategoryFile);
			f := Files.Old(fn);

			если f = НУЛЬ то
				nofCategories := 3; нов(categoryStrings, 3);
				categoryStrings[0] := Строки8.ЯвиУСтроку("Shit Happens");
				categoryStrings[1] := Строки8.ЯвиУСтроку("Department");
				categoryStrings[2] := Строки8.ЯвиУСтроку("People");
			иначе
				нов(categoryStrings, 64); nofCategories := 0;
				Files.OpenReader(r, f, 0);
				нцПока (r.кодВозвратаПоследнейОперации = 0) и (nofCategories < 64) делай
					r.чСтроку8ДоКонцаСтрокиТекстаВключительно(s);
					categoryStrings[nofCategories] := Строки8.ЯвиУСтроку(s);
					увел(nofCategories)
				кц
			всё;
		кон LoadCategories;


		проц &Init*(конст path, id, title : массив из симв8);
		перем
			f : Files.File;
			r : Files.Reader;
			s : массив 32 из симв8;
			al : ArticleList;
			fn : массив 256 из симв8;
		нач
			копируйСтрокуДо0(path, сам.path);
			копируйСтрокуДо0(id, сам.id);
			копируйСтрокуДо0(title, сам.title);

			nofAuthor := 0;
			копируйСтрокуДо0(path, fn);
			Строки8.ПодклейВСтрокуХвост(fn, AuthorDataFile);

			f := Files.Old(fn);
			если f = НУЛЬ то
				nofAuthor := 1;
				authorList[0].uid := "T";
				authorList[0].pwd := "F";
				authorList[0].name := "Hobbes the Rat";
				authorList[0].email := "hobbestherat@bimbodot.org";
				authorList[0].dept := "bimbo";
			иначе
				Files.OpenReader(r, f, 0);
				нцПока (r.кодВозвратаПоследнейОперации = 0) и (nofAuthor < MaxAuthor) делай
					r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(authorList[nofAuthor].uid); r.ПропустиБелоеПоле;
					r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(authorList[nofAuthor].pwd); r.ПропустиБелоеПоле;
					r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(authorList[nofAuthor].name); r.ПропустиБелоеПоле;
					r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(authorList[nofAuthor].email); r.ПропустиБелоеПоле;
					r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(authorList[nofAuthor].dept); r.ПропустиДоКонцаСтрокиТекстаВключительно;
					если r.кодВозвратаПоследнейОперации = 0 то увел(nofAuthor) всё;
				кц;
			всё;

			LoadCategories;

			нов(first); нов(last); first.next := last; last.prev := first;


			нов(articles, 4);
			копируйСтрокуДо0(path, aList);
			Строки8.ПодклейВСтрокуХвост(aList, ArticleListFile);

			f := Files.Old(aList);
			если f # НУЛЬ то
				Files.OpenReader(r, f, 0);

				нцПока r.кодВозвратаПоследнейОперации = 0 делай
					r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(s); r.ПропустиДоКонцаСтрокиТекстаВключительно;
					если r.кодВозвратаПоследнейОперации = 0 то
						нов(al);

						al.uid := Строки8.ЯвиУСтроку(s);
						al.prev := last.prev; last.prev := al;
						al.next := last; al.prev.next := al;
					всё
				кц
			всё;
			CreateFrontPage
		кон Init;

		проц CreateFrontPage;
		перем w : Потоки.Писарь;
			h : HTMLWriter;
			al : ArticleList;
			a : Article; count : цел32;
			b : Строки8.Строкодел;
			l : массив 128 из симв8;
		нач
			нов(b, 1024); w := b.ДайПисаря();
			нов(h, w);
			h.Head(title);
			MakeListLink(сам, l);
			Строки8.ПодклейВСтрокуХвост(l, "&action=Edit");
			h.TextLink("Author New Article", l);
			h.Br;

			al := last.prev;
			count := 0;

			нцПока (count < 20) и (al # first) делай
				a := InternalGetArticle(al.uid^);
				если a # НУЛЬ то	RenderArticleHeadline(h, сам, a) всё;
				увел(count); al := al.prev;
			кц;
			h.Tail;
			w.ПротолкниБуферВПоток;
			frontPage := b
		кон CreateFrontPage;

		проц ArticleUpdated;
		нач  {единолично}
			needUpdate := истина;
		кон ArticleUpdated;

		проц LoadArticle(конст uid : массив из симв8) : Article;
		перем a : Article;
		нач
			нов(a); a.site := сам;
			если a.Load(path, uid) то возврат a
			иначе возврат НУЛЬ
			всё
		кон LoadArticle;

		проц AddLoadedArticle(a : Article);
		перем grow : Articles; i : цел32;
		нач
			если nofLoadedArticles >= длинаМассива(articles) то
				нов(grow, длинаМассива(articles) * 2);
				нцДля i := 0 до nofLoadedArticles - 1 делай	grow[i] := articles[i]	кц;
				articles := grow
			всё;
			articles[nofLoadedArticles] := a; увел(nofLoadedArticles)
		кон AddLoadedArticle;

		проц InternalGetArticle(конст uid : массив из симв8) : Article;
		перем i : цел32;
			a : Article;
		нач
			нцДля i := 0 до nofLoadedArticles - 1 делай
				если articles[i].uid^ = uid то
					возврат articles[i]
				всё;
			кц;
			a := LoadArticle(uid);
			если a # НУЛЬ то AddLoadedArticle(a) всё;
			возврат a
		кон InternalGetArticle;

		проц GetArticle*(конст uid : массив из симв8): Article;
		нач
			возврат InternalGetArticle(uid)
		кон GetArticle;


		проц GetVersion*(конст uid : массив из симв8; vNr : цел32) : Version;
		перем a : Article;
		нач {единолично}
			a := InternalGetArticle(uid);
			возврат a.GetVersion(vNr)
		кон GetVersion;

		проц GetUID(перем uid : массив из симв8);
		перем t, ts : массив 32 из симв8;
			i : цел32;
		нач
			uid := "D";
			Строки8.ПишиДатуВремяВСтроку("yyyymmdd", Dates.Now(), t);
			Строки8.ПодклейВСтрокуХвост(uid, t);
			Строки8.ПодклейВСтрокуХвост(uid, "A");
			копируйСтрокуДо0(uid, t); i := 0;
			нцДо
				Строки8.ПишиЦел64_вСтроку(i, ts);
				копируйСтрокуДо0(t, uid); Строки8.ПодклейВСтрокуХвост(uid, ts);
				увел(i)
			кцПри InternalGetArticle(uid) = НУЛЬ;
		кон GetUID;


		проц CreateArticle(v : Version) : Article;
		перем
			a : Article;
			uid : массив 32 из симв8;
			al : ArticleList;

			f: Files.File; w : Files.Writer;
		нач {единолично}
			GetUID(uid);

			нов(a);a.site := сам;
			a.Create(path, uid, v);
			AddLoadedArticle(a);
			нов(al);
			al.uid := Строки8.ЯвиУСтроку(uid);
			al.prev := last.prev; last.prev := al;
			al.next := last; al.prev.next := al;

			f := Files.Old(aList);
			если f = НУЛЬ то f := Files.New(aList) всё;

			Files.OpenWriter(w, f, f.Length());
			w.пСтроку8(uid); w.пВК_ПС;
			w.ПротолкниБуферВПоток;
			Files.Register(f);

			CreateFrontPage;
			needUpdate := истина;
			возврат a
		кон CreateArticle;

		проц Finish;
		нач {единолично}
			alive := ложь
		кон Finish;


	нач {активное}
		alive := истина;
		нцПока alive делай
			нач{единолично}
				дождись(needUpdate или ~alive);
				CreateFrontPage;
				needUpdate := ложь
			кон
		кц;
	кон Archive;

	ArchiveInfo = запись
		id : массив 256 из симв8;
		path, title : массив 256 из симв8;
		content : Archive;
	кон;
	ArchiveList = укль на массив из ArchiveInfo;

перем
	uriLiteral : массив 256 из булево;
	empty : String;
	archiveList : ArchiveList;
	nofArchive : цел32;

проц MakeListLink(archive : Archive; перем l : массив из симв8);
нач
	l := "Bimbodot?archive=";
	Строки8.ПодклейВСтрокуХвост(l, archive.id)
кон MakeListLink;


проц RenderArticleHeadline(h :HTMLWriter; archive : Archive; a : Article);
перем version : Version;
	w : Потоки.Писарь; l : массив 256 из симв8;
нач
	version := a.GetVersion(-1);
	если version = НУЛЬ то ЛогЯдра.пСтроку8("Version is NIL"); ЛогЯдра.пВК_ПС; всё;
	если version = НУЛЬ то возврат всё;
	w := h.w;
	w.пСтроку8('<h2>');
	l := "Bimbodot?action=Show&archive=";Строки8.ПодклейВСтрокуХвост(l, archive.id);
	Строки8.ПодклейВСтрокуХвост(l, "&article="); Строки8.ПодклейВСтрокуХвост(l, version.articleID^);
	h.TextLink(version.title^, l);
	w.пСтроку8("</h2>");

	w.пСтроку8("<b>");
	если version.category # НУЛЬ то w.пСтроку8("["); w.пСтроку8(version.category^); w.пСтроку8("] ") всё;
	w.пСтроку8('Posted by <a href="mailto:');w.пСтроку8(version.email^); w.пСтроку8('">');
	w.пСтроку8(version.author^); w.пСтроку8("</a> on "); w.пСтроку8(version.date^);	w.пСтроку8("</b>"); h.Br;
	w.пСтроку8("<i> from the "); w.пСтроку8(version.department^); w.пСтроку8(" dept. </i>"); h.Br;
	w.пСтроку8(version.text^);
	h.Br
кон RenderArticleHeadline;

проц FindVersion(archive : Archive; r : HTTPSupport.HTTPRequest) : Version;
перем
	article : Article;
	var: HTTPSupport.HTTPVariable;
	vNr : цел32;
нач
	var := r.GetVariableByName("article");
	если var # НУЛЬ то
		article := archive.GetArticle(var.value);
		var := r.GetVariableByName("version");
		если var = НУЛЬ то возврат article.current
		иначе
			Строки8.ПрочтиЦел32_изСтроки(var.value, vNr);
			возврат article.GetVersion(vNr)
		всё
	иначе
		возврат НУЛЬ
	всё
кон FindVersion;

проц Frontpage*(archive : Archive; data : динамическиТипизированныйУкль) : динамическиТипизированныйУкль;
перем context : WebCGI.CGIContext;
	b : Строки8.Строкодел;
	s : String;
нач
	если (data # НУЛЬ) и (data суть WebCGI.CGIContext) то
		context := data(WebCGI.CGIContext);
		b := archive.frontPage;
		context.reply.statuscode := WebHTTP.OK;
		context.reply.contentlength:= b.ДайКвоБайт˛неСчитаяЗавершающего0();
		context.reply.contenttype := "text/html; charset=UTF-8";
		WebHTTP.SendResponseHeader(context.reply, context.w);
		s := b.ДайСсылкуНаТекущиеДанные();
		context.w.пБайты(s^, 0, b.ДайКвоБайт˛неСчитаяЗавершающего0());
		context.w.ПротолкниБуферВПоток;
	всё;
	возврат НУЛЬ
кон Frontpage;

проц QueryEdit*(archive : Archive; data : динамическиТипизированныйУкль) : динамическиТипизированныйУкль;
перем context : WebCGI.CGIContext;
	w : Потоки.Писарь;
	chunker : WebHTTP.ChunkedOutStream;
	v : Version;
	 tv : String;
	now : массив 64 из симв8;
	h : HTMLWriter;
	i : цел32;
нач
	если (data # НУЛЬ) и (data суть WebCGI.CGIContext) то
		context := data(WebCGI.CGIContext);
		v := FindVersion(archive, context.request);

		нов(chunker, w, context.w, context.request.header, context.reply);
		context.reply.statuscode := WebHTTP.OK;
		context.reply.contenttype := "text/html; charset=UTF-8";
		WebHTTP.SendResponseHeader(context.reply, context.w);

		нов(h, w);
		h.Head("Bimbodot Edit Article");

		w.пСтроку8('<form action="Bimbodot?action=Publish" method="POST" accept-charset="UTF-8" >'); w.пВК_ПС;

		w.пСтроку8("<b>Accreditiation:</b><br/>");
		w.пСтроку8('Editor : '); h.InputText("editor", НУЛЬ);
		w.пСтроку8('Authorization : '); h.InputText("password", НУЛЬ);
		h.Br; w.пСтроку8("<hr/>");  w.пВК_ПС;

		h.Hide("archive", archive.id);
		если (v # НУЛЬ) и (v.articleID # НУЛЬ) то h.Hide("article", v.articleID^) всё;

		если (v # НУЛЬ) то tv := v.title всё;
		w.пСтроку8('Title : '); h.InputText("title", tv); h.Br; w.пВК_ПС;

		если (v # НУЛЬ) то tv := v.author всё;
		w.пСтроку8("Author : "); h.InputText("author", tv); w.пСтроку8("<i>leave empty for poster default</i>"); h.Br; w.пВК_ПС;

		если (v # НУЛЬ) то tv := v.email всё;
		w.пСтроку8("Email : "); h.InputText("email", tv); w.пСтроку8("<i>leave empty for poster default</i>"); h.Br; w.пВК_ПС;

		если (v # НУЛЬ) то tv := v.department всё;
		w.пСтроку8("Department : "); h.InputText("department", tv); w.пСтроку8("<i>leave empty for poster default</i>"); h.Br; w.пВК_ПС;

		если (v # НУЛЬ) то tv := v.category иначе tv := empty всё;
		w.пСтроку8("Category : ");
		h.BeginOptionField("category", tv^); h.Br; w.пВК_ПС;
		нцДля i := 0 до archive.nofCategories - 1 делай
			h.Option(archive.categoryStrings[i]^)
		кц;
		h.EndOptionField;


		Строки8.ПишиДатуВремяВСтроку("yyyy.mm.dd @ hh.nn.ss", Dates.Now(), now);
		w.пСтроку8("Date : "); h.InputText("date", Строки8.ЯвиУСтроку(now)); h.Br; w.пВК_ПС;

		если (v # НУЛЬ) то tv := v.text всё;
		w.пСтроку8("Text : "); h.InputArea("text", tv); h.Br; w.пВК_ПС;
		h.Submit("Publish");

		w.пСтроку8('</form>');

		h.Tail;
		w.пВК_ПС; w.ПротолкниБуферВПоток;
		chunker.Close;
	всё;
	возврат НУЛЬ
кон QueryEdit;

проц Publish*(archive: Archive;  data : динамическиТипизированныйУкль) : динамическиТипизированныйУкль;
перем context : WebCGI.CGIContext;
	w : Потоки.Писарь;
	h : HTMLWriter;
	chunker : WebHTTP.ChunkedOutStream;
	version : Version;
	article : Article;
	var: HTTPSupport.HTTPVariable;
	editor, pw : массив 64 из симв8;
	l : массив 256 из симв8;
	id : цел32;
нач
	если (data # НУЛЬ) и (data суть WebCGI.CGIContext) то
		context := data(WebCGI.CGIContext);

		var := context.request.GetVariableByName("editor");
		если var # НУЛЬ то копируйСтрокуДо0(var.value, editor) всё;
		var := context.request.GetVariableByName("password");
		если var # НУЛЬ то копируйСтрокуДо0(var.value, pw) всё;

		id := 0; нцПока (id < archive.nofAuthor) и (archive.authorList[id].uid # editor) делай
		увел(id) кц;

		если (id < archive.nofAuthor) и (archive.authorList[id].pwd = pw) то
			нов(chunker, w, context.w, context.request.header, context.reply);
			context.reply.statuscode := WebHTTP.OK;
			WebHTTP.SendResponseHeader(context.reply, context.w);
			нов(h, w);

			h.Head("Bimbodot Edit Result");

			var := context.request.GetVariableByName("article");
			если (var # НУЛЬ) то article := archive.GetArticle(var.value) всё;

			нов(version);

			var := context.request.GetVariableByName("title");
			если (var # НУЛЬ) и (var.value # "")  то version.title := Строки8.ЯвиУСтроку(var.value)
			иначе version.title := Строки8.ЯвиУСтроку("I forgot the Title");
			всё;

			var := context.request.GetVariableByName("author");
			если (var # НУЛЬ) и (var.value # "") то version.author := Строки8.ЯвиУСтроку(var.value)
			иначе version.author := Строки8.ЯвиУСтроку(archive.authorList[id].name);
			всё;

			var := context.request.GetVariableByName("email");
			если (var # НУЛЬ) и (var.value # "") то version.email:= Строки8.ЯвиУСтроку(var.value)
			иначе version.email := Строки8.ЯвиУСтроку(archive.authorList[id].email);
			всё;


			var := context.request.GetVariableByName("department");
			если (var # НУЛЬ) и (var.value # "") то version.department := Строки8.ЯвиУСтроку(var.value)
			иначе version.department := Строки8.ЯвиУСтроку(archive.authorList[id].dept);
			всё;

			var := context.request.GetVariableByName("category");
			если (var # НУЛЬ) и (var.value # "") то version.category := Строки8.ЯвиУСтроку(var.value)
			иначе version.category := Строки8.ЯвиУСтроку("Bimbo");
			всё;


			var := context.request.GetVariableByName("date");
			если (var # НУЛЬ) и (var.value # "") то version.date := Строки8.ЯвиУСтроку(var.value)
			иначе version.date := Строки8.ЯвиУСтроку("Whenever");
			всё;

			var := context.request.GetVariableByName("text");
			если (var # НУЛЬ) и (var.value # "") то version.text := Строки8.ЯвиУСтроку(var.value)
			иначе version.text := Строки8.ЯвиУСтроку("Whatever");
			всё;

			если article = НУЛЬ то
				article := archive.CreateArticle(version);
				w.пСтроку8("Article : "); w.пСтроку8(article.uid^); w.пСтроку8(" has been created"); w.пВК_ПС
			иначе
				w.пСтроку8("Article : "); w.пСтроку8(article.uid^); w.пСтроку8(" has been updated"); w.пВК_ПС;
				article.AddVersion(version);
			всё;
			MakeListLink(archive, l);
			h.Br; h.TextLink("Home", l);

			h.Tail;
			w.пВК_ПС; w.ПротолкниБуферВПоток;
			chunker.Close;
		иначе
			нов(chunker, w, context.w, context.request.header, context.reply);
			context.reply.statuscode := WebHTTP.Unauthorized;
			WebHTTP.SendResponseHeader(context.reply, context.w);

			нов(h, w);
			h.Head("Error : Editing Article");
			w.пСтроку8("You need a-huga-accredition to publish an article! If you have one... go back and fill it in otherwise just forget about it.");
			h.Tail;
			w.пВК_ПС; w.ПротолкниБуферВПоток;
			chunker.Close;
		всё;
	всё;
	возврат НУЛЬ
кон Publish;


проц ShowArticle*(archive : Archive; data : динамическиТипизированныйУкль) : динамическиТипизированныйУкль;
перем r : HTTPSupport.HTTPRequest;
	context : WebCGI.CGIContext;
	i : цел32;
	var: HTTPSupport.HTTPVariable;
	w : Потоки.Писарь;
	chunker : WebHTTP.ChunkedOutStream;

	article : Article;
	version : Version;
	vNr : цел32;
	h : HTMLWriter;
	l : массив 128 из симв8;
нач
	если (data # НУЛЬ) и (data суть WebCGI.CGIContext) то
		context := data(WebCGI.CGIContext);
		r := context.request;
		var := r.GetVariableByName("article");
		если var # НУЛЬ то
			article := archive.GetArticle(var.value);
		всё;
		если article # НУЛЬ то

			(* reply *)
			нов(chunker, w, context.w, context.request.header, context.reply);
			context.reply.statuscode := WebHTTP.OK;
			WebHTTP.SendResponseHeader(context.reply, context.w);

			нов(h, w);
			h.Head(archive.title);

			MakeListLink(archive, l);
			h.TextLink(archive.title, l);

			var := r.GetVariableByName("version");
			если var = НУЛЬ то version := article.current
			иначе
				Строки8.ПрочтиЦел32_изСтроки(var.value, vNr);
				version := article.GetVersion(vNr)
			всё;

			если version # НУЛЬ то
				w.пСтроку8('<h2>');
				l := "Bimbodot?action=Show&archive="; Строки8.ПодклейВСтрокуХвост(l, archive.id); Строки8.ПодклейВСтрокуХвост(l, "&article=");Строки8.ПодклейВСтрокуХвост(l, version.articleID^); h.TextLink(version.title^, l);
				w.пСтроку8("</h2>");

				w.пСтроку8("<b>");
				если version.category # НУЛЬ то w.пСтроку8("["); w.пСтроку8(version.category^); w.пСтроку8("] ") всё;
				w.пСтроку8('Posted by <a href="mailto:');w.пСтроку8(version.email^); w.пСтроку8('">');
				w.пСтроку8(version.author^); w.пСтроку8("</a> on "); w.пСтроку8(version.date^);	w.пСтроку8("</b>"); h.Br;
				w.пСтроку8("<i> from the "); w.пСтроку8(version.department^); w.пСтроку8(" dept. </i>"); h.Br;
				w.пСтроку8(version.text^);
				h.Br;

				w.пСтроку8('<a href="Bimbodot?action=Edit&archive='); w.пСтроку8(archive.id);
				w.пСтроку8("&article="); w.пСтроку8(version.articleID^); w.пСтроку8('">Change this article</a>');	w.пСтроку8("<br/>");
				w.пСтроку8("<br/>");
				если article.nofVersions > 1 то
					w.пСтроку8("Other versions of the article :");
					нцДля i := 0 до article.nofVersions - 1 делай
						w.пСтроку8('<a href="Bimbodot?action=Show&archive=');w.пСтроку8(archive.id);
						w.пСтроку8("&article="); w.пСтроку8(article.uid^);
						w.пСтроку8("&version="); w.пЦел64(i, 0); w.пСтроку8('">'); w.пЦел64(i, 0); w.пСтроку8("</a>&nbsp;");
					кц
				всё;
			всё;
			h.Tail;
			w.пВК_ПС; w.ПротолкниБуферВПоток;
			chunker.Close;

		иначе
			нов(chunker, w, context.w, context.request.header, context.reply);
			context.reply.statuscode := WebHTTP.NotFound;
			WebHTTP.SendResponseHeader(context.reply, context.w);
			w.пСтроку8("<html><head><title>Bimbodot</title></head>");
			w.пСтроку8("<body>");
			w.пСтроку8("Article not found : "); w.пСтроку8(var.value); w.пВК_ПС;
			w.пСтроку8("</body></html>");
			w.пВК_ПС; w.ПротолкниБуферВПоток;
			chunker.Close;
		всё;
	всё;
	возврат НУЛЬ
кон ShowArticle;

проц Access*(context : WebCGI.CGIContext);
перем
	r : HTTPSupport.HTTPRequest;
	var: HTTPSupport.HTTPVariable;

	action, archiveID, entry : массив 32 из симв8;

	w : Потоки.Писарь;
	chunker : WebHTTP.ChunkedOutStream;

	ignore : динамическиТипизированныйУкль;

	defaultAction : булево;
	milliTimer : Kernel.MilliTimer;
	archive : Archive;
нач
	Kernel.SetTimer(milliTimer, 0);
	r := context.request;
	defaultAction := истина;
	var := r.GetVariableByName("action");
	если var # НУЛЬ то копируйСтрокуДо0(var.value, action); defaultAction := ложь всё;

	var := r.GetVariableByName("archive");
	если var # НУЛЬ то копируйСтрокуДо0(var.value, archiveID) всё;

	var := r.GetVariableByName("entry");
	если var # НУЛЬ то копируйСтрокуДо0(var.value, entry) всё;

	archive := GetArchive(archiveID);

	если archive = НУЛЬ то
		нов(chunker, w, context.w, context.request.header, context.reply);
		context.reply.statuscode := WebHTTP.NotFound;
		WebHTTP.SendResponseHeader(context.reply, context.w);
		w.пСтроку8("<html><head><title>Forum</title></head>");
		w.пСтроку8("<body>");
		w.пСтроку8("Archive not found"); w.пВК_ПС;
		w.пСтроку8("</body></html>");
		w.пВК_ПС; w.ПротолкниБуферВПоток;
		chunker.Close
	иначе
		если action = "Show" то ignore := ShowArticle(archive, context); возврат;
		аесли defaultAction или (action = "List") то ignore := Frontpage(archive, context); возврат;
		аесли action = "Publish" то ignore := Publish(archive, context); возврат;
		аесли action = "Edit" то ignore := QueryEdit(archive, context); возврат;
		иначе
			нов(chunker, w, context.w, context.request.header, context.reply);
			context.reply.statuscode := WebHTTP.NotFound;
			WebHTTP.SendResponseHeader(context.reply, context.w);
			w.пСтроку8("<html><head><title>Forum</title></head>");
			w.пСтроку8("<body>");
			w.пСтроку8("Illegal Bimbo request"); w.пВК_ПС;
			w.пСтроку8("</body></html>");
			w.пВК_ПС; w.ПротолкниБуферВПоток;
			chunker.Close
		всё
	всё;
	ЛогЯдра.пСтроку8("Bimbodot request handled in "); ЛогЯдра.пЦел64(Kernel.Elapsed(milliTimer), 0); ЛогЯдра.пСтроку8("ms"); ЛогЯдра.пВК_ПС;
кон Access;

проц AddArchive(конст id, path, title: массив из симв8);
перем new : ArchiveList;
	i : цел32;
нач
	если nofArchive >= длинаМассива(archiveList) то
		нов(new, длинаМассива(archiveList) * 2);
		нцДля i := 0 до nofArchive - 1 делай new[i] := archiveList[i] кц;
		archiveList := new
	всё;
	копируйСтрокуДо0(id, archiveList[nofArchive].id);
	копируйСтрокуДо0(path, archiveList[nofArchive].path);
	копируйСтрокуДо0(title, archiveList[nofArchive].title);
	увел(nofArchive)
кон AddArchive;

проц GetArchiveInternal(конст id : массив из симв8) : Archive;
перем i : цел32; result : Archive;
нач
	i := 0;
	нцПока (i < nofArchive) и (result = НУЛЬ)  делай
		если archiveList[i].id = id то
			если archiveList[i].content = НУЛЬ то
				нов(archiveList[i].content, archiveList[i].path, id, archiveList[i].title);
			(*	IF archiveList[i].content.Load() THEN
					KernelLog.String(archiveList[i].id); KernelLog.String(" loaded from "); KernelLog.String(archiveList[i].path); KernelLog.Ln;
				ELSE
					KernelLog.String(archiveList[i].id); KernelLog.String("FAILED loading  from "); KernelLog.String(archiveList[i].path); KernelLog.Ln;
				END;*)
			всё;
			result := archiveList[i].content
		всё;
		увел(i)
	кц;
	возврат result
кон GetArchiveInternal;

проц GetArchive(конст id : массив из симв8) : Archive;
нач {единолично}
	возврат GetArchiveInternal(id)
кон GetArchive;

проц LoadArchiveList;
перем f : Files.File;
	r : Files.Reader;
	id, path, title: массив 256 из симв8;
нач {единолично}
	f := Files.Old(BimbodotConfigFile);
	если f # НУЛЬ то
		Files.OpenReader(r, f, 0);
		нцПока r.кодВозвратаПоследнейОперации = 0 делай
			r.чЦепочкуСимв8ДоБелогоПоля(id); r.ПропустиБелоеПоле;
			r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(path); r.ПропустиБелоеПоле;
			r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(title);
			если r.кодВозвратаПоследнейОперации = 0 то AddArchive(id, path, title) всё;
			r.ПропустиДоКонцаСтрокиТекстаВключительно;
		кц
	всё;
кон LoadArchiveList;

проц StoreArchiveList;
перем f : Files.File;
	w : Files.Writer;
	i : цел32;
нач {единолично}
	f := Files.New(BimbodotConfigFile);
	Files.OpenWriter(w, f, 0);
	нцДля i := 0 до nofArchive- 1 делай
		w.пСтроку8(archiveList[i].id); w.пСтроку8(' "'); w.пСтроку8(archiveList[i].path); w.пСтроку8('" '); w.пСтроку8(' "'); w.пСтроку8(archiveList[i].title); w.пСтроку8('" '); w.пВК_ПС
	кц;
	w.ПротолкниБуферВПоток;
	Files.Register(f)
кон StoreArchiveList;

проц Create*(context : Commands.Context);
перем
	id, path, title: массив 128 из симв8;
	archive : Archive;
нач
	context.arg.чЦепочкуСимв8ДоБелогоПоля(id); context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(path);
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(title);
	нач{единолично}
		archive := GetArchiveInternal(id);
		если archive # НУЛЬ то
			context.error.пСтроку8("Archive already exists"); context.error.пВК_ПС;
			возврат;
		иначе
			AddArchive(id, path, title);
		всё
	кон;
	StoreArchiveList;
кон Create;

проц Finalizer;
перем i : цел32;
	t : Kernel.Timer;
нач
	нцДля i := 0 до nofArchive - 1 делай
		если archiveList[i].content # НУЛЬ то archiveList[i].content.Finish всё
	кц;
	нов(t);
	t.Sleep(100);
кон Finalizer;

проц InitURILiterals;
перем i : цел32;
нач
	нцДля i := 0 до 255 делай uriLiteral[i] := ложь кц;
	нцДля i :=  61H до  7AH делай uriLiteral[i] := истина кц;(* RFC2396 lowalpha *)
	нцДля i :=  41H до 5AH делай uriLiteral[i] := истина кц;(* RFC2396 upalpha *)
	нцДля i := 30H до 39H делай uriLiteral[i] := истина кц; (* RFC2396 digit *)
	uriLiteral[2DH] := истина; (* - *)
	uriLiteral[5FH] := истина; (* underscore *)
	uriLiteral[2EH] := истина; (* . *)
	uriLiteral[21H] := истина; (* ! *)
	uriLiteral[7EH] := истина; (* ~ *)
	uriLiteral[2AH] := истина; (* * *)
	uriLiteral[27H] := истина; (* ' *)
	uriLiteral[28H] := истина; (* ( *)
	uriLiteral[29H] := истина;  (* ) *)
кон InitURILiterals;

нач
	empty := Строки8.ЯвиУСтроку("");
	InitURILiterals;
	нов(archiveList, 128); nofArchive := 0;

	LoadArchiveList;
	Modules.InstallTermHandler(Finalizer)
кон WebBimbodot.


WebBimbodot.Test ~
System.Free WebBimbodot ~

WebHTTPServerTools.Start ~
WebCGI.Install ~

WebCGI.RegisterCGI Bimbodot WebBimbodot.Access~
WebCGI.ListCGI ~
WebBimbodot.Create Bimbodot "FAT:/BDATA/BIMBO/" "The Original Bimbodot" ~
WebBimbodot.Create CSucks "FAT:/BDATA/CSUCKS/" "The Reasons Why C Sucks" ~

Example for BimbodotAuthors.txt :
"hobbes" "password" "Hobbes the Rat" "hobbestherat@bimbodot.org" "RZ-H23"
"barnoid" "password" "Barnoid the Master" "barnoidthemaster@bimbodot.org" "South-Korea"

модуль JoysticksTest; (** AUTHOR "staubesv"; PURPOSE "Joystick simulator for testing purposes"; *)
(**
 * Usage:
 *
 *	JoysticksTest.CreateJoystick [nbrOfButtons] ~ creates a new simulated joystick
 *	System.Free JoysticksTest ~	unload module, unregisters all simulated joysticks at Joysticks.registry
 *
 * History:
 *
 *	28.11.2006	First release (staubesv)
 *)

использует
	ЛогЯдра, Modules, Kernel, Plugins, Commands, Random, Joysticks;

конст
	DefaultNbrOfButtons = 2;

	MaxSimulatedJoysticks = 16;

	MinAxisValueX = -255;
	MaxAxisValueX = 255;

	MinAxisValueY = 0;
	MaxAxisValueY = 1024;

	MinAxisValueSlider1 = -16384;
	MaxAxisValueSlider1 = 16384;

	MinAxisDefaultValue = -1024;
	MaxAxisDefaultValue = 1024;

	MinSleepTime = 200;
	MaxSleepTime = 1500;

тип

	SimulatedJoystick = окласс(Joysticks.Joystick)
	перем
		msg : Joysticks.JoystickDataMessage;
		random : Random.Generator;
		timer : Kernel.Timer;
		alive, dead : булево;
		i, minValue, maxValue : цел32;

		проц Stop;
		нач {единолично}
			alive := ложь; timer.Wakeup;
			дождись(dead);
		кон Stop;

		проц &{перекрыта}Init*(nbrOfButtons  : цел32);
		нач
			Init^(nbrOfButtons);
			alive := истина; dead := ложь;
			нов(timer);
			нов(random);
		кон Init;

	нач {активное}
		нцПока alive делай
			нцДля i := 0 до nbrOfButtons-1 делай
				если random.Dice(2) = 1 то
					включиВоМнвоНаБитах(msg.buttons, i);
				иначе
					исключиИзМнваНаБитах(msg.buttons, i);
				всё;
			кц;
			нцДля i := 0 до Joysticks.MaxNbrOfAxis - 1 делай
				если i в implementedAxis то
					просей i из
						| Joysticks.AxisX: minValue := MinAxisValueX; maxValue := MaxAxisValueX;
						| Joysticks.AxisY: minValue := MinAxisValueY; maxValue := MaxAxisValueY;
						| Joysticks.Slider1: minValue := MinAxisValueSlider1; maxValue := MaxAxisValueSlider1;
					иначе
						minValue := MinAxisDefaultValue; maxValue := MaxAxisDefaultValue;
					всё;
					msg.axis[i] := random.Dice(maxValue - minValue) + 1 + minValue;
				всё;
			кц;
			нцДля i := 0 до nbrOfCoolieHats-1 делай
				исключиИзМнваНаБитах(msg.coolieHat[i], random.Dice(4));
				исключиИзМнваНаБитах(msg.coolieHat[i], random.Dice(4));
				включиВоМнвоНаБитах(msg.coolieHat[i], random.Dice(4));
			кц;
			Handle(msg);
			timer.Sleep(random.Dice(MaxSleepTime - MinSleepTime) + MinSleepTime);
		кц;
		нач {единолично} dead := истина; кон;
	кон SimulatedJoystick;

перем
	joysticks : массив MaxSimulatedJoysticks из SimulatedJoystick;
	nbrOfJoysticks : цел32;

проц CreateJoystick*(context : Commands.Context); (* [nbrOfButtons] ~ *)
перем joystick : SimulatedJoystick; nbrOfButtons, nbrOfAxis: цел32; res : целМЗ;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(nbrOfButtons, ложь);
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(nbrOfAxis, ложь);
	если nbrOfButtons <= 0 то nbrOfButtons := DefaultNbrOfButtons; всё;
	если nbrOfJoysticks < длинаМассива(joysticks) то
		нов(joystick, nbrOfButtons);
		joystick.AddAxis(Joysticks.AxisX, MinAxisValueX, MaxAxisValueX);
		joystick.AddAxis(Joysticks.AxisY, MinAxisValueY, MaxAxisValueY);
		joystick.AddAxis(Joysticks.Slider1, MinAxisValueSlider1, MaxAxisValueSlider1);
		joystick.AddCoolieHat;
		joystick.AddCoolieHat;
		joystick.desc := "Simulated Joystick";
		joysticks[nbrOfJoysticks] := joystick;
		увел(nbrOfJoysticks);
		Joysticks.registry.Add(joystick, res);
		если res = Plugins.Ok то
			context.out.пСтроку8("Joystick created."); ЛогЯдра.пВК_ПС;
		иначе
			context.error.пСтроку8("Could not add joystick to registry, res: "); context.error.пЦел64(res, 0); context.error.пВК_ПС;
		всё;
	иначе
		context.error.пСтроку8("Maximum number of joysticks exceeded."); context.error.пВК_ПС;
	всё;
кон CreateJoystick;

проц Cleanup;
перем i : цел32;
нач
	нцДля i := 0 до MaxSimulatedJoysticks - 1 делай
		если joysticks[i] # НУЛЬ то
			joysticks[i].Stop;
			Joysticks.registry.Remove(joysticks[i]);
			joysticks[i] := НУЛЬ;
		всё;
	кц;
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
кон JoysticksTest.

Joysticks.Show ~

JoysticksTest.CreateJoystick ~

System.Free WMJoysticks JoysticksTest Joysticks ~

WMJoysticks.Open ~

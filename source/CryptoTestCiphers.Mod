модуль CryptoTestCiphers;	(** AUTHOR "F.N."; PURPOSE "Ciphers Test"; *)

использует
	U := CryptoUtils,	Ciphers := CryptoCiphers, Kernel,	Commands,
	Log := ЛогЯдра;

	проц  Ecb1*( c : Commands.Context );
		перем
			hex, bindata, binkey, modname, orig: массив 64 + 1 из симв8; cipher: Ciphers.Cipher;
			keybits: цел32;
	нач
		(* read in the parameter *)
		c.arg.ПропустиБелоеПоле; c.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(modname);
		c.arg.ПропустиБелоеПоле; c.arg.чЦел32(keybits, ложь);
		(* encryption *)
		cipher := Ciphers.NewCipher( modname );
		hex := "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF";
		U.Hex2Bin( hex, 0, binkey, 0, keybits DIV 8 );	U.Hex2Bin( hex, 0, bindata, 0, cipher.blockSize );
		orig := bindata; orig[cipher.blockSize] := 0X;
		cipher.InitKey( binkey, keybits );
		Log.пВК_ПС; Log.пСтроку8( "*********************************" );
		Log.пВК_ПС; Log.пСтроку8( "Encrypt-Decrypt-Test in ECB-mode: " ); Log.пСтроку8( cipher.name );
		Log.пВК_ПС; Log.пСтроку8( "Key: " ); U.PrintHex( binkey, 0, keybits DIV 8 );
		Log.пВК_ПС; Log.пСтроку8( "Original: " ); U.PrintHex( bindata, 0, cipher.blockSize );
		cipher.Encrypt( bindata, 0, cipher.blockSize );
		Log.пВК_ПС; Log.пСтроку8( "Encrypted: " ); U.PrintHex( bindata, 0, cipher.blockSize );
		(* decryption *)
		cipher.InitKey( binkey, keybits );
		cipher.Decrypt( bindata, 0, cipher.blockSize );
		Log.пВК_ПС; Log.пСтроку8( "Decrypted: " ); U.PrintHex( bindata, 0, cipher.blockSize );
		bindata[cipher.blockSize] := 0X;
		Log.пВК_ПС;
		если bindata = orig то  Log.пСтроку8( "OK" )  иначе  Log.пСтроку8( "FAIL" )  всё;
		Log.пВК_ПС
	кон Ecb1;

	проц  Cbc1*(c : Commands.Context);
		перем
			hex, bindata, binkey, modname, iv, orig: массив 2 * 64 + 1 из симв8; cipher: Ciphers.Cipher;
			keybits: цел32;
	нач
		(* read in the parameter *)
		c.arg.ПропустиБелоеПоле; c.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(modname);
		c.arg.ПропустиБелоеПоле; c.arg.чЦел32(keybits, ложь);
				(* encryption *)
		cipher := Ciphers.NewCipher( modname );
		hex := "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF";
		U.Hex2Bin( hex, 0, binkey, 0, keybits DIV 8 );	U.Hex2Bin( hex, 0, bindata, 0, 2 * cipher.blockSize );
		cipher.InitKey( binkey, keybits );
		U.RandomBytes( iv, 0, cipher.blockSize );
		(*U.RandomBytes( bindata, 0, cipher.blockSize);*)
		cipher.SetIV( iv, Ciphers.CBC );
		Log.пВК_ПС; Log.пСтроку8( "*********************************" );
		Log.пВК_ПС; Log.пСтроку8( "Encrypt-Decrypt-Test in CBC-mode: " ); Log.пСтроку8( cipher.name );
		Log.пВК_ПС; Log.пСтроку8( "Key: " ); U.PrintHex( binkey, 0, keybits DIV 8 );
		Log.пВК_ПС; Log.пСтроку8( "IV: "); U.PrintHex( iv, 0, cipher.blockSize );
		Log.пВК_ПС; Log.пСтроку8( "Original: " ); U.PrintHex( bindata, 0, 2 * cipher.blockSize );
		orig := bindata;  orig[2 * cipher.blockSize] := 0X;
		cipher.Encrypt( bindata, 0, 2 * cipher.blockSize );
		Log.пВК_ПС; Log.пСтроку8( "Encrypted: " ); U.PrintHex( bindata, 0, 2 * cipher.blockSize );
		(* decryption *)
		cipher.SetIV( iv, Ciphers.CBC );
		cipher.Decrypt( bindata, 0, 2 * cipher.blockSize );
		Log.пВК_ПС; Log.пСтроку8( "Decrypted: " ); U.PrintHex( bindata, 0, 2 * cipher.blockSize );
		bindata[2 * cipher.blockSize] := 0X;
		Log.пВК_ПС;
		если bindata = orig то  Log.пСтроку8( "OK" )  иначе  Log.пСтроку8( "FAIL" )  всё;
		Log.пВК_ПС
	кон Cbc1;

	проц  Ctr1*(c : Commands.Context);
		перем
			hex, bindata, binkey, modname, iv, orig: массив 64 из симв8; cipher: Ciphers.Cipher;
			keybits: цел32;
	нач
		(* read in the parameter *)
		c.arg.ПропустиБелоеПоле; c.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(modname);
		c.arg.ПропустиБелоеПоле; c.arg.чЦел32(keybits, ложь);
				(* encryption *)
		cipher := Ciphers.NewCipher( modname );
		hex := "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF";
		U.Hex2Bin( hex, 0, binkey, 0, 24 );	(*U.Hex2Bin( hex, 0, bindata, 0, 16 );*)
		cipher.InitKey( binkey, keybits );
		U.RandomBytes( iv, 0, cipher.blockSize );
		U.RandomBytes( bindata, 0, cipher.blockSize);
		cipher.SetIV( iv, Ciphers.CTR );
		Log.пВК_ПС; Log.пСтроку8( "*********************************" );
		Log.пВК_ПС; Log.пСтроку8( "Encrypt-Decrypt-Test in CTR-mode: " ); Log.пСтроку8( cipher.name );
		Log.пВК_ПС; Log.пСтроку8( "Key: " ); U.PrintHex( binkey, 0, keybits DIV 8 );
		Log.пВК_ПС; Log.пСтроку8( "Original: " ); U.PrintHex( bindata, 0, 16 );
		orig := bindata;  orig[16] := 0X;
		cipher.Encrypt( bindata, 0, 16 );
		Log.пВК_ПС; Log.пСтроку8( "Encrypted: " ); U.PrintHex( bindata, 0, 16 );
		(* decryption *)
		cipher.InitKey( binkey, keybits );
		cipher.SetIV( iv, Ciphers.CTR );
		cipher.Decrypt( bindata, 0, 16 );
		Log.пВК_ПС; Log.пСтроку8( "Decrypted: " ); U.PrintHex( bindata, 0, 16 );
		bindata[16] := 0X;
		Log.пВК_ПС;
		если bindata = orig то  Log.пСтроку8( "OK" )  иначе  Log.пСтроку8( "FAIL" )  всё;
		Log.пВК_ПС
	кон Ctr1;


	(** encrypt input with key (ebc-mode). output is a testvector *)
	проц  Ecb2(	c: Commands.Context;
							конст modname, input, output, key: массив из симв8;
							datalen, keybits: цел32 );
		перем
			cipher: Ciphers.Cipher;
			temp1, temp2: массив 256 из симв8;
	нач
		cipher := Ciphers.NewCipher( modname );
		U.Hex2Bin( key, 0, temp1, 0, keybits DIV 8 );
		cipher.InitKey( temp1, keybits );
		Log.пВК_ПС; Log.пСтроку8( "*********************************" );
		Log.пВК_ПС; Log.пСтроку8( "Encryption-Test: " ); Log.пСтроку8( cipher.name );
		Log.пВК_ПС; Log.пСтроку8( "Key: " ); U.PrintHex( temp1, 0, keybits DIV 8 );
		U.Hex2Bin( input, 0, temp1, 0, datalen );
		Log.пВК_ПС; Log.пСтроку8( "plaintext: " ); U.PrintHex( temp1, 0, datalen );
		cipher.Encrypt( temp1, 0, datalen );
		Log.пВК_ПС; Log.пСтроку8( "encryption: " ); U.PrintHex( temp1, 0, datalen );
		U.Hex2Bin( output, 0, temp2, 0, datalen );
		Log.пВК_ПС; Log.пСтроку8( "correct encryption: " ); U.PrintHex( temp2, 0, datalen );
		Log.пВК_ПС;
		temp1[datalen] := 0X;  temp2[datalen] := 0X;
		если temp1 = temp2 то  Log.пСтроку8( "OK" )  иначе  Log.пСтроку8( "FAIL" )  всё;
		Log.пВК_ПС
	кон Ecb2;

	(** encrypt input with key (cbc-mode). output is a testvector *)
	проц  Cbc2(	c: Commands.Context;
							конст modname, input, output, key, iv: массив из симв8; 
							datalen, keybits: цел32 );
		перем
			cipher: Ciphers.Cipher;
			temp1, temp2: массив 64 из симв8;
	нач
		cipher := Ciphers.NewCipher( modname );
		U.Hex2Bin( key, 0, temp1, 0, keybits DIV 8 );
		cipher.InitKey( temp1, keybits );
		U.Hex2Bin( iv, 0, temp2, 0, cipher.blockSize );
		cipher.SetIV( temp2, Ciphers.CBC );
		Log.пВК_ПС; Log.пСтроку8( "*********************************" );
		Log.пВК_ПС; Log.пСтроку8( "Encryption-Test: " ); Log.пСтроку8( cipher.name );
		Log.пВК_ПС; Log.пСтроку8( "Key: " ); U.PrintHex( temp1, 0, keybits DIV 8 );
		U.Hex2Bin( input, 0, temp1, 0, datalen );
		Log.пВК_ПС; Log.пСтроку8( "plaintext: " ); U.PrintHex( temp1, 0, datalen );
		cipher.Encrypt( temp1, 0, datalen );
		Log.пВК_ПС; Log.пСтроку8( "encryption: " ); U.PrintHex( temp1, 0, datalen );
		U.Hex2Bin( output, 0, temp2, 0, datalen );
		Log.пВК_ПС; Log.пСтроку8( "correct encryption: " ); U.PrintHex( temp2, 0, datalen );
		Log.пВК_ПС;
		temp1[datalen] := 0X;  temp2[datalen] := 0X;
		если temp1 = temp2 то  Log.пСтроку8( "OK" )  иначе  Log.пСтроку8( "FAIL" )  всё;
		Log.пВК_ПС
	кон Cbc2;

	(** decrypt input with key (cbc-mode). output is a testvector *)
	проц DecryptCbc2(	c: Commands.Context;
									конст modname, input, output, key, iv: массив из симв8;
									datalen, keybits: цел32);
		перем
			cipher: Ciphers.Cipher;
			temp1, temp2: массив 64 из симв8;
	нач
		cipher := Ciphers.NewCipher( modname );
		U.Hex2Bin( key, 0, temp1, 0, keybits DIV 8 );
		cipher.InitKey( temp1, keybits );
		U.Hex2Bin( iv, 0, temp2, 0, cipher.blockSize );
		cipher.SetIV( temp2, Ciphers.CBC );
		Log.пВК_ПС; Log.пСтроку8( "*********************************" );
		Log.пВК_ПС; Log.пСтроку8( "Decryption-Test: " ); Log.пСтроку8( cipher.name );
		Log.пВК_ПС; Log.пСтроку8( "Key: " ); U.PrintHex( temp1, 0, keybits DIV 8 );
		U.Hex2Bin( input, 0, temp1, 0, datalen );
		Log.пВК_ПС; Log.пСтроку8( "ciphertext: " ); U.PrintHex( temp1, 0, datalen );
		cipher.Decrypt( temp1, 0, datalen );
		Log.пВК_ПС; Log.пСтроку8( "decryption: " ); U.PrintHex( temp1, 0, datalen );
		U.Hex2Bin( output, 0, temp2, 0, datalen );
		Log.пВК_ПС; Log.пСтроку8( "correct decryption: " ); U.PrintHex( temp2, 0, datalen );
		Log.пВК_ПС;
		temp1[datalen] := 0X;  temp2[datalen] := 0X;
		если temp1 = temp2 то  Log.пСтроку8( "OK" )  иначе  Log.пСтроку8( "FAIL" )  всё;
		Log.пВК_ПС
	кон DecryptCbc2;

	(** encrypt input with key (counter-mode). output is a testvector *)
	проц  Ctr2(	c: Commands.Context;
						конст modname, input, output, key, iv: массив из симв8; 
						datalen, keybits: цел32 );
		перем
			cipher: Ciphers.Cipher;
			temp1, temp2: массив 64 из симв8;
	нач
		cipher := Ciphers.NewCipher( modname );
		U.Hex2Bin( key, 0, temp1, 0, keybits DIV 8 );
		cipher.InitKey( temp1, keybits );
		U.Hex2Bin( iv, 0, temp2, 0, cipher.blockSize );
		cipher.SetIV( temp2, Ciphers.CTR );
		Log.пВК_ПС; Log.пСтроку8( "*********************************" );
		Log.пВК_ПС; Log.пСтроку8( "Encryption-Test: " ); Log.пСтроку8( cipher.name );
		Log.пВК_ПС; Log.пСтроку8( "Key: " ); U.PrintHex( temp1, 0, keybits DIV 8 );
		U.Hex2Bin( input, 0, temp1, 0, datalen );
		Log.пВК_ПС; Log.пСтроку8( "plaintext: " ); U.PrintHex( temp1, 0, datalen );
		cipher.Encrypt( temp1, 0, datalen );
		Log.пВК_ПС; Log.пСтроку8( "encryption: " ); U.PrintHex( temp1, 0, datalen );
		U.Hex2Bin( output, 0, temp2, 0, datalen );
		Log.пВК_ПС; Log.пСтроку8( "correct encryption: " ); U.PrintHex( temp2, 0, datalen );
		Log.пВК_ПС;
		temp1[datalen] := 0X;  temp2[datalen] := 0X;
		если temp1 = temp2 то  Log.пСтроку8( "OK" )  иначе  Log.пСтроку8( "FAIL" )  всё;
		Log.пВК_ПС
	кон Ctr2;

	проц MeasureTime*( c : Commands.Context );
		перем
			buf, key: массив 1024 из симв8;
			milliTimer : Kernel.MilliTimer;
			i, j, k, t, keybits: цел32;
			cipher: Ciphers.Cipher;
			modname, mode, iv: массив 64 из симв8;
	нач
		(* read in the parameter *)
		c.arg.ПропустиБелоеПоле; c.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(modname);
		c.arg.ПропустиБелоеПоле; c.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(mode);
		c.arg.ПропустиБелоеПоле; c.arg.чЦел32(keybits, ложь);
		(* measure time *)
		нцДля i := 0 до 1023 делай	buf[i] := 'a'	кц;
		cipher := Ciphers.NewCipher( modname );
		cipher.InitKey( key, keybits );
		если mode = "CBC" то	cipher.SetIV( iv, Ciphers.CBC )	всё;
		Log.пВК_ПС; Log.пСтроку8( "***********************************" );
		Log.пВК_ПС; Log.пСтроку8( "Encrypting 100 MB with " ); Log.пСтроку8( cipher.name );
		Kernel.SetTimer(milliTimer, 0);
		нцДля k := 0 до 9 делай
			Log.пСтроку8( "." );
			нцДля j := 0 до 9 делай
				нцДля i := 0 до 999 делай	cipher.Encrypt( buf, 0, 1024 )	кц(* 100 MB data *)
			кц
		кц;
		t := Kernel.Elapsed(milliTimer);
		Log.пЦел64( t, 4 ); Log.пСтроку8( " ms" ); Log.пВК_ПС;
	кон MeasureTime;


	проц DesEcb2*( c : Commands.Context );
		перем input, output, key: массив 64 из симв8;
	нач
		key := "0123456789ABCDEF";
		input := "4E6F772069732074";		output := "3FA40E8A984D4815";
		Ecb2( c, "CryptoDES", input, output, key, 8, 64 );
	кон DesEcb2;

	проц Des3Ecb2*( c : Commands.Context );
		перем input, output, key: массив 64 из симв8;
	нач
		key := "0123456789ABCDEF23456789ABCDEF01456789ABCDEF0123";
		input := "4E6F772069732074";		output := "314F8327FA7A09A8";
		Ecb2( c, "CryptoDES3", input, output, key, 8, 192 );
	кон Des3Ecb2;

	проц  IdeaEcb2*( c : Commands.Context );
		перем input, output, key: массив 64 из симв8;
	нач
		key := "00010002000300040005000600070008";
		input := "0000000100020003";		output := "11FBED2B01986DE5";
		Ecb2( c, "CryptoIDEA", input, output, key, 8, 128 );
	кон IdeaEcb2;

	проц  AesEcb2*( c : Commands.Context );
		перем input, output, key: массив 128 из симв8;
	нач
		key := "8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b";
		input := "6bc1bee22e409f96e93d7e117393172a";
		output := "bd334f1d6e45f25ff712a214571fa5cc";
		Ecb2( c, "CryptoAES", input, output, key, 16, 192 );

		key := "603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4";
		input := "6bc1bee22e409f96e93d7e117393172a";
		output := "f3eed1bdb5d2a03c064b5a7e3db181f8";
		Ecb2( c, "CryptoAES", input, output, key, 16, 256 );
	кон AesEcb2;


	проц  Arc4Ecb2*( c : Commands.Context );
		перем input, output, key: массив 64 из симв8;
	нач
		key := "0123456789abcdef";
		input := "0123456789abcdef";		output := "75b7878099e0c596";
		Ecb2( c, "CryptoARC4", input, output, key, 8, 64 );
	кон Arc4Ecb2;


	проц  CastEcb2*( c : Commands.Context );
		перем input, output, key: массив 64 из симв8;
	нач
		key := "0123456712345678234567893456789A";
		input := "0123456789abcdef";		output := "238B4FE5847E44B2";
		Ecb2( c, "CryptoCAST", input, output, key, 8, 128 );

		output := "EB6A711A2C02271B";
		Ecb2( c, "CryptoCAST", input, output, key, 8, 80 );

		output := "7AC816D16E9B302E";
		Ecb2( c, "CryptoCAST", input, output, key, 8, 40 );
	кон CastEcb2;




	проц  DesCbc2*( c : Commands.Context );
		перем input, output, key, iv: массив 64 из симв8;
	нач
		key := "0123456789ABCDEF";
		iv := "0123456789ABCDEF";
		input := "4E6F772069732074";		output := "96C3D4A6DC1C0117";
		Cbc2( c, "CryptoDES", input, output, key, iv, 8, 64 );
	кон DesCbc2;

	проц  IdeaCbc2*( c : Commands.Context );
		перем input, output, key, iv: массив 64 из симв8;
	нач
		key := "00010002000300040005000600070008";
		iv := "0000000000000000";
		input := "0000000100020003";		output := "11FBED2B01986DE5";
		Cbc2( c, "CryptoIDEA", input, output, key, iv, 8, 128 );
	кон IdeaCbc2;

	проц  AesCbc2*( c : Commands.Context );
		перем input, output, key, iv: массив 256 из симв8;
	нач
		key := "2b7e151628aed2a6abf7158809cf4f3c";
		iv := "000102030405060708090A0B0C0D0E0F";
		input := "6bc1bee22e409f96e93d7e117393172a";
		output := "7649abac8119b246cee98e9b12e9197d";
		Cbc2( c, "CryptoAES", input, output, key, iv, 16, 128 );

		key := "603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4";
		iv := "000102030405060708090A0B0C0D0E0F";
		input := "6bc1bee22e409f96e93d7e117393172a";
		output := "f58c4c04d6e5f1ba779eabfb5f7bfbd6";
		Cbc2( c, "CryptoAES", input, output, key, iv, 16, 256 );

		key := "2b7e151628aed2a6abf7158809cf4f3c";
		iv := "000102030405060708090A0B0C0D0E0F";
		output := "6bc1bee22e409f96e93d7e117393172a";
		input := "7649abac8119b246cee98e9b12e9197d";
		DecryptCbc2( c, "CryptoAES", input, output, key, iv, 16, 128 );

		key := "603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4";
		iv := "000102030405060708090A0B0C0D0E0F";
		output := "6bc1bee22e409f96e93d7e117393172a";
		input := "f58c4c04d6e5f1ba779eabfb5f7bfbd6";
		DecryptCbc2( c, "CryptoAES", input, output, key, iv, 16, 256 );
	кон AesCbc2;

	проц  AesCtr2*( c : Commands.Context );
		перем input, output, key, iv: массив 256 из симв8;
	нач
		key := "2b7e151628aed2a6abf7158809cf4f3c";
		iv := "f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff";
		input := "6bc1bee22e409f96e93d7e117393172a";
		output := "874d6191b620e3261bef6864990db6ce";
		Ctr2( c, "CryptoAES", input, output, key, iv, 16, 128 );

		key := "603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4";
		iv := "f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff";
		input := "6bc1bee22e409f96e93d7e117393172a";
		output := "601ec313775789a5b7a7f504bbf3d228";
		Ctr2( c, "CryptoAES", input, output, key, iv, 16, 256 );
	кон AesCtr2;

	проц  CbcRandom*( c : Commands.Context );
		перем
			bindata, binkey, modname, iv, orig: массив 2 * 64 + 1 из симв8; 
			cipher: Ciphers.Cipher;
			keybits: цел32;
	нач
		(* read in the parameter *)
		c.arg.ПропустиБелоеПоле; c.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(modname);
		c.arg.ПропустиБелоеПоле; c.arg.чЦел32(keybits, ложь);
				(* encryption *)
		cipher := Ciphers.NewCipher( modname );
		U.RandomBytes(binkey, 0, keybits DIV 8);
		U.RandomBytes(bindata, 0, 2 * cipher.blockSize); (* test data at least 2 blocks to test CBC *)
		cipher.InitKey( binkey, keybits );
		U.RandomBytes( iv, 0, cipher.blockSize );
		(*U.RandomBytes( bindata, 0, cipher.blockSize);*)
		cipher.SetIV( iv, Ciphers.CBC );
		Log.пВК_ПС; Log.пСтроку8( "*********************************" );
		Log.пВК_ПС; Log.пСтроку8( "Encrypt-Decrypt-Test in CBC-mode: " ); Log.пСтроку8( cipher.name );
		Log.пВК_ПС; Log.пСтроку8( "Key: " ); U.PrintHex( binkey, 0, keybits DIV 8 );
		Log.пВК_ПС; Log.пСтроку8( "IV: "); U.PrintHex( iv, 0, cipher.blockSize );
		Log.пВК_ПС; Log.пСтроку8( "Original: " ); U.PrintHex( bindata, 0, 2 * cipher.blockSize );
		orig := bindata;  orig[2 * cipher.blockSize] := 0X;
		cipher.Encrypt( bindata, 0, 2 * cipher.blockSize );
		Log.пВК_ПС; Log.пСтроку8( "Encrypted: " ); U.PrintHex( bindata, 0, 2 * cipher.blockSize );
		(* decryption *)
		cipher.SetIV( iv, Ciphers.CBC );
		cipher.Decrypt( bindata, 0, 2 * cipher.blockSize );
		Log.пВК_ПС; Log.пСтроку8( "Decrypted: " ); U.PrintHex( bindata, 0, 2 * cipher.blockSize );
		bindata[2 * cipher.blockSize] := 0X;
		Log.пВК_ПС;
		если bindata = orig то  Log.пСтроку8( "OK" )  иначе  Log.пСтроку8( "FAIL" )  всё;
		Log.пВК_ПС
	кон CbcRandom;
кон CryptoTestCiphers.


     System.Free
     		CryptoTestCiphers CryptoDES3 CryptoDES CryptoIDEA
     		CryptoARC4  CryptoCAST CryptoAES ~

     CryptoTestCiphers.DesEcb2 ~
     CryptoTestCiphers.Des3Ecb2 ~
     CryptoTestCiphers.IdeaEcb2 ~
     CryptoTestCiphers.Arc4Ecb2 ~
     CryptoTestCiphers.CastEcb2 ~
	CryptoTestCiphers.AesEcb2 ~

     CryptoTestCiphers.DesCbc2 ~
     CryptoTestCiphers.IdeaCbc2 ~
     CryptoTestCiphers.AesCbc2 ~

     CryptoTestCiphers.AesCtr2 ~



     CryptoTestCiphers.Ecb1	CryptoDES	64 ~
     CryptoTestCiphers.Ecb1	CryptoDES3	192 ~
     CryptoTestCiphers.Ecb1	CryptoIDEA	128 ~
     CryptoTestCiphers.Ecb1	CryptoARC4	128 ~
     CryptoTestCiphers.Ecb1	CryptoCAST	128 ~
     CryptoTestCiphers.Ecb1	CryptoAES	128 ~
     CryptoTestCiphers.Ecb1	CryptoAES	256 ~
     CryptoTestCiphers.Ecb1	CryptoBlowfish 256 ~

     CryptoTestCiphers.Cbc1	CryptoDES	64 ~
     CryptoTestCiphers.Cbc1	CryptoDES3	192 ~
     CryptoTestCiphers.Cbc1	CryptoIDEA	128 ~
     CryptoTestCiphers.Cbc1	CryptoAES 128 ~
     CryptoTestCiphers.Cbc1	CryptoAES 256 ~
     CryptoTestCiphers.Cbc1	CryptoBlowfish 256 ~

     CryptoTestCiphers.CbcRandom CryptoDES	64 ~
     CryptoTestCiphers.CbcRandom CryptoDES3	192 ~
     CryptoTestCiphers.CbcRandom CryptoIDEA	128 ~
     CryptoTestCiphers.CbcRandom CryptoAES 128 ~
     CryptoTestCiphers.CbcRandom CryptoAES 256 ~
     CryptoTestCiphers.CbcRandom CryptoBlowfish 256 ~

     CryptoTestCiphers.Ctr1		CryptoAES	128 ~
     CryptoTestCiphers.Ctr1		CryptoAES	256 ~


     CryptoTestCiphers.MeasureTime	CryptoDES	ECB 64 ~
     CryptoTestCiphers.MeasureTime	CryptoDES	CBC 64 ~
     CryptoTestCiphers.MeasureTime	CryptoDES3	ECB 192 ~
     CryptoTestCiphers.MeasureTime	CryptoDES3	CBC 192 ~
     CryptoTestCiphers.MeasureTime	CryptoAES	ECB 128 ~
     CryptoTestCiphers.MeasureTime	CryptoAES	CBC 128 ~
     CryptoTestCiphers.MeasureTime	CryptoIDEA	ECB 128 ~
     CryptoTestCiphers.MeasureTime	CryptoIDEA	CBC 128 ~
     CryptoTestCiphers.MeasureTime	CryptoARC4	ECB 128 ~




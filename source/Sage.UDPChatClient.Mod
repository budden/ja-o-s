модуль UDPChatClient; (** AUTHOR "SAGE"; PURPOSE "UDP Chat Client" *)

использует
	Base := UDPChatBase, UDP, IP, DNS,
	Dates, Строки8,
	WMStandardComponents, WMComponents, WM := WMWindowManager,
	WMDialogs, WMEditors, WMRectangles,
	Modules, Texts, UTF8Strings, Inputs, Kernel, Events;

конст
	serverStr = "127.0.0.1";

	branchInit					= 0;
	branchPacketReceive	= 1;
	branchVersionCheck	= 2;
	branchPacketHandle	= 3;
	branchEnd					= 4;
	branchTerminated		= 5;

	moduleName = "UDPChatClient";

	(* Event classification as in Events.XML *)
	EventClass = 3; (* UDP Chat *)
	EventSubclass = 3; (* UDP Chat Client *)

	(* Window size at application startup *)
	WindowWidth = 40 * 12;
	WindowHeight = 30 * 12;

тип

	msg = массив 1500 из симв8; (* Maximum allowed message length caused by Network MTU limit *)

	String = Строки8.уСтрока;

	Instance = окласс
	перем
		next: Instance;

		chat: ChatWindow;
		server: массив 256 из симв8;
		CRLF: массив 3 из симв8;

		login: массив 9 из симв8;
		password, passwordConfirm: массив 33 из симв8;
		shortName, fullName, eMail: массив 65 из симв8;

		uin: цел32; res: целМЗ;
		dt: Dates.DateTime;

		keepAliveTimer: Kernel.MilliTimer;

		s: UDP.Socket;
		serverIP, ip: IP.Adr;
		running, terminated, onLine: булево;

		str1, str2: массив 256 из симв8;

		branch, command, seqNum, messageType, inSeqNum, outSeqNum: цел16;
		senderUin, receiverUin, port, receiveBufOffset: цел32;
		len: размерМЗ;
		sendBuf-: Base.Buffer;

		receiveBuf, message, string: String;

		userInfos: Base.List;
		userInfo: Base.UserInfo;

		ACKReqList: Base.List;
		ACKReq: Base.ACKRec;

		csa: Texts.CharacterStyleArray;
		psa: Texts.ParagraphStyleArray;

	проц &New*;
	нач

		(* Chain the previous instance(s) to this new one, for guaranteed cleanup. *)
		next := instances;
		instances := сам
	кон New;

	проц Finalize;
	нач

		если chat # НУЛЬ то chat.Close всё;

		running := ложь;

		нач {единолично}
			дождись (terminated)
		кон;

		FreeInstance (сам);

	кон Finalize;

	проц Client_ACK (seqNum: цел16; uin: цел32; sendBuf: Base.Buffer;
		s: UDP.Socket; ip: IP.Adr);
	перем
		res: целМЗ;
		string: String;
	нач {единолично}
		Base.ClientPacketInit (Base.ACK, seqNum, uin, sendBuf);
		string := sendBuf.ДайСсылкуНаТекущиеДанные ();
		s.Send (ip, Base.serverPort, string^, 0, sendBuf.ДайКвоБайт˛неСчитаяЗавершающего0 (), res);
	кон Client_ACK;

	проц Client_NewUserReg (password, shortName, fullName, eMail: массив из симв8;
		перем seqNum: цел16; sendBuf: Base.Buffer;
		s: UDP.Socket; ip: IP.Adr);
	перем
		len: размерМЗ; res: целМЗ;
		string: String;
	нач {единолично}
		Base.ClientPacketInit (Base.NEW_USER_REG, seqNum, 0, sendBuf);

		нов (ACKReq);
		ACKReq.seqNum := seqNum;
		ACKReqList.Add (ACKReq);

		увел (seqNum);

		len := Строки8.КвоБайтБезЗавершающего0 (password) + 1;
		sendBuf.AddInt (len, 2);
		sendBuf.РеализацияЗаписиВПоток (password, 0, len, истина, res);

		len := Строки8.КвоБайтБезЗавершающего0 (shortName) + 1;
		sendBuf.AddInt (len, 2);
		sendBuf.РеализацияЗаписиВПоток (shortName, 0, len, истина, res);

		len := Строки8.КвоБайтБезЗавершающего0 (fullName) + 1;
		sendBuf.AddInt (len, 2);
		sendBuf.РеализацияЗаписиВПоток (fullName, 0, len, истина, res);

		len := Строки8.КвоБайтБезЗавершающего0 (eMail) + 1;
		sendBuf.AddInt (len, 2);
		sendBuf.РеализацияЗаписиВПоток (eMail, 0, len, истина, res);

		string := sendBuf.ДайСсылкуНаТекущиеДанные ();
		s.Send (ip, Base.serverPort, string^, 0, sendBuf.ДайКвоБайт˛неСчитаяЗавершающего0 (), res);
	кон Client_NewUserReg;

	проц Client_Login (password: массив из симв8;
		перем seqNum: цел16; uin: цел32; sendBuf: Base.Buffer;
		s: UDP.Socket; ip: IP.Adr);
	перем
		len: размерМЗ; res: целМЗ;
		string: String;
	нач {единолично}
		Base.ClientPacketInit (Base.LOGIN, seqNum, uin, sendBuf);

		нов (ACKReq);
		ACKReq.seqNum := seqNum;
		ACKReqList.Add (ACKReq);

		увел (seqNum);

		len := Строки8.КвоБайтБезЗавершающего0 (password) + 1;
		sendBuf.AddInt (len, 2);
		sendBuf.РеализацияЗаписиВПоток (password, 0, len, истина, res);

		string := sendBuf.ДайСсылкуНаТекущиеДанные ();
		s.Send (ip, Base.serverPort, string^, 0, sendBuf.ДайКвоБайт˛неСчитаяЗавершающего0 (), res);
	кон Client_Login;

	проц Client_InfoReq (userUIN: цел32; перем seqNum: цел16;
		uin: цел32; sendBuf: Base.Buffer;
		s: UDP.Socket; ip: IP.Adr);
	перем
		res: целМЗ;
		string: String;
	нач {единолично}
		Base.ClientPacketInit (Base.INFO_REQ, seqNum, uin, sendBuf);

		нов (ACKReq);
		ACKReq.seqNum := seqNum;
		ACKReqList.Add (ACKReq);

		увел (seqNum);

		sendBuf.AddInt (userUIN, 4);

		string := sendBuf.ДайСсылкуНаТекущиеДанные ();
		s.Send (ip, Base.serverPort, string^, 0, sendBuf.ДайКвоБайт˛неСчитаяЗавершающего0 (), res);
	кон Client_InfoReq;

	проц Client_SendMessage (
		userUIN: цел32; messageType: цел16; message: String;
		перем seqNum: цел16; uin: цел32; sendBuf: Base.Buffer;
		s: UDP.Socket; ip: IP.Adr);
	перем
		string: String;
		len: размерМЗ; res: целМЗ;
	нач {единолично}
		Base.ClientPacketInit (Base.SEND_MESSAGE, seqNum, uin, sendBuf);

		нов (ACKReq);
		ACKReq.seqNum := seqNum;
		ACKReqList.Add (ACKReq);

		увел (seqNum);

		sendBuf.AddInt (userUIN, 4);

		sendBuf.AddInt (messageType, 2);

		(*
		len := Strings.Length (message^) + 1;
		*)
		len := длинаМассива (message^);

		sendBuf.AddInt (len, 2);
		sendBuf.РеализацияЗаписиВПоток (message^, 0, len, истина, res);

		string := sendBuf.ДайСсылкуНаТекущиеДанные ();
		s.Send (serverIP, Base.serverPort, string^, 0, sendBuf.ДайКвоБайт˛неСчитаяЗавершающего0 (), res);
	кон Client_SendMessage;

	проц Client_SendTextCode (code: String;
		перем seqNum: цел16; uin: цел32; sendBuf: Base.Buffer;
		s: UDP.Socket; ip: IP.Adr);
	перем
		string: String;
		len: размерМЗ; res: целМЗ;
	нач {единолично}
		Base.ClientPacketInit (Base.SEND_TEXT_CODE, seqNum, uin, sendBuf);

		нов (ACKReq);
		ACKReq.seqNum := seqNum;
		ACKReqList.Add (ACKReq);

		увел (seqNum);

		len := Строки8.КвоБайтБезЗавершающего0 (code^) + 1;
		sendBuf.AddInt (len(цел32), 2);
		sendBuf.РеализацияЗаписиВПоток (code^, 0, len, истина, res);

		string := sendBuf.ДайСсылкуНаТекущиеДанные ();
		s.Send (serverIP, Base.serverPort, string^, 0, sendBuf.ДайКвоБайт˛неСчитаяЗавершающего0 (), res);
	кон Client_SendTextCode;

	проц Client_KeepAlive (перем seqNum: цел16; uin: цел32; sendBuf: Base.Buffer;
		s: UDP.Socket; ip: IP.Adr);
	перем
		res: целМЗ;
		string: String;
	нач {единолично}
		Base.ClientPacketInit (Base.KEEP_ALIVE, seqNum, uin, sendBuf);

		нов (ACKReq);
		ACKReq.seqNum := seqNum;
		ACKReqList.Add (ACKReq);

		увел (seqNum);

		string := sendBuf.ДайСсылкуНаТекущиеДанные ();
		s.Send (ip, Base.serverPort, string^, 0, sendBuf.ДайКвоБайт˛неСчитаяЗавершающего0 (), res);
	кон Client_KeepAlive;

	проц FindUserInfo (list: Base.List; uin: цел32): Base.UserInfo;
	перем
		i: цел32;
		u: Base.UserInfo;
		ptr: динамическиТипизированныйУкль;
	нач
		i := 0;
		нцПока i < list.GetCount () делай
			ptr := list.GetItem (i);
			u := ptr (Base.UserInfo);
			если uin = u.uin то
				возврат u;
			всё;
			увел (i);
		кц;
		возврат НУЛЬ;
	кон FindUserInfo;

	проц Log (type, code : цел8; msg: массив из симв8; showOnKernelLog : булево);
	перем message : Events.Message;
	нач
		копируйСтрокуДо0(msg, message);
		Events.AddEvent(moduleName, type, EventClass, EventSubclass, code, message, showOnKernelLog);
	кон Log;

	нач {активное}

		branch := branchInit;

		нцДо

			просей branch из
			| branchInit:

				server := serverStr;
				running := ложь;
				terminated := истина;
				onLine := ложь;

				branch := branchEnd;

				csa := Texts.GetCharacterStyleArray ();
				psa := Texts.GetParagraphStyleArray ();

				res := WMDialogs.QueryString ("Server", server);

				если res = WMDialogs.ResOk то

					DNS.HostByName (server, serverIP, res);

					если res # DNS.Ok то

						Log (Events.Error, 0, "host name not found!", истина);

						serverIP := IP.StrToAdr (server);

						если IP.IsNilAdr (serverIP) то

							Log (Events.Error, 0, "IP address not valid!", истина);

						всё;

					всё;

					если ~IP.IsNilAdr (serverIP) то

						CRLF[0] := 0DX;
						CRLF[1] := 0AX;
						CRLF[2] := 0X;

						нов (s, UDP.NilPort, res);

						нов (receiveBuf, Base.MaxUDPDataLen);
						нов (sendBuf, 0);
						нов (ACKReqList);

						running := истина;
						terminated := ложь;
						onLine := ложь;

						inSeqNum := -1;
						outSeqNum := 1;

						res := WMDialogs.Message (WMDialogs.TQuestion, "Chat Client", "Get new User ID?",
							{WMDialogs.ResYes, WMDialogs.ResNo});

						просей res из
						| WMDialogs.ResYes:

							res := WMDialogs.QueryUserInfo ("Register new user",
								shortName, fullName, eMail, password, passwordConfirm);

							если res = WMDialogs.ResOk то

								если (shortName # "") и
									(password # "") и
									(password = passwordConfirm) то

									Client_NewUserReg (password, shortName, fullName,
										eMail, outSeqNum, sendBuf, s, serverIP);

									branch := branchPacketReceive;

								всё;

							всё;

						| WMDialogs.ResNo:

							res := WMDialogs.QueryLogin ("Login", login, password);

							если res = WMDialogs.ResOk то

								Строки8.ПрочтиЦел32_изСтроки (login, uin);

								если uin # 0 то

									нов (chat, сам);
									Client_Login (password, outSeqNum, uin,
										sendBuf, s, serverIP);

									branch := branchPacketReceive;

								всё;

							всё;

						иначе

						всё;

					всё;

				всё;

			| branchPacketReceive:

				если running то

					s.Receive (receiveBuf^, 0, Base.MaxUDPDataLen, 1, ip, port, len, res);

					если (res = UDP.Ok) и (len > 0) то

						receiveBufOffset := 0;

						branch := branchVersionCheck;

					иначе

						branch := branchPacketReceive;

					всё;

					если onLine то
						если Kernel.Expired (keepAliveTimer) то
							Client_KeepAlive (outSeqNum, uin, sendBuf, s, serverIP);
							Kernel.SetTimer (keepAliveTimer, Base.clientKeepAliveInterval);
						всё;
					всё;

				иначе

					branch := branchEnd;

				всё;

			| branchVersionCheck:

				если Base.BufGetInt (receiveBuf, receiveBufOffset) = Base.VERSION то

					branch := branchPacketHandle;

				иначе

					branch := branchPacketReceive;

				всё;

			| branchPacketHandle:

				command := Base.BufGetInt (receiveBuf, receiveBufOffset);
				seqNum := Base.BufGetInt (receiveBuf, receiveBufOffset);

				Строки8.ПишиЦел64_вСтроку (seqNum, str1);
				Строки8.Склей (" SeqNum: ", str1, str1);
				Строки8.Склей (str1, " Command: ", str1);

				Строки8.ПишиЦел64_вСтроку (uin, str2);
				Строки8.Склей ("User ID: ", str2, str2);
				Строки8.Склей (str2, str1, str1);

				Base.CommandDecode (command, str2);
				Строки8.Склей (str1, str2, str1);

				Log (Events.Information, 0, str1, ложь);

				если onLine то

					просей command из
					| Base.ACK:

						если Base.SeqNumInACKList (ACKReqList, seqNum, ACKReq) то

							ACKReqList.Remove (ACKReq);

						всё;

					| Base.INFO_REPLY:

						если Base.SeqNumInACKList (ACKReqList, seqNum, ACKReq) то

							ACKReqList.Remove (ACKReq);

							receiverUin := Base.BufGetLInt (receiveBuf, receiveBufOffset);

							userInfo := FindUserInfo (userInfos, receiverUin);
							если userInfo = НУЛЬ то
								нов (userInfo);
								userInfos.Add (userInfo);
								userInfo.uin := receiverUin;
							всё;

							string := Base.BufGetString (receiveBuf, receiveBufOffset);
							копируйСтрокуДо0 (string^, userInfo.shortName);

							Строки8.ПишиЦел64_вСтроку (receiverUin, str1);
							Строки8.Склей ("User with User ID: #", str1, str1);
							Строки8.Склей (str1, " now known as '", str1);
							Строки8.Склей (str1, userInfo.shortName, str1);
							Строки8.Склей (str1, "'", str1);
							Строки8.Склей (CRLF, str1, str1);

							chat.Append (Строки8.ЯвиУСтроку (str1), csa[8], psa[1]);

						всё;

					иначе (* CASE *)

						если Base.isNextSeqNum (seqNum, inSeqNum) то

							inSeqNum := seqNum;

							Client_ACK (inSeqNum, uin, sendBuf, s, serverIP);

							просей command из
							| Base.USER_ONLINE:

								receiverUin := Base.BufGetLInt (receiveBuf, receiveBufOffset);

								Строки8.ПишиЦел64_вСтроку (receiverUin, str1);
								Строки8.Склей ("User with User ID: #", str1, str1);

								userInfo := FindUserInfo (userInfos, receiverUin);
								если userInfo = НУЛЬ то
									Client_InfoReq (receiverUin, outSeqNum, uin, sendBuf, s, serverIP);
								иначе
									Строки8.Склей (str1, " known as '", str1);
									Строки8.Склей (str1, userInfo.shortName, str1);
									Строки8.Склей (str1, "'", str1);
								всё;

								Строки8.Склей (str1, " is ON-LINE!", str1);
								Строки8.Склей (CRLF, str1, str1);

								chat.Append (Строки8.ЯвиУСтроку (str1), csa[8], psa[1]);

							| Base.USER_OFFLINE:

								receiverUin := Base.BufGetLInt (receiveBuf, receiveBufOffset);
								Строки8.ПишиЦел64_вСтроку (receiverUin, str1);
								Строки8.Склей ("User with User ID: #", str1, str1);

								userInfo := FindUserInfo (userInfos, receiverUin);
								если userInfo # НУЛЬ то
									Строки8.Склей (str1, " known as '", str1);
									Строки8.Склей (str1, userInfo.shortName, str1);
									Строки8.Склей (str1, "'", str1);
								всё;

								Строки8.Склей (str1, " is OFF-LINE!", str1);
								Строки8.Склей (CRLF, str1, str1);

								chat.Append (Строки8.ЯвиУСтроку (str1), csa[8], psa[1]);

							| Base.RECEIVE_MESSAGE:

								senderUin := Base.BufGetLInt (receiveBuf, receiveBufOffset);

								dt.year := Base.BufGetInt (receiveBuf, receiveBufOffset);
								dt.month := Base.BufGetSInt (receiveBuf, receiveBufOffset);
								dt.day := Base.BufGetSInt (receiveBuf, receiveBufOffset);
								dt.hour := Base.BufGetSInt (receiveBuf, receiveBufOffset);
								dt.minute := Base.BufGetSInt (receiveBuf, receiveBufOffset);
								dt.second := 0;

								messageType := Base.BufGetInt (receiveBuf, receiveBufOffset);

								message := Base.BufGetString (receiveBuf, receiveBufOffset);

								просей messageType из
								| Base.MESSAGE_TYPE_NORMAL:

									userInfo := FindUserInfo (userInfos, senderUin);
									если userInfo = НУЛЬ то
										Строки8.ПишиЦел64_вСтроку (senderUin, str1);
										Строки8.Склей ("#", str1, str1);
									иначе
										копируйСтрокуДо0 (userInfo.shortName, str1);
									всё;

									Строки8.Склей (CRLF, str1, str1);
									chat.Append (Строки8.ЯвиУСтроку (str1), csa[1], psa[0]);

									Строки8.ПишиДатуВремяВСтроку ("yyyy.mm.dd hh:nn:ss", dt, str1);
									Строки8.Склей (" (", str1, str1);
									Строки8.Склей (str1, ")", str1);
									chat.Append (Строки8.ЯвиУСтроку (str1), csa[3], psa[0]);

									message := Строки8.СклейДвеСтрокиВСвежуюУСтроку (CRLF, message^);

									chat.Append (message, csa[0], psa[0]);

								| Base.MESSAGE_TYPE_URL:

								| Base.MESSAGE_TYPE_DATA:
									chat.Append (Строки8.ЯвиУСтроку ("data"), csa[0], psa[0]);
								иначе

								всё;

							иначе

							всё;

						всё;

					всё;

					branch := branchPacketReceive;

				иначе

					если Base.SeqNumInACKList (ACKReqList, seqNum, ACKReq) то

						ACKReqList.Remove (ACKReq);

						просей command из
						| Base.LOGIN_REPLY:

							нов (userInfos);

							onLine := истина;

							Kernel.SetTimer (keepAliveTimer, Base.clientKeepAliveInterval);

							Client_InfoReq (uin, outSeqNum, uin, sendBuf, s, serverIP);

						| Base.NEW_USER_REPLY:

							uin := Base.BufGetLInt (receiveBuf, receiveBufOffset);

							Строки8.ПишиЦел64_вСтроку (uin, login);
							Строки8.Склей ("Remember your User ID: ", login, str1);

							WMDialogs.Information ("New user registered", str1);

							res := WMDialogs.QueryLogin ("Login", login, password);
							если res = WMDialogs.ResOk то
								Строки8.ПрочтиЦел32_изСтроки (login, uin);

								если uin # 0 то

									нов (chat, сам);
									Client_Login (password, outSeqNum, uin, sendBuf, s, serverIP);

								всё;

							всё;

						иначе

						всё;

					всё;

					branch := branchPacketReceive;

				всё;

			| branchEnd:

				нач {единолично}
					terminated := истина
				кон;

				branch := branchTerminated;

			иначе

			всё;

		кцПри branch = branchTerminated;

	кон Instance;

	ChatWindow = окласс (WMComponents.FormWindow)
	перем
		instance: Instance;
		editSend*, editChat*: WMEditors.Editor;
		buttonSend: WMStandardComponents.Button;

		проц {перекрыта}Close*;
		нач
			Close^;
			если instance.onLine то
				instance.Client_SendTextCode (Строки8.ЯвиУСтроку("USER_DISCONNECTED"),
					instance.outSeqNum, instance.uin, instance.sendBuf, instance.s, instance.serverIP);
			всё;
		кон Close;

		проц {перекрыта}KeyEvent*(ucs: размерМЗ; flags: мнвоНаБитахМЗ; keysym: размерМЗ);
		нач
			если Inputs.Release в flags то возврат всё;
			если (keysym = 0FF0DH) и (flags * Inputs.Ctrl # {})  то (* Ctrl + Enter *)
				SendClick (сам, НУЛЬ);
			всё;
		кон KeyEvent;

		проц Append (message: String; cs: Texts.CharacterStyle; ps: Texts.ParagraphStyle);
		перем
			len, idx: размерМЗ;
			ucs32: Texts.PUCS32String;
		нач

			нов (ucs32, Строки8.КвоБайтБезЗавершающего0 (message^) + 1);
			idx := 0;
			UTF8Strings.UTF8toUnicode (message^, ucs32^, idx);

			editChat.text.AcquireRead;
			len := editChat.text.GetLength ();
			editChat.text.ReleaseRead;

			editChat.text.AcquireWrite;
			editChat.text.InsertUCS32 (len, ucs32^);
			editChat.text.SetCharacterStyle (len, idx-1, cs);
			editChat.text.SetParagraphStyle (len+2, idx-3, ps);
			editChat.text.ReleaseWrite;

			editChat.tv.End (истина, ложь);

		кон Append;

		проц SendClick (sender, data:динамическиТипизированныйУкль);
		перем
			message: msg;
			string: String;
		нач

			editSend.text.AcquireRead;

			(*
			NEW (string, editSend.text.GetLength () * 2 + 1); (* GetLength () returns nuber of characters, not bytes!!! *)
			editSend.GetAsString (string^); (* text that appears in string are in UTF8 encoding *)
			*)

			editSend.GetAsString (message);
			нов (string, Строки8.КвоБайтБезЗавершающего0 (message) + 1);
			копируйСтрокуДо0 (message, string^);

			editSend.text.ReleaseRead;

			editSend.SetAsString ("");
			если instance.onLine то
				instance.Client_SendMessage (
					0, Base.MESSAGE_TYPE_NORMAL, string, instance.outSeqNum, instance.uin,
					instance.sendBuf, instance.s, instance.serverIP);
			всё;

		кон SendClick;

		проц CreateForm (): WMComponents.VisualComponent;
		перем
			panel, sendPanel, buttonPanel: WMStandardComponents.Panel;
			resizerV : WMStandardComponents.Resizer;
			manager: WM.WindowManager;
			windowStyle: WM.WindowStyle;
		нач
			manager := WM.GetDefaultManager ();
			windowStyle := manager.GetStyle ();

			нов (panel);
			panel.bounds.SetExtents (WindowWidth, WindowHeight);
			panel.fillColor.Set (windowStyle.bgColor);
			panel.takesFocus.Set (ложь);

			нов(buttonPanel);
			buttonPanel.alignment.Set(WMComponents.AlignBottom); buttonPanel.bounds.SetHeight(20);
			buttonPanel.bearing.Set(WMRectangles.MakeRect(12, 0, 12, 12));
			panel.AddContent(buttonPanel);

			нов (buttonSend); buttonSend.caption.SetAOC ("Send");
			buttonSend.alignment.Set(WMComponents.AlignRight);
			buttonSend.onClick.Add (SendClick);
			buttonPanel.AddContent (buttonSend);

			нов(sendPanel);
			sendPanel.alignment.Set(WMComponents.AlignBottom); sendPanel.bounds.SetHeight(5 * 12 + 20);
			sendPanel.fillColor.Set(windowStyle.bgColor);
			panel.AddContent(sendPanel);

			нов(resizerV);
			resizerV.alignment.Set(WMComponents.AlignTop);
			resizerV.bounds.SetHeight(4);
			sendPanel.AddContent(resizerV);

			нов (editSend);
			editSend.tv.defaultTextColor.Set (windowStyle.fgColor);
			editSend.tv.defaultTextBgColor.Set (windowStyle.bgColor);
			editSend.bearing.Set(WMRectangles.MakeRect(12, 12, 12, 12));
			editSend.alignment.Set(WMComponents.AlignClient);
			editSend.multiLine.Set (истина); editSend.tv.borders.Set (WMRectangles.MakeRect(5, 2, 3, 2));
			editSend.tv.showBorder.Set (истина);
			sendPanel.AddContent (editSend);

			нов (editChat);
			editChat.tv.defaultTextColor.Set (windowStyle.fgColor);
			editChat.tv.defaultTextBgColor.Set (windowStyle.bgColor);
			editChat.bearing.Set(WMRectangles.MakeRect(12, 12, 12,12));
			editChat.alignment.Set(WMComponents.AlignClient);
			editChat.readOnly.Set (истина);
			editChat.multiLine.Set (истина); editChat.tv.borders.Set (WMRectangles.MakeRect (5, 2, 3, 2));
			editChat.tv.showBorder.Set (истина);
			panel.AddContent(editChat);

			возврат panel
		кон CreateForm;

		проц &New *(inst: Instance);
		перем
			vc: WMComponents.VisualComponent;
			vp: WM.ViewPort;
			i, j: размерМЗ;
			str: массив 128 из симв8;
		нач

			instance := inst;

			vc := CreateForm ();
			i := vc.bounds.GetWidth ();
			j := vc.bounds.GetHeight ();
			Init (i, j, ложь);
			SetContent (vc);

			vp := WM.GetDefaultView ();

			WM.AddWindow (сам,
				(округлиВниз (vp.range.r - vp.range.l) - i) DIV 2,
				(округлиВниз (vp.range.b - vp.range.t) - j) DIV 2);

			копируйСтрокуДо0 ("Chat - ", str);
			Строки8.ПодклейВСтрокуХвост (str, instance.login);
			SetTitle (WM.NewString (str));

		кон New;

	кон ChatWindow;

перем
	instances: Instance;

(* Remove the instance from the linked list *)
проц FreeInstance (free: Instance);
перем
	instance: Instance;
нач
	если free = instances то		(* the element to free is the first in list *)
		instances := instances.next
	иначе
		instance := instances;
		нцПока (instance # НУЛЬ) и (instance.next # free) делай
			instance := instance.next
		кц;
		если instance # НУЛЬ то			(* not yet at the end of the chain: unchain it*)
			instance.next := free.next
		всё
	всё
кон FreeInstance;

проц Open*;
перем
	instance: Instance;
нач
	нов (instance);
кон Open;

проц Cleanup;
нач
	нцПока instances # НУЛЬ делай
		instances.Finalize ();
	кц
кон Cleanup;

нач
	Modules.InstallTermHandler (Cleanup);
кон UDPChatClient.

System.Free UDPChatClient ~	UDPChatClient.Open ~

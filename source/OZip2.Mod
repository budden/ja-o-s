модуль OZip2; (** AUTHOR GF; PURPOSE "files and streams compression tool"; *)

использует Потоки, Commands, Files, Strings,
	BW :=  BorrowsWheeler, AH := AdaptiveHuffman;

(*
	Format	=	ComprTag { Block }
	Block		=	size  bwindex  { CHAR }
	bwindex	=	RawNum
	size		=	RawNum
*)

конст
	BlockSize* = 8*1024;
	ComprTag = цел32(0FEFD1F18H);
	Suffix = ".oz2";


	проц Compress*( r: Потоки.Чтец;  w: Потоки.Писарь );
	перем
		huff: AH.Encoder;
		bw: BW.Encoder;
		buffer: массив BlockSize из симв8;  len: размерМЗ;
	нач
		нов( huff, w );  нов( bw );
		w.пЦел32_мз( ComprTag );
		нц
			r.чБайты( buffer, 0, BlockSize, len );
			если len = 0 то  прервиЦикл  всё;
			w.пЦел64_7б( len );
			w.пЦел64_7б( bw.EncodeBlock( buffer, len ) );
			huff.CompressBlock( buffer, len );
		кц;
		w.пЦел64_7б( 0 );	(* mark end of stream *)
		w.ПротолкниБуферВПоток
	кон Compress;


	(* returns false if input is not an OZip2 compressed stream *)
	проц Uncompress*( r: Потоки.Чтец;  w: Потоки.Писарь ): булево;
	перем
		huff: AH.Decoder;
		bw: BW.Decoder;
		tag, len, bwIndex: цел32;
		buffer: массив BlockSize из симв8;
	нач
		r.чЦел32_мз( tag );
		если tag = ComprTag  то
			нов( huff, r );  нов( bw );
			нц
				r.чЦел32_7б( len );
				если len = 0 то  прервиЦикл  всё;
				r.чЦел32_7б( bwIndex );
				huff.ExtractBlock( buffer, len );
				bw.DecodeBlock( buffer, len, bwIndex );
				w.пБайты( buffer, 0, len )
			кц;
			w.ПротолкниБуферВПоток;
			возврат истина
		иначе
			возврат ложь
		всё
	кон Uncompress;




	проц NewFile( конст name: массив из симв8 ): Files.File;
	перем
		tname, name2: массив 128 из симв8;  res: цел32;
	нач
		tname := "./";
		Strings.Append( tname, name );
		если Files.Old( tname ) # НУЛЬ то
			копируйСтрокуДо0( name, name2);  Strings.Append( name2, ".Bak" );
			Files.Rename( name, name2, res );
		всё;
		возврат Files.New( name )
	кон NewFile;



	(** OZip2.CompressFile  infile [outfile] ~ *)
	проц CompressFile*( c: Commands.Context );
	перем
		f1, f2: Files.File;
		r: Files.Reader;  w: Files.Writer;
		name1, name2: массив 128 из симв8;
	нач
		если c.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( name1 ) то
			c.out.пСтроку8( "OZip2.CompressFile " ); c.out.пСтроку8( name1 );  c.out.ПротолкниБуферВПоток;
			если ~c.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( name2 ) то
				name2 := name1;  Strings.Append( name2, Suffix )
			всё;
			f1 := Files.Old( name1 );
			если f1 # НУЛЬ то
				Files.OpenReader( r, f1, 0 );
				f2 := NewFile( name2 );  Files.OpenWriter( w, f2, 0 );
				Compress( r, w );  Files.Register( f2 );
				c.out.пСтроку8( " => " );  c.out.пСтроку8( name2 );  c.out.пВК_ПС;  c.out.ПротолкниБуферВПоток
			иначе
				c.error.пСтроку8( "  ### file not found" );  c.error.пВК_ПС
			всё
		иначе
			c.error.пСтроку8( "usage: OZip2.CompressFile infile [outfile] ~ " );  c.error.пВК_ПС;
		всё;
		c.error.ПротолкниБуферВПоток
	кон CompressFile;


	(** OZip2.UncompressFile  infile [outfile] ~ *)
	проц UncompressFile*( c: Commands.Context );
	перем
		f1, f2: Files.File;
		r: Files.Reader;  w: Files.Writer;
		name1, name2: массив 128 из симв8;
	нач
		если c.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( name1 ) то
			c.out.пСтроку8( "OZip2.UncompressFile " );  c.out.пСтроку8( name1 );  c.out.ПротолкниБуферВПоток;
			если ~c.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( name2 ) то
				name2 := name1;
				если Strings.EndsWith( Suffix, name2 ) то  name2[Strings.Length( name2 ) - 4] := 0X
				иначе  Strings.Append( name2, ".uncomp" )
				всё
			всё;
			f1 := Files.Old( name1 );
			если f1 # НУЛЬ то
				Files.OpenReader( r, f1, 0 );
				f2 := NewFile( name2 );  Files.OpenWriter( w, f2, 0 );
				если Uncompress( r, w ) то
					Files.Register( f2 );
					c.out.пСтроку8( " => " );  c.out.пСтроку8( name2 );  c.out.пВК_ПС;  c.out.ПротолкниБуферВПоток
				иначе
					c.error.пСтроку8( "  ### wrong input (OZip2 compressed data expected)" );  c.error.пВК_ПС
				всё
			иначе
				c.error.пСтроку8( "   ### file not found" );  c.error.пВК_ПС
			всё
		иначе
			c.error.пСтроку8( "usage: OZip2.UncompressFile infile [outfile] ~ " );  c.error.пВК_ПС;
		всё;
		c.error.ПротолкниБуферВПоток
	кон UncompressFile;

кон OZip2.


	OZip2.CompressFile   TLS.Mod ~
	OZip2.CompressFile   OZip2.GofUu ~
	OZip2.CompressFile   guide.pdf ~

	OZip2.UncompressFile   TLS.Mod.oz2  TTT.Mod ~
	OZip2.UncompressFile   OZip2.GofUu.oz2  TTT.GofUu ~
	OZip2.UncompressFile   guide.pdf.oz2  TTT.pdf ~

	System.Free  OZip2 AdaptiveHuffman  BorrowsWheeler ~

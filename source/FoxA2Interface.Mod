модуль FoxA2Interface; (** AUTHOR ""; PURPOSE ""; *)

использует Потоки, Basic := FoxBasic, WMUtilities, TextUtilities, WMGraphics, Diagnostics, Texts;

конст
	Tab = 9X;

тип
	Writer* = окласс (Basic.Writer)
	перем
		alertCount, commentCount, keywordCount : цел32;

		проц &InitA2Writer(l0w: Потоки.Писарь);
		нач
			InitBasicWriter(l0w); alertCount := 0; commentCount := 0; keywordCount := 0;
			если l0w суть TextUtilities.TextWriter то
				l0w(TextUtilities.TextWriter).SetFontName(Texts.defaultAttributes.fontInfo.name);
			всё;
		кон InitA2Writer;

		проц SetFontStyle*(style: мнвоНаБитахМЗ);
		нач
			если w суть TextUtilities.TextWriter то
				w(TextUtilities.TextWriter).SetFontStyle(style);
			всё;
		кон SetFontStyle;

		проц SetColor;
		нач
			возврат; (* prohibitively expensive for large files *)
			если w суть TextUtilities.TextWriter то
				если alertCount > 0 то w(TextUtilities.TextWriter).SetFontColor(WMGraphics.Red);
				аесли commentCount > 0 то w(TextUtilities.TextWriter).SetFontColor(цел32(0999999FFH));
				иначе w(TextUtilities.TextWriter).SetFontColor(WMGraphics.Black);
				всё;
			всё;
		кон SetColor;

		проц SetStyle;
		нач
			возврат; (* prohibitively expensive for large files *)
			если w суть TextUtilities.TextWriter то
				если keywordCount > 0 то w(TextUtilities.TextWriter).SetFontStyle({WMGraphics.FontBold});
				иначе w(TextUtilities.TextWriter).SetFontStyle({});
				всё;
			всё;
		кон SetStyle;

		проц {перекрыта}BeginAlert*;
		нач увел(alertCount); если alertCount = 1 то SetColor всё;
		кон BeginAlert;

		проц {перекрыта}EndAlert*;
		нач умень(alertCount); если alertCount = 0 то SetColor всё;
		кон EndAlert;

		проц {перекрыта}BeginComment*;
		нач увел(commentCount); если commentCount = 1 то SetColor всё;
		кон BeginComment;

		проц {перекрыта}EndComment*;
		нач умень(commentCount); если commentCount = 0 то SetColor всё;
		кон EndComment;

		проц {перекрыта}BeginKeyword*;
		нач увел(keywordCount);  если keywordCount = 1 то SetStyle всё;
		кон BeginKeyword;

		проц {перекрыта}EndKeyword*;
		нач умень(keywordCount); если keywordCount = 0 то SetStyle всё;
		кон EndKeyword;

		проц {перекрыта}AlertString*(конст s: массив из симв8);
		нач
			BeginAlert; w.пСтроку8(s); EndAlert;
		кон AlertString;

	кон Writer;

	StreamDiagnostics* = окласс (Diagnostics.Diagnostics);
	перем
		writer: Потоки.Писарь;

		проц &Init *(w: Потоки.Писарь);
		нач
			утв(w # НУЛЬ);
			writer := w;
		кон Init;

		проц {перекрыта}Error* (конст source : массив из симв8; position : Потоки.ТипМестоВПотоке; конст message : массив из симв8);
		нач Print (writer, source, position, Diagnostics.TypeError, message);
		кон Error;

		проц {перекрыта}Warning* (конст source : массив из симв8; position : Потоки.ТипМестоВПотоке; конст message : массив из симв8);
		нач Print (writer, source, position, Diagnostics.TypeWarning, message);
		кон Warning;

		проц {перекрыта}Information* (конст source : массив из симв8; position : Потоки.ТипМестоВПотоке; конст message : массив из симв8);
		нач Print (writer, source, position, Diagnostics.TypeInformation, message);
		кон Information;

	кон StreamDiagnostics;

	проц Print (w: Потоки.Писарь; конст source : массив из симв8; position: Потоки.ТипМестоВПотоке; type: целМЗ; конст message: массив из симв8);
	перем attributes: Texts.Attributes;
	нач
		если w суть TextUtilities.TextWriter то
			attributes := w(TextUtilities.TextWriter).currentAttributes;
			если (type = Diagnostics.TypeWarning) то
				w(TextUtilities.TextWriter).SetFontColor(цел32(0808000FFH));
			аесли (type = Diagnostics.TypeError) то
				w(TextUtilities.TextWriter).SetFontColor(WMGraphics.Red);
			иначе
				w(TextUtilities.TextWriter).SetFontColor(WMGraphics.Black);
			всё;
		всё;
		w.пСимв8(Tab);
		если (source # "") то w.пСтроку8 (source); всё;
		если (position # Потоки.НевернаяПозицияВПотоке) то w.пСимв8 ('@'); w.пЦел64(position, 0); всё;
		w.пСимв8(Tab);
		если (type = Diagnostics.TypeWarning) то
			w.пСтроку8("warning");
		аесли (type = Diagnostics.TypeError) то
			w.пСтроку8("error");
		иначе
		всё;
		если (type # Diagnostics.TypeInformation) то w.пСтроку8(": ") всё;
		w.пСтроку8(message); w.пВК_ПС;
		w.ПротолкниБуферВПоток;
		если attributes # НУЛЬ то
			w(TextUtilities.TextWriter).SetAttributes(attributes);
		всё;
	кон Print;

	проц DebugWriterFactory(конст title: массив из симв8): Потоки.Писарь;
	перем writer: WMUtilities.WindowWriter;
	нач
		нов(writer,title,600,400,ложь); возврат writer
	кон DebugWriterFactory;

	проц WriterFactory(w: Потоки.Писарь): Basic.Writer;
	перем writer: Writer;
	нач
		нов(writer,w);
		возврат writer
	кон WriterFactory;

	проц DiagnosticsFactory(w: Потоки.Писарь): Diagnostics.Diagnostics;
	перем diagnostics: StreamDiagnostics;
	нач
		нов(diagnostics, w);
		возврат diagnostics
	кон DiagnosticsFactory;

	проц Install*;
	нач
		Basic.InstallWriterFactory(WriterFactory, DebugWriterFactory, DiagnosticsFactory);
	кон Install;

кон FoxA2Interface.

FSTools.DeleteFiles FoxA2Interface.Obw ~

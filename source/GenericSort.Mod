модуль GenericSort; (** AUTHOR "Luc Blaeser"; PURPOSE "Generic Sort Functionality" *)
тип
	GenericArray* = укль на массив из динамическиТипизированныйУкль;
	(** has to return true iff obj1 occurs before obj2 *)
	GenericCompareFunct* = проц {делегат} (obj1, obj2: динамическиТипизированныйУкль): булево;

проц QuickSort*(перем genArray: GenericArray; compFunc: GenericCompareFunct);
нач
	QuickSortRec(genArray, compFunc, 0, длинаМассива(genArray)-1)
кон QuickSort;

проц QuickSortRec(перем genArray: GenericArray; comp: GenericCompareFunct; lo, hi: размерМЗ);
перем i, j: размерМЗ; x, t: динамическиТипизированныйУкль;
нач
	i := lo; j := hi;
	x := genArray[(lo+hi) DIV 2];

	нцПока (i <= j) делай
		нцПока (comp(genArray[i], x)) делай увел(i) кц;
		нцПока (comp(x, genArray[j])) делай умень(j) кц;
		если (i <= j) то
			t := genArray[i]; genArray[i] := genArray[j]; genArray[j] := t;
			увел(i); умень(j)
		всё
	кц;

	если (lo < j) то QuickSortRec(genArray, comp, lo, j) всё;
	если (i < hi) то QuickSortRec(genArray, comp, i, hi) всё
кон QuickSortRec;

кон GenericSort.

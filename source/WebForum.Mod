модуль WebForum; (** AUTHOR "Luc Blaeser"; PURPOSE "Example implementation of a web forum" *)

использует DynamicWebpage, HTTPSupport, HTTPSession, WebAccounts, WebComplex, WebStd, PrevalenceSystem,
	XML, XMLObjects, Dates, Строки8;

конст
	FeatureTrackerObjName = "FeatureTracker";
	ThisModuleNameStr = "WebForum";
	DefaultMaxPriority = 3;

	EditLabel = "Edit";
	DeleteLabel = "Delete";
	InsertSubEntryLabel = "Insert subentry";
	AuthorLabel = "Author: ";
	AuthorHeaderLabel = "Author";
	TitleTextLabel = "Title text: ";
	TitleTextHeaderLabel = "Title";
	DetailTextLabel = "Detail text: ";
	ModifiedDateLabel = "Modified date: ";
	ModifiedDateHeaderLabel = "Date";
	PriorityLabel = "Priority: ";
	PriorityHeaderLabel = "Priority";
	TypeLabel = "Type: ";
	TypeHeaderLabel = "Type";
	StatusLabel = "Status: ";
	StatusHeaderLabel = "Status";
	RemoveInterestedContainerLabel = "Remove from my interested containers";
	AddInterestedContainerLabel = "Add to my interested containers";
	DetailTextIsMissingLabel = "detail text is missing";

 тип
	StringList = укль на массив из Строки8.уСтрока;

	FeatureEntry = окласс(WebComplex.WebForumEntry);
		перем
			author: Строки8.уСтрока;
			titleText: Строки8.уСтрока;
			detailText: Строки8.уСтрока;
			modifiedDate: WebStd.PtrDateTime;
			priority: цел32; (* priority number *)
			type: Строки8.уСтрока; (* type information like "Bug", "Feature", "Question" *)
			status: Строки8.уСтрока; (* status information like "unconfirmed", "confirmed" *)

		проц {перекрыта}Internalize*(input: XML.Content);
		перем container: XML.Container;
		нач
			container := input(XML.Container);
			Internalize^(input); (* internalize inherited subEntries and superEntry fields *)

			(* feature element specific fields *)
			author := WebStd.InternalizeString(container, "Author");
			titleText := WebStd.InternalizeString(container, "TitleText");
			detailText := WebStd.InternalizeString(container, "DetailText");
			modifiedDate := WebStd.InternalizeDateTime(container, "ModifiedDate");
			priority := WebStd.InternalizeInteger(container, "Priority");
			type := WebStd.InternalizeString(container, "Type");
			status := WebStd.InternalizeString(container, "Status")
		кон Internalize;

		проц {перекрыта}Externalize*() : XML.Content;
		перем container: XML.Container;
		нач
			нов(container);

			(* append externalized inherited fields subEntries and superEntry *)
			WebStd.AppendXMLContent(container, Externalize^());

			(* feature element specific fields *)
			WebStd.ExternalizeString(author, container, "Author");
			WebStd.ExternalizeString(titleText, container, "TitleText");
			WebStd.ExternalizeString(detailText, container, "DetailText");
			WebStd.ExternalizeDateTime(modifiedDate, container, "ModifiedDate");
			WebStd.ExternalizeInteger(priority, container, "Priority");
			WebStd.ExternalizeString(type, container, "Type");
			WebStd.ExternalizeString(status, container, "Status");
			возврат container
		кон Externalize;

		проц MakeNewEntryBold(lastLoginTime: WebStd.PtrDateTime; xmlText: XML.Container): XML.Container;
		перем bold: XML.Element;
		нач
			если ((lastLoginTime # НУЛЬ) и (modifiedDate # НУЛЬ) и (WebStd.CompareDateTime(modifiedDate^, lastLoginTime^))) то
				нов(bold); bold.SetName("b");
				WebStd.AppendXMLContent(bold, xmlText);
				возврат bold
			иначе
				возврат xmlText
			всё
		кон MakeNewEntryBold;

		проц {перекрыта}TableView*(forum: WebComplex.WebForum; request: HTTPSupport.HTTPRequest) : WebComplex.TableRow;
		перем row: WebComplex.TableRow; cell: WebComplex.TableCell; xmlText: XML.Container; tracker: FeatureTracker;
			lastLoginTime: WebStd.PtrDateTime; modifDateStr: Строки8.уСтрока; prioStr: массив 14 из симв8;
		нач
			(* display bold if modified since last login time *)
			если (forum суть FeatureTracker) то
				tracker := forum(FeatureTracker);
				lastLoginTime := tracker.lastLoginTime;
			иначе
				lastLoginTime := НУЛЬ
			всё;

			нов(row, 9);

			если (author # НУЛЬ) то
				xmlText := MakeNewEntryBold(lastLoginTime, WebStd.CreateXMLText(author^))
			иначе
				xmlText := WebStd.CreateXMLText(" ")
			всё;
			нов(cell, xmlText, WebComplex.WebForumNormalCell);
			row[0] := cell;

			если (titleText # НУЛЬ) то
				xmlText := MakeNewEntryBold(lastLoginTime, WebStd.CreateXMLText(titleText^))
			иначе
				xmlText := WebStd.CreateXMLText(" ")
			всё;
			нов(cell, xmlText, WebComplex.WebForumDetailViewCell);
			row[1] := cell;

			если (modifiedDate # НУЛЬ) то
				modifDateStr := WebStd.DateTimeToStr(modifiedDate^);
				xmlText := MakeNewEntryBold(lastLoginTime, WebStd.CreateXMLText(modifDateStr^))
			иначе
				xmlText := WebStd.CreateXMLText(" ")
			всё;
			нов(cell, xmlText, WebComplex.WebForumNormalCell);
			row[2] := cell;

			Строки8.ПишиЦел64_вСтроку(priority, prioStr);
			xmlText := MakeNewEntryBold(lastLoginTime, WebStd.CreateXMLText(prioStr));
			нов(cell, xmlText, WebComplex.WebForumNormalCell);
			row[3] := cell;

			если (type # НУЛЬ) то
				xmlText := MakeNewEntryBold(lastLoginTime, WebStd.CreateXMLText(type^))
			иначе
				xmlText := WebStd.CreateXMLText(" ")
			всё;
			нов(cell, xmlText, WebComplex.WebForumNormalCell);
			row[4] := cell;

			если (status # НУЛЬ) то
				xmlText := MakeNewEntryBold(lastLoginTime, WebStd.CreateXMLText(status^))
			иначе
				xmlText := WebStd.CreateXMLText(" ")
			всё;
			нов(cell, xmlText, WebComplex.WebForumNormalCell);
			row[5] := cell;

			xmlText := MakeNewEntryBold(lastLoginTime, WebStd.CreateXMLText(EditLabel));
			нов(cell, xmlText, WebComplex.WebForumEditViewCell);
			row[6] := cell;

			xmlText := MakeNewEntryBold(lastLoginTime, WebStd.CreateXMLText(DeleteLabel));
			нов(cell, xmlText, WebComplex.WebForumDeleteCell);
			row[7] := cell;

			xmlText := MakeNewEntryBold(lastLoginTime, WebStd.CreateXMLText(InsertSubEntryLabel));
			нов(cell, xmlText, WebComplex.WebForumSubInsertViewCell);
			row[8] := cell;

			возврат row
		кон TableView;

		проц {перекрыта}DetailView*(forum: WebComplex.WebForum; request: HTTPSupport.HTTPRequest) : XML.Content;
		перем container: XML.Container; pTag: XML.Element; modifDateStr: Строки8.уСтрока; prioStr: массив 14 из симв8;
		нач
			нов(container);
			WebComplex.AddStandardDetailView(container, AuthorLabel, author);
			WebComplex.AddStandardDetailView(container, TitleTextLabel, titleText);
			WebComplex.AddMultipleLinesDetailView(container, DetailTextLabel, detailText);

			нов(pTag); pTag.SetName("p");
			WebStd.AppendXMLContent(pTag, WebStd.CreateXMLText(ModifiedDateLabel));
			если (modifiedDate # НУЛЬ) то
				modifDateStr := WebStd.DateTimeToStr(modifiedDate^);
				WebStd.AppendXMLContent(pTag, WebStd.CreateXMLText(modifDateStr^))
			всё;
			container.AddContent(pTag);

			нов(pTag); pTag.SetName("p");
			WebStd.AppendXMLContent(pTag, WebStd.CreateXMLText(PriorityLabel));
			Строки8.ПишиЦел64_вСтроку(priority, prioStr);
			WebStd.AppendXMLContent(pTag, WebStd.CreateXMLText(prioStr));
			container.AddContent(pTag);

			WebComplex.AddStandardDetailView(container, TypeLabel, type);
			WebComplex.AddStandardDetailView(container, StatusLabel, status);
			возврат container
		кон DetailView;

		проц {перекрыта}EditView*(forum: WebComplex.WebForum; request: HTTPSupport.HTTPRequest) : XML.Content;
		перем table, tr, td, select, option: XML.Element;
			tracker: FeatureTracker; i: цел32; iStr: массив 14 из симв8;
		нач
			нов(table); table.SetName("table");
			WebComplex.AddTextFieldInputRow(table, AuthorLabel, "author", author);
			WebComplex.AddTextFieldInputRow(table, TitleTextLabel, "titletext", titleText);
			WebComplex.AddTextAreaInputRow(table, DetailTextLabel, "detailtext", detailText);

			если (forum суть FeatureTracker) то
				tracker := forum(FeatureTracker);

				нов(tr); tr.SetName("tr"); table.AddContent(tr);
				нов(td); td.SetName("td"); tr.AddContent(td);
				WebStd.AppendXMLContent(td, WebStd.CreateXMLText(PriorityLabel));
				нов(td); td.SetName("td"); tr.AddContent(td);
				нов(select); select.SetName("select"); td.AddContent(select);
				select.SetAttributeValue("name", "priority");
				нцДля i := 1 до tracker.maxPrio делай
					Строки8.ПишиЦел64_вСтроку(i, iStr);
					нов(option); option.SetName("option"); select.AddContent(option);
					option.SetAttributeValue("value", iStr);
					если (i = priority) то
						option.SetAttributeValue("selected", "true")
					всё;
					WebStd.AppendXMLContent(option, WebStd.CreateXMLText(iStr));
				кц;

				если (tracker.types # НУЛЬ) то
					нов(tr); tr.SetName("tr"); table.AddContent(tr);
					нов(td); td.SetName("td"); tr.AddContent(td);
					WebStd.AppendXMLContent(td, WebStd.CreateXMLText(TypeLabel));
					нов(td); td.SetName("td"); tr.AddContent(td);
					нов(select); select.SetName("select"); td.AddContent(select);
					select.SetAttributeValue("name", "type");
					AppendOptionList(select, tracker.types, type)
				всё;

				если (tracker.status # НУЛЬ) то
					нов(tr); tr.SetName("tr"); table.AddContent(tr);
					нов(td); td.SetName("td"); tr.AddContent(td);
					WebStd.AppendXMLContent(td, WebStd.CreateXMLText(StatusLabel));
					нов(td); td.SetName("td"); tr.AddContent(td);
					нов(select); select.SetName("select"); td.AddContent(select);
					select.SetAttributeValue("name", "status");
					AppendOptionList(select, tracker.status, status)
				всё
			всё;
			возврат table
		кон EditView;
	кон FeatureEntry;

	(** recursive feature tracker web forum statefull active element with detail view and optional modification functionality
	 * if granted by authorization. If 'prevalenceSystem' is not specified then the standard prevalence system will be used.
	 * Omitting an access constraint means publishing the functionality to all users.
	 * 'OnlyNewEntries' indicates optionlally that only new entries since the last login have to be displayed.
	 *  'MaxPriority' specifies the heighest possible priority in the system. The default priority is 3.
	 *  'MessageTypes' specifies the domain for a message type.
	 *  'MessageStatus' specifies the domain for a message status. The first status entry is the default type for a status.
	 * usage example:
	 *  <WebComplex:WebForum id="MyForum3" containername="MyForum" prevalencesystem="..">
	 *    <OnlyNewEntries/>
	 *    <MaxPriority number="5"/>
	 *    <MessageTypes>
	 *        <Type>Bug</Type>
	 *        <Type>Feature request</Type>
	 *        <Type>Question</Type>
	 *    </MessageTypes>
	 *    <MessageStatus>
	 *       <Status>Unconfirmed</Status>
	 *       <Status>Confirmed</Status>
	 *    <MessageStatus>
	 *    <Paging size="10" nextlabel="more.." previouslabel="..back"/>
	 *    <Searching label="Search for entries:" buttonname="Search!"/>
	 *    <AccessContraint>
	 *         <Edit><WebStd:AuthorizationCheck domain=".."/></Edit>
	 *         <Insert><WebStd:AuthorizationCheck domain=".."/></Insert>
	 *         <Delete><WebStd:AuthorizationCheck domain=".."/></Delete>
	 *    </AccessConstraint>
	 *  </WebComplex:WebForum>
	 *)
	FeatureTracker* = окласс(WebComplex.WebForum);
		перем
			searchText: Строки8.уСтрока;
			thisContainerName: Строки8.уСтрока;
			lastLoginTime: WebStd.PtrDateTime; (* # NIL if the forum has only to display new entries since lastLoginDate *)
			maxPrio: цел32;
			types: StringList; (* the list of different message types specified by subelement 'MessageTypes' *)
			status: StringList; (* the list of different message status codes specified by subelement 'Status' *)

		проц &Initialize*;
		нач Init(); lastLoginTime := НУЛЬ; maxPrio := DefaultMaxPriority; types := НУЛЬ; status := НУЛЬ
		кон Initialize;

		проц {перекрыта}Transform*(input: XML.Element; request: HTTPSupport.HTTPRequest) : XML.Content;
		перем elem: XML.Element; session: HTTPSession.Session; webAccount: WebAccounts.WebAccount;
			str: Строки8.уСтрока;
		нач
			elem := WebStd.GetXMLSubElement(input, "OnlyNewEntries");
			lastLoginTime := НУЛЬ;
			если (elem # НУЛЬ) то
				session := HTTPSession.GetSession(request);
				webAccount := WebAccounts.GetAuthWebAccountForSession(session);
				если (webAccount # НУЛЬ) то
					lastLoginTime := webAccount.GetLastLoginTime()
				всё
			всё;

			elem := WebStd.GetXMLSubElement(input, "MaxPriority");
			если (elem # НУЛЬ) то
				str := elem.GetAttributeValue("number");
				если (str # НУЛЬ) то
					Строки8.ПрочтиЦел32_изСтроки(str^, maxPrio)
				всё
			иначе
				maxPrio := DefaultMaxPriority
			всё;

			elem := WebStd.GetXMLSubElement(input, "MessageTypes");
			если (elem # НУЛЬ) то
				types := GetStringListFromXML(elem, "Type")
			иначе
				types := НУЛЬ
			всё;

			elem := WebStd.GetXMLSubElement(input, "MessageStatus");
			если (elem # НУЛЬ) то
				status := GetStringListFromXML(elem, "Status")
			иначе
				status := НУЛЬ
			всё;

			возврат Transform^(input, request)
		кон Transform;

		проц GetStringListFromXML(elem: XML.Element; subElemName: массив из симв8) : StringList;
		перем subElem: XML.Element; enum: XMLObjects.Enumerator; counter: цел32; list: StringList;
			elemName: Строки8.уСтрока; p: динамическиТипизированныйУкль;
		нач
			enum := elem.GetContents(); counter := 0;
			нцПока (enum.HasMoreElements()) делай
				p := enum.GetNext();
				если (p суть XML.Element) то
					subElem := p(XML.Element); elemName := subElem.GetName();
					если ((elemName # НУЛЬ) и (elemName^ = subElemName)) то увел(counter) всё
				всё
			кц;
			если (counter > 0) то
				enum.Reset(); нов(list, counter); counter := 0;
				нцПока (enum.HasMoreElements()) делай
					p := enum.GetNext();
					если (p суть XML.Element) то
						subElem := p(XML.Element); elemName := subElem.GetName();
						если ((elemName # НУЛЬ) и (elemName^ = subElemName)) то
							list[counter] := WebStd.GetXMLCharContent(subElem); увел(counter)
						всё
					всё
				кц;
				возврат list
			иначе
				возврат НУЛЬ
			всё
		кон GetStringListFromXML;

		проц {перекрыта}GetAdditionalEventHandlers*() : DynamicWebpage.EventHandlerList;
		перем list: DynamicWebpage.EventHandlerList;
		нач
			нов(list, 1);
			нов(list[0], "SetInterested", SetInterested);
			возврат list
		кон GetAdditionalEventHandlers;

		проц SetInterested(request: HTTPSupport.HTTPRequest; params: DynamicWebpage.ParameterList);
		перем session: HTTPSession.Session; webAccount: WebAccounts.WebAccount;
		нач
			session := HTTPSession.GetSession(request);
			webAccount := WebAccounts.GetAuthWebAccountForSession(session);
			если ((webAccount # НУЛЬ) и (thisContainerName # НУЛЬ)) то
				если (webAccount.IsInterestedOnContainer(thisContainerName^)) то
					webAccount.RemoveInterestedContainer(thisContainerName^)
				иначе
					webAccount.AddInterestedContainer(thisContainerName^)
				всё
			всё
		кон SetInterested;

		проц {перекрыта}GetDefaultSearchFilter*() : WebStd.PersistentDataFilter;
		нач возврат DefaultFilter
		кон GetDefaultSearchFilter;

		проц RecursiveSearchFilter(featureEntry: FeatureEntry) : булево;
		перем modifDate: WebStd.PtrDateTime; list: WebStd.PersistentDataObjectList;
			subFeature: FeatureEntry; i: размерМЗ;
		нач
			modifDate := featureEntry.modifiedDate;
			если (modifDate # НУЛЬ) то
				если (WebStd.CompareDateTime(modifDate^, lastLoginTime^)) то возврат истина всё
			всё;
			если (featureEntry.subEntries # НУЛЬ) то
				list := featureEntry.subEntries.GetElementList(WebStd.DefaultPersistentDataFilter, НУЛЬ);
				если (list # НУЛЬ) то
					нцДля i := 0 до длинаМассива(list)-1 делай
						если (list[i] суть FeatureEntry) то
							subFeature := list[i](FeatureEntry);
							если (RecursiveSearchFilter(subFeature)) то возврат истина всё
						всё
					кц
				всё
			всё;
			возврат ложь
		кон RecursiveSearchFilter;

		(* true iff the element or a subelement has been modified since last login *)
		проц DefaultFilter(obj: WebStd.PersistentDataObject) : булево;
		перем featureEntry: FeatureEntry;
		нач
			если ((lastLoginTime # НУЛЬ) и (obj суть FeatureEntry)) то
				featureEntry := obj(FeatureEntry);
				возврат RecursiveSearchFilter(featureEntry)
			иначе
				возврат истина
			всё
		кон DefaultFilter;

		проц {перекрыта}GetHeaderXMLContent*(persContainer: WebStd.PersistentDataContainer;
			input: XML.Element; request: HTTPSupport.HTTPRequest) : XML.Content;
		перем pTag, eventLink, label: XML.Element; objectId: Строки8.уСтрока; container: XML.Container;
			session: HTTPSession.Session; webAccount: WebAccounts.WebAccount;
		нач
			objectId := input.GetAttributeValue(DynamicWebpage.XMLAttributeObjectIdName); (* objectId # NIL *)
			thisContainerName := input.GetAttributeValue("containername");

			session := HTTPSession.GetSession(request);
			webAccount := WebAccounts.GetAuthWebAccountForSession(session);
			если ((webAccount # НУЛЬ) и (thisContainerName # НУЛЬ)) то
				нов(container);
				нов(pTag); pTag.SetName("p");

				нов(eventLink); eventLink.SetName("WebStd:EventLink");
				eventLink.SetAttributeValue("xmlns:WebStd", "WebStd");

				нов(label); label.SetName("Label");
				eventLink.AddContent(label);

				eventLink.SetAttributeValue("method", "SetInterested");
				eventLink.SetAttributeValue("object", "FeatureTracker");
				eventLink.SetAttributeValue("module", ThisModuleNameStr);
				eventLink.SetAttributeValue("objectid", objectId^);
				pTag.AddContent(eventLink);

				container.AddContent(pTag);
				если (webAccount.IsInterestedOnContainer(thisContainerName^)) то
					WebStd.AppendXMLContent(label, WebStd.CreateXMLText(RemoveInterestedContainerLabel));
				иначе
					WebStd.AppendXMLContent(label, WebStd.CreateXMLText(AddInterestedContainerLabel))
				всё;
				возврат container
			иначе
				возврат НУЛЬ
			всё
		кон GetHeaderXMLContent;

		проц {перекрыта}InsertObject*(container: WebStd.PersistentDataContainer; superEntry: WebComplex.WebForumEntry;
			request: HTTPSupport.HTTPRequest; params: DynamicWebpage.ParameterList;
			перем statusMsg: XML.Content) : булево;
			(* parameters "author", "titletext", "detailtext", "priority", "type" but not "status" *)
		перем author, titleText, detailText, priorityStr, type: Строки8.уСтрока; obj: FeatureEntry;
			subEntries: WebStd.PersistentDataContainer;
		нач
			author := params.GetParameterValueByName("author");
			titleText := params.GetParameterValueByName("titletext");
			detailText := params.GetParameterValueByName("detailtext");
			priorityStr := params.GetParameterValueByName("priority");
			type := params.GetParameterValueByName("type");

			если ((titleText # НУЛЬ) и (titleText^ # "")) то
				нов(obj); obj.author := author; obj.titleText := titleText; obj.detailText := detailText;
				если (priorityStr # НУЛЬ) то
					Строки8.ПрочтиЦел32_изСтроки(priorityStr^, obj.priority)
				иначе
					obj.priority := 0
				всё;
				obj.type := type;
				если (status # НУЛЬ) то
					obj.status := status[0]
				иначе
					obj.status := НУЛЬ
				всё;
				нов(obj.modifiedDate); obj.modifiedDate^ := Dates.Now();

				container.AddPersistentDataObject(obj, featureEntryDesc); (* adds it also to the prevalence system *)

				если (superEntry # НУЛЬ) то
					если (superEntry.subEntries = НУЛЬ) то
						нов(subEntries);
						superEntry.BeginModification;
						superEntry.subEntries := subEntries;
						PrevalenceSystem.AddPersistentObject(subEntries, WebStd.persistentDataContainerDesc);
						(* the object must be added to the prevalence system after there is a reference from a persistent object to it
						 * otherwise it could be already collected from the garbage collection mechanism of the prevalence system *)
						superEntry.EndModification
					всё;
					superEntry.subEntries.AddPersistentDataObject(obj, featureEntryDesc);
					obj.BeginModification;
					obj.superEntry := superEntry;
					obj.EndModification
				всё;
				возврат истина
			иначе
				statusMsg := WebStd.CreateXMLText(DetailTextIsMissingLabel);
				возврат ложь
			всё
		кон InsertObject;

		проц {перекрыта}UpdateObject*(obj: WebComplex.WebForumEntry; request: HTTPSupport.HTTPRequest;
			params: DynamicWebpage.ParameterList; перем statusMsg: XML.Content) : булево;
			(* parameters "author", "titletext", "detailtext", "priority", "type", "status" *)
		перем author, titleText, detailText, priorityStr, status, type: Строки8.уСтрока; feature: FeatureEntry;
		нач (* obj # NIL *)
			если (obj суть FeatureEntry) то
				feature := obj(FeatureEntry);
				author := params.GetParameterValueByName("author");
				titleText := params.GetParameterValueByName("titletext");
				detailText := params.GetParameterValueByName("detailtext");
				priorityStr := params.GetParameterValueByName("priority");
				type := params.GetParameterValueByName("type");
				status := params.GetParameterValueByName("status");

				если ((titleText # НУЛЬ) и (titleText^ # "")) то
					feature.BeginModification;
					feature.author := author; feature.titleText := titleText; feature.detailText := detailText;
					если (priorityStr # НУЛЬ) то
						Строки8.ПрочтиЦел32_изСтроки(priorityStr^, feature.priority)
					иначе
						feature.priority := 0
					всё;
					feature.type := type;
					feature.status := status;
					нов(feature.modifiedDate); feature.modifiedDate^ := Dates.Now();
					feature.EndModification;
					возврат истина
				иначе
					statusMsg := WebStd.CreateXMLText(DetailTextIsMissingLabel);
					возврат ложь
				всё
			иначе
				statusMsg := WebStd.CreateXMLText("object is not of type FeatureEntry");
				возврат ложь
			всё
		кон UpdateObject;

		проц {перекрыта}ThisObjectName*() : Строки8.уСтрока;
		нач возврат WebStd.GetString(FeatureTrackerObjName)
		кон ThisObjectName;

		проц {перекрыта}ThisModuleName*() : Строки8.уСтрока;
		нач возврат WebStd.GetString(ThisModuleNameStr)
		кон ThisModuleName;

		(** abstract, returns the insert view for the initialization of a new web forum entry, without submit/back-input fields
		 * and without hidden parameter for super entry in hierarchy.
		 * superEntry is the parent web forum entry in a hierachical web forum, superEntry is NIL iff it is a root entry *)
		проц {перекрыта}GetInsertView*(superEntry: WebComplex.WebForumEntry; request: HTTPSupport.HTTPRequest): XML.Content;
		перем table, tr, td, select, option: XML.Element; session: HTTPSession.Session;
			webAccount: WebAccounts.WebAccount; str: Строки8.уСтрока; i: цел32; iStr: массив 14 из симв8;
		нач
			нов(table); table.SetName("table");

			session := HTTPSession.GetSession(request);
			webAccount := WebAccounts.GetAuthWebAccountForSession(session);

			(* set users default message name if present *)
			str := НУЛЬ;
			если (webAccount # НУЛЬ) то
				str := webAccount.GetDefaultMsgName()
			всё;
			WebComplex.AddTextFieldInputRow(table, AuthorLabel, "author", str);
			WebComplex.AddTextFieldInputRow(table, TitleTextLabel, "titletext", НУЛЬ);
			WebComplex.AddTextAreaInputRow(table, DetailTextLabel, "detailtext", НУЛЬ);

			нов(tr); tr.SetName("tr"); table.AddContent(tr);
			нов(td); td.SetName("td"); tr.AddContent(td);
			WebStd.AppendXMLContent(td, WebStd.CreateXMLText(PriorityLabel));
			нов(td); td.SetName("td"); tr.AddContent(td);
			нов(select); select.SetName("select"); td.AddContent(select);
			select.SetAttributeValue("name", "priority");
			нцДля i := 1 до maxPrio делай
				Строки8.ПишиЦел64_вСтроку(i, iStr);
				нов(option); option.SetName("option"); select.AddContent(option);
				option.SetAttributeValue("value", iStr);
				WebStd.AppendXMLContent(option, WebStd.CreateXMLText(iStr));
			кц;

			если (types # НУЛЬ) то
				нов(tr); tr.SetName("tr"); table.AddContent(tr);
				нов(td); td.SetName("td"); tr.AddContent(td);
				WebStd.AppendXMLContent(td, WebStd.CreateXMLText(TypeLabel));
				нов(td); td.SetName("td"); tr.AddContent(td);
				нов(select); select.SetName("select"); td.AddContent(select);
				select.SetAttributeValue("name", "type");
				AppendOptionList(select, types, НУЛЬ)
			всё;

			возврат table
		кон GetInsertView;

		проц {перекрыта}GetTableHeader*(request: HTTPSupport.HTTPRequest): WebComplex.HeaderRow;
		перем row: WebComplex.HeaderRow;
		нач
			нов(row, 9);
			row[0] := WebComplex.GetHeaderCellForText(AuthorHeaderLabel, CompareAuthor);
			row[1] := WebComplex.GetHeaderCellForText(TitleTextHeaderLabel, CompareTitle);
			row[2] := WebComplex.GetHeaderCellForText(ModifiedDateHeaderLabel, CompareModifDate);
			row[3] := WebComplex.GetHeaderCellForText(PriorityHeaderLabel, ComparePriority);
			row[4] := WebComplex.GetHeaderCellForText(TypeHeaderLabel, CompareType);
			row[5] := WebComplex.GetHeaderCellForText(StatusHeaderLabel, CompareStatus);
			row[6] := WebComplex.GetHeaderCellForText(" ", НУЛЬ);
			row[7] := WebComplex.GetHeaderCellForText(" ", НУЛЬ);
			row[8] := WebComplex.GetHeaderCellForText(" ", НУЛЬ);
			возврат row
		кон GetTableHeader;

		проц {перекрыта}GetSearchFilter*(text: Строки8.уСтрока) : WebStd.PersistentDataFilter;
		нач
			если (text # НУЛЬ) то
				нов(searchText, Строки8.КвоБайтБезЗавершающего0(text^)+3);
				Строки8.Склей("*", text^, searchText^);
				если (Строки8.КвоБайтБезЗавершающего0(text^) > 0) то
					Строки8.ПодклейВСтрокуХвост(searchText^, "*");
					Строки8.СтрокуВНижнийРегистрASCII(searchText^)
				всё;
				возврат SearchFilter
			всё;
			возврат НУЛЬ
		кон GetSearchFilter;

		проц SearchFilter(obj: WebStd.PersistentDataObject) : булево;
		перем entry: FeatureEntry;
			проц Matches(перем str: массив из симв8) : булево;
			перем lowStr: Строки8.уСтрока;
			нач
				lowStr := WebStd.GetString(str);
				Строки8.СтрокуВНижнийРегистрASCII(lowStr^);
				возврат Строки8.ПодходитЛиПодМаскуИмениФайла¿(searchText^, lowStr^)
			кон Matches;
		нач (* searchText # NIL *)
			если (obj суть FeatureEntry) то
				entry := obj(FeatureEntry);
				если ((entry.author # НУЛЬ) и (Matches(entry.author^))) то
					возврат истина
				всё;
				если ((entry.titleText # НУЛЬ) и (Matches(entry.titleText^))) то
					возврат истина
				всё;
				если ((entry.detailText # НУЛЬ) и (Matches(entry.detailText^))) то
					возврат истина
				всё
			всё;
			возврат ложь
		кон SearchFilter;

		проц CompareAuthor(obj1, obj2: WebStd.PersistentDataObject): булево;
		перем f1, f2: FeatureEntry;
		нач
			если ((obj1 суть FeatureEntry) и (obj2 суть FeatureEntry)) то
				f1 := obj1(FeatureEntry); f2 := obj2(FeatureEntry);
				если (f2.author = НУЛЬ) то
					возврат ложь
				аесли (f1.author = НУЛЬ) то (* f2.author # NIL *)
					возврат истина
				иначе
					возврат f1.author^ < f2.author^
				всё
			иначе
				возврат ложь
			всё
		кон CompareAuthor;

		проц CompareTitle(obj1, obj2: WebStd.PersistentDataObject): булево;
		перем f1, f2: FeatureEntry;
		нач
			если ((obj1 суть FeatureEntry) и (obj2 суть FeatureEntry)) то
				f1 := obj1(FeatureEntry); f2 := obj2(FeatureEntry);
				если (f2.titleText = НУЛЬ) то
					возврат ложь
				аесли (f1.titleText = НУЛЬ) то (* f2.titleText # NIL *)
					возврат истина
				иначе
					возврат f1.titleText^ < f2.titleText^
				всё
			иначе
				возврат ложь
			всё
		кон CompareTitle;

		проц CompareModifDate(obj1, obj2: WebStd.PersistentDataObject): булево;
		перем f1, f2: FeatureEntry;
		нач
			если ((obj1 суть FeatureEntry) и (obj2 суть FeatureEntry)) то
				f1 := obj1(FeatureEntry); f2 := obj2(FeatureEntry);
				если (f2.modifiedDate = НУЛЬ) то
					возврат ложь
				аесли (f1.modifiedDate = НУЛЬ) то (* f2.modifiedDate # NIL *)
					возврат истина
				иначе
					возврат WebStd.CompareDateTime(f1.modifiedDate^, f2.modifiedDate^)
				всё
			иначе
				возврат ложь
			всё
		кон CompareModifDate;

		проц ComparePriority(obj1, obj2: WebStd.PersistentDataObject): булево;
		перем f1, f2: FeatureEntry;
		нач
			если ((obj1 суть FeatureEntry) и (obj2 суть FeatureEntry)) то
				f1 := obj1(FeatureEntry); f2 := obj2(FeatureEntry);
				возврат f1.priority > f2.priority
			иначе
				возврат ложь
			всё
		кон ComparePriority;

		проц CompareType(obj1, obj2: WebStd.PersistentDataObject): булево;
		перем f1, f2: FeatureEntry;
		нач
			если ((obj1 суть FeatureEntry) и (obj2 суть FeatureEntry)) то
				f1 := obj1(FeatureEntry); f2 := obj2(FeatureEntry);
				если (f2.type = НУЛЬ) то
					возврат ложь
				аесли (f1.type = НУЛЬ) то (* f2.type # NIL *)
					возврат истина
				иначе
					возврат f1.type^ < f2.type^
				всё
			иначе
				возврат ложь
			всё
		кон CompareType;

		проц CompareStatus(obj1, obj2: WebStd.PersistentDataObject): булево;
		перем f1, f2: FeatureEntry;
		нач
			если ((obj1 суть FeatureEntry) и (obj2 суть FeatureEntry)) то
				f1 := obj1(FeatureEntry); f2 := obj2(FeatureEntry);
				если (f2.status = НУЛЬ) то
					возврат ложь
				аесли (f1.status = НУЛЬ) то (* f2.status # NIL *)
					возврат истина
				иначе
					возврат f1.status^ < f2.status^
				всё
			иначе
				возврат ложь
			всё
		кон CompareStatus;
	кон FeatureTracker;

	перем
		featureEntryDesc: PrevalenceSystem.PersistentObjectDescriptor; (* descriptor for FeatureEntry *)

	(* append HTML option list to 'select'. the string equal to 'actualValue' is selected *)
	проц AppendOptionList(select: XML.Element; list: StringList; actualValue: Строки8.уСтрока);
	перем str: Строки8.уСтрока; option: XML.Element; i: размерМЗ;
	нач (* select # NIL & list # NIL *)
		нцДля i := 0 до длинаМассива(list)-1 делай
			str := list[i];
			если (str # НУЛЬ) то
				нов(option); option.SetName("option"); select.AddContent(option);
				option.SetAttributeValue("value", str^);
				если ((actualValue # НУЛЬ) и (actualValue^ = str^)) то
					option.SetAttributeValue("selected", "true")
				всё;
				WebStd.AppendXMLContent(option, WebStd.CreateXMLText(str^));
			всё
		кц
	кон AppendOptionList;

	проц GetNewFeatureEntry() : PrevalenceSystem.PersistentObject;
	перем obj: FeatureEntry;
	нач нов(obj); возврат obj
	кон GetNewFeatureEntry;

	(** used by the prevalence system *)
	проц GetPersistentObjectDescriptors*() : PrevalenceSystem.PersistentObjectDescSet;
	перем descSet : PrevalenceSystem.PersistentObjectDescSet;
		descs: массив 1 из PrevalenceSystem.PersistentObjectDescriptor;
	нач
		descs[0] := featureEntryDesc;
		нов(descSet, descs);
		возврат descSet
	кон GetPersistentObjectDescriptors;

	проц CreateFeatureTrackerElement() : DynamicWebpage.ActiveElement;
	перем obj: FeatureTracker;
	нач
		нов(obj); возврат obj
	кон CreateFeatureTrackerElement;

	проц GetActiveElementDescriptors*() : DynamicWebpage.ActiveElementDescSet;
	перем desc: укль на массив из DynamicWebpage.ActiveElementDescriptor;
		descSet: DynamicWebpage.ActiveElementDescSet;
	нач
		нов(desc, 1);
		нов(desc[0],  "FeatureTracker", CreateFeatureTrackerElement);
		нов(descSet, desc^); возврат descSet
	кон GetActiveElementDescriptors;

нач
	нов(featureEntryDesc, ThisModuleNameStr, "FeatureEntry", GetNewFeatureEntry)
кон WebForum.


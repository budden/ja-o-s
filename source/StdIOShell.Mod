модуль StdIOShell; (** AUTHOR "Felix Friedrich"; PURPOSE "Command shell for standalone Oberon/A2 Applications"; *)

использует StdIO, Commands, Modules, Трассировка;

конст Verbose = ложь;

проц Activate(context: Commands.Context; конст cmd: массив из симв8): булево;
перем msg: массив 256 из симв8;  res: целМЗ;
нач
	если Verbose то
		Трассировка.пСтроку8("StdIOShell: Activate Command "); Трассировка.пСтроку8(cmd); Трассировка.пВК_ПС;
	всё;
	Commands.Activate(cmd, context, {Commands.Wait}, res, msg);
	если res # 0 то context.error.пСтроку8(msg); context.error.пВК_ПС; возврат ложь всё;
	возврат истина;
кон Activate;

проц Execute(context: Commands.Context): булево;
перем str: массив 256 из симв8;
нач
	если ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(str) то
		context.error.пСтроку8("Critical error: no arg"); context.error.ПротолкниБуферВПоток;
		возврат ложь;
	всё;
	если ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(str) то
		если StdIO.Echo то str := "Shell.StartEchoed" иначе str := "Shell.Start" всё;
	аесли (str = "compile") то str := "Compiler.Compile";
	аесли (str = "link") то str := "Linker.Link";
	аесли (str = "interpreter") или (str = "i") то str := "InterpreterShell.Start";
	аесли (str = "execute") или (str = "e") то str := "System.DoFile";
	аесли (str = "do") или (str = "d") то str := "System.DoCommands";
	аесли (str = "run") или (str = "r") то
		если ~Activate(context, "System.DoFile") то возврат ложь всё;
		str := "Shell.Start";
	всё;
	возврат Activate(context, str);
кон Execute;

тип
	(* excute the shell and termination in separate thread with proper process data structure *)
	Executor=окласс(Modules.ЛХА)
	перем done := ложь: булево;
	перем code := Modules.PowerDown: цел32;

		проц Wait;
		нач{единолично}
			дождись(done);
		кон Wait;

	нач {активное, единолично}
		если Execute(StdIO.env) то code := Modules.Reboot всё;
	выходя
		done := истина;
	кон Executor;

перем execute: Executor;

(* do not add commands here -- the module loader does not finish here and they will not become available *)
нач
	нов(execute);
	execute.Wait;
	если Verbose то Трассировка.пСтроку8("StdIOShell: Exit"); Трассировка.пВК_ПС всё;
	Modules.Shutdown(execute.code);
кон StdIOShell.



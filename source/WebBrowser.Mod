модуль WebBrowser; (** AUTHOR "Simon L. Keel"; PURPOSE "Simple Web Browser GUI"; *)

использует
	WebBrowserPanel,
	Строки8, ЛогЯдра, WMGraphics, WMComponents, WMStandardComponents, WMWindowManager, WMEditors,
	Modules, WMRestorable, XML, WMRectangles, WMMessages, Commands, Files;

конст
	HomePage = "http://groups.umd.umich.edu/cis/course.des/cis400/index.html";
	BrowserTitle = "BimBrowser";
	BookmarkPage = "file://bookmarks.html"; (* This URL is loaded when pressing the "Bookmarks"-button *)
	BookmarkFile = "bookmarks.html"; (* New bookmarks are appended to this file *)
	loadingNew = 0;
	loadingOld = 1;
	loadingNone = 2;

тип
	String = Строки8.уСтрока;

	URLNode = укль на запись
		url, title : String;
		back, forward : URLNode;
	кон;

	KillerMsg = окласс
	кон KillerMsg;

	Window* = окласс (WMComponents.FormWindow)
	перем
		webPanel : WebBrowserPanel.WebPanel;
		topToolbar : WMStandardComponents.Panel;
		urlEdit : WMEditors.Editor;
		back, forward, reload, home, bookm, addBM, go : WMStandardComponents.Button;
		loadID : цел32;
		actualURL : URLNode;

		проц CreateForm() : WMComponents.VisualComponent;
		перем
			panel : WMStandardComponents.Panel;
		нач

			нов(topToolbar); topToolbar.bounds.SetHeight(20); topToolbar.alignment.Set(WMComponents.AlignTop);

			нов(panel); panel.bounds.SetExtents(1000, 650); panel.fillColor.Set(0FFFFFFFFH); panel.takesFocus.Set(истина);
			нов(topToolbar); topToolbar.bounds.SetHeight(20); topToolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(topToolbar);

			нов(back); back.caption.SetAOC("Back"); back.alignment.Set(WMComponents.AlignLeft);
			back.onClick.Add(Back);
			topToolbar.AddContent(back);

			нов(forward); forward.caption.SetAOC("Forward"); forward.alignment.Set(WMComponents.AlignLeft);
			forward.onClick.Add(Forward);
			topToolbar.AddContent(forward);

			нов(reload); reload.caption.SetAOC("Reload"); reload.alignment.Set(WMComponents.AlignLeft);
			reload.onClick.Add(Reload);
			topToolbar.AddContent(reload);

			нов(home); home.caption.SetAOC("Home"); home.alignment.Set(WMComponents.AlignLeft);
			home.onClick.Add(Home);
			topToolbar.AddContent(home);

			нов(bookm); bookm.caption.SetAOC("Bookmarks"); bookm.alignment.Set(WMComponents.AlignLeft);
			bookm.bounds.SetWidth(77);
			bookm.onClick.Add(Bookmarks);
			topToolbar.AddContent(bookm);

			нов(addBM); addBM.caption.SetAOC("Add Bookmark"); addBM.alignment.Set(WMComponents.AlignLeft);
			addBM.bounds.SetWidth(97);
			addBM.onClick.Add(AddBookmark);
			topToolbar.AddContent(addBM);

			нов(go); go.caption.SetAOC("Go"); go.alignment.Set(WMComponents.AlignRight);
			go.onClick.Add(Go);
			topToolbar.AddContent(go);

			нов(urlEdit); urlEdit.alignment.Set(WMComponents.AlignClient);
			urlEdit.tv.textAlignV.Set(WMGraphics.AlignCenter);
			urlEdit.multiLine.Set(ложь); urlEdit.bounds.SetWidth(500);
			topToolbar.AddContent(urlEdit); urlEdit.fillColor.Set(0FFFFFFFFH);
			urlEdit.tv.showBorder.Set(истина);
			urlEdit.tv.borders.Set(WMRectangles.MakeRect(3,3,1,1));
			urlEdit.onEnter.Add(Go);

			нов(webPanel);
			 webPanel.alignment.Set(WMComponents.AlignClient);
			panel.AddContent(webPanel);

			webPanel.notify := Notify;
			webPanel.openNewWindow := OpenLinkFromString;
			webPanel.loadLink := LoadExternal;

			возврат panel
		кон CreateForm;

		проц &New*(c : WMRestorable.Context; url : String);
		перем
			vc : WMComponents.VisualComponent;
			xml : XML.Element;
			m : WMWindowManager.WindowManager;
			indent : размерМЗ;
		нач
			IncCount;
			loadID := 0;
			vc := CreateForm();
			Init(vc.bounds.GetWidth(), vc.bounds.GetHeight(), ложь);
			SetContent(vc);
			SetIcon(WMGraphics.LoadImage("WMIcons.tar://WebBrowser.png", истина));

			если c # НУЛЬ то
				(* restore the desktop *)
				WMRestorable.AddByContext(сам, c);
				если c.appData # НУЛЬ то
					xml := c.appData(XML.Element);
					url := xml.GetAttributeValue("url");
					Resized(GetWidth(), GetHeight())
				всё
			иначе
				WMWindowManager.DefaultAddWindow(сам);
				indent := leftW.bounds.r;
				если topW.bounds.b > indent то indent := topW.bounds.b; всё;
				m := GetManager();
				m.SetWindowPos(сам, (nofWindows-1) * indent + leftW.bounds.r, (nofWindows-1) * indent + topW.bounds.b);
			всё;

			если url = НУЛЬ то
				url := Строки8.ЯвиУСтроку(HomePage);
			всё;

			нов(actualURL);
			actualURL.url := Строки8.ЯвиУСтроку(url^);

			Load();

		кон New;

		проц Load;
		перем
			s : String;
		нач
			urlEdit.SetAsString(actualURL.url^);
			если actualURL.title = НУЛЬ то
				SetTitle(Строки8.ЯвиУСтроку(BrowserTitle));
			иначе
				s := Строки8.СклейДвеСтрокиВСвежуюУСтроку(actualURL.title^, " - ");
				s := Строки8.СклейДвеСтрокиВСвежуюУСтроку(s^, BrowserTitle);
				SetTitle(s);
			всё;
			webPanel.url.Set(actualURL.url);
			webPanel.Load(loadID);
			loadID := (loadID + 1) остОтДеленияНа (матМаксимум(цел32));
		кон Load;

		проц Go(sender, data : динамическиТипизированныйУкль);
		перем
			urlNode : URLNode;
			urlAOC : массив 1024 из симв8;
			i: размерМЗ;
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.Go, sender, data)
			иначе
				нов(urlNode);
				urlEdit.GetAsString(urlAOC);
				i := Строки8.НайдиПодстроку("://", urlAOC);
				если i = -1 то
					urlNode.url := Строки8.СклейДвеСтрокиВСвежуюУСтроку("http://", urlAOC);
				иначе
					urlNode.url := Строки8.ЯвиУСтроку(urlAOC);
				всё;
				actualURL.forward := urlNode;
				urlNode.back := actualURL;
				actualURL := urlNode;
				Load();
			всё;
		кон Go;

		проц Back(sender, data : динамическиТипизированныйУкль);
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.Back, sender, data)
			иначе
				если actualURL.back # НУЛЬ то
					actualURL := actualURL.back;
					Load();
				всё;
			всё;
		кон Back;

		проц Forward(sender, data : динамическиТипизированныйУкль);
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.Forward, sender, data)
			иначе
				если actualURL.forward # НУЛЬ то
					actualURL := actualURL.forward;
					Load();
				всё;
			всё;
		кон Forward;

		проц Reload(sender, data : динамическиТипизированныйУкль);
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.Reload, sender, data)
			иначе
				Load();
			всё;
		кон Reload;

		проц Home(sender, data : динамическиТипизированныйУкль);
		перем
			urlNode : URLNode;
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.Home, sender, data)
			иначе
				нов(urlNode);
				urlNode.url := Строки8.ЯвиУСтроку(HomePage);
				actualURL.forward := urlNode;
				urlNode.back := actualURL;
				actualURL := urlNode;
				Load();
			всё;
		кон Home;

		проц Bookmarks(sender, data : динамическиТипизированныйУкль);
		перем
			urlNode : URLNode;
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.Bookmarks, sender, data)
			иначе
				нов(urlNode);
				urlNode.url := Строки8.ЯвиУСтроку(BookmarkPage);
				actualURL.forward := urlNode;
				urlNode.back := actualURL;
				actualURL := urlNode;
				Load();
			всё;
		кон Bookmarks;

		проц LoadExternal*(sender, data : динамическиТипизированныйУкль);
		перем
			link, target : String;
			urlNode : URLNode;
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.LoadExternal, sender, data)
			иначе
				WebBrowserPanel.DecodeLinkData(data, link, target);
				нов(urlNode);
				urlNode.url := Строки8.ЯвиУСтроку(link^);
				actualURL.forward := urlNode;
				urlNode.back := actualURL;
				actualURL := urlNode;
				Load();
			всё;
		кон LoadExternal;

		проц Notify(sender, data : динамическиТипизированныйУкль);
		перем
			msg : WebBrowserPanel.NotifyMsg;
			s : String;
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.Notify, sender, data)
			иначе
				если loadID # сам.loadID то возврат всё;
				msg := data(WebBrowserPanel.NotifyMsg);
				если msg.url # НУЛЬ то
					actualURL.url := Строки8.ЯвиУСтроку(msg.url^);
					urlEdit.SetAsString(actualURL.url^);
				всё;
				если (msg.title # НУЛЬ) и (msg.title^ # "") то
					actualURL.title := Строки8.ЯвиУСтроку(msg.title^);
					s := Строки8.СклейДвеСтрокиВСвежуюУСтроку(actualURL.title^, " - ");
					s := Строки8.СклейДвеСтрокиВСвежуюУСтроку(s^, BrowserTitle);
					SetTitle(s);
				всё;
			всё;
		кон Notify;

		проц AddBookmark(sender, data : динамическиТипизированныйУкль);
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.AddBookmark, sender, data)
			иначе
				если actualURL.title # НУЛЬ то
					AddBookmarkToFile(actualURL.url^, actualURL.title^);
				иначе
					AddBookmarkToFile(actualURL.url^, actualURL.url^);
				всё;
			всё;
		кон AddBookmark;

		проц {перекрыта}Close*;
		нач
			webPanel.notify := НУЛЬ;
			webPanel.openNewWindow := НУЛЬ;
			webPanel.loadLink := НУЛЬ;
			Close^;
			DecCount
		кон Close;

		проц {перекрыта}Handle*(перем x: WMMessages.Message);
		перем
			data : XML.Element;
			a : XML.Attribute;
			n : массив 16 из симв8;
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) то
				если (x.ext суть KillerMsg) то Close
				аесли (x.ext суть WMRestorable.Storage) то
					нов(data);  n := "WebBrowserData"; data.SetName(n);
					нов(a); n := "url"; a.SetName(n); a.SetValue(actualURL.url^); data.AddAttribute(a);
					x.ext(WMRestorable.Storage).Add("WebBrowser", "WebBrowser.Restore", сам, data)
				иначе Handle^(x)
				всё
			иначе Handle^(x)
			всё
		кон Handle;

	кон Window;

перем
	nofWindows : цел32;

проц AddBookmarkToFile(конст link : массив из симв8; title : массив из симв8);
перем
	file : Files.File;
	w : Files.Writer;
нач
	Строки8.ОтрежьПробелыСДвухСторон(title);
	file := Files.Old(BookmarkFile);
	если file = НУЛЬ то
		file := Files.New(BookmarkFile);
		если file = НУЛЬ то
			ЛогЯдра.пСтроку8("Writing "); ЛогЯдра.пСтроку8(BookmarkFile); ЛогЯдра.пСтроку8(" failed."); ЛогЯдра.пВК_ПС;
			возврат;
		всё;
		Files.OpenWriter(w, file, 0);
		w.пСтроку8('<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">'); w.пВК_ПС();
		w.пСтроку8("<TITLE>Bookmarks</TITLE>"); w.пВК_ПС();
		w.пСтроку8("<H1>Bookmarks</H1>"); w.пВК_ПС();
		w.пВК_ПС();
		w.пСтроку8("<DL><P>"); w.пВК_ПС();
	иначе
		Files.OpenWriter(w, file, file.Length());
	всё;
	w.пСтроку8('    <DT><A HREF="');
	w.пСтроку8(link);
	w.пСтроку8('">');
	w.пСтроку8(title);
	w.пСтроку8('</A>');
	w.пВК_ПС();
	w.ПротолкниБуферВПоток();
	Files.Register(file);
	file.Update;
кон AddBookmarkToFile;

проц Open*;
перем inst : Window;
нач
	нов(inst, НУЛЬ, НУЛЬ);
кон Open;

проц OpenURL*(context : Commands.Context);
перем inst : Window; name : массив 1024 из симв8;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name) то
		нов(inst, НУЛЬ, Строки8.ЯвиУСтроку(name));
	всё;
кон OpenURL;

проц OpenFile*(context : Commands.Context);
перем inst : Window; prefix, name, path : массив 1024 из симв8; urlpath, url : String; i, j : размерМЗ;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name) то
		Files.SplitName(name, prefix, path);
		j:=0;
		нцДля i:=0 до Строки8.КвоБайтБезЗавершающего0(path)-1 делай
			если path[i]#" " то name[j]:=path[i]
			иначе name[j]:="%"; name[j+1]:="2"; name[j+2]:="0"; увел(j,2);
			всё;
			увел(j);
		кц;
		name[j]:=0X;
		path:="file://";
		если prefix#"" то Строки8.ПодклейВСтрокуХвост(path,prefix); Строки8.ПодклейВСтрокуХвост(path,":"); всё;
		url := Строки8.СклейДвеСтрокиВСвежуюУСтроку(path, name);
		нов(inst, НУЛЬ, url);
	всё;
кон OpenFile;

проц OpenLinkFromString*(url : String);
перем inst : Window;
нач
	нов(inst, НУЛЬ, url);
кон OpenLinkFromString;

проц Restore*(context : WMRestorable.Context);
перем w : Window;
нач
	нов(w, context, НУЛЬ)
кон Restore;

проц IncCount;
нач {единолично}
	увел(nofWindows)
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows)
кон DecCount;

проц Cleanup;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WMWindowManager.WindowManager;
нач {единолично}
	нов(die); msg.ext := die; msg.msgType := WMMessages.MsgExt;
	m := WMWindowManager.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0)
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
кон WebBrowser.
(* 
PC.Compile \s WMCharCodes.Mod NewHTTPClient.Mod Strings.Mod WebBrowserComponents.Mod XMLTransformer.Mod HTMLTransformer.Mod HTMLScanner.Mod HTMLParser.Mod WebBrowserPanel.Mod WebBrowser.Mod~

System.Free WebBrowser WebBrowserPanel HTMLParser HTMLScanner HTMLTransformer XMLTransformer WebBrowserComponents Utilities NewHTTPClient WMCharCodes~

WebBrowser.Open ~

WebBrowser.OpenURL http://www.google.com ~
WebBrowser.OpenURL  http://www.wikipedia.org~
WebBrowser.OpenURL http://www.wikipedia.org/wiki/man ~

WebBrowser.OpenFile OberonReport.html ~
*)

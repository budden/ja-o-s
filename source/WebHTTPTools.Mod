(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль WebHTTPTools; (** AUTHOR "TF"; PURPOSE "HTTP download tool"; *)

использует
	Commands, Files, IP, TCP, WebHTTP, WebHTTPClient, Потоки, TFLog, Modules;

перем log : TFLog.Log;

(*
PROCEDURE StrToIntDef(x: ARRAY OF CHAR; def : SIGNED32):SIGNED32;
VAR i, v, sgn: SIGNED32;
BEGIN
	IF x[0] = "-" THEN sgn := -1; INC(i) ELSE sgn := 1 END;
	WHILE (i < LEN(x)) & (x[i] # 0X) DO
		IF (x[i] >= "0") & (x[i] <= "9") THEN v := v * 10 + (ORD(x[i])-ORD("0")) ELSE RETURN def END;
		INC(i)
	END;
	RETURN sgn * v
END StrToIntDef;
*)

проц HexStrToIntDef(конст x: массив из симв8; def : цел32):цел32;
перем i, v: цел32;
нач
	нцПока (i < длинаМассива(x)) и (x[i] # 0X) делай
		если (x[i] >= "0") и (x[i] <= "9") то v := v * 16 + (кодСимв8(x[i])-кодСимв8("0"))
		аесли (ASCII_вЗаглавную(x[i]) >= "A") и (ASCII_вЗаглавную(x[i]) <= "F") то v := v * 16 + (кодСимв8(ASCII_вЗаглавную(x[i]))-кодСимв8("A") + 10)
		иначе возврат def всё;
		увел(i)
	кц;
	возврат v
кон HexStrToIntDef;

проц MatchPrefixI(конст prefix, str: массив из симв8):булево;
перем i: цел32;
нач
	i := 0; нцПока (prefix[i] # 0X) и (ASCII_вЗаглавную(prefix[i]) = ASCII_вЗаглавную(str[i])) делай увел(i) кц;
	возврат prefix[i] = 0X
кон MatchPrefixI;

проц Get*(context : Commands.Context);
перем h : WebHTTP.ResponseHeader;
		rh : WebHTTP.RequestHeader;
		in : Потоки.Чтец;
		res: целМЗ; i: размерМЗ; cs : цел32;
		ch : симв8; token : массив 16 из симв8;
		name : массив 32 из симв8;
		url : массив 256 из симв8;
		file : Files.File;
		fw : Files.Writer;
		con : TCP.Connection;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(url);
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name);

	log.Enter; log.TimeStamp; log.String("GET "); log.String(url); log.String(" to "); log.String(name); log.Exit;
	rh.useragent := "WebHTTPTool/0.1";
	WebHTTPClient.Get(url, rh, con, h, in, res);
	если res = WebHTTPClient.Ok то
		file := Files.New(name);
		Files.OpenWriter(fw, file, 0);
		если (h.transferencoding # "") и MatchPrefixI("chunked", h.transferencoding) то
			in.ПропустиПробелыИТабуляции(); in.чЦепочкуСимв8ДоБелогоПоля(token); cs := HexStrToIntDef(token, 0); in.ПропустиДоКонцаСтрокиТекстаВключительно();
			нцПока cs # 0 делай
				нцДля i := 0 до cs - 1 делай in.чСимв8(ch); fw.пСимв8( ch) кц;
				in.ПропустиДоКонцаСтрокиТекстаВключительно;in.ПропустиПробелыИТабуляции; in.чЦепочкуСимв8ДоБелогоПоля(token); cs := HexStrToIntDef(token, 0); in.ПропустиДоКонцаСтрокиТекстаВключительно;
			кц;
			если fw.кодВозвратаПоследнейОперации = Потоки.Успех то log.Enter; log.TimeStamp; log.String("GET "); log.String(url); log.String(" - OK"); log.Exit
			иначе log.Enter; log.TimeStamp; log.String("GET "); log.String(url); log.String(" - failed"); log.Exit
			всё
		иначе
			если h.contentlength >= 0 то
				нцДля i := 0 до h.contentlength - 1 делай in.чСимв8(ch);  fw.пСимв8(ch) кц;
				если fw.кодВозвратаПоследнейОперации = Потоки.Успех то log.Enter; log.TimeStamp; log.String("GET "); log.String(url); log.String(" - OK"); log.Exit
				иначе log.Enter; log.TimeStamp; log.String("GET "); log.String(url); log.String(" - failed"); log.Exit
				всё
			иначе
				нцПока in.кодВозвратаПоследнейОперации = Потоки.Успех делай in.чСимв8(ch); fw.пСимв8(ch) кц;
				log.Enter; log.TimeStamp; log.String("GET "); log.String(url); log.String(" - OK"); log.Exit
			всё
		всё;
		fw.ПротолкниБуферВПоток;
		Files.Register(file);
		con.Закрой
	иначе
		log.Enter; log.TimeStamp; log.String("GET "); log.String(url); log.String(" - "); log.Int(h.statuscode, 5);
		log.String(h.reasonphrase); log.Exit
	всё;
кон Get;

проц Head*(context : Commands.Context);
перем
	h : WebHTTP.ResponseHeader;
	res : целМЗ;
	url : массив 256 из симв8;
	con : TCP.Connection;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(url);
	WebHTTPClient.Head(url, con, h, res);
	если res = WebHTTPClient.Ok то
		WebHTTP.LogResponseHeader(log, h)
	иначе log.Enter; log.String("Head not done."); log.Exit
	всё;
кон Head;

проц GetAll*(context : Commands.Context);
перем
	baseUrl, baseDir, fileName, url, name, token: массив 256 из симв8;
	file: Files.File;
	fw: Files.Writer;
	rh: WebHTTP.RequestHeader;
	h: WebHTTP.ResponseHeader;
	con: TCP.Connection;
	in: Потоки.Чтец;
	res: целМЗ; i: размерМЗ; cs: цел32;
	ch : симв8;
нач
	если ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(baseUrl) то
		context.error.пСтроку8("Expected base URL");
		context.error.пВК_ПС;
		возврат;
	всё;
	если ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(baseDir) то
		context.error.пСтроку8("Expected base directory");
		context.error.пВК_ПС;
		возврат;
	всё;

	нцПока context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fileName) делай
		Files.JoinPath(baseUrl, fileName, url);
		Files.JoinPath(baseDir, fileName, name);
		log.Enter; log.TimeStamp; log.String("GET "); log.String(url); log.String(" to "); log.String(name); log.Exit;
		rh.fadr := IP.NilAdr;
		rh.fport := 0;
		rh.method := 0;
		rh.maj := 0; rh.min := 0;
		rh.uri := '';
		rh.host := '';
		rh.referer := '';
		rh.useragent := "WebHTTPTool/0.1";
		rh.accept := '';
		rh.transferencoding := '';
		rh.additionalFields := НУЛЬ;
		res := WebHTTPClient.Ok;
		con := НУЛЬ;
		in := НУЛЬ;
		WebHTTPClient.Get(url, rh, con, h, in, res);
		если res = WebHTTPClient.Ok то
			file := Files.New(name);
			Files.OpenWriter(fw, file, 0);
			если (h.transferencoding # "") и MatchPrefixI("chunked", h.transferencoding) то
				in.ПропустиПробелыИТабуляции(); in.чЦепочкуСимв8ДоБелогоПоля(token); cs := HexStrToIntDef(token, 0); in.ПропустиДоКонцаСтрокиТекстаВключительно();
				нцПока cs # 0 делай
					нцДля i := 0 до cs - 1 делай in.чСимв8(ch); fw.пСимв8( ch) кц;
					 in.ПропустиДоКонцаСтрокиТекстаВключительно;in.ПропустиПробелыИТабуляции; in.чЦепочкуСимв8ДоБелогоПоля(token); cs := HexStrToIntDef(token, 0); in.ПропустиДоКонцаСтрокиТекстаВключительно;
				кц;
				если fw.кодВозвратаПоследнейОперации = Потоки.Успех то log.Enter; log.TimeStamp; log.String("GET "); log.String(url); log.String(" - OK"); log.Exit
				иначе log.Enter; log.TimeStamp; log.String("GET "); log.String(url); log.String(" - failed"); log.Exit
				всё
			иначе
				если h.contentlength >= 0 то
					нцДля i := 0 до h.contentlength - 1 делай in.чСимв8(ch);  fw.пСимв8(ch) кц;
					если fw.кодВозвратаПоследнейОперации = Потоки.Успех то log.Enter; log.TimeStamp; log.String("GET "); log.String(url); log.String(" - OK"); log.Exit
					иначе log.Enter; log.TimeStamp; log.String("GET "); log.String(url); log.String(" - failed"); log.Exit
					всё
				иначе
					нцПока in.кодВозвратаПоследнейОперации = Потоки.Успех делай in.чСимв8(ch); fw.пСимв8(ch) кц;
					log.Enter; log.TimeStamp; log.String("GET "); log.String(url); log.String(" - OK"); log.Exit
				всё
			всё;
			fw.ПротолкниБуферВПоток;
			Files.Register(file);
			con.Закрой
		иначе
			log.Enter; log.TimeStamp; log.String("GET "); log.String(url); log.String(" - "); log.Int(h.statuscode, 5);
			log.String(" ("); log.Int(res, 0); log.String(") ");
			log.String(h.reasonphrase); log.Exit
		всё
	кц;
кон GetAll;

проц Read*(context : Commands.Context);
перем
	url: массив 512 из симв8;
	reader: WebHTTPClient.ContentReader;
	rh: WebHTTP.RequestHeader;
	h: WebHTTP.ResponseHeader;
	in: Потоки.Чтец;
	con: TCP.Connection;
	res: целМЗ;
нач
	если ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(url) то возврат всё;

	log.Enter; log.TimeStamp; log.String("GET "); log.String(url); log.Exit;
	rh.useragent := "WebHTTPTool/0.1";
	WebHTTPClient.Get(url, rh, con, h, in, res);
	если res = WebHTTPClient.Ok то
		нов(reader, in, h);
		нцПока reader.кодВозвратаПоследнейОперации = 0 делай
			context.out.пСимв8(reader.чИДайСимв8());
		кц
	всё
кон Read;

проц Cleanup;
нач
	log.Close
кон Cleanup;

нач
	нов(log, "WebHTTPTools");
	Modules.InstallTermHandler(Cleanup)
кон WebHTTPTools.
(* 
WebHTTPTools.Get http://www.enigon.com/ test.html~
WebHTTPTools.Get http://212.254.73.92/ test.html~
WebHTTPTools.Get http://www.nzz.ch/ test.html~
WebHTTPTools.Head http://212.254.73.92/~
WebHTTPTools.Head http://www.microsoft.com~
WebHTTPTools.Head http://slashdot.org~ (* whats wrong with slashdot ? telnet worked... *)

WebHTTPTools.Get https://www.mediapart.fr/ test.html ~
WebHTTPTools.Get http://files.rcsb.org/download/4hhb.cif.gz test.gz ~
WebHTTPTools.Get https://highdim.com/ test.html ~
WebHTTPTools.Get https://www.startpage.com/ test.html ~
WebHTTP.Mod

System.Free WebHTTPTools
System.FreeDownTo WebHTTP  ~
WebHTTPClient WebWormWatch WebHTTPServer WebHTTP~
WebHTTPServer.Start~
WebWormWatch.Install~

WebHTTPTools.GetAll http://files.rcsb.org/download/ WORK: 4hhb.cif 4hhb.cif.gz 4hhb.cif 4hhb.cif.gz ~
WebHTTPTools.Read http://www.highdim.com ~
*)

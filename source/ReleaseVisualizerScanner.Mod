модуль ReleaseVisualizerScanner; (** AUTHOR "TF"; PURPOSE "Active Oberon Scanner for Release Visualizer Tool"; *)

использует
	Texts, Потоки, UTF8Strings, Strings;

конст
	Eot* = 0X;
	ObjectMarker = 020X;

	(* numtyp values *)
	char* = 1; integer* = 2; longinteger* = 3; real* = 4; longreal* = 5;
	null* =   0; times* =   1; slash* =   2; div* =   3; mod* =   4; and* =   5;
	plus* =   6; minus* =   7; or* =   8; eql* =   9; neq* =  10; lss* =  11;
	leq* =  12; gtr* =  13; geq* =  14; in* =  15; is* =  16; arrow* =  17;
	period* =  18; comma* =  19; colon* =  20; upto* =  21; rparen* =  22;
	rbrak* =  23; rbrace* =  24; of* =  25; then* =  26; do* =  27; to* =  28;
	by* =  29; lparen* =  30; lbrak* =  31; lbrace* =  32; not* =  33;
	becomes* =  34; number* =  35; nil* =  36; true* =  37; false* =  38;
	string* =  39; ident* =  40; semicolon* =  41; bar* =  42; end* =  43;
	else* =  44; elsif* =  45; until* =  46; if* =  47; case* =  48; while* =  49;
	repeat* =  50; for* =  51; loop* =  52; with* =  53; exit* =  54;
	passivate* =  55; return* =  56; refines* =  57; implements* =  58;
	array* =  59; definition* =  60; object* =  61; record* =  62; pointer* =  63;
	begin* =  64; code* =  65; const* =  66; type* =  67; var* =  68;
	procedure* =  69; import* =  70; module* =  71; eof* =  72;
	comment* = 73;

перем
	reservedChar-, newChar: массив 256 из булево;

тип

	StringMaker* = окласс
	перем
		length : размерМЗ;
		data : Strings.String;

		проц &Init(initialSize : размерМЗ);
		нач
			если initialSize < 256 то initialSize := 256 всё;
			нов(data, initialSize); length := 0;
		кон Init;

		проц Add*(конст buf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);
		перем i : размерМЗ; n : Strings.String;
		нач
			если length + len + 1 >= длинаМассива(data) то
				нов(n, длинаМассива(data) + len + 1); нцДля i := 0 до length - 1 делай n[i] := data[i] кц;
				data := n
			всё;
			нцПока len > 0 делай
				data[length] := buf[ofs];
				увел(ofs); увел(length); умень(len)
			кц;
			data[length] := 0X;
		кон Add;

		проц Clear*;
		нач
			data[0] := 0X;
			length := 0
		кон Clear;

		проц GetWriter*() : Потоки.Писарь;
		перем w : Потоки.Писарь;
		нач
			нов(w, сам.Add, 256);
			возврат w
		кон GetWriter;

		проц GetLength*() : размерМЗ;
		нач
			возврат length
		кон GetLength;

		проц GetString*() : Strings.String;
		нач
			возврат data
		кон GetString;

	кон StringMaker;


	Scanner* = окласс
		перем
			buffer: Strings.String;
			pos: цел32;	(*pos in buffer*)
			ch-: симв8;	(**look-ahead *)
			str-: массив 1024 из симв8;
			sym- : цел32;
			numStartPos, numEndPos: цел32;
			curpos-, errpos-: цел32;	(*pos in text*)
			isNummer: булево;
			commentStr- : StringMaker;
			cw : Потоки.Писарь;

		проц &Init;
		нач
			нов(commentStr, 1024);
			cw := commentStr.GetWriter()
		кон Init;

		проц err(n: цел16);
		нач
		кон err;

		проц NextChar*;
		нач
			если pos < длинаМассива(buffer) то
				ch := buffer[pos]; увел(pos)
			иначе
				ch := Eot
			всё;
			если newChar[кодСимв8(ch)] то увел(curpos) всё; (* curpos := pos; *)
		кон NextChar;

		проц Str(перем sym: цел32);
		перем i: цел32; och: симв8;
		нач i := 0; och := ch;
			нц NextChar;
				если ch = och то прервиЦикл всё ;
				если ch < " " то err(3); прервиЦикл всё ;
				если i = длинаМассива(str)-1 то err(241); прервиЦикл всё ;
				str[i] := ch; увел(i)
			кц ;
			NextChar; str[i] := 0X;
			если i = 1 то
				sym := number
			иначе sym := string
			всё
		кон Str;

		проц Identifier(перем sym: цел32);
			перем i: цел32;
		нач i := 0;
			нцДо
				str[i] := ch; увел(i); NextChar
			кцПри reservedChar[кодСимв8(ch)] или (i = длинаМассива(str));
			если i = длинаМассива(str) то err(240); умень(i) всё ;
			str[i] := 0X; sym := ident;
			(* temporary code! delete when moving to ANY and adapt PCT *)
			если str = "ANY" то копируйСтрокуДо0("PTR", str) всё;
		кон Identifier;

		проц GetNumAsString*(перем val: массив из симв8);
		перем i, l: размерМЗ;
		нач
			(*Strings.Copy(buffer^, numStartPos, numEndPos-numStartPos, val);*)
			если isNummer то
				i := 0; l := длинаМассива(val)-1;
				нцПока (i < numEndPos-numStartPos) и (i < l) делай
					val[i] := buffer[numStartPos + i];
					увел(i);
				кц;
			всё;
			val[i] := 0X
		кон GetNumAsString;

		проц Get(перем s: цел32);

			проц Comment;	(* do not read after end of file *)
			нач NextChar; cw.пСимв8(ch);
				нц
					нц
						нцПока ch = "(" делай NextChar; cw.пСимв8(ch);
							если ch = "*" то Comment всё
						кц;
						если ch = "*" то NextChar; cw.пСимв8(ch); прервиЦикл всё ;
						если ch = Eot то прервиЦикл всё ;
						NextChar; cw.пСимв8(ch);
					кц ;
					если ch = ")" то NextChar; cw.пСимв8(ch); прервиЦикл всё ;
					если ch = Eot то err(5); прервиЦикл всё
				кц
			кон Comment;

		нач
			нцДо
				нцПока ch <= " " делай (*ignore control characters*)
					если ch = Eot то
						s := eof; возврат
					иначе NextChar
					всё
				кц ;
				errpos := curpos - 1;
				isNummer := ложь;
				просей ch из   (* ch > " " *)
					| 22X, 27X  : Str(s)
					| "#"  : s := neq; NextChar
					| "&"  : s :=  and; NextChar
					| "("  : NextChar;
							 если ch = "*" то commentStr.Clear; Comment; cw.ПротолкниБуферВПоток; s := comment;		(*allow recursion without reentrancy*)
							 иначе s := lparen
							 всё
					| ")"  : s := rparen; NextChar
					| "*"  : s:=times; NextChar
					| "+"  : s :=  plus; NextChar
					| ","  : s := comma; NextChar
					| "-"  : s :=  minus; NextChar
					| "."  : NextChar;
									 если ch = "." то NextChar; s := upto иначе s := period всё
					| "/"  : s :=  slash; NextChar
					| "0".."9": isNummer := истина; numStartPos := pos-1; (*Number;*) numEndPos := pos-1; s := number
					| ":"  : NextChar;
									 если ch = "=" то NextChar; s := becomes иначе s := colon всё
					| ";"  : s := semicolon; NextChar
					| "<"  : NextChar;
									 если ch = "=" то NextChar; s := leq; иначе s := lss; всё
					| "="  : s :=  eql; NextChar
					| ">"  : NextChar;
									 если ch = "=" то NextChar; s := geq; иначе s := gtr; всё
					| "A": Identifier(s);
								если str = "ARRAY" то s := array
								аесли str = "AWAIT" то s := passivate
								всё
					| "B": Identifier(s);
								если str = "BEGIN" то s := begin
								аесли str = "BY" то s := by
								всё
					| "C": Identifier(s);
								если str = "CONST" то s := const
								аесли str = "CASE" то s := case
								аесли str = "CODE" то s := code
								всё
					| "D": Identifier(s);
								если str = "DO" то s := do
								аесли str = "DIV" то s := div
								аесли str = "DEFINITION" то s := definition
								всё
					| "E": Identifier(s);
								если str = "END" то s := end
								аесли str = "ELSE" то s := else
								аесли str = "ELSIF" то s := elsif
								аесли str = "EXIT" то s := exit
								всё
					| "F": Identifier(s);
								если str = "FALSE" то s := false
								аесли str = "FOR" то s := for
								всё
					| "I": Identifier(s);
								если str = "IF" то s := if
								аесли str = "IN" то s := in
								аесли str = "IS" то s := is
								аесли str = "IMPORT" то s := import
								аесли str = "IMPLEMENTS" то s := implements
								всё
					| "L": Identifier(s);
								если str = "LOOP" то s := loop всё
					| "M": Identifier(s);
								если str = "MOD" то s := mod
								аесли str = "MODULE" то s := module
								всё
					| "N": Identifier(s);
								если str = "NIL" то s := nil всё
					| "O": Identifier(s);
								если str = "OR" то s := or
								аесли str = "OF" то s := of
								аесли str = "OBJECT" то s := object
								всё
					| "P": Identifier(s);
								если str = "PROCEDURE" то s := procedure
								аесли str = "POINTER" то s := pointer
								всё
					| "R": Identifier(s);
								если str = "RECORD" то s := record
								аесли str = "REPEAT" то s := repeat
								аесли str = "RETURN" то s := return
								аесли str = "REFINES" то s := refines
								всё
					| "T": Identifier(s);
								если str = "THEN" то s := then
								аесли str = "TRUE" то s := true
								аесли str = "TO" то s := to
								аесли str = "TYPE" то s := type
								всё
					| "U": Identifier(s);
								если str = "UNTIL" то s := until всё
					| "V": Identifier(s);
								если str = "VAR" то s := var всё
					| "W": Identifier(s);
								если str = "WHILE" то s := while
								аесли str = "WITH" то s := with
								всё
					| "G".."H", "J", "K", "Q", "S", "X".."Z": Identifier(s)
					| "["  : s := lbrak; NextChar
					| "]"  : s := rbrak; NextChar
					| "^"  : s := arrow; NextChar
					| "a".."z": Identifier(s)
					| "{"  : s := lbrace; NextChar
					| "|"  : s := bar; NextChar
					| "}"  : s := rbrace; NextChar
					| "~"  : s := not; NextChar
					| 7FX  : s := upto; NextChar
				иначе  Identifier(s); (* s := null; NextChar; *)
				всё ;
			кцПри s >= 0;
		кон Get;

		проц Next*;
		нач
			Get(sym)
		кон Next;


	кон Scanner;

проц InitWithText*(t: Texts.Text; pos: цел32): Scanner;
	перем buffer: Strings.String; len, i, j: размерМЗ; ch: симв32; r: Texts.TextReader;
	bytesPerChar: цел32;
	s : Scanner;
нач
	t.AcquireRead;
	len := t.GetLength();
	bytesPerChar := 2;
	нов(buffer, len * bytesPerChar);	(* UTF8 encoded characters use up to 5 bytes *)
	нов(r, t);
	r.SetPosition(pos);
	j := 0;
	нцДля i := 0 до len-1 делай
		r.ReadCh(ch);
		нцПока ~UTF8Strings.EncodeChar(ch, buffer^, j) делай
				(* buffer too small *)
			увел(bytesPerChar);
			ExpandBuf(buffer, bytesPerChar * len);
		кц;
	кц;
	t.ReleaseRead;
	нов(s); s.buffer := buffer;
	s.pos := 0;
	s.ch := " ";
	возврат s;
кон InitWithText;

проц ExpandBuf(перем oldBuf: Strings.String; newSize: размерМЗ);
перем newBuf: Strings.String; i: размерМЗ;
нач
	если длинаМассива(oldBuf^) >= newSize то возврат всё;
	нов(newBuf, newSize);
	нцДля i := 0 до длинаМассива(oldBuf^)-1 делай
		newBuf[i] := oldBuf[i];
	кц;
	oldBuf := newBuf;
кон ExpandBuf;

проц InitReservedChars;
перем
	i: цел32;
нач
	нцДля i := 0 до длинаМассива(reservedChar)-1 делай
		если симв8ИзКода(i) <= 20X то	(* TAB, CR, ESC ... *)
			reservedChar[i] := истина;
		иначе
			просей симв8ИзКода(i) из
				| "#", "&", "(", ")", "*", "+", ",", "-", ".", "/": reservedChar[i] := истина;
				| ":", ";", "<", "=", ">": reservedChar[i] := истина;
				| "[", "]", "^", "{", "|", "}", "~": reservedChar[i] := истина;
				| "$": reservedChar[i] := истина;
				| 22X, 27X, 7FX: reservedChar[i] := истина;	(* 22X = ", 27X = ', 7FX = del *)
			иначе
				reservedChar[i] := ложь;
			всё;
		всё;
	кц;
кон InitReservedChars;

проц InitNewChar;
перем
	i: цел32;
нач
	нцДля i := 0 до длинаМассива(newChar)-1 делай
		(* UTF-8 encoded characters with bits 10XXXXXX do not start a new unicode character *)
		если (i < 80H) или (i > 0BFH) то
			newChar[i] := истина;
		иначе
			newChar[i] := ложь;
		всё
	кц
кон InitNewChar;


нач
	InitReservedChars;
	InitNewChar;
кон ReleaseVisualizerScanner.

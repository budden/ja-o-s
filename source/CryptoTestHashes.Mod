модуль CryptoTestHashes;	(** AUTHOR "F.N."; PURPOSE "Hashes Test"; *)

использует
	Hashes := CryptoHashes,	Utils := CryptoUtils, Kernel, Log := ЛогЯдра;

	(* data: binary, exphash: hexadecimal *)
	проц CheckHash( конст modname, data, exphash: массив из симв8 );
	перем
		temp: массив 1024 из симв8;
		hash: массив 128 из симв8;
		h: Hashes.Hash;
	нач
		h := Hashes.NewHash( modname );
		h.Initialize;
		h.Update( data, 0, длинаМассива( data )-1 );
		h.GetHash( hash, 0 );
		Log.пВК_ПС; Log.пСтроку8( "************************************" );
		Log.пВК_ПС; Log.пСтроку8( "Checking: " ); Log.пСтроку8( h.name );
		Log.пВК_ПС; Log.пСтроку8( "Value:" ); Log.пВК_ПС; Log.пСтроку8( "      '" ); Log.пСтроку8( data ); Log.пСтроку8( "'" );
		Log.пВК_ПС; Log.пСтроку8( "Computed Hash:" ); Utils.PrintHex( hash, 0, h.size );
		Utils.Hex2Bin( exphash, 0, temp, 0, h.size );
		Log.пВК_ПС; Log.пСтроку8( "Expected Hash:" ); Utils.PrintHex( temp, 0, h.size );
	кон CheckHash;



	проц MeasureTime( конст modname: массив из симв8 );
	перем
		c, i : цел32;
		milliTimer : Kernel.MilliTimer;
		buf: массив 1024 из симв8;
		hash: массив 1024 из симв8;
		h: Hashes.Hash;
	нач
		h := Hashes.NewHash( modname );
		Log.пВК_ПС; Log.пСтроку8( "************************************" );
		Log.пВК_ПС; Log.пСтроку8( "Measuring: " ); Log.пСтроку8( h.name ); Log.пВК_ПС;
		нцДля i := 0 до 999 делай buf[i] := 'a' кц;
		Kernel.SetTimer(milliTimer, 0);
		нцДля c := 1 до 10 делай
			h.Initialize;
			нцДля i := 1 до 1000 делай h.Update( buf, 0, 1000 ) кц;
			Log.пСимв8( "." )
		кц;
		h.GetHash( hash, 0 );
		Log.пСтроку8( "hashed 10 MB in " ); Log.пЦел64( Kernel.Elapsed(milliTimer), 0 ); Log.пСтроку8( " msec" );
		Log.пВК_ПС;
	кон MeasureTime;

	проц TestMD5*;
	нач
		CheckHash( "CryptoMD5", "", "D41D8CD98F00B204E9800998ECF8427E" );
		CheckHash( "CryptoMD5",
						"abc",
						"900150983CD24FB0D6963F7D28E17F72" );
		CheckHash( "CryptoMD5",
						"abcdefghijklmnopqrstuvwxyz",
						"C3FCD3D76192E4007DFB496CCA67E13B");
		MeasureTime( "CryptoMD5" );
	кон TestMD5;

	проц TestSHA1*;
	нач
		CheckHash( "CryptoSHA1", 
				"", 
				"DA39A3EE5E6B4B0D3255BFEF95601890AFD80709" );
		CheckHash( "CryptoSHA1", 
				"abc", 
				"A9993E364706816ABA3E25717850C26C9CD0D89D" );
		CheckHash( "CryptoSHA1",
				"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq",
				"84983E441C3BD26EBAAE4AA1F95129E5E54670F1");
		MeasureTime( "CryptoSHA1" );
	кон TestSHA1;

	проц TestSHA256*;
	нач
		CheckHash( "CryptoSHA256",
				"abc",
				"BA7816BF8F01CFEA414140DE5DAE2223B00361A396177A9CB410FF61F20015AD" );
		CheckHash( "CryptoSHA256",
				"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq",
				"248D6A61D20638B8E5C026930C3E6039A33CE45964FF2167F6ECEDD419DB06C1" );
		MeasureTime( "CryptoSHA256" );
	кон TestSHA256;

	проц TestSHA3*;
	нач
		CheckHash( "CryptoSHA3",
				"",
				"a7ffc6f8bf1ed76651c14756a061d662f580ff4de43b49fa82d80a4b80f8434a" );
		CheckHash( "CryptoSHA3",
				"abc",
				"3a985da74fe225b2045c172d6bd390bd855f086e3e9d525b46bfe24511431532" );
		CheckHash( "CryptoSHA3",
				"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq",
				"41c0dba2a9d6240849100376a8235e2c82e1b9998a999e21db32dd97496d3376" );
		MeasureTime( "CryptoSHA3" );
	кон TestSHA3;

кон CryptoTestHashes.


System.Free
	CryptoTestHashes
	CryptoSHA3 CryptoKeccakSponge CryptoKeccakF1600
	CryptoMD5 CryptoSHA1 CryptoSHA256
	CryptoHashes
	~

CryptoTestHashes.TestMD5  ~
CryptoTestHashes.TestSHA1  ~
CryptoTestHashes.TestSHA256  ~
CryptoTestHashes.TestSHA3  ~

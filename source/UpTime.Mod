модуль UpTime; (** AUTHOR "staubesv"; PURPOSE "Up-Time Monitor"; *)

использует
	ЛогЯдра, Потоки, Commands, Dates;

перем
	startTime : Dates.DateTime;

проц Get*(перем days, hours, minutes, seconds : цел32);
нач
	Dates.TimeDifference(startTime, Dates.Now(), days, hours, minutes, seconds);
кон Get;

проц ToStream*(stream : Потоки.Писарь);
перем days, hours, minutes, seconds : цел32;
нач
	утв(stream # НУЛЬ);
	Get(days, hours, minutes, seconds);
	stream.пЦел64(days, 0); stream.пСтроку8(" days ");
	если (hours < 10) то stream.пСимв8("0"); всё;
	stream.пЦел64(hours, 0); stream.пСимв8(":");
	если (minutes < 10) то stream.пСимв8("0"); всё;
	stream.пЦел64(minutes, 0); stream.пСимв8(":");
	если (seconds < 10) то stream.пСимв8("0"); всё;
	stream.пЦел64(seconds, 0);
кон ToStream;

проц GetStartTime*() : Dates.DateTime;
нач
	возврат startTime;
кон GetStartTime;

проц Show*(context : Commands.Context);
нач
	context.out.пСтроку8("Uptime: "); ToStream(context.out); context.out.пВК_ПС;
кон Show;

проц Install*;
нач
	ЛогЯдра.пСтроку8("Uptime monitor started."); ЛогЯдра.пВК_ПС;
кон Install;

нач
	startTime := Dates.Now();
кон UpTime.

System.Free UpTime ~

UpTime.Show ~

(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль LinEqCholesky;   (** AUTHOR "adf"; PURPOSE "LLT matrix decomposition"; *)

использует Nbr := NbrRe, Vec := VecRe, Mtx := MtxRe, Errors := DataErrors, Math := MathRe, LinEq := LinEqRe;

тип
	(** For solving moderate sized linear systems of equations where the matrix is symmetric positive definite. *)
	Solver* = окласс (LinEq.Solver)
	перем dim: цел32;
		mtxMag: Nbr.Real;
		lMtx: укль на массив из укль на массив из Nbr.Real;

		проц Decompose( перем a: Mtx.Matrix );
		перем i, j, k: цел32;  adjustment, sum: Nbr.Real;
		нач
			(* Cholesky decomposition of the normalized matrix  a. *)
			нцДля k := 0 до dim - 1 делай
				нцДля i := 0 до k - 1 делай
					sum := a.Get( k, i );
					нцДля j := 0 до i - 1 делай adjustment := lMtx[i, j] * lMtx[k, j];  sum := sum - adjustment кц;
					lMtx[k, i] := sum / lMtx[i, i]
				кц;
				sum := a.Get( k, k );
				нцДля i := 0 до k - 1 делай adjustment := lMtx[k, i] * lMtx[k, i];  sum := sum - adjustment кц;
				если sum > 0 то lMtx[k, k] := Math.Sqrt( sum )
				иначе Errors.Error( "The supplied matrix was not positive definite." )
				всё
			кц
		кон Decompose;

	(** Requires NEW to pass matrix A as a parameter when creating a solver object. *)
		проц & {перекрыта}Initialize*( перем A: Mtx.Matrix );
		перем i: цел32;  a: Mtx.Matrix;
		нач
			если A # НУЛЬ то
				если A.rows = A.cols то
					a := A.Copy();  dim := A.cols;  LinEq.NormalizeMatrix( a, mtxMag );  нов( lMtx, dim );
					нцДля i := 0 до dim - 1 делай нов( lMtx[i], i + 1 ) кц;
					Decompose( a );  a := НУЛЬ
				иначе Errors.Error( "The supplied matrix was not square." )
				всё
			иначе Errors.Error( "A NIL matrix was supplied." )
			всё
		кон Initialize;

	(** Solves  Ax = b  for  x  given  b. *)
		проц {перекрыта}Solve*( перем b: Vec.Vector ): Vec.Vector;
		перем i, k: цел32;  adjustment, coef, mag, sum: Nbr.Real;  x: Vec.Vector;
		нач
			если b # НУЛЬ то
				если dim = b.lenx то
					x := b.Copy();  LinEq.NormalizeVector( x, mag );
					(* Forward substitution.  Solves  L y = b  for  y  *)
					нцДля i := 0 до dim - 1 делай
						sum := x.Get( i );
						нцДля k := 0 до i - 1 делай adjustment := lMtx[i, k] * x.Get( k );  sum := sum - adjustment кц;
						coef := sum / lMtx[i, i];  x.Set( i, coef )
					кц;
					(* Backward substitution.  Solves  LTx = y  for  x  *)
					нцДля i := dim - 1 до 0 шаг -1 делай
						sum := x.Get( i );
						нцДля k := i + 1 до dim - 1 делай adjustment := lMtx[k, i] * x.Get( k );  sum := sum - adjustment кц;
						coef := sum / lMtx[i, i];  x.Set( i, coef )
					кц;
					(* Renormalize the solution. *)
					x.Multiply( mag / mtxMag )
				иначе x := НУЛЬ;  Errors.Error( "Incompatible dimension for vector b." )
				всё
			иначе x := НУЛЬ;  Errors.Error( "A NIL right-hand-side vector was supplied." )
			всё;
			возврат x
		кон Solve;

	кон Solver;


	(** Computes the inverse of matrix A and returns A-1 if it exists; otherwise, it returns NIL. *)
	проц Invert*( перем A: Mtx.Matrix ): Mtx.Matrix;
	перем i, j, k: цел32;  adjustment, sum: Nbr.Real;  inverse: Mtx.Matrix;
		lMtxInv: укль на массив из укль на массив из Nbr.Real;
		llt: Solver;
	нач
		inverse := НУЛЬ;
		если A # НУЛЬ то
			если A.rows = A.cols то
				нов( llt, A );
				(* Invert the Cholesky decomposition matrix  L  to get  L-1. *)
				нов( lMtxInv, llt.dim );
				нцДля i := 0 до llt.dim - 1 делай нов( lMtxInv[i], i + 1 ) кц;
				нцДля i := 0 до llt.dim - 1 делай
					lMtxInv[i, i] := 1 / llt.lMtx[i, i];
					нцДля j := i + 1 до llt.dim - 1 делай
						sum := 0;
						нцДля k := i до j - 1 делай adjustment := llt.lMtx[j, k] * lMtxInv[k, i];  sum := sum - adjustment кц;
						lMtxInv[j, i] := sum / llt.lMtx[j, j]
					кц
				кц;
				(* Acquire the inverse, i.e.,  A-1 = L-TL-1. *)
				нов( inverse, 0, llt.dim, 0, llt.dim );
				нцДля i := 0 до llt.dim - 1 делай
					нцДля j := 0 до i делай
						sum := 0;
						нцДля k := i до llt.dim - 1 делай adjustment := lMtxInv[k, i] * lMtxInv[k, j];  sum := sum + adjustment кц;
						inverse.Set( i, j, sum );
						если i # j то inverse.Set( j, i, sum ) всё
					кц
				кц;
				(* Renormalize the result. *)
				inverse.Divide( llt.mtxMag )
			иначе Errors.Error( "The supplied matrix was not square." )
			всё
		иначе Errors.Error( "A NIL matrix was supplied." )
		всё;
		возврат inverse
	кон Invert;

кон LinEqCholesky.

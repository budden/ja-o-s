модуль TeletextFont;	(** TF, modified by OJ **)

использует
	ЛогЯдра,
	Graphics := WMGraphics, Raster,
	XML, Parser := XMLParser, Scanner := XMLScanner, Objects := XMLObjects,
	Strings, WMRectangles,
	Files;

тип
	Симв32ВВидеЦел32 = цел32;
	Glyph = запись
		img : Graphics.Image;
		code : Симв32ВВидеЦел32; (* import only *)
		fpos : цел32;
		loaded : булево;
	кон;
	GlyphArray = укль на массив из Glyph;

	GlyphRange = запись
		firstCode, lastCode : цел32; (* inclusive *)
		glyphs : GlyphArray;
	кон;
	GlyphRangeArray = укль на массив из GlyphRange;

	Font = окласс(Graphics.Font)
	перем
		nofGlyphRanges : цел32;
		glyphRanges : GlyphRangeArray;
		grc : цел32;
		placeholderimg : Graphics.Image;
		fontFile : Files.File;
		empty : WMRectangles.Rectangle;

		проц &{перекрыта}Init*;
		перем mode : Raster.Mode; pix : Raster.Pixel;
		нач
			Init^;
			nofGlyphRanges := 0; grc := 0;
			empty := WMRectangles.MakeRect(0, 0, 0, 0); (* save the proc call *)
			нов(placeholderimg); Raster.Create(placeholderimg, 16, 16, Raster.A1);
			Raster.InitMode(mode, Raster.srcCopy);
			Raster.SetRGBA(pix, 255, 0, 0, 0);
			Raster.Fill(placeholderimg, 0, 0, 15, 15, pix, mode);
			ascent := 10; descent := 8;
		кон Init;

		проц {перекрыта}GetGlyphMap*(code : симв32; перем map : Graphics.Image);
		перем g : Glyph;
		нач
			если FindGlyph(кодСимв32(code), g) то
				если ~g.loaded то LoadGlyph(code, g) всё;
				map := g.img
			иначе map := placeholderimg
			всё
		кон GetGlyphMap;

		проц {перекрыта}HasChar(char : симв32) : булево;
		перем dummy : цел32;
		нач
			возврат FindGlyphRange(кодСимв32(char), dummy)
		кон HasChar;


		проц {перекрыта}GetGlyphSpacings*(code : симв32; перем glyphSpacings : Graphics.GlyphSpacings);
		перем g : Glyph;
		нач
			если FindGlyph(кодСимв32(code), g) то
				если ~g.loaded то LoadGlyph(code, g) всё;
				glyphSpacings.width := g.img.width; glyphSpacings.height := g.img.height; glyphSpacings.dy := 0;
				glyphSpacings.bearing := empty;
				glyphSpacings.ascent := 10; glyphSpacings.descent := 8;

			иначе glyphSpacings.width := 13; glyphSpacings.height := 10;
			всё
		кон GetGlyphSpacings;

		проц LoadGlyph(code : симв32; перем g : Glyph);
		перем gri : цел32;
		нач
			если FindGlyph(кодСимв32(code), g) и ~g.loaded то
				ReadGlyph(fontFile, g); g.loaded := истина;
				если FindGlyphRange(кодСимв32(code), gri) то glyphRanges[gri].glyphs[кодСимв32(code) - glyphRanges[gri].firstCode] := g всё;
			всё
		кон LoadGlyph;

		проц FindGlyphRange(code : Симв32ВВидеЦел32; перем gri : цел32) : булево;
		перем a, b, m : цел32;
		нач
			если nofGlyphRanges = 0 то возврат ложь всё;
			(* check cached clyph range *)
			если (glyphRanges[grc].firstCode <= code) и (glyphRanges[grc].lastCode >= code) то
				gri := grc; возврат истина
			всё;
			a := 0; b := nofGlyphRanges - 1;
			нцПока (a < b) делай m := (a + b) DIV 2;
				если glyphRanges[m].lastCode < code то a := m + 1
				иначе b := m
				всё
			кц;
			если (glyphRanges[a].firstCode <= code) и (glyphRanges[a].lastCode >= code) то
				gri := a; grc := a; возврат истина
			иначе возврат ложь
			всё
		кон FindGlyphRange;

		проц FindGlyph(code : Симв32ВВидеЦел32; перем glyph : Glyph) : булево;
		перем gri : цел32;
		нач
			если FindGlyphRange(code, gri) то glyph := glyphRanges[gri].glyphs[code - glyphRanges[gri].firstCode]; возврат истина
			иначе возврат ложь
			всё
		кон FindGlyph;

		проц CountGlyphes():цел32;
		перем i, c : цел32;
		нач
			нцДля i := 0 до nofGlyphRanges - 1 делай
				c := c + glyphRanges[i].lastCode - glyphRanges[i].firstCode + 1;
			кц;
			возврат c
		кон CountGlyphes;

		проц Import(filename : массив из симв8);
		перем f : Files.File;
			scanner : Scanner.Scanner;
			parser : Parser.Parser;
			reader : Files.Reader;
			doc : XML.Document;
			p : динамическиТипизированныйУкль;
			root : XML.Element;
			el : XML.Content;
			s : Strings.String;
			cont : Objects.Enumerator;
			nofGlyphs : цел32;
			glyphs : GlyphArray;

			curindex : цел32;

			проц CountRanges(): цел32;
			перем i : цел32; c, r: цел32;
			нач
				c := glyphs[0].code; r := 1;
				нцДля i := 1 до nofGlyphs - 1 делай
					если (glyphs[i].code # c) то увел(r); c := glyphs[i].code всё;
					увел(c);
				кц;
				возврат r
			кон CountRanges;

			проц GetRangeLength(i : цел32): цел32;
			перем count, c : цел32;
			нач
				count := 1; c := glyphs[i].code;
				нцПока (i + count < nofGlyphs) и (glyphs[i + count].code = c + count) делай увел(count) кц;
				возврат count
			кон GetRangeLength;

			проц MakeRanges;
			перем i, j, rl, r : цел32;
			нач
				nofGlyphRanges := CountRanges();
				нов(glyphRanges, nofGlyphRanges);
				i := 0; r := 0;
				нцПока i < nofGlyphs делай
					rl := GetRangeLength(i);
					нов(glyphRanges[r].glyphs, rl)
					;glyphRanges[r].firstCode := glyphs[i].code;
					glyphRanges[r].lastCode := glyphs[i].code + rl - 1;
					нцДля j := 0 до rl - 1 делай glyphRanges[r].glyphs[j] := glyphs[i]; увел(i) кц;
					увел(r)
				кц
			кон MakeRanges;

			проц HexStrToInt(перем s: массив из симв8): цел32;
				перем vh, d, i: цел32;
			нач
				i:=0;
				vh := 0;
				нц
					если (s[i] >= "0") и (s[i] <= "9") то d := кодСимв8(s[i])-кодСимв8("0")
					аесли (ASCII_вЗаглавную(s[i]) >= "A") и (ASCII_вЗаглавную(s[i]) <= "F") то d := кодСимв8(ASCII_вЗаглавную(s[i]))-кодСимв8("A")+10
					иначе прервиЦикл
					всё;
					 vh := 16 * vh + d;
					увел(i)
				кц;
				возврат vh
			кон HexStrToInt;

			проц ReadByte(перем s : массив из симв8; pos: цел32): цел32;
			перем hex : массив 3 из симв8;
			нач
				Strings.Copy(s, pos, 2, hex);
				возврат HexStrToInt(hex)
			кон ReadByte;

			проц GenChar(x : XML.Element);
			перем scode, sbitmap : XML.String; code, count, i, w, h : цел32; bitmap : Graphics.Image;
				pos : цел32; color : булево;
				p0, p1 : Raster.Pixel;
				mode : Raster.Mode;
			нач
				scode := x.GetAttributeValue("code");
				sbitmap := x.GetAttributeValue("bitmap");
				Raster.InitMode(mode, Raster.srcCopy);
				если (scode # НУЛЬ) и (sbitmap # НУЛЬ) то
					Strings.StrToInt(scode^, code);
					pos := 0;
					w := ReadByte(sbitmap^, pos); увел(pos, 2);
					h := ReadByte(sbitmap^, pos); увел(pos, 2);
					Raster.SetRGBA(p0, 0, 0, 255, 255); Raster.SetRGBA(p1, 255, 255, 255, 255);
					если w * h = 0 то ЛогЯдра.пСтроку8("Illegal char : "); ЛогЯдра.пЦел64(code, 5); ЛогЯдра.пВК_ПС всё;
					нов(bitmap); Raster.Create(bitmap, w, h, Raster.BGR888);
					i := 0;
					нцПока sbitmap[pos] # 0X делай
						count := ReadByte(sbitmap^, pos); увел(pos, 2);
						нцПока count > 0 делай
							если color то Raster.Put(bitmap, i остОтДеленияНа w, i DIV w, p1, mode)
							иначе Raster.Put(bitmap, i остОтДеленияНа w, i DIV w, p0, mode)
							всё;
							увел(i); умень(count);
						кц;
						color := ~color
					кц;

					glyphs[curindex].code := code;
					glyphs[curindex].img := bitmap;
					увел(curindex)
				всё;
			кон GenChar;

		нач
			f := Files.Old(filename);
			если f # НУЛЬ то
				нов(reader, f, 0);
				нов(scanner, reader); нов(parser, scanner); doc := parser.Parse()
			всё;

			root := doc.GetRoot();
			cont := root.GetContents(); cont.Reset();
			nofGlyphs := root.GetNumberOfContents();
			нов(glyphs, nofGlyphs);

			ЛогЯдра.пЦел64(root.GetNumberOfContents(), 5); ЛогЯдра.пСтроку8(" glyphs loaded."); ЛогЯдра.пВК_ПС;
			curindex := 0;
			нцПока cont.HasMoreElements() делай
				p := cont.GetNext();
				el := p(XML.Element);
				если el суть XML.Element то
					s := el(XML.Element).GetName();
					если s^ = "char" то GenChar(el(XML.Element)) всё
				всё
			кц;
			MakeRanges
		кон Import;

		(** works up to 255x255x2 *)
		проц RasterToBWRLBytes(img : Raster.Image; перем buf : массив из симв8; перем pos : цел32);
		перем i: размерМЗ; count: цел32;
				p : Raster.Pixel;
				pix, curpix : булево; mode : Raster.Mode;
		нач
			ЛогЯдра.пСтроку8("Entering Raster... ");
			buf[pos] := симв8ИзКода(img.width); увел(pos);
			buf[pos] := симв8ИзКода(img.height); увел(pos);
			если (img.width = 0) или (img.height = 0) то СТОП(12345) всё;
			Raster.InitMode(mode, Raster.srcCopy);
			count := 0; curpix := ложь;
			нцДля i := 0 до img.width * img.height - 1 делай
				Raster.Get(img, i остОтДеленияНа img.width, i DIV img.width, p, mode);
				pix := p[Raster.r] > симв8ИзКода(128);
				если pix # curpix то
					curpix := pix;
					нцПока count > 255 делай buf[pos] := симв8ИзКода(255); увел(pos); buf[pos] := симв8ИзКода(0); увел(pos); умень(count, 255) кц;
					buf[pos] := симв8ИзКода(count); увел(pos);
					count := 0
				всё;
				увел(count)
			кц;
			если count > 0 то
				нцПока count > 255 делай buf[pos] := симв8ИзКода(255); увел(pos); buf[pos] := симв8ИзКода(0); увел(pos); умень(count, 255) кц;
				buf[pos] := симв8ИзКода(count); увел(pos)
			всё
;			ЛогЯдра.пСтроку8("Leaving Raster"); ЛогЯдра.пВК_ПС;
		кон RasterToBWRLBytes;

		проц Save(filename : массив из симв8);
		перем w : Files.Rider; i, j, c: цел32; f : Files.File;
				buf : массив 2024 из симв8; fixup: Files.Position; pos : цел32;
		нач
			f := Files.New(filename);f.Set(w, 0);
			(* write number of ranges *)
			Files.WriteLInt(w, nofGlyphRanges);
			(* write ranges *)
			нцДля i := 0 до nofGlyphRanges - 1 делай
				Files.WriteLInt(w, glyphRanges[i].firstCode);
				Files.WriteLInt(w, glyphRanges[i].lastCode);
			кц;
			fixup := f.Pos(w);

			(* reserve space for per character file position table *)
			нцДля i := 0 до nofGlyphRanges - 1 делай
				нцДля j := 0 до glyphRanges[i].lastCode - glyphRanges[i].firstCode делай
					Files.WriteLInt(w, 0);
				кц
			кц;
			c := 0;
			нцДля i := 0 до nofGlyphRanges - 1 делай
				нцДля j := 0 до glyphRanges[i].lastCode - glyphRanges[i].firstCode делай
					увел(c);
					pos := 0;
					ЛогЯдра.пСтроку8("Calling Raster... ");
					если c < 319 то
						RasterToBWRLBytes(glyphRanges[i].glyphs[j].img, buf, pos);
						glyphRanges[i].glyphs[j].fpos := f.Pos(w)(цел32);
						ЛогЯдра.пЦел64(pos, 5); ЛогЯдра.пВК_ПС;
						f.WriteBytes(w, buf, 0, pos)
					всё
				кц
			кц;
			f.Set(w, fixup);
			c := 0;
			нцДля i := 0 до nofGlyphRanges - 1 делай
				нцДля j := 0 до glyphRanges[i].lastCode - glyphRanges[i].firstCode делай
					увел(c);
					если c < 319 то
						Files.WriteLInt(w, glyphRanges[i].glyphs[j].fpos)
					всё
				кц
			кц;
			f.Update;
			Files.Register(f)
		кон Save;

		проц ReadGlyph(перем f: Files.File;  перем g : Glyph);
		перем r : Files.Rider;
			w, h, i, c : цел32;
			pix : булево;
			p0, p1 : Raster.Pixel;
			mode : Raster.Mode;

			проц GetB():цел32;
			перем ch : симв8;
			нач
				f.Read(r, ch);
				возврат кодСимв8(ch)
			кон GetB;

		нач
			Raster.InitMode(mode, Raster.srcCopy);
			Raster.SetRGBA(p0, 0, 0, 0, 0); Raster.SetRGBA(p1, 0, 0, 0, 255);
			f.Set(r, g.fpos);
			w := GetB(); h := GetB();
			если w * h <= 0 то
				ЛогЯдра.пСтроку8("Empty"); ЛогЯдра.пВК_ПС;
				возврат
			всё;
			нов(g.img); Raster.Create(g.img, w, h, Raster.A1);
			i := 0; pix := ложь;
			нцПока i < w * h делай
				c := GetB();
				нцПока c > 0 делай
					если i >= w * h то ЛогЯдра.пСтроку8("error."); ЛогЯдра.пВК_ПС
					иначе
						если pix то Raster.Put(g.img, i остОтДеленияНа w, i DIV w, p1, mode)
						иначе Raster.Put(g.img, i остОтДеленияНа w, i DIV w, p0, mode)
						всё;
					всё;
					увел(i); умень(c)
				кц;
				pix := ~pix;
			кц
		кон ReadGlyph;

		проц Load(filename : массив из симв8);
		перем r : Files.Rider; i, j: цел32; f : Files.File;
				notenoughregisters: цел32;
		нач
			f := Files.Old(filename); f.Set(r, 0);
			fontFile := f;
			(* read number of ranges *)
			Files.ReadLInt(r, nofGlyphRanges);
			(* read ranges *)
			нов(glyphRanges, nofGlyphRanges);
			нцДля i := 0 до nofGlyphRanges - 1 делай
				Files.ReadLInt(r, glyphRanges[i].firstCode);
				Files.ReadLInt(r, glyphRanges[i].lastCode);
				notenoughregisters := glyphRanges[i].lastCode - glyphRanges[i].firstCode;
				нов(glyphRanges[i].glyphs, notenoughregisters + 1)
			кц;
			нцДля i := 0 до nofGlyphRanges - 1 делай
				нцДля j := 0 до glyphRanges[i].lastCode - glyphRanges[i].firstCode делай
					Files.ReadLInt(r, glyphRanges[i].glyphs[j].fpos);
				кц
			кц;
			(* for now no indexing *)
(*			FOR i := 0 TO nofGlyphRanges - 1 DO
				FOR j := 0 TO glyphRanges[i].lastCode - glyphRanges[i].firstCode DO

					ReadGlyph(f, r, glyphRanges[i].glyphs[j])
				END
			END; *)
		кон Load;

	кон Font;

перем bimbofont* : Font;

проц Load*;
нач
	нов(bimbofont);
	bimbofont.Load("teletext.bfnt");
кон Load;

проц Import*;
нач
	нов(bimbofont);
	bimbofont.Import("teletext.XML");
	ЛогЯдра.пСтроку8("Imported."); ЛогЯдра.пВК_ПС;
	bimbofont.Save("teletext.bfnt");
	ЛогЯдра.пСтроку8("Saved."); ЛогЯдра.пВК_ПС;
кон Import;

нач
	Load;
кон TeletextFont.


TeletextFont.Import ~

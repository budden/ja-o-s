(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль WebWormWatch; (** AUTHOR "TF"; PURPOSE "HTTP plugin to catch worms"; *)

использует
		Потоки, WebHTTP, AosLog := TFLog, Files, WebHTTPServer, Modules, Clock,
		IP, DNS, Kernel, Mail, AosSMTPClient := SMTPClient;

конст
	ShowWormOffals = ложь;

	(* virus capture files *)
	CodeRedVar = "Virus.CodeRedVar.Bin";
	UnknownWorm = "Virus.Unknown.Bin";
	NimdaWorm = "Virus.Nimda.Bin";

	WormLog = "Virus.Log";	(* ASCII log file *)
	WormCache = "Virus.Cache";	(* binary cache file storing virus name and source address *)

		(* mail parameters *)
	ToName1 = "Thomas Frey";  ToAddr1 = "frey@inf.ethz.ch";
	FromName = "Worm Watch";  FromAddr = "frey@inf.ethz.ch";
	SMTPServer = "lillian.inf.ethz.ch";
	SMTPClient = "eth20853.ethz.ch";
	LocalPrefix = "129.132.";

перем log : AosLog.Log;
	nofNimda*, nofCodeRedVar*:цел32;
	lastWormIP*, lastWormName*, lastWormOrigin*: массив 64 из симв8;

тип
	CodeRedPlugin = окласс(WebHTTPServer.HTTPPlugin)

		проц {перекрыта}CanHandle(host: WebHTTPServer.Host; перем h : WebHTTP.RequestHeader; secure : булево): булево;
		перем i : цел32;
		нач
			нцПока (h.uri[i] # 0X) и (i < длинаМассива(h.uri)) делай увел(i) кц;
			возврат (i>100)
		кон CanHandle;

		проц {перекрыта}Handle(host: WebHTTPServer.Host; перем request: WebHTTP.RequestHeader; перем reply: WebHTTP.ResponseHeader;
			перем in: Потоки.Чтец; перем out: Потоки.Писарь);
		перем fn, vn : массив 32 из симв8;
		нач
			если MyMatch(request.uri, "/default.ida") то
				vn := "Code Red variant"; fn := CodeRedVar; увел(nofCodeRedVar)
			иначе
				vn := "Unknown"; fn := UnknownWorm
			всё;
			MyHandle(vn, fn, in, out, request, reply)
		кон Handle;
	кон CodeRedPlugin;

тип
	NimdaPlugin = окласс(WebHTTPServer.HTTPPlugin)

		проц {перекрыта}CanHandle(host: WebHTTPServer.Host; перем h : WebHTTP.RequestHeader; secure : булево): булево;
		нач
			возврат h.uri = "/scripts/root.exe"
		кон CanHandle;

		проц {перекрыта}Handle(host: WebHTTPServer.Host; перем request: WebHTTP.RequestHeader; перем reply: WebHTTP.ResponseHeader;
			перем in: Потоки.Чтец; перем out: Потоки.Писарь);
		нач
			увел(nofNimda);
			MyHandle("Nimda", NimdaWorm, in, out, request, reply)
		кон Handle;

	кон NimdaPlugin;

перем
	crp : CodeRedPlugin;
	np : NimdaPlugin;

проц MyMatch(перем uri :массив из симв8;  y: массив из симв8) : булево;
перем i : цел32;
нач
	нцПока (i < длинаМассива(uri)) и (i < длинаМассива(y)) и (uri[i] = y[i]) и  (y[i] # 0X) делай увел(i) кц;
	возврат  (i < длинаМассива(uri)) и (i < длинаМассива(y)) и (y[i] = 0X)
кон MyMatch;

проц Cached(vn, adr: массив из симв8): булево;
перем f: Files.File; n: массив 64 из симв8; a: массив 16 из симв8; r: Files.Reader; w: Files.Writer; cached: булево;
нач {единолично}
	cached := ложь;
	f := Files.Old(WormCache);
	если f = НУЛЬ то f := Files.New(WormCache) всё;
	если f # НУЛЬ то	(* search cache *)
		Files.OpenReader(r, f, 0);
		нц
			r.чСтроку8˛включаяСимв0(n); r.чСтроку8˛включаяСимв0(a);
			если r.кодВозвратаПоследнейОперации # 0 то прервиЦикл всё;
			если (n = vn) и (a = adr) то cached := истина; прервиЦикл всё
		кц;
		если ~cached то	(* add to cache *)
			Files.OpenWriter(w, f, f.Length());
			w.пСтроку8˛включаяСимв0(vn); w.пСтроку8˛включаяСимв0(adr);
			w.ПротолкниБуферВПоток;
			Files.Register(f)
		всё
	всё;
	возврат cached
кон Cached;

проц MyHandle(vn, fn: массив из симв8; перем in: Потоки.Чтец; перем out: Потоки.Писарь;
	перем header : WebHTTP.RequestHeader; перем reply: WebHTTP.ResponseHeader);
перем
	f: Files.File; w : Files.Writer; res: целМЗ; i, time, date: цел32;
	ch :симв8;
	md : массив 32 из симв8;
	origin : массив 64 из симв8;
	ipstr:массив 16 из симв8;
	timer : Kernel.Timer;
	msg : Mail.Message;
	smtpSession: AosSMTPClient.SMTPSession;
	str: Потоки.ПисарьВСтроку8ФиксированногоРазмера;
нач
	IP.AdrToStr(header.fadr, ipstr);
	DNS.HostByNumber(header.fadr, origin, res);
	копируйСтрокуДо0(ipstr, lastWormIP); копируйСтрокуДо0(origin, lastWormOrigin); копируйСтрокуДо0(vn, lastWormName);
	log.Enter; log.TimeStamp; log.String("Worm Alert : "); log.String(vn); log.String(" ");
	log.String(ipstr); если res = DNS.Ok то log.String("("); log.String(origin); log.String(")") всё;

	если Cached(vn, ipstr) то
		log.Enter; log.TimeStamp;
		log.String("Worm Cache : "); log.String(vn); log.String(" "); log.String(ipstr);
		log.Exit;
		возврат
	всё;

	если MyMatch(ipstr, LocalPrefix) то	(* ETH infection: send a Mail *)
		log.Ln; i := 0 ;
		нов(msg);
		msg.AddTo(ToName1, ToAddr1);
		msg.SetFrom(FromName, FromAddr);
		нов(str, 64);
		Clock.Get(time, date); str.пДатуОберона822(time, date, 0);
		str.ДайПрочитанное˛сколькоПоместитсяИСимвол0(md); msg.SetDate(md);
		msg.SetSubject("Worm Infection report");
		IP.AdrToStr(header.fadr, ipstr);
		msg.AddLine("Infected IP");
		msg.AddLine(ipstr);
		msg.AddLine(origin);
		msg.AddLine(vn);
		нов(smtpSession);
		smtpSession.Open(SMTPServer, SMTPClient, 25, res);
		если res = AosSMTPClient.Ok то
			smtpSession.Send(msg, res)
		всё;
		smtpSession.Close
	всё;
	f := Files.New(fn); Files.OpenWriter(w, f, 0);
	нов(timer);
	нцПока in.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() > 0 делай
		ch := in.чИДайСимв8();
		если ShowWormOffals то log.Hex(кодСимв8(ch), -3); увел(i); если i остОтДеленияНа 16 = 0 то log.Ln всё всё;
		если in.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() = 0 то timer.Sleep(2000) всё;
		w.пСимв8(ch);
	кц;
	w.ПротолкниБуферВПоток;
	Files.Register(f);
	log.Exit;
	если header.method в {WebHTTP.GetM, WebHTTP.HeadM} то
		WebHTTP.WriteStatus(reply, out);
		out.пСтроку8("Content-Type: "); out.пСтроку8("text/html"); out.пВК_ПС;
		out.пВК_ПС;
		если (header.method = WebHTTP.GetM) то
			out.пСтроку8("<HTML>");
			out.пСтроку8("Your request seems to be a worm attack. Failed."); out.пВК_ПС;
			out.пСтроку8("</HTML>");
			out.пВК_ПС
		всё
	иначе
		reply.statuscode := WebHTTP.NotImplemented;
		WebHTTP.WriteStatus(reply, out)
	всё;
	out.ПротолкниБуферВПоток
кон MyHandle;

проц Install*;
перем hl: WebHTTPServer.HostList;
нач
	если crp = НУЛЬ то
		нов(crp, "CodeRed-Plugin");
		нов(np, "Nimda-Plugin");
		hl := WebHTTPServer.FindHosts("*");
		нцПока (hl # НУЛЬ) делай
			hl.host.AddPlugin(crp);
			hl.host.AddPlugin(np);
			hl := hl.next
		кц;
		log.Enter; log.String("Worm Watch Plugin installed"); log.Exit;
	 иначе
		log.Enter; log.String("Worm Watch Plugin already installed"); log.Exit;
	всё;
кон Install;

проц Close;
перем h: WebHTTPServer.HostList;
нач
	если crp # НУЛЬ то
		h := WebHTTPServer.FindHosts("*");
		нцПока (h # НУЛЬ) делай
			h.host.RemovePlugin(crp);
			h.host.RemovePlugin(np);
			h := h.next
		кц;
		log.Enter; log.String("Worm Watch Plugin removed"); log.Exit; log.Close;
		crp := НУЛЬ; np := НУЛЬ;
	всё
кон Close;

нач
	lastWormOrigin := "No last worm origin";
	lastWormIP := "No last worm IP";
	lastWormName := "No last worm name";
	нов(log, "Worm Watch");
	log.SetLogFile(WormLog);
	Modules.InstallTermHandler(Close)
кон WebWormWatch.


System.Free WebWormWatch ~
Aos.Call WebWormWatch.Install ~

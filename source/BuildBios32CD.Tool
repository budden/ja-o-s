# Постройка образа ЯОС, с которого можно загзурить компьютер IBM PC 
# Порядок выполнения:
#   - установить QEMU (у меня работает на версии 2.11.1
#   - создать пустой img образ диска, например, баш-скриптом ЯОС:BIOS32/создай-или-сотри-жёсткий-диск-для-установки-с-cd.баш
#   - выполнить 4 команды в данном файле - появится образ ЯОС:BIOS32/JA-O-S.iso
#   - выйти из ЯОС
#   - перейти в директорию ЯОС:BIOS32
#   - запустить в QEMU скриптом яос-загрузка-с-живого-cd.баш
#   - после успешной инсталляции запускать скриптом яос-установленная-с-cd.баш
#   - записать образ на диск, например, в Windows правая кнопка мыши/записать образ диска
#   - загрузиться с диска (сам диск работает на hp nc8000, инсталлятор не проверял)


# 1. Создаём директорию

FSTools.CreateDirectory ЯОС:NewBios32CD ~

# 2. Очищаем старое и собираем

System.DoCommands
	FSTools.DeleteFiles -i ЯОС:NewBios32CD/* ~
	СборщикВыпускаЯОС.Скомпилируй --скомпилируй --лис --путьКОбъектнымФайлам="ЯОС:NewBios32CD/" --порождаяАрхивы --генерируяФайлXMLДляУстановщика Bios32 ~
~

# 3. удаление лишнего из архива (не всегда работает)

ZipTool.Delete --ignore ЯОС:NewBios32CD/Applications.zip InstallerPackages.XML ~ 


# 4. Подготовка образа диска

System.DoCommands 

System.Timer start ~

ZipTool.Add --nopath ЯОС:NewBios32CD/Applications.zip ЯОС:NewBios32CD/InstallerPackages.XML ~
FSTools.DeleteFiles -i ЯОС:NewBios32CD/JA-O-S.iso ~

FSTools.Mount RAMDISK RamFS 200000 4096 ~
FSTools.DeleteFiles -i RAMDISK:JA-O-S.Dsk ~

PCAAMD64.Assemble OBLUnreal.Asm ~

Linker.Link --path=ЯОС:NewBios32CD/ 
	--displacement=100000H 
	--fileName=ЯОС:NewBios32CD/CD.Bin 
	Kernel Traps ATADisks
	DiskVolumes 
	RAMVolumes 
	DiskFS Loader BootConsole ~


VirtualDisks.Create RAMDISK:JA-O-S.Dsk 480000 512 ~
VirtualDisks.Install -c=80 -h=2 -s=18 -b=512 VDISK0 RAMDISK:JA-O-S.Dsk  ~

PartitionsLib.SetBootLoaderFile OBLUnreal.Bin ~
Partitions.Format VDISK0#0 AosFS -1 ЯОС:NewBios32CD/CD.Bin ~ (* -1 makes sure that actual boot file size is taken as offset for AosFS *)
FSTools.Mount TEMP AosFS VDISK0#0 ~ 


ZipTool.ExtractAll --prefix=TEMP: --sourcePath=ЯОС:NewBios32CD/ --overwrite --silent
Kernel.zip System.zip Drivers.zip ApplicationsMini.zip Applications.zip Compiler.zip 
GuiApplicationsMini.zip GuiApplications.zip Build.zip EFI.zip
TrueTypeFonts.zip BuildSrc.zip KernelSrc.zip SystemSrc.zip DriversSrc.zip ApplicationsMiniSrc.zip ApplicationsSrc.zip GuiApplicationsMiniSrc.zip GuiApplicationsSrc.zip 
ScreenFonts.zip TrueTypeFonts.zip ~


FSTools.CopyFiles ЯОС:NewBios32CD/*.zip => TEMP:*.zip ~
Linker.Link --path=ЯОС:NewBios32CD/ --displacement=100000H --fileName=ЯОС:NewBios32CD/IDE.Bin Kernel Traps ATADisks DiskVolumes DiskFS Loader BootConsole ~

FSTools.CopyFiles ЯОС:NewBios32CD/IDE.Bin => TEMP:IDE.Bin ~
FSTools.CopyFiles OBLUnreal.Bin => TEMP:OBLUnreal.Bin ~
FSTools.DeleteFiles TEMP:OBL.Bin ~

FSTools.DeleteFiles -i TEMP:Configuration.XML ~
FSTools.CopyFiles BIOS.Configuration.XML => TEMP:Configuration.XML ~

FSTools.Unsafe ~
FSTools.DeleteFiles -i TEMP:*.перев.XML ~
FSTools.CopyFiles *.перев.XML => TEMP:/*.XML ~
FSTools.Safe ~

FSTools.Watch TEMP ~
FSTools.Unmount TEMP ~

Partitions.SetConfig VDISK0#0
TraceMode="4" TracePort="1" TraceBPS="115200"
BootVol1="RAM RamFS 80000 4096"
BootVol2="CD AosFS #0,R"
RamFS="RAMVolumes.New DiskFS.NewFS"
AosFS="DiskVolumes.New DiskFS.NewFS"
CacheSize="1000"
ExtMemSize="512"
MaxProcs="-1"
ATADetect="legacy"
Init="117"
Boot="DisplayLinear.Install"
Boot1="Keyboard.Install;MousePS2.Install"
Boot2="DriverDatabase.Enable;UsbHubDriver.Install;UsbEhciPCI.Install;UsbUhci.Install;UsbOhci.Install"
Boot3="WindowManager.Install"
Boot4="ЗагрузиПереводыЭлементовКода.ИзВсехФайлов"
Boot5="Autostart.Run"
Boot6="WMInstaller.Open"
~
VirtualDisks.Uninstall VDISK0 ~
IsoImages.Make ЯОС:NewBios32CD/JA-O-S.iso RAMDISK:JA-O-S.Dsk ~
FSTools.Unmount RAMDISK ~

System.Show время подготовки образа CD: ~ System.Timer elapsed ~

FSTools.CloseFiles ЯОС:NewBios32CD/JA-O-S.iso ~
FSTools.DeleteFiles -i ЯОС:BIOS32/JA-O-S.iso  ~
FSTools.CopyFiles ЯОС:NewBios32CD/JA-O-S.iso => ЯОС:BIOS32/JA-O-S.iso ~

~

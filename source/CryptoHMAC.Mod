модуль CryptoHMAC;  	(** AUTHOR "G.F."; PURPOSE "RFC 2104 HMAC"; *)

использует Hashes := CryptoHashes, Strings, BIT;

тип
	HMac* = окласс
		перем
			size-: размерМЗ;		(** mac size L in bytes *)
			name-: массив 64 из симв8;
			ih, oh: Hashes.Hash;
			L: размерМЗ;  (* hash blocksize, mac size *)
			n: размерМЗ; (* processed data bytes *)

		проц & Init*( конст hashmod: массив из симв8 );
		нач
			ih := Hashes.NewHash( hashmod );
			oh := Hashes.NewHash( hashmod );
			L := ih.size;  size := L;
			n := 0;
			name := "hmac-";  Strings.Append( name, ih.name );
		кон Init;
		
		проц ShrinkLength*( len: размерМЗ );
		перем bits: массив 8 из симв8;
		нач
			утв( (len < size) и (n = 0) );
			Strings.IntToStr( 8*len, bits );
			Strings.Append( name, "-" );
			Strings.Append( name, bits );
			size := len
		кон ShrinkLength;

		(** Set a key, recommended key-length is the hash-size of the underlying hash-function.
			This method has to be invoked for EACH mac to be calculated *)
		проц Initialize*( конст key: массив из симв8; len: размерМЗ );
		перем
			usedkey, buf: массив 64 из симв8;
			i: размерМЗ;
		нач
			если len > 64 то
				ih.Initialize;
				ih.Update( key, 0, len );
				ih.GetHash( usedkey, 0 ); 
				i := L;
				нцПока i < 64 делай  usedkey[i] := 0X;  увел( i )  кц;
			иначе
				i := 0;
				нцПока i < len делай  usedkey[i] := key[i];  увел( i )  кц;
				нцПока i < 64 делай  usedkey[i] := 0X;  увел( i )  кц;
			всё;
			oh.Initialize;
			нцДля i := 0 до 63 делай  buf[i] := BIT.CXOR( 5CX, usedkey[i] )  кц;
			oh.Update( buf, 0, 64 );
			ih.Initialize;
			нцДля i := 0 до 63 делай  buf[i] := BIT.CXOR( 36X, usedkey[i] )  кц;
			ih.Update( buf, 0, 64 )
		кон Initialize;

		(** set string from which a mac will be calculated. strings can be concatenated by
			invoking Update several times without invoking Initialize *)
		проц Update*( конст data: массив из симв8;  pos, len: размерМЗ );
		нач
			ih.Update( data, pos, len );  увел( n, len )
		кон Update;

		(** Load the generated mac of size L into buf, starting at position pos *)
		проц GetMac*( перем buf: массив из симв8;  pos: размерМЗ );
		перем
			tmp: массив 64 из симв8;
			i: размерМЗ;
		нач
			ih.GetHash( tmp, 0 );
			oh.Update( tmp, 0, L );
			oh.GetHash( tmp, 0 );
			нцДля i := 0 до size-1 делай  buf[pos + i] := tmp[i]  кц
		кон GetMac;

	кон HMac;

кон CryptoHMAC.


System.Free CryptoHMAC ~

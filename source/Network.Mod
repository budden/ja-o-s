(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Network; (** AUTHOR "pjm, mvt"; PURPOSE "Abstract network device driver"; *)

использует НИЗКОУР, ЭВМ, ЛогЯдра, Plugins, Kernel, Objects, Modules;

конст
	MaxLinkAdrSize* = 8; (** largest link address size in bytes *)
	MaxPacketSize* = 1600; (** maximum amount of data bytes in a link layer frame *)
	MaxNofBuffers = 10000; (* maximum number of buffers allowed within the whole net system *)

	(** Constants for LinkDevice.type *)
	TypePointToPoint* = 0;
	TypeEthernet* = 1;

	(** Constants for LinkDevice.Linked *)
	LinkNotLinked* = 0;
	LinkLinked* = 1;
	LinkUnknown* = 2;

	(** Constants for LinkDevice.calcChecksum and Buffer.calcChecksum *)
	ChecksumIP* = 0;
	ChecksumUDP* = 1;
	ChecksumTCP* = 2;

	(* Number of loopback packets that can be sent per 1-2 ms.
		This protects the upcall buffers from running out. *)
	MaxLoopbackPacketsPerMS = 500;

тип
	LinkAdr* = массив MaxLinkAdrSize из симв8; (** link layer address *)

	(** Buffer for passing network packets to upper layer protocols *)
	Buffer* = укль на запись
		data*:массив MaxPacketSize из симв8;
		ofs*: цел32; (** valid data starts at this offset *)
		len*: цел32; (** length of valid data *)
		l3ofs*: цел32; (** the layer 3 header starts at this offset *)
		l4ofs*: цел32; (** the layer 4 header starts at this offset *)
		src*: LinkAdr; (** link layer source address *)
		calcChecksum*: мнвоНаБитахМЗ; (** these checksums are already verified by the device *)
		int*: цел32; (** used in TCP, UDP and ICMP, but can be used by any upper layer protocol *)
		set*: мнвоНаБитахМЗ; (** used in TCP, but can be used by any upper layer protocol *)
		next*, prev*: Buffer; (** for queueing the buffer *)
		nextFragment*: Buffer; (** next buffer of a fragmented packet *)
	кон;

	(* List of type i.e. 800X for IP *)
	TypeList = укль на запись
		next: TypeList;
		type: цел32;
		recList: ReceiverList;
	кон;

	ReceiverList = укль на запись
		next: ReceiverList;
		owner: динамическиТипизированныйУкль;
		receiver: Receiver;
		isPacketValid: IsPacketValid;
		isPacketForSingleRec: IsPacketForSingleRec;
		isPacketAccepted: IsPacketAccepted;
		isForwardingOn: булево;
	кон;

	SendSnifferList = укль на запись
		next: SendSnifferList;
		sniffer: SendSniffer;
	кон;

	RecvSnifferList = укль на запись
		next: RecvSnifferList;
		sniffer: ReceiveSniffer;
	кон;

	(** Abstract implementation of a generic network driver object *)

	LinkDevice* = окласс (Plugins.Plugin)
		перем
			(** pubic device properties *)
			type-: цел32; (** LinkType: TypePointToPoint, TypeEthernet *)
			local*: LinkAdr; (** local link address *)
			broadcast*: LinkAdr; (** link address for sending a broadcast *)
			mtu-: цел32; (** largest packet size in bytes *)
			adrSize*: цел32; (** link address size in bytes *)
			sendCount*, recvCount-: цел64; (** number of bytes sent and received *)
			calcChecksum*: мнвоНаБитахМЗ; (** these checksums are calculated by the device hardware when sending. *)

			typeList: TypeList; (* List of types i.e. 0800X for IP, holds all receiver for a certain type *)
			recList: ReceiverList; (* receiver list *)
			sendSnifferList: SendSnifferList; (* list for send sniffers *)
			recvSnifferList: RecvSnifferList; (* list for receive sniffers *)

			typeItem: TypeList; (* temporary item in type list *)
			recItem: ReceiverList; (* temporary item in receiver list *)
			sniffer: RecvSnifferList; (* temporary item in receive sniffer list *)

			discard: булево; (* shall the current packet be discarded? (used in active body) *)
			finalized: булево; (* is object already finalized or currently finalizing? *)

			(* queue for buffers waiting for upcall *)
			upBufFirst, upBufLast: Buffer;
			buf: Buffer; (* temporary buffer for active body *)
			bufSec: Buffer; (* temporary buffer for multiple interfaces listening *)

			(* timer and packet count for loopback bandwidth control *)
			timer: Kernel.MilliTimer;
			packetCount: цел32;

			i: цел32;

		(** Constructor - Initialize the driver and the device.
			NOTE:
			Is normally overridden by device driver. If so, this constructor has to be called at the beginning
			of the overriding constructor!
		*)
		проц &Constr*(type, mtu, adrSize: цел32);
		нач
			утв((mtu >= 0) и (mtu <= MaxPacketSize));
			утв((adrSize >= 0) и (adrSize <= MaxLinkAdrSize));
			если type = TypeEthernet то
				утв(adrSize = 6);
			всё;
			сам.type := type;
			сам.mtu := mtu;
			сам.adrSize := adrSize;
			сам.sendCount := 0;
			сам.recvCount := 0;
			сам.calcChecksum := {};

			typeList := НУЛЬ;
			recList := НУЛЬ;
			upBufFirst := НУЛЬ;

			Kernel.SetTimer(timer, 2);
			packetCount := 0;

			finalized := ложь;

			sendSnifferList := НУЛЬ;
			recvSnifferList := НУЛЬ;
		кон Constr;

		(** Destructor - Finalize driver object. If connected = TRUE, device is still connected and has to be deinitialized.
			NOTE:
			Is normally overridden by device driver. If so, this method has to be called at the end
			of the overriding method!
		*)
		проц Finalize*(connected: булево);
		нач {единолично}
			утв(~finalized);
			finalized := истина;
		кон Finalize;

		(** Return the link status of the device.
			This function has to be overridden by the device driver in order to provide this information.
		*)
		проц Linked*(): цел32;
		нач
			возврат LinkUnknown;
		кон Linked;

		(** Send a packet. Called by its user. Can be called concurrently. *)
		проц Send*(dst: LinkAdr; type: цел32; конст l3hdr, l4hdr, data: массив из симв8; h3len, h4len, dofs, dlen: цел32; loopback: булево);
		перем
			sniffer: SendSnifferList;
			discard: булево; (* shall the packet be discarded? *)
		нач (* can run concurrently with InstallSendSniffer and RemoveSendSniffer *)
			утв(~finalized);
			discard := ложь;
			sniffer := sendSnifferList;
			нцПока sniffer # НУЛЬ делай
				(* call sniffer *)
				discard := discard или sniffer^.sniffer(сам, dst, type, l3hdr, l4hdr, data, h3len, h4len, dofs, dlen);
				sniffer := sniffer^.next;
			кц;
			если ~discard то
				(* send the packet *)
				если loopback то
					Loopback(dst, type, l3hdr, l4hdr, data, h3len, h4len, dofs, dlen);
				иначе
					DoSend(dst, type, l3hdr, l4hdr, data, h3len, h4len, dofs, dlen);
				всё;
				увел(sendCount, dlen + h3len + h4len);
			всё;
		кон Send;

		(** Do frame send operation. Must be overridden and implemented by device driver! *)
		(** Must be able to handle concurrent calls. e.g. by declaring itself as EXCLUSIVE! *)

		проц DoSend*(dst: LinkAdr; type: цел32; конст l3hdr, l4hdr, data: массив из симв8; h3len, h4len, dofs, dlen: цел32);
		нач
			СТОП(301); (* Abstract! *)
		кон DoSend;

		(* Do internal loopback. Send packet directly to the receive queue. *)

		проц Loopback(dst: LinkAdr; type: цел32; конст l3hdr, l4hdr, data: массив из симв8; h3len, h4len, dofs, dlen: цел32);
		перем buf: Buffer;
		нач
			если packetCount >= MaxLoopbackPacketsPerMS то
				нцПока ~Kernel.Expired(timer) делай
					(* no more packets can be sent until timer is expired *)
					Objects.Yield();
				кц;
				Kernel.SetTimer(timer, 2);
				packetCount := 0;
			всё;

			buf := GetNewBuffer();
			если buf # НУЛЬ то
				buf.l3ofs := 0;
				buf.l4ofs := 0;
				buf.ofs := 0;
				buf.len := 0;
				buf.src := dst;
				buf.calcChecksum := {ChecksumIP, ChecksumUDP, ChecksumTCP};

				(* Copy data to receive buffer *)
				Copy(l3hdr, buf.data, 0, buf.len, h3len);
				увел(buf.len, h3len);
				Copy(l4hdr, buf.data, 0, buf.len, h4len);
				увел(buf.len, h4len);
				Copy(data, buf.data, dofs, buf.len, dlen);
				увел(buf.len, dlen);

				(* Queue the receive buffer *)
				QueueBuffer(buf, type);
				ЭВМ.атомарноУвел(packetCount)
			иначе (* packet loss in loopback :o *)

			всё
		кон Loopback;

		(** Install a receiver for the given type. *)
		проц InstallReceiver*(owner: динамическиТипизированныйУкль;
									type: цел32;
									receiver: Receiver;
									isPacketValid: IsPacketValid;
									isPacketForSingleRec: IsPacketForSingleRec;
									isPacketAccepted: IsPacketAccepted;
									isForwardingOn: булево);
		перем
			typeItem: TypeList;
			recItem: ReceiverList;

		нач {единолично}	(* can run concurrently with active body *)
			утв(owner # НУЛЬ);
			утв(~finalized);
			утв(receiver # НУЛЬ);
			утв(isPacketValid # НУЛЬ);
			утв(isPacketForSingleRec # НУЛЬ);
			утв(isPacketAccepted # НУЛЬ);

			(* exists the type already? *)
			typeItem := typeList;
			нцПока (typeItem # НУЛЬ) и (typeItem^.type # type) делай
				typeItem := typeItem^.next;
			кц;

			если typeItem = НУЛЬ то
				(* create new type item *)
				нов(typeItem);
				typeItem^.next := typeList;
				typeItem^.type := type;
				typeItem^.recList := НУЛЬ;
				typeList := typeItem;
			всё;

			(* create a new receiver list item *)
			нов(recItem);
			recItem^.owner := owner;
			recItem^.receiver := receiver;
			recItem^.isPacketValid := isPacketValid;
			recItem^.isPacketForSingleRec := isPacketForSingleRec;
			recItem^.isPacketAccepted := isPacketAccepted;
			recItem^.isForwardingOn := isForwardingOn;
			recItem^.next := typeItem^.recList;
			typeItem^.recList := recItem;
		кон InstallReceiver;

		(** Remove the currently installed receiver for the given type. *)
		проц RemoveReceiver*(owner: динамическиТипизированныйУкль; type: цел32);
		перем
			typeItem: TypeList;
			recItem: ReceiverList;
			typeItemDel: TypeList;

		нач {единолично}		(* can run concurrently with active body *)
			утв(owner # НУЛЬ);
			утв(~finalized);

			(* search type *)
			typeItem := typeList;
			нцПока (typeItem # НУЛЬ) и (typeItem^.type # type) делай
				typeItem := typeItem^.next;
			кц;

			если typeItem # НУЛЬ то
				(* search and remove receiver *)
				recItem := typeItem^.recList;

				если (recItem # НУЛЬ) и (recItem^.owner = owner) то
					(* remove first item *)
					typeItem^.recList := recItem^.next;
				иначе
					(* search list *)
					нцПока (recItem^.next # НУЛЬ) и (recItem^.next^.owner # owner) делай
						recItem := recItem^.next;
					кц;

					если recItem^.next # НУЛЬ то
						(* found a receiver *)
						recItem^.next := recItem^.next^.next;
					всё;
				всё;

				(* If there is no receiver anymore remove type *)
				если typeItem^.recList = НУЛЬ то
					typeItemDel := typeItem;
					typeItem := typeList;

					если (typeItem = typeItemDel) то
						(* remove first type *)
						typeList := typeItem^.next;
					иначе
						(* search type list *)
						нцПока (typeItem^.next # typeItemDel) делай
							typeItem := typeItem^.next;
						кц;
						typeItem^.next := typeItem^.next^.next;
					всё;
				всё;
			всё;
		кон RemoveReceiver;

		(** Install a sniffer for sent packets *)
		проц InstallSendSniffer*(s: SendSniffer);
		перем item: SendSnifferList;
		нач {единолично}
			утв(~finalized);
			item := sendSnifferList;
			нцПока (item # НУЛЬ) и (item^.sniffer # s) делай
				item := item^.next;
			кц;
			если item # НУЛЬ то
				(* sniffer already registered *)
			иначе
				нов(item);
				item^.sniffer := s;
				item^.next := sendSnifferList;
				sendSnifferList := item;
			всё;
		кон InstallSendSniffer;

		(** Remove a sniffer for sent packets *)
		проц RemoveSendSniffer*(s: SendSniffer);
		перем item: SendSnifferList;
		нач {единолично}
			утв(~finalized);
			если sendSnifferList = НУЛЬ то
				(* empty list *)
			аесли sendSnifferList^.sniffer = s то
				(* remove first item *)
				sendSnifferList := sendSnifferList^.next;
			иначе
				(* search list *)
				item := sendSnifferList;
				нцПока (item^.next # НУЛЬ) и (item^.next^.sniffer # s) делай
					item := item^.next;
				кц;
				если item^.next # НУЛЬ то
					item^.next := item^.next^.next;
				иначе
					(* sniffer not found *)
				всё;
			всё;
		кон RemoveSendSniffer;

		(** Install a sniffer for received packets *)
		проц InstallReceiveSniffer*(s: ReceiveSniffer);
		перем item: RecvSnifferList;
		нач {единолично}
			утв(~finalized);
			item := recvSnifferList;
			нцПока (item # НУЛЬ) и (item^.sniffer # s) делай
				item := item^.next;
			кц;
			если item # НУЛЬ то
				(* sniffer already registered *)
			иначе
				нов(item);
				item^.sniffer := s;
				item^.next := recvSnifferList;
				recvSnifferList := item;
			всё;
		кон InstallReceiveSniffer;

		(** Remove a sniffer for received packets *)
		проц RemoveReceiveSniffer*(s: ReceiveSniffer);
		перем item: RecvSnifferList;
		нач {единолично}
			утв(~finalized);
			если recvSnifferList = НУЛЬ то
				(* empty list *)
			аесли recvSnifferList^.sniffer = s то
				(* remove first item *)
				recvSnifferList := recvSnifferList^.next;
			иначе
				(* search list *)
				item := recvSnifferList;
				нцПока (item^.next # НУЛЬ) и (item^.next^.sniffer # s) делай
					item := item^.next;
				кц;
				если item^.next # НУЛЬ то
					item^.next := item^.next^.next;
				иначе
					(* sniffer not found *)
				всё;
			всё;
		кон RemoveReceiveSniffer;

		(** Queue buffer for upcall. Called from inside the LinkDevice object, normally from the interrupt handler. *)
		проц QueueBuffer*(buf: Buffer; type: цел32);
		нач {единолично}
			утв(buf # НУЛЬ);
			buf.int := type; (* use "int" field for type information *)
			buf.next := НУЛЬ;
			если upBufFirst = НУЛЬ то
				upBufFirst := buf;
			иначе
				upBufLast.next := buf;
			всё;
			upBufLast := buf;
		кон QueueBuffer;

	нач {активное, приоритет(Objects.High)}
		(* can run concurrently with SetReceiver, QueueBuffer, InstallReceiverSniffer and RemoveReceiverSniffer *)
		нц
			нач {единолично}
				дождись((upBufFirst # НУЛЬ) или finalized);
				если (upBufFirst = НУЛЬ) и finalized то
					(* terminate process after all buffer upcalls are done *)
					прервиЦикл;
				всё;
				buf := upBufFirst;
				upBufFirst := upBufFirst.next;
			кон;
			увел(recvCount, buf.len);
			discard := ложь;
			sniffer := recvSnifferList;
			нцПока sniffer # НУЛЬ делай
				(* call sniffer *)
				discard := discard или sniffer^.sniffer(сам, buf.int, buf);
				sniffer := sniffer^.next;
			кц;
			если ~discard то
				(* search for type *)
				typeItem := typeList;
				нцПока (typeItem # НУЛЬ) и (typeItem^.type # buf.int) делай
					typeItem := typeItem^.next;
				кц;
				если typeItem # НУЛЬ то
					(* type item found check if packet is valid *)
					recItem := typeItem^.recList;
					если (recItem # НУЛЬ) и (recItem^.isPacketValid(buf)) то
						если recItem^.next # НУЛЬ то
							(* multiple receivers installed *)
							если ~(recItem^.isPacketForSingleRec(buf)) то
								(* multiple receivers copy buffer *)
								нцПока recItem^.next # НУЛЬ делай
									bufSec := GetNewBuffer();
									(* copy buffer *)
									нцДля i := 0 до MaxPacketSize - 1 делай
										bufSec^.data[i] := buf^.data[i];
									кц;
									bufSec^.ofs := buf^.ofs;
									bufSec^.len := buf^.len;
									bufSec^.l3ofs := buf^.l3ofs;
									bufSec^.l4ofs := buf^.l4ofs;
									bufSec^.src := buf^.src;
									bufSec^.calcChecksum := buf^.calcChecksum;
									bufSec^.int := buf^.int;
									bufSec^.set := buf^.set;

									(* deliver copied buffer to interface *)
									recItem^.receiver(сам, typeItem^.type, bufSec);
									recItem := recItem^.next;
								кц;

								(* deliver original buffer *)
								recItem^.receiver(сам, typeItem^.type, buf);
							иначе
								(* search for a single receiver *)
								нцПока (recItem # НУЛЬ) и ~(recItem^.isPacketAccepted(buf)) делай
									recItem := recItem^.next;
								кц;
								если recItem # НУЛЬ то
									(* deliver buffer *)
									recItem^.receiver(сам, typeItem^.type, buf);
								иначе
									(* Packet is not accepted ip ipforwarding is turned on deliver it *)
									recItem := typeItem^.recList;
									если recItem^.isForwardingOn то
										(* deliver buffer *)
										recItem^.receiver(сам, typeItem^.type, buf);
									иначе
										discard := истина;
									всё;
								всё;
							всё;
						иначе
							(* single receiver deliver buffer directly *)
							если recItem^.isPacketAccepted(buf) или ~(recItem^.isPacketForSingleRec(buf)) то
								recItem^.receiver(сам, typeItem^.type, buf);
							иначе
								(* Packet is not accepted ip ipforwarding is turned on deliver it *)
								если recItem^.isForwardingOn то
									(* deliver buffer *)
									recItem^.receiver(сам, typeItem^.type, buf);
								иначе
									discard := истина;
								всё;
							всё;
						всё;
					иначе
						discard := истина;
					всё;
				иначе
					discard := истина;
				всё;
			всё;

			если discard то
				(* discard packet and return buffer *)
				ReturnBuffer(buf);
			всё;
		кц;
	кон LinkDevice;

тип
	(** Upcall procedures *)

	(** Packet receiver upcall
		CAUTION:
		After the buffer has been used, it has to be returned by calling Network.ReturnBuffer(buffer)!
		The Receiver can do this by itself or delegate this job to other procedures or processes, wherever the
		buffer is passed to. It has not necessarily to be returned within the receiver upcall.
	*)
	Receiver* = проц {делегат} (dev: LinkDevice; type: цел32; buffer: Buffer);
	IsPacketForSingleRec* = проц {делегат} (buffer: Buffer): булево;	(* Checks if an incoming packet should be sent to only one installed receivers *)
	IsPacketAccepted* = проц {делегат} (buffer: Buffer): булево;	(* Checks if an incoming packet is accepted from a certain installed receiver *)
	IsPacketValid* = проц {делегат} (перем buffer: Buffer): булево;	(* Checks if an incoming packet is valid *)

	(* Sniffer for sent packets. May modify type, headers and data. Return TRUE if packet shall be discarded. *)
	(* Must be able to handle concurrent calls. e.g. by declaring itself as EXCLUSIVE. *)
	SendSniffer* = проц {делегат} (dev: LinkDevice; перем dst: LinkAdr; перем type: цел32; конст l3hdr, l4hdr, data: массив из симв8; перем h3len, h4len, dofs, dlen: цел32): булево;

	(* Sniffer for received packets. May modify type and buffer. Return TRUE if packet shall be discarded. *)
	(* Will never be called concurrenty from the same LinkDevice. *)
	ReceiveSniffer* = проц {делегат} (dev: LinkDevice; перем type: цел32; buffer: Buffer): булево;

(** Module variables *)

перем
	registry*: Plugins.Registry;

	nofBuf: цел32; (* number of buffers existing *)
	nofFreeBuf: цел32; (* number of free buffers *)
	freeBufList: Buffer; (* free buffer list *)

(** Get a new buffer - return NIL if MaxNofBuffers is exceeded *)
проц GetNewBuffer*(): Buffer;
перем item: Buffer;
нач {единолично}
	если freeBufList # НУЛЬ то
		(* free buffer is available *)
		item := freeBufList;
		freeBufList := freeBufList.next;
		ЭВМ.атомарноУвелНа(nofFreeBuf, -1);
	аесли nofBuf < MaxNofBuffers то
		(* no free buffer available - create new one *)
		нов(item);
		ЭВМ.атомарноУвел(nofBuf);
	иначе
		(* not allowed to create more buffers *)
		item := НУЛЬ;
	всё;
	возврат item;
кон GetNewBuffer;

(** Return a buffer to be reused *)
проц ReturnBuffer*(buf: Buffer);
перем
	oldBuffer: Buffer;

нач {единолично}
	oldBuffer := buf;
	нцПока buf # НУЛЬ делай
		buf.next := freeBufList;
		freeBufList := buf;
		ЭВМ.атомарноУвел(nofFreeBuf);
		oldBuffer := buf;
		buf := buf.nextFragment;
		oldBuffer.nextFragment := НУЛЬ;
	кц;
кон ReturnBuffer;

(* Passed to registry.Enumerate() to Finalize each registered LinkDevice *)

проц Finalize(p: Plugins.Plugin);
нач
	p(LinkDevice).Finalize(истина);
кон Finalize;

(** Test whether the n bytes of buf1 and buf2 starting at ofs1 and ofs2 respectively are equal *)

проц Equal*(перем buf1, buf2: массив из симв8; ofs1, ofs2, n: размерМЗ): булево;
нач
	нцПока (n > 0) и (buf1[ofs1] = buf2[ofs2]) делай увел(ofs1); увел(ofs2); умень(n) кц;
	возврат n <= 0
кон Equal;

(** Procedures to put and get data from and to arrays. No index checks are done due to performance! *)

(** Put a 32-bit host value into buf[ofs..ofs+3] *)

проц Put4*(перем buf: массив из симв8; ofs: размерМЗ; val: целМЗ);
машКод
#если I386 то
	MOV EAX, [EBP+val]
	MOV EBX, [EBP+ofs]
	MOV ECX, [EBP+buf]
	MOV [ECX+EBX], EAX
#аесли AMD64 то
	MOV EAX, [RBP+val]
	MOV RBX, [RBP+ofs]
	MOV RCX, [RBP+buf]
	MOV [RCX+RBX], EAX
#аесли ARM то
	LDR R0, [FP, #val]
	LDR R1, [FP, #ofs]
	LDR R2, [FP, #buf]
	ADD R2, R2, R1
	#если ALIGNED то
		STR R0, [R2, #0]
	#иначе
		STRB R0, [R2, #0]
		MOV R0, R0, LSR #8
		STRB R0, [R2, #1]
		MOV R0, R0, LSR #8
		STRB R0, [R2, #2]
		MOV R0, R0, LSR #8
		STRB R0, [R2, #3]
	#кон
#иначе
	unimplemented
#кон
кон Put4;

(** Put a 16-bit host value into buf[ofs..ofs+1] *)

проц Put2*(перем buf: массив из симв8; ofs: размерМЗ; val: целМЗ);
машКод
#если I386 то
	MOV EAX, [EBP+val]
	MOV EBX, [EBP+ofs]
	MOV ECX, [EBP+buf]
	MOV [ECX+EBX], AX
#аесли AMD64 то
	MOV EAX, [RBP+val]
	MOV RBX, [RBP+ofs]
	MOV RCX, [RBP+buf]
	MOV [RCX+RBX], AX
#аесли ARM то
	LDR R0, [FP, #val]
	LDR R1, [FP, #ofs]
	LDR R2, [FP, #buf]
	ADD R2, R2, R1
	#если ALIGNED то
		STRH R0, [R2, #0]
	#иначе
		STRB R0, [R2, #0]
		MOV R0, R0, LSR #8
		STRB R0, [R2, #1]
	#кон
#иначе
	unimplemented
#кон
кон Put2;

(** Get a 32-bit host value from buf[ofs..ofs+3] *)

проц Get4*(конст buf: массив из симв8; ofs: размерМЗ): целМЗ;
машКод
#если I386 то
	MOV EBX, [EBP+ofs]
	MOV ECX, [EBP+buf]
	MOV EAX, [ECX+EBX]
#аесли AMD64 то
	MOV RBX, [RBP+ofs]
	MOV RCX, [RBP+buf]
	MOV EAX, [RCX+RBX]
#аесли ARM то
	LDR R1, [FP, #ofs]
	LDR R2, [FP, #buf]
	ADD R2, R2, R1
	#если ALIGNED то
		LDR R0, [R2, #0]
	#иначе
		LDRB R0, [R2, #0]
		LDRB R1, [R2, #1]
		ORR R0, R0, R1, LSL #8
		LDRB R1, [R2, #2]
		ORR R0, R0, R1, LSL #16
		LDRB R1, [R2, #3]
		ORR R0, R0, R1, LSL #24
	#кон
#иначе
	unimplemented
#кон
кон Get4;

(** Get a 16-bit host value from buf[ofs..ofs+1] *)

проц Get2*(конст buf: массив из симв8; ofs: размерМЗ): целМЗ;
машКод
#если I386 то
	MOV EBX, [EBP+ofs]
	MOV ECX, [EBP+buf]
	XOR EAX, EAX
	MOV AX, [ECX+EBX]
#аесли AMD64 то
	MOV RBX, [RBP+ofs]
	MOV RCX, [RBP+buf]
	XOR EAX, EAX
	MOV AX, [RCX+RBX]
#аесли ARM то
	LDR R1, [FP, #ofs]
	LDR R2, [FP, #buf]
	ADD R2, R2, R1
	#если ALIGNED то
		LDRH R0, [R2, #0]
	#иначе
		LDRB R0, [R2, #0]
		LDRB R1, [R2, #1]
		ORR R0, R0, R1, LSL #8
	#кон
#иначе
	unimplemented
#кон
кон Get2;

(** Put a 32-bit host value into buf[ofs..ofs+3] in network byte order *)

проц PutNet4*(перем buf: массив из симв8; ofs: размерМЗ; val: целМЗ);
машКод
#если I386 то
	MOV EAX, [EBP+val]
	XCHG AL, AH
	ROL EAX, 16
	XCHG AL, AH
	MOV EBX, [EBP+ofs]
	MOV ECX, [EBP+buf]
	MOV [ECX+EBX], EAX
#аесли AMD64 то
	MOV EAX, [RBP+val]
	XCHG AL, AH
	ROL EAX, 16
	XCHG AL, AH
	MOV RBX, [RBP+ofs]
	MOV RCX, [RBP+buf]
	MOV [RCX+RBX], EAX
#аесли ARM то
	LDR R0, [FP, #val]
	LDR R1, [FP, #ofs]
	LDR R2, [FP, #buf]
	ADD R2, R2, R1
	#если ALIGNED то
		REV R0, R0
		STR R0, [R2, #0]
	#иначе
		STRB R0, [R2, #3]
		MOV R0, R0, LSR #8
		STRB R0, [R2, #2]
		MOV R0, R0, LSR #8
		STRB R0, [R2, #1]
		MOV R0, R0, LSR #8
		STRB R0, [R2, #0]
	#кон
#иначе
	unimplemented
#кон
кон PutNet4;

(** Put a 16-bit host value into buf[ofs..ofs+1] in network byte order *)

проц PutNet2*(перем buf: массив из симв8; ofs: размерМЗ; val: целМЗ);
машКод
#если I386 то
	MOV EAX, [EBP+val]
	XCHG AL, AH
	MOV EBX, [EBP+ofs]
	MOV ECX, [EBP+buf]
	MOV [ECX+EBX], AX
#аесли AMD64 то
	MOV EAX, [RBP+val]
	XCHG AL, AH
	MOV RBX, [RBP+ofs]
	MOV RCX, [RBP+buf]
	MOV [RCX+RBX], AX
#аесли ARM то
	LDR R0, [FP, #val]
	LDR R1, [FP, #ofs]
	LDR R2, [FP, #buf]
	ADD R2, R2, R1
	#если ALIGNED то
		REV16 R0, R0
		STRH R0, [R2, #0]
	#иначе
		STRB R0, [R2, #1]
		MOV R0, R0, LSR #8
		STRB R0, [R2, #0]
	#кон
#иначе
	unimplemented
#кон
кон PutNet2;

(** Get a 32-bit network value from buf[ofs..ofs+3] in host byte order *)

проц GetNet4*(конст buf: массив из симв8; ofs: размерМЗ): целМЗ;
машКод
#если I386 то
	MOV EBX, [EBP+ofs]
	MOV ECX, [EBP+buf]
	MOV EAX, [ECX+EBX]
	XCHG AL, AH
	ROL EAX, 16
	XCHG AL, AH
#аесли AMD64 то
	MOV RBX, [RBP+ofs]
	MOV RCX, [RBP+buf]
	MOV EAX, [RCX+RBX]
	XCHG AL, AH
	ROL EAX, 16
	XCHG AL, AH
#аесли ARM то
	LDR R1, [FP, #ofs]
	LDR R2, [FP, #buf]
	ADD R2, R2, R1
	#если ALIGNED то
		LDR R0, [R2, #0]
		REV R0, R0
	#иначе
		LDRB R0, [R2, #3]
		LDRB R1, [R2, #2]
		ORR R0, R0, R1, LSL #8
		LDRB R1, [R2, #1]
		ORR R0, R0, R1, LSL #16
		LDRB R1, [R2, #0]
		ORR R0, R0, R1, LSL #24
	#кон
#иначе
	unimplemented
#кон
кон GetNet4;

(** Get a 16-bit network value from buf[ofs..ofs+1] in host byte order *)

проц GetNet2*(конст buf: массив из симв8; ofs: размерМЗ): целМЗ;
машКод
#если I386 то
	MOV EBX, [EBP+ofs]
	MOV ECX, [EBP+buf]
	XOR EAX, EAX
	MOV AX, [ECX+EBX]
	XCHG AL, AH
#аесли AMD64 то
	MOV RBX, [RBP+ofs]
	MOV RCX, [RBP+buf]
	XOR EAX, EAX
	MOV AX, [RCX+RBX]
	XCHG AL, AH
#аесли ARM то
	LDR R1, [FP, #ofs]
	LDR R2, [FP, #buf]
	ADD R2, R2, R1
	#если ALIGNED то
		LDRH R0, [R2, #0]
		REV16 R0, R0
	#иначе
		LDRB R0, [R2, #1]
		LDRB R1, [R2, #0]
		ORR R0, R0, R1, LSL #8
	#кон
#иначе
	unimplemented
#кон
кон GetNet2;

(** Convert a LinkAdr to a printable string (up to size*3 characters) *)

проц LinkAdrToStr*(перем adr: LinkAdr; size: размерМЗ; перем s: массив из симв8);
перем
	i, j: размерМЗ;
	hex: массив 17 из симв8;
нач
	утв(длинаМассива(s) >= size*3); (* enough space for largest result *)
	hex := "0123456789ABCDEF";
	i := 0;
	нцДля j := 0 до size-1 делай
		s[i] := hex[кодСимв8(adr[j]) DIV 10H остОтДеленияНа 10H]; увел(i);
		s[i] := hex[кодСимв8(adr[j]) остОтДеленияНа 10H]; увел(i);
		если j = size-1 то s[i] := 0X иначе s[i] := ":" всё;
		увел(i);
	кц;
кон LinkAdrToStr;

(** Write a link address *)

проц OutLinkAdr*(перем adr: LinkAdr; size: размерМЗ);
перем s: массив MaxLinkAdrSize*3 из симв8;
нач
	LinkAdrToStr(adr, size, s);
	ЛогЯдра.пСтроку8(s);
кон OutLinkAdr;

(** Are two link addresses equl *)
проц LinkAdrsEqual*(adr1: LinkAdr; adr2: LinkAdr): булево;
перем
	i: размерМЗ;
	adrsEqual: булево;

нач
	adrsEqual := истина;
	i := 0;
	нцПока (i<8) и (adr1[i] = adr2[i]) делай
		увел(i);
	кц;

	если i < 8 то
		adrsEqual := ложь;
	всё;

	возврат adrsEqual;
кон LinkAdrsEqual;

(** Copy data from array to array *)
проц Copy*(конст from: массив из симв8; перем to: массив из симв8; fofs, tofs, len: размерМЗ);
нач
	если len > 0 то
		утв((fofs+len <= длинаМассива(from)) и (tofs+len <= длинаМассива(to)));
		НИЗКОУР.копируйПамять(адресОт(from[fofs]), адресОт(to[tofs]), len);
	всё;
кон Copy;

проц Cleanup;
нач
	registry.Enumerate(Finalize);
	Plugins.main.Remove(registry)
кон Cleanup;

нач
	nofBuf := 0;
	nofFreeBuf := 0;

	нов(registry, "Network", "Network interface drivers");
	Modules.InstallTermHandler(Cleanup);
кон Network.

(*
History:
10.10.2003	mvt	Complete redesign and additional implementation of buffer handling and upcall mechanism
17.10.2003	mvt	Changed the way of initialization and finalization (now only Constr/Finalize)
21.10.2003	mvt	Changed SetReceiver to InstallReceiver and RemoveReceiver
15.11.2003	mvt	Changed buffering to work with EXCLUSIVE sections instead of using locking and a semaphore.
16.11.2003	mvt	Added support for checksum calclulation by the device.
25.11.2003	mvt	Added l3ofs and l4ofs to Buffer type.
17.12.2003	mvt	Changed variable "linked" to method "Linked".
02.05.2005	eb	Supports multiple interfaces per device.
*)

(**
How to use the module:

The module is loaded as soon as it is used first. It needn't to be loaded explicitly at startup. It can also be unloaded an reloaded without reboot.

How to use a driver:

Network driver objects in Bluebottle are extensions of the Network.LinkDevice object. All loaded instances of network driver objects are registered in the registry Network.registry. To obtain access to a network driver, use the Get, Await or GetAll methods of this registry.

Example:
	VAR dev: Network.LinkDevice;
	dev := Network.registry.Get(""); (* if no name is specified, first device (or NIL) is returned *)

The Send method of LinkDevice is used to send a packet. The dst parameter specifies the link destination address (e.g., a 6-byte ethernet MAC address for an ethernet device). The type parameter specifies the link-layer protocol type (e.g. 800H when sending IP over ethernet). The source address of the packet is automatically generated by the device, if necessary.
For reasons of reducing buffer copying between network layers, the method allows 3 buffers to be passed:
The l3hdr, l4hdr, data, h3len, h4len, dofs and dlen fields specify 3 buffers:
One buffer for a layer 3 header, one for a layer 4 header and one for the payload of the packet. The buffers don't have to be filled like this. They are simply concatenated to one frame by the device driver. Therefore, each of them is allowed to be empty.

The layer 3 header is stored in: l3hdr[0..h3len-1]
The layer 4 header is stored in: l4hdr[0..h4len-1]
The payload is stored in data[dofs..dofs+dlen-1]

Example:
	CONST
		Type = 05555H;	(* link-layer protocol type *)
	VAR
		dlen: SIGNED32;
		l3hdr: ARRAY HdrLen OF CHAR;
		data: ARRAY MaxDataLen OF CHAR;

	(* - l3hdr[0..HdrLen-1] contains the layer 3 packet header.
		- data[0..dlen-1] contains the packet data.
		- there is no layer 4 header in this example, i.e. an empty buffer is passed (len=0)
	*)
	dev.Send(dev.broadcast, Type, l3hdr, l3hdr, data, HdrLen, 0, 0, dlen); (* send a broadcast packet *)

Packet receiving is driven by the driver object. A receiver interested in a specific type of packet registers itself using the InstallReceiver method. The type parameter specifies the link-layer protocol type, which must be unique. There can only be one receiver installed per type. When a packet arrives, the driver object looks at the protocol type and calls the specific receiver (if any is installed for this type).

Example:
	PROCEDURE Receiver(dev: Network.LinkDevice; type: SIGNED32; buffer: Network.Buffer);
	BEGIN
		ASSERT(type = Type);
		CheckAdr(buffer.src); (* some link layer source address checks *)
		IF ~(ChecksumForThisProtocol IN buffer.calcChecksum) THEN
			VerifyChecksum(buffer);
		END;
		ExamineLayer3Header(buffer.data, buffer.ofs, Layer3HeaderSize);
		ProcessPayload(buffer.data, buffer.ofs+Layer3HeaderSize, buffer.len-Header3LayerSize);

		(* MANDATORY!!
			Buffer must be returned here! - or at higher layers, if the buffer is passed there! *)
		Network.ReturnBuffer(buffer);
	END Receiver;

	dev.InstallReceiver(Type, Receiver); (* install the receiver *)

When passing the buffer to a higher layer (e.g. layer 4), the field "l3ofs" should be set in order to enable the higher layer protocol to access this layer's header (required by ICMP).
The same is valid for the field "l4ofs" when passing the buffer to a higher layer than 4.

The "type" field of a LinkDevice specifies what kind of device it is. Currently, two options (constants) are available:
- TypePointToPoint: dev is a point-to-point link (device), e.g. PPP
- TypeEthernet: dev is an ethernet device

For point-to-point links, the following rules are met:
In constructor, the "local" and "broadcast" parameters are ignored.
If it is not possible to transmit the layer 3 type of packet, the type is set to 0 for the received packet.
If the field "adrSize" is 0, the dst address parameter in Send() is ignored and the src address parameter in the Receiver is not defined.
If "adrSize" is > 0, the dst address passed in Send() is transmitted and presented as src address for the received packet.

The "local" and "broadcast" fields of the object specify the link-level address of the device, and the broadcast address, respectively. If needed, they have to be set during device initialization.
The mtu field specifies the largest allowed packet size in bytes, excluding layer 2 headers and trailers (e.g. 1500 for ethernet).

The "sendCount" and "recvCount" fields show the number of data bytes sent and received by the device since the driver object was loaded.

How to implement a driver:

IMPORTANT:
- Read first "How to use a driver"!
- Read comments of methods you override!

A network driver object is implemented by extending the LinkDevice object. At least the "DoSend" method has to be overridden with a concrete implementation. Normally, you will also override the constructor, the destructor (method "Finalize") and the method "Linked".

CAUTION:
If you override the constructor or destructor:
- At the beginning of the overriding constructor, the overridden constructor has to be called!
- At the end of the overriding destructor, the overridden destructor has to be called!

NOTE:
The device has to be registered and deregistered as a Plugin in Network.registry by the device driver as follows:
Add the device to the registry after it is ready to send/receive data!
Remove the device from the registry before you begin to deinitialize the device!

Normally, the device driver will have to install an interrupt handler for receiving packets. This handler must be installed in Objects. Interrupts registered in Machine are not allowed! (because they are not allowed to call QueueBuffer due to its EXCLUSIVE section)

If you use interrupts:
- do the minimum operations required for receiving and queueing a packet!
- return immediately after having done the operations!

Receiving and queueing packets is done like this:

When you are notified of an incomming network packet (normally by an interrupt), get a new buffer by calling:
buf := Network.GetNewBuffer();

If buf = NIL, the packet has to be discarded because all buffers are currently in use and the maximum amount of buffers (MaxNofBuffers) is exceeded.
If buf # NIL (the normal case), read the packet data into buf.data and set buf.ofs and buf.len accordingly.
The buffer is not initialized in any way.

If the device supports DMA, you could try to get the buffers earlier and pass their physical addesses to the device. With this you can save one packet copying operation!

In addition to the fields "data", "ofs", "len" of "buffer", it is mandatory to set the fields "src" and "calcChecksum" correctly. "src" is the link layer source address and "calcChecksum" is the set of checksums already verified by the device (normally {}).

As soon as the buffer contains the whole packet data, it is passed to the upper layers by calling:
QueueBuffer(buf, type);
Where "type" is the layer 3 type of the packet.

See TestNet.Mod (SendBroadcast, SendTest and Receiver) and IP.Mod (ARPRequest and ARPInput) for an example of packet sending and receiving. See Ethernet3Com90x.Mod for an example of an ethernet driver object and Loopback.Mod for a simple point-to-point link implementation.

*)

модуль DynamicWebpage; (** AUTHOR "Luc Blaeser"; PURPOSE "Basetypes for Dynamic Webpages"; *)
	использует XML, HTTPSupport, Строки8;

	конст
		(** variable names for method invocation over HTTP to handle user events, must be lower case *)
		HTTPVarCommandModule* = "dxpmodule";
		HTTPVarCommandObject* = "dxpobject";
		HTTPVarCommandMethod* = "dxpmethod";
		HTTPVarCommandObjectId* = "dxpoid";
		HTTPVarCommandParamPrefix* = "dxpparam-";

		XMLAttributeObjectIdName* = "id"; (** to specifiy the instance of a statefull active element for a session and a requested file *)
		DXPReservedObjectIdPrefix* = "-dxp-"; (** this prefix is reserved for system generated unique object id for statefull active elements *)

		DynamicWebpageExtension* = "DXP"; (** in uppercase *)
		DefaultWebpage* = "index.dxp";
		ConfigurationSupperSectionName* = "DynamicWebpages";
		ConfigurationSubSectionName* = "ActiveElementModules";
		ProcNameGetDescriptors* = "GetActiveElementDescriptors";

		(** variable names to store the actual state counter, used to detect whether the user has pressed the back or refresh
		 * button in the browser's navigation bar *)
		StateCounterVariable* = "dxpstatecounter";

	тип
		Parameter* = укль на запись
			name*: Строки8.уСтрока;
			value*: Строки8.уСтрока
		кон;

		ParameterList* = окласс
			перем
				parameters*: укль на массив из Parameter;

			проц GetParameterValueByName*(конст name: массив из симв8): Строки8.уСтрока;
			перем par: Parameter;
			нач
				par := GetParameterByName(name);
				если (par # НУЛЬ) то
					возврат par.value
				иначе
					возврат НУЛЬ
				всё
			кон GetParameterValueByName;

			проц  GetParameterByName*(конст name: массив из симв8): Parameter;
			перем i: размерМЗ;
			нач
				если parameters # НУЛЬ то
					нцДля i := 0 до длинаМассива(parameters)-1 делай
						если parameters[i].name^ = name то
							возврат parameters[i]
						всё
					кц
				всё;
				возврат НУЛЬ
			кон GetParameterByName;

			проц GetCount*() : размерМЗ;
			нач
				возврат длинаМассива(parameters)
			кон GetCount;
		кон ParameterList;

		EventHandler* = проц {делегат} (request: HTTPSupport.HTTPRequest; params: ParameterList);

		EventHandlerObject* = окласс
			перем
				methodName*: массив 128 из симв8;
				handler*: EventHandler;

			проц &Init*(конст name: массив из симв8; handlerMeth: EventHandler);
			нач копируйСтрокуДо0(name, methodName); handler := handlerMeth
			кон Init;
		кон EventHandlerObject;

		EventHandlerList* = укль на массив из EventHandlerObject;

		(** abstract base class  for active web elements *)
		ActiveElement* = окласс
			(** abstract main transformation method, the active element occurring as descendants of 'input' are already transformed*)
			проц Transform*(input: XML.Element; request: HTTPSupport.HTTPRequest) : XML.Content;
			нач СТОП(301)
			кон Transform;

			(** transformation method invoked before the active element occurring as descendanrs of 'input' will be transformed *)
			проц PreTransform*(input: XML.Element; request: HTTPSupport.HTTPRequest) : XML.Content;
			нач возврат input
			кон PreTransform;

			проц GetEventHandlers*() : EventHandlerList;
			нач возврат НУЛЬ
			кон GetEventHandlers;
		кон ActiveElement;

		(** abstract base class for active web element which belongs to a session, i.e. is statefull *)
		StateFullActiveElement* = окласс (ActiveElement)
		кон StateFullActiveElement;

		(** abstract base class for singleton active web element which is not assigned to a session, i.e. is stateless *)
		StateLessActiveElement* = окласс (ActiveElement)
		кон StateLessActiveElement;

		(** a factory procedure to create instances of active elements of the module *)
		ActiveElementFactoryProc* = проц (): ActiveElement;

		ActiveElementDescriptor* = окласс
			перем
				elementName*: массив 128 из симв8;
				factory*: ActiveElementFactoryProc;

			проц &Init*(конст name: массив из симв8; factoryProc: ActiveElementFactoryProc);
			нач копируйСтрокуДо0(name, elementName); factory := factoryProc
			кон Init;

		кон ActiveElementDescriptor;

		ActiveElementDescSet* = окласс
			перем
				descriptors*: укль на массив из ActiveElementDescriptor;

			проц &Init*(конст descs: массив из ActiveElementDescriptor);
			перем i: размерМЗ;
			нач
				нов(descriptors, длинаМассива(descs));
				нцДля i := 0 до длинаМассива(descs)-1 делай
					descriptors[i] := descs[i]
				кц
			кон Init;

			проц GetCount*() : размерМЗ;
			нач
				возврат длинаМассива(descriptors^)
			кон GetCount;

			проц GetItem*(i: размерМЗ) : ActiveElementDescriptor;
			нач
				возврат descriptors[i]
			кон GetItem;
		кон ActiveElementDescSet;

		 ActiveElementDescSetFactory* = проц() : ActiveElementDescSet;

		(** additionally there must be a procedure which gives all descriptors for the active elements in the module
			PROCEDURE GetActiveElementDescriptors*(par:ANY) : ANY;
				 no parameter; returns the descriptors of active elements (ActiveElementDescSet)
				 must be thread safe
		 *)

		 перем idCounter: цел32;

		 (** get a new unique object id for statefull active element created while the transformation process *)
		 проц CreateNewObjectId*() : Строки8.уСтрока;
		 перем oid: Строки8.уСтрока; idString: массив 14 из симв8;
		 нач
		 	нов(oid, Строки8.КвоБайтБезЗавершающего0(DXPReservedObjectIdPrefix)+14);
		 	Строки8.ПишиЦел64_вСтроку(idCounter, idString); увел(idCounter);
		 	Строки8.Склей(DXPReservedObjectIdPrefix, idString, oid^);
		 	возврат oid
		 кон CreateNewObjectId;

нач
	idCounter := 0
кон DynamicWebpage.

System.Free DynamicWebpage ~

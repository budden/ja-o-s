модуль BorrowsWheeler;  (** AUTHOR GF; PURPOSE  "Borrows Wheeler Transformation"; *)

конст
	BlockSize* = 8*1024;

тип
	MTF = окласс (* move to front *)
		тип
			Node = укль на запись
				byte: симв8;  next: Node
			кон;
		перем
			alpha: Node;


			проц Initialize;
			перем n: Node;  i: цел32;
			нач
				alpha := НУЛЬ;
				нцДля i := 0 до 255 делай
					нов( n );  n.next :=alpha;  n.byte := симв8ИзКода( 255 - i );  alpha := n
				кц
			кон Initialize;

			проц Encode( перем buf: массив из симв8; len: размерМЗ );
			перем l, m: Node;  i: размерМЗ;  k: цел32; ch: симв8;
			нач
				Initialize;
				нцДля i := 0 до len - 1 делай
					ch := buf[i];
					если alpha.byte = ch то  k := 0
					иначе
						l := alpha;  m := alpha.next;  k := 1;
						нцПока m.byte # ch делай
							увел( k );  l := m;  m := m.next
						кц;
						l.next := m.next;  m.next := alpha;  alpha := m
					всё;
					buf[i] := симв8ИзКода( k )
				кц
			кон Encode;

			проц Decode( перем buf: массив из симв8; len: размерМЗ );
			перем l, m: Node;  i: размерМЗ;  c: цел32;  ch: симв8;
			нач
				Initialize;
				нцДля i := 0 до len - 1 делай
					ch := buf[i];
					если ch # 0X то
						c := кодСимв8( ch );  l := alpha;
						нцПока c > 1 делай  l := l.next;  умень( c )  кц;
						m := l.next;  l.next := m.next;  m.next := alpha;
						alpha := m
					всё;
					buf[i] := alpha.byte;
				кц
			кон Decode;

	кон MTF;

тип
	Encoder* = окласс
		тип
			Index = размерМЗ;
		перем
			mtf: MTF;  length: размерМЗ;
			sbuf: массив 2*BlockSize из симв8;
			rotation: массив BlockSize из Index;

			проц &New*;
			нач
				нов( mtf );
			кон New;


			проц Less( a, b: Index ): булево;
			перем i1, i2: Index;  n, diff: цел32;
			нач
				n := 0;  i1 := rotation[a];  i2 := rotation[b];
				нцДо
					diff := кодСимв8( sbuf[i1]) - кодСимв8( sbuf[i2] );
					увел( i1 );  увел( i2 );  увел( n );
				кцПри (diff # 0) или (n = length);
				возврат diff < 0
			кон Less;

			проц Swap( a, b: Index );
			перем  tmp: Index;
			нач
				tmp := rotation[a];  rotation[a] := rotation[b];  rotation[b] := tmp
			кон Swap;

			проц InsertSort( lo, hi: Index );
			перем x, i, l, m, ip, tmp: Index;
			нач
				x := lo + 1;
				нцПока x <= hi делай
					если Less( x, x - 1 )  то
						(* find insert position ip *)
						ip := x - 1;  l := lo;
						нцПока l < ip делай
							m := (l + ip) DIV 2;
							если Less( x, m ) то  ip := m  иначе  l := m + 1  всё
						кц;
						(* insert rotation[x] at position ip*)
						tmp := rotation[x];  i := x;
						нцДо  rotation[i] := rotation[i - 1];  умень( i )  кцПри i = ip;
						rotation[ip] := tmp;
					всё;
					увел( x )
				кц
			кон InsertSort;

			проц SortR( lo, hi: размерМЗ );
			перем i, j, m, n: размерМЗ;
			нач
				если lo < hi то
					n := hi - lo + 1;
					если n = 2 то
						если Less( hi, lo ) то  Swap( lo, hi )  всё
					аесли n < 16 то
						InsertSort( lo, hi )  (* less expensive string compares! *)
					иначе
						(* QuickSort *)
						i := lo;  j := hi;  m := (lo + hi) DIV 2;
						нцДо
							нцПока Less( i, m ) делай  увел( i )  кц;
							нцПока Less( m, j ) делай  умень( j )  кц;
							если i <= j то
								если m = i то  m := j  аесли  m = j то  m := i  всё;
								Swap( i, j );  увел( i );  умень( j )
							всё
						кцПри i > j;
						SortR( lo, j );  SortR( i, hi )
					всё
				всё
			кон SortR;

			проц EncodeBlock*( перем buf: массив из симв8; len: размерМЗ ): цел32;
			перем  i: размерМЗ;  index: цел32;
			нач
				утв( len <= BlockSize );  length := len;
				нцДля i := 0 до length - 1 делай  sbuf[i] := buf[i];  sbuf[i+length] := buf[i]  кц;
				нцДля i := 0 до length - 1 делай  rotation[i] := цел16( i )  кц;
				SortR( 0, length - 1 );
				(* find index of the original row *)
				index := 0;  нцПока rotation[index] # 0 делай  увел( index )  кц;
				(* replace buf by column L *)
				нцДля i := 0 до length -1 делай  buf[i] := sbuf[rotation[i] + length - 1]  кц;
				mtf.Encode( buf, length );
				возврат index
			кон EncodeBlock;

	кон Encoder;


тип
	Decoder* = окласс
		тип
			Index = размерМЗ;
		перем
			mtf: MTF;
			f, l: массив BlockSize из симв8;
			lc, fc: массив BlockSize из цел16;

			проц &New*;
			нач
				нов( mtf );
			кон New;


			проц -Swap( a, b: Index );
			перем  tmp: симв8;
			нач
				tmp := f[a];  f[a] := f[b];  f[b] := tmp
			кон Swap;


			проц SortF( lo, hi: Index );
			перем i, j, m: Index;
			нач
				если lo < hi то
					если (hi - lo) = 1 то
						если f[hi] < f[lo] то  Swap( lo, hi )  всё;
					иначе
						(* QuickSort *)
						i := lo;  j := hi;  m := (lo + hi) DIV 2;
						нцДо
							нцПока f[i] < f[m] делай  увел( i )  кц;
							нцПока f[m] < f[j] делай  умень( j )  кц;
							если i <= j то
								если m = i то  m := j  аесли m = j то  m := i  всё;
								Swap( i, j );  увел( i );  умень( j )
							всё
						кцПри i > j;
						SortF( lo, j );  SortF( i, hi )
					всё
				всё
			кон SortF;


			проц DecodeBlock*( перем buf: массив из симв8; len, index: размерМЗ );
			перем
				i, j: размерМЗ; n: цел32;  ch: симв8;
				xn: массив 256 из цел16;
			нач
				утв( len <= BlockSize );
				mtf.Decode( buf, len );
				нцДля i := 0 до 255 делай  xn[i] := 0  кц;
				нцДля i := 0 до len - 1 делай
					l[i] := buf[i];  f[i] := buf[i];
					j := кодСимв8( l[i] );  lc[i] := xn[j];  увел( xn[j] )
				кц;
				SortF( 0, len - 1 );
				нцДля i := 0 до 255 делай  xn[i] := 0  кц;
				нцДля i := 0 до len - 1 делай
					j := кодСимв8( f[i] );  fc[i] := xn[j];  увел( xn[j] )
				кц;
				нцДля i := 0 до len - 1 делай
					ch := f[index];  n := fc[index];  buf[i] := ch;  index := 0;
					нцПока (l[index] # ch) или (lc[index] # n) делай  увел( index )  кц
				кц;
			кон DecodeBlock;

	кон Decoder;


кон BorrowsWheeler.

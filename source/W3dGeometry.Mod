модуль W3dGeometry;	(** AUTHOR "TF"; PURPOSE "Geometrical primitives (case study)"; *)

использует
	Vectors := W3dVectors;

тип
	Plane* = запись n* : Vectors.TVector3d; d* : вещ64 кон;
	Ray* = запись p, d : Vectors.TVector3d кон;

	Frustum* = окласс
	перем
		nearP*, farP*, bottomP*, rightP*, topP*, leftP* : Plane;

		проц Make*(p, d, u : Vectors.TVector3d; focus, w, h, near, far : вещ64);
		перем left, z, p0, p1, p2, p3, f0, f1, f2, near0, near1, near2, far0, far1, far2 : Vectors.TVector3d; factor : вещ64;
		нач
			left := Vectors.Cross(d, u);
			z := Vectors.VAdd3(p, Vectors.VScaled3(d, - focus));
			p0 := Vectors.VAdd3(Vectors.VAdd3(p, Vectors.VScaled3(u, -h/2)), Vectors.VScaled3(left, w/2));
			p1 := Vectors.VAdd3(Vectors.VAdd3(p, Vectors.VScaled3(u, -h/2)), Vectors.VScaled3(left, -w/2));
			p2 := Vectors.VAdd3(Vectors.VAdd3(p, Vectors.VScaled3(u, h/2)), Vectors.VScaled3(left, -w/2));
			p3 := Vectors.VAdd3(Vectors.VAdd3(p, Vectors.VScaled3(u, h/2)), Vectors.VScaled3(left, w/2));

			f0 := Vectors.VNormed3(Vectors.VSub3(p0, z));
			f1 := Vectors.VNormed3(Vectors.VSub3(p1, z));
			f2 := Vectors.VNormed3(Vectors.VSub3(p2, z));

			factor := 1 / Vectors.Scalar3(f0, d); (* symmetric situation, all factors are the same *)
			near0 := Vectors.VAdd3(p0, Vectors.VScaled3(f0, near * factor));
			near1 := Vectors.VAdd3(p1, Vectors.VScaled3(f1, near * factor));
			near2 := Vectors.VAdd3(p1, Vectors.VScaled3(f2, near * factor));

			far0 := Vectors.VAdd3(p0, Vectors.VScaled3(f0, far * factor));
			far1 := Vectors.VAdd3(p1, Vectors.VScaled3(f1, far * factor));
			far2 := Vectors.VAdd3(p1, Vectors.VScaled3(f2, far * factor));

			nearP := MakePlane(near0, near2, near1);
			farP := MakePlane(far0, far1, far2);
			leftP := MakePlane(z, p0, p3);
			bottomP := MakePlane(z, p1, p0);
			rightP := MakePlane(z, p2, p1);
			topP := MakePlane(z, p3, p2);
		кон Make;

		проц IsBSOutsideBehind*(center : Vectors.TVector3d; r: вещ64): булево;
		нач
			возврат  (Distance(nearP, center) > r) или
							(Distance(leftP, center) > r) или
							(Distance(rightP, center) > r) или
							(Distance(bottomP, center) > r) или
							(Distance(topP, center) > r)
		кон IsBSOutsideBehind;
	кон Frustum;

проц MakePlane*(p0, p1, p2 : Vectors.TVector3d) : Plane;
перем result : Plane;
нач
	result.n := Vectors.VNormed3(Vectors.Cross(Vectors.VSub3(p1, p0), Vectors.VSub3(p2, p0)));
	result.d := - Vectors.Scalar3(result.n, p0);
	возврат result
кон MakePlane;

проц Distance*(e: Plane; p : Vectors.TVector3d) : вещ64;
нач
	возврат Vectors.Scalar3(e.n, p) + e.d
кон Distance;

(** intersection between g and e, g is treated as line *)
проц Intersection*(перем g : Ray; перем e : Plane; перем p : Vectors.TVector3d) : булево;
перем m, d: вещ64;
нач
  d := Vectors.Scalar3(e.n, g.d);
  если d = 0 то возврат ложь всё;
  m := -(Vectors.Scalar3(e.n, g.p)+e.d) / d;
  p := Vectors.VAdd3(g.p, Vectors.VScaled3(g.d, m));
  возврат истина
кон Intersection;

кон W3dGeometry.

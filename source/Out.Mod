модуль Out; (** AUTHOR "FOF"; PURPOSE "Simple console output, for educational purposes"; *)

(* threadsafe as far as commands don't share the same context *)

использует Commands, Потоки;

проц GetWriter*(): Потоки.Писарь;
нач
	возврат Commands.GetContext().out;
кон GetWriter;

проц String*(конст s: массив из симв8);
нач
	Commands.GetContext().out.пСтроку8(s);
кон String;

проц Char*(c: симв8);
нач
	Commands.GetContext().out.пСимв8(c);
кон Char;

проц Ln*();
нач
	Commands.GetContext().out.пВК_ПС();
кон Ln;

проц Set*(s: мнвоНаБитахМЗ);
нач
	Commands.GetContext().out.пМнвоНаБитахМЗ(s);
кон Set;

проц Int*(i: цел64; n = 1: целМЗ);
нач
	Commands.GetContext().out.пЦел64(i,n);
кон Int;

проц Hex*(i: цел64; n = -16: целМЗ);
нач
	Commands.GetContext().out.п16ричное(i,n);
кон Hex;

проц Address*(a: адресВПамяти);
нач
	Commands.GetContext().out.пАдресВПамяти(a);
кон Address;

проц Float*(x: вещ64; n = 4, f= 3, d=0: целМЗ);
нач
	Commands.GetContext().out.пВещ64_ФФТ(x,n,f,d);
кон Float;

проц Update*;
нач
	Commands.GetContext().out.ПротолкниБуферВПоток();
кон Update;



кон Out.

System.Free Out ~
Out.Hello



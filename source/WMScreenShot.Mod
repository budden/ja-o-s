модуль WMScreenShot;	(** AUTHOR "TF"; PURPOSE "Screenshot utility"; *)

использует
	Commands, Plugins, Raster, WMGraphics, WMRectangles,
	WM := WMWindowManager;

тип
	View = окласс (WM.ViewPort)
		перем
			backbuffer : WMGraphics.Image;
			deviceRect : WMRectangles.Rectangle;

			c : WMGraphics.BufferCanvas;
			state : WMGraphics.CanvasState;

			fx, fy, inffx, inffy, factor, intfactor : вещ32;

		проц &New*(manager : WM.WindowManager; w, h : цел32);
		нач
			сам.manager := manager;
			нов(backbuffer);
			Raster.Create(backbuffer, w, h, Raster.BGR565);
			нов(c, backbuffer);
			c.SetFont(WMGraphics.GetDefaultFont());
			c.SaveState(state);
			deviceRect := WMRectangles.MakeRect(0, 0, w, h);
			factor := 1; intfactor := 1;
			fx := factor; fy := factor; inffx := 1 ; inffy := inffx;
			SetRange(0, 0, w, h, ложь);
			manager.AddView(сам); manager.RefreshView(сам);
		кон New;
		(** r in wm coordinates *)
		проц {перекрыта}Update*(r : WMRectangles.Rectangle; top : WM.Window);
		нач
			Draw(WMRectangles.ResizeRect(r, 1), top.prev) (* assuming the src-domain is only 1 *)
		кон Update;

		проц {перекрыта}Refresh*(top : WM.Window);
		нач
			Update(WMRectangles.MakeRect(округлиВниз(range.l)-1, округлиВниз(range.t)-1, округлиВниз(range.r) + 1, округлиВниз(range.b) + 1), top)
		кон Refresh;

		проц {перекрыта}SetRange*(x, y, w, h : вещ32; showTransition : булево);

			проц Set(x, y, w, h : вещ32);
			перем tf : вещ32;
			нач
				range.l := x;
				range.t := y;
				factor := (backbuffer.width) / w;
				tf := (backbuffer.height) / h;
				если factor > tf то factor := tf всё;
				fx := factor; fy := factor; inffx := 1 / factor; inffy := inffx;
				range.r := x + backbuffer.width * inffx;
				range.b := y + backbuffer.height * inffy;
				intfactor := factor;
				manager.RefreshView(сам);
			кон Set;

		нач
			если w = 0 то w := 0.001 всё;
			если h = 0 то h := 0.001 всё;
			Set(x, y, w, h)
		кон SetRange;

		(* in wm coordinates *)
		проц Draw(r : WMRectangles.Rectangle; top : WM.Window);
		перем cur : WM.Window;
			wr, nr : WMRectangles.Rectangle;

			проц InternalDraw(r : WMRectangles.Rectangle; cur : WM.Window);
			перем nr, cb, dsr : WMRectangles.Rectangle;
			нач
				если cur.useAlpha и (cur.prev # НУЛЬ)  то Draw(r, cur.prev)
				иначе
					нцПока cur # НУЛЬ делай (* draw r in wm coordinates in all the windows from cur to top *)
						nr := r; cb := cur.bounds; WMRectangles.ClipRect(nr, cb);
						dsr.l := округлиВниз((nr.l - range.l) * fx) ; dsr.t := округлиВниз((nr.t - range.t) * fy);
						dsr.r := округлиВниз((nr.r - range.l) * fx + 0.5); dsr.b := округлиВниз((nr.b - range.t) * fy + 0.5);
						если (~WMRectangles.RectEmpty(dsr)) и (WMRectangles.Intersect(dsr, deviceRect)) то
							c.SetClipRect(dsr);  (* Set clip rect to dsr, clipped at current window *)
							c.ClipRectAsNewLimits(округлиВниз((cur.bounds.l - range.l) * fx), округлиВниз((cur.bounds.t - range.t) * fy));
							(* range can not be factored out because of rounding *)
							cur.Draw(c, округлиВниз((cb.r - range.l)* fx) - округлиВниз((cb.l - range.l) * fx),
													округлиВниз((cb.b - range.t) * fy) - округлиВниз((cb.t - range.t) * fy), 1);
							c.RestoreState(state);
						всё;
						cur := cur.next
					кц;
				всё
			кон InternalDraw;

		нач
			cur := top;
			если (cur # НУЛЬ) и (~WMRectangles.RectEmpty(r)) то
				wr := cur.bounds;
				если ~WMRectangles.IsContained(wr, r) то
					если WMRectangles.Intersect(r, wr) то
						(* r contains wr calculate r -  wr and recursively call for resulting rectangles*)
						(* calculate top rectangle *)
						если wr.t > r.t то WMRectangles.SetRect(nr, r.l, r.t, r.r, wr.t); Draw(nr, cur.prev) всё;
						(* calculate bottom rectangle *)
						если wr.b < r.b то WMRectangles.SetRect(nr, r.l, wr.b, r.r, r.b); Draw(nr, cur.prev) всё;
						(* calculate left rectangle *)
						если wr.l > r.l то WMRectangles.SetRect(nr, r.l, матМаксимум(r.t, wr.t), wr.l, матМинимум(r.b, wr.b)); Draw(nr, cur.prev) всё;
						(* calculate left rectangle *)
						если wr.r < r.r то WMRectangles.SetRect(nr, wr.r, матМаксимум(r.t, wr.t), r.r, матМинимум(r.b, wr.b)); Draw(nr, cur.prev) всё;
						(* calculate overlapping *)
						nr := r; WMRectangles.ClipRect(nr, wr);
						если ~WMRectangles.RectEmpty(nr) то InternalDraw(nr, cur) всё
					иначе Draw(r, cur.prev)
					всё
				иначе InternalDraw(r, cur)
				всё
			всё
		кон Draw;

		проц Close;
		нач
			 manager.RemoveView(сам)
		кон Close;

	кон View;

(** Parameters : filename [viewname] [width] [height] *)
проц SnapShotView*(context : Commands.Context);
перем manager : WM.WindowManager;
	viewportName, fn : массив 100 из симв8;
	viewport : WM.ViewPort;
	sv : View;
	p : Plugins.Plugin;
	w, h: цел32; res: целМЗ;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fn);
	если ~((context.arg.ПодглядиСимв8() >= '0') и (context.arg.ПодглядиСимв8() <= '9')) то
		context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(viewportName);
	всё;

	manager := WM.GetDefaultManager();
	p := manager.viewRegistry.Get(viewportName);
	если p # НУЛЬ то viewport := p(WM.ViewPort) иначе viewport := WM.GetDefaultView() всё;

	w := матМаксимум(округлиВниз(viewport.range.r - viewport.range.l), 1);
	h := матМаксимум(округлиВниз(viewport.range.b - viewport.range.t), 1);
	context.arg.ПропустиБелоеПоле;
	если (context.arg.ПодглядиСимв8() >= '0') и (context.arg.ПодглядиСимв8() <= '9') то context.arg.чЦел32(w, ложь) всё;
	context.arg.ПропустиБелоеПоле;
	если (context.arg.ПодглядиСимв8() >= '0') и (context.arg.ПодглядиСимв8() <= '9') то context.arg.чЦел32(h, ложь) всё;

	context.out.пСтроку8("Screenshot : ");
	нов(sv, manager, w, h);
	sv.SetRange(viewport.range.l, viewport.range.t, viewport.range.r, viewport.range.b, ложь);

	WMGraphics.StoreImage(sv.backbuffer, fn, res);
	если res = 0 то
		context.out.пСтроку8(" Click"); context.out.пВК_ПС; context.out.пСтроку8("-->  WMPicView.Open ");
		context.out.пСтроку8(fn); context.out.пСтроку8(" ~"); context.out.пВК_ПС;
	иначе
		context.error.пСтроку8("Failed not written : "); context.error.пСтроку8(fn); context.error.пВК_ПС;
	всё;
	sv.Close;
кон SnapShotView;

(** Parameters : filename width height [(left top)|(left top width height)]*)
проц SnapShotRange*(context : Commands.Context);
перем manager : WM.WindowManager;
	fn : массив 100 из симв8;
	sv : View;
	w, h, rl, rt, rw, rh: цел32; res: целМЗ;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fn);
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(w, ложь);
	если w <1 то w := 1 всё; если w > 10000 то w := 10000 всё;

	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(h, ложь);
	если h <1 то h := 1 всё; если h > 10000 то h := 10000 всё;

	context.arg.ПропустиБелоеПоле;
	если (context.arg.ПодглядиСимв8() >= '0') и (context.arg.ПодглядиСимв8() <= '9') или (context.arg.ПодглядиСимв8() = '-')то
		context.arg.ПропустиБелоеПоле; context.arg.чЦел32(rl, ложь);
		context.arg.ПропустиБелоеПоле; context.arg.чЦел32(rt, ложь);
	 всё;

	rw := w; rh := h;
	context.arg.ПропустиБелоеПоле;
	если (context.arg.ПодглядиСимв8() >= '0') и (context.arg.ПодглядиСимв8() <= '9') то
		context.arg.ПропустиБелоеПоле; context.arg.чЦел32(rw, ложь);
		context.arg.ПропустиБелоеПоле; context.arg.чЦел32(rh, ложь);
	 всё;

	 если rw <= 0 то rw := 1 всё;
	 если rh <= 0 то rh := 1 всё;

	context.out.пСтроку8("Screenshot : ");
	manager := WM.GetDefaultManager();
	нов(sv, manager, w, h);
	context.out.пЦел64(rl, 0); context.out.пСтроку8(",  "); context.out.пЦел64(rt, 0);  context.out.пСтроку8(", ");
	context.out.пЦел64(rl + rw, 0); context.out.пСтроку8(", "); context.out.пЦел64(rt + rh, 0);
	sv.SetRange(rl, rt, rw, rh, ложь);
	context.out.пСтроку8(" Click"); context.out.пВК_ПС;
	WMGraphics.StoreImage(sv.backbuffer, fn, res);
	если res = 0 то
		context.out.пСтроку8("-->  WMPicView.Open "); context.out.пСтроку8(fn); context.out.пСтроку8(" ~"); context.out.пВК_ПС;
	иначе
		context.error.пСтроку8("Failed not written : "); context.error.пСтроку8(fn); context.error.пВК_ПС;
	всё;
	sv.Close;
кон SnapShotRange;

кон WMScreenShot.

System.Free WMScreenShot ~

Take a snap shot of the default view store it in test.bmp
WMScreenShot.SnapShotView test.bmp ~

Take a snap shot of the default view store it in test.bmp scaled to 100 by 100 pixels
WMScreenShot.SnapShotView test.bmp 100 100~


Take a snap shot of the View#0 store it in test.bmp
WMScreenShot.SnapShotView test.bmp View#0 ~

Take a snap shot of the View#0 store it in test.bmp scaled to 200 by 200 pixels
WMScreenShot.SnapShotView test.bmp View#0 200 200 ~

To a image of 300 by 300 pixels store a snapshot of range -100 -100 to 200 200 in the display space
WMScreenShot.SnapShotRange test.bmp 300 300 -100 -100 300 300 ~

WMPicView.Open test.bmp ~

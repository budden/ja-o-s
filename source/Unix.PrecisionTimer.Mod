(**
	AUTHOR: Alexey Morozov
	PURPOSE: high precision timer support for Unix platforms
*)
модуль PrecisionTimer;

использует
	Unix, ЛогЯдра;

тип
	Counter* = цел64; (** Timer counter value type *)

конст
	(*
		The IDs of the various system clocks (for POSIX.1b interval timers):
	*)
	CLOCK_REALTIME = 0;
	CLOCK_MONOTONIC = 1;
	CLOCK_PROCESS_CPUTIME_ID = 2;
	CLOCK_THREAD_CPUTIME_ID = 3;
	CLOCK_MONOTONIC_RAW = 4;
	CLOCK_REALTIME_COARSE = 5;
	CLOCK_MONOTONIC_COARSE = 6;
	CLOCK_BOOTTIME = 7;
	CLOCK_REALTIME_ALARM = 8;
	CLOCK_BOOTTIME_ALARM = 9;

тип
	Timespec = запись
		sec: длинноеЦелМЗ;
		nsec: цел32;
	кон;

перем
	clock_gettime: проц{C}(clk_id: цел32; конст tp: Timespec): цел32;
	clock_getres: проц{C}(clk_id: цел32; конст res: Timespec): цел32;

	(*! It is preferable to use CLOCK_MONOTONIC_RAW (not influenced by system time adjustments),
		but for some reason CLOCK_MONOTONIC is ~3 times faster
	*)
	clockType := CLOCK_MONOTONIC: цел32; (* timer clock type *)
	frequency: Counter; (* timer frequency in Hz *)

	(**
		Query timer counter in ticks
	*)
	проц GetCounter*(): Counter;
	перем t: Timespec;
	нач
		если clock_gettime(clockType, t) = 0 то
			возврат цел64(t.sec)*1000000000 + цел64(t.nsec);
		иначе
			возврат 0;
		всё;
	кон GetCounter;

	(**
		Query timer tick frequency in Hz
	*)
	проц GetFrequency*(): Counter;
	перем t: Timespec;
	нач
		если clock_getres(clockType, t) = 0 то
			(*! a workaround for not known actual clock frequency *)
			возврат округлиВниз64(1.0D9 / t.nsec + 0.5D0);
		иначе
			возврат 0;
		всё;
	кон GetFrequency;

	проц InitMod;
	перем frequency: Counter;
	нач
		Unix.Dlsym(Unix.libc, "clock_gettime", адресОт(clock_gettime));
		утв(clock_gettime # НУЛЬ);
		Unix.Dlsym(Unix.libc, "clock_getres", адресОт(clock_getres));
		утв(clock_getres # НУЛЬ);

		ЛогЯдра.пСтроку8("PrecisionTimer: timer tick frequency is "); ЛогЯдра.пЦел64(GetFrequency(),0); ЛогЯдра.пСтроку8(" Hz"); ЛогЯдра.пВК_ПС;
	кон InitMod;

нач
	InitMod;
кон PrecisionTimer.

System.Free PrecisionTimer ~

модуль srM3Space;
использует srBase,  srE,  Out := ЛогЯдра;


тип SREAL=srBase.SREAL;
тип PT = srBase.PT;
тип COLOR = srBase.COLOR;
тип Ray = srBase.Ray;
тип Voxel = srBase.Voxel;
тип NCUBE=запись
	filled: булево;
	mirror:булево;
	reflectivity:вещ32;
	normal: PT;
	color:COLOR;
кон;

тип cell* = окласс(Voxel);

перем
	blox: AR3;
	nblox:  NR3;
	threeblox: BR3;
	airred, airgreen, airblue, airblack: SREAL;

проц & init*;
нач
	SetColor(0,0,0,0);
	complex:=истина;
	passable:=истина;
кон init;

проц SetColor* (R, G, B, BL: SREAL);
нач
	airred := R/3;
	airgreen := G/3;
	airblue := B/3;
	airblack :=  BL/3;
кон SetColor;

проц bounds* (i, j, k: цел32; перем out: булево);
нач
	если (i < 0) или (i > 2) или (j < 0) или (j > 2) или (k < 0) или (k > 2) то
		out := истина
	иначе
		out := ложь
	всё
кон bounds;

проц fill*(v: Voxel);
перем
	i,j,k: цел16;
нач
	нцДля i := 0 до 2 делай нцДля j := 0 до 2 делай нцДля k:= 0 до 2 делай
		blox[i,j,k] := v
	кц кц кц
кон fill;

проц erase*;
перем
	i,j,k: цел16;
нач
	нцДля i := 0 до 2 делай нцДля j := 0 до 2 делай нцДля k:= 0 до 2 делай
		blox[i,j,k] := НУЛЬ; nblox[i,j,k].filled := ложь; threeblox[i,j,k] := НУЛЬ;
	кц кц кц
кон erase;


проц fillwithprobability*(v: Voxel; p: SREAL);
перем
	i,j,k: цел16;
нач
	нцДля i := 0 до 2 делай нцДля j := 0 до 2 делай нцДля k:= 0 до 2 делай
		если srBase.rand.Uniform()<p то blox[i,j,k] := v всё
	кц кц кц
кон fillwithprobability;

проц fillchequer*(v,w: Voxel);
перем
	i,j,k: цел16;
нач
	нцДля i := 0 до 2 делай нцДля j := 0 до 2 делай нцДля k:= 0 до 2 делай
		если нечётноеЛи¿(i+j+k) то blox[i,j,k] := v иначе blox[i,j,k] := w всё
	кц кц кц
кон fillchequer;

проц ncolor(перем ray: Ray; cube:NCUBE);
перем
	dot: SREAL;

проц reflect(перем m,n:PT);
перем
	dot: SREAL;
нач
	dot := m.x*n.x+m.y*n.y+m.z*n.z;
	n.x:= 2*n.x*dot; n.y := 2*n.y*dot; n.z := 2*n.z*dot;
	m.x := m.x-n.x; m.y := m.y-n.y; m.z := m.z-n.z;
кон reflect;

нач
	если cube.mirror то
		reflect(ray.dxyz,cube.normal);
		ray.a:=ray.a-0.1;
		ray.changed:=истина
	иначе
		dot := матМодуль(cube.normal.x*ray.dxyz.x + cube.normal.y*ray.dxyz.y+ cube.normal.z*ray.dxyz.z);
		ray.r := ray.r + cube.color.red * ray.ra*dot;
		ray.g := ray.g + cube.color.green * ray.ga*dot;
		ray.b := ray.b + cube.color.blue * ray.ba*dot;
		ray.ra:=0; ray.ga:=0; ray.ba:=0;
		ray.a := 0;
	всё
кон ncolor;

проц {перекрыта}Shade*(перем ray: Ray);
перем
	oldxyz: srBase.PT;
	ijk: srBase.IPT;
	drx, dry, drz, dr,rr,gr,br,bl: SREAL;
	di, dj, dk: цел16;
	out: булево;
	v: Voxel;
нач
	если ray.recursion>16 то
		ray.a :=0
	иначе
		oldxyz := ray.xyz;
		ray.scale := ray.scale/3;
		ray.xyz.x := ray.lxyz.x * 3  - ray.ddxyz.x;
		ray.xyz.y := ray.lxyz.y * 3  - ray.ddxyz.y;
		ray.xyz.z := ray.lxyz.z * 3  - ray.ddxyz.z;
		srE.E(ray.xyz,ijk);
		bounds(ijk.i,ijk.j,ijk.k, out);
		если ~out и (ray.a > 1/10) то
			v := blox[ijk.i,ijk.j,ijk.k];
			если v#НУЛЬ то
				ray.lxyz.x := матМодуль(ray.xyz.x - ijk.i);
				ray.lxyz.y := матМодуль(ray.xyz.y - ijk.j);
				ray.lxyz.z := матМодуль(ray.xyz.z - ijk.k);
				v.Shade(ray);
			иначе
				v := threeblox[ijk.i,ijk.j,ijk.k];
				если v#НУЛЬ то
				 	ray.lxyz.x := матМодуль(ray.xyz.x - ijk.i);
					ray.lxyz.y := матМодуль(ray.xyz.y - ijk.j);
					ray.lxyz.z := матМодуль(ray.xyz.z - ijk.k);
					v.Shade(ray);
				аесли nblox[ijk.i,ijk.j,ijk.k].filled то
					ncolor(ray,nblox[ijk.i,ijk.j,ijk.k])
				всё
			всё
		всё;
		нцДо
			ray.changed := ложь;
			если ray.dxyz.x < 0 то di := - 1  иначе di := 1 всё;
			если ray.dxyz.y < 0 то dj := - 1  иначе dj := 1 всё;
			если ray.dxyz.z< 0 то dk := - 1  иначе dk := 1 всё;
			нцДо
				если di > 0 то
					drx := ( (ijk.i + 1) - ray.xyz.x) / ray.dxyz.x
				иначе
					drx :=  (ijk.i -  ray.xyz.x) / ray.dxyz.x
				всё;
				если dj > 0 то
					dry := ( (ijk.j + 1) - ray.xyz.y) / ray.dxyz.y
				иначе
					dry :=  (ijk.j - ray.xyz.y) / ray.dxyz.y
				всё;
				если dk > 0 то
					drz := ( (ijk.k + 1) - ray.xyz.z) / ray.dxyz.z
				иначе
					drz :=  (ijk.k - ray.xyz.z) / ray.dxyz.z
				всё;
				если (drx < dry) то
					если (drx < drz ) то
						dr := drx;
						увел(ijk.i, di);
						если di > 0 то
							ray.face := 1; ray.normal:= srBase.Face[0]
						иначе
							ray.face := 4; ray.normal:= srBase.Face[3]
						всё;
						ray.xyz.x := ray.xyz.x + drx * ray.dxyz.x; ray.xyz.y := ray.xyz.y + drx * ray.dxyz.y; ray.xyz.z  := ray.xyz.z + drx * ray.dxyz.z
					иначе
						dr := drz;
						увел(ijk.k, dk);
						если dk > 0 то
							ray.face := 3; ray.normal:= srBase.Face[2]
						иначе
							ray.face := 6; ray.normal:= srBase.Face[5]
						всё;
						ray.xyz.x := ray.xyz.x + drz * ray.dxyz.x; ray.xyz.y := ray.xyz.y + drz * ray.dxyz.y; ray.xyz.z  := ray.xyz.z + drz * ray.dxyz.z
					всё
				аесли (dry < drz) то
					dr := dry;
					увел(ijk.j, dj);
					если dj > 0 то
						ray.face := 2; ray.normal:= srBase.Face[1]
					иначе
						ray.face := 5; ray.normal:= srBase.Face[4]
					всё;
					ray.xyz.x := ray.xyz.x + dry * ray.dxyz.x; ray.xyz.y := ray.xyz.y + dry * ray.dxyz.y; ray.xyz.z  := ray.xyz.z+ dry * ray.dxyz.z
				иначе
					dr := drz;
					увел(ijk.k, dk);
					если dk > 0 то
						ray.face := 3; ray.normal:= srBase.Face[2]
					иначе
						ray.face := 6; ray.normal:= srBase.Face[5]
					всё;
					ray.xyz.x := ray.xyz.x + drz * ray.dxyz.x; ray.xyz.y := ray.xyz.y + drz * ray.dxyz.y; ray.xyz.z  := ray.xyz.z + drz * ray.dxyz.z
				всё;
				rr := airred*dr; gr := airgreen*dr; br := airblue*dr; bl:=airblack*dr;
				ray.r := ray.r + rr*ray.a;
				ray.g:= ray.g + gr*ray.a;
				ray.b := ray.b + br*ray.a;
				ray.ra := ray.ra -rr - bl;
				ray.ga := ray.ga -gr -bl;
				ray.ba := ray.ba -br -bl;
				srBase.clamp3(ray.ra,ray.ga,ray.ba);
				ray.a := (ray.ra+ray.ga+ray.ba)/3;
				bounds(ijk.i,ijk.j,ijk.k, out);
				если ~out и (ray.a > 1/10) то
					v := blox[ijk.i,ijk.j,ijk.k];
					если v#НУЛЬ то
						ray.lxyz.x := матМодуль(ray.xyz.x - ijk.i);
						ray.lxyz.y := матМодуль(ray.xyz.y - ijk.j);
						ray.lxyz.z := матМодуль(ray.xyz.z - ijk.k);
						v.Shade(ray);
					иначе
						v := threeblox[ijk.i,ijk.j,ijk.k];
						 если v#НУЛЬ то
							ray.lxyz.x := матМодуль(ray.xyz.x - ijk.i);
							ray.lxyz.y := матМодуль(ray.xyz.y - ijk.j);
							ray.lxyz.z := матМодуль(ray.xyz.z - ijk.k);
							v.Shade(ray);
						аесли nblox[ijk.i,ijk.j,ijk.k].filled то
							ncolor(ray,nblox[ijk.i,ijk.j,ijk.k])
						всё
					всё
				всё;
			кцПри   (ray.a < 0.1) или out или ray.changed;
		кцПри   (ray.a < 0.1) или out;
		ray.scale := ray.scale*3;
		ray.xyz := oldxyz;
	всё
кон Shade;

проц {перекрыта}probe*(x,y,z: SREAL):Voxel;
перем
	X,Y,Z: SREAL;
	i,j,k: цел32;
нач
	srBase.clamp3(x,y,z);
	X := x*3; Y := y*3; Z := z*3;
	i := округлиВниз(X);
	j := округлиВниз(Y);
	k := округлиВниз(Z);
	если blox[i,j,k]#НУЛЬ то возврат(blox[i,j,k].probe(X-i, Y-j, Z-k)) всё;
	если threeblox[i,j,k]#НУЛЬ то возврат(threeblox[i,j,k].probe(X-i, Y-j, Z-k)) всё;
	если nblox[i,j,k].filled то возврат(сам) всё;
	возврат(НУЛЬ);
кон probe;

проц {перекрыта}probeShade*(перем ray: Ray; перем dx,dy,dz: SREAL);
перем
	ijk: srBase.IPT;
	out: булево;
	v: Voxel;
нач
	ray.xyz.x := ray.lxyz.x * 3;
	ray.xyz.y := ray.lxyz.y * 3;
	ray.xyz.z := ray.lxyz.z * 3;
	srE.E(ray.xyz,ijk);
	bounds(ijk.i,ijk.j,ijk.k, out);
	если ~out то
		v := blox[ijk.i,ijk.j,ijk.k];
		если v#НУЛЬ то
			ray.lxyz.x := ray.xyz.x;
			ray.lxyz.y := ray.xyz.y;
			ray.lxyz.z := ray.xyz.z;
			v.probeShade(ray,dx,dy,dz);
		всё
	всё
кон probeShade;

проц {перекрыта}deathray*(перем ray: Ray);
перем
	oldxyz: srBase.PT;
	ijk: srBase.IPT;
	drx, dry, drz: SREAL;
	di, dj, dk: цел16;
	out: булево;
	v: Voxel;
	killed: булево;
нач
	Out.пСтроку8('..looking for something to kill..');
	oldxyz := ray.xyz;
	ray.scale := ray.scale/3;
	ray.xyz.x := ray.lxyz.x * 3  - ray.dxyz.x / 1000000 ;
	ray.xyz.y := ray.lxyz.y * 3  - ray.dxyz.y / 1000000 ;
	ray.xyz.z := ray.lxyz.z * 3  - ray.dxyz.z / 1000000 ;
	srE.E(ray.xyz,ijk);
	bounds(ijk.i,ijk.j,ijk.k, out);
	если ~out то
		v := blox[ijk.i,ijk.j,ijk.k];
		если  v # НУЛЬ то
			Out.пСтроку8('..inside something..');
			если v.complex то
				ray.lxyz.x := матМодуль(ray.xyz.x - ijk.i);
				ray.lxyz.y := матМодуль(ray.xyz.y - ijk.j);
				ray.lxyz.z := матМодуль(ray.xyz.z - ijk.k);
				Out.пСтроку8('..something complex..');
				v.deathray(ray);
				если ray.changed то killed := истина всё;
			всё
		всё
	всё;
	если ~killed то нцДо
		если ray.dxyz.x < 0 то di := - 1  иначе di := 1 всё;
		если ray.dxyz.y < 0 то dj := - 1  иначе dj := 1 всё;
		если ray.dxyz.z< 0 то dk := - 1  иначе dk := 1 всё;
		нцДо
			если di > 0 то
				drx := ( (ijk.i + 1) - ray.xyz.x) / ray.dxyz.x
			иначе
				drx :=  (ijk.i -  ray.xyz.x) / ray.dxyz.x
			всё;
			если dj > 0 то
				dry := ( (ijk.j + 1) - ray.xyz.y) / ray.dxyz.y
			иначе
				dry :=  (ijk.j - ray.xyz.y) / ray.dxyz.y
			всё;
			если dk > 0 то
				drz := ( (ijk.k + 1) - ray.xyz.z) / ray.dxyz.z
			иначе
				drz :=  (ijk.k - ray.xyz.z) / ray.dxyz.z
			всё;
			если (drx < dry) то
				если (drx < drz ) то
					увел(ijk.i, di);
					если di > 0 то
						ray.face := 1; ray.normal:= srBase.Face[0]
					иначе
						ray.face := 4; ray.normal:= srBase.Face[3]
					всё;
					ray.xyz.x := ray.xyz.x + drx * ray.dxyz.x; ray.xyz.y := ray.xyz.y + drx * ray.dxyz.y; ray.xyz.z  := ray.xyz.z + drx * ray.dxyz.z
				иначе
					увел(ijk.k, dk);
					если dk > 0 то
						ray.face := 3; ray.normal:= srBase.Face[2]
					иначе
						ray.face := 6; ray.normal:= srBase.Face[5]
					всё;
					ray.xyz.x := ray.xyz.x + drz * ray.dxyz.x; ray.xyz.y := ray.xyz.y + drz * ray.dxyz.y; ray.xyz.z  := ray.xyz.z + drz * ray.dxyz.z
				всё
			аесли (dry < drz) то
				увел(ijk.j, dj);
				если dj > 0 то
					ray.face := 2; ray.normal:= srBase.Face[1]
				иначе
					ray.face := 5; ray.normal:= srBase.Face[4]
				всё;
				ray.xyz.x := ray.xyz.x + dry * ray.dxyz.x; ray.xyz.y := ray.xyz.y + dry * ray.dxyz.y; ray.xyz.z  := ray.xyz.z+ dry * ray.dxyz.z
			иначе
				увел(ijk.k, dk);
				если dk > 0 то
					ray.face := 3; ray.normal:= srBase.Face[2]
				иначе
					ray.face := 6; ray.normal:= srBase.Face[5]
				всё;
				ray.xyz.x := ray.xyz.x + drz * ray.dxyz.x; ray.xyz.y := ray.xyz.y + drz * ray.dxyz.y; ray.xyz.z  := ray.xyz.z + drz * ray.dxyz.z
			всё;
			bounds(ijk.i,ijk.j,ijk.k, out);
			если ~out то
				v := blox[ijk.i,ijk.j,ijk.k];
				Out.пСтроку8('nil ');
				если v # НУЛЬ то
					если v.complex то
						ray.lxyz.x := матМодуль(ray.xyz.x - ijk.i);
						ray.lxyz.y := матМодуль(ray.xyz.y - ijk.j);
						ray.lxyz.z := матМодуль(ray.xyz.z - ijk.k);
						Out.пСтроку8('complex ');
						v.deathray(ray);
						если ray.changed то killed := истина всё;
					иначе
						Out.пСтроку8('simple: killing ');
						blox[ijk.i,ijk.j,ijk.k] := НУЛЬ;
						killed := истина; ray.changed := истина;
					всё;
				всё
			всё;
		кцПри  killed или out;
	кцПри  killed или out;
	всё;
	если killed то ray.changed := истина всё;
	ray.scale := ray.scale*3;
	ray.xyz := oldxyz;
	Out.пВК_ПС;
кон deathray;

проц {перекрыта}stroke*(p:PT; level: цел32; normal:PT; color: COLOR; mirror: булево);
перем
	i,j,k: цел32;
нач
	если level>=1 то
		(* top mcell is 1x1x1 by definition *) (*root only*)
		srBase.clamPT(p);
		pdiv(p,3);
		i := округлиВниз(p.x); j := округлиВниз(p.y); k := округлиВниз(p.z);
		если level=1 то
			(* we're here. *)
			nblox[i,j,k].normal:=normal;
			nblox[i,j,k].color:=color;
			nblox[i,j,k].filled:=истина;
			если mirror то nblox[i,j,k].mirror:=истина всё;
		иначе
			если threeblox[i,j,k] = НУЛЬ то
				нов(threeblox[i,j,k]);
			всё;
			p.x:=p.x-i; p.y:=p.y-j; p.z:=p.z-k;
			threeblox[i,j,k].stroke(p, level-1,normal,color,mirror);
		всё
	всё
кон stroke;


проц {перекрыта}strokevoxel*(p:PT; level: цел32; voxel:Voxel);
перем
	i,j,k: цел32;
нач
	если level>=1 то
		(* top mcell is 1x1x1 by definition *) (*root only*)
		srBase.clamPT(p);
		pdiv(p,2.9999);
		i := округлиВниз(p.x); j := округлиВниз(p.y); k := округлиВниз(p.z);
		p.x:=p.x-i; p.y:=p.y-j; p.z:=p.z-k;
		если level=1 то
			(* we're here. *)
			blox[i,j,k]:=voxel;
		иначе
			если threeblox[i,j,k] = НУЛЬ то
				нов(threeblox[i,j,k]);
			всё;
			threeblox[i,j,k].strokevoxel(p, level-1,voxel);
		всё
	всё
кон strokevoxel;

проц line*(a,b: PT; level: цел32; color:COLOR; mirror:булево);
перем
	tx,ty,tz, dxdt, dydt, dzdt: SREAL;
	t: цел32;
	delta: SREAL;
	n: цел32;
	p, normal: PT;
нач
	просей level из
		1: delta := 1/3;
		|2: delta := 1/9;
		| 3: delta := 1/81;
		|4: delta := 1/243;
		|5: delta := 1/729;
	иначе
		delta := 0;
	всё;
	normal.x := матМодуль(a.x - b.x);
	normal.y := матМодуль(a.y - b.y);
	normal.z := матМодуль(a.z - b.z);
	srBase.normalizePT(normal);
	если delta > 0 то
		n := округлиВниз(srBase.distance(a,b)/delta);
		tx := b.x; ty := b.y; tz := b.z;
		dxdt := (a.x-b.x)/n; dydt := (a.y-b.y)/n; dzdt := (a.z-b.z)/n;
		нцДля t := 0 до n делай
			srBase.setPT(p,tx, ty, tz);
			stroke(p, level,normal,color,mirror);
			tx := tx + dxdt; ty := ty + dydt; tz := tz+dzdt;
		кц
	всё
кон line;


проц nline*(a,b: PT; level: цел32;  normal:PT; color:COLOR; mirror:булево);
перем
	tx,ty,tz, dxdt, dydt, dzdt: SREAL;
	t: цел32;
	delta: SREAL;
	n: цел32;
	p: PT;
нач
	просей level из
		1: delta := 1/3;
		|2: delta := 1/9;
		| 3: delta := 1/81;
		|4: delta := 1/243;
		|5: delta := 1/729;
	иначе
		delta := 0;
	всё;
	если delta > 0 то
		n := округлиВниз(srBase.distance(a,b)/delta);
		tx := b.x; ty := b.y; tz := b.z;
		dxdt := (a.x-b.x)/n; dydt := (a.y-b.y)/n; dzdt := (a.z-b.z)/n;
		нцДля t := 0 до n делай
			srBase.setPT(p,tx, ty, tz);
			stroke(p, level,normal,color,mirror);
			tx := tx + dxdt; ty := ty + dydt; tz := tz+dzdt;
		кц
	всё
кон nline;

проц {перекрыта}linevoxel*(a,b: PT; level: цел32; v: Voxel);
перем
	tx,ty,tz, dxdt, dydt, dzdt: SREAL;
	t: цел32;
	delta: SREAL;
	n: цел32;
	p: PT;

нач
	просей level из
		1: delta := 1/3;
		|2: delta := 1/9;
		| 3: delta := 1/81;
		|4: delta := 1/243;
		|5: delta := 1/729;
		|6: delta := 1/2157
		|7: delta := 1/6471
	иначе
		delta := 0;
	всё;
	если delta > 0 то
		n := округлиВниз(srBase.distance(a,b)/delta);
		tx := b.x; ty := b.y; tz := b.z;
		dxdt := (a.x-b.x)/n; dydt := (a.y-b.y)/n; dzdt := (a.z-b.z)/n;
		нцДля t := 0 до n делай
			srBase.setPT(p,tx, ty, tz);
			strokevoxel(p, level,v);
			tx := tx + dxdt; ty := ty + dydt; tz := tz+dzdt;
		кц
	всё
кон linevoxel;


кон cell;

тип AR3 = массив 3,3,3 из Voxel;
тип NR3 = массив 3,3,3 из NCUBE;
тип BR3 = массив 3,3,3 из cell;

проц pdiv(перем p:PT; d:SREAL);
нач
	p.x:=p.x*d;
	p.y:=p.y*d;
	p.z:=p.z*d;
кон pdiv;

кон srM3Space.

System.Free srM3Space

модуль HierarchicalProfiler0; (** AUTHOR "staubesv"; PURPOSE "WinAos platform-specific part of the hierarchical profiler"; *)

использует
	НИЗКОУР, Kernel32, Objects, Modules, ProcessInfo;

конст
	Initialized = 0;
	Running = 1;
	Terminating = 2;
	Terminated = 3;

	Intervall = 20; (* milliseconds *)

тип

	ProcessTimeArray = укль на массив ProcessInfo.MaxNofProcesses из цел64;

	Callback = проц (id : цел32; process : Objects.Process; pc, bp, lowAdr, highAdr : адресВПамяти);

	Poller = окласс
	перем
		processes, oldProcesses : массив ProcessInfo.MaxNofProcesses из Objects.Process;
		nofProcesses, oldNofProcesses : размерМЗ;
		times, oldTimes : ProcessTimeArray;
		me : Objects.Process;
		state : целМЗ;

		проц &Init;
		нач
			state := Running;
			ProcessInfo.Clear(processes); nofProcesses := 0;
			ProcessInfo.Clear(oldProcesses); oldNofProcesses := 0;
			нов(times); Clear(times);
			нов(oldTimes); Clear(oldTimes);
		кон Init;

		проц Terminate;
		нач {единолично}
			если (state # Terminated) то state := Terminating; всё;
			дождись(state = Terminated);
		кон Terminate;

		проц Clear(array : ProcessTimeArray);
		перем i : размерМЗ;
		нач
			нцДля i := 0 до длинаМассива(array)-1 делай array[i] := 0; кц;
		кон Clear;

		проц RanMeanwhile(process : Objects.Process; currentCycles : цел64) : булево;
		перем i : размерМЗ;
		нач

			если ~(process.mode в {Objects.Running,Objects.Ready}) то возврат ложь всё;

			i := 0; нцПока (i < oldNofProcesses) и	(oldProcesses[i] # process) делай увел(i); кц;
			возврат (i >= oldNofProcesses) или (oldTimes[i] < currentCycles);
		кон RanMeanwhile;

		проц Process;
		перем process : Objects.Process; cycles : Objects.CpuCyclesArray; temp : ProcessTimeArray; i : размерМЗ;
			t0,t1,t2,t3: Kernel32.FileTime;
		нач
			ProcessInfo.GetProcesses(processes, nofProcesses);
			нцДля i := 0 до nofProcesses - 1 делай
				process := processes[i];

				Objects.GetCpuCycles(process, cycles, ложь); (* higher granularity counter, but does not detect suspending of thread *)
				times[i] := cycles[0];
				(*
				Kernel32.GetThreadTimes(process.handle, t0,t1,t2,t3);
				times[i] := SIGNED64(t2.dwLowDateTime+t3.dwLowDateTime) + 10000000H * SIGNED64(t2.dwHighDateTime+t3.dwHighDateTime) ;
				*)
				если (process # me) и (cycles[0] # 0)  и (process.mode = Objects.Running) (* (process.mode # Objects.AwaitingEvent) & (process.mode # Objects.AwaitingCond)  & (process.mode < Objects.Suspended) & (process.mode >= Objects.Ready)  (*RanMeanwhile(process, times[i]) *) *) то
					HandleProcess(process);
				всё;
			кц;
			temp := oldTimes;
			oldTimes := times;
			times := temp;
			ProcessInfo.Copy(processes, oldProcesses); oldNofProcesses := nofProcesses;
			ProcessInfo.Clear(processes);
		кон Process;

	нач {активное, приоритет(Objects.Realtime)}
		me := Objects.CurrentProcess();
		нц
			нцПока (state = Running) делай
				Process;
				Kernel32.Sleep(Intervall);
			кц;
			если (state = Terminating) то прервиЦикл; всё;
		кц;
		ProcessInfo.Clear(processes);
		ProcessInfo.Clear(oldProcesses);
		нач {единолично} state := Terminated; кон;
	кон Poller;

перем
	poller : Poller;
	callback : Callback;
	state : целМЗ;

проц HandleProcess(process : Objects.Process);
перем context : Kernel32.Context; handle : Kernel32.HANDLE; res : Kernel32.BOOL; stackBottom, sp, bp: адресВПамяти;
нач
	утв(process # НУЛЬ);
	handle := process.handle;
	если (handle # Kernel32.NULL) и (handle # Kernel32.InvalidHandleValue) то
		res := Kernel32.SuspendThread(handle);
		если (res >= 0) то
			context.ContextFlags := Kernel32.ContextControl+Kernel32.ContextInteger;
			res := Kernel32.GetThreadContext(handle, context);
			если (res = Kernel32.True) то
				если (context.PC # 0) то
					stackBottom := Objects.GetStackBottom(process);
					bp := context.BP;
					sp := context.SP;
					утв(context.BP <= stackBottom);
					callback(1, process, context.PC, context.BP, context.SP,  stackBottom );
				всё;
			всё;
			res := Kernel32.ResumeThread(handle);
		всё;
	всё;
кон HandleProcess;

проц Enable*(proc : Callback);
нач {единолично}
	утв(proc # НУЛЬ);
	утв((state = Initialized) и (poller = НУЛЬ));
	callback := proc;
	нов(poller);
	state := Running;
кон Enable;

проц Disable*;
нач {единолично}
	утв((state = Running) и (poller # НУЛЬ));
	poller.Terminate;
	poller := НУЛЬ;
	state := Initialized;
кон Disable;

проц Cleanup;
нач
	если (poller # НУЛЬ) то poller.Terminate; poller := НУЛЬ; всё;
кон Cleanup;

нач
	state := Initialized;
	Modules.InstallTermHandler(Cleanup);
кон HierarchicalProfiler0.

WMProfiler.Open

System.Free WMProfiler HierarchicalProfiler HierarchicalProfiler0 ~

Debugging.DisableGC~

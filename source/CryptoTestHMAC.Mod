модуль CryptoTestHMAC;	(** AUTHOR "F.N."; PURPOSE "HMAC Test"; *)

использует
		CryptoHMAC, Utils := CryptoUtils, Log := ЛогЯдра;
	
	
	проц Check( конст a, b: массив из симв8; len: размерМЗ ): булево;
	перем i: размерМЗ;
	нач
		i := 0;
		нцПока (i < len) и (a[i] = b[i]) делай  увел( i )  кц;
		возврат i = len
	кон Check;

	проц DoTest( конст modname, data, key, expDigest: массив из симв8; dataLen, keyLen, hashLen: цел32 );
		перем
			hmac: CryptoHMAC.HMac;
			output: массив 64 из симв8;
	нач
		нов( hmac, modname );
		если hashLen < hmac.size то  hmac.ShrinkLength( hashLen )  всё;
		Log.пВК_ПС; Log.пСтроку8( "=========================================" ); Log.пВК_ПС;
		Log.пСтроку8( "HMAC-Test: " ); Log.пСтроку8( hmac.name ); Log.пВК_ПС;
		hmac.Initialize( key, keyLen );
		hmac.Update( data, 0, dataLen );
		hmac.GetMac( output, 0 );
		если Check( output, expDigest, hashLen ) то
			Log.пСтроку8( "Test Ok" ); Log.пВК_ПС
		иначе
			Log.пСтроку8( "Test failed" ); Log.пВК_ПС; Log.пВК_ПС;
			Log.пСтроку8( "key: " ); Utils.PrintHex( key, 0, keyLen ); Log.пВК_ПС;
			Log.пСтроку8( "data: " ); Utils.PrintHex( data, 0, dataLen ); Log.пВК_ПС;
			Log.пСтроку8( "expected digest: " ); Utils.PrintHex( expDigest, 0, hmac.size ); Log.пВК_ПС;
			Log.пСтроку8( "computed digest: " ); Utils.PrintHex( output, 0, hmac.size ); Log.пВК_ПС;	
		всё;
		
	кон DoTest;

	(* produces two macs from the same data: in one and in two iterations respective *)
	проц ConcatenateTest( modname: массив из симв8; hashLen: размерМЗ );
		перем
			hmac: CryptoHMAC.HMac;
			binData, output, key: массив 20 из симв8;
	нач
		нов( hmac, modname );
		Log.пВК_ПС; Log.пСтроку8( "=========================================" ); Log.пВК_ПС;
		Log.пСтроку8( "HMAC Concatenation-Test. Digest: " ); Log.пСтроку8( modname ); Log.пВК_ПС; Log.пВК_ПС;
		key := "abcdefghijklmnop";
		binData := "hey mister music";
		hmac.Initialize( key, 16 );
		hmac.Update( binData, 0, 16 );
		hmac.GetMac( output, 0 );
		Log.пСтроку8( "digest when Update is invoked once:" ); Utils.PrintHex( output, 0, hmac.size ); Log.пВК_ПС;
		hmac.Initialize( key, 16 );
		hmac.Update( binData, 0, 4 );
		hmac.Update( binData, 4, 6 );
		hmac.Update( binData, 10, 6 );
		hmac.GetMac( output, 0 );
		Log.пСтроку8( "digest when Update is invoked three times:" ); Utils.PrintHex( output, 0, hmac.size ); Log.пВК_ПС
	кон ConcatenateTest;

	(* test vectors from rfc 2104 *)
	проц Test1MD5*;
		перем key, hexKey, hexDigest, digest: массив 64 из симв8;
	нач
		hexKey := "0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b";
		hexDigest := "9294727a3638bb1c13f48ef8158bfc9d";
		Utils.Hex2Bin( hexKey, 0, key, 0, 16 );
		Utils.Hex2Bin( hexDigest, 0, digest, 0, 16 );
		DoTest( "CryptoMD5", "Hi There", key, digest, 8, 16, 16 );
		
		hexDigest := "750c783e6ab0b503eaa86e310a5db738";
		Utils.Hex2Bin( hexDigest, 0, digest, 0, 16 );
		DoTest( "CryptoMD5", "what do ya want for nothing?", "Jefe", digest, 28, 4, 16 );
		DoTest( "CryptoMD5", "what do ya want for nothing?", "Jefe", digest, 28, 4, 12 );
	
	кон Test1MD5;

	проц Test1SHA1*;
		перем key, hexKey, hexDigest, digest: массив 64 из симв8;
	нач
		hexKey := "0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b";
		hexDigest := "b617318655057264e28bc0b6fb378c8ef146be00";
		Utils.Hex2Bin( hexKey, 0, key, 0, 20 );
		Utils.Hex2Bin( hexDigest, 0, digest, 0, 20 );
		DoTest( "CryptoSHA1", "Hi There", key, digest, 8, 20, 20 );
		DoTest( "CryptoSHA1", "Hi There", key, digest, 8, 20, 12 )
	кон Test1SHA1;

	проц Fill( перем a: массив из симв8; len: размерМЗ; val: симв8 );
	перем i: размерМЗ;
	нач
		нцДля i := 0 до len-1 делай  a[i] := val  кц
	кон Fill;
	
	проц Test1SHA256*;
		перем hexKey, key, text, hexDigest, digest: массив 128 из симв8;
	нач
		hexDigest := "f7bc83f430538424b13298e6aa6fb143ef4d59a14946175997479dbc2d1a3cd8";
		Utils.Hex2Bin( hexDigest, 0, digest, 0, 32 );
		DoTest( "CryptoSHA256", 
						"The quick brown fox jumps over the lazy dog", 
						"key", digest, 43, 3, 32 );
						
		hexDigest := "82558a389a443c0ea4cc819899f2083a85f0faa3e578f8077a2e3ff46729665b";
		hexKey := "0102030405060708090a0b0c0d0e0f10111213141516171819";
		Fill( text, 50, 0CDX );
		Utils.Hex2Bin( hexDigest, 0, digest, 0, 32 );
		Utils.Hex2Bin( hexKey, 0, key, 0, 25 );
		DoTest( "CryptoSHA256", text, key, digest, 50, 25, 32 );
		DoTest( "CryptoSHA256", text, key, digest, 50, 25, 12 );
	кон Test1SHA256;
	

	проц MD5ConcatenateTest*;
	нач
		ConcatenateTest( "CryptoMD5", 16 );
	кон MD5ConcatenateTest;

кон CryptoTestHMAC.


System.Free CryptoTestHMAC CryptoHMAC CryptoMD5 CryptoSHA1 CryptoSHA256~

 CryptoTestHMAC.Test1MD5~
 CryptoTestHMAC.Test1SHA1~
 CryptoTestHMAC.Test1SHA256~
 CryptoTestHMAC.MD5ConcatenateTest~


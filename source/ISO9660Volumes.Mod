(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль ISO9660Volumes;	(* AUTHOR "?/be"; PURPOSE "ISO 9660 volume (ported from Native Oberon)" *)

(** non-portable *)

	использует
		ЛогЯдра, Plugins, Disks, Files;

	конст
		debug = ложь; getBlockDebug = ложь;

	конст	(* svr *)
		SS = 2048;	(* sector size *)
		MaxRetries = 10;

	тип
		Volume* = окласс (Files.Volume)
		перем
			dev-: Disks.Device;
			bpc: цел32;	(* bytes per sector *)
			spc: цел32;	(* sectors per cluster *)

			проц {перекрыта}Finalize*;
			перем res: целМЗ;
			нач {единолично}
				если debug то ЛогЯдра.пСтроку8("Entering OFSISO9660Volumes.Finalize"); ЛогЯдра.пВК_ПС всё;
				исключиИзМнваНаБитах(dev.flags, Disks.Mounted);
				dev.Close(res)
			кон Finalize;

			проц {перекрыта}Available*(): цел32;
			нач
				возврат 0
			кон Available;

			проц {перекрыта}GetBlock*(adr: цел32; перем blk: массив из симв8);
			перем res: целМЗ; i: цел32;
			нач {единолично}
				утв(dev # НУЛЬ, 101);
				i := 0;
				нцДо
					dev.Transfer(Disks.Read, adr, 1, blk, 0, res);
					увел(i)
				кцПри (res = 0) или (i >= MaxRetries);
				если getBlockDebug и (i > 1) то ЛогЯдра.пСтроку8("GetBlock; "); ЛогЯдра.пЦел64(i, 0); ЛогЯдра.пСтроку8(" retries"); ЛогЯдра.пВК_ПС всё;
				утв(res = 0, 102)
			кон GetBlock;

			проц {перекрыта}AllocBlock*(hint: Files.Address; перем adr: Files.Address);
			нач СТОП(301)
			кон AllocBlock;

			проц {перекрыта}FreeBlock*(adr: Files.Address);
			нач СТОП(301)
			кон FreeBlock;

			проц {перекрыта}MarkBlock*(adr: Files.Address);
			нач СТОП(301)
			кон MarkBlock;

			проц {перекрыта}Marked*(adr: Files.Address): булево;
			нач СТОП(301)
			кон Marked;
		кон Volume;

проц GetISO9660Volume(p: Files.Parameters; dev: Disks.Device);
перем vol: Volume; b: массив SS из симв8;
нач
	нов(vol); vol.flags := {}; vol.dev := dev;
	включиВоМнвоНаБитах(vol.flags, Files.ReadOnly); включиВоМнвоНаБитах(vol.flags, Files.Removable);
	vol.bpc := SS; vol.spc := 1;
	vol.GetBlock(16, b); (* dummy; necessary after disc change *)
	копируйСтрокуДо0(vol.dev.name, vol.name);
	vol.blockSize := vol.bpc;
	если debug то
		ЛогЯдра.пСтроку8("GetISO9660Volume"); ЛогЯдра.пВК_ПС;
		ЛогЯдра.пСтроку8("  spc="); ЛогЯдра.пЦел64(vol.spc, 0); ЛогЯдра.пСтроку8("  bpc="); ЛогЯдра.пЦел64(vol.bpc, 0); ЛогЯдра.пВК_ПС
	всё;
	p.vol := vol
кон GetISO9660Volume;

(** Generate a new ISO9660 volume object. Files.Par: device [# part (ignored)] *)
проц New*(context : Files.Parameters);
перем
	name: Plugins.Name;  i, ignore: цел32; res: целМЗ;
	table: Plugins.Table; dev: Disks.Device;
нач
	context.vol := НУЛЬ;
	Files.GetDevPart(context.arg, name, ignore);
	если (name # "") то
		Disks.registry.GetAll(table);
		если (table # НУЛЬ) то
			context.out.пСтроку8("ISO9660Volumes: Device ");  context.out.пСтроку8(name);

			i := 0; нцПока (i # длинаМассива(table)) и (table[i].name # name) делай увел(i) кц;
			если (i < длинаМассива(table)) то
				dev := table[i](Disks.Device);
				dev.Open(res);
				если (res = Disks.Ok) то
					если ~(Disks.Mounted в dev.table[0].flags) то
						GetISO9660Volume(context, dev);
					иначе context.error.пСтроку8(" already mounted")
					всё;

					если (context.vol = НУЛЬ) то
						dev.Close(res) (* close again - ignore res *)
					всё
				иначе context.error.пСтроку8(" cannot open device"); context.error.пВК_ПС;
				всё
			иначе context.error.пСтроку8(" not found"); context.error.пВК_ПС;
			всё;
		всё
	всё;
кон New;

кон ISO9660Volumes.

OFSTools.Mount TEST "IDE1.0" OFSISO9660Volumes.New OFSN2KFiles.NewFS
OFSTools.Mount A "Diskette0" OFSISO9660Volumes.New OFSN2KFiles.NewFS
OFSTools.Unmount ^ TEST A


System.Free OFSISO9660Volumes ~

модуль EnetArp;
(**
	AUTHOR: Alexey Morozov, HighDim GmbH, 2015
	PURPOSE: Ethernet networking stack, ARP protocol
*)

использует
	НИЗКОУР, EnetBase, EnetTiming, Interfaces := EnetInterfaces, Trace := EnetTrace;

конст
	ArpHwTypeEth* = 0x0100; (** ARP hardware type *)

	(**
		ARP operation types
	*)
	ArpOpRequest* = 0x0100;
	ArpOpResponse* = 0x0200;

	ArpCacheSize* = 256;

	SubnetMaskC = 0x00FFFFFF; (* mask for class C subnetwork (255.255.255.0) *)

	UpdateIntervalMs = 10000; (* update ARP cache state interval in ms *)
	ArpEntryRequestTimeoutMs = 100; (* ARP entry request timeout in ms *)
	ArpEntryExpireTimeoutMs = 20*60*1000; (* default ARP entry expiration timeout in ms *)

тип
	Int32 = EnetBase.Int32;
	Int16 = EnetBase.Int16;
	Int = EnetBase.Int;
	ResultCode = EnetBase.ResultCode;

	(**
		ARP cache
	*)
	ArpCache* = укль на ArpCacheDesc;
	ArpCacheDesc* = запись(EnetBase.IpAddrCacheDesc)
		entries*: массив ArpCacheSize из ArpEntry;
		requested: ArpEntry; (* linked list of entries being under resolution *)
	кон;

	(**
		Entry of Address Resolution Protocol (ARP) cache
	*)
	ArpEntry* = укль на ArpEntryDesc;
	ArpEntryDesc* = запись(EnetBase.IpAddrCacheEntryDesc)
		static*: булево; (** TRUE for static entries not to be removed automatically *)
		resolved*: булево; (** becomes TRUE after the IPv4 address of the entry has been resolved *)
		requested*: булево; (** TRUE if the entry is in the requested list *)
		timestamp*: EnetTiming.Time; (* time when the entry was registered as resolved *)

		completionHandler: EnetBase.TaskHandler; (* completion handler from the user *)
		timeoutHandler: EnetBase.TaskHandler; (* request timeout handler *)

		prevReq, nextReq: ArpEntry; (* for managing a linked list of requested entries *)
		cache: ArpCache;
	кон;

	(*
		Initialize ARP cache
	*)
	проц InitArpCache(cache: ArpCache; intf: EnetBase.Interface);
	перем
		i: Int;
		entry: ArpEntry;
	нач
		утв(cache # НУЛЬ);

		нцДля i := 0 до ArpCacheSize-1 делай
			нов(entry);
			entry.ipAddr.addr[0] := 0;
			entry.ipAddr.ver := 4;
			entry.macAddr := EnetBase.NilMacAddr;
			entry.static := ложь;
			entry.resolved := ложь;
			entry.requested := ложь;
			entry.timestamp := 0;
			entry.cache := cache;
			нов(entry.timeoutHandler);

			cache.entries[i] := entry;
		кц;

		cache.requested := НУЛЬ;

		cache.cleanCache := CleanArpCache;
		cache.addStaticEntry := AddStaticEntry;
		cache.enumerateEntries := EnumerateEntries;
		cache.intf := intf;
	кон InitArpCache;

	проц IsSubnetMaskC(конст subnetMask: EnetBase.IpAddr): булево;
	нач
		возврат НИЗКОУР.MSK(subnetMask.addr[0],SubnetMaskC) = SubnetMaskC;
	кон IsSubnetMaskC;

	(*
		Get a free ARP entry given an IP address
	*)
	проц GetFreeEntry(cache: ArpCache; ipv4Addr: Int32): ArpEntry;
	перем
		i: Int;
		entry: ArpEntry;
	нач
		если IsSubnetMaskC(cache.intf.ipv4SubnetMask) то
			i := (ipv4Addr DIV 1000000H) остОтДеленияНа 100H; (* the most significant byte *)
			entry := cache.entries[i];
			утв(~entry.static и ~entry.resolved и ~entry.requested);
			entry.ipAddr.addr[0] := ipv4Addr;
			возврат entry;
		иначе
			СТОП(100);
		всё;
	кон GetFreeEntry;

	(*
		Find a resolved ARP cache entry given an IPv4 address
	*)
	проц FindResolvedEntryByIpAddr(cache: ArpCache; ipv4Addr: Int32): ArpEntry;
	перем
		i: Int;
		entry: ArpEntry;
	нач
		если IsSubnetMaskC(cache.intf.ipv4SubnetMask) то
			i := (ipv4Addr DIV 1000000H) остОтДеленияНа 100H; (* the most significant byte *)
			entry := cache.entries[i];
			если entry.resolved то возврат entry; иначе возврат НУЛЬ; всё;
		иначе
			СТОП(100);
		всё;
	кон FindResolvedEntryByIpAddr;

	(*
		Find a requested ARP entry given an IPv4 address
	*)
	проц FindRequestedEntryByIpAddr(cache: ArpCache; ipv4Addr: Int32; removeIfResolved: булево): ArpEntry;
	перем entry, prev: ArpEntry;
	нач
		entry := cache.requested;
		нцПока (entry # НУЛЬ) и (entry.ipAddr.addr[0] # ipv4Addr) делай
			prev := entry;
			entry := prev.nextReq;
		кц;
		если removeIfResolved и (entry # НУЛЬ) то
			RemoveRequestedEntry(cache,entry);
		всё;
		возврат entry;
	кон FindRequestedEntryByIpAddr;

	(*
		Add an ARP entry to the requested list
	*)
	проц AddRequestedEntry(cache: ArpCache; entry: ArpEntry);
	нач
		entry.requested := истина;
		entry.prevReq := НУЛЬ;
		если cache.requested = НУЛЬ то
			entry.nextReq := НУЛЬ;
			cache.requested := entry;
		иначе
			entry.nextReq := cache.requested;
			cache.requested.prevReq := entry;
			cache.requested := entry;
		всё;
	кон AddRequestedEntry;

	(*
		Remove an ARP entry from the requested list
	*)
	проц RemoveRequestedEntry(cache: ArpCache; entry: ArpEntry);
	нач
		entry.requested := ложь;
		если entry.prevReq # НУЛЬ то entry.prevReq.nextReq := entry.nextReq;
		аесли entry = cache.requested то cache.requested := entry.nextReq; всё;
		если entry.nextReq # НУЛЬ то entry.nextReq.prevReq := entry.prevReq; всё;
		entry.nextReq := НУЛЬ;
		entry.prevReq := НУЛЬ;
	кон RemoveRequestedEntry;

	(*
		Make an ARP announcement on a given network interface
	*)
	проц ArpAnnouncement(intf: EnetBase.Interface; перем res: ResultCode): булево;
	перем packet: EnetBase.Packet;
	нач
		если ~Interfaces.GetTxPacket(intf,packet) то
			res := EnetBase.ErrTxPacketPoolEmpty;
			возврат ложь;
		всё;

		(* setup Ethernet frame header *)
		packet.ethFrameHdr.dstMacAddr := EnetBase.BroadcastMacAddr;
		packet.ethFrameHdr.srcMacAddr := intf.dev.macAddr;
		packet.ethFrameHdr.etherType := EnetBase.EtherTypeArp;

		(* setup ARP header *)
		packet.arpHdr.hwType := ArpHwTypeEth;
		packet.arpHdr.protoType := EnetBase.EtherTypeIpv4;
		packet.arpHdr.hwAddrLen := 6;
		packet.arpHdr.protoAddrLen := 4;
		packet.arpHdr.operation := ArpOpRequest;
		packet.arpHdr.srcMacAddr := intf.dev.macAddr;
		packet.arpHdr.srcIpAddr := intf.ipv4Addr.addr[0];
		packet.arpHdr.dstMacAddr := EnetBase.BroadcastMacAddr;
		packet.arpHdr.dstIpAddr := intf.ipv4Addr.addr[0];

		packet.dataLen := размер16_от(EnetBase.EthFrameHdr) + размер16_от(EnetBase.ArpHdr);

		возврат Interfaces.SendPacket(intf,packet,{},НУЛЬ,res);
	кон ArpAnnouncement;

	(**
		Send an ARP request for resolving a given IP address
	*)
	проц ArpRequest(intf: EnetBase.Interface; конст ipv4AddrToResolve: Int32; перем res: ResultCode): булево;
	перем
		packet: EnetBase.Packet;
	нач
		если ~Interfaces.GetTxPacket(intf,packet) то
			res := EnetBase.ErrTxPacketPoolEmpty;
			возврат ложь;
		всё;

		(* setup Ethernet frame header *)
		packet.ethFrameHdr.dstMacAddr := EnetBase.BroadcastMacAddr;
		packet.ethFrameHdr.srcMacAddr := intf.dev.macAddr;
		packet.ethFrameHdr.etherType := EnetBase.EtherTypeArp;

		(* setup ARP header *)
		packet.arpHdr.hwType := ArpHwTypeEth;
		packet.arpHdr.protoType := EnetBase.EtherTypeIpv4;
		packet.arpHdr.hwAddrLen := 6;
		packet.arpHdr.protoAddrLen := 4;
		packet.arpHdr.operation := ArpOpRequest;
		packet.arpHdr.srcMacAddr := intf.dev.macAddr;
		packet.arpHdr.srcIpAddr := intf.ipv4Addr.addr[0];
		packet.arpHdr.dstMacAddr := EnetBase.NilMacAddr;
		packet.arpHdr.dstIpAddr := ipv4AddrToResolve;

		packet.dataLen := размер16_от(EnetBase.EthFrameHdr) + размер16_от(EnetBase.ArpHdr);

		возврат Interfaces.SendPacket(intf,packet,{},НУЛЬ,res);
	кон ArpRequest;

	(*
		Handle an ARP packet
	*)
	проц HandleArpPacket(intf: EnetBase.Interface; packet: EnetBase.Packet; flags: мнвоНаБитахМЗ);
	перем
		cache: ArpCache;
		entry: ArpEntry;
		srcIpAddr: Int32;
		res: ResultCode;
		completionHandler, timeoutHandler: EnetBase.TaskHandler;
	нач
		если (packet.arpHdr.hwType = ArpHwTypeEth) и (packet.arpHdr.protoType = EnetBase.EtherTypeIpv4) и (packet.arpHdr.dstIpAddr = intf.ipv4Addr.addr[0]) то
			если packet.arpHdr.operation = ArpOpRequest то
				packet.arpHdr.operation := ArpOpResponse;
				packet.arpHdr.dstMacAddr := packet.arpHdr.srcMacAddr;
				packet.arpHdr.dstIpAddr := packet.arpHdr.srcIpAddr;
				packet.arpHdr.srcMacAddr := intf.dev.macAddr;
				packet.arpHdr.srcIpAddr := intf.ipv4Addr.addr[0];
				если ~Interfaces.ReplyEth(intf,packet,{},НУЛЬ,res) то
				всё;
			аесли packet.arpHdr.operation = ArpOpResponse то

				cache := intf.ipv4AddrCache(ArpCache);
				если EnetBase.ThreadSafe то cache.acquireWrite; всё;

				(* find the corresponding ARP entry request *)
				srcIpAddr := packet.arpHdr.srcIpAddr;
				entry := FindRequestedEntryByIpAddr(cache,srcIpAddr,истина);
				если entry # НУЛЬ то (*! account for cases when the requested entry was removed earlier due to timeout expiration *)
					entry.macAddr := packet.arpHdr.srcMacAddr;
					entry.timestamp := EnetTiming.getTimeCounter();
					entry.resolved := истина;
					EnetBase.RemoveTask(intf,entry.timeoutHandler); (* remove timeout handler from the interface non-periodic task list *)
					completionHandler := entry.completionHandler;
					entry.completionHandler := НУЛЬ;
				всё;

				если EnetBase.ThreadSafe то cache.releaseWrite; всё;

				(* notify the user about the operation completion *)
				если completionHandler # НУЛЬ то
					completionHandler.res := 0;
					если completionHandler.handle # НУЛЬ то
						completionHandler.handle(completionHandler);
					всё;
				всё;
			всё;
		всё;
	кон HandleArpPacket;

	(*
		Request resolution of a new ARP cache entry
	*)
	проц ArpEntryRequest(
											intf: EnetBase.Interface;
											cache: ArpCache;
											entry: ArpEntry;
											completionHandler: EnetBase.TaskHandler;
											handleTimeout: булево;
											перем res: ResultCode): булево;
	перем
		i: Int;
		b: булево;
	нач
		если ArpRequest(intf,entry.ipAddr.addr[0],res) то

			AddRequestedEntry(cache,entry); (* add the requested entry to the linked list of requested entries *)
			entry.completionHandler := completionHandler;
			если handleTimeout то
				entry.timeoutHandler.handle := HandleArpEntryRequestTimeout;
				entry.timeoutHandler.param := entry;
				EnetBase.ScheduleTask(intf,entry.timeoutHandler,ложь,entryRequestTimeout);
			всё;

			res := EnetBase.OpInProgress;

			возврат истина;
		иначе
			возврат ложь;
		всё;
	кон ArpEntryRequest;

	(*
		Resolve IPv4 address, thread-safe
	*)
	проц ResolveIpv4Addr(intf: EnetBase.Interface; конст ipAddr: EnetBase.IpAddr; перем macAddr: EnetBase.MacAddr; completionHandler: EnetBase.TaskHandler; перем res: ResultCode): булево;
	перем
		cache: ArpCache;
		entry: ArpEntry;
		b: булево;
	нач
		утв(ipAddr.ver = 4);

		(* Broadcast address is always resolved to broadcast MAC *)
		если ipAddr = EnetBase.BroadcastIpAddr то
			macAddr := EnetBase.BroadcastMacAddr;
			res := 0;
			
			если (completionHandler # НУЛЬ) и (completionHandler.handle # НУЛЬ) то
				completionHandler.res := 0;
				EnetBase.ScheduleTask(intf, completionHandler, ложь, 1)
			всё;
			возврат истина
		всё;

		cache := intf.ipv4AddrCache(ArpCache);

		если EnetBase.ThreadSafe то cache.acquireRead; всё;

		entry := FindResolvedEntryByIpAddr(cache,ipAddr.addr[0]);
		если entry # НУЛЬ то
			macAddr := entry.macAddr;

			если EnetBase.ThreadSafe то cache.releaseRead; всё;

			res := 0;
			b := истина;

			если (completionHandler # НУЛЬ) и (completionHandler.handle # НУЛЬ) то
				completionHandler.res := 0;
				EnetBase.ScheduleTask(intf, completionHandler, ложь, 1)
			всё;
		иначе
			если EnetBase.ThreadSafe то cache.releaseRead; всё;

			если completionHandler # НУЛЬ то

				completionHandler.res := EnetBase.OpInProgress;

				если EnetBase.ThreadSafe то cache.acquireWrite; всё;

				entry := FindRequestedEntryByIpAddr(cache,ipAddr.addr[0],ложь);
				если entry # НУЛЬ то (*! the specified IP address is being already in resolution *)
					EnetBase.LinkTaskHandlers(entry.completionHandler,completionHandler);
				иначе (* send an ARP request *)
					entry := GetFreeEntry(cache,ipAddr.addr[0]);
					утв(entry # НУЛЬ);
					b := ArpEntryRequest(intf,cache,entry,completionHandler,истина,res);
				всё;

				если EnetBase.ThreadSafe то cache.releaseWrite; всё;
			иначе
				b := ложь;
				res := EnetBase.ErrUnresolvedAddr;
			всё;
		всё;

		возврат b;
	кон ResolveIpv4Addr;

	проц CleanArpCache(cache: EnetBase.IpAddrCache; cleanStatic: булево);
	перем
		i: Int;
		entry: ArpEntry;
	нач
		просейТип cache : ArpCache делай
			если EnetBase.ThreadSafe то cache.acquireWrite; всё;
			нцДля i := 0 до ArpCacheSize-1 делай
				entry := cache.entries[i];
				если ~entry.requested и (cleanStatic или ~entry.static) то
					entry.resolved := ложь;
				всё;
			кц;
			если EnetBase.ThreadSafe то cache.releaseWrite; всё;
		всё;
	кон CleanArpCache;

	(*
		Add a static entry to the address resolution table
	*)
	проц AddStaticEntry(cache: EnetBase.IpAddrCache; конст ipAddr: EnetBase.IpAddr; конст macAddr: EnetBase.MacAddr; перем res: ResultCode): булево;
	перем
		i: Int;
		entry: ArpEntry;
	нач
		утв(ipAddr.ver = 4);

		просейТип cache : ArpCache делай

			если IsSubnetMaskC(cache.intf.ipv4SubnetMask) то

				если EnetBase.ThreadSafe то cache.acquireWrite; всё;

				i := (ipAddr.addr[0] DIV 1000000H) остОтДеленияНа 100H; (* the most significant byte *)

				entry := cache.entries[i];
				entry.ipAddr := ipAddr;
				entry.macAddr := macAddr;
				entry.static := истина;
				entry.resolved := истина;
				entry.requested := ложь;
				entry.timestamp := 0;
				entry.prevReq := НУЛЬ;
				entry.nextReq := НУЛЬ;

				если EnetBase.ThreadSafe то cache.releaseWrite; всё;

				res := 0;
				возврат истина;
			иначе
				СТОП(100);
			всё;
		всё;
	кон AddStaticEntry;

	(*
		Enumerate all resolved entries of the address resolution table
	*)
	проц EnumerateEntries(cache: EnetBase.IpAddrCache; enumerator: проц{делегат}(entry: EnetBase.IpAddrCacheEntry));
	перем
		i: Int;
		entry: ArpEntry;
	нач
		утв(enumerator # НУЛЬ);
		просейТип cache : ArpCache делай
			если EnetBase.ThreadSafe то cache.acquireRead; всё;
			нцДля i := 0 до ArpCacheSize-1 делай
				entry := cache.entries[i];
				если entry.resolved то
					enumerator(entry);
				всё;
			кц;
			если EnetBase.ThreadSafe то cache.releaseRead; всё;
		всё;
	кон EnumerateEntries;

	проц ShowEntry(entry: ArpEntry);
	перем j: Int;
	нач
		Trace.StringLn("ipv4Addr=0x" и Trace.Hx(entry.ipAddr.addr[0],8));
		Trace.String("macAddr=" и Trace.Hx(entry.macAddr.addr[0],2));
		нцДля j := 1 до длинаМассива(entry.macAddr.addr)-1 делай
			Trace.String(":" и Trace.Hx(entry.macAddr.addr[j],2));
		кц;
		Trace.StringLn("");
		Trace.StringLn("static=" и entry.static);
		Trace.StringLn("resolved=" и entry.resolved);
		Trace.StringLn("requested=" и entry.requested);
		Trace.StringLn("timestamp=" и entry.timestamp);
	кон ShowEntry;

	проц ShowArpCache*(cache: ArpCache);
	перем
		i, j, n: Int;
		entry: ArpEntry;
	нач
		если EnetBase.ThreadSafe то cache.acquireRead; всё;

		Trace.StringLn("resolved entries: ");
		n := 0;
		нцДля i := 0 до длинаМассива(cache.entries)-1 делай
			entry := cache.entries[i];
			если entry.resolved то
				Trace.StringLn("#" и n);
				ShowEntry(entry);
				увел(n);
			всё;
		кц;

		если n = 0 то
			Trace.StringLn("none");
		всё;

		Trace.StringLn("requested entries: ");
		n := 0;
		entry := cache.requested;
		нцПока entry # НУЛЬ делай
			Trace.StringLn("#" и n);
			ShowEntry(entry);
			увел(n);
			entry := entry.nextReq;
		кц;

		если EnetBase.ThreadSafe то cache.releaseRead; всё;

		если n = 0 то
			Trace.StringLn("none");
		всё;
	кон ShowArpCache;

	проц HandleArpEntryRequestTimeout(handler: EnetBase.TaskHandler);
	перем
		intf: EnetBase.Interface;
		cache: ArpCache;
		requested: ArpEntry;
		completionHandler, tmp: EnetBase.TaskHandler;
	нач
		requested := handler.param(ArpEntry);
		cache := requested.cache;
		intf := cache.intf;
		если EnetBase.ThreadSafe то cache.acquireWrite; всё;
		completionHandler := requested.completionHandler;
		RemoveRequestedEntry(cache,requested);
		если EnetBase.ThreadSafe то cache.releaseWrite; всё;

		(* inform the user about timeout expiration *)
		нцПока completionHandler # НУЛЬ делай
			completionHandler.res := EnetBase.ErrTimeoutExpired;
			если completionHandler.handle # НУЛЬ то
				completionHandler.handle(completionHandler);
			всё;
			tmp := completionHandler.next;
			completionHandler.next := НУЛЬ; (*! unlink handlers *)
			completionHandler := tmp;
		кц;
	кон HandleArpEntryRequestTimeout;

	проц Update(handler: EnetBase.TaskHandler);
	перем
		intf: EnetBase.Interface;
		cache: ArpCache;
		entry: ArpEntry;
		t: EnetTiming.Time;
		i: Int; res: ResultCode;
		requestedEntryUpdate: булево;
	нач
		intf := handler.param(EnetBase.Interface);
		cache := intf.ipv4AddrCache(ArpCache);

		если EnetBase.ThreadSafe то cache.acquireWrite; всё;

		(*
			update expired dynamic entries
		*)
		t := EnetTiming.getTimeCounter();
		requestedEntryUpdate := ложь;
		нцДля i := 0 до ArpCacheSize-1 делай
			entry := cache.entries[i];
			если ~requestedEntryUpdate и ~entry.static и entry.resolved и ~entry.requested и (t - entry.timestamp >= entryExpireTimeout) то
				если ArpEntryRequest(intf,cache,entry,НУЛЬ,ложь,res) то (*! do not schedule timeout task - no task scheduling is allowed in a task handler *)
					requestedEntryUpdate := истина; (*! request updating of one entry at a time to avoid ARP message storm *)

					Trace.StringLn("requested update of an ARP entry: ");
					ShowEntry(entry);

				иначе
					entry.resolved := ложь;
					утв(~entry.requested);
				всё;
			аесли ~entry.static и entry.resolved и entry.requested и (t - entry.timestamp >= entryRequestTimeout) то
				RemoveRequestedEntry(cache,entry);
				entry.resolved := ложь;
			всё;
		кц;

		если EnetBase.ThreadSafe то cache.releaseWrite; всё;

	кон Update;

перем
	entryRequestTimeout: EnetTiming.Time;
	entryExpireTimeout: EnetTiming.Time;

	проц Install*(intf: EnetBase.Interface);
	перем
		cache: ArpCache;
		updateTask: EnetBase.TaskHandler;
	нач

		если entryRequestTimeout = 0 то
			entryRequestTimeout := EnetTiming.fromMilli(ArpEntryRequestTimeoutMs);
			entryExpireTimeout := EnetTiming.fromMilli(ArpEntryExpireTimeoutMs);
		всё;

		нов(cache);
		InitArpCache(cache,intf);
		intf.ipv4AddrResolve := ResolveIpv4Addr;
		intf.ipv4AddrCache := cache;

		EnetBase.SetEthFrameHandler(intf,EnetBase.EtherTypeArp,HandleArpPacket);

		нов(updateTask);
		updateTask.res := 0;
		updateTask.handle := Update;
		updateTask.param := intf;

		EnetBase.ScheduleTask(intf,updateTask,истина,EnetTiming.fromMilli(UpdateIntervalMs));
	кон Install;

кон EnetArp.

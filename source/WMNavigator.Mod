модуль WMNavigator; (** AUTHOR "staubesv"; PURPOSE "Viewport in a window for navigation"; *)

(* STATUS: First draft - NOT STABLE!!! *)

использует
	Modules, Kernel, Locks, Displays, Raster, Строки8, XML, WMRectangles, WMGraphics, WMGraphicUtilities, WMWindowManager, WMComponents;

тип

	Level = запись
		x, y, width, height : размерМЗ;
	кон;

	OnDrawnProc = проц {делегат};

	ViewPort* = окласс (WMWindowManager.ViewPort);
	перем
		backbuffer- : WMGraphics.Image;
		deviceRect : WMRectangles.Rectangle;
		width, height : размерМЗ;
		canvas : WMGraphics.BufferCanvas;
		state : WMGraphics.CanvasState;
		internnavig, navig : булево;
		fx, fy, inffx, inffy, factor, intfactor : вещ32;
		lock : Locks.Lock;
		onDrawn : OnDrawnProc;

		zoomLevel : массив 7 из Level;
		currentZoomLevel : размерМЗ;

		проц &New*;
		нач
			нов(backbuffer);
			Raster.Create(backbuffer, 1280, 1024, Raster.DisplayFormat(Displays.color8888));
			range.l := 0; range.t := 0;
			range.r := range.l + 1280; range.b := range.t + 1024;
			width := 1280; height := 1024;
			deviceRect.l := 0; deviceRect.t := 0;
			deviceRect.r := 1280; deviceRect.b := 1024;
			width0 := 1280; height0 := 1024;
			desc := "Graphics adapter view";
			нов(canvas, backbuffer);
			canvas.SetFont(WMGraphics.GetDefaultFont());
			canvas.SaveState(state);
			factor := 1; intfactor := 1;
			fx := factor; fy := factor; inffx := 1 ; inffy := inffx;
			internnavig := ложь;
			нов(lock);
			onDrawn := НУЛЬ;
			currentZoomLevel := 0;
			SetZoomLevels(1280, 1024);
		кон New;

		проц SetZoomLevels(width, height : размерМЗ);
		перем i : размерМЗ;
		нач
			нцДля i := 0 до длинаМассива(zoomLevel)-1 делай
				zoomLevel[i].width := (i + 1) * width;
				zoomLevel[i].height :=(i + 1) * height ;
				zoomLevel[i].x := (zoomLevel[i].width - width) DIV 2;
				zoomLevel[i].y := (zoomLevel[i].height - height) DIV 2;
			кц;
		кон SetZoomLevels;

		проц SetZoomLevel(level, xg, yg : размерМЗ);
		нач
			если (level < 0) то level := 0;
			аесли (level >= длинаМассива(zoomLevel)) то level := длинаМассива(zoomLevel)-1; всё;
			SetRange(xg - zoomLevel[level].x, yg - zoomLevel[level].y, zoomLevel[level].width, zoomLevel[level].height, истина);
			currentZoomLevel := level;
		кон SetZoomLevel;

		проц ChangeZoom(dz, xg, yg : размерМЗ);
		нач
			SetZoomLevel(currentZoomLevel + dz, xg, yg);
		кон ChangeZoom;

		проц ReInit(width, height : размерМЗ; format : целМЗ; onDrawn : OnDrawnProc);
		перем tf : вещ32;
		нач
			сам.onDrawn := onDrawn;
			если (width # сам.width) или (height # сам.height) то
				lock.Acquire;
				сам.width := width; сам.height := height;
				если (width > 0) и (height > 0) то
					нов(backbuffer);
					Raster.Create(backbuffer, width, height, Raster.DisplayFormat(format));
					deviceRect.l := 0; deviceRect.t := 0;
					deviceRect.r := width; deviceRect.b := height;
					width0 := width; height0 := height;
					нов(canvas, backbuffer);
					canvas.SetFont(WMGraphics.GetDefaultFont());
					canvas.SaveState(state);

					factor := width / (range.r - range.l);
					tf := height / (range.b - range.t);
					если factor > tf то factor := tf всё;
					fx := factor; fy := factor; inffx := 1 / factor; inffy := inffx;
					intfactor := factor;
					range.r := range.l + inffx * width;
					range.b := range.t + inffy * height;
					SetZoomLevels(width, height);
				иначе
					canvas := НУЛЬ;
				всё;
				lock.Release;
			всё;
		кон ReInit;

		проц GetWMCoordinates*(конст r : WMRectangles.Rectangle) : WMRectangles.Rectangle;
		перем rect : WMRectangles.Rectangle;
		нач
			rect.l := округлиВниз(range.l + r.l * inffx);
			rect.r := округлиВниз(range.l + r.r * inffx + 0.5);
			rect.t := округлиВниз(range.t + r.t * inffy);
			rect.b := округлиВниз(range.t + r.b * inffy + 0.5);
			возврат rect;
		кон GetWMCoordinates;

		проц GetWMPosition(x, y : размерМЗ; перем xg, yg : размерМЗ);
		нач
			xg := округлиВниз(range.l + x * inffx);
			yg := округлиВниз(range.t + y * inffy);
		кон GetWMPosition;

		(**  Return the modifier keys that are pressed in the view *)
		проц {перекрыта}GetKeyState*(перем state : мнвоНаБитахМЗ);
		нач
			state := {};
		кон GetKeyState;

		(** Set the observed range. *)
		проц {перекрыта}SetRange*(x, y, w, h : вещ32; showTransition : булево);
		перем
			sx, sy, sx2, sy2, dx, dy, dx2, dy2, x2, y2  : вещ32;
			i, steps : цел32;
		конст Steps = 16;

			проц Set(x, y, w, h : вещ32);
			перем tf : вещ32;
			нач
				range.l := x;
				range.t := y;
				factor := (width) / w;
				tf := (height) / h;
				если factor > tf то factor := tf всё;
				fx := factor; fy := factor; inffx := 1 / factor; inffy := inffx;
				range.r := x + width * inffx;
				range.b := y + height * inffy;
				intfactor := factor;
				manager.RefreshView(сам);
				если onDrawn # НУЛЬ то onDrawn(); всё;
			кон Set;

		нач
			если w = 0 то w := 0.001 всё;
			если h = 0 то h := 0.001 всё;
			если showTransition то
				sx := range.l; sy := range.t;
				sx2 := range.r; sy2 := range.b;
				x2 := x + w; y2 := y + h;
				steps := Steps;
				если (sx = x) и (sy = y) и (sx2 - sx = w) и (sy2- sy = h) то steps := 1 всё;
				dx := (x - sx) / steps;
				dy := (y - sy) / steps;
				dx2 := (x2 - sx2) / steps;
				dy2 := (y2 - sy2) / steps;

				internnavig := истина; navig := истина;
				нцДля i := 1 до steps-1 делай
					Set(sx + dx * i, sy + dy * i, (sx2 + dx2 * i) - (sx + dx * i), (sy2 + dy2 * i) - (sy + dy * i))
				кц;
				internnavig := ложь; navig := ложь
			всё;
			Set(x, y, w, h)
		кон SetRange;

		(** r in wm coordinates *)
		проц {перекрыта}Update*(r : WMRectangles.Rectangle; top : WMWindowManager.Window);
		нач
			lock.Acquire;
			Draw(WMRectangles.ResizeRect(r, 1), top.prev);(* assuming the src-domain is only 1 *)
			lock.Release;
		кон Update;

		проц {перекрыта}Refresh*(top : WMWindowManager.Window);
		нач
			Update(WMRectangles.MakeRect(округлиВниз(range.l)-1, округлиВниз(range.t)-1, округлиВниз(range.r) + 1, округлиВниз(range.b) + 1), top)
		кон Refresh;

		проц DrawWindow(window : WMWindowManager.Window) : булево;
		перем title : Строки8.уСтрока;
		нач
			утв(window # НУЛЬ);
			если (window.isVisible и ~(WMWindowManager.FlagNavigation в window.flags)) то
				title := window.GetTitle();
				возврат (title = НУЛЬ) или ((title^ # "Mouse Cursor"));
			иначе
				возврат ложь;
			всё;
		кон DrawWindow;

		(* in wm coordinates *)
		проц Draw(r : WMRectangles.Rectangle; top : WMWindowManager.Window);
		перем cur : WMWindowManager.Window;
			wr, nr : WMRectangles.Rectangle;

			проц InternalDraw(r : WMRectangles.Rectangle; cur : WMWindowManager.Window);
			перем nr, cb, dsr : WMRectangles.Rectangle; width, height : цел32;
			нач
				утв(cur.isVisible);
				если cur.useAlpha и (cur.prev # НУЛЬ)  то Draw(r, cur.prev)
				иначе
					нцПока (cur # НУЛЬ) делай (* draw r in wm coordinates in all the windows from cur to top *)
						если DrawWindow(cur) то
							cb := cur.bounds;
							nr := r; WMRectangles.ClipRect(nr, cb);
							dsr.l := округлиВниз((nr.l - range.l) * fx) ; dsr.t := округлиВниз((nr.t - range.t) * fy);
							dsr.r := округлиВниз((nr.r - range.l) * fx + 0.5); dsr.b := округлиВниз((nr.b - range.t) * fy + 0.5);
							если  (~WMRectangles.RectEmpty(dsr)) и (WMRectangles.Intersect(dsr, deviceRect)) то
								canvas.SetClipRect(dsr);  (* Set clip rect to dsr, clipped at current window *)
								(* range can not be factored out because of rounding *)
								canvas.ClipRectAsNewLimits(округлиВниз((cur.bounds.l - range.l) * fx), округлиВниз((cur.bounds.t - range.t) * fy));
								width := округлиВниз((cb.r - range.l)* fx) - округлиВниз((cb.l - range.l) * fx);
								height := округлиВниз((cb.b - range.t) * fy) - округлиВниз((cb.t - range.t) * fy);
								если navig то
									cur.Draw(canvas, width, height, WMGraphics.ScaleBox);
								иначе
									cur.Draw(canvas, width, height, WMGraphics.ScaleBilinear);
								всё;
								canvas.RestoreState(state);
							всё;
						всё;
						cur := cur.next
					кц;
				всё
			кон InternalDraw;

		нач
			если (canvas # НУЛЬ) то
				cur := top;
				если (cur # НУЛЬ) и (~WMRectangles.RectEmpty(r)) то
					если DrawWindow(cur) то
						wr := cur.bounds;
						если ~WMRectangles.IsContained(wr, r) то
							если WMRectangles.Intersect(r, wr) то
								(* r contains wr calculate r -  wr and recursively call for resulting rectangles*)
								(* calculate top rectangle *)
								если wr.t > r.t то WMRectangles.SetRect(nr, r.l, r.t, r.r, wr.t); Draw(nr, cur.prev) всё;
								(* calculate bottom rectangle *)
								если wr.b < r.b то WMRectangles.SetRect(nr, r.l, wr.b, r.r, r.b); Draw(nr, cur.prev) всё;
								(* calculate left rectangle *)
								если wr.l > r.l то WMRectangles.SetRect(nr, r.l, матМаксимум(r.t, wr.t), wr.l, матМинимум(r.b, wr.b)); Draw(nr, cur.prev) всё;
								(* calculate left rectangle *)
								если wr.r < r.r то WMRectangles.SetRect(nr, wr.r, матМаксимум(r.t, wr.t), r.r, матМинимум(r.b, wr.b)); Draw(nr, cur.prev) всё;
								(* calculate overlapping *)
								nr := r; WMRectangles.ClipRect(nr, wr);
								если ~WMRectangles.RectEmpty(nr) то InternalDraw(nr, cur) всё
							иначе Draw(r, cur.prev)
							всё
						иначе InternalDraw(r, cur)
						всё
					иначе
						Draw(r, cur.prev);
					всё;
				всё;
			всё;
			если (onDrawn # НУЛЬ) то onDrawn(); всё;
		кон Draw;

	кон ViewPort;

тип

	Navigator = окласс(WMComponents.VisualComponent)
	перем
		viewPort : ViewPort;

		selectedWindow : WMWindowManager.Window;

		timer : Kernel.Timer;
		alive, dead, refresh, doRefresh : булево;

		offsetX, offsetY : размерМЗ;
		lastX, lastY : размерМЗ;

		проц &{перекрыта}Init*;
		перем style : WMWindowManager.WindowStyle;
		нач
			Init^;
			нов(viewPort);
			нов(timer);
			alive := истина; dead := ложь; refresh := истина; doRefresh := ложь;
			manager.AddView(viewPort);
			style := manager.GetStyle();
			если (style # НУЛЬ) то
				fillColor.Set(style.desktopColor);
			всё;
		кон Init;

		проц {перекрыта}Finalize*;
		нач
			Finalize^;
			нач {единолично} alive := ложь; кон;
			нач {единолично} дождись(dead); кон;
			manager.RemoveView(viewPort);
		кон Finalize;

		проц {перекрыта}PropertyChanged*(sender, data : динамическиТипизированныйУкль);
		нач
			PropertyChanged^(sender, data);
			если (data = bounds) то
				RecacheProperties;
			всё;
		кон PropertyChanged;

		проц {перекрыта}RecacheProperties*;
		нач
			RecacheProperties^;
			viewPort.ReInit(bounds.GetWidth(), bounds.GetHeight(), Displays.color8888, Refresh);
			viewPort.manager.RefreshView(viewPort);
			Invalidate;
		кон RecacheProperties;

		проц {перекрыта}PointerLeave*;
		нач
			PointerLeave^;
		кон PointerLeave;

		проц {перекрыта}PointerDown*(x, y: размерМЗ; keys: мнвоНаБитахМЗ);
		перем xg, yg : размерМЗ; rect : WMRectangles.Rectangle; title : Строки8.уСтрока;
		нач
			PointerDown^(x, y, keys);
			если (0 в keys) то
				viewPort.GetWMPosition(x, y, xg, yg);
				selectedWindow := manager.GetPositionOwner(xg, yg);
				если (selectedWindow # НУЛЬ) то
					title := selectedWindow.GetTitle();
					если (title # НУЛЬ) и ((title^ = "Old background") или (title^ = "New background")) то selectedWindow := НУЛЬ; возврат; всё;
					manager.lock.AcquireRead;
					offsetX := (xg - selectedWindow.bounds.l);
					offsetY := (yg - selectedWindow.bounds.t);
					manager.lock.ReleaseRead;
				иначе
					offsetX := 0; offsetY := 0;
				всё;
			аесли (keys = {1}) то
				manager.GetPopulatedArea(rect);
				manager.lock.AcquireWrite;
				viewPort.SetRange(rect.l, rect.t, rect.r - rect.l, rect.b - rect.t, истина);
				manager.lock.ReleaseWrite;
			всё;
		кон PointerDown;

		проц {перекрыта}PointerMove*(x, y: размерМЗ; keys: мнвоНаБитахМЗ);
		перем xg, yg : размерМЗ;
		нач
			lastX := x; lastY := y;
			PointerMove^(x, y, keys);
			если (0 в keys) то
				если (selectedWindow # НУЛЬ) то
					viewPort.GetWMPosition(x, y, xg, yg);
					manager.SetWindowPos(selectedWindow, xg - offsetX, yg - offsetY);
				всё;
			всё;
		кон PointerMove;

		проц {перекрыта}WheelMove*(dz: размерМЗ);
		перем xg, yg : размерМЗ;
		нач
			WheelMove^(dz);
			viewPort.GetWMPosition(lastX, lastY, xg, yg);
			viewPort.ChangeZoom(dz, xg, yg);
		кон WheelMove;

		проц {перекрыта}PointerUp*(x, y: размерМЗ; keys: мнвоНаБитахМЗ);
		нач
			PointerUp^(x, y, keys);
			selectedWindow := НУЛЬ;
		кон PointerUp;

		проц Refresh;
		нач {единолично}
			refresh := истина;
		кон Refresh;

		проц {перекрыта}Draw*(canvas : WMGraphics.Canvas);
		перем r0, r1, res : WMWindowManager.RealRect; rect : WMRectangles.Rectangle;
		нач
			если (viewPort.backbuffer.width = bounds.GetWidth()) и (viewPort.backbuffer.height = bounds.GetHeight()) то
				canvas.DrawImage(0, 0, viewPort.backbuffer,WMGraphics.ModeSrcOverDst);
			иначе
				canvas.ScaleImage(viewPort.backbuffer, WMRectangles.MakeRect(0, 0, viewPort.backbuffer.width, viewPort.backbuffer.height),
					WMRectangles.MakeRect(0, 0, bounds.GetWidth(), bounds.GetHeight()), WMGraphics.ModeSrcOverDst, 1)
			всё;
			r0 := viewport.range;
			r1 := viewPort.range;
			если (r0.l > r1.l) то res.l := r0.l; иначе res.l := r1.l; всё;
			если (r0.t > r1.t) то res.t := r0.t; иначе res.t := r1.t; всё;
			если (r0.r < r1.r) то res.r := r0.r; иначе res.r := r1.r; всё;
			если (r0.b < r1.b) то res.b := r0.b; иначе res.b := r1.b; всё;

			rect := WMRectangles.MakeRect(округлиВниз(viewPort.fx * (res.l - r1.l)), округлиВниз(viewPort.fy * (res.t - r1.t)), округлиВниз(viewPort.fx * (res.r - r1.l)), округлиВниз(viewPort.fy * (res.b - r1.t)));
			WMGraphicUtilities.DrawRect(canvas, rect, цел32(0FF0000FFH), WMGraphics.ModeCopy);
		кон Draw;

	нач {активное}
		manager.lock.AcquireWrite;
		viewPort.SetRange(-1280, -1024, 2560, 2048, ложь);
		manager.lock.ReleaseWrite;
		manager.RefreshView(viewPort);
		Invalidate;
		нц
			нач {единолично}
				дождись(refresh или ~alive);
				doRefresh := refresh;
				refresh := ложь;
			кон;
			timer.Sleep(30);
			если ~alive то прервиЦикл; всё;
			если doRefresh то
				doRefresh := ложь;
				 Invalidate;
			всё;
		кц;
		нач {единолично} dead := истина; кон;
	кон Navigator;

тип

	Window = окласс(WMComponents.FormWindow)

		проц {перекрыта}Close*;
		нач
			Close^;
			window := НУЛЬ;
		кон Close;

	кон Window;

перем
	manager : WMWindowManager.WindowManager;
	viewport : WMWindowManager.ViewPort;
	window : Window;

проц GenNavigator*() : XML.Element;
перем n : Navigator;
нач
	нов(n); возврат n;
кон GenNavigator;

проц Init;
нач
	manager := WMWindowManager.GetDefaultManager();
	viewport := WMWindowManager.GetDefaultView();
кон Init;

проц Open*;
перем n : Navigator;
нач {единолично}
	если (window = НУЛЬ) то
		нов(n); n.alignment.Set(WMComponents.AlignClient);
		нов(window, 400, 200, истина);
		window.SetContent(n);
		WMWindowManager.ExtAddViewBoundWindow(window, 20, 20, НУЛЬ,
			{WMWindowManager.FlagFrame, WMWindowManager.FlagStayOnTop, WMWindowManager.FlagNavigation, WMWindowManager.FlagHidden});
	всё;
кон Open;

проц Close*;
нач {единолично}
	если (window # НУЛЬ) то window.Close; window := НУЛЬ; всё;
кон Close;

нач
	Modules.InstallTermHandler(Close);
	Init;
кон WMNavigator.

WMNavigator.Open ~

WMNavigator.Close ~

System.Free WMNavigator ~

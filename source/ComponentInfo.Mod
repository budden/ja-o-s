модуль ComponentInfo; (** AUTHOR "staubesv/TF"; PURPOSE "Component Information"; *)

использует
	Потоки, Commands, Строки8, ЛогЯдра, XML, XMLObjects, WMWindowManager, WMProperties, WMEvents, WMComponents;

конст
	MaxNofWindows = 100;

тип
	Windows = массив MaxNofWindows из WMWindowManager.Window;

	Statistics = окласс
	перем
		nofComponents, nofVisualComponents, nofOtherElements, nofEventSources, nofEventListeners : размерМЗ;
		nofBooleanProperties, nofInt32Properties, nofColorProperties, nofRectangleProperties,
		nofStringProperties, nofOtherProperties : размерМЗ;

		проц &Reset;
		нач
			nofComponents := 0; nofVisualComponents := 0;  nofOtherElements := 0; nofEventSources := 0; nofEventListeners := 0;
			nofBooleanProperties := 0; nofInt32Properties := 0; nofColorProperties := 0; nofRectangleProperties := 0;
			nofStringProperties := 0; nofOtherProperties := 0;
		кон Reset;

		проц Add(stats : Statistics);
		нач
			утв(stats # НУЛЬ);
			увел(nofComponents, stats.nofComponents);
			увел(nofVisualComponents, stats.nofVisualComponents);
			увел(nofOtherElements, stats.nofOtherElements);
			увел(nofEventSources, stats.nofEventSources);
			увел(nofEventListeners, stats.nofEventListeners);
			увел(nofBooleanProperties, stats.nofBooleanProperties);
			увел(nofInt32Properties, stats.nofInt32Properties);
			увел(nofColorProperties, stats.nofColorProperties);
			увел(nofRectangleProperties, stats.nofRectangleProperties);
			увел(nofStringProperties, stats.nofStringProperties);
			увел(nofOtherProperties, stats.nofOtherProperties);
		кон Add;

		проц Show(out : Потоки.Писарь);
		нач
			утв(out # НУЛЬ);
			out.пСтроку8(" elements: "); out.пЦел64(nofComponents + nofVisualComponents + nofOtherElements, 0);  out.пВК_ПС;
			out.пСтроку8("   Components: "); out.пЦел64(nofComponents, 0); out.пВК_ПС;
			out.пСтроку8("   VisualComponents: "); out.пЦел64(nofVisualComponents, 0); out.пВК_ПС;
			out.пСтроку8("   Other: "); out.пЦел64(nofOtherElements, 0); out.пВК_ПС;
			out.пСтроку8("event sources: "); out.пЦел64(nofEventSources, 0); out.пВК_ПС;
			out.пСтроку8("event listeners: "); out.пЦел64(nofEventListeners, 0); out.пВК_ПС;
			out.пСтроку8("properties: ");
			out.пЦел64(nofBooleanProperties + nofInt32Properties + nofColorProperties + nofRectangleProperties +
				nofStringProperties + nofOtherProperties, 0);
			out.пВК_ПС;
			out.пСтроку8("   Boolean: "); out.пЦел64(nofBooleanProperties, 0); out.пВК_ПС;
			out.пСтроку8("   Int32: "); out.пЦел64(nofInt32Properties, 0); out.пВК_ПС;
			out.пСтроку8("   Color: "); out.пЦел64(nofColorProperties, 0); out.пВК_ПС;
			out.пСтроку8("   Rectangle: "); out.пЦел64(nofRectangleProperties, 0); out.пВК_ПС;
			out.пСтроку8("   String: "); out.пЦел64(nofStringProperties, 0); out.пВК_ПС;
			out.пСтроку8("   Other: "); out.пЦел64(nofOtherProperties, 0); out.пВК_ПС;
		кон Show;

	кон Statistics;

проц DumpComponent*(obj : динамическиТипизированныйУкль; out : Потоки.Писарь);

	проц DumpProperties(pl : WMProperties.PropertyList);
	перем i : размерМЗ; pa : WMProperties.PropertyArray; p :WMProperties.Property; s : Строки8.уСтрока;
		st : Потоки.ПисарьВСтроку8ФиксированногоРазмера;
		value : массив 100 из симв8;
	нач
		pa := pl.Enumerate();
		нцДля i := 0 до длинаМассива(pa) - 1 делай
			p := pa[i];
			если p = НУЛЬ то out.пСтроку8("<property is nil>"); out.пВК_ПС;
			иначе
				out.пСтроку8("Name : "); s := p.GetName(); если s # НУЛЬ то out.пСтроку8(s^) иначе out.пСтроку8("<NIL>") всё;
				нов(st, 100); p.ToStream(st); st.ДайПрочитанное˛сколькоПоместитсяИСимвол0(value);
				out.пСтроку8(" Value : "); out.пСтроку8(value); если p.GetIsDefault() то out.пСтроку8("(default)") всё;
				out.пСтроку8(" Info : "); s := p.GetInfo(); если s # НУЛЬ то out.пСтроку8(s^) иначе out.пСтроку8("<NIL>") всё;
				out.пВК_ПС
			всё
		кц
	кон DumpProperties;

	проц DumpEvents(el : WMEvents.EventSourceList);
	перем i : размерМЗ; ea : WMEvents.EventSourceArray; e : WMEvents.EventSource; s : Строки8.уСтрока;
	нач
		ea := el.Enumerate();
		нцДля i := 0 до длинаМассива(ea) - 1 делай
			e := ea[i];
			если e # НУЛЬ то
				out.пСтроку8("Name : "); s := e.GetName(); если s # НУЛЬ то out.пСтроку8(s^) иначе out.пСтроку8("<NIL>") всё;
				out.пСтроку8("  ");
				out.пСтроку8(" Info : "); s := e.GetInfo(); если s # НУЛЬ то out.пСтроку8(s^) иначе out.пСтроку8("<NIL>") всё;
				out.пВК_ПС
			всё;
		кц;
	кон DumpEvents;

	проц DumpListeners(el : WMEvents.EventListenerList);
	перем i : размерМЗ; ea : WMEvents.EventListenerArray; e : WMEvents.EventListenerInfo; s : Строки8.уСтрока;
	нач
		ea := el.Enumerate();
		нцДля i := 0 до длинаМассива(ea)- 1 делай
			e := ea[i];
			если e # НУЛЬ то
				out.пСтроку8("Name : "); s := e.GetName(); если s # НУЛЬ то out.пСтроку8(s^) иначе out.пСтроку8("<NIL>") всё;
				out.пСтроку8("  ");
				out.пСтроку8(" Info : "); s := e.GetInfo(); если s # НУЛЬ то out.пСтроку8(s^) иначе out.пСтроку8("<NIL>") всё;
				out.пВК_ПС
			всё;
		кц;
	кон DumpListeners;

нач
	если (out = НУЛЬ) то нов(out, ЛогЯдра.ЗапишиВПоток, 1024); всё;
	если obj = НУЛЬ то
		out.пСтроку8("NIL"); out.пВК_ПС
	иначе
		если obj суть WMComponents.Component то
			out.пСтроку8("Properties : "); out.пВК_ПС;
			DumpProperties(obj(WMComponents.Component).properties);
			out.пСтроку8("Events : "); out.пВК_ПС;
			DumpEvents(obj(WMComponents.Component).events);
			out.пСтроку8("Listeners : "); out.пВК_ПС;
			DumpListeners(obj(WMComponents.Component).eventListeners);
		иначе out.пСтроку8("is not a component"); out.пВК_ПС
		всё
	всё
кон DumpComponent;

(** Gather statistics about component incl. its subcomponents *)
проц GatherStatistics*(component : XML.Element; stats : Statistics);

	проц VisitElement(element : XML.Element);
	перем
		component : WMComponents.Component;
		events : WMEvents.EventSourceArray;
		listeners : WMEvents.EventListenerArray;
		properties : WMProperties.PropertyArray;
		enum : XMLObjects.Enumerator;
		ptr : динамическиТипизированныйУкль;
		i : размерМЗ;
	нач
		утв(element # НУЛЬ);
		если (element суть WMComponents.Component) или (element суть WMComponents.VisualComponent) то
			если (element суть WMComponents.VisualComponent) то увел(stats.nofVisualComponents); иначе увел(stats.nofComponents); всё;
			component := element (WMComponents.Component);
			events := component.events.Enumerate();
			если (events # НУЛЬ) то увел(stats.nofEventSources, длинаМассива(events)); всё;
			listeners := component.eventListeners.Enumerate();
			если (listeners # НУЛЬ) то увел(stats.nofEventListeners, длинаМассива(listeners)); всё;
			properties := component.properties.Enumerate();
			если (properties # НУЛЬ) то
				нцДля i := 0 до длинаМассива(properties)-1 делай
					если (properties[i] суть WMProperties.BooleanProperty) то увел(stats.nofBooleanProperties);
					аесли (properties[i] суть WMProperties.Int32Property) то увел(stats.nofInt32Properties);
					аесли (properties[i] суть WMProperties.ColorProperty) то увел(stats.nofColorProperties);
					аесли (properties[i] суть WMProperties.RectangleProperty) то увел(stats.nofRectangleProperties);
					аесли (properties[i] суть WMProperties.StringProperty) то увел(stats.nofStringProperties);
					иначе
						увел(stats.nofOtherProperties);
					всё;
				кц;
			всё;
		иначе
			увел(stats.nofOtherElements);
		всё;
		enum := element.GetContents();
		нцПока enum.HasMoreElements() делай
			ptr := enum.GetNext();
			если (ptr суть XML.Element) то
				VisitElement(ptr (XML.Element));
			всё;
		кц;
	кон VisitElement;

нач
	утв(stats # НУЛЬ);
	если (component # НУЛЬ) то
		VisitElement(component);
	всё;
кон GatherStatistics;

проц ShowStatistics*(context : Commands.Context); (** windowTitleMask  ~ *)
перем
	stats, tot : Statistics;
	windows : Windows; window : WMWindowManager.Window;
	windowTitle : Строки8.уСтрока;
	manager : WMWindowManager.WindowManager;
	mask : массив 128 из симв8;
	nofWindows, nofMatches,  i : размерМЗ;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(mask);
	нцДля i := 0 до длинаМассива(windows)-1 делай windows[i] := НУЛЬ; кц;
	nofWindows := 0;
	manager := WMWindowManager.GetDefaultManager();
	manager.lock.AcquireWrite;
	window := manager.GetFirst();
	нцПока (window # НУЛЬ) и (nofWindows < MaxNofWindows) делай
		windows[nofWindows] := window;
		увел(nofWindows);
		window := manager.GetNext(window);
	кц;
	manager.lock.ReleaseWrite;
	нов(tot); нов(stats);
	nofMatches := 0;
	нцДля i := 0 до nofWindows - 1 делай
		windowTitle := windows[i].GetTitle();
		если (windowTitle # НУЛЬ) то
			если Строки8.ПодходитЛиПодМаскуИмениФайла¿(mask, windowTitle^) то
				увел(nofMatches);
				context.out.пСтроку8("*** Window "); context.out.пСтроку8(windowTitle^); context.out.пСтроку8(": ");
				если (windows[i] суть WMComponents.FormWindow) то
					context.out.пВК_ПС;
					stats.Reset;
					GatherStatistics(windows[i](WMComponents.FormWindow).form, stats);
					stats.Show(context.out);
					tot.Add(stats);
				иначе
					context.out.пСтроку8("Not a FormWindow"); context.out.пВК_ПС;
				всё;
			всё;
		всё;
	кц;
	context.out.пЦел64(nofMatches, 0); context.out.пСтроку8(" matches"); context.out.пВК_ПС;
	если (nofMatches > 1) то
		context.out.пСтроку8("Statistics for all matches: "); context.out.пВК_ПС;
		tot.Show(context.out);
	всё;
кон ShowStatistics;

проц ShowPrototypes*(context : Commands.Context);
перем
	array : WMComponents.ListArray;
	propertyArray : WMProperties.PropertyArray;
	nofPrototypes, i, j : размерМЗ;
	string : Строки8.уСтрока;
нач
	nofPrototypes := 0;
	context.out.пСтроку8("Property Prototypes:"); context.out.пВК_ПС;
	array := WMComponents.propertyListList.Enumerate();
	если (array # НУЛЬ) то
		нцДля i := 0 до длинаМассива(array)-1 делай
			propertyArray := array[i].Enumerate();
			если (propertyArray # НУЛЬ) то
				увел(nofPrototypes, длинаМассива(propertyArray));
				нцДля j := 0 до длинаМассива(propertyArray)-1 делай
					string := propertyArray[j].GetName();
					если (string # НУЛЬ) то context.out.пСтроку8(string^); иначе context.out.пСтроку8("NoName"); всё;
					string := propertyArray[j].GetInfo();
					context.out.пСтроку8(" (");
					если (string # НУЛЬ) то context.out.пСтроку8(string^); иначе context.out.пСтроку8("NoInfo"); всё;
					context.out.пСтроку8(")");
					context.out.пВК_ПС;
				кц;
			всё;
		кц;
	всё;
	context.out.пСтроку8("Number of property prototypes: "); context.out.пЦел64(nofPrototypes, 0); context.out.пВК_ПС;
кон ShowPrototypes;


кон ComponentInfo.

System.Free ComponentInfo ~

ComponentInfo.ShowStatistics * ~

ComponentInfo.ShowStatistics File* ~

ComponentInfo.ShowPrototypes ~

модуль TVChannels;	(** AUTHOR "oljeger@student.ethz.ch"; PURPOSE "List of TV channels *)

использует
	Files, XMLScanner, XMLParser, XML, XMLObjects, Dates, Strings, Clock;

конст
	ChannelFile* = "TVChannels.XML";
	MaxAge = 180;	(* minutes *)	(* Maximum age for teletext data to be treated as "recent" *)

тип
	TVChannel* = окласс
		перем
			name*: массив 33 из симв8;
			freq*: цел32;
			hasTeletext*: булево;
			cachingTime*: Dates.DateTime;

		проц &Init*;
		нач
			hasTeletext := истина
		кон Init;

		(** Does the current channel have "recent" teletext data? (see MaxAge) *)
		проц HasRecentData*() : булево;
		перем
			dNow, tNow, dCache, tCache, hmNow, hmCache: цел32;
		нач
			если ~Dates.ValidDateTime(cachingTime) то
				возврат ложь
			всё;
			Clock.Get(tNow, dNow);
			Dates.DateTimeToOberon(cachingTime, dCache, tCache);
			(* Date must be identical *)
			если dNow # dCache то
				возврат ложь
			всё;
			hmNow := tNow DIV 64;
			hmCache := tCache DIV 64;
			возврат hmCache > (hmNow - MaxAge);
		кон HasRecentData;
	кон TVChannel;

	ChannelArray = укль на массив из TVChannel;

	(** Lockable Object List. *)
	ChannelList* = окласс
		перем
			list : ChannelArray;
			count : размерМЗ;
			readLock : размерМЗ;

		проц &New*;
		нач нов(list, 8); readLock := 0
		кон New;

		(** return the number of objects in the list. If count is used for indexing elements (e.g. FOR - Loop) in a multi-process
			situation, the process calling the GetCount method should call Lock before GetCount and Unlock after the
			last use of an index based on GetCount *)
		проц GetCount*():размерМЗ;
		нач
			возврат count
		кон GetCount;

		проц Grow;
		перем old: ChannelArray;
				i : размерМЗ;
		нач
			old := list;
			нов(list, длинаМассива(list)*2);
			нцДля i := 0 до count-1 делай list[i] := old[i] кц
		кон Grow;

		(** Add an object to the list. Add may block if number of calls to Lock is bigger than the number of calls to Unlock *)
		проц Add*(x : TVChannel);
		нач {единолично}
			дождись(readLock = 0);
			если count = длинаМассива(list) то Grow всё;
			list[count] := x;
			увел(count)
		кон Add;

		(** return the index of an object. In a multi-process situation, the process calling the IndexOf method should
			call Lock before IndexOf and Unlock after the last use of an index based on IndexOf.
			If the object is not found, -1 is returned *)
		проц IndexOf *(x: TVChannel) : размерМЗ;
		перем i : размерМЗ;
		нач
			i := 0 ; нцПока i < count делай если list[i] = x то возврат i всё; увел(i) кц;
			возврат -1
		кон IndexOf;

		(** Remove an object from the list. Remove may block if number of calls to Lock is bigger than the number of calls to Unlock *)
		проц Remove*(x : TVChannel);
		перем i : размерМЗ;
		нач {единолично}
			дождись(readLock = 0);
			i:=0; нцПока (i<count) и (list[i]#x) делай увел(i) кц;
			если i<count то
				нцПока (i<count-1) делай list[i]:=list[i+1]; увел(i) кц;
				умень(count);
				list[count]:=НУЛЬ
			всё
		кон Remove;

		(** Removes all objects from the list. Clear may block if number of calls to Lock is bigger than the number of calls to Unlock *)
		проц Clear*;
		перем i : размерМЗ;
		нач {единолично}
			дождись(readLock = 0);
			нцДля i := 0 до count - 1 делай list[i] := НУЛЬ кц;
			count := 0
		кон Clear;

		(** return an object based on an index. In a multi-process situation, GetItem is only safe in a locked region Lock / Unlock *)
		проц GetItem*(i: размерМЗ) : TVChannel;
		нач
			утв((i >= 0) и (i < count), 101);
			возврат list[i]
		кон GetItem;

		(** Lock prevents modifications to the list. All calls to Lock must be followed by a call to Unlock. Lock can be nested*)
		проц Lock*;
		нач {единолично}
			увел(readLock); утв(readLock > 0)
		кон Lock;

		(** Unlock removes one modification lock. All calls to Unlock must be preceeded by a call to Lock. *)
		проц Unlock*;
		нач {единолично}
			умень(readLock); утв(readLock >= 0)
		кон Unlock;
	кон ChannelList;

перем
	(** Globally accessible list that contains all available channel names together with their frequency *)
	channels*: ChannelList;

(** Load TV channels from a file *)
проц LoadChannelTable* (filename: массив из симв8);
перем
	f: Files.File; reader: Files.Reader;
	scanner: XMLScanner.Scanner; parser: XMLParser.Parser;
	xmlChannels : XML.Document; enum: XMLObjects.Enumerator;
	e : XML.Element; s : XML.String;
	p : динамическиТипизированныйУкль;
	ch : TVChannel;
нач
	если channels = НУЛЬ то
		нов(channels)
	всё;
	channels.Clear;
	xmlChannels := НУЛЬ;
	f := Files.Old (filename);
	если f # НУЛЬ то
		нов(reader, f, 0);
		нов(scanner, reader);
		нов(parser, scanner);
		xmlChannels := parser.Parse();
		если xmlChannels # НУЛЬ то
			e := xmlChannels.GetRoot();
			enum := e.GetContents();
			нцПока enum.HasMoreElements() делай
				p := enum.GetNext();
				если p суть XML.Element то
					e := p(XML.Element);
					s := e.GetName();
					если (s # НУЛЬ) и (s^ = "Channel") то
						нов(ch);
						(* read channel name *)
						s := e.GetAttributeValue ("name");
						если s # НУЛЬ то
							копируйСтрокуДо0 (s^, ch.name)
						всё;
						(* read TV frequency *)
						s := e.GetAttributeValue ("freq");
						если s # НУЛЬ то
							Strings.StrToInt (s^, ch.freq)
						всё;
						(* read optional teletext attribute *)
						s := e.GetAttributeValue ("teletext");
						если (s # НУЛЬ) и (s^ = "false") то
							ch.hasTeletext := ложь
						всё;
						channels.Add (ch)
					всё
				всё
			кц
		всё
	всё
кон LoadChannelTable;

нач
	нов(channels);
	LoadChannelTable(ChannelFile)
кон TVChannels.

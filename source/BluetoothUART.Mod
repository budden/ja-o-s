модуль BluetoothUART;	(** AUTHOR "be"; PURPOSE "HCI UART transport layer"; *)

использует
	ЛогЯдра, Потоки, Bluetooth, Objects;

(* HCI command packet format (RS-232)
	| 01X | opcode (2bytes) | total parameter length | par0 | ... | parN |; and opcode = OGF (6bit) || OCF (10bit)

	HCI ACL data packet format (RS-232)
	| 02X |

	HCI SCO data packet format (RS-232)
	| 03X |

	HCI event packet format (RS-232)
	| 04X | Event Code | total parameter length | par0 | ... | parN |

	Error message packed format (RS-232)
	| 05X |

	Negotiation packet format (RS-232)
	| 06X |
*)

конст

(*
	TraceSend = FALSE;
	TraceReceive = FALSE;
*)

	ModuleName = "[BTUART]";

	uartCommand = 01X;
	uartACLData = 02X;
	uartSCOData = 03X;
	uartEvent = 04X;

тип

	TransportLayer* = окласс(Bluetooth.TransportLayer)

		перем
			TraceReceive*, TraceSend*: булево;
			dead-: булево;

		проц &{перекрыта}Init*(name: массив из симв8; sender: Потоки.Делегат˛реализующийЗаписьВПоток; receiver: Потоки.Делегат˛реализующийЧтениеИзПотока);
		нач
			Init^(name, sender, receiver);
			нов(out, sender, 512); нов(in, receiver, 512);
			dead := ложь;
			TraceReceive := ложь;	TraceSend := ложь;
		кон Init;

		проц {перекрыта}Close*;
		нач {единолично}
			dead := истина
		кон Close;

		проц IsOpen*() :булево;
		нач {единолично}
			возврат ~dead;
		кон IsOpen;

		проц ReadACLPacket() : Bluetooth.Packet;
		перем acl: Bluetooth.ACLPacket; i : цел32;
		нач
			нов(acl);
			i := кодСимв8(in.чИДайСимв8()) + кодСимв8(in.чИДайСимв8())*100H;
			acl.handle := i  остОтДеленияНа 1000H;
			acl.PB := (i DIV 1000H) остОтДеленияНа 4;
			acl.BC := (i DIV 4000H) остОтДеленияНа 4;
			acl.len := кодСимв8(in.чИДайСимв8()) + кодСимв8(in.чИДайСимв8())*100H;
			(*ASSERT(acl.len <= Bluetooth.MaxACLDataLen);*)
			если (acl.len > Bluetooth.MaxACLDataLen) то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.ReadACLPacket: acl.len > Bluetooth.MaxACLDataLen");
				ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("acl.len= "); ЛогЯдра.пЦел64(acl.len, 0);
				ЛогЯдра.пСтроку8("; BluetoothMaxACLDataLen= "); ЛогЯдра.пЦел64(Bluetooth.MaxACLDataLen, 0);
				ЛогЯдра.пСтроку8("; in.res= 0x"); ЛогЯдра.п16ричное(in.кодВозвратаПоследнейОперации, -2);
				ЛогЯдра.пВК_ПС;
				возврат НУЛЬ;
			всё;
			нцДля i := 0 до acl.len-1 делай
				acl.data[i] := in.чИДайСимв8();
			кц;
			если (in.кодВозвратаПоследнейОперации # 0) то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.ReadACLPacket: UART failure; in.res= 0x"); ЛогЯдра.п16ричное(in.кодВозвратаПоследнейОперации, -2);
				ЛогЯдра.пВК_ПС;
				возврат НУЛЬ;
			всё;
			если TraceReceive то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.ReadACLPacket: reading ACL data");
				ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("handle= 0x");ЛогЯдра.п16ричное(acl.handle,-2);
				ЛогЯдра.пСтроку8("; packet boundary= 0x");ЛогЯдра.п16ричное(acl.PB,-2);
				ЛогЯдра.пСтроку8("; broadcast= 0x"); ЛогЯдра.п16ричное(acl.BC,-2);
				ЛогЯдра.пСтроку8("; payload length= 0x"); ЛогЯдра.пЦел64(acl.len,0);
				ЛогЯдра.пСтроку8("; acl.data= ");
				нцДля i := 0 до  acl.len-1 делай
					ЛогЯдра.пСтроку8(" 0x"); ЛогЯдра.п16ричное(кодСимв8(acl.data[i]),-2);
				кц;
				ЛогЯдра.пВК_ПС;
			всё;
			возврат acl;
		кон ReadACLPacket;

		проц ReadSCOPacket() : Bluetooth.Packet;
		перем sco: Bluetooth.SCOPacket; i : цел32;
		нач
			ЛогЯдра.пСтроку8(ModuleName);
			ЛогЯдра.пСтроку8("TransportLayer.ReadSCOPacket: uartSCOData received!! continue ....");
			ЛогЯдра.пВК_ПС;
			нов(sco);
			i := кодСимв8(in.чИДайСимв8()) + кодСимв8(in.чИДайСимв8())*100H;
			sco.handle := i остОтДеленияНа 1000H;
			sco.len := кодСимв8(in.чИДайСимв8());
			(*ASSERT(sco.len <= Bluetooth.MaxSCODataLen);*)
			если (sco.len > Bluetooth.MaxSCODataLen) то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.ReadSCOPacket: sco.len > Bluetooth.MaxSCODataLen");
				ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("sco.len= "); ЛогЯдра.пЦел64(sco.len, 0);
				ЛогЯдра.пСтроку8("; BluetoothMaxACLDataLen= "); ЛогЯдра.пЦел64(Bluetooth.MaxSCODataLen, 0);
				ЛогЯдра.пСтроку8("; in.res= 0x"); ЛогЯдра.п16ричное(in.кодВозвратаПоследнейОперации, -2);
				ЛогЯдра.пВК_ПС;
				возврат НУЛЬ;
			всё;
			нцДля i := 0 до sco.len-1 делай
				sco.data[i] := in.чИДайСимв8();
			кц;
			если (in.кодВозвратаПоследнейОперации # 0) то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.ReadSCOPacket: UART failure; in.res= 0x"); ЛогЯдра.п16ричное(in.кодВозвратаПоследнейОперации, -2);
				ЛогЯдра.пВК_ПС;
				возврат НУЛЬ;
			всё;
			если TraceReceive то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.ReadSCOPacket: reading SCO data");
				ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("handle= 0x");ЛогЯдра.п16ричное(sco.handle,-2);
				ЛогЯдра.пСтроку8(" payload length= 0x"); ЛогЯдра.пЦел64(sco.len,0);
				ЛогЯдра.пСтроку8("; sco.data= ");
				нцДля i := 0 до sco.len-1 делай
					ЛогЯдра.пСтроку8(" 0x"); ЛогЯдра.п16ричное(кодСимв8(sco.data[i]),-2);
				кц;
				ЛогЯдра.пВК_ПС;
			всё;
			возврат sco;
		кон ReadSCOPacket;

		проц ReadEventPacket() : Bluetooth.Packet;
		перем event: Bluetooth.EventPacket; i : цел32;
		нач
			нов(event);
			event.code :=in. чИДайСимв8();
			event.paramLen := кодСимв8(in.чИДайСимв8());
			(*ASSERT(event.paramLen < Bluetooth.MaxEventParamLen);*)
			если (event.paramLen > Bluetooth.MaxEventParamLen) то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.ReadEventPacket: paramLen > MaxParamLen");
				ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("paramLen= "); ЛогЯдра.пЦел64(event.paramLen, 0);
				ЛогЯдра.пСтроку8("; MaxParamLen= "); ЛогЯдра.пЦел64(Bluetooth.MaxEventParamLen, 0);
				ЛогЯдра.пСтроку8("; in.res= 0x"); ЛогЯдра.п16ричное(in.кодВозвратаПоследнейОперации, -2);
				ЛогЯдра.пВК_ПС;
			всё;
			нцДля i := 0 до event.paramLen-1 делай
				event.params[i] := in.чИДайСимв8();
			кц;
			если (in.кодВозвратаПоследнейОперации # 0) то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.ReadEventPacket: UART failure; in.res= 0x"); ЛогЯдра.п16ричное(in.кодВозвратаПоследнейОперации, -2);
				ЛогЯдра.пВК_ПС;
				возврат НУЛЬ;
			всё;
			если TraceReceive то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.ReadEventPacket: reading event Data");
				ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("event.code = 0x"); ЛогЯдра.п16ричное(кодСимв8(event.code),-2);
				ЛогЯдра.пСтроку8("; event.paramLen= "); ЛогЯдра.пЦел64(event.paramLen,0);
				ЛогЯдра.пСтроку8("; event.params= ");
				нцДля i := 0 до event.paramLen-1 делай
					ЛогЯдра.пСтроку8(" 0x"); ЛогЯдра.п16ричное(кодСимв8(event.params[i]),-2);
				кц;
				ЛогЯдра.пВК_ПС;
			всё;
			возврат event;
		кон ReadEventPacket;

		проц ReadUnknownPacket() : Bluetooth.Packet;
		перем unknown: Bluetooth.UnknownPacket; ch : симв8;
		нач
			ЛогЯдра.пСтроку8(ModuleName);
			ЛогЯдра.пСтроку8("TransportLayer.ReadUnknownPacket: unknown/invalid packet ch= 0x"); ЛогЯдра.п16ричное(кодСимв8(ch),-2);
			ЛогЯдра.пСтроку8("; in.res= 0x"); ЛогЯдра.п16ричное(in.кодВозвратаПоследнейОперации, -2);
			ЛогЯдра.пСтроку8("; in.Available()= "); ЛогЯдра.пЦел64(in.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество(), 0);
			ЛогЯдра.пВК_ПС;
			нов(unknown);
			unknown.len := 0;
			нцПока ((in.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() > 0) и (in.кодВозвратаПоследнейОперации = 0)) делай
				если(unknown.len < Bluetooth.MaxUnknownDataLen) то
					unknown.data[unknown.len] := in.чИДайСимв8();
					ЛогЯдра.пСтроку8("unknown.data["); ЛогЯдра.пЦел64(unknown.len,0); ЛогЯдра.пСтроку8("]= 0x");
					ЛогЯдра.п16ричное(кодСимв8(unknown.data[unknown.len]),-2);
					ЛогЯдра.пСтроку8("; in.res= 0x"); ЛогЯдра.п16ричное(in.кодВозвратаПоследнейОперации, -2);
					ЛогЯдра.пВК_ПС;
					увел(unknown.len);
				иначе
					ch := in.чИДайСимв8();
					ЛогЯдра.пСтроку8("discard ch= 0x"); ЛогЯдра.п16ричное(кодСимв8(ch),-2);
					ЛогЯдра.пСтроку8("; in.res= 0x"); ЛогЯдра.п16ричное(in.кодВозвратаПоследнейОперации, -2);
					ЛогЯдра.пВК_ПС;
				всё;
			кц;
			возврат unknown;
		кон ReadUnknownPacket;


		проц Read;
		перем
			ch: симв8;
			queue: Bluetooth.Queue;
			packet: Bluetooth.Packet;
		нач
			ch := in.чИДайСимв8();
			если (in.кодВозвратаПоследнейОперации # 0) то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.Read: UART failure in.res= 0x"); ЛогЯдра.п16ричное(in.кодВозвратаПоследнейОперации, -2);
				ЛогЯдра.пСтроку8("; closing layer");
				ЛогЯдра.пВК_ПС;
				Close;
				возврат;
			всё;
			если (ch = uartCommand) то 	(* HCI command packet *)
				(*HALT(100)	(* not sent by host controller *)*)
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.Read: uartCommand received! closing layer");
				ЛогЯдра.пВК_ПС;
				Close;
				возврат;
			аесли (ch = uartACLData) то	(* HCI ACL data packet *)
				packet := ReadACLPacket();
				queue := sink[Bluetooth.ACL];
			аесли (ch = uartSCOData) то	(* HCI SCO data packet *)
				packet := ReadSCOPacket();
				queue := sink[Bluetooth.SCO];
			аесли (ch = uartEvent) то		(* HCI event packet *)
				packet := ReadEventPacket();
				queue := sink[Bluetooth.Event];
			иначе 							(* unknown/invalid packet *)
				packet := ReadUnknownPacket();
				queue := sink[Bluetooth.Default];
			всё;
			если (packet = НУЛЬ) то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.Read: error while reading packet; ch= "); ЛогЯдра.пСимв8(ch);
				ЛогЯдра.пСтроку8("; in.res= "); ЛогЯдра.пЦел64(in.кодВозвратаПоследнейОперации, 0);
				ЛогЯдра.пСтроку8("; closing layer");
				ЛогЯдра.пВК_ПС;
				Close;
				возврат;
			иначе
				queue.Add(packet);
			всё;
			если TraceReceive то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.Read done.");
				ЛогЯдра.пСтроку8(" in.Available()= "); ЛогЯдра.пЦел64(in.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество(), 0);
				ЛогЯдра.пВК_ПС;
			всё;
		кон Read;

		проц GetPacketType(type: цел32; перем c: симв8): булево;
		перем res: булево;
		нач
			res := истина;
			просей type из
				| Bluetooth.Command: c := uartCommand
				| Bluetooth.ACL: c := uartACLData
				| Bluetooth.SCO: c := uartSCOData
				иначе res := ложь
			всё;
			возврат res
		кон GetPacketType;

		проц {перекрыта}Send*(type: цел32; перем data: массив из симв8; ofs, len: цел32; перем res: целМЗ);
		перем  pt: симв8; i: цел32;
		нач {единолично}
			если ~GetPacketType(type, pt) то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.Send: wrong packet type= 0x"); ЛогЯдра.п16ричное(type,-2);
				ЛогЯдра.пВК_ПС;
				res := Bluetooth.ErrInvalidParameters;
				возврат
			всё;
			если TraceSend то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.Send: packet type= 0x"); ЛогЯдра.п16ричное(кодСимв8(pt), -2);
				нцДля i := 0 до len-1 делай
					ЛогЯдра.пСимв8(" ");
					ЛогЯдра.п16ричное(кодСимв8(data[ofs+i]), -2);
				кц;
				ЛогЯдра.пВК_ПС
			всё;
			out.пСимв8(pt); out.пБайты(data, ofs, len); out.ПротолкниБуферВПоток;
			res := out.кодВозвратаПоследнейОперации
		кон Send;

		проц {перекрыта}Send1H*(type: цел32; перем hdr: массив из симв8; hdrlen: цел32; перем data: массив из симв8; ofs, len: цел32; перем res: целМЗ);
		перем pt: симв8; i: цел32;
		нач
			если ~GetPacketType(type, pt) то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.Send1H: wrong packet type= 0x"); ЛогЯдра.п16ричное(type,-2);
				ЛогЯдра.пВК_ПС;
				res := Bluetooth.ErrInvalidParameters;
				возврат
			всё;
			если TraceSend то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.Send1H: packet type= 0x"); ЛогЯдра.п16ричное(кодСимв8(pt), -2);
				нцДля i := 0 до hdrlen-1 делай
					 ЛогЯдра.пСимв8(" "); ЛогЯдра.п16ричное(кодСимв8(hdr[i]), -2);
				кц;
				нцДля i := 0 до len-1 делай
					ЛогЯдра.пСимв8(" "); ЛогЯдра.п16ричное(кодСимв8(data[ofs+i]), -2);
				кц;
				ЛогЯдра.пВК_ПС
			всё;
			out.пСимв8(pt); out.пБайты(hdr, 0, hdrlen); out.пБайты(data, ofs, len); out.ПротолкниБуферВПоток;
			res := out.кодВозвратаПоследнейОперации
		кон Send1H;

		проц {перекрыта}Send2H*(type: цел32; перем hdr1: массив из симв8; hdr1len: цел32;
								перем hdr2: массив из симв8; hdr2len: цел32;
								перем data: массив из симв8; ofs, len: цел32; перем res: целМЗ);
		перем pt: симв8; i: цел32;
		нач
			если ~GetPacketType(type, pt) то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.Send2H: wrong packet type= 0x"); ЛогЯдра.п16ричное(type,-2);
				ЛогЯдра.пВК_ПС;
				res := Bluetooth.ErrInvalidParameters;
				возврат
			всё;
			если TraceSend то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("TransportLayer.Send2H: packet type= 0x"); ЛогЯдра.п16ричное(кодСимв8(pt), -2);
				нцДля i := 0 до hdr1len-1 делай
					 ЛогЯдра.пСимв8(" "); ЛогЯдра.п16ричное(кодСимв8(hdr1[i]), -2);
				кц;
				нцДля i := 0 до hdr2len-1 делай
					ЛогЯдра.пСимв8(" "); ЛогЯдра.п16ричное(кодСимв8(hdr2[ofs+i]), -2);
				кц;
				нцДля i := 0 до len-1 делай
					ЛогЯдра.пСимв8(" "); ЛогЯдра.п16ричное(кодСимв8(data[ofs+i]), -2);
				кц;
				ЛогЯдра.пВК_ПС
			всё;
			out.пСимв8(pt); out.пБайты(hdr1, 0, hdr1len); out.пБайты(hdr2, 0, hdr2len); out.пБайты(data, ofs, len); out.ПротолкниБуферВПоток;
			res := out.кодВозвратаПоследнейОперации
		кон Send2H;

	нач {активное}
		Objects.SetPriority(3);
		нцДо
			Read;
		кцПри dead
	кон TransportLayer;

кон BluetoothUART.

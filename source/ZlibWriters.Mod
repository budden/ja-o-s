(* ETH Oberon, Copyright 2001 ETH Zuerich Institut fuer Computersysteme, ETH Zentrum, CH-8092 Zuerich.
Refer to the "General ETH Oberon System Source License" contract available at: http://www.oberon.ethz.ch/ *)

модуль ZlibWriters;	(** Stefan Walthert  **)

использует
	Files, Zlib, ZlibBuffers, ZlibDeflate;

конст
	(** result codes **)
	Ok* = ZlibDeflate.Ok; StreamEnd* = ZlibDeflate.StreamEnd;
	StreamError* = ZlibDeflate.StreamError; DataError* = ZlibDeflate.DataError; BufError* = ZlibDeflate.BufError;

	(** flush values **)
	NoFlush* = ZlibDeflate.NoFlush;
	SyncFlush* = ZlibDeflate.SyncFlush;
	FullFlush* = ZlibDeflate.FullFlush;

	(** compression levels **)
	DefaultCompression* = ZlibDeflate.DefaultCompression; NoCompression* = ZlibDeflate.NoCompression;
	BestSpeed* = ZlibDeflate.BestSpeed; BestCompression* = ZlibDeflate.BestCompression;

	(** compression strategies **)
	DefaultStrategy* = ZlibDeflate.DefaultStrategy; Filtered* = ZlibDeflate.Filtered; HuffmanOnly* = ZlibDeflate.HuffmanOnly;

	BufSize = 10000H;

тип
	(** structure for writing deflated data in a file **)
	Writer* = запись
		res-: цел32;	(** current stream state **)
		flush-: цел8;	(** flush strategy **)
		wrapper-: булево;	(** if set, zlib header and checksum are generated **)
		r: Files.Rider;	(* file rider *)
		pos: цел32;	(* logical position in uncompressed input stream *)
		crc32-: цел32;	(** crc32 of uncompressed data **)
		out: укль на массив BufSize из симв8;	(* output buffer space *)
		s: ZlibDeflate.Stream	(* compression stream *)
	кон;


(** change deflate parameters within the writer **)
проц SetParams*(перем w: Writer; level, strategy, flush: цел8);
нач
	если flush в {NoFlush, SyncFlush, FullFlush} то
		ZlibDeflate.SetParams(w.s, level, strategy);
		w.flush := flush;
		w.res := w.s.res
	иначе
		w.res := StreamError
	всё
кон SetParams;

(** open writer on a Files.Rider **)
проц Open*(перем w: Writer; level, strategy, flush: цел8; wrapper: булево; r: Files.Rider);
нач
	если flush в {NoFlush, SyncFlush, FullFlush} то
		w.flush := flush;
		w.wrapper := wrapper;
		ZlibDeflate.Open(w.s, level, strategy, ложь);
		если w.s.res = Ok то
			нов(w.out); ZlibBuffers.Init(w.s.out, w.out^, 0, BufSize, BufSize);
			w.crc32 := Zlib.CRC32(0, w.out^, -1, -1);
			w.r := r;
			w.res := Ok
		иначе
			w.res := w.s.res
		всё
	иначе
		w.res := StreamError
	всё
кон Open;

(** write specified number of bytes from buffer into and return number of bytes actually written **)
проц WriteBytes*(перем w: Writer; перем buf: массив из симв8; offset, len: размерМЗ; перем written: размерМЗ);
нач
	утв((0 <= offset) и (0 <= len) и (len <= длинаМассива(buf)), 110);
	если ~w.s.open то
		w.res := StreamError; written := 0
	аесли (w.res < Ok) или (len <= 0) то
		written := 0
	иначе
		ZlibBuffers.Init(w.s.in, buf, offset, len, len);
		нцПока (w.res = Ok) и (w.s.in.avail # 0) делай
			если (w.s.out.avail = 0) то
				w.r.file.WriteBytes(w.r, w.out^, 0, BufSize);
				ZlibBuffers.Rewrite(w.s.out)
			всё;
			если w.res = Ok то
				ZlibDeflate.Deflate(w.s, w.flush);
				w.res := w.s.res
			всё
		кц;
		w.crc32 := Zlib.CRC32(w.crc32, buf, offset, len - w.s.in.avail);
		written := len - w.s.in.avail;
	всё;
кон WriteBytes;

(** write byte **)
проц Write*(перем w: Writer; ch: симв8);
перем
	buf: массив 1 из симв8;
	written: размерМЗ;
нач
	buf[0] := ch;
	WriteBytes(w, buf, 0, 1, written)
кон Write;

(** close writer **)
проц Close*(перем w: Writer);
перем
	done: булево;
	len: размерМЗ;
нач
	утв(w.s.in.avail = 0, 110);
	done := ложь;
	нц
		len := BufSize - w.s.out.avail;
		если len # 0 то
			w.r.file.WriteBytes(w.r, w.out^, 0, len);
			ZlibBuffers.Rewrite(w.s.out)
		всё;
		если done то прервиЦикл всё;
		ZlibDeflate.Deflate(w.s, ZlibDeflate.Finish);
		если (len = 0) и (w.s.res = BufError) то
			w.res := Ok
		иначе
			w.res := w.s.res
		всё;
		done := (w.s.out.avail # 0) или (w.res = StreamEnd);
		если (w.res # Ok) и (w.res # StreamEnd) то прервиЦикл всё
	кц;
	ZlibDeflate.Close(w.s);
	w.res := w.s.res
кон Close;

(** compress srclen bytes from src to dst with specified level and strategy. dstlen returns how many bytes have been written. **)
проц Compress*(перем src, dst: Files.Rider; srclen: цел32; перем dstlen: цел32; level, strategy: цел8; перем crc32: цел32; перем res: целМЗ);
перем
	w: Writer; buf: массив BufSize из симв8; totWritten, written, read: размерМЗ;
нач
	Open(w, level, strategy, NoFlush, ложь, dst);
	если w.res = Ok то
		totWritten := 0;
		нцДо
			если (srclen - totWritten) >= BufSize то read := BufSize
			иначе read := srclen - totWritten
			всё;
			src.file.ReadBytes(src, buf, 0, read);
			WriteBytes(w, buf, 0, read - src.res, written);
			увел(totWritten, written)
		кцПри (w.res # Ok) или (totWritten >= srclen);
		Close(w);
		crc32 := w.crc32;
		dstlen := (w.r.file.Pos(w.r) - dst.file.Pos(dst))(цел32);
	всё;
	res := w.res
кон Compress;


кон ZlibWriters.

модуль WMPerfMonPluginDisks; (** AUTHOR "staubesv"; PURPOSE "Performance Monitor disk transfer rate plugin"; *)
(**
 * History:
 *
 *	16.02.2006	First Release (staubesv)
 *	23.06.2006	Adapted to WMPerfMonPlugins (staubesv)
 *	27.02.2007	Remove plugins when unloading module, distinct bytesRead/bytesWritten (staubesv)
 *	26.03.2007	Added NnofReads, NnofWrites, NnofOthers, NnofErrors, AvgBlockSize (staubesv)
 *)

использует
	WMPerfMonPlugins,
	ЛогЯдра, Disks, Plugins, Modules;

конст
	PluginName = "DiskDevice";
	ModuleName = "WMPerfMonPluginDisks";

тип

	DiskParameter = укль на запись(WMPerfMonPlugins.Parameter)
		dev : Disks.Device;
	кон;

	DiskPlugin = окласс(WMPerfMonPlugins.Plugin)
	перем
		dev : Disks.Device;

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		перем ds : WMPerfMonPlugins.DatasetDescriptor;
		нач
			p.name := PluginName; p.description := "Disk device statistics";
			сам.dev := p(DiskParameter).dev;
			WMPerfMonPlugins.GetNameDesc(dev, p.devicename);
			p.modulename := ModuleName;
			p.autoMax := истина; p.unit := "KB"; p.perSecond := истина; p.minDigits := 5; p.showSum := истина;

			нов(ds, 8);
			ds[0].name := "TotalKB";
			ds[1].name := "ReadKB";
			ds[2].name := "WrittenKB";
			ds[3].name := "AvgBlockSizeBytes";
			ds[4].name := "NnofReads";
			ds[5].name := "NnofWrites";
			ds[6].name := "NnofOthers";
			ds[7].name := "NnofErrors";
			p.datasetDescriptor := ds;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		перем read, written, nofReads, nofWrites, nofTot, nofErrors : цел64; total : вещ32;
		нач
			read := dev.NbytesRead; nofReads := dev.NnofReads;
			written := dev.NbytesWritten; nofWrites := dev.NnofWrites;
			total := read + written;
			nofErrors := dev.NnofErrors;
			nofTot := nofReads + nofWrites - nofErrors;
			dataset[0] := total / 1024;
			dataset[1] := read / 1024;
			dataset[2] := written / 1024;
			dataset[3] := total / nofTot;
			dataset[4] := nofReads;
			dataset[5] := nofWrites;
			dataset[6] := dev.NnofOthers;
			dataset[7] := nofErrors;
		кон UpdateDataset;

	кон DiskPlugin;

проц AddPlugin(disk : Disks.Device);
перем par : DiskParameter; plugin : DiskPlugin;
нач {единолично}
	нов(par); par.dev := disk; нов(plugin, par);
кон AddPlugin;

проц RemovePlugin(disk : Disks.Device);
перем devicename : WMPerfMonPlugins.DeviceName;
нач {единолично}
	WMPerfMonPlugins.GetNameDesc(disk, devicename);
	WMPerfMonPlugins.updater.RemoveByName(PluginName, devicename);
кон RemovePlugin;

проц EventHandler(event : целМЗ; plugin : Plugins.Plugin);
нач
	если event = Plugins.EventAdd то
		AddPlugin(plugin (Disks.Device))
	аесли event = Plugins.EventRemove то
		RemovePlugin(plugin (Disks.Device));
	всё;
кон EventHandler;

проц InitPlugins;
перем table : Plugins.Table; i : размерМЗ; res: целМЗ;
нач
	если Disks.Stats то
		Disks.registry.AddEventHandler(EventHandler, res); (* ignore res *)
		Disks.registry.GetAll(table);
		если table # НУЛЬ то нцДля i := 0 до длинаМассива(table)-1 делай AddPlugin(table[i] (Disks.Device)); кц; всё;
	иначе ЛогЯдра.пСтроку8("WMPerfMonPluginDisks: Disks.PerformanceMonitoring is FALSE."); ЛогЯдра.пВК_ПС;
	всё;
кон InitPlugins;

проц Install*;
кон Install;

проц Cleanup;
перем res : целМЗ;
нач
	если Disks.Stats то
		Disks.registry.RemoveEventHandler(EventHandler, res);
		WMPerfMonPlugins.updater.RemoveByModuleName(ModuleName);
	всё;
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
	InitPlugins;
кон WMPerfMonPluginDisks.

WMPerfMonPluginDisks.Install ~ System.Free WMPerfMonPluginDisks ~

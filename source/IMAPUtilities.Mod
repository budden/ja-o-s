модуль IMAPUtilities; (** AUTHOR "retmeier"; PURPOSE "some useful procedures for the IMAP client"; *)

(*! to do: IMAPUtilities.getRFC822Date seems one weekday off, on thursday it says: Fri, 28 May 2015 21:15:04 +0100*)

использует
	Dates, Strings, Classes := TFClasses, Потоки, ЛогЯдра, UTF8Strings, Texts, TextUtilities, WMEditors, UCS32;

конст
	CR = 0DX; LF = 0AX; SP = 32;

перем
	base64Table: массив 128 из цел32;
	index: цел32;

тип

	String = Strings.String;

	Address* = укль на запись (** according to RFC 2060 *)
		realName*: String;
		namePart*: String; (** in front of @ *)
		domainPart*: String (** behind @: namePart@domainPart *)
	кон;

проц ParseAddresses*(string: String; перем addresses: Classes.List);
перем
	address: Address;
	r: Потоки.ЧтецИзСтроки;
	w: Потоки.Писарь;
	buffer: Strings.Buffer;
	s: String;
	c: симв8;
	i, j: размерМЗ;
нач
	нов(addresses);
	нов(buffer, 16);
	w := buffer.GetWriter();
	нов(r, Strings.Length(string^));
	r.ПримиСтроку8ДляЧтения(string^);
	r.ПропустиБелоеПоле();
	r.чСимв8(c);
	нцПока(c # 0X) делай
		нов(address);
		нцПока (c # ",") и (c # 0X) делай
			w.пСимв8(c);
			r.чСимв8(c);
		кц;
		s := buffer.GetString();
		i := 0;
		нцПока (s^[i] # 0X) и (s^[i] # "@") и (s^[i] # "<") делай
			увел(i);
		кц;
		если s^[i] = 0X то
			ЛогЯдра.пСтроку8("Address Format invalid"); ЛогЯдра.пВК_ПС();
			возврат;
		всё;

		нов(address);
		если s^[i] = "@" то
			address.realName := Strings.NewString("");
			нов(address.namePart, i+1);
			Strings.Copy(s^, 0, i, address.namePart^);
			j := Strings.Length(s^) - i;
			нов(address.domainPart, j);
			Strings.Copy(s^, i+1, j-1, address.domainPart^);
		иначе (* s^[i] = "<" *)
			нов(address.realName, i+1);
			Strings.Copy(s^, 0, i, address.realName^);
			Strings.Trim(address.realName^, " ");
			j := i;
			нцПока (s^[j] # 0X) и (s^[j] # "@") делай
				увел(j);
			кц;
			если s^[j] = 0X то
				ЛогЯдра.пСтроку8("Address Format invalid"); ЛогЯдра.пВК_ПС();
				возврат;
			всё;
			нов(address.namePart, j-i);
			Strings.Copy(s^, i+1, j-i-1, address.namePart^);
			i := j;
			нцПока(s^[i] # 0X) и (s^[i] # ">") делай
				увел(i);
			кц;
			если s^[i] = 0X то
				ЛогЯдра.пСтроку8("Address Format invalid"); ЛогЯдра.пВК_ПС();
				возврат;
			всё;
			нов(address.domainPart, i-j);
			Strings.Copy(s^, j + 1, i - j - 1, address.domainPart^);
		всё;
		addresses.Add(address);
		buffer.Clear();
		r.ПропустиБелоеПоле();
		r.чСимв8(c);
	кц;
кон ParseAddresses;

проц AddressesToString*(list: Classes.List; перем string: String);
перем
	address: Address;
	p: динамическиТипизированныйУкль;
	buffer: Strings.Buffer;
	w: Потоки.Писарь;
	i: размерМЗ;
	s: String;
нач
	нов(buffer, 16);
	w := buffer.GetWriter();
	i := 0;
	нцПока i < list.GetCount() делай
		p := list.GetItem(i);
		address := p(Address);
		AddressToString(address, s);
		w.пСтроку8(s^);
		w.пСтроку8(", ");
		увел(i);
	кц;
	string := buffer.GetString();
	Strings.Trim(string^, " ");
	Strings.TrimRight(string^, ",");
кон AddressesToString;

проц AddressToString*(address: Address; перем string: String);
перем
	buffer: Strings.Buffer;
	w: Потоки.Писарь;
нач
	нов(buffer, 16);
	w := buffer.GetWriter();

	если (address.realName # НУЛЬ) и (StringLength(address.realName^) > 0) то
		w.пСтроку8(address.realName^);
		w.пСтроку8(" <");
	всё;
	w.пСтроку8(address.namePart^);
	w.пСтроку8("@");
	w.пСтроку8(address.domainPart^);
	если (address.realName # НУЛЬ) и (StringLength(address.realName^) > 0) то
		w.пСтроку8(">");
	всё;
	string := buffer.GetString();
кон AddressToString;

(* Strings that are longer then a certain limit must be passed as VAR-parameters so Strings.Length cannot be used. *)
проц StringLength*(конст string: массив из симв8): размерМЗ;
перем len: размерМЗ;
нач
	len := 0; нцПока (string[len] # 0X) делай увел(len) кц;
	возврат len
кон StringLength;

(* Strings that are longer then a certain limit must be passed as VAR-parameters so Strings.Copy cannot be used. *)
проц StringCopy*(перем s: массив из симв8; index, count: размерМЗ; перем result: массив из симв8);
перем i, l: размерМЗ;
нач
	i := 0; l := длинаМассива(result)-1;
	нцПока (i < count) и (i < l) делай
		result[i] := s[index+i];
		увел(i)
	кц;
	result[i] := 0X
кон StringCopy;

(* Returns TRUE if string contains subString as a sub-String *)
проц StringContains*(string, subString: String): булево;
перем
	pos: размерМЗ;
нач
	нцДля pos := 0 до StringLength(string^) - 1 шаг 1 делай
		если StringStartsWith(subString^, pos, string^) то
			возврат истина;
		всё;
	кц;
	возврат ложь;
кон StringContains;

(* Tests if string s starts with the specified prefix beginning a specified index *)
проц StringStartsWith*(перем prefix : массив из симв8; toffset : размерМЗ; перем s : массив из симв8) : булево;
перем
	lenString, lenPrefix, i : размерМЗ;
нач
	lenString := StringLength(s);
	lenPrefix := StringLength(prefix);
	если (toffset < 0) или (toffset > lenString - lenPrefix) то
		возврат ложь;
	всё;
	нцДля i := 0 до lenPrefix-1 делай
		если prefix[i] # s[toffset + i] то возврат ложь; всё;
	кц;
	возврат истина;
кон StringStartsWith;

(* Strings.Uppercase doesn't work for me... *)
проц UpperCase*(перем s: массив из симв8);
перем i: размерМЗ;
нач
	i := 0;
	нцПока (s[i] # 0X) делай
		если (s[i] > "9") или (s[i]  < "0") то
			s[i] := ASCII_вЗаглавную(s[i]);
		всё;
		увел(i)
	кц
кон UpperCase;

(* Strings that are longer then a certain limit must be passed as VAR-parameters so TextUtilities.StrToInt cannot be used. *)
проц StrToText*(text : Texts.Text; pos : размерМЗ; перем string : массив из симв8);
перем r : Потоки.ЧтецИзСтроки;
	i, m: размерМЗ;
	tempUCS32 : массив 1024 из Texts.Char32;
	ch, last : Texts.Char32;
нач
	text.AcquireWrite;
	нов(r, длинаМассива(string));
	m := длинаМассива(tempUCS32) - 1;
	r.ПримиСрезСтроки8ВСтрокуДляЧтения(string, 0, длинаМассива(string));
	i := 0;
	нцДо
		если TextUtilities.GetUTF8Char(r, ch) то
			если i = m то tempUCS32[i] := Texts.Симв32Нулевой; text.InsertUCS32(pos, tempUCS32); увел(pos, m); i := 0 всё;
			если (last # UCS32.CHARvCharJQ(0DX)) или (ch # UCS32.CHARvCharJQ(0AX)) то
				если ch = UCS32.CHARvCharJQ(0DX) то tempUCS32[i] := UCS32.CHARvCharJQ(0AX)
				иначе tempUCS32[i] := ch
				всё;
				увел(i)
			всё;
			last := ch
		всё
	кцПри (r.кодВозвратаПоследнейОперации # Потоки.Успех);
	tempUCS32[i] := Texts.Симв32Нулевой; text.InsertUCS32(pos, tempUCS32);
	text.ReleaseWrite
кон StrToText;

(** Text to UTF8 string. Objects and attributes are lost. *)
проц TextToStr*(text : Texts.Text; перем string :String);
перем
	i, l, pos: размерМЗ;
	r : Texts.TextReader;
	ch : Texts.Char32;
	ok : булево;
	buffer: Strings.Buffer;
	w: Потоки.Писарь;
	s: массив 7 из симв8;
нач
	нов(buffer, 16);
	w := buffer.GetWriter();
	text.AcquireRead;
	нов(r, text);
	i := 0; l := text.GetLength(); pos := 0; ok := истина;
	нцПока (i < l) и ok делай
		r.ReadCh(ch);
		если (ch > Texts.Симв32Нулевой) то
			pos := 0;
			ok := UTF8Strings.EncodeChar(ch, s, pos);
			w.пСтроку8(s);
		всё;
		увел(i)
	кц;
	text.ReleaseRead;
	string := buffer.GetString();
кон TextToStr;

проц SetEditorText*(editor: WMEditors.Editor; string: String);
перем
	text: Texts.Text;
	newString: String;
нач
	нов(text);
	newString := NewString(string^);
	StrToText(text, 0, newString^);
	editor.SetText(text);
кон SetEditorText;

(* Strings that are longer then a certain limit must be passed as VAR-parameters so Strings.NewString cannot be used. *)
проц NewString*(перем str : массив из симв8) : String;
перем l : размерМЗ; s : String;
нач
	l := StringLength(str) + 1;
	нов(s, l);
	копируйСтрокуДо0(str, s^);
	возврат s
кон NewString;

проц MakeQuotedString*(перем s: Strings.String);
перем
	i, count, len: размерМЗ;
	new: Strings.String;
нач
	(* count the number of the characters " and \ because we want to send a quoted string *)
	i := 0;
	count := 0;
	len := StringLength(s^);
	нцПока i < len делай
		если s^[i] = 22X то увел(count); всё;
		если s^[i] = "\" то увел(count); всё;
		увел(i);
	кц;
	нов(new, len + 3 + count);
	new[0] := 22X;
	i := 0;
	count := 0;
	нцПока i < len делай
		если s^[i] = 22X то
			new[i + count + 1] := "\";
			new[i + count + 2] := 22X;
			увел(count);
		аесли s^[i] = "\" то
			new[i + count + 1] := "\";
			new[i + count + 2] := "\";
			увел(count);
		иначе
			new[i + count + 1] := s^[i];
		всё;
		увел(i);
	кц;
	new[i + count + 1] := 22X;
	new[i + count + 2] := 0X;
	s := new;
кон MakeQuotedString;

(* transforms the string s which is in Base64 Transfer-Encoding to its normal representation *)
проц decodeBase64*(перем s: массив из симв8): String;
перем
	buf: Strings.Buffer;
	string: String;
	w: Потоки.Писарь;
	i: размерМЗ;
	sum, value, factor: цел32;
нач
	нов(buf, 16);
	w := buf.GetWriter();
	i := 0;
	sum := 0;
	factor := 64*64*64;
	нцПока i < StringLength(s) делай

		если base64Table[кодСимв8(s[i])] = -1 то
			увел(i);
		аесли base64Table[кодСимв8(s[i])] = 64 то
			(* finish *)
			если factor =  64 то
				(* decode 1 CHAR *)
				value := sum DIV (256*256);
				w.пСимв8(симв8ИзКода(value));
			иначе
				(* decode 2 CHARs *)
				value := sum DIV (256*256);
				w.пСимв8(симв8ИзКода(value));
				sum := sum остОтДеленияНа (256*256);

				value := sum DIV 256;
				w.пСимв8(симв8ИзКода(value));
			всё;
			i := StringLength(s);
		иначе
			sum := sum + factor*base64Table[кодСимв8(s[i])];
			если factor = 1 то
				value := sum DIV (256*256);

				w.пСимв8(симв8ИзКода(value));
				sum := sum остОтДеленияНа (256*256);
				value := sum DIV 256;

				w.пСимв8(симв8ИзКода(value));
				sum := sum остОтДеленияНа 256;

				w.пСимв8(симв8ИзКода(sum));

				sum := 0;
				factor := 64*64*64;
			иначе
				factor := factor DIV 64;
			всё;
			увел(i);
		всё;
	кц;
	string := buf.GetString();
	возврат string;
кон decodeBase64;

(* transforms the string s which is in QuotedPrintable Transfer-Encoding to its normal representation *)
проц decodeQuotedPrintable*(перем s: массив из симв8): String;
перем
	buf: Strings.Buffer;
	string: String;
	w: Потоки.Писарь;
	i: размерМЗ;
	value: цел32;
нач
	нов(buf, 16);
	w := buf.GetWriter();
	i := 0;
	нцПока i < StringLength(s) делай
		если кодСимв8(s[i]) = 61 то
			если (s[i+1] = 0DX) и (s[i+2] = 0AX) то
			иначе
				если (s[i+1] >= "0") и (s[i+1] <= "9") то
					value := 16 * (кодСимв8(s[i+1]) - кодСимв8("0"));
				аесли (s[i+1] >= "A") и (s[i+1] <= "F") то
					value := 16 * (кодСимв8(s[i+1]) - кодСимв8("A") + 10);
				всё;

				если (s[i+2] >= "0") и (s[i+2] <= "9") то
					value := value + (кодСимв8(s[i+2]) - кодСимв8("0"));
				аесли (s[i+2] >= "A") и (s[i+2] <= "F") то
					value := value + (кодСимв8(s[i+2]) - кодСимв8("A") + 10);
				всё;
				w.пСимв8(симв8ИзКода(value));
			всё;
			i := i + 3;
		иначе
			w.пСимв8(s[i]);
			увел(i);
		всё;
	кц;
	string := buf.GetString();
	возврат string;
кон decodeQuotedPrintable;

проц encodeQuotedPrintable*(перем string: String);
перем
	i, count: размерМЗ;
	value: цел32;
	c: симв8;
	chars: массив 17 из симв8;
	buf: Strings.Buffer;
	w: Потоки.Писарь;
нач
	Strings.Copy("0123456789ABCDEF", 0, 16, chars);
	нов(buf, 16);
	w := buf.GetWriter();

	i := 0;
	count := 0;
	c := string^[i];
	нцПока c # 0X делай
		если (кодСимв8(c) < 33) или (кодСимв8(c) = 61) или (кодСимв8(c) > 127) то
			w.пСимв8("=");
			value := кодСимв8(c) DIV 16;
			w.пСимв8(chars[value]);
			value := кодСимв8(c) остОтДеленияНа 16;
			w.пСимв8(chars[value]);
			count := count + 3;
		иначе
			w.пСимв8(c);
			увел(count);
		всё;

		если count > 72 то
			w.пСимв8("=");
			w.пСимв8(CR);
			w.пСимв8(LF);
			count := 0;
		всё;

		увел(i);
		c := string^[i];
	кц;
	string := buf.GetString();
кон encodeQuotedPrintable;

проц encodeXML*(перем s: массив из симв8): String;
перем
	temp: String;
	buffer: Strings.Buffer;
	w: Потоки.Писарь;
	i: размерМЗ;
	c: симв8;
нач
	нов(buffer, 16);
	w := buffer.GetWriter();
	i := 0;
	нцПока i < StringLength(s) делай
		c := s[i];
		если c = "&" то
			w.пСтроку8("&amp;");
		аесли c = "<" то
			w.пСтроку8("&lt;");
		аесли c = ">" то
			w.пСтроку8("&gt;");
		аесли c = "'" то
			w.пСтроку8("&apos;");
		аесли c = '"' то
			w.пСтроку8("&quot");
		иначе
			w.пСимв8(c);
		всё;
		увел(i);
	кц;
	temp := buffer.GetString();
	возврат temp;
кон encodeXML;

проц replaceEncodedHeaderWord*(перем buf: массив из симв8);
перем
	i, j, k: размерМЗ;
	buffer, res: Strings.Buffer;
	writer, resWriter: Потоки.Писарь;
	charset, text, string: String;
	encoding: симв8;
нач
	нов(buffer,16);
	нов(res, 16);
	writer := buffer.GetWriter();
	resWriter := res.GetWriter();
	i := 0;
	нцПока i < (StringLength(buf)) делай
		если (buf[i] = "=") и (buf[i+1] = "?") то
			i := i + 2;
			(* read charset *)
			нцПока (buf[i] # "?") и (i < StringLength(buf)) делай
				writer.пСимв8(buf[i]);
				увел(i);
			кц;
			если buf[i] # "?" то
				возврат;
			всё;

			charset := buffer.GetString();
			Strings.UpperCase(charset^);
			charset := Strings.NewString(charset^);
			buffer.Clear();

			увел(i);
			encoding := buf[i];
			увел(i);

			если buf[i] # "?" то
				возврат;
			всё;
			увел(i);
			нцПока (buf[i] # "?") и (i < StringLength(buf)) делай
				writer.пСимв8(buf[i]);
				увел(i);
			кц;
			text := buffer.GetString();
			text := Strings.NewString(text^);
			buffer.Clear();

			если buf[i] # "?" то возврат всё;
			увел(i);
			если buf[i] # "=" то возврат всё;
			увел(i);

			если (encoding = "Q") или (encoding = "q") то
				(* replace "-" by SPACE *)
				k := 0;
				нцПока k < StringLength(text^) делай
					если (text^[k]) = "_" то text^[k] := симв8ИзКода(SP); всё;
					увел(k);
				кц;
				string := decodeQuotedPrintable(text^);
			аесли (encoding = "B") или (encoding = "b")  то
				string := decodeBase64(text^);
			всё;

			если charset^ = "UTF-8" то
				resWriter.пСтроку8(string^);
			аесли charset^ = "ISO-8859-1" то
				j := StringLength(string^);
				нов(text, 6*j + 1);
				UTF8Strings.ASCIItoUTF8(string^, text^);
				resWriter.пСтроку8(text^);
			иначе (* assume US-ASCII *)
				j := StringLength(string^);
				нов(text, 6*j + 1);
				UTF8Strings.ASCIItoUTF8(string^, text^);
				resWriter.пСтроку8(text^);
			всё;
		иначе
			resWriter.пСимв8(buf[i]);
			увел(i);
		всё;
	кц;
	string := res.GetString();
	StringCopy(string^, 0, StringLength(string^), buf);
кон replaceEncodedHeaderWord;

проц replaceEncodedFolderName*(перем name: String);
перем
	i: размерМЗ;
	buffer: Strings.Buffer;
	w: Потоки.Писарь;
	utf8: массив 7 из симв8;
	state, value: цел32;
	pos: размерМЗ;
	r: булево;
нач
	нов(buffer, 16);
	w := buffer.GetWriter();
	i := 0;
	нцПока i < StringLength(name^) делай
		если (name^[i] = "&") и (name^[i+1] = "-") то
			w.пСимв8("&");
			i := i + 2;
		аесли name^[i] = "&" то
			увел(i);
			state := 0;
			pos := 0;
			нцПока name^[i] # "-" делай
				если (state = 0) или (state = 1) или (state = 3) или (state = 4) или (state = 6) то
					value := base64Table[кодСимв8(name^[i])];
					увел(state);
				аесли state = 2 то
					value := value * 64 + base64Table[кодСимв8(name^[i])];
					r := UTF8Strings.EncodeChar(симв32ИзКода(value DIV 4), utf8, pos);
					w.пСтроку8(utf8);
					value := value остОтДеленияНа 4;
					увел(state);
				аесли state = 5 то
					value := value * 64 + base64Table[кодСимв8(name^[i])];
					r := UTF8Strings.EncodeChar(симв32ИзКода(value DIV 16), utf8, pos);
					w.пСтроку8(utf8);
					value := value остОтДеленияНа 16;
					увел(state);
				аесли state = 7 то
					value := value * 64 + base64Table[кодСимв8(name^[i])];
					r := UTF8Strings.EncodeChar(симв32ИзКода(value), utf8, pos);
					w.пСтроку8(utf8);
					value := 0;
					state := 0;
				всё;
				увел(i);
			кц;
			увел(i);

		иначе
			w.пСимв8(name^[i]);
			увел(i);
		всё;
	кц;

	name := buffer.GetString();
кон replaceEncodedFolderName;

(* This procedure returns a string that is a valid string for the date field in an RFC822 message header *)
проц getRFC822Date*():String;
перем
	year, week : цел32;
	dayOfWeek: цел32;
	buffer: Strings.Buffer;
	w: Потоки.Писарь;
	answer: String;
	dayTable: массив 25 из симв8;
	monthTable: массив 37 из симв8;
	td : Dates.DateTime;
нач
	(* was: dayTable := "MonTueWedThuFriSatSun"; but: ISO 8601  week day is (Monday=1, ....Sunday=7) *)
	dayTable := "SunMonTueWedThuFriSatSun";
	monthTable := "JanFebMarAprMayJunJulAugSepOctNovDec";
	нов(buffer, 16);
	w := buffer.GetWriter();

	td := Dates.Now();
	Dates.WeekDate(td, year, week, dayOfWeek);

	w.пСимв8(dayTable[dayOfWeek*3]); w.пСимв8(dayTable[dayOfWeek*3+1]); w.пСимв8(dayTable[dayOfWeek*3+2]); w.пСтроку8(", ");
	w.пЦел64(td.day,0); w.пСимв8(" ");
	w.пСимв8(monthTable[(td.month-1)*3]); w.пСимв8(monthTable[(td.month-1)*3+1]); w.пСимв8(monthTable[(td.month-1)*3+2]); w.пСимв8(" ");
	w.пЦел64(year, 0); w.пСимв8(" ");

	если td.hour < 10 то w.пСимв8("0"); всё;
	w.пЦел64(td.hour, 0);
	w.пСимв8(":");
	если td.minute < 10 то w.пСимв8("0");всё;
	w.пЦел64(td.minute,0);
	w.пСимв8(":");
	если td.second < 10 то w.пСимв8("0"); всё;
	w.пЦел64(td.second,0);
	w.пСтроку8(" +0100");

	answer := buffer.GetString();

	возврат answer;
кон getRFC822Date;

нач
	(* create decoding table *)
	нцДля index := 0 до 127 делай
		base64Table[index] := -1;
	кц;

	нцДля index := 48 до 57 делай
		base64Table[index] := index + 4;
	кц;
	нцДля index := 65 до 90 делай
		base64Table[index] := index - 65;
	кц;
	нцДля index := 97 до 122 делай
		base64Table[index] := index - 71;
	кц;
	base64Table[43] := 62;
	base64Table[44] := 63; (* for replaceEncodedFolderName the BASE64 Encoding uses "," instead of "/" *)
	base64Table[47] := 63;
	base64Table[61] := 64;
кон IMAPUtilities.

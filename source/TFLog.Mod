модуль TFLog; (** AUTHOR "TF"; PURPOSE "Log utility"; *)

использует ЛогЯдра, Clock, Files;

конст
	Unknown* = 0;
	Information* = 1;
	Warning* = 2;
	Error* = 3;

тип
	Log* = окласс
	перем
		appName : массив 64 из симв8;
		logPos : цел32;
		logLine : массив 1024 из симв8;
		kind : цел32;
		locked : булево;
		disableLogToOut : булево;
		f : Files.File;
		w : Files.Writer;

		проц &Init*(logName : массив из симв8);
		нач
			копируйСтрокуДо0(logName, appName)
		кон Init;

		проц SetLogFile*(fn : массив из симв8);
		нач {единолично}
			если f # НУЛЬ то Files.Register(f) всё;
			если fn = "" то f := НУЛЬ
			иначе
				f := Files.Old(fn);
				если f = НУЛЬ то f := Files.New(fn) всё;
				Files.OpenWriter(w, f, f.Length())
			всё
		кон SetLogFile;

		проц SetLogToOut*(enabled : булево);
		нач {единолично}
			disableLogToOut := ~enabled
		кон SetLogToOut;

		проц SetKind(kind : цел32);
		нач {единолично}
			сам.kind := kind
		кон SetKind;

		проц InternalLn;
		нач
			logPos := 0; kind := Unknown;
			если ~disableLogToOut то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				если kind = Information то ЛогЯдра.пСтроку8("[I] ") всё;
				если kind = Warning то ЛогЯдра.пСтроку8("[W] ") всё;
				если kind = Error то ЛогЯдра.пСтроку8("[E] ") всё;
				ЛогЯдра.пСтроку8(appName); ЛогЯдра.пСтроку8(" : "); ЛогЯдра.пСтроку8(logLine);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
			всё;
			logLine[0] := 0X;
			если f # НУЛЬ то w.пВК_ПС; w.ПротолкниБуферВПоток; f.Update() всё
		кон InternalLn;

		проц Ln*;
		нач {единолично}
			InternalLn
		кон Ln;

		проц Enter*;
		нач {единолично}
			дождись(~locked); locked := истина
		кон Enter;

		проц Exit*;
		нач {единолично}
			InternalLn;
			locked := ложь
		кон Exit;

		проц InternalChar(x: симв8);
		нач
			если logPos >= длинаМассива(logLine) - 1 то InternalLn всё;
			logLine[logPos] := x; logLine[logPos + 1] := 0X; увел(logPos);
			если f # НУЛЬ то w.пСимв8(x) всё
		кон InternalChar;

		проц Char*(x: симв8);
		нач {единолично}
			InternalChar(x)
		кон Char;

		проц InternalString*(перем x: массив из симв8);
		перем i : цел32;
		нач
			нцПока (i < длинаМассива(x)) и (x[i] # 0X) делай InternalChar(x[i]); увел(i) кц
		кон InternalString;

		проц String*(x: массив из симв8);
		нач {единолично}
			InternalString(x)
		кон String;

		проц Hex*(x, w: цел32);
		перем i, j: цел32; buf: массив 10 из симв8;
		нач {единолично}
			если w >= 0 то j := 8 иначе j := 2; w := -w всё;
			нцДля i := j+1 до w делай InternalChar(" ") кц;
			нцДля i := j-1 до 0 шаг -1 делай
				buf[i] := симв8ИзКода(x остОтДеленияНа 10H + 48);
				если buf[i] > "9" то
					buf[i] := симв8ИзКода(кодСимв8(buf[i]) - 48 + 65 - 10)
				всё;
				x := x DIV 10H
			кц;
			buf[j] := 0X;
			InternalString(buf)
		кон Hex;

		проц Int*(x: цел64; w: цел32);
		перем i: цел32; x0: цел64; a: массив 21 из симв8;
		нач {единолично}
			если x < 0 то
				если x = матМинимум( цел64 ) то
					умень(w, 20);
					нцПока w > 0 делай Char(" "); умень(w) кц;
					 a := "-9223372036854775808"; InternalString(a);
					возврат
				иначе
					умень(w); x0 := -x
				всё
			иначе
				x0 := x
			всё;
			i := 0;
			нцДо
				a[i] := симв8ИзКода(x0 остОтДеленияНа 10 + 30H); x0 := x0 DIV 10; увел(i)
			кцПри x0 = 0;
			нцПока w > i делай InternalChar(" "); умень(w) кц;
			если x < 0 то InternalChar("-") всё;
			нцДо умень(i); InternalChar(a[i]) кцПри i = 0
		кон Int;

		проц TimeStamp*;
		тип TimeDate = запись h, m, s, day,month,year: цел32 кон;
			перем s : массив 32 из симв8;
				now : TimeDate;

			проц LZ(v, len: цел32; перем s: массив из симв8; перем pos: цел32);
			перем i: цел32;
			нач
				нцДля i := 1 до len делай s[pos+len-i] := симв8ИзКода(кодСимв8("0")+v остОтДеленияНа 10); v := v DIV 10 кц;
				увел(pos, len)
			кон LZ;

			проц GetTime(перем dt: TimeDate);
			нач
				Clock.Get(dt.h, dt.year);
				dt.s := dt.h остОтДеленияНа 64; dt.h := dt.h DIV 64;
				dt.m := dt.h остОтДеленияНа 64; dt.h := dt.h DIV 64;
				dt.h := dt.h остОтДеленияНа 24;
				dt.day := dt.year остОтДеленияНа 32; dt.year := dt.year DIV 32;
				dt.month := dt.year остОтДеленияНа 16; dt.year := dt.year DIV 16;
				увел(dt.year, 1900)
			кон GetTime;

			проц TimeDateToStr(dt: TimeDate; перем s: массив из симв8);
			перем p: цел32;
			нач
				LZ(dt.day, 2, s, p); s[p] := "."; увел(p);
				LZ(dt.month, 2, s, p); s[p] := "."; увел(p);
				LZ(dt.year, 2, s, p); s[p] := " "; увел(p);
				LZ(dt.h, 2, s, p); s[p] := ":"; увел(p);
				LZ(dt.m, 2, s, p); s[p] := ":"; увел(p);
				LZ(dt.s, 2, s, p); s[p] := 0X
			кон TimeDateToStr;

		нач
			GetTime(now);
			TimeDateToStr(now, s);
			InternalString(s); InternalChar(" ")
		кон TimeStamp;

		проц Close*;
		нач
			если f # НУЛЬ то Files.Register(f)
			всё
		кон Close;

	кон Log;

кон TFLog.


System.Free TFLog ~

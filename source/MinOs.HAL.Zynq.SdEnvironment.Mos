модуль SdEnvironment;
(**
	AUTHOR Timothée Martiel, 01/2016
	PURPOSE SD driver environment for Minos
*)

использует
	Platform, Board, Трассировка, Caches, Interrupts, GlobalTimer;

конст
	MaxHandlers = 2;

перем
	Char *: проц (c: симв8);
	String *: проц (конст str: массив из симв8);
	Int *: проц (i: цел64; w: цел32);
	Hex *: проц (i: цел32; w: цел32);
	Address *: проц (a: адресВПамяти);
	Set *: проц (s: мнвоНаБитахМЗ);
	Boolean *: проц (b: булево);
	Ln *: проц;
	FlushDCacheRange *,
	InvalidateDCacheRange *: проц (adr: адресВПамяти; size: размерМЗ);
	GetTimeCounter *: проц (): Time;

	handlers: массив MaxHandlers из запись irq: цел32; handle: проц кон;

тип
	Time * = цел64;

	проц InstallHandler * (handler: проц; irq: цел32);
	перем
		i: цел32;
	нач
		i := 0;
		нцПока (i < MaxHandlers) и (handlers[i].handle # НУЛЬ) делай увел(i) кц;
		утв(i < MaxHandlers);
		handlers[i].irq := irq;
		handlers[i].handle := handler;
		Interrupts.InstallHandler(InterruptHandler, irq)
	кон InstallHandler;

	проц Enable * (sd: цел32): булево;
	перем
		enable: булево;
	нач
		просей sd из
			 0: enable := Board.SdEnable0
			|1: enable := Board.SdEnable1
		всё;
		возврат enable
	кон Enable;

	проц HcClock * (sd: цел32): цел32;
	перем
		reg, div, base: цел32;
	нач
		div := логСдвиг(Platform.slcr.SDIO_CLK_CTRL, -8) остОтДеленияНа 40H;
		просей логСдвиг(Platform.slcr.SDIO_CLK_CTRL, -4) остОтДеленияНа 4 из
			 0:
				reg := Platform.slcr.IO_PLL_CTRL;
				base := Board.PsRefClockHz
			|2:
				reg := Platform.slcr.ARM_PLL_CTRL;
				base := Board.CpuClockHz DIV 2
			|3:
				reg := Platform.slcr.DDR_PLL_CTRL;
				Трассировка.StringLn("ERROR: DDR PLL clock source for SD not supported")
		всё;
		base := base * (логСдвиг(reg, -12) остОтДеленияНа 80H);
		возврат base DIV div
	кон HcClock;

	(** Convert microseconds to time counts *)
	проц FromMicro*(us: Time): Time;
	нач
		возврат us * округлиВниз64(0.5D0+Board.CpuClockHz/2.0D6);
	кон FromMicro;

	(** Convert time counts to microseconds *)
	проц ToMicro*(time: Time): Time;
	нач
		возврат округлиВниз64((0.5D0 + time) / (вещ64(Board.CpuClockHz)) * 2.0D6)
	кон ToMicro;

	(** Convert milliseconds to time counts *)
	проц FromMilli*(ms: Time): Time;
	нач
		возврат ms * округлиВниз64(0.5D0+Board.CpuClockHz/2.0D3);
	кон FromMilli;

	(** Convert time counts to milliseconds *)
	проц ToMilli*(time: Time): Time;
	нач
		возврат округлиВниз64((0.5D0 + time) / (вещ64(Board.CpuClockHz)) * 2.0D3)
	кон ToMilli;

	проц GetLock * (перем acq, rel: проц {делегат});
	кон GetLock;

	проц InterruptHandler (irq: цел32);
	перем
		i: цел32;
	нач
		нцДля i := 0 до MaxHandlers - 1 делай
			если handlers[i].irq = irq то
				handlers[i].handle
			всё
		кц
	кон InterruptHandler;
нач
	GlobalTimer.EnableTimer;
	Char := Трассировка.пСимв8;
	String := Трассировка.пСтроку8;
	Int := Трассировка.HInt;
	Hex := Трассировка.п16ричное;
	Address := Трассировка.пАдресВПамяти;
	Set := Трассировка.пМнвоНаБитахМЗКакЧисла;
	Boolean := Трассировка.пБулево;
	Ln := Трассировка.пВК_ПС;
	FlushDCacheRange := Caches.CleanDCacheRange;
	InvalidateDCacheRange := Caches.InvalidateDCacheRange;
	GetTimeCounter := GlobalTimer.GetTime;
кон SdEnvironment.

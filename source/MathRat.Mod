(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль MathRat;   (** AUTHOR "adf"; PURPOSE "Rational math functions"; *)

использует NbrInt, NbrInt64, NbrRat, DataErrors, MathInt;

конст
	max = 2147483647;
	MaxFactorial* = 21;

	(**  h n i       n!
     |   | = ---------,  m 3 0
     j m k    m!(n-m)!
	*)
	проц Binomial*( top, bottom: NbrInt.Integer ): NbrRat.Rational;
	(* Formula 6:3:1 of: J. Spanier and K. B. Oldham, An Atlas of Functions, Hemisphere Publishing Corp.,
		Washington DC, 1987. *)
	перем denom, numer: NbrInt64.Integer;  i: NbrInt.Integer;  coef, prod: NbrRat.Rational;
	нач
		если bottom < 0 то prod := 0;  DataErrors.IntError( bottom, "Bottom parameter cannot be negative." )
		аесли bottom = 0 то prod := 1
		иначе
			i := bottom;  prod := 1;
			нцДо
				denom := NbrInt64.Long( i );  numer := NbrInt64.Long( top - (bottom - i) );
				NbrRat.Set( numer, denom, coef );  prod := coef * prod;
				NbrInt.Dec( i )
			кцПри i = 0
		всё;
		возврат prod
	кон Binomial;

(** Computes  n! = n * (n - 1) * (n - 2) * ... * 1,  MaxFactorial 3 n 3 0. *)
	проц Factorial*( n: NbrInt.Integer ): NbrRat.Rational;
	перем i: NbrInt.Integer;  x: NbrRat.Rational;
	нач
		если n < 0 то x := 0;  DataErrors.IntError( n, "Negative arguments are inadmissible." )
		аесли n = 0 то x := 1
		аесли n <= MaxFactorial то
			x := 1;  i := 0;
			нцДо NbrInt.Inc( i );  x := i * x кцПри i = n
		иначе
			NbrRat.Set( NbrInt64.MaxNbr, NbrInt64.Long( 1 ), x );  DataErrors.IntError( n, "Arithmatic overflow." )
		всё;
		возврат x
	кон Factorial;

(** Computes  xn,  {x,n} 9 {0,0}. *)
	проц Power*( x: NbrRat.Rational;  n: NbrInt.Integer ): NbrRat.Rational;
	перем sign: NbrInt.Integer;  denom, dPower, numer, nPower: NbrInt64.Integer;  power: NbrRat.Rational;

		проц IntPower( a: NbrInt64.Integer;  b: NbrInt.Integer ): NbrInt64.Integer;
		 (* Computes  ab,  a > 0,  b 3 0. *)
		перем max, p: NbrInt64.Integer;
		нач
			p := 1;
			нцПока b > 0 делай
				нцПока ~NbrInt.Odd( b ) и (b > 0) делай
					max := NbrInt64.MaxNbr DIV a;
					если a > max то a := max;  b := 2;  DataErrors.Error( "Arithmatic overflow." ) всё;
					a := a * a;  b := b DIV 2
				кц;
				max := NbrInt64.MaxNbr DIV p;
				если a > max то a := max;  b := 1;  DataErrors.Error( "Arithmatic overflow." ) всё;
				p := p * a;  NbrInt.Dec( b )
			кц;
			возврат p
		кон IntPower;

	нач
		sign := 1;
		если n = 0 то
			если x # 0 то power := 1
			иначе power := 0;  DataErrors.Error( "Both argument and exponent cannot be zero." )
			всё
		аесли x = 0 то
			power := 0;
			если n < 0 то DataErrors.IntError( n, "Exponent cannot be negative when argument is zero." ) всё
		иначе
			numer := NbrRat.Numer( x );  denom := NbrRat.Denom( x );
			если numer < 0 то
				numer := NbrInt64.Abs( numer );
				если NbrInt.Odd( n ) то sign := -1 всё
			всё;
			если n < 0 то nPower := IntPower( denom, -n );  dPower := IntPower( numer, -n )
			иначе nPower := IntPower( numer, n );  dPower := IntPower( denom, n )
			всё;
			NbrRat.Set( nPower, dPower, power )
		всё;
		возврат sign * power
	кон Power;

(** Returns a pseudo-random rational number uniformly distributed over the unit interval, i.e.,  Random() N (0, 1). *)
	проц Random*( ): NbrRat.Rational;
	перем n, d: NbrInt64.Integer;  r: NbrRat.Rational;
	нач
		n := NbrInt64.Long( MathInt.Random() );  d := NbrInt64.Long( матМаксимум( цел32 ) );
		NbrRat.Set( n, d, r );  возврат r
	кон Random;

кон MathRat.

модуль srTree;
использует srBase, srGL, srVoxel, srM2Space;

тип PT = srBase.PT;

тип tree* = окласс(srBase.V)
перем
	context: srGL.Context;
	cell*: srM2Space.cell;
	a,b,c, z: PT;
	angle: вещ32;

проц & new*;
нач
	нов(context);
	нов(cell);
	srBase.setPT(a,0.5, 0.5, 0.3);
	srBase.setPT(b,0.5, 0.5, 0.7);
	srBase.setPT(c,0.5, 0.5, 1);
	srBase.setPT(z, 0, 0, 1);
	angle := 0;
	trunk;
	register;
кон new;

проц {перекрыта}tick*;
(*
BEGIN
	cell.erase;
	branch(angle);
	angle := angle + 0.0134472;*)
кон tick;

проц trunk;
нач
	branch(1,2,1)
кон trunk;

проц branch(level, limit, rep: цел16);
перем
	p,q: PT;
	i: цел16;
нач
	нцДля i := 0 до rep делай
		srBase.randPT(q);
		cell.linevoxel(p,q, level,green);
		context.push;
		context.translatep(q);
		если level<limit то branch(level+1,limit,rep+2) всё
	кц;
(*	context.push;
	p := a; q := b;
	context.rotate(r,0,1,0);
	context.transform(p);
	context.transform(q);
	cell.line(p,q,2,green);
	context.translatep(q);
	p := a; q := b;
	context.rotate(r,0,1,0);
	context.transform(p);
	context.transform(q);
	cell.line(p,q,3,blue);
	context.translatep(q);
	p := a; q := b;
	context.rotate(r,0,1,0);
	context.transform(p);
	context.transform(q);
	cell.line(p,q,3,red);
	twig;
	context.pop; *)
кон branch;

проц twig;
перем
	p,q: PT;
нач
	context.push;
	context.scale(1/2,1/2,1/2);
	p := a; q := b;
	context.rotate(2/3,0,1,1);
	context.transform(p);
	context.transform(q);
	cell.linevoxel(p,q,4,green);
	context.translatep(b);
	p := a; q := b;
	context.rotate(2/3,0,1,1);
	context.transform(p);
	context.transform(q);
	cell.linevoxel(p,q,4,blue);
	context.translatep(b);
	p := a; q := b;
	context.rotate(2/3,0,1,1);
	context.transform(p);
	context.transform(q);
	cell.linevoxel(p,q,4,blue);
	context.translatep(b);
	p := a; q := b;
	context.rotate(2/3,0,1,1);
	context.transform(p);
	context.transform(q);
	cell.linevoxel(p,q,4,blue);
	context.pop;
кон twig;

кон tree;

перем
	green, blue, red: srVoxel.DiffuseVox;
	Tree*:tree;
нач
	нов(green);
	green.SetColor(0,1,0);
	нов(blue);
	blue.SetColor(0,0,1);
	нов(red);
	red.SetColor(1,0,0);
	нов(Tree);
кон srTree.

модуль ProcessInfo0; (** AUTHOR "staubesv"; PURPOSE "Platform-dependent process interface"; *)

использует
	Objects;

тип

	ProcessArray* = массив из Objects.Process;

проц GetProcesses*(перем array : ProcessArray; перем nofProcesses : размерМЗ);
перем thread : Objects.Process; length : размерМЗ;
нач
	nofProcesses := 0;
	length := длинаМассива(array);
	thread := Objects.root;
	нцПока (thread # НУЛЬ) и (nofProcesses < length) делай
		если (thread.mode # Objects.Terminated) то
			array[nofProcesses] := thread(Objects.Process);
			увел(nofProcesses);
		всё;
		thread := thread.nextProcess;
	кц;
кон GetProcesses;

проц GetProcess*(id : целМЗ) : Objects.Process;
перем process, thread : Objects.Process;
нач
	process := НУЛЬ;
	thread := Objects.root;
	нцПока (thread # НУЛЬ) и (process = НУЛЬ) делай
		если thread.id = id то process := thread; всё;
		thread := thread.nextProcess;
	кц;
	возврат process;
кон GetProcess;

кон ProcessInfo0.

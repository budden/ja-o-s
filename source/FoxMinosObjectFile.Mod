модуль FoxMinosObjectFile; (** AUTHOR "fof"; PURPOSE "Oberon Compiler Minos Object File Writer"; *)

использует
	Scanner := FoxScanner, Basic := FoxBasic, SyntaxTree := FoxSyntaxTree, Global := FoxGlobal, SemanticChecker := FoxSemanticChecker, Fingerprinter := FoxFingerprinter, Sections := FoxSections,
	Потоки, D := Debugging, Files, НИЗКОУР,Строки8, BinaryCode := FoxBinaryCode, ЛогЯдра, Diagnostics, SymbolFileFormat := FoxTextualSymbolFile, Options,
	Formats := FoxFormats, IntermediateCode := FoxIntermediateCode, ЭВМ, ПереводыЭлементовКода;

конст
	Trace=ложь;

тип Name=массив 256 из симв8;
	ByteArray = укль на массив из симв8;

тип

	Fixup = окласс
	перем
		nextFixup: Fixup;
		fixup: BinaryCode.Fixup;
		fixupSection: Sections.Section;
	кон Fixup;

	ObjectFileFormat*= окласс (Formats.ObjectFileFormat)
	перем extension,prefix: Basic.FileName;

		проц {перекрыта}Export*(module: Formats.GeneratedModule; symbolFileFormat: Formats.SymbolFileFormat): булево;
		перем symbolFile: Files.File; moduleName: SyntaxTree.СтрокаИдентификатора; fileName: Files.FileName; f: Files.File; w: Files.Writer;
		перем varSize, codeSize, bodyOffset: размерМЗ; перем code: ByteArray; error: булево;
		нач
			Global.ModuleFileName(module.module.имя,module.module.контекстA2,moduleName,ПереводыЭлементовКода.АнглоязычныйРежим (* ПРАВЬМЯ *));
			Basic.Concat(fileName,prefix,moduleName,extension);
			если Trace то D.Str("FoxMinosObjectFile.ObjectFileFormat.Export "); D.Str(moduleName); D.Ln; всё;

			если ~(module суть Sections.Module) то
				Basic.Error(diagnostics, module.module.имяФайлаИсходника, Basic.invalidPosition, "generated module format does not match object file format");
				возврат ложь;
			аесли module.findPC # матМаксимум(размерМЗ) то
				MakeSectionOffsets(module(Sections.Module),varSize, codeSize, bodyOffset, code);
				возврат FindPC(module.findPC,module(Sections.Module),diagnostics);
			иначе
				просейТип module: Sections.Module делай
					f := Files.New(fileName);
					утв(f # НУЛЬ);
					нов(w,f,0);

					error := ~WriteObjectFile(w,module,symbolFile, diagnostics);
					w.ПротолкниБуферВПоток;
					Files.Register(f);
					возврат ~error
				всё;
			всё;
		кон Export;

		проц {перекрыта}DefineOptions*(options: Options.Options);
		нач
			options.Add(0X,"objectFileExtension",Options.String);
			options.Add(0X,"objectFilePrefix",Options.String);
		кон DefineOptions;

		проц {перекрыта}GetOptions*(options: Options.Options);
		нач
			если ~options.GetString("objectFileExtension",extension) то
				extension := ".arm"
			всё;
			если ~options.GetString("objectFilePrefix",prefix) то prefix := "" всё
		кон GetOptions;

		проц {перекрыта}DefaultSymbolFileFormat*(): Formats.SymbolFileFormat;
		нач возврат SymbolFileFormat.Get();
		кон DefaultSymbolFileFormat;

		проц {перекрыта}ForceModuleBodies*(): булево; (* necessary in binary object file format as bodies not recognizable later on *)
		нач возврат истина
		кон ForceModuleBodies;

		проц {перекрыта}GetExtension*(перем ext: массив из симв8);
		нач копируйСтрокуДо0(extension, ext)
		кон GetExtension;

	кон ObjectFileFormat;

	(*
		this procedure converts the section-based representation of fixups into a symbol based representation
	*)
	проц GetFixups(diagnostics: Diagnostics.Diagnostics; module: Sections.Module; symbol: Sections.Section; перем first: Fixup): размерМЗ;
	перем temp: Fixup; fixup: BinaryCode.Fixup; nr, i: размерМЗ; section: Sections.Section; sectionList: Sections.SectionList;

		проц Do;
		нач
			нцДля i := 0 до sectionList.Length() - 1 делай
				section := sectionList.GetSection(i);
				если (section.type # Sections.InlineCodeSection) и (section.type # Sections.InitCodeSection) то
					если section(IntermediateCode.Section).resolved # НУЛЬ то
					fixup := section(IntermediateCode.Section).resolved.fixupList.firstFixup;
					нцПока (fixup # НУЛЬ) делай
						если (fixup.symbol.name = symbol.name) то
							увел(nr);
							нов(temp);
							temp.fixup := fixup;
							temp.fixupSection := section;
							temp.nextFixup := first;
							если fixup.displacement # 0 то
								Basic.Error(diagnostics, module.moduleName, Basic.invalidPosition, "Fixups with displacement # 0 not supported in Minos Object File.");
							всё;
							first := temp;
						всё;
						fixup := fixup.nextFixup;
					кц;
					всё
				всё
			кц;
		кон Do;

	нач
		first := НУЛЬ; nr := 0;
		sectionList := module.allSections; Do;
		sectionList := module.importedSections; Do;
		возврат nr
	кон GetFixups;

	проц FindPC(pc: размерМЗ; module: Sections.Module; diagnostics: Diagnostics.Diagnostics): булево;
	перем
		section:Sections.Section; binarySection: BinaryCode.Section; label: BinaryCode.LabelList;
		i: размерМЗ;
	нач
		нцДля i := 0 до module.allSections.Length() - 1 делай
			section := module.allSections.GetSection(i);
			binarySection := section(IntermediateCode.Section).resolved;
			если ((section.offset ) <= pc) и (pc < (section.offset +binarySection.pc )) то
				label := binarySection.labels;
				нцПока (label # НУЛЬ) и ((label.offset  + section.offset ) > pc) делай
					label := label.prev;
				кц;
				если label # НУЛЬ то
					Basic.Information(diagnostics, module.module.имяФайлаИсходника,label.position," pc position");
					возврат истина
				всё;
			всё
		кц;
		Basic.Error(diagnostics, module.module.имяФайлаИсходника,Basic.invalidPosition, " could not locate pc");
		возврат ложь
	кон FindPC;

	проц MakeSectionOffsets(module: Sections.Module; перем varSize, codeSize, bodyOffset: размерМЗ; перем code: ByteArray);
	перем symbolName: SyntaxTree.СтрокаИдентификатора; symbol: SyntaxTree.ОбъявлениеИменованнойСущности; binarySection: BinaryCode.Section;

		проц Copy(section: BinaryCode.Section; to: ByteArray; offset: размерМЗ);
		перем i: BinaryCode.Unit; ofs: размерМЗ;
		нач
			ofs := (offset );
			нцДля i := 0 до ((section.pc-1) ) делай
				to[i+ofs] := симв8ИзКода(section.os.bits.GetBits(i*8,8));
			кц;
		кон Copy;

		(*
		PROCEDURE ReportSection(section: Sections.Section);
		BEGIN
			D.String("Section ");  Basic.WriteSegmentedName(D.Log, section.name); D.String(" allocated at "); D.Int(section.offset,1); D.Ln;
		END ReportSection;
		*)

		(*
		not necessary
		*)

		(* link body as first section: entry[0] = 0 *)
		проц FirstOffsets(sectionList: Sections.SectionList);
		перем
			section: Sections.Section;
			i: размерМЗ;
		нач
			нцДля i := 0 до sectionList.Length() - 1 делай
				section := sectionList.GetSection(i);
				binarySection := section(IntermediateCode.Section).resolved;
				symbol := section.symbol;
				если symbol # НУЛЬ то
					symbol.ДайИмяВВидеСтроки(symbolName);
					если section.symbol = module.module.внутренняяОбластьВидимостиМодуля.процедураˉтело то
						section.SetOffset(0); увел(codeSize,binarySection.pc);
						(*ReportSection(section)*)
					всё;
				всё
			кц;
		кон FirstOffsets;

		(* note: if 'caseSections' is TRUE, only case table sections are processed, otherwise only regular sections (imported symbol/system call sections are never processed) *)
		проц SetOffsets(sectionList: Sections.SectionList);
		перем
			section: Sections.Section;
			i: размерМЗ;
		нач
			нцДля i := 0 до sectionList.Length() - 1 делай
				section := sectionList.GetSection(i);

				binarySection := section(IntermediateCode.Section).resolved;
				symbol := section.symbol;
				если symbol # НУЛЬ то
					symbol.ДайИмяВВидеСтроки(symbolName);
				иначе symbolName := "";
				всё;

				если section.symbol = module.module.внутренняяОбластьВидимостиМодуля.процедураˉтело то
				аесли symbolName = "@moduleSelf" то
				аесли section.type = Sections.ConstSection то
					если binarySection.os.alignment # 0 то
						увел(codeSize,(-codeSize) остОтДеленияНа binarySection.os.alignment);
					всё;
					section.SetOffset(codeSize); увел(codeSize,binarySection.pc); (* global constants: in code *)
					Basic.Align(codeSize, 4); (* word alignment *)
					(*ReportSection(section)*)
				аесли (section.type = Sections.CodeSection) или (section.type = Sections.BodyCodeSection) то
					(*IF section.symbol = module.module.moduleScope.bodyProcedure THEN
						bodyOffset := codeSize
					END;
					*)
					section.SetOffset(codeSize); увел(codeSize, binarySection.pc);
					Basic.Align(codeSize, 4); (* word alignment *)
					(*ReportSection(section)*)
				аесли section.type = Sections.VarSection то
					увел(varSize, binarySection.pc);
					если binarySection.os.alignment # 0 то
						увел(varSize,(-varSize) остОтДеленияНа binarySection.os.alignment);
					всё;
					section.SetOffset(-varSize); (* global variables: negative offset *)
					(*ReportSection(section)*)
				всё
			кц;
		кон SetOffsets;

		(* note: if 'caseSections' is TRUE, only case table sections are processed, otherwise only regular sections (imported symbol/system call sections are never processed) *)
		проц CopySections(sectionList: Sections.SectionList);
		перем
			section: Sections.Section;
			i: размерМЗ;
		нач
			нцДля i := 0 до sectionList.Length() - 1 делай
				section := sectionList.GetSection(i);
				binarySection := section(IntermediateCode.Section).resolved;
				если (section.type = Sections.CodeSection) или (section.type = Sections.BodyCodeSection) или (section.type = Sections.ConstSection) то
					Copy(binarySection,code,section.offset);
				всё
			кц;
		кон CopySections;

	нач

		FirstOffsets(module.allSections); (* regular sections *)

		SetOffsets(module.allSections); (* regular sections and case table sections -- a case table is a special case of a constant section *)
		нов(code,codeSize );
		CopySections(module.allSections); (* regular sections *)
	кон MakeSectionOffsets;

	проц WriteObjectFile*(w:Потоки.Писарь; module: Sections.Module; symbolFile: Files.File; diagnostics: Diagnostics.Diagnostics): булево;
	перем codeSize, dataSize, bodyOffset: размерМЗ;
		moduleScope: SyntaxTree.ВнутренняяОбластьВидимостиМодуля; fingerprinter: Fingerprinter.Fingerprinter;
		code: ByteArray;
		fp: SyntaxTree.КонтрольнаяСумма;
		error : булево;
		(** helper procedures *)
		проц GetEntries(l0moduleScope: SyntaxTree.ВнутренняяОбластьВидимостиМодуля; перем numberEntries: размерМЗ; перем entries: массив 256 из IntermediateCode.Section);
		перем symbol: SyntaxTree.ОбъявлениеИменованнойСущности; p: Sections.Section;

			проц ConstantNeedsSection(constant: SyntaxTree.ОбъявлениеКонстанты): булево;
			нач
				возврат ложь;
				(* constant sections are not used any more in importing modules, they are copied instead *)
			кон ConstantNeedsSection;

			проц TypeNeedsSection(type: SyntaxTree.ОбъявлениеТипаˉнаименования): булево;
			нач
				возврат (type.выражениеˉтип.резОсмысления суть SyntaxTree.ВыражˉтипИзРодаЗаписей)
			кон TypeNeedsSection;

		нач
			numberEntries := 0;
			symbol := l0moduleScope.первоеОбъявление;
			нцПока symbol # НУЛЬ делай
				если (symbol.праваДоступа * SyntaxTree.ВиденВсем # {}) то
					если (symbol суть SyntaxTree.ОбъявлениеПроцедуры) и ~(symbol(SyntaxTree.ОбъявлениеПроцедуры).встраиваемаяЛи¿)
							или (symbol суть SyntaxTree.ОбъявлениеПеременной)
							или (symbol суть SyntaxTree.ОбъявлениеТипаˉнаименования) и TypeNeedsSection(symbol(SyntaxTree.ОбъявлениеТипаˉнаименования))
							или (symbol суть SyntaxTree.ОбъявлениеКонстанты) и (ConstantNeedsSection(symbol(SyntaxTree.ОбъявлениеКонстанты))) то
						p := module.allSections.FindBySymbol(symbol);
						если p = НУЛЬ то
							p := module.importedSections.FindBySymbol(symbol);
						всё;
						увел(numberEntries); (* start at 1 !! *)
						если p # НУЛЬ то
							entries[numberEntries] := p(IntermediateCode.Section);
							если Trace то
								если l0moduleScope = module.module.внутренняяОбластьВидимостиМодуля (* self *) то
									D.String("Entry "); D.Int(numberEntries,1); D.String(": "); D.Str0(symbol.имя); D.String(" @"); D.Int(p.offset,1); D.Ln;
								иначе
									D.String("Imported Entry "); D.Int(numberEntries,1); D.String(": "); D.Str0(symbol.имя); D.String(" @"); D.Int(p.offset,1); D.Ln;
								всё;
							всё;
						иначе
							entries[numberEntries] := НУЛЬ;
							если l0moduleScope = module.module.внутренняяОбластьВидимостиМодуля (* self *) то
								СТОП(100); (* must not happen: all symbols that need a section in this module must have a section *)
							всё;
						всё;
					всё;
				всё;
				symbol := symbol.следщОИС;
			кц;
		кон GetEntries;

		проц Put32(offset: размерМЗ; number: цел32);
		нач
			если Trace то
				D.String("put32 at offset "); D.Int(offset,1);D.String(" : "); D.Hex(number,-8); D.Ln;
			всё;
			code[offset] := симв8ИзКода(number остОтДеленияНа 100H);
			увел(offset); number := number DIV 100H;
			code[offset] := симв8ИзКода(number остОтДеленияНа 100H);
			увел(offset); number := number DIV 100H;
			code[offset] := симв8ИзКода(number остОтДеленияНа 100H);
			увел(offset); number := number DIV 100H;
			code[offset] := симв8ИзКода(number остОтДеленияНа 100H);
		кон Put32;

		проц Get32(offset: размерМЗ): цел32;
		нач
			возврат кодСимв8(code[offset]) + 100H*кодСимв8(code[offset+1]) + 10000H * кодСимв8(code[offset+2]) + 1000000H*кодСимв8(code[offset+3]);
		кон Get32;

		(* ObjectFile = name:String key:Int fixSelf:Int Imports Commands Entries Data Code *)
		проц ObjectFile(l1bodyOffset: размерМЗ);
		перем moduleName: Name;

			проц Resolve(fixup: BinaryCode.Fixup);
			нач
				если fixup.resolved = НУЛЬ то fixup.resolved := module.allSections.FindByName(fixup.symbol.name) всё;
				если fixup.resolved = НУЛЬ то fixup.resolved := module.importedSections.FindByName(fixup.symbol.name) всё;
			кон Resolve;

			проц InModule(s: Basic.SegmentedName):булево;
			перем
				section: Sections.Section;
				i: размерМЗ;
			нач
				нцДля i := 0 до module.allSections.Length() - 1 делай
					section := module.allSections.GetSection(i);
					если section.name = s то возврат истина всё
				кц;
				возврат ложь
			кон InModule;

			(* go through list of all sections and all fixups in sections and if it is a self fixup, chain it *)
			проц FixSelf(): размерМЗ;
			перем prev,this,patch: размерМЗ; section: Sections.Section;
				binarySection: BinaryCode.Section; fixup: BinaryCode.Fixup; i, patchOffset: размерМЗ;
				msg, name: массив  256 из симв8;
			нач
				prev := 0;
				нцДля i := 0 до module.allSections.Length() - 1 делай
					section := module.allSections.GetSection(i);
					если  (section.type # Sections.InitCodeSection) то
						binarySection := section(IntermediateCode.Section).resolved;
						fixup := binarySection.fixupList.firstFixup;
						нцПока fixup # НУЛЬ делай
							если (fixup.mode = BinaryCode.Relative)  и InModule(fixup.symbol.name) то
								Basic.Error(diagnostics, module.moduleName, Basic.invalidPosition, "Relative self fixup not supported by Minos Object File.");
							аесли (fixup.mode = BinaryCode.Absolute) и InModule(fixup.symbol.name) то
								this := section.offset  + fixup.offset; (* location of the fixup *)
								(*
								ASSERT(this < 8000H);
								ASSERT(this >= -8000H);
								*)
								Resolve(fixup);
								patchOffset := fixup.resolved.offset + fixup.displacement;
								если (patchOffset DIV 4 >= 8000H) или (patchOffset DIV 4< -8000H)
									или (patchOffset остОтДеленияНа 4 # 0)
								то
									msg := "fixup problem: ";
									Basic.SegmentedNameToString(fixup.symbol.name, name);
									Строки8.ПодклейВСтрокуХвост(msg, name);
									Строки8.ПодклейВСтрокуХвост(msg," : ");
									Строки8.ПодклейВСтрокуРаспечаткуЦел64(msg, patchOffset);

									Basic.Error(diagnostics, module.moduleName,Basic.invalidPosition, msg);

									error := истина
								всё;
								patch := prev DIV 4 + 10000H * (patchOffset DIV 4);
								если Trace то
									D.String("fix self "); Basic.WriteSegmentedName(D.Log, section.name); D.String("+"); D.Int(fixup.offset,1);
									D.String(" -> ");
									Basic.WriteSegmentedName(D.Log, fixup.symbol.name); D.String("+"); D.Int(fixup.displacement,1) ;
									D.Ln;
								всё;
								Put32(this, patch(цел32));
								prev := this;
							иначе (* external fixup, handled in imports *)
							всё;
							fixup := fixup.nextFixup;
						кц
					всё
				кц;

				возврат prev DIV 4
			кон FixSelf;

		нач
			Global.ModuleFileName(module.module.имя,module.module.контекстA2,moduleName,ПереводыЭлементовКода.АнглоязычныйРежим (* ПРАВЬМЯ *));
			fp := fingerprinter.SymbolFP(module.module);
			w.пСтроку8˛включаяСимв0(moduleName); w.пЦел32_мз(цел32(fp.Публичная));
			w.пЦел32_мз(FixSelf()(цел32));
			Imports;
			Commands;
			Entries(l1bodyOffset);
			Data;
			Code;
		кон ObjectFile;

		(* Imports = {name:String key:Int fix:Int} 0X:Char *)
		проц Imports;
		перем name: Name; import: SyntaxTree.ОбъявлениеИмпортаМодуля; numberEntries: размерМЗ; entries: массив 256 из IntermediateCode.Section;

			проц IsFirstOccurence(l2import: SyntaxTree.ОбъявлениеИмпортаМодуля): булево; (*! inefficient *)
			перем i: SyntaxTree.ОбъявлениеИмпортаМодуля;
			нач
				i := moduleScope.первыйИмпорт;
				нцПока (i # НУЛЬ) и (i.деревоМодуля # l2import.деревоМодуля) делай
					i := i.следщОИМ;
				кц;
				возврат i = l2import
			кон IsFirstOccurence;

			проц MakeFixups(): размерМЗ;
			перем prev,this,i,instr: размерМЗ; section: Sections.Section; first: Fixup; numberFixups: размерМЗ;
			нач
				prev := 0;
				нцДля i := 1 до numberEntries делай
					section := entries[i];
					если section # НУЛЬ то (* used imported symbol *)
						numberFixups := GetFixups(diagnostics, module, section, first);
						если Trace то
							D.Int(numberFixups,1); D.String(" fixups "); Basic.WriteSegmentedName(D.Log, section.name); D.Ln;
						всё;
						нцПока first # НУЛЬ делай
							this := first.fixupSection.offset  + first.fixup.offset;
							instr := Get32(this);
							утв(prev < 10000H); утв(i < 100H);
							(*
								31 ... 24 | 23 .. 16 | 15 .. 0
								opCode | pno | next
							*)
							instr := instr остОтДеленияНа 1000000H + i * 10000H + prev DIV 4;
							Put32(this, instr(цел32));
							prev := this;
							first := first.nextFixup;
						кц;
					всё;
				кц;
				если Trace то D.String(" fixup chain starting at "); D.Int(prev,1); D.Ln всё;
				возврат prev DIV 4
			кон MakeFixups;

		нач
			import := moduleScope.первыйИмпорт;
			нцПока(import # НУЛЬ) делай
				если ~Global.IsSystemModule(import.деревоМодуля) и IsFirstOccurence(import) то
					Global.ModuleFileName(import.деревоМодуля.имя,import.деревоМодуля.контекстA2,name,ПереводыЭлементовКода.АнглоязычныйРежим (* ПРАВЬМЯ *));
					если Trace то
						D.Str("Import module : "); D.Str(name); D.Ln;
					всё;
					w.пСтроку8˛включаяСимв0(name);
					fp := fingerprinter.SymbolFP(import.деревоМодуля);
					w.пЦел32_мз(цел32(fp.Публичная));

					(* get all imported entries of imported module *)
					GetEntries(import.деревоМодуля.внутренняяОбластьВидимостиМодуля, numberEntries, entries);
					(* generate fixups to all non-zero entries *)
					w.пЦел32_мз(MakeFixups()(цел32));
				всё;
				import := import.следщОИМ;
			кц;
			w.пСимв8(0X);
		кон Imports;

		(* Commands = {name:String offset:Int} 0X:Char *)
		проц Commands;
		перем
			procedure : SyntaxTree.ОбъявлениеПроцедуры; procedureType: SyntaxTree.ВыражˉтипИзРодаПроцедур;
			p: Sections.Section; name: Name; i: размерМЗ;
		нач
			нцДля i := 0 до module.allSections.Length() - 1 делай
				p := module.allSections.GetSection(i);
				если (p.type # Sections.InitCodeSection) и (p.symbol # НУЛЬ) и (p.symbol суть SyntaxTree.ОбъявлениеПроцедуры)  то
					procedure := p.symbol(SyntaxTree.ОбъявлениеПроцедуры);
					procedureType := procedure.Тип(SyntaxTree.ВыражˉтипИзРодаПроцедур);
					если (SyntaxTree.ЧтениеОбщедоступно в procedure.праваДоступа) и ~(procedure.встраиваемаяЛи¿) и ~(procedureType.делегатЛи¿) и (procedureType.квоПараметров = 0) то
						procedure.ДайИмяВВидеСтроки(name);
						если Trace то
							D.Str("Command : "); D.Str(name); D.Str(" @ "); D.Int(p.offset ,1);
						всё;
						w.пСтроку8˛включаяСимв0(name);
						w.пЦел32_мз((p.offset DIV 4)(цел32));
						если Trace то D.Ln всё
					всё
				всё
			кц;
			w.пСимв8(0X);
		кон Commands;

		(* noEntries:Int BodyEntry {entry:Int32}:noEntries *)
		проц Entries(bodyOffset: размерМЗ);
		перем
			i,numberEntries: размерМЗ; entry: массив 256 из IntermediateCode.Section; (* more is not allowed anyway in the runtime system *)
		нач
			GetEntries(moduleScope, numberEntries, entry);
			w.пЦел32_мз(numberEntries(цел32));
			w.пЦел32_мз(0); (* body entry: body is fixed at position 0, cf. MakeSectionOffsets *)
			нцДля i := 1 до numberEntries делай
				утв(entry[i].offset остОтДеленияНа 4 = 0);
				w.пЦел32_мз((entry[i].offset)(цел32) DIV 4); (* entries here must be byte wise because jumps take place with absolute fixup - I cannot distinguish here *)
			кц;
		кон Entries;

		(* dataSize:Int32 *)
		проц Data;
		нач
			w.пЦел32_мз(dataSize(цел32));
		кон Data;

		(* codeLen:Int32 {code:Int32}:codeLen *)
		проц Code;
		перем i: размерМЗ;
		нач
			утв(codeSize остОтДеленияНа 4 = 0);
			w.пЦел32_мз((codeSize DIV 4)(цел32));
			нцДля i := 0 до codeSize-1 делай
				w.пСимв8(code[i]);
			кц;
		кон Code;

	нач
		error := ложь;
		moduleScope := module.module.внутренняяОбластьВидимостиМодуля;
		нов(fingerprinter);
		MakeSectionOffsets(module,dataSize,codeSize,bodyOffset,code); (* --> all sections are now assembled as one piece in code *)
		ObjectFile(bodyOffset);
		w.ПротолкниБуферВПоток;
		возврат ~error
	кон WriteObjectFile;

	проц Get*(): Formats.ObjectFileFormat;
	перем objectFileFormat: ObjectFileFormat;
	нач нов(objectFileFormat); возврат objectFileFormat
	кон Get;

кон FoxMinosObjectFile.

System.Free FoxMinosObjectFile ~

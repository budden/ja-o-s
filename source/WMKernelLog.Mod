модуль WMKernelLog; (** AUTHOR "TF"; PURPOSE "Kernel log window"; *)

использует
	Modules, KernelLogger, Строки8, WMGraphics, WMComponents, DE := WMDocumentEditor,
	WMRestorable, WMMessages, WMWindowManager;

конст
	WindowWidth = 640;
	WindowHeight = 420;

тип
	Window = окласс (WMComponents.FormWindow)
	перем
		out : DE.Editor;

		проц &New*(c : WMRestorable.Context);
		нач
			нов(out); out.alignment.Set(WMComponents.AlignClient);
			out.SetToolbar(DE.StoreButton + DE.ClearButton + DE.SearchButton + DE.WrapButton);
			out.editor.multiLine.Set(истина);

			если (c # НУЛЬ) то
				Init(c.r - c.l, c.b - c.t, ложь);
			иначе
				Init(WindowWidth, WindowHeight, ложь);
			всё;

			SetContent(out);
			SetTitle(Строки8.ЯвиУСтроку("Kernel log"));
			SetIcon(WMGraphics.LoadImage("WMIcons.tar://WMKernelLog.png", истина));

			out.SetText(KernelLogger.kernelLog);
			KernelLogger.kernelLog.AcquireRead;
			out.editor.tv.End(истина, ложь); (*cursor.SetPosition(KernelLogger.kernelLog.GetLength());*)
			KernelLogger.kernelLog.ReleaseRead;

			если c # НУЛЬ то
				WMRestorable.AddByContext(сам, c);
			иначе
				WMWindowManager.DefaultAddWindow(сам)
			всё
		кон New;

		проц {перекрыта}Handle*(перем x : WMMessages.Message);
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) то
				если (x.ext суть WMRestorable.Storage) то
					x.ext(WMRestorable.Storage).Add("WMKernelLog", "WMKernelLog.Restore", сам, НУЛЬ)
				иначе Handle^(x)
				всё
			иначе Handle^(x)
			всё
		кон Handle;

		проц {перекрыта}Close*;
		нач
			Close^;
			winstance := НУЛЬ
		кон Close;

	кон Window;

перем winstance : Window;

проц Open*;
нач
	если winstance = НУЛЬ то
		нов(winstance, НУЛЬ)
	иначе
		WMWindowManager.DefaultBringToView(winstance, истина)
	всё;
кон Open;

проц Restore* (context : WMRestorable.Context);
нач
	нов(winstance, context)
кон Restore;

проц Cleanup;
нач
	если winstance # НУЛЬ то winstance.Close всё
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон WMKernelLog.

WMKernelLog.Open ~
System.Free WMKernelLog ~

KernelLogger.Mod
KernelLogger.Start ~
KernelLogger.Stop ~



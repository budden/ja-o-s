модуль UsbTouchscreen;  (** AUTHOR "felixf"; PURPOSE "USB Touchscreen drivers"; *)
(* 2014.04.02 felixf: USB touchscreen moved back as generic feature to HIDDrivers *)

использует
	UsbHidDriver, Displays, Plugins, Objects, Modules;

перем display: Displays.Display;

проц GetDisplayDimensions (перем w,h: цел32);
перем o: Plugins.Plugin;
нач
	если display = НУЛЬ то
		o := Displays.registry.Get("");
		если o # НУЛЬ то display := o(Displays.Display) всё;
	всё;
	если display # НУЛЬ то
		w := display.width; h := display.height
	иначе
		w := 1024; h := 768
	всё;
кон GetDisplayDimensions;

проц Cleanup;
нач
	UsbHidDriver.InstallDisplayDimensions(НУЛЬ);
кон Cleanup;

проц Install*;
нач
	UsbHidDriver.InstallDisplayDimensions(GetDisplayDimensions);
кон Install;


нач
	display := НУЛЬ;
	Modules.InstallTermHandler(Cleanup);
	Install;
кон UsbTouchscreen.

System.Free UsbTouchscreen ~

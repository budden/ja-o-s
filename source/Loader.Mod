модуль Loader; (** AUTHOR "fof"; PURPOSE "Object File Loader"; *)

(* cf. Linker *)

использует НИЗКОУР, ЛогЯдра, Modules, Потоки, Files, D := ЛогЯдра, GenericLinker, ObjectFile, Diagnostics, StringPool, Трассировка, ЭВМ;

конст
	Ok = 0;
	LinkerError=3400;
	FileNotFound = 3401;
	CommandTrapped* = 3904; (* cf module Commands *)

	TraceLoading = ложь;
	TraceBlocks = ложь;
тип

	HashEntryIntInt = запись
		key,value: размерМЗ;
	кон;
	HashIntArray = укль на массив из HashEntryIntInt;

	HashEntryIntAny = запись
		key: размерМЗ; value: динамическиТипизированныйУкль;
	кон;

	HashIntAnyArray = укль на массив из HashEntryIntAny;

	HashTableInt = окласс
	перем
		table: HashIntArray;
		size: размерМЗ;
		used-: размерМЗ;
		maxLoadFactor: вещ32;

		(* Interface *)

		проц & Init* (initialSize: размерМЗ);
		нач
			утв(initialSize > 2);
			нов(table, initialSize);
			size := initialSize;
			used := 0;
			maxLoadFactor := 0.75;
		кон Init;

		проц Put*(key: размерМЗ; value: размерМЗ);
		перем hash: размерМЗ;
		нач
			утв(key # 0);
			утв(used < size);
			hash := HashValue(key);
			если table[hash].key = 0 то
				увел(used, 1);
			всё;
			table[hash].key := key;
			table[hash].value := value;
			если (used / size) > maxLoadFactor то Grow всё;
		кон Put;

		проц Get*(key: размерМЗ):размерМЗ;
		нач
			возврат table[HashValue(key)].value;
		кон Get;

		проц Has*(key: размерМЗ):булево;
		нач
			возврат table[HashValue(key)].key = key;
		кон Has;

		проц Length*():размерМЗ;
		нач возврат used; кон Length;

		проц Clear*;
		перем i: размерМЗ;
		нач нцДля i := 0 до size - 1 делай table[i].key := 0; кц; кон Clear;

		(* Internals *)

		проц HashValue(key: размерМЗ):размерМЗ;
		перем value, h1, h2, i: размерМЗ;
		нач
			i := 0;
			value := key;
			h1 := key остОтДеленияНа size;
			h2 := 1; (* Linear probing *)
			нцДо
				value := (h1 + i*h2) остОтДеленияНа size;
				увел(i);
			кцПри((table[value].key = 0) или (table[value].key = key) или (i > size));
			утв((table[value].key = 0) или (table[value].key = key));
			возврат value;
		кон HashValue;

		проц Grow;
		перем oldTable: HashIntArray; oldSize, i, key: размерМЗ;
		нач
			oldSize := size;
			oldTable := table;
			Init(size*2);
			нцДля i := 0 до oldSize-1 делай
				key := oldTable[i].key;
				если key # 0 то
					Put(key, oldTable[i].value);
				всё;
			кц;
		кон Grow;

	кон HashTableInt;

	HashTableIntAny = окласс
	перем
		table: HashIntAnyArray;
		size: размерМЗ;
		used-: размерМЗ;
		maxLoadFactor: вещ32;

		(* Interface *)

		проц & Init* (initialSize: размерМЗ);
		нач
			утв(initialSize > 2);
			нов(table, initialSize);
			size := initialSize;
			used := 0;
			maxLoadFactor := 0.75;
		кон Init;

		проц Put*(key: размерМЗ; value: динамическиТипизированныйУкль);
		перем hash: размерМЗ;
		нач
			утв(key # 0);
			утв(used < size);
			hash := HashValue(key);
			если table[hash].key = 0 то
				увел(used, 1);
			всё;
			table[hash].key := key;
			table[hash].value := value;
			если (used / size) > maxLoadFactor то Grow всё;
		кон Put;

		проц Get*(key: размерМЗ):динамическиТипизированныйУкль;
		нач
			возврат table[HashValue(key)].value;
		кон Get;

		проц Has*(key: размерМЗ):булево;
		нач
			возврат table[HashValue(key)].key = key;
		кон Has;

		проц Length*():размерМЗ;
		нач возврат used; кон Length;

		проц Clear*;
		перем i: размерМЗ;
		нач нцДля i := 0 до size - 1 делай table[i].key := 0; кц; кон Clear;

		(* Interface for integer values *)

		(* Internals *)

		проц HashValue(key: размерМЗ):размерМЗ;
		перем value, h1, h2, i:размерМЗ;
		нач
			i := 0;
			value := key;
			h1 := key остОтДеленияНа size;
			h2 := 1; (* Linear probing *)
			нцДо
				value := (h1 + i*h2) остОтДеленияНа size;
				увел(i);
			кцПри((table[value].key = 0) или (table[value].key = key) или (i > size));
			утв((table[value].key = 0) или (table[value].key = key));
			возврат value;
		кон HashValue;

		проц Grow;
		перем oldTable: HashIntAnyArray; oldSize, i, key: размерМЗ;
		нач
			oldSize := size;
			oldTable := table;
			Init(size*2);
			нцДля i := 0 до oldSize-1 делай
				key := oldTable[i].key;
				если key # 0 то
					Put(key, oldTable[i].value);
				всё;
			кц;
		кон Grow;

	кон HashTableIntAny;

	Data=запись size, alignment: размерМЗ; bytes: Modules.Bytes; address: адресВПамяти; кон;

	Arrangement* = окласс (GenericLinker.Arrangement);
	перем
		code, data: Data;

		проц & InitArrangement;
		нач InitData(code); InitData(data);
		кон InitArrangement;

		проц {перекрыта}Preallocate*(конст section: ObjectFile.Section);
		нач
			утв(section.unit = 8);
			утв(section.bits.GetSize() остОтДеленияНа 8 = 0);
			утв(section.type # ObjectFile.InitCode);
			если section.type в {ObjectFile.Code, ObjectFile.BodyCode} то
				DoPreallocate(section, code);
			иначе утв (section.type в {ObjectFile.Const, ObjectFile.Data});
				DoPreallocate(section, data);
			всё;
		кон Preallocate;

		проц {перекрыта}Allocate* (конст section: ObjectFile.Section): GenericLinker.Address;
		перем adr: GenericLinker.Address;
		нач
			если section.type в {ObjectFile.Code, ObjectFile.BodyCode} то
				adr := DoAllocate(section, code);
			иначе утв(section.type в {ObjectFile.Const, ObjectFile.Data});
				adr := DoAllocate(section, data);
			всё;

			возврат adr;
		кон Allocate;

		проц {перекрыта}Patch* (pos, value: GenericLinker.Address; offset, bits, unit: ObjectFile.Bits);
		перем char: симв8;
		нач
			утв(bits остОтДеленияНа 8 = 0);
			утв(unit = 8);

			нцПока bits > 0 делай
				char := симв8ИзКода(value);
				НИЗКОУР.запишиОбъектПоАдресу(pos, char);
				value := value DIV 256;
				умень(bits,8); увел(pos,1);
			кц;

		кон Patch;

	кон Arrangement;

	ModuleList=окласс
	перем
		hash: HashTableIntAny;

		проц &Init;
		нач
			нов(hash,128);
		кон Init;

		проц ThisModule(module: Modules.Module): HashTableInt;
		перем modList: HashTableInt;
			any: динамическиТипизированныйУкль;

			проц TraverseScopes(конст scope: Modules.ExportDesc; level: размерМЗ);
			перем i: размерМЗ;
			нач
				если (level > 2) то возврат всё;
				если (scope.fp # 0) то
					modList.Put(размерМЗ(scope.fp), scope.adr)
				всё;
				нцДля i := 0 до scope.exports-1 делай
					если scope.dsc # НУЛЬ то TraverseScopes(scope.dsc[i],level+1) всё;
				кц;
			кон TraverseScopes;

		нач{единолично}
			если hash.Has(НИЗКОУР.подмениТипЗначения(размерМЗ, module)) то
				any := hash.Get(НИЗКОУР.подмениТипЗначения(размерМЗ,module));
				modList := any(HashTableInt);
			иначе
				нов(modList,256);  TraverseScopes(module.export,0);
				hash.Put(НИЗКОУР.подмениТипЗначения(размерМЗ,module), modList);
				возврат modList
			всё;
			возврат modList;
		кон ThisModule;

	кон ModuleList;

	Linker = окласс (GenericLinker.Linker)
	перем
		moduleName: ObjectFile.SegmentedName;
		importBlock: GenericLinker.Block;
		hash: HashTableIntAny;

		moduleBlock: массив 32 из GenericLinker.Block;
		numberModuleBlocks: размерМЗ;

		проц &InitLinkerX* (diagnostics: Diagnostics.Diagnostics; log: Потоки.Писарь; code, data: GenericLinker.Arrangement; конст name: массив из симв8);
		нач
			ObjectFile.StringToSegmentedName(name, moduleName);
			InitLinker(diagnostics, log, GenericLinker.UseAllButInitCode (* strip init code *), code, data);
			нов(importBlock);
			нов(hash,256); (* hash for blocks *)
			numberModuleBlocks := 0;
		кон InitLinkerX;

		(* oerwritten functionality of generic linker *)
		проц {перекрыта}FindBlock*(конст identifier: ObjectFile.Identifier): GenericLinker.Block;
		перем block: GenericLinker.Block; any: динамическиТипизированныйУкль;
		нач
			если identifier.fingerprint = 0 то (* not identifiable via fingerprint *)
				block := FindBlock^(identifier);
			иначе
				any := hash.Get(размерМЗ(identifier.fingerprint));
				если any # НУЛЬ то block := any(GenericLinker.Block)  (* local block *) всё;
				если (block # НУЛЬ) и (block.identifier.name # identifier.name) то (* local block, false or duplicate fingerprint *)
					block := FindBlock^(identifier)
				всё;
			всё;
			возврат block;
		кон FindBlock;

		проц {перекрыта}ExportBlock*(block: GenericLinker.Block);
		нач
			если block.identifier.fingerprint # 0 то
				hash.Put(размерМЗ(block.identifier.fingerprint), block)
			всё;
			если (block.identifier.name[0] >= 0) и (block.identifier.name[1] >= 0)  то
				если (block.identifier.name[1] = InternalModuleName) и (block.identifier.name[2]<0) или 
				(block.identifier.name[2] = InternalModuleName) и (block.identifier.name[3] < 0) то
					moduleBlock[numberModuleBlocks] := block;
					увел(numberModuleBlocks);
				всё;
			всё;
		кон ExportBlock;

		проц {перекрыта}ImportBlock*(конст fixup: ObjectFile.Fixup): GenericLinker.Block;
		перем name: Modules.Name; res: целМЗ;
			msg: массив 128 из симв8; module: Modules.Module; adr: адресВПамяти; m: HashTableInt;
			s: ObjectFile.SectionName; isModule: булево; identifier: ObjectFile.Identifier;
			fp: цел64;

			проц CheckName(n: StringPool.Index;  name {неОтслСборщиком}: Modules.DynamicName): целМЗ;
			перем s: ObjectFile.SectionName; i: размерМЗ;
			нач
				если name = НУЛЬ то возврат -1 всё;
				StringPool.GetString(n, s);
				i := 0;
				нцПока (s[i] # 0X) и (name[i] # 0X) и (s[i] = name[i]) делай
					увел(i);
				кц;
				возврат кодСимв8(s[i]) - кодСимв8(name[i]);
			кон CheckName;

			(* stupid implementation: just search for fp in all exports *)
			проц CheckScope(конст scope: Modules.ExportDesc; level: размерМЗ): адресВПамяти;
			перем adr,lo,hi,m,res: размерМЗ;
			нач
				adr := 0;
				(* export names are sorted, binary search: *)
				lo := 0; hi := scope.exports-1;
				нцПока (lo <= hi) делай
				 	m := (lo + hi) DIV 2;
					res := CheckName(identifier.name[level], scope.dsc[m].name);
					если res = 0 то
						если (level = длинаМассива(identifier.name)-1) или (identifier.name[level+1] <= 0) то
							fp := scope.dsc[m].fp;
							возврат scope.dsc[m].adr
						иначе
							возврат CheckScope(scope.dsc[m], level+1);
						всё;
					аесли res > 0 то lo := m+1;
					иначе hi := m-1;
					всё;
				кц;
				возврат 0;
			кон CheckScope;


		нач
			identifier := fixup.identifier;

			если ObjectFile.IsPrefix(moduleName, identifier.name) то
				D.пСтроку8("circular import while trying to fetch ");
				s := identifier.name; D.пСтроку8(s);
				D.пВК_ПС;
				возврат НУЛЬ
			всё;

			StringPool.GetString(identifier.name[0], name);
			isModule := identifier.name[1] = InternalModuleName;

			module := Modules.ThisModule(name,res,msg);
			если module = НУЛЬ то
				D.пСтроку8("could not get module while importing "); D.пСтроку8(name); D.пВК_ПС;
				возврат НУЛЬ
			всё;
			если isModule то
				adr := НИЗКОУР.подмениТипЗначения(адресВПамяти, module) - fixup.patch[0].displacement;
			иначе
				m := moduleList.ThisModule(module);
				утв(module # НУЛЬ);
				adr := CheckScope(module.export,1(*level*) );
			всё;

			если adr = 0 то
				возврат НУЛЬ;
			иначе (* found *)
				importBlock.identifier.fingerprint := fp; importBlock.address := adr
			всё;
			возврат importBlock
		кон ImportBlock;

	кон Linker;

перем
	moduleList: ModuleList;
	InternalModuleName: StringPool.Index;

	проц InitData(перем data: Data);
	нач
		data.address := 0; data.size := 0; data.alignment := 0; data.bytes := НУЛЬ;
	кон InitData;



	проц DoPreallocate(конст section: ObjectFile.Section; перем data: Data);
	нач
		утв(section.bits.GetSize() остОтДеленияНа 8 = 0);
		если section.alignment > 0 то
			увел(data.size, (-data.size) остОтДеленияНа section.alignment);
			если section.alignment > data.alignment то
				data.alignment := section.alignment;
			всё;
		всё;
		увел(data.size, section.bits.GetSize() DIV 8);
	кон DoPreallocate;

	проц DoAllocate(конст section: ObjectFile.Section; перем data: Data): адресВПамяти;
	перем address: адресВПамяти; size: размерМЗ;
	нач
		если (data.bytes = НУЛЬ) или (длинаМассива(data.bytes) # data.size + data.alignment) то
			нов(data.bytes, data.size + data.alignment);
			data.address := адресОт(data.bytes[0]);
			если data.alignment > 0 то
				увел(data.address, (-data.address) остОтДеленияНа data.alignment);
			всё;
		всё;

		если section.alignment > 0 то
			увел(data.address, (-data.address) остОтДеленияНа section.alignment);
		всё;

		address := data.address;
		size := section.bits.GetSize();
		section.bits.CopyTo(address, size);
		увел(data.address, size DIV 8);
		возврат address
	кон DoAllocate;

	проц LoadObj*(конст name, fileName: массив из симв8;  перем res: целМЗ; перем msg: массив из симв8): Modules.Module;
	перем
		file: Files.File; reader: Files.Reader; linker: Linker;
		arrangement: Arrangement; diagnostics: Diagnostics.StreamDiagnostics; stringWriter: Потоки.ПисарьВСтроку8ФиксированногоРазмера;
		module: Modules.Module; heapBlockAdr,moduleAdr: адресВПамяти;
		sname: ObjectFile.SectionName;
		Log, debug: Потоки.Писарь;
		i: размерМЗ;
	нач
		file := Files.Old(fileName);

		если file # НУЛЬ то
			если TraceLoading то Трассировка.пСтроку8("loading"); Трассировка.пСтроку8(fileName); Трассировка.пВК_ПС всё;
			res := Ok; msg[0] := 0X;
			Files.OpenReader(reader, file, 0);
			нов(arrangement); нов(stringWriter,256);
			Потоки.НастройПисаря( Log, ЛогЯдра.ЗапишиВПоток );
			нов(diagnostics,Log);
			если TraceBlocks то debug := Log иначе debug := НУЛЬ всё;
			нов(linker, diagnostics, debug, arrangement, arrangement,name);
			если TraceLoading то Трассировка.пСтроку8("before linking"); Трассировка.пВК_ПС всё;
			GenericLinker.Load (reader, linker);
			если (linker.offers # НУЛЬ) и (длинаМассива(linker.offers) > 1) то (* only when several modules in one object file *)
				(* remove all already loaded modules from linker sections *)
				нцДля i := 0 до длинаМассива(linker.offers)-1 делай
					sname := linker.offers[i];
					module := Modules.ModuleByName(sname);
					трассируй(sname, module);
					если module # НУЛЬ то
						linker.RemovePrefixed(linker.offers[i]);
					всё;
				кц;

			всё;
			если ~linker.error то linker.EnterBlocks всё;
			если ~linker.error то linker.Resolve всё;
			если ~linker.error то linker.Link всё;
			(*D.Update;*)
			если ~linker.error то
				если TraceLoading то Трассировка.пСтроку8("linking done"); Трассировка.пВК_ПС всё;
				нцДля i := 0 до linker.numberModuleBlocks-1 делай
					moduleAdr := linker.moduleBlock[i].address;
					#если ~COOP то
						НИЗКОУР.прочтиОбъектПоАдресу(moduleAdr+2*размер16_от(адресВПамяти)+1*размер16_от(размерМЗ), moduleAdr);
						НИЗКОУР.прочтиОбъектПоАдресу(moduleAdr-2*размер16_от(адресВПамяти), heapBlockAdr);
						утв(heapBlockAdr = linker.moduleBlock[i].address+2*размер16_от(адресВПамяти));
					#кон
					module := НИЗКОУР.подмениТипЗначения(Modules.Module,moduleAdr);
					module.data := arrangement.data.bytes;
					module.code := arrangement.code.bytes;
 					module.sb := 0;
					(*
						careful: when GC uses a heuristic for pointer detection on the stack, it will not
						trace the module because the module is not reachable as a heap block in a sweep
						Therefore the code and data array has to be secured in addition.
						Here this is made sure to enter the module in the data structure before returning it.
					*)
					Modules.Initialize(module);
				кц;
				Log.ПротолкниБуферВПоток;
			иначе module := НУЛЬ; res := LinkerError; stringWriter.ПротолкниБуферВПоток; stringWriter.ДайПрочитанное˛сколькоПоместитсяИСимвол0(msg);

			всё;
		иначе
			res := FileNotFound;  копируйСтрокуДо0(fileName, msg);  Modules.Append(" not found", msg)
		всё;
		если res # Ok то module := НУЛЬ всё;
		если (res # Ok) и (res # FileNotFound) то D.пСтроку8(msg);D.пВК_ПС всё;
		возврат module
	выходя
		res := CommandTrapped;
		возврат НУЛЬ
	кон LoadObj;

	проц Install*;
	перем extension: массив 32 из симв8;
	нач
		ЭВМ.ДайЗначениеКлючаКонфигурацииЯОС("ObjectFileExtension", extension);
		если extension = "" то
			копируйСтрокуДо0(ЭВМ.РасширениеОбъектногоФайлаПоУмолч, extension)
		всё;
		Modules.AddLoader(extension, LoadObj);
	кон Install;

	проц Remove*;
	перем extension: массив 32 из симв8;
	нач
		ЭВМ.ДайЗначениеКлючаКонфигурацииЯОС("ObjectFileExtension", extension);
		если extension = "" то
			копируйСтрокуДо0(ЭВМ.РасширениеОбъектногоФайлаПоУмолч, extension)
		всё;
		Modules.RemoveLoader(extension,LoadObj);
	кон Remove;

	нач
		Modules.InstallTermHandler(Remove);
		StringPool.GetIndex("@Module",InternalModuleName);
		нов(moduleList);
		Install;
	кон Loader.


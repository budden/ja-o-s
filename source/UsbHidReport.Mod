модуль UsbHidReport; (** AUTHOR "ottigerm"; PURPOSE "generating HID reports" *)
(**
 * Bluebottle USB HID Report Module
 *
 * This is the module generating hid reports
 *
 *
 * History:
 *
 *	02.06.2006	History started (ottigerm)
 *)

использует  НИЗКОУР, UsbHidUP, ЛогЯдра;


конст
	Debug*					= ложь;

	UseUsageDictionaryExt*	= истина;

	MainTypeInput			=   0;
	MainTypeOutput 		=   1;
	MainTypeFeature		=   2;

	UndefinedState*		= -1;

тип

	MainType* = цел32;

	(*for storing collections*)
	HidCollection*= укль на запись
		type*:				цел32;
		usagePage*:		цел32;
		usage*:				цел32;
		firstCollection:		HidCollection;
		lastCollection:		HidCollection;
		firstReport*:		HidReport;
		lastReport:			HidReport;
		next:				HidCollection;
	кон;

	UsageTuple* = укль на запись
		usageID*:			цел32;
		usagePage*:		цел32;
		usageValue*:		цел32;
	кон;

	UsageDictElement*= запись
		firstUsageID*:			цел32;
		nofFollowing*:			цел32;
		otherUsagePage*:		цел32;
	кон;

	UsageDictionary*= укль на запись
		elements*:			укль на массив из UsageDictElement;
	кон;

	PtrToUsageTupleArr*= укль на массив из UsageTuple;

	HidReport* = укль на запись
		next*:				HidReport;
		reportID*:			цел32;

		usagePage*:		цел32;
		(*holding usageID and usageValue in each entry*)
		usages*:			PtrToUsageTupleArr;

		mainState*:			цел32;
		(*reportOffset*:		SIGNED32;		(*bit offset in the report*)
		*)reportSize*:			цел32;
		reportCount*:		цел32;
		reportType*:		MainType;		(*Input/Output/Feature Item*)
		logicalMinimum*:	цел32;
		logicalMaximum*:	цел32;
		physicalMinimum*:	цел32;
		physicalMaximum*:	цел32;
		unitExponent*:		цел32;
		unit*:				цел32;

		(*this field is used, when the device does not send the values for each usage but sends, which usages are used -> when mainState has Array flag set*)
		supportedUsages*:	UsageDictionary;
	кон;

	HidReportIterator*= укль на запись
		report*:				HidReport;
		next*:				HidReportIterator;
	кон;

	(*used for keeping parent collections, when new Collection found*)
	ParentStackItem= укль на запись
		value:				HidCollection;
		next:				ParentStackItem;
	кон;


	(*used for keeping all references, where to store the reports*)
	ReportItem*= укль на запись
		(*bit index in the hid report*)
		index:						цел32;
		(*reportID*)
		reportID*:					цел32;
		(*the size of each usage*)
		reportItemSize*:			цел32;
		(*the number of usages to read*)
		reportItemCount*:			цел32;
		(*where to store the values*)
		values*:						PtrToUsageTupleArr;
		next*:						ReportItem;
	кон;

	(*holding reportItems*)
	ReportItemQueue* = укль на запись
		first*:						ReportItem;
		bitsAlreadyUsed*:			цел32;
	кон;

	(*stack, holding parent tree of current collection*)
	HIDParentList*=окласс
	перем
		first : ParentStackItem;

		(*to put item on top of the parent stack*)
		проц PushFront*(value: HidCollection);
		перем cur : ParentStackItem;
		нач
			если (first = НУЛЬ) то
				нов(first);
				first.value := value;
			иначе
				нов(cur);
				cur.value := value;
				cur.next := first;
				first := cur;
			всё;
		кон PushFront;

		(*to get  the top item from parent stack*)
		проц PopFront*(): HidCollection;
		перем ret : HidCollection;
		нач
			если(first=НУЛЬ) то
				ret := НУЛЬ;
			иначе
				ret := first.value;
				first := first.next;
			всё;
			возврат ret;
		кон PopFront;
	кон HIDParentList;


	(*stores the collections with its reports in a n-ary tree*)
	HidReportManager*=окласс
	перем
		rootCollection, current:	HidCollection;
		parentList:				HIDParentList;
		reportItemQueue:		ReportItemQueue;
		reportIterator:			HidReportIterator;

		(*appends a collection to the current collection, stores the current collection on the stack and goes to new collection
		* 	param: 	type		like physical, application...
		*			usagePage	the collection's usage page
		*			usage		the collection's usage
		*)
		проц BeginCollection*(type, usagePage, usage: цел32);
		перем newCollection : HidCollection;
		нач
			нов(newCollection);
			newCollection.type 			:= type;
			newCollection.usagePage 	:= usagePage;
			newCollection.usage 		:= usage;

			если(current.firstCollection=НУЛЬ) то
				current.firstCollection := newCollection;
				current.lastCollection := current.firstCollection;
			иначе
				current.lastCollection.next := newCollection;
				current.lastCollection := newCollection;
			всё;
			parentList.PushFront(current);
			current := newCollection;
		кон BeginCollection;

		(*closes the current collection
		*)
		проц EndCollection*;
		перем ret : HidCollection;
		нач
			утв(current#rootCollection);
			ret := parentList.PopFront();
			current := ret;
		кон EndCollection;


		(*finds the collection with usagePage and usage from the rootCollection including subnodes
		* 	param: 	usagePage		usagePage to find
		*			usage 			usage to find
		*	return:	hidCollection	with usagePage and usage if found
		*			NIL				ohterwise
		*)
		проц GetCollection*(usagePage, usage: цел32): HidCollection;
		перем collIterator, result : HidCollection;
		нач
			утв(rootCollection.firstCollection#НУЛЬ);
			collIterator := rootCollection.firstCollection;
			нцПока (collIterator#НУЛЬ) делай
				result := FindColl(collIterator, usagePage, usage);
				если result#НУЛЬ то
					возврат result;
				всё;
				collIterator := collIterator.next;
			кц;
			возврат НУЛЬ;
		кон GetCollection;


		(*finds the collection with usagePage and usage from hidCollection
		* 	param: 	hidColl	collection where to start searching
		*			usagPage		usagePage to find
		*			usage			usage to find
		*	return:	hidCollection 	with usagePage and usage if found
		*			NIL				otherwise
		*)
		проц FindColl(hidColl: HidCollection; usagePage, usage: цел32): HidCollection;
		перем rv : HidCollection;
		нач
			если (hidColl=НУЛЬ) то возврат НУЛЬ; всё;
			rv := НУЛЬ;
			если Debug то
				ЛогЯдра.пСтроку8("parsing item: UsagePage"); ЛогЯдра.пЦел64(hidColl.usagePage,5); ЛогЯдра.пСтроку8(" Usage("); ЛогЯдра.пЦел64(hidColl.usage,0); ЛогЯдра.пСтроку8("):"); UsbHidUP.PrintUsagePage(hidColl.usagePage,hidColl.usage); ЛогЯдра.пВК_ПС;
			всё;
			если ((hidColl.usagePage=usagePage) и  (hidColl.usage=usage)) то
				rv := hidColl;
			иначе
				если (hidColl.firstCollection#НУЛЬ) то
					rv := FindColl(hidColl.firstCollection, usagePage, usage);
					если((rv=НУЛЬ) и (hidColl.next#НУЛЬ)) то
						rv := FindColl(hidColl.next, usagePage, usage);
					всё;
				всё;
			всё;
			возврат rv;
		кон FindColl;

		(*finds the usage with usagePage in the hidCollection
		* 	param: 	usagPage		usagePage to find
		*			usage			usage to find
		*			hidCollection	from where to find the usage
		*			report			to store the hidReport, where the usage was found. NIL if not found
		*	return:	usageTuple		the reference to the usageTuple with usage and usagePage
		*			NIL				otherwise
		*)
		проц GetUsage*(usagePage, usage: цел32; hidCollection: HidCollection; перем report: HidReport): UsageTuple;
		перем
			cursor		: HidReport;
			usageTuple	: UsageTuple;
			subColl		: HidCollection;
			i,
			tupleLength	: размерМЗ;
		нач
			утв(hidCollection#НУЛЬ);
			cursor := hidCollection.firstReport;
			нцПока(cursor#НУЛЬ) делай
				если((cursor.usagePage= usagePage) и (cursor.usages#НУЛЬ)) то
					tupleLength := длинаМассива(cursor.usages);
					нцДля i:= 0 до tupleLength-1 делай
						если (cursor.usages[i].usageID=usage) то
							usageTuple := cursor.usages[i];
							report := cursor;
							возврат usageTuple;
						всё;
					кц;
					если UseUsageDictionaryExt то
						если cursor.supportedUsages # НУЛЬ то
							нцДля i:=0 до длинаМассива(cursor.supportedUsages.elements)-1 делай
								если (cursor.supportedUsages.elements[i].firstUsageID<=usage) и
									(cursor.supportedUsages.elements[i].firstUsageID+
									cursor.supportedUsages.elements[i].nofFollowing>=usage) то
									usageTuple := cursor.usages[0];
									report := cursor;
									если Debug то
										ЛогЯдра.пСтроку8("searching in dictionary: firstUsage, lastUsage, val");
										ЛогЯдра.пЦел64(cursor.supportedUsages.elements[i].firstUsageID,10);
										ЛогЯдра.пЦел64(cursor.supportedUsages.elements[i].nofFollowing+cursor.supportedUsages.elements[i].firstUsageID,10);
										ЛогЯдра.пЦел64(usage,10);
									всё;
									ЛогЯдра.пВК_ПС;
									возврат usageTuple;
								всё;
							кц;
						всё;
					всё;
				всё;
				cursor := cursor.next;
			кц;
			subColl := hidCollection.firstCollection;
			нцПока(subColl#НУЛЬ) делай
				usageTuple := GetUsage(usagePage, usage, subColl, report);
				если (usageTuple#НУЛЬ) то
					возврат usageTuple;
				иначе
					subColl := subColl.next;
				всё;
			кц;
			возврат НУЛЬ;
		кон GetUsage;

		(*creates a usageTuple with the id which was found at position index in the dictionary dict
		* DOES NOT RETURN REFERENCE TO ORIGINAL USAGETUPLE
		* 	param: 	index			position to search
		*			dict				where to search
		*	return:	new usageTuple
		*)
		проц GetDictKey*(index: цел32; dict: UsageDictionary):UsageTuple;
		перем
			i	: размерМЗ;
			counter	: цел32;
			rv			: UsageTuple;
		нач
			утв(dict.elements#НУЛЬ);
			нов(rv);
			нцДля i:=0 до длинаМассива(dict.elements)-1 делай
				если (index>counter+dict.elements[i].nofFollowing) то
					если Debug то
						ЛогЯдра.пСтроку8("looking in "); ЛогЯдра.пЦел64(dict.elements[i].firstUsageID,0); ЛогЯдра.пСтроку8("..");
						ЛогЯдра.пЦел64(dict.elements[i].firstUsageID+dict.elements[i].nofFollowing,0);ЛогЯдра.пВК_ПС;
					всё;
					counter := counter + 1 + dict.elements[i].nofFollowing;
				иначе
					если Debug то
						ЛогЯдра.пСтроку8("found in interval "); ЛогЯдра.пЦел64(dict.elements[i].firstUsageID,0); ЛогЯдра.пСтроку8("..");
						ЛогЯдра.пЦел64(dict.elements[i].firstUsageID+dict.elements[i].nofFollowing,0);ЛогЯдра.пВК_ПС;
					всё;
					rv.usageID := dict.elements[i].firstUsageID+index-counter;
					rv.usagePage := dict.elements[i].otherUsagePage;
					возврат rv;
				всё;
			кц;
			возврат rv;
		кон  GetDictKey;

		(*calculates the length of a dictionary
		* 	param: 	dict 			dictionary to calculate
		*	return:	length of the dictionary
		*)
		проц DictSize*(dict: UsageDictionary):цел32;
		перем sum: цел32; i: размерМЗ;
		нач
			утв(dict.elements#НУЛЬ);
			нцДля i:=0 до длинаМассива(dict.elements)-1 делай
				sum := sum + 1 + dict.elements[i].nofFollowing;
			кц;
			возврат sum;
		кон DictSize;

		(* return true if the current collection is the root collection
		*	return:	TRUE			if the current collection is the root collection
		*			FALSE			otherwise
		*)
		проц OnTopLevel*(): булево;
		нач
			возврат current=rootCollection;
		кон OnTopLevel;

		(*add report to the current collection
		* 	param:	report 	report to add
		*)
		проц AddReport*(report : HidReport);
		перем
			newReportItem:	ReportItem;
		нач
			если (current.firstReport=НУЛЬ) то
				current.firstReport:= report;
				current.lastReport:= current.firstReport;
			иначе
				current.lastReport.next := report;
				current.lastReport := report;
			всё;

			если Debug то
				ЛогЯдра.пСтроку8("Report added"); ЛогЯдра.пВК_ПС;
			всё;
			(*generate reportItem*)
			нов(newReportItem);
			(*newReportItem.index 			:= reportItemQueue.bitsAlreadyUsed;*)
			newReportItem.reportID		:= report.reportID;
			newReportItem.reportItemSize 	:= report.reportSize;
			newReportItem.reportItemCount	:= report.reportCount;
			newReportItem.values 			:= report.usages;

			если Debug то
				ЛогЯдра.пСтроку8("Add report:");ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8(" index: "); ЛогЯдра.пЦел64(newReportItem.index,0); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8(" reportItemSize: "); ЛогЯдра.пЦел64(newReportItem.reportItemSize,0); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8(" reportItemCount: "); ЛогЯдра.пЦел64(newReportItem.reportItemCount,0); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8(" index: "); ЛогЯдра.пЦел64(newReportItem.index,0); ЛогЯдра.пВК_ПС;

				если (report.usages=НУЛЬ) то
					ЛогЯдра.пСтроку8("UsbHidReport::HIDReport.AddReport: found empty usageTupleList"); ЛогЯдра.пВК_ПС;
				иначе
					ЛогЯдра.пСтроку8("UsbHidReport::HIDReport.AddReport: found usageTupleList"); ЛогЯдра.пВК_ПС;
				всё;
			всё;
			AddReportItem(newReportItem);
			AddReportIterator(report);
		кон AddReport;

		(*add report item to reportItemQueue
		* 	param:	reportItem 	record holding the information how, from and to where to store reports.
		*)
		проц AddReportItem(reportItem: ReportItem);
		перем
			cursor:	ReportItem;
			i: 		цел32;
		нач

			если (reportItemQueue.first=НУЛЬ) то
				reportItemQueue.bitsAlreadyUsed:=0;
				reportItemQueue.first := reportItem;
			иначе
				cursor := reportItemQueue.first;
				нцПока(cursor.next#НУЛЬ) делай
					cursor := cursor.next;
				кц;
				cursor.next := reportItem;
			всё;
			reportItemQueue.bitsAlreadyUsed:= reportItemQueue.bitsAlreadyUsed + (reportItem.reportItemSize*reportItem.reportItemCount);
			если Debug то
				ЛогЯдра.пСтроку8("UsbHidReport:HIDReport.AddReportItem: Successfully added report item. Updated bitsAlreadyUsed: ");
				ЛогЯдра.пЦел64(reportItemQueue.bitsAlreadyUsed,0); ЛогЯдра.пВК_ПС;
				cursor := reportItemQueue.first;
				нцПока (cursor.next#НУЛЬ) делай
					cursor := cursor.next;
				кц;
				если (cursor.values#НУЛЬ) то
					нцДля i:=0 до cursor.reportItemCount-1 делай
						ЛогЯдра.пСтроку8("AddReportItem: cursor.values[i].usageID: "); ЛогЯдра.пЦел64(cursor.values[i].usageID,0); ЛогЯдра.пВК_ПС;
					кц;
				всё;
			всё;
		кон AddReportItem;

		(*add a report to the reportIterator
		*	param: report	report to add
		*)
		проц AddReportIterator(report: HidReport);
		перем cursor:	HidReportIterator;
		нач
			если(reportIterator=НУЛЬ) то
				нов(reportIterator);
				reportIterator.report := report;
			иначе
				cursor := reportIterator;
				нцПока cursor.next#НУЛЬ делай
					cursor := cursor.next;
				кц;
				нов(cursor.next);
				cursor.next.report := report;
			всё;
		кон AddReportIterator;

		(*checks, whether the device sends only parts of reports
		*	return:	TRUE 	if it sends reportIDs
					FALSE 	if it sends always the whole report
		*)
		проц UsesReportIDMechanism*(): булево;
		перем cursor:	HidReportIterator;
		нач
			cursor := reportIterator;
			нцПока(cursor#НУЛЬ) делай
				если(cursor.report.reportID#UndefinedState) то
					возврат истина;
				всё;
				cursor := cursor.next;
			кц;
			возврат ложь;
		кон UsesReportIDMechanism;

		(*return reportItemQueue, for parsing the hid report fast and easily
		* 	return:	reportItemQueue
		*)
		проц GetReportItemQueue*(): ReportItemQueue;
		нач
			возврат reportItemQueue;
		кон GetReportItemQueue;

		(*prints actual reports*)
		проц PrintReportState*;
		нач
			если rootCollection#НУЛЬ то
				PrintReport(rootCollection);
			всё;
		кон PrintReportState;

		(*prints actual reports starting from root collection*)
		проц PrintReport(collection:HidCollection);
		перем
			subCollectionCursor:	HidCollection;
			reportCursor:		HidReport;
			i:					размерМЗ;
			mainState:			мнвоНаБитахМЗ;
		нач
			reportCursor := collection.firstReport;
			нцПока reportCursor#НУЛЬ делай
				ЛогЯдра.пСтроку8("+"); 						ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("    reportID: "); 			ЛогЯдра.пЦел64(reportCursor.reportID,0); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("    usagePage("); 		ЛогЯдра.пЦел64(reportCursor.usagePage,0);
				ЛогЯдра.пСтроку8("): ");						UsbHidUP.PrintUsagePageName(reportCursor.usagePage); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("    reportSize: "); 			ЛогЯдра.пЦел64(reportCursor.reportSize,0); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("    reportCount: ");		ЛогЯдра.пЦел64(reportCursor.reportCount,0); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("    reportType: ");
				если reportCursor.reportType=MainTypeInput то
					ЛогЯдра.пСтроку8("MainTypeInput");
				иначе
					если reportCursor.reportType=MainTypeOutput то
						ЛогЯдра.пСтроку8("MainTypeOutput");
					иначе
						если  reportCursor.reportType=MainTypeFeature то
							ЛогЯдра.пСтроку8("MainTypeFeature");
						иначе
							ЛогЯдра.пСтроку8("MainTypeUndefined");
						всё;
					всё;
				всё;
				ЛогЯдра.пВК_ПС;
				mainState:= НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ,reportCursor.mainState);
				ЛогЯдра.пСтроку8("    mainState: ");			ЛогЯдра.пМнвоНаБитахМЗКакБиты(mainState,0,9); ЛогЯдра.пВК_ПС;

				если (0 в mainState) то ЛогЯдра.пСтроку8("      Constant (no usages!)") 	иначе ЛогЯдра.пСтроку8("      Data")				всё; ЛогЯдра.пВК_ПС;
				если (1 в mainState) то ЛогЯдра.пСтроку8("      Variable")				иначе ЛогЯдра.пСтроку8("      Array")				всё; ЛогЯдра.пВК_ПС;
				если (2 в mainState) то ЛогЯдра.пСтроку8("      Relative")				иначе ЛогЯдра.пСтроку8("      Absolute")			всё; ЛогЯдра.пВК_ПС;
				если (3 в mainState) то ЛогЯдра.пСтроку8("      Wrap") 					иначе ЛогЯдра.пСтроку8("      No Wrap")			всё; ЛогЯдра.пВК_ПС;
				если (4 в mainState) то ЛогЯдра.пСтроку8("      Non Linear") 			иначе ЛогЯдра.пСтроку8("      Linear")			всё; ЛогЯдра.пВК_ПС;
				если (5 в mainState) то ЛогЯдра.пСтроку8("      No Preferred") 			иначе ЛогЯдра.пСтроку8("      Preferred State")	всё; ЛогЯдра.пВК_ПС;
				если (6 в mainState) то ЛогЯдра.пСтроку8("      Null State") 			иначе ЛогЯдра.пСтроку8("      No Null Position")	всё; ЛогЯдра.пВК_ПС;
				если (7 в mainState) то ЛогЯдра.пСтроку8("      Volatile") 				иначе ЛогЯдра.пСтроку8("      Non Volatile") 		всё; ЛогЯдра.пВК_ПС;
				если (8 в mainState) то ЛогЯдра.пСтроку8("      Buffered Bytes") 		иначе ЛогЯдра.пСтроку8("      Bit Field"); 			всё; ЛогЯдра.пВК_ПС;

				ЛогЯдра.пСтроку8("    logicalMinimum: "); 	ЛогЯдра.пЦел64(reportCursor.logicalMinimum,0); 	ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("    logicalMaximum: "); 	ЛогЯдра.пЦел64(reportCursor.logicalMaximum,0); 	ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("    physicalMinimum: "); 	ЛогЯдра.пЦел64(reportCursor.physicalMinimum,0);	ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("    physicalMaximum: "); 	ЛогЯдра.пЦел64(reportCursor.physicalMaximum,0);ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("    unitExponent: "); 		ЛогЯдра.пЦел64(reportCursor.unitExponent,0); 	ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("    unit"); 				ЛогЯдра.пЦел64(reportCursor.unit,0); 				ЛогЯдра.пВК_ПС;

				если(reportCursor.usages#НУЛЬ) то
					нцДля i:=0 до длинаМассива(reportCursor.usages)-1 делай
						если(1 в mainState) то
							ЛогЯдра.пСтроку8("     usageID("); ЛогЯдра.пЦел64(reportCursor.usages[i].usageID,0); ЛогЯдра.пСтроку8("): ");
							UsbHidUP.PrintUsagePage(reportCursor.usagePage, reportCursor.usages[i].usageID); ЛогЯдра.пВК_ПС;
							ЛогЯдра.пСтроку8("     usageValue: "); ЛогЯдра.пЦел64(reportCursor.usages[i].usageValue,50); ЛогЯдра.пВК_ПС;
						иначе
							ЛогЯдра.пСтроку8("     usageID(");
							если(reportCursor.usages[i].usageValue#0) то
								UsbHidUP.PrintUsagePage(reportCursor.usagePage, reportCursor.usages[i].usageValue);
							иначе
								ЛогЯдра.пСтроку8("0");
							всё;
							ЛогЯдра.пСтроку8(") returned"); ЛогЯдра.пВК_ПС;
						всё;
					кц;
				всё;
				ЛогЯдра.пВК_ПС;
				reportCursor := reportCursor.next;
			кц;
			subCollectionCursor := collection.firstCollection;
			нцПока subCollectionCursor#НУЛЬ делай
				PrintReport(subCollectionCursor);
				subCollectionCursor := subCollectionCursor.next;
			кц;
		кон PrintReport;

		проц &Init*;
		нач
			нов(rootCollection);
			current := rootCollection;
			нов(parentList);
			нов(reportItemQueue);
		кон Init;

	кон HidReportManager;

кон UsbHidReport.

System.Free UsbHidReport~

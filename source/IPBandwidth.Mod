модуль IPBandwidth;	(** AUTHOR "negelef"; PURPOSE "Bandwidth measurement on IP networks."; *)

использует Commands, Diagnostics, Kernel, Options, Потоки, DNS, IP, TCP, UDP;

конст DefaultPort = 5772;
конст DefaultDuration = 10;

тип Link = окласс

	перем context: Commands.Context;
	перем diagnostics: Diagnostics.StreamDiagnostics;
	перем hostname: DNS.Name;
	перем port, result: целМЗ;
	перем address: IP.Adr;
	перем buffer: массив 65000 из симв8;

	проц &Initialize (context: Commands.Context);
	нач
		сам.context := context;
		нов (diagnostics, context.error);
	кон Initialize;

	проц Send;
	перем sent: размерМЗ;
	нач
		если ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках (hostname) то
			context.result := Commands.CommandParseError; возврат;
		всё;
		DNS.HostByName (hostname, address, result);
		если result # DNS.Ok то
			diagnostics.Error (hostname, port, "failed to resolve");
			context.result := Commands.CommandError; возврат;
		всё;
		если ~context.arg.ПропустиБелоеПолеИЧитайЦел32 (port, ложь) то
			port := DefaultPort;
		всё;

		Connect;
		если result # Потоки.Успех то
			diagnostics.Error (hostname, port, "failed to connect to server");
			context.result := Commands.CommandError; возврат;
		всё;

		sent := 0;
		context.out.пСтроку8 ("Sending data..."); context.out.пВК_ПС; context.out.ПротолкниБуферВПоток;
		нцПока ReceiveBytes () = 0 делай
			увел (sent, SendBytes ());
			если result # Потоки.Успех то
				Close;
				diagnostics.Error (hostname, port, "failed to send");
				context.result := Commands.CommandError; возврат;
			всё;
		кц;

		Close;
		context.out.пСтроку8 ("Sent "); context.out.пРазмерМЗ (sent); context.out.пСтроку8 (" bytes"); context.out.пВК_ПС; context.out.ПротолкниБуферВПоток;
	кон Send;

	проц Receive;
	перем duration, ticks: целМЗ; received: размерМЗ;
	нач
		hostname := "server";
		если ~context.arg.ПропустиБелоеПолеИЧитайЦел32 (duration, ложь) то
			duration := DefaultDuration;
		всё;
		если ~context.arg.ПропустиБелоеПолеИЧитайЦел32 (port, ложь) то
			port := DefaultPort;
		всё;

		Listen;
		если result # Потоки.Успех то
			diagnostics.Error (hostname, port, "failed to connect to client");
			context.result := Commands.CommandError; возврат;
		всё;

		received := 0; ticks := 0;
		context.out.пСтроку8 ("Receiving data..."); context.out.пВК_ПС; context.out.ПротолкниБуферВПоток;
		нц
			увел (received, ReceiveBytes ());
			если result # Потоки.Успех то
				Close;
				diagnostics.Error (hostname, port, "failed to receive");
				context.result := Commands.CommandError; возврат;
			всё;
			если ticks = 0 то
				если received # 0 то ticks := Kernel.GetTicks () + Kernel.second * duration всё;
			иначе
				если Kernel.GetTicks () - ticks >= 0 то прервиЦикл всё;
			всё;
		кц;

		Close;
		context.out.пСтроку8 ("Received "); context.out.пРазмерМЗ (received); context.out.пСтроку8 (" bytes in ");
		context.out.пЦел64 (duration, 0); context.out.пСтроку8 (" seconds"); context.out.пВК_ПС;
		context.out.пСтроку8 ("Bandwidth: "); context.out.пВещ64_ФФТ (received / (1000000 * duration), 0, 3, 0);
		context.out.пСтроку8 (" MB/s"); context.out.пВК_ПС; context.out.ПротолкниБуферВПоток;
	кон Receive;

	проц Connect;
	нач СТОП (1001);
	кон Connect;

	проц Listen;
	нач СТОП (1001);
	кон Listen;

	проц SendBytes (): размерМЗ;
	нач СТОП (1001);
	кон SendBytes;

	проц ReceiveBytes (): размерМЗ;
	нач СТОП (1001);
	кон ReceiveBytes;

	проц Close;
	нач СТОП (1001);
	кон Close;

кон Link;

тип TCPLink = окласс (Link)

	перем service, connection: TCP.Connection;

	проц {перекрыта}Connect;
	нач
		нов (connection);
		connection.Open (TCP.NilPort, address, port, result);
	кон Connect;

	проц {перекрыта}Listen;
	нач
		нов (service);
		service.Open (port, IP.NilAdr, TCP.NilPort, result);
		если result = TCP.Ok то
			context.out.пСтроку8 ("Waiting for client..."); context.out.пВК_ПС; context.out.ПротолкниБуферВПоток;
			service.Accept (connection, result);
		всё;
	кон Listen;

	проц {перекрыта}SendBytes (): размерМЗ;
	нач
		connection.ЗапишиВПоток (buffer, 0, длинаМассива (buffer), ложь, result);
		возврат длинаМассива (buffer);
	кон SendBytes;

	проц {перекрыта}ReceiveBytes (): размерМЗ;
	перем received: размерМЗ;
	нач
		если connection.Available () # 0 то
			connection.ПрочтиИзПотока (buffer, 0, длинаМассива (buffer), 1, received, result);
			если result = TCP.Ok то возврат received всё;
		всё;
		возврат 0;
	кон ReceiveBytes;

	проц {перекрыта}Close;
	нач
		connection.Закрой;
		если service # НУЛЬ то service.Закрой всё;
	кон Close;

кон TCPLink;

тип UDPLink = окласс (Link)

	перем socket: UDP.Socket;

	проц {перекрыта}Connect;
	нач
		нов (socket, UDP.NilPort, result);
	кон Connect;

	проц {перекрыта}Listen;
	нач
		нов (socket, port, result);
	кон Listen;

	проц {перекрыта}SendBytes (): размерМЗ;
	нач
		socket.Send (address, port, buffer, 0, длинаМассива (buffer), result);
		возврат длинаМассива (buffer);
	кон SendBytes;

	проц {перекрыта}ReceiveBytes (): размерМЗ;
	перем address: IP.Adr; port: целМЗ; received: размерМЗ;
	нач
		socket.Receive (buffer, 0, длинаМассива (buffer), 0, address, port, received, result);
		если result = UDP.Ok то возврат received всё;
		если result = UDP.Timeout то result := UDP.Ok всё;
		возврат 0;
	кон ReceiveBytes;

	проц {перекрыта}Close;
	нач socket.Close;
	кон Close;

кон UDPLink;

(** Synposis: hostname [port] *)
проц SendTCP* (context: Commands.Context);
перем link: TCPLink;
нач
	нов (link, context);
	link.Send;
кон SendTCP;

(** Synposis: [duration] [port] *)
проц ReceiveTCP* (context: Commands.Context);
перем link: TCPLink; client: TCP.Connection;
нач
	нов (link, context);
	link.Receive;
кон ReceiveTCP;

(** Synposis: hostname [port] *)
проц SendUDP* (context: Commands.Context);
перем link: UDPLink;
нач
	нов (link, context);
	link.Send;
кон SendUDP;

(** Synposis: [duration] [port] *)
проц ReceiveUDP* (context: Commands.Context);
перем link: UDPLink;
нач
	нов (link, context);
	link.Receive;
кон ReceiveUDP;

кон IPBandwidth.

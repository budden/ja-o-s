модуль BimboScannerUCS32;

использует
	(* Trace, *) Texts, TextUtilities, Потоки, ЛогЯдра, UCS32, UCS32u, Commands, StringsUCS32;

конст
	(** См. также TFTypeSysUCS32.Трассировать *)
	Трассировать = ложь;

	Eot* = 0H;
	(* ObjectMarker = 020X; *)
	LF* = 0AH; CR* = 0DH; TAB* = 09H; (* ESC* = 1BX; *)
	SingleQuote = 027H;  DoubleQuote* = 022H;
	Ellipsis* = 07FH;   (* used in (Fox) Scanner.GetNumber to return with ".." when reading an interval like 3..5 *)

	(* numtyp values *)
	char* = 1; integer* = 2; longinteger* = 3; real* = 4; longreal* = 5;

	MaxHDig* = 8;	(* maximal hexadecimal longint length *)
	MaxHHDig* = 16;	(* maximal hexadecimal hugeint length *)
	MaxRExp* = 38;	(* maximal real exponent *)
	MaxLExp* = 308;	(* maximal longreal exponent *)

	null* =   0; times* =   1; slash* =   2; div* =   3; mod* =   4; and* =   5;
	plus* =   6; minus* =   7; or* =   8; eql* =   9; neq* =  10; lss* =  11;
	leq* =  12; gtr* =  13; geq* =  14; in* =  15; is* =  16; arrow* =  17;
	period* =  18; comma* =  19; colon* =  20; upto* =  21; rparen* =  22;
	rbrak* =  23; rbrace* =  24; of* =  25; then* =  26; do* =  27; to* =  28;
	by* =  29; lparen* =  30; lbrak* =  31; lbrace* =  32; not* =  33;
	becomes* =  34; number* =  35; nil* =  36; true* =  37; false* =  38;
	string* =  39; ident* =  40; semicolon* =  41; bar* =  42; end* =  43;
	else* =  44; elsif* =  45; until* =  46; if* =  47; case* =  48; while* =  49;
	repeat* =  50; for* =  51; loop* =  52; with* =  53; exit* =  54;
	passivate* =  55 (* AWAIT *); return* =  56; refines* =  57; implements* =  58;
	array* =  59; definition* =  60; object* =  61; record* =  62; pointer* =  63;
	begin* =  64; (* code* =  65; *) const* =  66; type* =  67; var* =  68;
	procedure* =  69; import* =  70; module* =  71; eof* =  72;
	comment* = 73; newLine* = 74; question* = 75; finally* = 76;

	(* From FoxScanner.Mod *)
	Operator* = 83; Extern*= 97; Enum* = 101; Escape* = 117; Ignore* = 157 (* originally 57 *);
	
	(* Novye *)
	address* = 202;
	size* = 203;
	Backslash* = 204;
	AsmBlock* = 205;
	SharpIf* = 206; SharpElse* = 207; SharpElsif* = 208; SharpEnd* = 209; 
	Size2* = 210;
	Address2* = 211;
	
	(* При обработке в парсере придётся сделать что-то более сложное, чем в компиляторе, и 
		обрабатывать отдельно возможные места, где может встретиться макровызов. Имеем в виду, что
		в компиляторе за макровызовом следует десигнатор, но понимаемый не в контексте (см. LisParser.Parser.РазбериМакроВызов *)
	СимволМакроВызова* = 215;
	
	
	(* цит_ и цитˉ просто пропускаем, в расчёте на то, что имена внутри цитаты или часть
		имён окажутся понятны в контексте *)
	МакросЧтенияЦитˉ* = 220;
	ЗакрывающееИмяКнцˉ* = 225;
	
	
	CmpEqual = UCS32.CmpEqual;
	
	YazykNeUspelUznatq = 0;
	YazykOberon = 1;
	YazykYa2 = 2;

перем
	reservedChar-: массив UCS32.DiapazonJunikodaSKirillicejj из булево;
	
	
тип
	Scanner* = окласс
		перем
			buffer: UCS32.PStringJQ;
			pos-: цел32;	(*pos in buffer, in chars*)
			ch-: UCS32.CharJQ;	(**look-ahead *)
			str-: массив 1024 из UCS32.CharJQ; (* FIXME ПРАВЬМЯ заменить на указатель на куче *)
			sym- : размерМЗ;
			numtyp-: цел16; (* 1 = char, 2 = integer, 3 = real, 4 = longreal *)
			intval-: цел32;	(* integer value or string length *)
			longintval-: цел64;
			realval-: вещ32;
			lrlval-: вещ64;
			numStartPos, numEndPos: цел32;
			lastpos-, curpos-, errpos-: цел32;	 (*pos in text, in chars*)
			tekStroka-: цел32; (* tek. stroka v tekste *)
			isNummer: булево;
			commentStr- : StringsUCS32.UCS32StringJQMaker;
			stringWriter- : Потоки.Писарь;
			stringMaker- : StringsUCS32.UCS32StringJQMaker;
			cw : Потоки.Писарь;
			yazyk- : бцел8; (* avtoopredeljonnyjj jazyk - YazykNeUspelUznatq i t.p. *)

		проц &Init;
		нач
			нов(commentStr, 1024); (* tak malo??? *)
			cw := commentStr.GetWriter();
			нов(stringMaker, 1024);
			stringWriter := stringMaker.GetWriter();
			tekStroka := 1;
		кон Init;

		проц err(n: цел16);
		нач
			если Трассировать то
				ЛогЯдра.пСтроку8("BimboScanner error, code = "); ЛогЯдра.пЦел64(n, 0); 
				ЛогЯдра.пСтроку8(", curpos ="); ЛогЯдра.пЦел64(curpos, 0); 
				ЛогЯдра.пВК_ПС всё кон err;

		проц NextChar*;
		нач
			если pos < длинаМассива(buffer) то
				ch := buffer[pos]; увел(pos)
			иначе
				ЛогЯдра.пСтроку8("NextChar: ik!"); ЛогЯдра.пВК_ПС;
				ch := UCS32.IntToCharJQ(Eot)
			всё;
			увел(curpos); (* curpos := pos; *)
			если кодСимв32(ch) = LF то увел(tekStroka) всё;
		кон NextChar;

		
		(* GetAsmBlock съедает END, поэтому мы делаем костыль в парсере,
		 к-рый возвращает END после ассемблерного блока *)
		проц GetAsmBlock;
		перем chisloBukvVEnd : цел16;
		нач
			chisloBukvVEnd := 0;
			нц
				если кодСимв32(ch) = кодСимв8('#') то
					chisloBukvVEnd := -1
				аесли (chisloBukvVEnd = 0) и (кодСимв32(ch) = кодСимв8('E')) то
					chisloBukvVEnd := 1
				аесли (chisloBukvVEnd = 1) и (кодСимв32(ch) = кодСимв8('N')) то
					chisloBukvVEnd := 2
				аесли (chisloBukvVEnd = 2) и (кодСимв32(ch) = кодСимв8('D')) то
					(* Здесь не вставляем NextChar. Следующий идетификатор будет просто 
					'D' и мы добавляем специальный костыль, чтобы 'D' после AsmBlock
					читалось как END *)					
					возврат 
				аесли (chisloBukvVEnd = 0) и (ch = UCS32u.StrochnajaK) то
					chisloBukvVEnd := 1
				аесли (chisloBukvVEnd = 1) и (ch = UCS32u.StrochnajaO) то
					chisloBukvVEnd := 2
				аесли (chisloBukvVEnd = 2) и (ch = UCS32u.StrochnajaN) то
					возврат
				иначе
					chisloBukvVEnd := 0 всё;
				если кодСимв32(ch) = Eot то
					err(115(*konec fajjla vnutri assemblernojj vstavki*));
					возврат 
				аесли кодСимв32(ch) = кодСимв8(';') то
					(* KernelLog.String('GetAsmBlock: Kommentarijj'); KernelLog.Ln; *)
					нц (*kommentarijj*)
						NextChar;
						если (кодСимв32(ch) = CR) или (кодСимв32(ch) = LF) то
							chisloBukvVEnd := 0; 
							прервиЦикл всё кц всё;
				NextChar кц кон GetAsmBlock;


		(** Iz FoxScanner.???.GetString() 
			get a string starting at current position
			string = {'"' {Character} '"'} | {"'" {Character} "'"}.
		**)
		(* multiline indicates that a string may occupy more than one lines, either concatenated or via multi-strings " "  " "
		
		Так получилось, что строка не может идти в конце текста! Для модуля это опять же нормально (как и в случае с идентификатором).
		*)
		проц GetAString(multiLine, multiString, useControl: булево);
		перем och: UCS32.CharJQ; error: булево; (* done: BOOLEAN; *)
			eotDetector : размерМЗ; vzjalBukvuVzajjmy : булево;

			проц Append(ch1 :UCS32.CharJQ);
			нач
				если ch1.UCS32CharCode = 0 то
					err(78(*"Unexpected end of text in string"*)); error := истина
				иначе
					stringWriter.пСимв32_вЮ8(ch1)
			всё ;
			кон Append;

		нач
			stringMaker.Clear;
			och := ch; error := ложь; vzjalBukvuVzajjmy := ложь;
			нцДо (* cikl po strokam v multistroke stilja C "s1" "s2" "s3" *)
				нц (* cikl sobstvenno po stroke, napr, "abc" ili \"""\ *)
					если error то прервиЦикл всё;
					если vzjalBukvuVzajjmy то
						vzjalBukvuVzajjmy := ложь
					иначе
						NextChar всё;
					если (кодСимв32(ch) = Eot) то прервиЦикл
					аесли (UCS32.SravniRunyJQ(ch, och) = CmpEqual) то
						NextChar;
						если ~useControl то прервиЦикл
						аесли кодСимв32(ch) = кодСимв8('\') то NextChar; прервиЦикл 
						иначе 
							Append(och); Append(ch);
							vzjalBukvuVzajjmy := истина; всё
					аесли useControl и (кодСимв32(ch) = кодСимв8('\')) то
						NextChar;
						если (кодСимв32(ch) = кодСимв8('\')) или (UCS32.SravniRunyJQ(ch, och) = CmpEqual) то
							Append(ch)
						аесли кодСимв32(ch) = кодСимв8('n') то
							Append(UCS32.IntToCharJQ(CR)); Append(UCS32.IntToCharJQ(LF)); (* FIXME ПРАВЬМЯ УБРАТЬ CR *)
						аесли кодСимв32(ch) = кодСимв8('t') то
							Append(UCS32.IntToCharJQ(TAB))
						иначе
							err(35(*"Unknown control sequence"*));
							error := истина всё
					иначе
						если ~multiLine и (кодСимв32(ch) < кодСимв8(" ")) то err(47(* "Nevernyjj znachok v strokovom literale"*) ); прервиЦикл всё;
						Append(ch) всё 
				кц; (* Cikl po bukvam stroki *)
				если кодСимв32(ch) = Eot то
					err(36(*"Unexpected end of text in string"*));
					error := истина;
				иначе
					если multiString то 
						SkipBlanks(eotDetector, истина);
						если eotDetector = eof то
							err(37(*"Unexpected end of text in string"*));
							возврат
						всё;
						если useControl и (кодСимв32(ch) = кодСимв8('\')) то
							NextChar всё всё всё
			кцПри ~multiString или (UCS32.SravniRunyJQ(ch, och) # CmpEqual);
			stringWriter.пСимв32_вЮ8(UCS32.IntToCharJQ(0));
			stringWriter.ПротолкниБуферВПоток;
			UCS32.COPYJQvJQДоУпора(stringMaker.GetString()^,str);  (* нам тут нет нужды обязательно всё забрать *)
			(* Ehtot kusok - iz starogo BimboScanner'a *)
			(* KernelLog.String("GetAStr h.c. "); KernelLog.String(str); *)
			если UCS32.LengthStringJQ(str) = 1 то
				sym := number
			иначе
				sym := string всё кон GetAString;

		проц Identifier(перем sym1: размерМЗ);
			перем i: цел32;
		нач i := 0;
			нцДо
				str[i] := ch; увел(i); NextChar
			кцПри (кодСимв32(ch) = 0) или (кодСимв32(ch) < длинаМассива(reservedChar)) и reservedChar[кодСимв32(ch)] или (i = длинаМассива(str));
			(* Странная логика - текст не может заканчиваться идентификатором. Хотя для правильного кода модуля на AO это выполняется *)
			если (кодСимв32(ch) = 0) или (i = длинаМассива(str)) то err(240); умень(i) всё ;
			str[i] := UCS32.IntToCharJQ(0); sym1 := ident;
			(* temporary code! delete when moving to ANY and adapt PCT *)
			если UCS32.SravniStringJQiUTF8ArrayOfChar(str,"ANY") = CmpEqual то UCS32.COPYU8vJQ("PTR", str) всё;
		кон Identifier;

		проц Number;
		перем i, m, n, d, e: цел16; dig: массив 24 из UCS32.CharJQ; f: вещ64; expCh: UCS32.CharJQ; neg, long: булево;
		detect0x : цел16; 

			проц Ten(e1: цел16): вещ64;
				перем x, p: вещ64;
			нач x := 1; p := 10;
				нцПока e1 > 0 делай
					если нечётноеЛи¿(e1) то x := x*p всё;
					e1 := e1 DIV 2;
					если e1 > 0 то p := p*p всё (* prevent overflow *)
				кц;
				возврат x
			кон Ten;

			проц Ord(ch1: UCS32.CharJQ; hex: булево): цел16;
			нач 
				если ch1.UCS32CharCode <= кодСимв8("9") то возврат цел16(ch1.UCS32CharCode - кодСимв8("0"))
				аесли hex то возврат цел16(ch1.UCS32CharCode - кодСимв8("A") + 10)
				иначе err(2); возврат 0
				всё
			кон Ord;

		нач (* ("0" <= ch) & (ch <= "9") *)
			i := 0; m := 0; n := 0; d := 0; long := ложь;
			нц (* read mantissa *)
				если (i = 1) и (кодСимв8("x") = кодСимв32(ch)) и (detect0x = 1) то detect0x := 2; NextChar всё;
				если (кодСимв8("0") <= кодСимв32(ch)) и (кодСимв32(ch) <= кодСимв8("9")) 
						или (d = 0) 
							и ((кодСимв8("A") <= кодСимв32(ch)) и (кодСимв32(ch) <= кодСимв8("F")) или (кодСимв8("a") <= кодСимв32(ch)) и (кодСимв32(ch) <= кодСимв8("f"))) то
					если (i = 0) и (кодСимв8("0") = кодСимв32(ch)) то detect0x := 1 всё;
					если (m > 0) или (кодСимв32(ch) # кодСимв8("0")) то (* ignore leading zeros *)
						если n < длинаМассива(dig) то dig[n] := UCS32.UCS32UP(ch); увел(n) всё;
						увел(m)
					всё;
					NextChar; увел(i)
				аесли кодСимв32(ch) = кодСимв8(".") то NextChar;
					если кодСимв32(ch) = кодСимв8(".") то (* ellipsis *) ch := UCS32.IntToCharJQ(07FH); прервиЦикл
					аесли d = 0 то (* i > 0 *) d := i
					иначе err(2)
					всё
				иначе прервиЦикл
				всё
			кц; (* 0 <= n <= m <= i, 0 <= d <= i *)
			если d = 0 то (* integer *)
				если n = m то intval := 0; i := 0;
(* > bootstrap 1 *)
					longintval := 0;
(* < bootstrap 1 *)
					если кодСимв32(ch) = кодСимв8("X") то (* character *) NextChar; numtyp := char;
					(*	IF PCM.LocalUnicodeSupport & (n <= 8) THEN
							IF (n = 8) & (dig[0] > "7") THEN (* prevent overflow *) intval := -1 END;
							WHILE i < n DO intval := intval*10H + Ord(dig[i], TRUE); INC(i) END
						ELSIF ~PCM.LocalUnicodeSupport & (n <= 2) THEN
							WHILE i < n DO intval := intval*10H + Ord(dig[i], TRUE); INC(i) END
						ELSE err(203)
						END *)
					аесли (кодСимв32(ch) = кодСимв8("H")) или (detect0x = 2) то (* hexadecimal *) 
						если (кодСимв32(ch) = кодСимв8("H")) то NextChar всё;
						если n <= MaxHDig то
							numtyp := integer;
							если (n = MaxHDig) и (dig[0].UCS32CharCode > кодСимв8("7")) то (* prevent overflow *) intval := -1 всё;
							нцПока i < n делай intval := intval*10H + Ord(dig[i], истина); увел(i) кц
(* > bootstrap 1 *)
						аесли n <= MaxHHDig то
							numtyp := longinteger;
							если (n = MaxHHDig) и (dig[0].UCS32CharCode > кодСимв8("7")) то (* prevent overflow *) longintval := -1 всё;
							нцПока i < n делай longintval := Ord(dig[i], истина) + longintval*10H; увел(i) кц
(* < bootstrap 1 *)
						иначе err(2030)
						всё
					иначе (* decimal *) numtyp := integer;
						нцПока i < n делай d := Ord(dig[i], ложь); увел(i);
							если intval <= (матМаксимум(цел32) - d) DIV 10 то intval := intval*10 + d
(* > bootstrap 2
							ELSE err(203)
< bootstrap 2 *)
(* > bootstrap 1 *)
							иначе long := истина
(* < bootstrap 1 *)
							всё
						кц;
(* > bootstrap 1 *)
						если long то
							numtyp := longinteger; longintval := устарПреобразуйКБолееШирокомуЦел(intval)*10+d;
							нцПока i < n делай d := Ord(dig[i], ложь); увел(i);
								если longintval*10+d >= 0 то longintval := longintval*10 + d
								иначе err(2031)
								всё
							кц
						всё
(* < bootstrap 1 *)
					всё
				иначе err(2032)
				всё
			иначе (* fraction *)
				f := 0; e := 0; expCh := UCS32.IntToCharJQ(кодСимв8("E"));
				нцПока n > 0 делай (* 0 <= f < 1 *) умень(n); f := (Ord(dig[n], ложь) + f)/10 кц;
				если (кодСимв32(ch) = кодСимв8("E")) или (кодСимв32(ch) = кодСимв8("D")) то expCh := ch; NextChar; neg := ложь;
					если кодСимв32(ch) = кодСимв8("-") то neg := истина; NextChar
					аесли кодСимв32(ch) = кодСимв8("+") то NextChar
					всё;
					если (кодСимв8("0") <= кодСимв32(ch)) и (кодСимв32(ch) <= кодСимв8("9")) то
						нцДо n := Ord(ch, ложь); NextChar;
							если e <= (матМаксимум(цел16) - n) DIV 10 то e := e*10 + n
							иначе err(2033)
							всё
						кцПри (кодСимв32(ch) < кодСимв8("0")) или (кодСимв8("9") < кодСимв32(ch));
						если neg то e := -e всё
					иначе err(2)
					всё
				всё;
				умень(e, i-d-m); (* decimal point shift *)
				если expCh.UCS32CharCode = кодСимв8("E") то numtyp := real;
					если (1-MaxRExp < e) и (e <= MaxRExp) то
						если e < 0 то realval := устарПреобразуйКБолееУзкомуЦел(f / Ten(-e))
						иначе realval := устарПреобразуйКБолееУзкомуЦел(f * Ten(e))
						всё
					иначе err(2034)
					всё
				иначе numtyp := longreal;
					если (1-MaxLExp < e) и (e <= MaxLExp) то
						если e < 0 то lrlval := f / Ten(-e)
						иначе lrlval := f * Ten(e)
						всё
					иначе err(2035)
					всё
				всё
			всё
		кон Number;

		проц SkipBlanks(перем s : размерМЗ; alsoSkipLF : булево);
		нач
			нцПока (кодСимв32(ch) <= кодСимв8(" ")) 
				и (alsoSkipLF или (кодСимв32(ch) # LF)) 
				(* & (ch # ESC) *) делай 
				если кодСимв32(ch) = Eot то
					s := eof; возврат
				иначе NextChar
				всё кц кон SkipBlanks;

		проц GetNumAsString*(перем val: UCS32.StringJQ);
		перем i, l: размерМЗ;
		нач
			(*Strings.Copy(buffer^, numStartPos, numEndPos-numStartPos, val);*)
			если isNummer то
				i := 0; l := UCS32.LengthStringJQ(val)-1;
				нцПока (i < numEndPos-numStartPos) и (i < l) делай
					val[i] := buffer[numStartPos + i];
					увел(i);
				кц;
			всё;
			val[i] := UCS32.IntToCharJQ(0)
		кон GetNumAsString;

		проц Get*(перем s: размерМЗ);
			перем bylAsmBlock : булево;

			проц Comment;	(* do not read after end of file *)
			нач NextChar; cw.пЦел32_вЮ8(кодСимв32(ch));
				нц
					нц
						нцПока кодСимв32(ch) = кодСимв8("(") делай NextChar; cw.пЦел32_вЮ8(кодСимв32(ch));
							если кодСимв32(ch) = кодСимв8("*") то Comment всё
						кц;
						если кодСимв32(ch) = кодСимв8("*") то NextChar; cw.пЦел32_вЮ8(кодСимв32(ch)); прервиЦикл всё ;
						если кодСимв32(ch) = Eot то прервиЦикл всё ;
						NextChar; cw.пЦел32_вЮ8(кодСимв32(ch));
					кц ;
					если кодСимв32(ch) = кодСимв8(")") то NextChar; cw.пЦел32_вЮ8(кодСимв32(ch)); прервиЦикл всё ;
					если кодСимв32(ch) = Eot то err(5); прервиЦикл всё
				кц;
			кон Comment;
			
		проц ПрочтиЛексемуНезависимоОтЯзыкаКлючСлов();
		нач
			просей кодСимв32(ch) из
				| кодСимв8("\"): NextChar;
					если кодСимв32(ch) = DoubleQuote то 
						GetAString(истина, истина, истина);
						s := sym; (* При ошибке это будет криво, но до этого было ещё кривее - строки просто игнорировались! *)
					иначе 
						s := Backslash всё
				| LF: s := newLine; NextChar
				| DoubleQuote : GetAString(истина, истина, ложь); s := sym; (* При ошибке это будет криво, но до этого было ещё кривее - строи просто игнорировались! *)
				| SingleQuote : GetAString(ложь, ложь, ложь); s := sym; (* При ошибке это будет криво, но до этого было ещё кривее - строки просто игнорировались! *)
				| кодСимв8("#")  : s := neq; NextChar
				| кодСимв8("(")  : NextChar;
						 если кодСимв32(ch) = кодСимв8("*") то commentStr.Clear; Comment; cw.ПротолкниБуферВПоток; commentStr.Shorten(2); s := comment;		(*allow recursion without reentrancy*)
						 иначе s := lparen
						 всё
				| кодСимв8(")")  : s := rparen; NextChar
				| кодСимв8("*")  : s:=times; NextChar
				| кодСимв8("+")  : s :=  plus; NextChar
				| кодСимв8(",")  : s := comma; NextChar
				| кодСимв8("-")  : s :=  minus; NextChar
				| кодСимв8(".")  : NextChar;
								 если кодСимв32(ch) = кодСимв8(".") то NextChar; s := upto иначе s := period всё
				| кодСимв8("/")  : s :=  slash; NextChar
				| кодСимв8("0")..кодСимв8("9"): isNummer := истина; numStartPos := pos-1;
					(*	WHILE (ch >="0") & (ch <= "9") OR (ch >= "A") & (ch <="F") OR (ch="H") OR (ch="X") OR (ch=".") DO NextChar END; *)
					Number;
					numEndPos := pos-1; s := number
				| кодСимв8(":")  : NextChar;
								 если кодСимв32(ch) = кодСимв8("=") то NextChar; s := becomes иначе s := colon всё
				| кодСимв8(";")  : s := semicolon; NextChar
				| кодСимв8("<")  : NextChar;
								 если кодСимв32(ch) = кодСимв8("=") то NextChar; s := leq; иначе s := lss; всё
				| кодСимв8("=")  : s :=  eql; NextChar
				| кодСимв8(">")  : NextChar;
								 если кодСимв32(ch) = кодСимв8("=") то NextChar; s := geq; иначе s := gtr; всё
				| кодСимв8("[")  : s := lbrak; NextChar
				| кодСимв8("]")  : s := rbrak; NextChar
				| кодСимв8("^")  : s := arrow; NextChar
				| кодСимв8("{")  : s := lbrace; NextChar
				| кодСимв8("|")  : s := bar; NextChar
				| кодСимв8("}")  : s := rbrace; NextChar
				| кодСимв8("~")  : s := not; NextChar
				| кодСимв8("?")  : s := question; NextChar
				| 07FH  : s := upto; NextChar
				иначе
					если кодСимв32(ch) > матМаксимум(цел32) то
						ЛогЯдра.пСтроку8("Плохая буква в тексте: "); ЛогЯдра.пЦел64(кодСимв32(ch),0); ЛогЯдра.пВК_ПС; всё;
					если TextUtilities.IsAlphaNum(ch) то
						Identifier(s); (* s := null; NextChar; *)
					аесли ch = ЛитСимв32("§") то
						s := СимволМакроВызова; NextChar
					аесли UCS32.ЗнакПрепинанияДляИдентификатораЛи(ch) то
						Identifier(s);
					иначе (* Вообще-то, это запрещённая буква, но сейчас не лучшее время для такого реакторинга ПРАВЬМЯ FIXME *)
						Identifier(s) всё всё	кон ПрочтиЛексемуНезависимоОтЯзыкаКлючСлов;

			
		проц ПрочтиЛексемуАнглКлючСлова();
		нач
			просей кодСимв32(ch) из
				| кодСимв8("A"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str,"ADDRESS") = CmpEqual то s := address
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "ARRAY") = CmpEqual то s := array
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "AWAIT") = CmpEqual то s := passivate
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "ADDRESSOF2") = CmpEqual то s := Address2 
							всё
				| кодСимв8("B"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str, "BEGIN") = CmpEqual то s := begin
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "BY") = CmpEqual то s := by
							всё
				| кодСимв8("C"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str, "CONST") = CmpEqual то s := const
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "CASE") = CmpEqual то s := case
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "CODE") = CmpEqual то 
								s := AsmBlock;
								GetAsmBlock();
							всё
				| кодСимв8("D"): (* Исключение! Не вызываем Identifier сразу! *)
							bylAsmBlock := (s = AsmBlock); 
							Identifier(s);
							если (UCS32.SravniStringJQiUTF8ArrayOfChar(str, "D") = CmpEqual) и bylAsmBlock то 
								(* sm. primechanie v GetAsmBlock() *)
								s := end
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "DO") = CmpEqual то s := do
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "DIV") = CmpEqual то s := div
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "DEFINITION") = CmpEqual то s := definition
							всё
				| кодСимв8("E"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str, "END") = CmpEqual то s := end
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "ENUM") = CmpEqual то s := Enum
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "ELSE") = CmpEqual то s := else
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "ELSIF") = CmpEqual то s := elsif
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "EXIT") = CmpEqual то s := exit
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, 'EXTERN') = CmpEqual то s := Extern
							всё
				| кодСимв8("F"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str, "FALSE") = CmpEqual то s := false
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "FOR") = CmpEqual то s := for
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "FINALLY") = CmpEqual то s := finally
							всё
				| кодСимв8("I"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str, "IF") = CmpEqual то s := if
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "IN") = CmpEqual то s := in
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "IS") = CmpEqual то s := is
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "IMPORT") = CmpEqual то s := import
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "IMPLEMENTS") = CmpEqual то s := implements
							всё
				| кодСимв8("L"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str, "LOOP") = CmpEqual то s := loop всё
				| кодСимв8("M"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str, "MOD") = CmpEqual то s := mod
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "MODULE") = CmpEqual то 
								s := module
							всё
				| кодСимв8("N"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str, "NIL") = CmpEqual то s := nil всё
				| кодСимв8("O"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str, "OR") = CmpEqual то s := or
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "OF") = CmpEqual то s := of
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "OBJECT") = CmpEqual то s := object
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "OPERATOR") = CmpEqual то s := Operator
							всё
				| кодСимв8("P"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str, "PROCEDURE") = CmpEqual то s := procedure
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "POINTER") = CmpEqual то s := pointer
							всё
				| кодСимв8("R"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str, "RECORD") = CmpEqual то s := record
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "REPEAT") = CmpEqual то s := repeat
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "RETURN") = CmpEqual то s := return
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "REFINES") = CmpEqual то s := refines
							всё
				| кодСимв8("S"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str, "SIZE") = CmpEqual то s := size
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "SIZEOF2") = CmpEqual то s := Size2 всё
				| кодСимв8("T"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str, "THEN") = CmpEqual то s := then
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "TRUE") = CmpEqual то s := true
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "TO") = CmpEqual то s := to
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "TYPE") = CmpEqual то s := type
							всё
				| кодСимв8("U"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str, "UNTIL") = CmpEqual то s := until всё
				| кодСимв8("V"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str, "VAR") = CmpEqual то s := var всё
				| кодСимв8("W"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str, "WHILE") = CmpEqual то s := while
							аесли UCS32.SravniStringJQiUTF8ArrayOfChar(str, "WITH") = CmpEqual то s := with
							всё
				| кодСимв8("&")  : s :=  and; NextChar
			иначе ПрочтиЛексемуНезависимоОтЯзыкаКлючСлов() всё	кон ПрочтиЛексемуАнглКлючСлова;
		
		
		проц strЭто(конст что: массив из симв32) : булево;
		нач возврат str = что кон strЭто;
		
		проц ПрочтиЛексемуРусКлючСлова();
			нач
			просей кодСимв32(ch) из
				| кодСимв32(UCS32u.PropisnajaN): Identifier(s);
					если strЭто(Лит32("НУЛЬ")) то s := nil всё
				| кодСимв32(UCS32u.StrochnajaA): Identifier(s);
					если strЭто(Лит32("аесли")) то s := elsif 
					аесли strЭто(Лит32("адресВПамяти")) то s := address
					всё
				| кодСимв32(UCS32u.StrochnajaV): Identifier(s);
					если strЭто(Лит32("в")) то s := in 
					аесли strЭто(Лит32("возврат")) то s := return
					аесли strЭто(Лит32("воплощает")) то s := implements 
					аесли strЭто(Лит32("всё")) то s := end
					аесли strЭто(Лит32("выходя")) то s := finally 
					всё
				| кодСимв32(UCS32u.StrochnajaD): Identifier(s);
					если strЭто(Лит32("до")) то s := to 
					аесли strЭто(Лит32("делай")) то s := do
					аесли strЭто(Лит32("дождись")) то s := passivate 
					всё
				| кодСимв32(UCS32u.StrochnajaE): Identifier(s);
					если strЭто(Лит32("если")) то s := if всё
				| кодСимв32(UCS32u.StrochnajaZ): Identifier(s);
					если strЭто(Лит32("запись")) то s := record всё
				| кодСимв32(UCS32u.StrochnajaI): Identifier(s);
					если strЭто(Лит32("и")) то s := and
					аесли strЭто(Лит32("из")) то s := of 
					аесли strЭто(Лит32("или")) то s := or 
					аесли strЭто(Лит32("использует")) то s := import 
					аесли strЭто(Лит32("истина")) то s := true 
					аесли strЭто(Лит32("иначе")) то s := else всё
				| кодСимв32(UCS32u.StrochnajaK): Identifier(s); 
					если strЭто(Лит32("кон")) то s := end
					аесли strЭто(Лит32("кнцˉ")) то s := ЗакрывающееИмяКнцˉ;
					аесли strЭто(Лит32("кц")) то s := end
					аесли strЭто(Лит32("кцПри")) то s := until;  
					аесли strЭто(Лит32("конст")) то s := const всё 
				| кодСимв32(UCS32u.StrochnajaL): Identifier(s);
					если strЭто(Лит32("ложь")) то s := false всё
				| кодСимв32(UCS32u.StrochnajaM): Identifier(s);
					если strЭто(Лит32("машКод")) то s := AsmBlock; GetAsmBlock();
					аесли strЭто(Лит32("массив")) то s := array
					аесли strЭто(Лит32("модуль")) то s := module всё 
				| кодСимв32(UCS32u.StrochnajaN): (* см. примечание около "D" *)
					bylAsmBlock := (s = AsmBlock); 
					Identifier(s);
					если (UCS32.SravniStringJQiUTF8ArrayOfChar(str, "н") = CmpEqual) и bylAsmBlock то 
								(* sm. primechanie v GetAsmBlock() *)
								s := end
					аесли strЭто(Лит32("нач")) то s := begin 
					аесли strЭто(Лит32("на")) то s := to 
					аесли strЭто(Лит32("неважно")) то s := Ignore
					аесли strЭто(Лит32("нц")) то s := loop 
					аесли strЭто(Лит32("нцДля")) то s := for 
					аесли strЭто(Лит32("нцДо")) то s := repeat 
					аесли strЭто(Лит32("нцПока")) то s := while всё
				| кодСимв32(UCS32u.StrochnajaO): Identifier(s);
					если strЭто(Лит32("окласс")) то s := object 
					аесли strЭто(Лит32("операция")) то s := Operator  
					аесли strЭто(Лит32("определение")) то s := definition 
					аесли strЭто(Лит32("отКомпоновщика")) то s := Extern 
					аесли strЭто(Лит32("остОтДеленияНа")) то s := mod 
					аесли strЭто(Лит32("операцияАдресОт")) то s := Address2
					всё 
				| кодСимв32(UCS32u.StrochnajaP): Identifier(s);
					если strЭто(Лит32("перем")) то s := var 
					аесли strЭто(Лит32("перечисление")) то s := Enum
					аесли strЭто(Лит32("повторяй")) то s := do
					аесли strЭто(Лит32("прервиЦикл")) то s := exit 
					аесли strЭто(Лит32("просей")) то s := case 
					аесли strЭто(Лит32("просейТип")) то s := with
					аесли strЭто(Лит32("проц")) то s := procedure 
					всё
				| кодСимв32(UCS32u.StrochnajaR): Identifier(s);
					если strЭто(Лит32("разностьАдресов")) то s := is 
					аесли strЭто(Лит32("размерОт")) то s := Size2 всё
				| кодСимв32(UCS32u.StrochnajaS): Identifier(s);
					если strЭто(Лит32("суть")) то s := is всё
				| кодСимв32(UCS32u.StrochnajaT): Identifier(s);
					если strЭто(Лит32("тип")) то s := type 
					аесли strЭто(Лит32("то")) то s := then всё
				| кодСимв32(UCS32u.StrochnajaU): Identifier(s);
					если strЭто(Лит32("укль")) то s := pointer всё
				| кодСимв32(ЛитСимв32("ц")): Identifier(s);
					если strЭто(Лит32("цитˉ")) то s := МакросЧтенияЦитˉ всё
				| кодСимв32(UCS32u.StrochnajaSH): Identifier(s);
					если strЭто(Лит32("шаг")) то s := by всё 

				(* | UCS32u.StrochnajaJA: Identifier(s);
					IF strЭто(Лит32("ящик") THEN s := record END *)

				| кодСимв8("D"): (* Ne vyzyvaem Identifier pryamo seychas! *)
							bylAsmBlock := (s = AsmBlock); 
							Identifier(s);
							если strЭто(Лит32("D")) и bylAsmBlock то 
								(* sm. primechanie v GetAsmBlock() *)
								s := end
							аесли strЭто(Лит32("DIV")) то s := div
							всё
				| кодСимв8("M"): Identifier(s);
							если strЭто(Лит32("MOD")) то s := mod
							всё
				| кодСимв8("S"): Identifier(s);
							если UCS32.SravniStringJQiUTF8ArrayOfChar(str, "SIZE") = CmpEqual то s := size всё
				| кодСимв8("&"): s :=  and; NextChar
							
				(* | UCS32u.KodParagraf:
							s := end; NextChar; *)
			иначе ПрочтиЛексемуНезависимоОтЯзыкаКлючСлов() всё	кон ПрочтиЛексемуРусКлючСлова;
		
		проц ПрочтиЛексемуЯзыкКлючСловНеУспелУзнать();
		нач 
			просей кодСимв32(ch) из
				| кодСимв8("M"): Identifier(s);
							если strЭто(Лит32("MODULE")) то 
								yazyk := YazykOberon; s := module
							всё 
				| кодСимв32(UCS32u.StrochnajaM): Identifier(s);
							если strЭто(Лит32("модуль")) то 
								yazyk := YazykYa2; s := module;
							всё
				иначе ПрочтиЛексемуНезависимоОтЯзыкаКлючСлов() всё	кон ПрочтиЛексемуЯзыкКлючСловНеУспелУзнать;

		нач
			нцДо
				(**ignore control characters*)
				
				SkipBlanks(s, ложь);
				если s = eof то возврат всё;
				lastpos := curpos - 1;
				errpos := curpos - 1;
				isNummer := ложь;
				просей yazyk из
					| YazykOberon : ПрочтиЛексемуАнглКлючСлова();
					| YazykYa2 : ПрочтиЛексемуРусКлючСлова();
					| YazykNeUspelUznatq : ПрочтиЛексемуЯзыкКлючСловНеУспелУзнать();
					иначе СТОП(3055) всё
			кцПри s >= 0;
			(* KernelLog.String("BimboScanner.Get: "); KernelLog.Int(s,0); KernelLog.String(", pos="); 
			KernelLog.Int(pos,0); KernelLog.Ln; *)
		кон Get;

		проц Next*;
		нач
			Get(sym);
			если Трассировать то
				ЛогЯдра.пСтроку8("Bimbo: ");
				ЛогЯдра.пЦел64(sym,0);
				ЛогЯдра.пСтроку8(" @ ");
				ЛогЯдра.пЦел64(pos,0);
				ЛогЯдра.пВК_ПС всё;			
			 кон Next;


	кон Scanner;

проц InitWithText*(t: Texts.Text; pos: цел32): Scanner;
	перем buffer: UCS32.PStringJQ; len, i: размерМЗ; ch: симв32; r: Texts.TextReader;
	s : Scanner;
нач
	t.AcquireRead;
	len := t.GetLength();
	нов(buffer, len + 1);	(* при выходе на границу буфера происходит ерунда со счётчиком
	  текущей позиции, посему отступим чуть вперёд *)
	нов(r, t);
	r.SetPosition(pos);
	нцДля i := 0 до len-1 делай
		r.ReadCh(ch);
		buffer[i] := ch кц;
	t.ReleaseRead;
	нов(s); s.buffer := buffer;
	s.pos := 0;
	s.ch := UCS32.IntToCharJQ(кодСимв8(" "));
	возврат s;
кон InitWithText;


проц InitWithUTF8String*(конст vkh: массив из симв8): Scanner;
	перем buffer: UCS32.PStringJQ; 
	s : Scanner;
нач
	нов(buffer, длинаМассива(vkh)+1);	
	UCS32.COPYU8vJQ(vkh, buffer^); 
	нов(s); s.buffer := buffer;
	s.pos := 0;
	s.ch := UCS32.IntToCharJQ(кодСимв8(" "));
	возврат s;
кон InitWithUTF8String;

(* PROCEDURE ExpandBuf(VAR oldBuf: UCS32.PStringJQ; newSize: SIZE);
VAR newBuf: UCS32.PStringJQ; i: SIZE;
BEGIN
	IF LEN(oldBuf^) >= newSize THEN RETURN END;
	NEW(newBuf, newSize);
	FOR i := 0 TO LEN(oldBuf^)-1 DO
		newBuf[i] := oldBuf[i];
	END;
	oldBuf := newBuf;
END ExpandBuf; *)


(* rez - на входе это ожидаемый результат, на выходе - актуальный *)
проц TestInner(конст iskh : массив 10000 из симв8; перем rez : массив 10000 из симв8) : булево;
перем scanner : Scanner; sym : размерМЗ; i : целМЗ; sw : Потоки.ПисарьВСтроку8ФиксированногоРазмера; r2 : массив 10000 из симв8;
конст limit = 500; 
нач
	scanner := InitWithUTF8String(iskh);
	нов(sw, 10000);
	нц
		scanner.Get(sym);
		увел(i);
		sw.пЦел64(sym,0); sw.пСтроку8(" ");
		если i остОтДеленияНа 16 = 0 то
			sw.пВК_ПС всё;
		если sym = eof то
			sw.пВК_ПС;			
			прервиЦикл всё;
		если i = limit то
			sw.пВК_ПС; sw.пСтроку8("Защита от зацикливания");
			sw.пВК_ПС;
			sw.ДайПрочитанное˛сколькоПоместитсяИСимвол0(r2);
			прервиЦикл всё кц;
	sw.ДайПрочитанное˛сколькоПоместитсяИСимвол0(r2);
	если rez # r2 то
		rez := r2;
		возврат ложь всё;
	возврат истина кон TestInner;

проц Test*(c : Commands.Context);
перем iskh, ozhidalosq, rez : массив 10000 из симв8; 
перем imja : массив 100 из симв8;
нач
	неважно c.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(imja);
	неважно c.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(iskh);
	неважно c.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(ozhidalosq);
	копируйСтрокуДо0(ozhidalosq, rez);
	если ~TestInner(iskh, rez) то
		ЛогЯдра.пСтроку8("BimboScanner2.Test "); ЛогЯдра.пСтроку8(imja); ЛогЯдра.пСтроку8(" НЕ ПРОШЁЛ"); ЛогЯдра.пВК_ПС;
		ЛогЯдра.пСтроку8(rez); ЛогЯдра.пВК_ПС;
		всё кон Test;

проц InitReservedChars;
перем
	i: размерМЗ;
нач
	нцДля i := 0 до длинаМассива(reservedChar)-1 делай
		если i <= 020H то	(* TAB, CR, ESC ... *)
			reservedChar[i] := истина;
		аесли i < UCS32.GranicaASCII то
			просей симв8ИзКода(бцел8(i)) из
				| "#", "&", "(", ")", "*", "+", ",", "-", ".", "/", "?": reservedChar[i] := истина;
				| ":", ";", "<", "=", ">": reservedChar[i] := истина;
				| "[", "]", "^", "{", "|", "}", "~": reservedChar[i] := истина;
				| "$": reservedChar[i] := истина;
				| 22X, 27X, 7FX: reservedChar[i] := истина;	(* 22X = ", 27X = ', 7FX = del *)
			иначе
				reservedChar[i] := ложь;
			всё
		иначе
			reservedChar[i] := ложь;
		всё;
	кц;
кон InitReservedChars;

нач
	InitReservedChars;
кон BimboScannerUCS32.

(* 

BimboScanner2.Test Дым 'MODULE aaa; "bbbb" (* cccc *)' "71 40 41 41 73 72" ~

BimboScanner2.Test Гуля ~
40 72

BimboScanner2.Test 'CONST limit = 500; BEGIN IGNORE c.arg.GetString(s); 	scanner := InitWithUTF8String(s);	KernelLog.String("BimboScanner2.Test: "); 	KernelLog.String(s); KernelLog.Ln;	LOOP		scanner.Get(sym);		INC(i);		KernelLog.Int(sym,0); KernelLog.String(" ");		IF sym = eof THEN			KernelLog.Ln;			RETURN END;		IF i = limit THEN			KernelLog.String("Защита от зацикливания");			KernelLog.Ln;			RETURN END END END Test;PROCEDURE InitReservedChars;VAR	i: SIZE; BEGIN	FOR i := 0 TO LEN(reservedChar)-1 DO		IF i <= 020H THEN	(* TAB, CR, ESC ... *)			reservedChar[i] := TRUE;		ELSIF i < UCS32.GranicaASCII THEN CASE CHR(UNSIGNED8(i)) OF | "#", "&", "(", ")", "*", "+", ",", "-", ".", "/", "?": reservedChar[i] := TRUE;	| ":", ";", "<", "=", ">": reservedChar[i] := TRUE;		| "[", "]", "^", "{", "|", "}", ppp'~				

BimboScanner2.Test '| mmm , " "'~


BimboScanner2.Test ', " " ddd'~

WMDebugger.Open BimboScanner2.Mod ~

*)

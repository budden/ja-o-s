модуль SystemVersion; (** AUTHOR "Patrick Hunziker"; PURPOSE "unequivocally identify system by combined CRC of Kernel"; *)
(* load this module early in the boot sequence, but after the statically linked modules. Currently, it is called early through Traps.Mod  *)

использует
	Modules, CRC, ЛогЯдра;

	перем	BootCRC-: цел32;

	(* combined CRC of those Kernel Modules that loaded at this point in time.	 *)
	проц GetKernelCRC*():цел32;
	перем m:Modules.Module; crc: CRC.CRC32;
	нач
		нов(crc);
		m:=Modules.ModuleByName("Kernel32"); если m#НУЛЬ то crc.Add(m.crc) всё;
		m:=Modules.ModuleByName("Trace"); если m#НУЛЬ то crc.Add(m.crc) всё;
		m:=Modules.ModuleByName("Machine"); если m#НУЛЬ то crc.Add(m.crc) всё;
		m:=Modules.ModuleByName("Heaps"); если m#НУЛЬ то crc.Add(m.crc) всё;
		m:=Modules.ModuleByName("Modules"); если m#НУЛЬ то crc.Add(m.crc) всё;
		m:=Modules.ModuleByName("Objects"); если m#НУЛЬ то crc.Add(m.crc) всё;
		m:=Modules.ModuleByName("Kernel"); если m#НУЛЬ то crc.Add(m.crc) всё;
		m:=Modules.ModuleByName("KernelLog"); если m#НУЛЬ то crc.Add(m.crc) всё;
		m:=Modules.ModuleByName("Streams"); если m#НУЛЬ то crc.Add(m.crc) всё;
		m:=Modules.ModuleByName("Commands"); если m#НУЛЬ то crc.Add(m.crc) всё;
		m:=Modules.ModuleByName("Files"); если m#НУЛЬ то crc.Add(m.crc) всё;
		m:=Modules.ModuleByName("WinFS"); если m#НУЛЬ то crc.Add(m.crc) всё;
		m:=Modules.ModuleByName("Loader"); если m#НУЛЬ то crc.Add(m.crc) всё;
		m:=Modules.ModuleByName("BootConsole"); если m#НУЛЬ то crc.Add(m.crc) всё;
		возврат crc.Get()
	кон GetKernelCRC;


	проц GetKernelDate(конст name: массив из симв8; перем d,t: цел32): булево;
	нач
		(*! TBD*)
		d:=0; t:=0; возврат ложь;
	кон GetKernelDate;

	проц Test*;
	нач
		ЛогЯдра.пСтроку8("Kernel CRC at boot time: "); ЛогЯдра.п16ричное(BootCRC,8); ЛогЯдра.пВК_ПС;
		ЛогЯдра.пСтроку8("Kernel CRC now: "); ЛогЯдра.п16ричное(GetKernelCRC(),8); ЛогЯдра.пВК_ПС;
	кон Test;


нач
	BootCRC:=GetKernelCRC();
кон SystemVersion.

SystemVersion.Test
System.FreeDownTo SystemVersion ~


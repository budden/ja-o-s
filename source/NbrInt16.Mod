(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль NbrInt16;   (** AUTHOR "adf"; PURPOSE "Alias for type SIGNED16."; *)

использует NbrInt8, Потоки;

тип
	Integer* = цел16;

перем
	MinNbr-, MaxNbr-: Integer;

	(** All arithmetic operations are built into the compiler and therefore need not to be defined here. *)

	(** Basic Functions*)
	проц Abs*( i: Integer ): Integer;
	нач
		если i > MinNbr то возврат матМодуль( i ) иначе возврат i всё
	кон Abs;

	проц Dec*( перем i: Integer );
	нач
		если i > MinNbr то умень( i ) всё
	кон Dec;

	проц Inc*( перем i: Integer );
	нач
		если i < MaxNbr то увел( i ) всё
	кон Inc;

	проц Odd*( i: Integer ): булево;
	нач
		возврат (i остОтДеленияНа 2) = 1
	кон Odd;

	проц Long*( i: NbrInt8.Integer ): Integer;
	нач
		возврат устарПреобразуйКБолееШирокомуЦел( i )
	кон Long;

	проц IsInt8*( i: Integer ): булево;
	нач
		если (i >= NbrInt8.MinNbr) и (i <= NbrInt8.MaxNbr) то возврат истина иначе возврат ложь всё
	кон IsInt8;

	проц Short*( i: Integer ): NbrInt8.Integer;
	нач
		возврат устарПреобразуйКБолееУзкомуЦел( i )
	кон Short;

	проц Max*( x1, x2: Integer ): Integer;
	нач
		если x1 > x2 то возврат x1 иначе возврат x2 всё
	кон Max;

	проц Min*( x1, x2: Integer ): Integer;
	нач
		если x1 < x2 то возврат x1 иначе возврат x2 всё
	кон Min;

	проц Sign*( x: Integer ): NbrInt8.Integer;
	перем sign: NbrInt8.Integer;
	нач
		если x < 0 то sign := -1
		аесли x = 0 то sign := 0
		иначе sign := 1
		всё;
		возврат sign
	кон Sign;

	(** String conversions. *)
(** Admissible characters include: {" ", "-", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ","}. *)
	проц StringToInt*( string: массив из симв8;  перем x: Integer );
	перем negative: булево;  i: NbrInt8.Integer;
	нач
		i := 0;
		(* Pass over any leading white space. *)
		нцПока string[i] = симв8ИзКода( 20H ) делай NbrInt8.Inc( i ) кц;
		(* Determine the sign. *)
		если string[i] = симв8ИзКода( 2DH ) то negative := истина;  NbrInt8.Inc( i ) иначе negative := ложь всё;
		(* Read in the string and convert it into an integer. *)
		x := 0;
		нцПока string[i] # 0X делай
			если (симв8ИзКода( 30H ) <= string[i]) и (string[i] <= симв8ИзКода( 39H )) то x := 10 * x + (кодСимв8( string[i] ) - 30H)
			иначе
				(* Inadmissible character - it is skipped. *)
			всё;
			NbrInt8.Inc( i )
		кц;
		если negative то x := -x всё
	кон StringToInt;

(** LEN(string) >= 8 *)
	проц IntToString*( x: Integer;  перем string: массив из симв8 );
	перем positive: булево;  i, k: NbrInt8.Integer;
		a: массив 7 из симв8;
	нач
		если x > MinNbr то
			(* Determine the sign. *)
			если x < 0 то x := -x;  positive := ложь иначе positive := истина всё;
			(* Convert the integer into a string. *)
			нцПока x > 0 делай a[i] := симв8ИзКода( (x остОтДеленияНа 10) + 30H );  x := x DIV 10;  NbrInt8.Inc( i ) кц;
			(* Test for zero. *)
			если i = 0 то a[0] := симв8ИзКода( 30H );  NbrInt8.Inc( i ) всё;
			(* Terminate the string. *)
			a[i] := 0X;  k := 0;
			если ~positive то
				(* Write a minus sign. *)
				string[k] := симв8ИзКода( 2DH );  NbrInt8.Inc( k )
			всё;
			(* Rewrite the string in a formatted output, inverting the order stored in a[i]. *)
			нцДо
				NbrInt8.Dec( i );  string[k] := a[i];  NbrInt8.Inc( k );
				если (i > 0) и ((i остОтДеленияНа 3) = 0) то
					(* Write a comma. *)
					string[k] := симв8ИзКода( 2CH );  NbrInt8.Inc( k )
				всё
			кцПри i = 0;
			string[k] := 0X
		иначе копируйСтрокуДо0( "-32,768", string )
		всё
	кон IntToString;

(** Persistence: file IO *)
	проц Load*( R: Потоки.Чтец;  перем x: Integer );
	перем char: симв8;  sInt: NbrInt8.Integer;
	нач
		R.чСимв8( char );
		если char = "S" то R.чЦел8_мз( sInt );  x := Long( sInt ) иначе  (* char = "I" *) R.чЦел16_мз( x ) всё
	кон Load;

	проц Store*( W: Потоки.Писарь;  x: Integer );
	перем sInt: NbrInt8.Integer;
	нач
		если IsInt8( x ) то sInt := Short( x );  W.пСимв8( "S" );  W.пЦел8_мз( sInt ) иначе W.пСимв8( "I" );  W.пЦел16_мз( x ) всё
	кон Store;

нач
	MinNbr := матМинимум( цел16 );  MaxNbr := матМаксимум( цел16 )
кон NbrInt16.

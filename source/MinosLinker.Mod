модуль MinosLinker;   (**  AUTHOR "fof"; PURPOSE "Link Minos Image. Standalone Linker taken from OSACompiler from Niklaus Wirth";  **)

использует Потоки, Commands, Files, ЛогЯдра;

конст
	РазмерИмениВMinos = 64; (* при изменении ищи упоминания в других модулях *)
тип
	Name = массив РазмерИмениВMinos из симв8;
	Command = запись name: Name; offset: цел32 кон;
	Module = укль на запись
		name: Name;
		key:  цел32;
		dbase, pbase: цел32;
		size, refcnt: цел32;
		next: Module;
		entries: цел32;
		entry: массив 256 из цел32;
		command: массив РазмерИмениВMinos из Command;
	кон ;

	Linker* = окласс
	перем
		first, last: Module;
		base, heap, descriptorBase, bodyBase: цел32;
		W: Потоки.Писарь;
		Out: Files.File; Rout: Files.Writer;
		code: массив 256*1024 из цел32;		(* tt: increased image size to one megabyte *)
		plain, descriptors: булево;

		проц &InitLinker* (w: Потоки.Писарь; plain, descriptors: булево);
		нач W := w;
			сам.plain := plain; сам.descriptors := descriptors;
		кон InitLinker;

		проц SetPos (pos: Files.Position);
		нач
			Rout.ПротолкниБуферВПоток;
			если pos > Out.Length () то
				Files.OpenWriter(Rout, Out, Out.Length ());
				pos := pos - Out.Length (); нцДо Rout.пСимв8 (0X); умень (pos) кцПри pos = 0
			иначе Files.OpenWriter(Rout, Out, pos)
			всё;
		кон SetPos;

		проц WriteCodeBlock(len, adr: цел32);
		перем i: цел32;
		нач
			если plain то SetPos (adr - base) иначе Rout.пЦел32_мз (len); Rout.пЦел32_мз (adr) всё;
			нцПока i < len делай Rout.пЦел32_мз( code[i]); увел(i) кц;
			если ~plain и (len # 0) то Rout.пЦел32_мз( 0) всё
		кон WriteCodeBlock;

		проц Fixup(fixloc, offset, base: цел32; перем entry: массив из цел32);
			перем instr, next, pno: цел32;
		нач
			нцПока fixloc # 0 делай
				instr := code[fixloc]; next := instr остОтДеленияНа 10000H;
				pno := instr DIV 10000H остОтДеленияНа 100H;
				если instr DIV 1000000H остОтДеленияНа 100H = 0EBH то  (* case  BL *)
					instr := instr DIV 1000000H * 1000000H + (entry[pno] + offset - fixloc - 2) остОтДеленияНа 1000000H
				иначе (*indir. proc. address or indir. variable address *) instr := entry[pno]*4 + base
				всё ;
				code[fixloc] := instr; fixloc := next
			кц
		кон Fixup;

		проц FixSelf(fixloc, base: цел32);
			перем instr, next: цел32;
		нач
			нцПока fixloc # 0 делай
				instr := code[fixloc]; next := instr остОтДеленияНа 10000H;
				code[fixloc] := instr DIV 10000H * 4 + base; fixloc := next
			кц
		кон FixSelf;

		проц ThisMod(перем modname: массив из симв8; перем success: булево): Module;
			перем mod, imp: Module;
				nofimp, nofentries, codelen, fix, fixself, i: цел32;
				R: Files.Reader; F: Files.File;
				name: Name;
				key, datasize: цел32;
				import: массив 256 из Module;			(* tt: Increased from 16 to 256 *)
				fixroot: массив 256 из цел32;		(* tt: Increased from 16 to 256 *)

		нач
			success := истина;
			mod := first;
			нцПока (mod # НУЛЬ) и (mod.name # modname) делай mod := mod.next кц ;
			если mod = НУЛЬ то  (*load*)
(*				W.String(" trying to load module with name: "); W.String(modname); W.Ln; W.Update; *)
				F := ThisFile(modname);
				если F # НУЛЬ то
					Files.OpenReader(R, F, 0);
					нов(mod); mod.next := НУЛЬ; mod.refcnt := 0;
					R.чСтроку8˛включаяСимв0( mod.name); R.чЦел32_мз( mod.key);
					R.чЦел32_мз( fixself);
					R.чСтроку8˛включаяСимв0( name); success := истина; i := 0;
					W.пСтроку8( "module "); W.пСтроку8( mod.name); W.пСтроку8(" key: "); W.п16ричное( mod.key, -9); W.пВК_ПС();
					нцПока (name[0] # 0X) и success делай
						R.чЦел32_мз (key); R.чЦел32_мз (fix);
(*						W.String ("    importing "); W.String(name); W.String(" Key: " );
						W.Hex (key, 9); W.String(" fix: "); W.Int (fix, 6); W.Ln; W.Update;
*)						imp := ThisMod(name, success);
						если imp # НУЛЬ то
							если (key = imp.key) то
								import[i] := imp; увел (imp.refcnt); fixroot[i] := fix; увел(i)
							иначе success := ложь;
								W.пСтроку8( name); W.пСтроку8( " wrong version");
								W.пВК_ПС(); W.ПротолкниБуферВПоток();
							всё ;
						иначе success := ложь;
							W.пСтроку8( name); W.пСтроку8( " not found");
							W.пВК_ПС();
						всё ;
					R.чСтроку8˛включаяСимв0( name); W.ПротолкниБуферВПоток()
					кц ;
					nofimp := i;
					если success то
						если first = НУЛЬ то first := mod иначе last.next := mod всё; last := mod;
						i := 0; R.чСтроку8˛включаяСимв0( mod.command[i].name);
						нцПока mod.command[i].name[0] # 0X делай  (*skip commands*)
							R.чЦел32_мз( mod.command[i].offset); увел (i);
							R.чСтроку8˛включаяСимв0( mod.command[i].name);
						кц ;
						R.чЦел32_мз( nofentries); R.чЦел32_мз( mod.entry[0]); i := 0;
						W.пСтроку8("modEntry ="); W.пЦел64(mod.entry[0],1); W.пВК_ПС;
						нцПока i < nofentries делай увел(i); R.чЦел32_мз( mod.entry[i]) кц ; увел (i); mod.entry[i] := 0; mod.entries := i;
						mod.dbase := heap; R.чЦел32_мз( datasize); увел (heap, datasize); mod.pbase := heap;
						R.чЦел32_мз( codelen); mod.size := codelen*4; увел (heap, mod.size); i := 0;
						нцПока i < codelen делай R.чЦел32_мз( code[i]); увел(i) кц ;
						FixSelf(fixself, mod.pbase); i := 0;
						нцПока i < nofimp делай
							Fixup(fixroot[i], (import[i].pbase - mod.pbase) DIV 4, import[i].pbase, import[i].entry); увел(i)
						кц ;
						W.пСтроку8( "    loading "); W.пСтроку8( mod.name);
						W.пЦел64( codelen*4, 6);
						W.пСтроку8(" ");
						W.п16ричное( mod.dbase,-8);
						W.пСтроку8(" ");
						W.п16ричное( mod.pbase,-8);
						W.пСтроку8(" ");
						W.п16ричное( mod.entry[0]*4 + mod.pbase,-8);
						WriteCodeBlock(codelen, mod.pbase)
					всё
				иначе
					W.пСтроку8( modname); W.пСтроку8( " not found");
					success := ложь;
				всё;
				W.пВК_ПС();  W.ПротолкниБуферВПоток();
			всё ;
			возврат mod
		кон ThisMod;

		проц Bodies;
		перем len, base: цел32; mod: Module;
		нач
			len := 0; base := heap; mod := first;
			нцПока mod # НУЛЬ делай
				code[len] := BodyBranch (mod, heap); увел (len); увел (heap, 4);
				mod := mod.next;
			кц;
			code[len] := Branch (heap, heap); увел (len); увел (heap, 4);
			WriteCodeBlock (len, base);
		кон Bodies;

		проц String (перем str: массив из симв8; перем index: цел32);
		перем i, len: цел32; пачка : бцел32;
		нач
			len := 0; нцПока str[len] # 0X делай увел (len) кц; i := 0;
			нцПока i <= len делай
				пачка := бцел32(кодСимв8 (str[i])) + бцел32(кодСимв8 (str[i+1])) * 100H +
					 бцел32(кодСимв8 (str[i+2])) * 10000H + бцел32(кодСимв8 (str[i+3])) * 1000000H;
				code[index] := цел32(пачка);
				увел (index); увел (i, 4)
			кц;
		кон String;

		проц ModuleDescriptors;
		перем mod: Module; len, prevmod, prevcmd, i, cfix, efix: цел32;
		нач
			mod := first; len := 0; prevmod := 0;
			нцПока mod # НУЛЬ делай
				(* Module *)
(*		W.String (mod.name); W.String (": "); W.Hex (heap + len * 4,9); W.Ln; W.Update; *)
				code[len] := prevmod; prevmod := heap + len * 4; увел (len);
				code[len] := mod.key; увел (len);
				code[len] := mod.dbase; увел (len);
				code[len] := mod.pbase; увел (len);
				code[len] := mod.size; увел (len);
				code[len] := mod.refcnt; увел (len);
				cfix := len; увел (len);
				efix := len; увел (len);
				String (mod.name, len);
				(* Commands *)
				i := 0; prevcmd := 0;
				нцПока mod.command[i].name[0] # 0X делай
(*		W.String ("   "); W.String (mod.command[i].name); W.String (":"); W.Hex (heap + len * 4,10); W.Hex (mod.command[i].offset,10); W.Ln; W.Update; *)
					code[len] := prevcmd; prevcmd := heap + len * 4; увел (len);
					code[len] := mod.command[i].offset; увел (len);
					String (mod.command[i].name, len); увел (i)
				кц;
				если i # 0 то code[len] := 0; увел (len) всё; (* sentinel *)
				code[cfix] := prevcmd;
				code[efix] := heap + len * 4; i := 0;

(*			W.String ("   Entries:"); W.Ln; *)
				нцПока i # mod.entries делай
(* 			W.String ("      "); W.Int (i,0); W.String (": "); W.Hex (mod.entry[i], 0); W.Ln; *)
					code[len] := mod.entry[i]; увел (len); увел (i);
				кц;
				mod := mod.next;
			кц;
			WriteCodeBlock (len, heap);
			увел (heap, len * 4);
			code[0] := prevmod;
			WriteCodeBlock (1, descriptorBase);
		кон ModuleDescriptors;

		проц AddHeader(fileHeader: массив из симв8; перем success: булево);
		перем
			header: Files.File;
			in: Files.Reader;
			data, i: цел32;
		нач
			i := 0;
			если fileHeader # "" то
				header := Files.Old(fileHeader);
				если header = НУЛЬ то
					W.пСтроку8("Could not open header file "); W.пСтроку8(fileHeader); W.пВК_ПС; W.ПротолкниБуферВПоток;
					success := ложь;
				иначе
			 		Files.OpenReader(in, header, 0);
			 		нцПока in.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() >= 4 делай
			 			in.чЦел32_мз(data); code[i] := data; увел(heap, 4); увел(i);
			 		кц;

					WriteCodeBlock(i, base);
				всё;
			всё;
		кон AddHeader;

		проц Begin* (base: цел32; fileOut, fileHeader: массив из симв8; перем success: булево);
		нач сам.base := base; heap := base;
			first := НУЛЬ; last := НУЛЬ;
			Out := Files.New(fileOut);  Files.OpenWriter(Rout, Out, 0);
			AddHeader(fileHeader, success);
			bodyBase := heap;
			если plain то увел (heap, 4) всё; (* jump to entry point *)
			если descriptors то descriptorBase := heap; увел (heap, 4) всё; (* pointer to first module descriptor *)
		кон Begin;

		проц Link*(fileIn: массив из симв8; перем success: булево);
		перем mod: Module;
		нач
			success := истина;
			mod := ThisMod(fileIn, success);
		кон Link;

		проц End*;
		перем link: цел32;
			fileName: Files.FileName;
		нач
			если first = НУЛЬ то
				W.пСтроку8 ("No output");
			иначе
				если descriptors то ModuleDescriptors всё;
				link := heap; Bodies;
				если plain то code[0] := Branch (link, bodyBase); WriteCodeBlock (1, bodyBase)
				иначе WriteCodeBlock (0, link) всё;
				Out.GetName(fileName); Rout.ПротолкниБуферВПоток(); Files.Register(Out);
				W.пСтроку8("Wrote image file "); W.пСтроку8(fileName); W.пВК_ПС;
				W.пСтроку8( "Output file length ="); W.пЦел64( Out.Length(), -8); W.пСимв8(' ');
				W.пСтроку8("First entry at "); W.п16ричное( first.entry[0]*4 + first.pbase, -9); W.пВК_ПС(); W.ПротолкниБуферВПоток();
				сам.first := НУЛЬ; сам.last := НУЛЬ; Out := НУЛЬ;
			всё;
		кон End;

	кон Linker;

	проц Branch (dest, pc: цел32): цел32;
	нач возврат цел32(0EA000000H) + ((dest - pc) DIV 4 - 2) остОтДеленияНа 1000000H
	кон Branch;

	проц BranchLink (dest, pc: цел32): цел32;
	нач возврат цел32(0EB000000H) + ((dest - pc) DIV 4 - 2) остОтДеленияНа 1000000H
	кон BranchLink;

	проц BodyBranch (m: Module; pc: цел32): цел32;
	нач возврат BranchLink (m.pbase + m.entry[0] * 4, pc);
	кон BodyBranch;

	проц ThisFile(name: массив из симв8): Files.File;
		перем i: цел16;
	нач i := 0;
		нцПока name[i] # 0X делай увел(i) кц ;
		name[i] := "."; name[i+1] := "a"; name[i+2] := "r"; name[i+3] := "m"; name[i+4] := 0X;
		возврат Files.Old(name)
	кон ThisFile;

перем
	log: Потоки.Писарь;   (* logger to KernelLog *)

	проц DoLink( linker: Linker; addHeaderFile: булево; context: Commands.Context );
	перем S: Потоки.Чтец;  fileOut,fileIn, fileHeader: массив 256 из симв8; base: цел32;
		success: булево; intRes: цел32;
	нач
		success := истина;
		S := context.arg;
		если addHeaderFile то
			S.ПропустиБелоеПоле;  S.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( fileHeader );
		иначе
			fileHeader := "";
		всё;
		S.ПропустиБелоеПоле;  S.чЦел32( base, истина );
		S.ПропустиБелоеПоле;  S.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( fileOut );
		Files.Delete(fileOut, intRes);				(* Try to delete an existing output file *)
		linker.Begin (base, fileOut, fileHeader, success);
		нцПока (S.кодВозвратаПоследнейОперации = Потоки.Успех) и success делай
			S.ПропустиБелоеПоле;  S.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( fileIn );
			если fileIn[0] # 0X то linker.Link (fileIn, success) всё;
		кц;
		если success то linker.End иначе context.result := Commands.CommandError всё;
		SetLog(НУЛЬ);
	кон DoLink;

	проц Link*( context: Commands.Context );
	перем linker: Linker;
	нач
		SetLog(context.out);
		нов (linker, log, истина, истина);
		DoLink(linker, истина, context);
		SetLog(НУЛЬ);
	кон Link;

	проц SetLog*( Log: Потоки.Писарь );
	нач
		если Log = НУЛЬ то нов( log, ЛогЯдра.ЗапишиВПоток, 512 ) иначе log := Log всё;
	кон SetLog;

нач
	SetLog( НУЛЬ );
кон MinosLinker.

System.Free MinosLinker ~

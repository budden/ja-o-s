модуль NewHTTPClient; (** AUTHOR "TF"; PURPOSE "HTTP 1.1 client"; *)

использует
	Потоки, WebHTTP, IP, DNS, TCP, Строки8, Files, TFLog, Modules, ЛогЯдра;


конст
	ErrIllegalURL* = -1;
	ErrNotConnected* = -2;
	ErrIllegalResponse* = -3;

тип
	(*LimitedInStream* = OBJECT
	VAR inR: Streams.Reader;
		remain: SIGNED32;

		PROCEDURE &Init*(VAR inR, outR: Streams.Reader; size : SIGNED32);
		BEGIN
			SELF.inR := inR;	remain := size;
			Streams.OpenReader(outR, Receiver);
		END Init;

		PROCEDURE Receiver(VAR buf: ARRAY OF CHAR; ofs, size, min: SIGNED32; VAR len, res: SIGNED32);
		VAR l: SIGNED32;
		BEGIN
			IF remain > 0 THEN
				ASSERT((size > 0) & (min <= size) & (min >= 0));
				res := Streams.Ok;
				l := size; IF l > remain THEN l := remain END;
				inR.Bytes(buf, ofs, l, len);
				DEC(remain, len);
(*				KernelLog.String("remain= "); KernelLog.Int(remain, 0); KernelLog.Ln; *)
			ELSE res := Streams.EOF
			END
		END Receiver;
	END LimitedInStream;
*)

	HTTPConnection* = окласс
	перем host, referer, useragent, accept : массив 128 из симв8;
		port : цел32;
		http11 : булево;
		con : TCP.Connection;
		requestHeader*: WebHTTP.RequestHeader;
		responseHeader*: WebHTTP.ResponseHeader;

		проц &New*;
		нач
			requestHeader.referer := "";
			requestHeader.useragent := "BimBrowser (BimbOS 2004)";
		кон New;

		проц Open;
		перем
			fadr: IP.Adr;
			res : целМЗ;

		нач
			если (con # НУЛЬ) и (con.state = TCP.Established) то возврат всё;
			DNS.HostByName(host, fadr, res);
			если res = DNS.Ok то
				нов(con); con.Open(TCP.NilPort, fadr, port, res);
				если res # TCP.Ok то con := НУЛЬ всё
			всё
		кон Open;

		проц Close*;
		нач
			если con # НУЛЬ то con.Закрой; con := НУЛЬ всё;
		кон Close;

		проц Get*(конст url : массив из симв8; http11 : булево; перем out : Потоки.Чтец; перем res : целМЗ);
		перем w : Потоки.Писарь; r : Потоки.Чтец;
			x : WebHTTP.AdditionalField;
			host : массив 128 из симв8;
			path : массив 2048 из симв8;
			port : цел32;
			dechunk: WebHTTP.ChunkedInStream;
			lin : WebHTTP.LimitedInStream;
		нач
			requestHeader.maj := 1;
			если http11 то requestHeader.min := 1 иначе requestHeader.min := 0 всё;

			если WebHTTP.SplitHTTPAdr(url, host, path, port) то
				если (host # сам.host) или (port # сам.port) то Close всё;
				копируйСтрокуДо0(host, сам.host);
				сам.port := port;

				если path = "" то path := "/" всё;
				(* (re)establish the connection *)
				Open;
				если con = НУЛЬ то res := ErrNotConnected; возврат всё;
				Потоки.НастройПисаря(w, con.ЗапишиВПоток); Потоки.НастройЧтеца(r, con.ПрочтиИзПотока);

				WebHTTP.WriteRequestLine(w, requestHeader.maj, requestHeader.min, WebHTTP.GetM, path, host);

				если requestHeader.referer # "" то w.пСтроку8("Referer: "); w.пСтроку8(requestHeader.referer); w.пВК_ПС() всё;
				если requestHeader.useragent # "" то w.пСтроку8("User-Agent: "); w.пСтроку8(requestHeader.useragent); w.пВК_ПС() всё;
				если requestHeader.accept # "" то w.пСтроку8("Accept: "); w.пСтроку8(requestHeader.accept); w.пВК_ПС() всё;
				x := requestHeader.additionalFields;
				нцПока x # НУЛЬ делай
					w.пСтроку8(x.key);  w.пСимв8(" "); w.пСтроку8(x.value);w.пВК_ПС();

					x := x.next
				кц;
				w.пВК_ПС(); w.ПротолкниБуферВПоток();

				WebHTTP.ParseReply(r, responseHeader, res, log);

				если (Строки8.НайдиПодстроку("hunked", responseHeader.transferencoding) > 0) то нов(dechunk, r, out)
				аесли responseHeader.contentlength >= 0 то нов(lin, r, out, responseHeader.contentlength)
				иначе out := r
				всё;

(*				WebHTTP.LogResponseHeader(log, responseHeader);*)
				res := 0;
				x := responseHeader.additionalFields;
				нцПока x # НУЛЬ делай
					x := x.next
				кц;


			иначе
				res := ErrIllegalURL
			всё

		кон Get;

				(*POST HTTP/1.1 message. needs either a Content-Length of the body or a chunked transfer encoding*)
		проц Post*(конст url : массив из симв8; конст headervars: массив из симв8; MIME: массив из симв8; body: Потоки.Чтец; length: размерМЗ;  перем out : Потоки.Чтец; перем res : целМЗ);
		перем w : Потоки.Писарь; r : Потоки.Чтец;
			x : WebHTTP.AdditionalField;
			host : массив 128 из симв8;
			path : массив 2048 из симв8;
			buf: массив 1024 из симв8;
			port : цел32;
			dechunk: WebHTTP.ChunkedInStream;
			lin : WebHTTP.LimitedInStream;
			len: массив 16 из симв8;
			l:размерМЗ;
		нач
			requestHeader.maj := 1;
			если истина то requestHeader.min := 1 иначе requestHeader.min := 0 всё;

			если WebHTTP.SplitHTTPAdr(url, host, path, port) то
				если (host # сам.host) или (port # сам.port) то Close всё;
				копируйСтрокуДо0(host, сам.host);
				сам.port := port;

				если path = "" то path := "/" всё;
				если Строки8.КвоБайтБезЗавершающего0(headervars)>0 то Строки8.ПодклейВСтрокуСимв8(path,"?"); Строки8.ПодклейВСтрокуХвост(path,headervars); всё;
				(* (re)establish the connection *)
				Open;
				если con = НУЛЬ то res := ErrNotConnected; возврат всё;
				Потоки.НастройПисаря(w, con.ЗапишиВПоток); Потоки.НастройЧтеца(r, con.ПрочтиИзПотока);

				WebHTTP.WriteRequestLine(w, requestHeader.maj, requestHeader.min, WebHTTP.PostM, path, host);

				если requestHeader.referer # "" то w.пСтроку8("Referer: "); w.пСтроку8(requestHeader.referer); w.пВК_ПС() всё;
				если requestHeader.useragent # "" то w.пСтроку8("User-Agent: "); w.пСтроку8(requestHeader.useragent); w.пВК_ПС() всё;
				если requestHeader.accept # "" то w.пСтроку8("Accept: "); w.пСтроку8(requestHeader.accept); w.пВК_ПС() всё;
				WebHTTP.SetAdditionalFieldValue(requestHeader.additionalFields,"Content-Type",MIME);
				если length>0 то
					Строки8.ПишиЦел64_вСтроку(length,len);
					WebHTTP.SetAdditionalFieldValue(requestHeader.additionalFields,"Content-Length", len);
				иначе СТОП(300) (*chunked sending not yet implemented - need adaptation of WebHTTP.ChunkedOutStream*)
				всё;
				x := requestHeader.additionalFields;
				нцПока x # НУЛЬ делай
					w.пСтроку8(x.key);  w.пСтроку8(": "); w.пСтроку8(x.value);w.пВК_ПС();
					x := x.next
				кц;
				w.пВК_ПС();

				нцПока length>0 делай
					body.чБайты(buf, 0, матМинимум(длинаМассива(buf),length), l);
					w.пБайты(buf,0,l);
					умень(length,l);
				кц;
				w.ПротолкниБуферВПоток();

				WebHTTP.ParseReply(r, responseHeader, res, log);

				если (Строки8.НайдиПодстроку("hunked", responseHeader.transferencoding) > 0) то нов(dechunk, r, out)
				аесли responseHeader.contentlength >= 0 то нов(lin, r, out, responseHeader.contentlength)
				иначе out := r
				всё;

				WebHTTP.LogResponseHeader(log, responseHeader);
				res := 0;
				x := responseHeader.additionalFields;
				нцПока x # НУЛЬ делай
					x := x.next
				кц;

			иначе
				res := ErrIllegalURL
			всё

		кон Post;

	кон HTTPConnection;

перем log : TFLog.Log;

проц CleanUp;
нач
	log.Close
кон CleanUp;


проц Test*;
перем h : HTTPConnection;
	r : Потоки.Чтец;
	res : целМЗ;
нач
	нов(h);
	h.Get("http://www.bimbodot.org", истина, r, res);
	ЛогЯдра.пСтроку8("res= "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;
	нцПока r.кодВозвратаПоследнейОперации = 0 делай ЛогЯдра.пСимв8(r.чИДайСимв8()) кц;
	ЛогЯдра.пСтроку8("Loop finished");
	h.Close;
кон Test;

проц Test2*;
перем h : HTTPConnection;
	r : Потоки.Чтец;
	res : целМЗ;
	f:Files.File;
	fr: Files.Reader;
нач
	нов(h);
	f:=Files.Old("test.txt"); Files.OpenReader(fr,f,0);
	h.Post("http://127.0.0.1/Trap", "sender=myself&receiver=you", "text/plain", fr, f.Length()(размерМЗ), r, res);
	ЛогЯдра.пСтроку8("res= "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;
	нцПока r.кодВозвратаПоследнейОперации = 0 делай ЛогЯдра.пСимв8(r.чИДайСимв8()) кц;
	ЛогЯдра.пСтроку8("Loop finished");
	h.Close;
кон Test2;

проц TestGoogle*;
перем h : HTTPConnection;
	r : Потоки.Чтец;
	res : целМЗ;
нач
	нов(h);

	(* Mit Mozilla als User Agent kriegt man UTF-8 *)
	h.requestHeader.useragent := "Mozilla/5.0";

	(* Mit BimBrowser ISO-8859-1 *)
	(* h.requestHeader.useragent := "BimBrowser (bluebottle.ethz.ch)"; *)

	(* ... auch wenn man UTF-8 anfordert ... *)
	(*h.requestHeader.useragent := "BimBrowser (bluebottle.ethz.ch)";
	NEW(af1);
	af1.key := "Accept-Charset:";
	af1.value := "utf-8";
	h.requestHeader.additionalFields := af1;*)

	(* ... auch wenn sonst alles identisch ist, wie bei Mozilla: *)
	(*h.requestHeader.useragent := "BimBrowser (bluebottle.ethz.ch)";
	h.requestHeader.accept := "text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,image/jpeg,image/gif;q=0.2,*/*;q=0.1";
	NEW(af1); NEW(af2); NEW(af3); NEW(af4); NEW(af5);
	af1.key := "Accept-Language:";
	af1.value := "de,en-us;q=0.7,en;q=0.3";
	af1.next := af2;
	af2.key := "Accept-Encoding:";
	af2.value := ""; (* af2.value := "gzip,deflate"; *)
	af2.next := af3;
	af3.key := "Accept-Charset:";
	af3.value := "utf-8,ISO-8859-1;q=0.7,*;q=0.7"; (* habe utf-8 und ISO vertauscht *)
	af3.next := NIL;
	af4.key := "Keep-Alive:";
	af4.value := "300";
	af4.next := af5;
	af5.key := "Connection:";
	af5.value := "keep-alive";
	h.requestHeader.additionalFields := af1;*)

	h.Get("http://www.google.ch", истина, r, res);
	ЛогЯдра.пСтроку8("res= "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;
	нцПока r.кодВозвратаПоследнейОперации = 0 делай ЛогЯдра.пСимв8(r.чИДайСимв8()) кц;
	ЛогЯдра.пСтроку8("Loop finished");
	h.Close;
кон TestGoogle;

нач
	нов(log, "HTTP Client");
	log.SetLogToOut(истина);
	Modules.InstallTermHandler(CleanUp)
кон NewHTTPClient.

System.Free NewHTTPClient ~
NewHTTPClient.Test ~
NewHTTPClient.TestGoogle ~

модуль FoxIntermediateAssembler; (** AUTHOR ""; PURPOSE ""; *)

использует IntermediateCode := FoxIntermediateCode, FoxAssembler, D := Debugging, Scanner := FoxScanner, Basic := FoxBasic;

конст Trace=FoxAssembler.Trace;
тип
	Register* = цел32; (* index for InstructionSet.registers *)
	Operand* = IntermediateCode.Operand;

	тип
	Assembler*= окласс (FoxAssembler.Assembler)

		проц {перекрыта}Instruction*(конст mnemonic: массив из симв8);
		перем i,numberOperands,mnem: цел32; pos: Basic.Position; перем operands: массив 3 из Operand; instruction: IntermediateCode.Instruction;

			проц ParseOperand;
			(* stub, must be overwritten by implementation *)
			перем operand: IntermediateCode.Operand;
				result: FoxAssembler.Result;
				register1,register2:цел32;
				class1, class2: IntermediateCode.RegisterClass;
				stop,memory: булево;
			нач
				stop := ложь;
				register1 := IntermediateCode.None;
				register2 := IntermediateCode.None;
				result.type := -1;
				result.value := 0;

				если numberOperands >= 3 то Error(errorPosition,"too many operands")
				иначе
					memory := ThisSymbol(Scanner.ОткрывающаяКвадратнаяСкобка);
					если (token.видЛексемы = Scanner.Идентификатор) и IntermediateCode.DenotesRegister(token.строкаИдентификатора,class1,register1) то
						NextToken;
						stop := ~ThisSymbol(Scanner.ЗнакПлюс);
					всё;
					если ~stop то
						если (token.видЛексемы = Scanner.Идентификатор) то
							если IntermediateCode.DenotesRegister(token.строкаИдентификатора,class2,register2) то
								NextToken;
							аесли GetNonConstant(errorPosition,token.строкаИдентификатора,result) то
								NextToken;
							аесли Expression(result,ложь) то
							всё;
						аесли Expression(result,ложь) то
						всё;
					всё;
					если memory и ExpectSymbol(Scanner.ЗакрывающаяКвадратнаяСкобка) то
						(*
						IntermediateCode.InitMemory(operand,register1,register2,result.value);
						*)
					аесли register1 # -1 то
						(*
						IntermediateCode.InitRegister(operand,0,register1);
						*)
					иначе
						(*
						IntermediateCode.InitImmediate(operand,result.sizeInBits,result.value);
						*)
					всё;
					(*
					IF result.fixup # NIL THEN
						IntermediateCode.AddFixup(operand,result.fixup);
					END;
					*)
					operands[numberOperands] := operand;
				всё;
			кон ParseOperand;

		нач
			если Trace то
				D.String("Instruction: "); D.String(mnemonic);  D.String(" "); D.Ln;
			всё;
			pos := errorPosition;
			mnem := IntermediateCode.FindMnemonic(mnemonic);
			если mnem >= 0 то
				нцДля i := 0 до 2 делай IntermediateCode.InitOperand(operands[i]) кц;
				numberOperands := 0;
				если token.видЛексемы # Scanner.Ln то
					нцДо
						ParseOperand;
						увел(numberOperands);
					кцПри error или ~ThisSymbol(Scanner.Запятая);
				всё;

				если ~error то
					IntermediateCode.InitInstruction(instruction, pos, цел8(mnem), operands[0], operands[1], operands[2]);
					section.Emit(instruction);
					(*

					mnem,operands[0],operands[1],operands[2],section.resolved);
					*)
				всё

			иначе
				ErrorSS(pos,"unknown instruction ",mnemonic)
			всё
		кон Instruction;

	кон Assembler;

кон FoxIntermediateAssembler.

System.Free FoxInlineAssembler FoxInlineInstructionSet ~

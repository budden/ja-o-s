(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль DNS; (** AUTHOR "pjm, mvt"; PURPOSE "DNS client"; *)

(* Portions based on NetDNS.Mod by mg et al. *)

использует ЛогЯдра, ЭВМ, Kernel, Network, IP, UDP;

конст
	(** Error codes *)
	Ok* = 0;
	NotFound* = 3601;
	BadName* = 3602;
	MaxNofServer* = 10; (* max. number registered of DNS servers *)

	UDPTimeout = 1000; (* time per server query in ms *)
	Tries = 5; (* number of tries per server *)

	BadNameTimeout = 30; (* how many seconds to cache a bad name *)

	ArpaDomain = "IN-ADDR.ARPA";

	TypeA = 1;
	TypeAAAA = 28;
	TypeMX = 15;
	TypePTR = 12;
	TypeIN = 1;
	TypeRD = 100H;

	DNSPort = 53;

	Trace = ложь;

тип
	Name* = массив 128 из симв8; (* domain or host name type *)

	Cache = укль на запись
		next: Cache;
		name, domain: Name;
		adr: IP.Adr;
		expire: цел32;
	кон;

тип
	(* Internal server list - updated before each query *)

	ServerList = окласс
		перем
			server: массив MaxNofServer из IP.Adr;
			currentServer, serverCount: цел32;

		проц &Constr*;
		нач
			currentServer := 0;
			serverCount := 0;
		кон Constr;

		(* Update internal server list. Return number of servers. *)

		проц Update(): цел32;
		нач {единолично}
			serverCount := 0;
			IP.Enumerate(InterfaceHandler);
			если currentServer >= serverCount то
				currentServer := 0;
			всё;
			возврат serverCount;
		кон Update;

		(* Get current server. *)

		проц GetServer(): IP.Adr;
		нач {единолично}
			если serverCount > 0 то
				возврат server[currentServer];
			иначе
				возврат IP.NilAdr;
			всё;
		кон GetServer;

		(* Report current server to be bad. *)

		проц ReportBadServer;
		нач {единолично}
			если serverCount > 0 то
				если Trace то
					ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("DNS: Server "); IP.OutAdr(server[currentServer]);
					ЛогЯдра.пСтроку8(" doesn't work. Switching to next..."); ЛогЯдра.пВК_ПС; ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
				всё;
				currentServer := (currentServer + 1) остОтДеленияНа serverCount;
			всё;
		кон ReportBadServer;

		(* Handle a call from IP.Enumerate() - update internal DNS server list. *)

		проц InterfaceHandler(int: IP.Interface);
		перем i: цел32;
		нач
			если int.dev.Linked() # Network.LinkNotLinked то
				i := 0;
				нцПока i < int.DNScount делай
					server[serverCount] := int.DNS[i];
					увел(serverCount);
					увел(i);
				кц;
			иначе
				(* device currently not linked to network *)
			всё;
		кон InterfaceHandler;

	кон ServerList;

перем
	(** Local domain name *)
	domain*: Name;

	id: цел32;
	cache: Cache;
	lastCleanup: цел32;

	serverlist: ServerList;

	(* Statistic variables *)
	NDNSReceived-, NDNSSent-, NDNSMismatchID-, NDNSError-: цел32;

проц CacheCleanup;
перем c, p: Cache; now: цел32;
нач {единолично}
	now := Kernel.GetTicks();
	если now - lastCleanup > Kernel.second то
		lastCleanup := now;
		p := cache; c := cache.next;
		нцПока c # НУЛЬ делай
			если c.expire - now < 0 то
				если Trace то
					ЛогЯдра.пСтроку8(" ("); ЛогЯдра.пСтроку8(c.name); ЛогЯдра.пСтроку8(" expired)")
				всё;
				p.next := c.next; c := c.next
			иначе
				p := c; c := c.next
			всё
		кц
	всё
кон CacheCleanup;

проц CacheAdd(name: массив из симв8; adr: IP.Adr; domain: массив из симв8; timeout: цел32);
перем c: Cache; expire: цел32;
нач {единолично}
	если timeout > матМаксимум(цел32) DIV Kernel.second то timeout := матМаксимум(цел32)
	иначе timeout := timeout * Kernel.second
	всё;
	expire := Kernel.GetTicks() + timeout; c := cache.next;
	нцПока (c # НУЛЬ) и ((name # c.name) или (~IP.AdrsEqual(adr, c.adr))) делай c := c.next кц;
	если c = НУЛЬ то
		если Trace то ЛогЯдра.пСтроку8(" added "); ЛогЯдра.пЦел64(timeout, 1) всё;
		нов(c); копируйСтрокуДо0(name, c.name); копируйСтрокуДо0(domain, c.domain); c.adr := adr;
		c.expire := expire; c.next := cache.next; cache.next := c
	иначе
		если expire - c.expire > 0 то
			если Trace то ЛогЯдра.пСтроку8(" refreshed "); ЛогЯдра.пЦел64(timeout, 1) всё;
			c.expire := expire; копируйСтрокуДо0(name, c.name); копируйСтрокуДо0(domain, c.domain); c.adr := adr
		всё
	всё
кон CacheAdd;

проц CacheFindDomain(domain: массив из симв8): Cache;
перем c: Cache;
нач
	CacheCleanup;
	c := cache.next;
	нцПока (c # НУЛЬ) и (domain # c.domain) делай c := c.next кц;
	если Trace то
		если c = НУЛЬ то ЛогЯдра.пСтроку8(" not") всё;
		ЛогЯдра.пСтроку8(" in cache");
		если c # НУЛЬ то ЛогЯдра.пСимв8(" "); ЛогЯдра.пЦел64((c.expire - Kernel.GetTicks()) DIV Kernel.second, 1) всё
	всё;
	возврат c
кон CacheFindDomain;

проц CacheFindName(name: массив из симв8): Cache;
перем c: Cache;
нач
	CacheCleanup;
	c := cache.next;
	нцПока (c # НУЛЬ) и (name # c.name) делай c := c.next кц;
	если Trace то
		если c = НУЛЬ то ЛогЯдра.пСтроку8(" not") всё;
		ЛогЯдра.пСтроку8(" in cache");
		если c # НУЛЬ то ЛогЯдра.пСимв8(" "); ЛогЯдра.пЦел64((c.expire - Kernel.GetTicks()) DIV Kernel.second, 1) всё
	всё;
	возврат c
кон CacheFindName;

проц CacheFindAdr(adr: IP.Adr): Cache;
перем c: Cache;
нач
	CacheCleanup;
	c := cache.next;
	нцПока (c # НУЛЬ) и (~IP.AdrsEqual(adr, c.adr)) делай c := c.next кц;
	если Trace то
		если c = НУЛЬ то ЛогЯдра.пСтроку8(" not") всё;
		ЛогЯдра.пСтроку8(" in cache");
		если c # НУЛЬ то ЛогЯдра.пСимв8(" "); ЛогЯдра.пЦел64((c.expire - Kernel.GetTicks()) DIV Kernel.second, 1) всё
	всё;
	возврат c
кон CacheFindAdr;

проц AppW(перем k: цел32; перем buf: массив из симв8; n: цел32);
нач
	buf[k] := симв8ИзКода(n DIV 100H остОтДеленияНа 100H); buf[k+1] := симв8ИзКода(n остОтДеленияНа 100H); увел(k, 2)
кон AppW;

проц QSect(перем k: цел32; перем buf, name: массив из симв8; type, class: цел32);
перем i, j: цел32;
нач
	i := 0; j := k; увел(k);
	нцПока name[i] # 0X делай
		если name[i] = "." то buf[j] := симв8ИзКода(k-j-1); j := k	(* fixup len *)
		иначе buf[k] := name[i]
		всё;
		увел(k); увел(i)
	кц;
	buf[j] := симв8ИзКода(k-j-1); buf[k] := 0X; увел(k);
	AppW(k, buf, type); AppW(k, buf, class)
кон QSect;

проц PickW(перем k: цел32; перем buf: массив из симв8; перем n: цел32);
нач
	n := арифмСдвиг(кодСимв8(buf[k]), 8) + кодСимв8(buf[k+1]); увел(k, 2)
кон PickW;

проц Lower(перем s: массив из симв8);
перем i: цел32;
нач
	i := 0;
	нцПока s[i] # 0X делай
		если (s[i] >= "A") и (s[i] <= "Z") то s[i] := симв8ИзКода(кодСимв8(s[i])+32) всё;
		увел(i)
	кц
кон Lower;

проц GetName(перем k, i: цел32; перем buf, name: массив из симв8);
перем len, k0: цел32;
нач
	len := кодСимв8(buf[k]); увел(k);
	нцПока len > 0 делай
		если len >= 0C0H то
			k0 := 100H*(len-0C0H)+кодСимв8(buf[k]); увел(k);
			GetName(k0, i, buf, name); name[i] := 0X; возврат
		иначе
			нцПока len > 0 делай name[i] := buf[k]; увел(i); увел(k); умень(len) кц
		всё;
		len := кодСимв8(buf[k]); увел(k);
		если len > 0 то name[i] := "."; увел(i) всё
	кц;
	name[i] := 0X; Lower(name)
кон GetName;

проц Header(перем k: цел32; перем buf: массив из симв8; id, flags, qd, an, ns, ar: цел32);
нач
	AppW(k, buf, id); AppW(k, buf, flags); AppW(k, buf, qd);
	AppW(k, buf, an); AppW(k, buf, ns); AppW(k, buf, ar)
кон Header;

проц Domain(перем name: массив из симв8; localdom: массив из симв8; force: булево);
перем i, j: цел32;
нач
	i := 0; j := 0;
	нцПока name[i] # 0X делай
		если name[i] = "." то j := i всё;
		увел(i)
	кц;
	если force или (j = 0) то
		j := 0; name[i] := "."; увел(i); (* append domain *)
		нцПока localdom[j] # 0X делай name[i] := localdom[j]; увел(i); увел(j) кц;
		name[i] := 0X
	всё;
	i := 0; j := 0;	(* remove extraneous dots *)
	нцПока name[i] = "." делай увел(i) кц;
	нцПока name[i] # 0X делай
		name[j] := name[i]; увел(i); увел(j);
		если name[i-1] = "." то
			нцПока name[i] = "." делай увел(i) кц;
			если name[i] = 0X то умень(j) всё
		всё
	кц;
	name[j] := 0X
кон Domain;

проц RetrieveInfo(qtype: цел32; перем adr: IP.Adr; перем buf, hname: массив из симв8; перем len: размерМЗ; перем timeout: цел32; перем res: целМЗ);
перем
	name0: Name;
	adr0: IP.Adr;
	c, i, k, l, id0, flags, qd, an, ns, ar, type, class, ttl1, ttl0, ttl: цел32;

нач
	k := 0; timeout := 0; res := NotFound; hname[0] := 0X;
	PickW(k, buf, id0);
	если id0 = id то
		PickW(k, buf, flags); PickW(k, buf, qd); PickW(k, buf, an); PickW(k, buf, ns); PickW(k, buf, ar);
		если flags остОтДеленияНа 10H = 0 то
			если Trace то
				ЛогЯдра.пСтроку8(" qd="); ЛогЯдра.пЦел64(qd, 1);
				ЛогЯдра.пСтроку8(" an="); ЛогЯдра.пЦел64(an, 1);
				ЛогЯдра.пСтроку8(" ns="); ЛогЯдра.пЦел64(ns, 1);
				ЛогЯдра.пСтроку8(" ar="); ЛогЯдра.пЦел64(ar, 1)
			всё;
			нцПока (qd > 0) и (k < len) делай
				i := 0; GetName(k, i, buf, name0); PickW(k, buf, type); PickW(k, buf, class);
				если Trace то
					ЛогЯдра.пСтроку8(" name="); ЛогЯдра.пСтроку8(name0);
					ЛогЯдра.пСтроку8(" type="); ЛогЯдра.пЦел64(type, 1);
					ЛогЯдра.пСтроку8(" class="); ЛогЯдра.пЦел64(class, 1)
				всё;
				умень(qd)
			кц;
			нцПока (an > 0) и (k < len) делай
				i := 0; GetName(k, i, buf, name0); PickW(k, buf, type); PickW(k, buf, class);
				PickW(k, buf, ttl1); PickW(k, buf, ttl0); PickW(k, buf, l);
				ttl := ttl1*10000H + ttl0;
				если Trace то
					ЛогЯдра.пСтроку8(" name="); ЛогЯдра.пСтроку8(name0);
					ЛогЯдра.пСтроку8(" type="); ЛогЯдра.пЦел64(type, 1);
					ЛогЯдра.пСтроку8(" class="); ЛогЯдра.пЦел64(class, 1);
					ЛогЯдра.пСтроку8(" timeout="); ЛогЯдра.пЦел64(ttl, 1);
					ЛогЯдра.пСтроку8(" len="); ЛогЯдра.пЦел64(l, 1)
				всё;
				если type = qtype то
					просей type из
						TypeA:
							adr0.ipv4Adr := Network.Get4(buf, k); (* get IPv4 address *)
							adr0.usedProtocol := IP.IPv4;
							если IP.IsNilAdr(adr) то adr := adr0; timeout := ttl; res := Ok всё;
							увел(k, 4)
						|TypeAAAA:
							adr0.usedProtocol := IP.IPv6;
							нцДля c := 0 до 15 делай
								adr0.ipv6Adr[c] := buf[k+c];
							кц;
							если IP.IsNilAdr(adr) то adr := adr0; timeout := ttl; res := Ok всё;
							увел(k,16);
						|TypePTR:
							если hname[0] = 0X то
								i := 0; GetName(k, i, buf, hname); timeout := ttl; res := Ok
							иначе
								увел(k, l);
							всё;
						| TypeMX:
							если hname[0] = 0X то
								PickW(k, buf, i); (* preference, not used yet *)
								i := 0; GetName(k, i, buf, hname); timeout := ttl; res := Ok
							иначе
								увел(k, l);
							всё;
					всё
				иначе
					увел(k, l)
				всё;
				умень(an)
			кц
		аесли flags остОтДеленияНа 10H = 3 то	(* name error *)
			res := BadName; timeout := BadNameTimeout
		иначе
			увел(NDNSError)
		всё
	иначе
		увел(NDNSMismatchID);
		если Trace то
			ЛогЯдра.пСтроку8(" ID mismatch! Sent ID: "); ЛогЯдра.пЦел64(id, 0);
			ЛогЯдра.пСтроку8(" / Received ID: "); ЛогЯдра.пЦел64(id0, 0); ЛогЯдра.пВК_ПС;
		всё;
	всё
кон RetrieveInfo;

проц SendQuery(pcb: UDP.Socket; server: IP.Adr; name: массив из симв8; type: цел32; перем buf: массив из симв8; перем res: целМЗ);
перем len: цел32;
нач
	len := 0; res := 0;
	Header(len, buf, id, 0 + TypeRD, 1, 0, 0, 0);
	QSect(len, buf, name, type, TypeIN);
	pcb.Send(server, DNSPort, buf, 0, len, res);
	увел(NDNSSent);
кон SendQuery;

проц ReceiveReply(pcb: UDP.Socket; перем buf: массив из симв8; перем len: размерМЗ; перем res: целМЗ);
перем radr: IP.Adr; rport: цел32;
нач
	нцДо
		pcb.Receive(buf, 0, длинаМассива(buf), UDPTimeout, radr, rport, len, res)
	кцПри (rport = DNSPort) и (len > 0) или (res # Ok);
	если res = Ok то увел(NDNSReceived) иначе len := 0 всё
кон ReceiveReply;

проц QueryDNS(type: цел32; перем buf, qname, hname: массив из симв8; перем adr: IP.Adr; перем timeout: цел32; перем res: целМЗ);
перем
	j, k, serverCount: цел32;
	len: размерМЗ;
	pcb: UDP.Socket;
нач
	serverCount := serverlist.Update();

	j := 0; res := NotFound;
	нцПока (res # Ok) и (j < serverCount) делай
		k := 0; ЭВМ.атомарноУвел(id);
		нц
			нов(pcb, UDP.NilPort, res);
			если res # UDP.Ok то
				возврат;
			всё;
			SendQuery(pcb, serverlist.GetServer(), qname, type, buf, res);
			если res # Ok то
				pcb.Close();
				прервиЦикл;
			всё;	(* can not reach this server *)
			нцДо	(* read replies *)
				ReceiveReply(pcb, buf, len, res);
				если (res = Ok) и (len > 0) то
					RetrieveInfo(type, adr, buf, hname, len, timeout, res);
					если (res = Ok) или (res = BadName) то
						pcb.Close();
						возврат;
					всё;
				всё
			кцПри res # Ok;
			pcb.Close();
			увел(k);
			если k = Tries то прервиЦикл всё;	(* maximum tries per server *)
			если Trace то ЛогЯдра.пСтроку8(" retry") всё
		кц;
		если res # Ok то
			serverlist.ReportBadServer();
		всё;
		увел(j)
	кц;
кон QueryDNS;

(** Find the host responsible for mail exchange of the specified domain. *)

проц MailHostByDomain*(domain: массив из симв8; перем hostname: массив из симв8; перем res: целМЗ);
перем
	buf: массив 512 из симв8;
	timeout: цел32;
	c: Cache;
	adr: IP.Adr;
нач
	adr := IP.NilAdr;
	если Trace то ЛогЯдра.пСтроку8("MailByDomain: "); ЛогЯдра.пСтроку8(domain) всё;
	c := CacheFindDomain(domain);
	если c # НУЛЬ то
		копируйСтрокуДо0(c.name, hostname);
		res := Ok;
	иначе
		Lower(domain);
		QueryDNS(TypeMX, buf, domain, hostname, adr, timeout, res);
		если (res = Ok) или (res = BadName) то CacheAdd(hostname, adr, domain, timeout) всё
	всё;
	если Trace то ЛогЯдра.пСтроку8(" res="); ЛогЯдра.пЦел64(res, 1); ЛогЯдра.пВК_ПС всё
кон MailHostByDomain;

(** Find the IP address of the specified host. *)

проц HostByName*(hostname: массив из симв8; перем adr: IP.Adr; перем res: целМЗ);
перем
	buf: массив 512 из симв8;
	name: Name;
	timeout: цел32;
	c: Cache;
	dummy: массив 1 из симв8;
нач
	dummy[0] := 0X;
	adr := IP.StrToAdr(hostname);

	если IP.IsNilAdr (adr) то
		если Trace то ЛогЯдра.пСтроку8("HostByName: "); ЛогЯдра.пСтроку8(hostname) всё;
		копируйСтрокуДо0(hostname, name); Domain(name, domain, ложь); Lower(name);
		если Trace то ЛогЯдра.пСимв8(" "); ЛогЯдра.пСтроку8(name) всё;
		c := CacheFindName(name);
		если c # НУЛЬ то
			adr := c.adr;
			если ~IP.IsNilAdr (adr) то res := Ok иначе res := BadName всё
		иначе
			adr := IP.NilAdr;
			(* Query first preferred protocol family *)
			если IP.preferredProtocol = IP.IPv4 то
				QueryDNS(TypeA, buf, name, dummy, adr, timeout, res);
			иначе
				QueryDNS(TypeAAAA, buf, name, dummy, adr, timeout, res);
			всё;
			если (res = Ok) или (res = BadName) то
				CacheAdd(name, adr, dummy, timeout)
			аесли IP.preferredProtocol = IP.IPv4 то
				(* If a error occured query not preferred protocol family *)
				QueryDNS(TypeAAAA, buf, name, dummy, adr, timeout, res);
			иначе
				QueryDNS(TypeA, buf, name, dummy, adr, timeout, res);
			всё;
			если (res = Ok) или (res = BadName) то
				CacheAdd(name, adr, dummy, timeout);
			всё;
		всё;
		если Trace то ЛогЯдра.пСтроку8(" res="); ЛогЯдра.пЦел64(res, 1); ЛогЯдра.пВК_ПС всё
	иначе
		res := Ok
	всё
кон HostByName;

(** Find the host name of the specified IP address. *)

проц HostByNumber*(adr: IP.Adr; перем hostname: массив из симв8; перем res: целМЗ);
перем
	buf: массив 512 из симв8;
	name: Name;
	i, j, k, timeout: цел32;
	c: Cache;
	int: IP.Interface;
нач
	если ~IP.IsNilAdr(adr) то
		int := IP.InterfaceByDstIP(adr);
		если ~int.IsBroadcast(adr) то
			IP.AdrToStr(adr, buf);
			если Trace то ЛогЯдра.пСтроку8("HostByNumber: "); ЛогЯдра.пСтроку8(buf) всё;
			c := CacheFindAdr(adr);
			если c # НУЛЬ то
				копируйСтрокуДо0(c.name, hostname);
				res := Ok;
			иначе
				hostname[0] := 0X;
				i := 0; нцПока buf[i] # 0X делай увел(i) кц;
				j := 0;
				нцДо
					нцПока (i # 0) и (buf[i] # ".") делай умень(i) кц;
					k := i;
					если buf[i] = "." то увел(i) всё;
					нцПока (buf[i] # ".") и (buf[i] # 0X) делай name[j] := buf[i]; увел(j); увел(i) кц;
					name[j] := "."; увел(j);
					i := k-1
				кцПри i < 0;
				name[j] := 0X;
				Domain(name, ArpaDomain, истина);
				если Trace то ЛогЯдра.пСимв8(" "); ЛогЯдра.пСтроку8(name) всё;
				QueryDNS(TypePTR, buf, name, hostname, adr, timeout, res);
				если (res = Ok) или (res = BadName) то CacheAdd(hostname, adr, "", timeout) всё
			всё;
			если (res = Ok) и (hostname[0] = 0X) то res := BadName всё;
			если Trace то ЛогЯдра.пСтроку8(" res="); ЛогЯдра.пЦел64(res, 1); ЛогЯдра.пВК_ПС всё
		всё;
	иначе
		hostname[0] := 0X;
		res := BadName;
	всё;
	если res # Ok то
		IP.AdrToStr(adr, hostname)
	всё;
кон HostByNumber;

нач
	(* Get domain name from configuration. *)
	ЭВМ.ДайЗначениеКлючаКонфигурацииЯОС("Domain", domain);
	id := 0;
	нов(serverlist);
	нов(cache);
	cache.next := НУЛЬ;
	lastCleanup := Kernel.GetTicks();
кон DNS.

(*
History:
02.11.2003	mvt	Adapted for new interfaces of Network, IP and UDP.
03.11.2003	mvt	Added support for MX queries (mail exchange).
21.11.2003	mvt	Support for concurrent queries.
02.05.2005	eb	Type AAAA supported
*)

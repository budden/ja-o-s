модуль WMFontCCGConverter;	(** AUTHOR "TF"; PURPOSE "CCG font support (CCG fonts by eForth Technology Corp)"; *)
(* 2003.01.29 - adapted to new single.fnt format *)

использует
	ЛогЯдра, Modules, Потоки, WMRectangles, Files, UCS32, UTF8Strings, WMGraphics, WMWindowManager, WMGrids,
	WMBitmapFont, WMComponents, Standard := WMStandardComponents, Editor := WMEditors,
	Classes := TFClasses, WMGraphicUtilities, Strings;

конст
	MaxStrokes = 128;
	MaxGlyphRefs = 64;
	MaxSplineSeg = 16;
	CMDStrokeMove = 0;
	CMDStrokeLine = 1;
	CMDStrokeSpline = 2;
	MaxRangeSize = 256;

тип
	TestWindow = окласс(WMComponents.FormWindow)
	перем mainPanel, toolbar, right, paintBox : Standard.Panel;
		l1 : Standard.Label;
		startEdit : Editor.Editor;
		grid : WMGrids.GenericGrid;
		selectedGlyph : Glyph;
		font : RawCCGFont;
		colWidths : WMGrids.Spacings;
		scaler : Standard.Scrollbar;
		scale : размерМЗ;

		проц &New*(f : RawCCGFont);
		нач
			SetTitle(WMWindowManager.NewString("CCG font explorer"));
			font := f;
			нов(mainPanel); mainPanel.bounds.SetExtents(480, 400);
			mainPanel.fillColor.Set(WMGraphics.RGBAToColor(200, 200, 200, 255));

			(* toolbar *)
			нов(toolbar); toolbar.bounds.SetHeight(40); toolbar.alignment.Set(WMComponents.AlignTop);
			mainPanel.AddContent(toolbar);

			нов(grid); grid.alignment.Set(WMComponents.AlignLeft); grid.bounds.SetWidth(201); mainPanel.AddContent(grid);
			нов(colWidths, 3); colWidths[0] := 80; colWidths[1] := 50; colWidths[2] := 50;
			grid.SetColSpacings(colWidths);
			grid.nofCols.Set(3);
			grid.SetDrawCellProc(DrawCell);
			grid.nofRows.Set(font.glyphList.GetCount()(цел32));
			grid.defaultRowHeight.Set(50);
			grid.defaultColWidth.Set(50);
			grid.onSelect.Add(GlyphSelected);

			нов(l1); l1.bounds.SetExtents(150, 20); l1.SetCaption("Range Start:"); l1.alignment.Set(WMComponents.AlignLeft);
			нов(startEdit); startEdit.bounds.SetExtents(200, 20); startEdit.SetAsString("1100"); startEdit.alignment.Set(WMComponents.AlignLeft);
			startEdit.multiLine.Set(ложь);
			startEdit.tv.textAlignV.Set(WMGraphics.AlignCenter);
			toolbar.AddContent(l1); toolbar.AddContent(startEdit);

			нов(right); right.alignment.Set(WMComponents.AlignClient); right.fillColor.Set(0); mainPanel.AddContent(right);
			(* add paintbox to draw glyph in full res *)
			нов(paintBox); paintBox.bounds.SetExtents(256, 256);
			paintBox.fillColor.Set(WMGraphics.RGBAToColor(255, 255, 255, 255));
			right.AddContent(paintBox);
			paintBox.SetExtDrawHandler(PaintBoxPaint);

			нов(scaler); scaler.alignment.Set(WMComponents.AlignRight); right.AddContent(scaler);
			scaler.max.Set(256); scaler.pos.Set(256);
			scaler.onPositionChanged.Add(Rescale); scale := 256;

			Init(mainPanel.bounds.GetWidth(), mainPanel.bounds.GetHeight(), ложь);
			manager := WMWindowManager.GetDefaultManager();
			manager.Add(200, 200, сам, {WMWindowManager.FlagFrame});
			SetContent(mainPanel)
		кон New;

		проц Rescale(sender, data : динамическиТипизированныйУкль);
		нач
			scale := scaler.pos.Get();
			paintBox.Invalidate
		кон Rescale;

		проц GlyphSelected(sender, data : динамическиТипизированныйУкль);
		перем sc, sr, ec, er : размерМЗ; ptr : динамическиТипизированныйУкль;
		нач
			grid.GetSelection(sc, sr, ec, er);
			если font # НУЛЬ то
				font.glyphList.Lock;
				если (sr >= 0) и (sr < font.glyphList.GetCount()) то
					ptr := font.glyphList.GetItem(sr); selectedGlyph := ptr(Glyph);
					paintBox.Invalidate
				всё;
				font.glyphList.Unlock
			всё
		кон GlyphSelected;

		проц PaintBoxPaint(canvas : WMGraphics.Canvas);
		нач
			если selectedGlyph # НУЛЬ то RenderGlyphReal(canvas, selectedGlyph, 0, 0, scale, scale, 0, истина) всё
		кон PaintBoxPaint;

		проц DrawCell(canvas : WMGraphics.Canvas; w, h : размерМЗ; state : мнвоНаБитахМЗ; x, y : размерМЗ);
		перем color : WMGraphics.Color; g : Glyph; ptr : динамическиТипизированныйУкль; str : массив 32 из симв8;
		нач
			color := WMGraphics.RGBAToColor(255, 255, 255, 255);
			если state * {WMGrids.CellFixed, WMGrids.CellSelected} = {WMGrids.CellFixed, WMGrids.CellSelected} то
				color := WMGraphics.RGBAToColor(0, 0, 255, 255)
			аесли WMGrids.CellFixed в state то color := WMGraphics.RGBAToColor(196, 196, 196, 255)
			аесли WMGrids.CellSelected в state то color := WMGraphics.RGBAToColor(196, 196, 255, 255)
			всё;

			если WMGrids.CellHighlighted в state то color := WMGraphics.RGBAToColor(255, 255, 0, 255) всё;
			если WMGrids.CellSelected в state то canvas.SetColor(WMGraphics.RGBAToColor(255, 255, 255, 255))
			иначе canvas.SetColor(WMGraphics.RGBAToColor(0, 0, 0, 255))
			всё;
			canvas.Fill(WMRectangles.MakeRect(0, 0, w, h), color, WMGraphics.ModeCopy);
			если (WMGrids.CellFocused в state) и ~(WMGrids.CellHighlighted в state) то
				WMGraphicUtilities.DrawRect(canvas, WMRectangles.MakeRect(0, 0, w, h), WMGraphics.RGBAToColor(0, 0, 0, 196),
					WMGraphics.ModeSrcOverDst);
			всё;
			если font # НУЛЬ то
				font.glyphList.Lock;
				если (y >= 0) и (y < font.glyphList.GetCount()) то
					ptr := font.glyphList.GetItem(y); g := ptr(Glyph);
					если x = 0 то Strings.IntToHexStr(g.ucs, 5, str);
						WMGraphics.DrawStringInRect(canvas,
							WMRectangles.MakeRect(0, 0, w, h), ложь,
							WMGraphics.AlignCenter, WMGraphics.AlignCenter, str)
					аесли x = 1 то WMBitmapFont.bimbofont.RenderChar(canvas, 0, h - 5, симв32ИзКода(g.ucs));
					аесли x = 2 то RenderGlyphReal(canvas, g, 0, 0, h, h, 0, ложь)
					всё
				всё;
				font.glyphList.Unlock;
			всё;
		кон DrawCell;

(*		PROCEDURE RenderGlyph(canvas : WMGraphics.Canvas; glyph : Glyph; x, y, w, h, level : SIGNED32; trace : BOOLEAN);
		VAR i, tx, ty, cx, cy, dx, dy : SIGNED32; ctrl : BOOLEAN; g : Glyph; r, bb : WMRectangles.Rectangle;
			dtx, dty, dtw, dth : SIGNED32;
		BEGIN
			IF level > 0 THEN (* then we must fit the bounding box in x, y, w, h *)
				bb := CalcBB(glyph);
				dx := (bb.r - bb.l); IF dx <= 0 THEN dx := 256 END;
				dy := (bb.b - bb.t); IF dy <= 0 THEN dy := 256 END;
				x := x - (bb.l * w DIV 256) * 256 DIV dx;
				y := y - (bb.t * h DIV 256) * 256 DIV dy;
				w := w * 256 DIV dx;
				h := h * 256 DIV dy
			 END;
			IF glyph.nofSubComponents > 0 THEN
				FOR i := 0 TO glyph.nofSubComponents - 1 DO
					g := font.FindGlyphSubComponent(glyph.subComponents[i]);
					IF g # NIL THEN
						r := CalcBB(glyph);
						dtx := x + glyph.subComponents[i].x * w DIV 256;
						dty := y + glyph.subComponents[i].y * h DIV 256;
						dtw := glyph.subComponents[i].w * w DIV 256;
						dth := glyph.subComponents[i].h * h DIV 256;
						IF trace THEN
							r := WMRectangles.MakeRect(dtx, dty, dtx + dtw, dty + dth);
							canvas.Fill(r, WMGraphics.RGBAToColor(0, 0, 255, 16), WMGraphics.ModeSrcOverDst)
						END;
						RenderGlyph(canvas, g, dtx, dty, dtw, dth, level + 1, trace)
					END
				END
			END;
(*			IF level > 0 THEN (* then we must fit the bounding box in x, y, w, h *)
				bb := CalcBB(glyph);
				dx := (bb.r - bb.l); IF dx <= 0 THEN dx := 256 END;
				dy := (bb.b - bb.t); IF dy <= 0 THEN dy := 256 END;
				x := x - (bb.l * w DIV 256) * 256 DIV dx;
				y := y - (bb.t * h DIV 256) * 256 DIV dy;
				w := w * 256 DIV dx;
				h := h * 256 DIV dy
			 END; *)
			ctrl := FALSE;
			IF TraceGlyphs IN Trace THEN
				KernelLog.String("============"); KernelLog.Ln;
				KernelLog.String("UCS : "); KernelLog.Hex(glyph.ucs, 8); KernelLog.Ln;
				KernelLog.String("NofStrokes : "); KernelLog.Int(glyph.nofStrokes, 5); KernelLog.Ln
			END;
			FOR i := 0 TO glyph.nofStrokes - 1 DO
				IF TraceGlyphs IN Trace THEN
					KernelLog.Int(glyph.strokes[i].cmd, 3); KernelLog.String(" : "); KernelLog.Int(glyph.strokes[i].x, 5);
					KernelLog.String(", "); KernelLog.Int(glyph.strokes[i].y, 5);
					KernelLog.Ln
				END;
				IF glyph.strokes[i].cmd = CMDStrokeMove THEN tx := glyph.strokes[i].x; ty := glyph.strokes[i].y; ctrl := FALSE;
					IF TraceGlyphs IN Trace THEN
	 					KernelLog.String(" --> MoveTo"); KernelLog.Int(glyph.strokes[i].x, 5); KernelLog.String(", "); KernelLog.Int(glyph.strokes[i].y, 5);
						KernelLog.Ln
					END
				ELSIF glyph.strokes[i].cmd = CMDStrokeSpline THEN cx := glyph.strokes[i].x; cy := glyph.strokes[i].y; ctrl := TRUE;
					IF TraceGlyphs IN Trace THEN
						KernelLog.String(" --> Spline"); KernelLog.Int(glyph.strokes[i].x, 5); KernelLog.String(", "); KernelLog.Int(glyph.strokes[i].y, 5);
						KernelLog.Ln
					END
				ELSIF glyph.strokes[i].cmd = CMDStrokeLine THEN
					IF TraceGlyphs IN Trace THEN
						KernelLog.String(" --> Execute"); KernelLog.Int(glyph.strokes[i].x, 5); KernelLog.String(", "); KernelLog.Int(glyph.strokes[i].y, 5);
						KernelLog.Ln;
						IF i = 0 THEN KernelLog.String("Strange... no move to"); KernelLog.Ln END;
					END;
					IF i > 0 THEN
						IF ctrl THEN Spline(canvas, tx, ty, cx, cy, glyph.strokes[i].x, glyph.strokes[i].y, x, y, w, h, 0FFH, WMGraphics.ModeCopy)
						ELSE
							canvas.Line(x + (tx * w) DIV 256, y + (ty * h) DIV 256,
									x + (glyph.strokes[i].x * w) DIV 256, y + (glyph.strokes[i].y* h) DIV 256, 0FFH, WMGraphics.ModeCopy)
						END
					END;
					tx := glyph.strokes[i].x; ty := glyph.strokes[i].y;
					ctrl := FALSE;
				END
			END;
		END RenderGlyph; *)

		(* floating point version *)
		проц RenderGlyphReal(canvas : WMGraphics.Canvas; glyph : Glyph; x, y, w, h : вещ32; level : цел32; trace : булево);
		перем i : цел32;  tx, ty, cx, cy, dx, dy : вещ32; ctrl : булево; g : Glyph; r, bb : WMRectangles.Rectangle;
			dtx, dty, dtw, dth : вещ32;
		нач
			если level > 0 то (* then we must fit the bounding box in x, y, w, h *)
				bb := CalcBB(glyph);
				dx := (bb.r - bb.l); если dx <= 0 то dx := 256 всё;
				dy := (bb.b - bb.t); если dy <= 0 то dy := 256 всё;
				x := x - (bb.l * w / 256) * 256 / dx;
				y := y - (bb.t * h / 256) * 256 / dy;
				w := w * 256 / dx;
				h := h * 256 / dy
			 всё;
			если glyph.nofSubComponents > 0 то
				нцДля i := 0 до glyph.nofSubComponents - 1 делай
					g := font.FindGlyphSubComponent(glyph.subComponents[i]);
					если g # НУЛЬ то
						r := CalcBB(glyph);
						dtx := x + glyph.subComponents[i].x * w / 256;
						dty := y + glyph.subComponents[i].y * h / 256;
						dtw := glyph.subComponents[i].w * w / 256;
						dth := glyph.subComponents[i].h * h / 256;
						если trace то
							r := WMRectangles.MakeRect(округлиВниз(dtx), округлиВниз(dty), округлиВниз(dtx + dtw), округлиВниз(dty + dth));
							canvas.Fill(r, WMGraphics.RGBAToColor(0, 0, 255, 16), WMGraphics.ModeSrcOverDst)
						всё;
						RenderGlyphReal(canvas, g, dtx, dty, dtw, dth, level + 1, trace)
					всё
				кц
			всё;
			ctrl := ложь;
			нцДля i := 0 до glyph.nofStrokes - 1 делай
				если glyph.strokes[i].cmd = CMDStrokeMove то tx := glyph.strokes[i].x; ty := glyph.strokes[i].y; ctrl := ложь;
				аесли glyph.strokes[i].cmd = CMDStrokeSpline то cx := glyph.strokes[i].x; cy := glyph.strokes[i].y; ctrl := истина;
				аесли glyph.strokes[i].cmd = CMDStrokeLine то
					если i > 0 то
						если ctrl то SplineReal(canvas, tx, ty, cx, cy, glyph.strokes[i].x, glyph.strokes[i].y, x, y, w, h, 0FFH, WMGraphics.ModeCopy)
						иначе
							canvas.Line(округлиВниз(x + (tx * w) / 256), округлиВниз(y + (ty * h) / 256),
									округлиВниз(x + (glyph.strokes[i].x * w) / 256), округлиВниз(y + (glyph.strokes[i].y* h) / 256), 0FFH, WMGraphics.ModeCopy)
						всё
					всё;
					tx := glyph.strokes[i].x; ty := glyph.strokes[i].y;
					ctrl := ложь;
				всё
			кц;
		кон RenderGlyphReal;

		проц CalcBB(glyph : Glyph) : WMRectangles.Rectangle;
		перем result, t : WMRectangles.Rectangle; i : цел32;
		нач
			result := WMRectangles.MakeRect(256, 256, 0, 0);
			если glyph.nofSubComponents > 0 то
				нцДля i := 0 до glyph.nofSubComponents - 1 делай
					t := WMRectangles.MakeRect(glyph.subComponents[i].x, glyph.subComponents[i].y,
					 glyph.subComponents[i].x + glyph.subComponents[i].w, glyph.subComponents[i].y + glyph.subComponents[i].h);
					WMRectangles.ExtendRect(result, t)
				кц
			всё;
			нцДля i := 0 до glyph.nofStrokes - 1 делай
				t := WMRectangles.MakeRect(glyph.strokes[i].x, glyph.strokes[i].y, glyph.strokes[i].x, glyph.strokes[i].y);
				WMRectangles.ExtendRect(result, t)
			кц;
			возврат result
		кон CalcBB;

		проц {перекрыта}Close*;
		нач
			Close^;
			testWindow := НУЛЬ
		кон Close;
	кон TestWindow;

	StrokeElement = запись
		cmd : цел32;
		x, y : цел32;
	кон;

	GlyphRef = запись
		 x, y, w, h : цел32;
		 refucs : симв32; refvariant : цел32;
		 refPtr : Glyph;
	кон;

	StrokeArray = укль на массив из StrokeElement;
	GlyphRefArray = укль на массив из GlyphRef;

	GlyphRange = запись
		firstCode, lastCode  : цел32;
		filePos : цел32;
		glyphs : укль на массив из Glyph;
	кон;

	RangeArray = укль на массив из GlyphRange;

	Glyph = укль на запись
		ucs, variant : цел32;
		nofStrokes, nofSubComponents : цел32;
		strokes : StrokeArray;
		subComponents : GlyphRefArray;
		nextVariant : Glyph;
	кон;
	GlyphArray = укль на массив из Glyph;

	RawCCGFont = окласс
	перем workStrokes : StrokeArray;
		workGlyphRef : GlyphRefArray;
		nofStrokes, nofGlyphRefs : цел32;
		glyphList : Classes.List;
(* BEGIN variables for native font *)
		glyphRanges : RangeArray;
		fontFile : Files.File;
(* END variables for native font *)

		проц &New*;
		нач
			нов(workStrokes, MaxStrokes);
			нов(workGlyphRef, MaxGlyphRefs);
			нов(glyphList);
		кон New;

(* BEGIN optimized routines for native font *)
		(* find the range where a glyph is inside *)
		проц FindGlyphRange(code : цел32; перем glyphRangeIndex : размерМЗ) : булево;
		перем a, b, m : размерМЗ;
		нач
			glyphRangeIndex := 0;
			a := 0; b := длинаМассива(glyphRanges) - 1;
			нцПока (a < b) делай m := (a + b) DIV 2;
				если glyphRanges[m].lastCode < code то a := m + 1
				иначе b := m
				всё
			кц;
			если (glyphRanges[a].firstCode <= code) и (glyphRanges[a].lastCode >= code) то
				glyphRangeIndex := a; возврат истина
			иначе возврат ложь
			всё
		кон FindGlyphRange;

		проц WritePackedGlyph(w : Потоки.Писарь; g : Glyph);
		перем i : цел32; врем: цел32;
		нач
			нцДо
				(* has more variants *)
				если g.nextVariant = НУЛЬ то w.пСимв8(0X) иначе w.пСимв8(1X) всё;
				(* variant *)
				w.пСимв8(симв8ИзКода(g.variant));
				(* sanity check *)
				w.пЦел32_сп(g.ucs);

				(* number of stroke commands *)
				утв(g.nofStrokes <= 255);
				w.пСимв8(симв8ИзКода(g.nofStrokes));
				нцДля i := 0 до g.nofStrokes - 1 делай
					w.пСимв8(симв8ИзКода(g.strokes[i].cmd));
					w.пСимв8(симв8ИзКода(g.strokes[i].x));
					w.пСимв8(симв8ИзКода(g.strokes[i].y))
				кц;

				(* number of sub components *)
				утв(g.nofSubComponents <= 255);
				w.пСимв8(симв8ИзКода(g.nofSubComponents));
				нцДля i := 0 до g.nofSubComponents - 1 делай
					w.пЦел32_сп(врем);
					g.subComponents[i].refucs := симв32ИзКода(врем);
					w.пСимв8(симв8ИзКода(g.subComponents[i].refvariant));
					w.пСимв8(симв8ИзКода(g.subComponents[i].x));
					w.пСимв8(симв8ИзКода(g.subComponents[i].y));
					w.пСимв8(симв8ИзКода(g.subComponents[i].w));
					w.пСимв8(симв8ИзКода(g.subComponents[i].h))
				кц;
				g := g.nextVariant
			кцПри g = НУЛЬ;
		кон WritePackedGlyph;

		проц ReadPackedGlyph(r : Потоки.Чтец; перем glyph : Glyph);
		перем g : Glyph;
			hasMoreVariants : булево;
			i : цел32;
		нач
			нов(g); glyph := g;
			нцДо
				(* has more variants *)
				hasMoreVariants := r.чИДайСимв8() = 1X;
				(* variant *)
				g.variant := кодСимв8(r.чИДайСимв8());
				(* sanity check *)
				g.ucs := r.чЦел32_сп();
			(*	KernelLog.String("Found:"); KernelLog.Hex(g.ucs, 8); KernelLog.Ln; *)
				(* number of stroke commands *)
				g.nofStrokes := кодСимв8(r.чИДайСимв8());
				нов(g.strokes, g.nofStrokes);
				нцДля i := 0 до g.nofStrokes - 1 делай
					g.strokes[i].cmd := кодСимв8(r.чИДайСимв8());
					g.strokes[i].x := кодСимв8(r.чИДайСимв8());
					g.strokes[i].y := кодСимв8(r.чИДайСимв8())
				кц;
				(* number of sub components *)
				g.nofSubComponents := кодСимв8(r.чИДайСимв8());
				нов(g.subComponents, g.nofSubComponents);
				нцДля i := 0 до g.nofSubComponents - 1 делай
					g.subComponents[i].refucs := симв32ИзКода(r.чЦел32_сп());
					g.subComponents[i].refvariant := кодСимв8(r.чИДайСимв8());
					g.subComponents[i].x := кодСимв8(r.чИДайСимв8());
					g.subComponents[i].y := кодСимв8(r.чИДайСимв8());
					g.subComponents[i].w := кодСимв8(r.чИДайСимв8());
					g.subComponents[i].h := кодСимв8(r.чИДайСимв8())
				кц;
				если hasMoreVariants то нов(g.nextVariant); g := g.nextVariant всё
			кцПри ~hasMoreVariants;

		кон ReadPackedGlyph;

		проц StoreRange(w : Потоки.Писарь; перем range : GlyphRange);
		перем i : цел32;
		нач
			(* sanity check *)
			ЛогЯдра.пСтроку8("Store range: ");
			ЛогЯдра.п16ричное(range.firstCode, 8); ЛогЯдра.пСтроку8(".."); ЛогЯдра.п16ричное(range.lastCode, 8);
			ЛогЯдра.пВК_ПС;
			w.п2МладшихБайта_сп(range.lastCode - range.firstCode);
			нцДля i := 0 до range.lastCode - range.firstCode делай WritePackedGlyph(w, range.glyphs[i]) кц;
		кон StoreRange;

		проц LoadRange(f : Files.File; rangeIndex : размерМЗ);
		перем r : Files.Reader;
			size, i : цел32;
			range : GlyphRange; (* because of too complex expression otherwise ;-) *)
		нач
			range := glyphRanges[rangeIndex];
			ЛогЯдра.пСтроку8("Loading range "); ЛогЯдра.п16ричное(range.firstCode, 8);
			ЛогЯдра.пСтроку8(".."); ЛогЯдра.п16ричное(range.lastCode, 8);
			ЛогЯдра.пВК_ПС;
			(* glyphRanges[rangeIndex].glyphs in the new statement may not be replaced with range! *)
			нов(glyphRanges[rangeIndex].glyphs, range.lastCode - range.firstCode + 1);
			нов(r, f, range.filePos);
			(* sanity check *)
			size := r.чЦел16_сп(); утв(size = glyphRanges[rangeIndex].lastCode - glyphRanges[rangeIndex].firstCode);
				(* glyphRanges[rangeIndex].glyphs in the following loop may not be replaced with range! *)
			нцДля i := 0 до size делай ReadPackedGlyph(r, glyphRanges[rangeIndex].glyphs[i]) кц
		кон LoadRange;

		проц GetGlyph(ucs, variant : цел32) : Glyph;
		перем rangeIndex : размерМЗ; glyph : Glyph;
		нач
			если FindGlyphRange(ucs, rangeIndex) то
				если glyphRanges[rangeIndex].glyphs = НУЛЬ то LoadRange(fontFile, rangeIndex) всё;
				если glyphRanges[rangeIndex].glyphs = НУЛЬ то возврат НУЛЬ всё;
				glyph := glyphRanges[rangeIndex].glyphs[ucs - glyphRanges[rangeIndex].firstCode];
				нцПока (glyph # НУЛЬ) и (glyph.variant # variant) делай glyph := glyph.nextVariant кц;
				если glyph # НУЛЬ то
					если (glyph.ucs # ucs) то ЛогЯдра.пСтроку8("Not correctly loaded : "); ЛогЯдра.п16ричное(glyph.ucs, 8);
						ЛогЯдра.пСтроку8(" instead of "); ЛогЯдра.п16ричное(ucs, 8); ЛогЯдра.пВК_ПС;
					всё;
					утв((glyph.ucs = ucs) и (glyph.variant = variant))
				всё;
				возврат glyph
			иначе
				ЛогЯдра.пСтроку8("Range not found"); ЛогЯдра.пВК_ПС;
				возврат НУЛЬ
			всё
		кон GetGlyph;

		проц Save(конст fileName : массив из симв8);
		перем f : Files.File;
			w : Files.Writer;
			i : размерМЗ;
		нач
			f := Files.New(fileName);
			Files.Register(f);
			Files.OpenWriter(w, f, 0);
			(* number of ranges *)
			w.пЦел32_сп(длинаМассива(glyphRanges)(цел32));
			(* reserve space for ranges *)
			нцДля i := 0 до длинаМассива(glyphRanges) - 1 делай
				w.пЦел32_сп(0); w.пЦел32_сп(0); w.пЦел32_сп(0)
			кц;
			(* write glyphs *)
			нцДля i := 0 до длинаМассива(glyphRanges) - 1 делай
				ЛогЯдра.пСтроку8("Writing range "); ЛогЯдра.пЦел64(i, 4); ЛогЯдра.пСтроку8(" of "); ЛогЯдра.пЦел64(длинаМассива(glyphRanges), 4);
				ЛогЯдра.пВК_ПС;
				w.ПротолкниБуферВПоток();
				glyphRanges[i].filePos := w.отправленоБайт(цел32);
				StoreRange(w, glyphRanges[i]);
			кц;
			w.ПротолкниБуферВПоток();
			(* fixup ranges *)
			Files.OpenWriter(w, f, 4);
			нцДля i := 0 до длинаМассива(glyphRanges) - 1 делай
				w.пЦел32_сп(glyphRanges[i].firstCode); w.пЦел32_сп(glyphRanges[i].lastCode); w.пЦел32_сп(glyphRanges[i].filePos)
			кц;
			w.ПротолкниБуферВПоток
		кон Save;

		проц Load(конст fileName : массив из симв8);
		перем
			r : Files.Reader;
			i, nofRanges : размерМЗ;
			ptr :динамическиТипизированныйУкль; glyph, tg : Glyph;
		нач
			fontFile := Files.Old(fileName);
			Files.OpenReader(r, fontFile, 0);
			nofRanges := r.чЦел32_сп();
			нов(glyphRanges, nofRanges);
			ЛогЯдра.пСтроку8("Loaded ranges..."); ЛогЯдра.пВК_ПС;
			нцДля i := 0 до nofRanges - 1 делай
				glyphRanges[i].firstCode := r.чЦел32_сп(); glyphRanges[i].lastCode := r.чЦел32_сп(); glyphRanges[i].filePos := r.чЦел32_сп()
				; DumpRange(glyphRanges[i])
			кц;
			ЛогЯдра.пСтроку8("Ranges Loaded."); ЛогЯдра.пВК_ПС;
			glyphList.Lock;
			нцДля i := 0 до glyphList.GetCount() - 1 делай
				ptr := glyphList.GetItem(i); glyph := ptr(Glyph);
				tg := GetGlyph(glyph.ucs, glyph.variant);
				если tg = НУЛЬ то ЛогЯдра.пСтроку8("Not loaded : "); ЛогЯдра.п16ричное(glyph.ucs, 8); ЛогЯдра.пЦел64(glyph.variant, 5); ЛогЯдра.пВК_ПС
				всё
			кц;
			glyphList.Unlock;
			ЛогЯдра.пСтроку8("done."); ЛогЯдра.пВК_ПС
		кон Load;

		проц DumpRange(перем r : GlyphRange);
		перем i : цел32; tg : Glyph;
		нач
			ЛогЯдра.пСтроку8("Range: "); ЛогЯдра.п16ричное(r.firstCode, 0); ЛогЯдра.пСтроку8(" .. "); ЛогЯдра.п16ричное(r.lastCode, 0); ЛогЯдра.пВК_ПС;
			если r.glyphs # НУЛЬ то
				нцДля i := r.firstCode до r.lastCode делай
					утв(r.glyphs[i - r.firstCode].ucs = i);
					если r.glyphs[i - r.firstCode].nextVariant # НУЛЬ то
						ЛогЯдра.п16ричное(i, 0); ЛогЯдра.пСтроку8(" has variants : ");
						tg := r.glyphs[i - r.firstCode].nextVariant;
						нцПока tg # НУЛЬ делай ЛогЯдра.п16ричное(i, 0); ЛогЯдра.пСтроку8(", "); tg := tg.nextVariant кц;
						ЛогЯдра.пВК_ПС
					всё
				кц
			иначе ЛогЯдра.пСтроку8("Glpyhs not loaded."); ЛогЯдра.пВК_ПС
			всё
		кон DumpRange;

		проц CreateRanges;
		перем i, j, firstIndex: размерМЗ;
			nofRanges, code, lastCode, rangeStart, range, rangeSize : цел32;
			glyph, tg : Glyph; ptr : динамическиТипизированныйУкль;
			bimboSortArray : GlyphArray;

			проц FillRange(перем range : GlyphRange; startIndex, endIndex : размерМЗ);
			перем glyph, tg : Glyph; i : размерМЗ;
			нач
				range.firstCode := bimboSortArray[startIndex].ucs;
				range.lastCode := bimboSortArray[endIndex].ucs;

				нов(range.glyphs, range.lastCode - range.firstCode + 1);
				нцДля i := startIndex до endIndex делай
					glyph := bimboSortArray[i];
					если range.glyphs[glyph.ucs - range.firstCode] = НУЛЬ то
						range.glyphs[glyph.ucs - range.firstCode] := glyph
					иначе
						tg := range.glyphs[glyph.ucs - range.firstCode];
						нцПока tg.nextVariant # НУЛЬ делай tg := tg.nextVariant кц;
						tg.nextVariant := glyph
					всё;
				кц;
			кон FillRange;

(*
			PROCEDURE CheckAllSorted;
			VAR ptr : ANY; glyph : Glyph; i : SIZE;
			BEGIN
				KernelLog.String("Searching all glyphs in the sorted array...");
				FOR i := 0 TO glyphList.GetCount() - 1 DO
					ptr := glyphList.GetItem(i); glyph := ptr(Glyph);
					found := FALSE; j := 0;
					WHILE ~found & (j < glyphList.GetCount()) DO
						found := glyph = bimboSortArray[j];
						INC(j)
					END;
					IF ~found THEN
						KernelLog.String("Not found:"); KernelLog.Hex(glyph.ucs, 5); KernelLog.String("v"); KernelLog.Int(glyph.variant, 5);
						KernelLog.Ln;
						HALT(123456);
					END;
				END;
				KernelLog.String("done."); KernelLog.Ln;
			END CheckAllSorted;
*)

			проц CheckAllInRanges;
			перем ptr : динамическиТипизированныйУкль; glyph, tg : Glyph; i : размерМЗ;
			нач
				ЛогЯдра.пСтроку8("Searching all glyphs...");
				нцДля i := 0 до glyphList.GetCount() - 1 делай
					ptr := glyphList.GetItem(i); glyph := ptr(Glyph);
					tg := GetGlyph(glyph.ucs, glyph.variant);
					если tg = НУЛЬ то
						ЛогЯдра.пСтроку8("Not found:"); ЛогЯдра.п16ричное(glyph.ucs, 5); ЛогЯдра.пСтроку8("v"); ЛогЯдра.пЦел64(glyph.variant, 5);
						СТОП(8888)
					аесли tg # glyph то
						ЛогЯдра.пСтроку8("Multiple defined:"); ЛогЯдра.п16ричное(glyph.ucs, 5); ЛогЯдра.пСтроку8("v"); ЛогЯдра.пЦел64(glyph.variant, 5);
						СТОП(8888)
					всё;
				кц;
				ЛогЯдра.пСтроку8("done."); ЛогЯдра.пВК_ПС;
			кон CheckAllInRanges;

		нач
			ЛогЯдра.пСтроку8("Creating ranges..."); ЛогЯдра.пВК_ПС;
			glyphList.Lock;
			(* Sort *)
			ЛогЯдра.пСтроку8("Sorting...");
			нов(bimboSortArray, glyphList.GetCount());
			ptr := glyphList.GetItem(0); bimboSortArray[0] := ptr(Glyph);
			нцДля i := 1 до glyphList.GetCount() - 1 делай
				ptr := glyphList.GetItem(i); tg := ptr(Glyph);
				(* insertion sort... (run once software ;-) ) *)
				j := i;
				нцПока (j >= 1) и ((tg.ucs < bimboSortArray[j - 1].ucs) или
					((tg.ucs = bimboSortArray[j - 1].ucs) и (tg.variant < bimboSortArray[j - 1].variant))) делай
					bimboSortArray[j] := bimboSortArray[j - 1];
					умень(j);
				кц;
				bimboSortArray[j] := tg
			кц;
			ЛогЯдра.пСтроку8("done."); ЛогЯдра.пВК_ПС;

			(* Sanity check *)
			(* CheckAllSorted; *)

			(* identify ranges *)
			lastCode := -1; nofRanges := 0; rangeSize := 0;
			нцДля i := 0 до glyphList.GetCount() - 1 делай
				glyph := bimboSortArray[i]; code := glyph.ucs;
				утв(lastCode  <= code, 12345);
						если (code = lastCode + 1) то увел(rangeSize) всё;
				если  (lastCode > -1) и
					((code # lastCode + 1) и (code # lastCode) или (code = lastCode + 1) и (rangeSize > MaxRangeSize)) то
						увел(nofRanges); rangeSize := 0;
						lastCode := -1;
				всё;lastCode := code
			кц;
			увел(nofRanges);

			(* create ranges *)
			нов(glyphRanges, nofRanges);
			ЛогЯдра.пСтроку8("Generated "); ЛогЯдра.пЦел64(nofRanges, 5); ЛогЯдра.пСтроку8(" ranges."); ЛогЯдра.пВК_ПС;

			(* fill ranges *)
			ЛогЯдра.пСтроку8("Filling ranges... ");
			range := 0; lastCode := -1; rangeSize := 0; firstIndex := 0;
			нцДля i := 0 до glyphList.GetCount() - 1 делай
				glyph := bimboSortArray[i]; code := glyph.ucs;
				если lastCode = -1 то rangeStart := code; всё;
						если (code = lastCode + 1) то увел(rangeSize) всё;
				если  (lastCode > -1) и
					((code # lastCode + 1) и (code # lastCode) или (code = lastCode + 1) и (rangeSize > MaxRangeSize)) то
					glyphRanges[range].firstCode := rangeStart;
					glyphRanges[range].lastCode := lastCode;

					(* sanity check *)
					нцДля j := firstIndex до i - 1 делай
						если (bimboSortArray[j].ucs < glyphRanges[range].firstCode) или
							(bimboSortArray[j].ucs > glyphRanges[range].lastCode) то
							ЛогЯдра.пСтроку8("Stupid!!!"); ЛогЯдра.пЦел64(bimboSortArray[j].ucs, 5); ЛогЯдра.пВК_ПС
						всё;
					кц;

					FillRange(glyphRanges[range], firstIndex, i - 1);
					DumpRange(glyphRanges[range]);
					увел(range);
					firstIndex := i;
					rangeSize := 0; rangeStart := code
				всё; lastCode := code
			кц;
			FillRange(glyphRanges[range], firstIndex,  glyphList.GetCount() - 1);
			DumpRange(glyphRanges[range]);
			ЛогЯдра.пСтроку8("done."); ЛогЯдра.пВК_ПС;

			(* Sanity check *)
			CheckAllInRanges;
			glyphList.Unlock;
			ЛогЯдра.пСтроку8("Done."); ЛогЯдра.пВК_ПС;
		кон CreateRanges;

(* END optimized routines for native font *)

		проц FindGlyph(ucs, variant : цел32) : Glyph;
		перем g, tg : Glyph; i : размерМЗ; ptr : динамическиТипизированныйУкль;
		нач
			g := НУЛЬ;
			glyphList.Lock;
			i := 0; нцПока (i < glyphList.GetCount()) и (g = НУЛЬ) делай
				ptr := glyphList.GetItem(i);
				tg := ptr(Glyph);
				если (tg.ucs = ucs) и (tg.variant = variant) то g := tg всё;
				увел(i)
			кц;
			glyphList.Unlock;
			возврат g
		кон FindGlyph;

		проц FindGlyphSubComponent(перем ref : GlyphRef) : Glyph;
		нач
			если ref.refPtr # НУЛЬ то возврат ref.refPtr всё;
			ref.refPtr := FindGlyph(кодСимв32(ref.refucs), ref.refvariant);
			возврат ref.refPtr
		кон FindGlyphSubComponent;

		проц GetNCharHex(r : Потоки.Чтец; nofChars : цел32) : цел32;
		перем c : симв8; i, res : цел32;
		нач
			res := 0;
			нцДля i := 0 до nofChars - 1 делай
				c := r.чИДайСимв8();
				если (c >= "0") и (c <= "9") то res := res * 16 + (кодСимв8(c)-кодСимв8("0"))
				иначе res := res * 16 + (кодСимв8(ASCII_вЗаглавную(c))-кодСимв8("A") + 10)
				всё
			кц;
			возврат res
		кон GetNCharHex;

		(* read "XXXXXXVV|NNv=", where
			XXXXXX UCS32 (ascii-hex),
			VV variant (ascii-hex),
			| fix separator,
			NN variable sized UTF-8 encoded UCS32 value,
			VV VV
			= fix separator *)
		проц ReadUCSVariant(r : Потоки.Чтец; перем ucs: цел32; перем variant : цел32);
		перем c : симв8; tucs: симв32; tv : цел32;
		нач
			ucs := GetNCharHex(r, 6);
			variant := GetNCharHex(r, 2);
			 (* sanity check *)
			c := r.чИДайСимв8(); утв(c = "|");
			если ~GetUTF8Char(r, tucs) то СТОП(1000) всё; утв(кодСимв32(tucs) = ucs);
			tv := GetNCharHex(r, 2); утв(tv = variant);
			c := r.чИДайСимв8(); утв(c = "=")
		кон ReadUCSVariant;

		проц ReadStrokes(r : Потоки.Чтец; g : Glyph);
		перем i : цел32;
			проц ReadStrokeElement;
			перем c : симв8;
			нач
				c := r.чИДайСимв8(); утв(c = "0");
				c := r.чИДайСимв8(); утв(c = "0");
				(* read command *)
				workStrokes[nofStrokes].cmd := GetNCharHex(r, 2);
				workStrokes[nofStrokes].x := GetNCharHex(r, 2);
				workStrokes[nofStrokes].y := GetNCharHex(r, 2);
				увел(nofStrokes);
			кон ReadStrokeElement;
		нач
			nofStrokes := 0;
			нцПока r.ПодглядиСимв8() = "0" делай ReadStrokeElement кц;
			нов(g.strokes, nofStrokes);
			g.nofStrokes := nofStrokes;
			нцДля i := 0 до nofStrokes - 1 делай g.strokes[i] := workStrokes[i] кц
		кон ReadStrokes;

		проц ReadBasicGlyph(r : Потоки.Чтец; g : Glyph);
		перем ch : симв8;
		нач
			ch := r.чИДайСимв8(); утв(ch = "!");
			ReadUCSVariant(r, g.ucs, g.variant);
			ReadStrokes(r, g);
			r.ПропустиДоКонцаСтрокиТекстаВключительно
		кон ReadBasicGlyph;

		проц ReadGlyphComponents(r: Потоки.Чтец; g : Glyph);
		перем i : цел32;
			проц ReadComponent;
			нач
				workGlyphRef[nofGlyphRefs].x := GetNCharHex(r, 2);
				workGlyphRef[nofGlyphRefs].y := GetNCharHex(r, 2);
				workGlyphRef[nofGlyphRefs].w := GetNCharHex(r, 2);
				workGlyphRef[nofGlyphRefs].h := GetNCharHex(r, 2);
				увел(nofGlyphRefs)
			кон ReadComponent;
		нач
			nofGlyphRefs := 0;
			нцПока (r.ПодглядиСимв8() > " ") делай
				если GetUTF8Char(r, workGlyphRef[nofGlyphRefs].refucs) то
					workGlyphRef[nofGlyphRefs].refvariant := GetNCharHex(r, 2);
					ReadComponent
				всё
			кц;
			нов(g.subComponents, nofGlyphRefs);
			g.nofSubComponents := nofGlyphRefs;
			нцДля i := 0 до nofGlyphRefs - 1 делай g.subComponents[i] := workGlyphRef[i] кц
		кон ReadGlyphComponents;

		проц ReadCompositGlyph(r : Потоки.Чтец; g : Glyph);
		перем ch : симв8;
		нач
			ch := r.чИДайСимв8(); утв(ch = " ");
			ReadUCSVariant(r, g.ucs, g.variant);
			ReadGlyphComponents(r, g);
			r.ПропустиДоКонцаСтрокиТекстаВключительно
		кон ReadCompositGlyph;

		проц ParseGlyph(r : Потоки.Чтец) : Glyph;
		перем new : Glyph;
		нач
			нов(new);
			если r.ПодглядиСимв8() = "!" то ReadBasicGlyph(r, new);
			иначе ReadCompositGlyph(r, new)
			всё;
			возврат new
		кон ParseGlyph;

		проц LoadFromStream*(r : Потоки.Чтец) : булево;
		нач
			ЛогЯдра.пСтроку8("Loading all glyphs..."); ЛогЯдра.пВК_ПС;
			нцПока (r.ПодглядиСимв8() >= " ") и (r.кодВозвратаПоследнейОперации = 0) делай
				glyphList.Add(ParseGlyph(r));
				если glyphList.GetCount() остОтДеленияНа 10000 = 0 то ЛогЯдра.пЦел64(glyphList.GetCount(), 5); ЛогЯдра.пВК_ПС всё
			кц;
			ЛогЯдра.пЦел64(glyphList.GetCount(), 5); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("Finished."); ЛогЯдра.пВК_ПС;
			возврат истина
		кон LoadFromStream;

		проц LoadFromFile*(конст fileName : массив из симв8) : булево;
		перем f : Files.File; r : Files.Reader;
		нач
			f := Files.Old(fileName);
			если f = НУЛЬ то возврат ложь всё;
			Files.OpenReader(r, f, 0);
			возврат LoadFromStream(r)
		кон LoadFromFile;
	кон RawCCGFont;


перем testWindow : TestWindow;

проц Open*;
перем f : RawCCGFont;
нач
	если testWindow = НУЛЬ то
		нов(f);
		если f.LoadFromFile("song.fnt") то нов(testWindow, f); ЛогЯдра.пСтроку8("done.") иначе ЛогЯдра.пСтроку8("failed.") всё;
	всё;
кон Open;

проц Export*;
нач
	если testWindow # НУЛЬ то
		testWindow.font.CreateRanges;
		testWindow.font.Save("Song.ccg");
		testWindow.font.Load("Song.ccg");
	всё;
кон Export;

(* read a UTF8 character form a stream *)
проц GetUTF8Char(r : Потоки.Чтец; перем u : симв32) : булево;
перем ch : массив 8 из симв8; i : размерМЗ;
нач
	ch[0] := r.чИДайСимв8();
	нцДля i := 1 до кодСимв8(UCS32.CodeLength[кодСимв8(ch[0])]) - 1 делай ch[i] := r.чИДайСимв8() кц;
	i := 0;
	возврат UTF8Strings.DecodeChar(ch, i, u)
кон GetUTF8Char;

(*
PROCEDURE Spline(canvas : WMGraphics.Canvas; x0, y0, x1, y1, x2, y2, x, y, w, h, color, mode : SIGNED32);
VAR i, tx, ty, nx, ny : SIGNED32;
	t, onet, dt : FLOAT32; (* CHECK : possible fixed-point implementation*)
BEGIN
	tx := x0; ty := y0;
	dt := 1 / MaxSplineSeg; t := 0; onet := 1;
	FOR i := 0 TO MaxSplineSeg DO
		nx := ENTIER(onet * onet * x0 + 2 * t * onet * x1 + t * t * x2);
		ny := ENTIER(onet * onet * y0 + 2 * t * onet * y1 + t * t * y2);
		canvas.Line(x + (tx * w) DIV 256, y + (ty * h) DIV 256, x + (nx * w) DIV 256, y + (ny * h) DIV 256, color, mode);
		t := t + dt; onet := 1 - t; tx := nx; ty := ny
	END
END Spline;
*)

проц SplineReal(canvas : WMGraphics.Canvas; x0, y0, x1, y1, x2, y2, x, y, w, h : вещ32; color, mode : цел32);
перем i: цел32;  tx, ty, nx, ny : вещ32;
	t, onet, dt : вещ32;
нач
	tx := x0; ty := y0;
	dt := 1 / MaxSplineSeg; t := 0; onet := 1;
	нцДля i := 0 до MaxSplineSeg делай
		nx := округлиВниз(onet * onet * x0 + 2 * t * onet * x1 + t * t * x2);
		ny := округлиВниз(onet * onet * y0 + 2 * t * onet * y1 + t * t * y2);
		canvas.Line(округлиВниз(x + (tx * w) / 256), округлиВниз(y + (ty * h) / 256),
		округлиВниз(x + (nx * w) / 256), округлиВниз(y + (ny * h) / 256), color, mode);
		t := t + dt; onet := 1 - t; tx := nx; ty := ny
	кц
кон SplineReal;

проц Cleanup;
нач
	если testWindow # НУЛЬ то testWindow.Close всё
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон WMFontCCGConverter.
(*

Aos.Call WMFontCCGConverter.Open ~
Aos.Call WMFontCCGConverter.Export ~
System.Free WMFontCCGConverter ~
OFSTools.Mount RAM RamFS 4096 4096 ~
Hex.Open RAM:Single.ccg ~
System.Directory RAM:Single.ccg\d ~
*)

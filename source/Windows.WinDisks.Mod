модуль WinDisks;   (**  AUTHOR "fof"; PURPOSE "module to access partitions under Windows";  **)

использует Kernel32, НИЗКОУР, Strings, ЛогЯдра, Потоки, Commands, Disks, Plugins, Modules, WinFS;

перем
	DeviceIoControl: проц {WINAPI} ( hDevice: Kernel32.HANDLE;  dwIoControlCode: цел32;  перем lpInBuffer: массив из НИЗКОУР.октет;  nInBufferSize: цел32;
																	    перем lpOutBuffer: массив из НИЗКОУР.октет;  nOutBufferSize: цел32;  перем lpBytesReturned: цел32;  lpOverlapped: динамическиТипизированныйУкль ): Kernel32.BOOL;

	SetFilePointer: проц {WINAPI} ( hFile: Kernel32.HANDLE;  lDistanceToMove: цел32;  перем lpDistanceToMoveHigh: цел32;  dwMoveMethod: цел32 ): цел32;

конст
	MaxExtents = 1;   (* do not handle more than one extents (yet?) *)
	BlockNumberInvalid* = 101;  Error* = 102;

тип
	DISK_GEOMETRY = запись
		Cylinders: цел64;
		MediaType: цел32;
		TracksPerCylinder: цел32;
		SectorsPerTrack: цел32;
		BytesPerSector: цел32;
	кон;

	DISK_EXTENT = запись   (* immer auf größten Member aligniert *)
		DiskNumber: цел32;
		padding: цел32;
		StartingOffset: цел64;
		ExtentLength: цел64;
	кон;

	VOLUME_DISK_EXTENTS = запись    (* immer auf größten Member aligniert: hugeint *)
		NumberOfDiskExtents: цел32;   (* the msdn reports something different (SIGNED32) but it works this way only *)
		padding: цел32;
		extents: массив   MaxExtents из DISK_EXTENT;   (* should be dynamic *)
	кон;

конст
	(* Media Types *)
	Unknown = 0;
	(* 1 - 10: Floppy *)
	RemovableMedia = 11;  FixedMedia = 12;
	(* 13-..: Floopy *)

	IOCTL_DISK_GET_DRIVE_GEOMETRY = 00070000H;  VOLUME_GET_VOLUME_DISK_EXTENTS = 00560000H;
	FSCTL_LOCK_VOLUME = 90018H; FSCTL_UNLOCK_VOLUME = 9001CH; FSCTL_DISMOUNT_VOLUME = 90020H;

тип

	VirtualDisk = окласс (Disks.Device)
	перем handle: Kernel32.HANDLE;
		size: цел32;
		next: VirtualDisk;
		drive: цел32;

		проц Finish( перем res: целМЗ );
		нач
			неважно Kernel32.CloseHandle( handle );  ЛогЯдра.пСтроку8("Disk closed"); ЛогЯдра.пВК_ПС;
		кон Finish;

		проц {перекрыта}Transfer*( op, block, num: цел32;  перем data: массив из симв8;  ofs: размерМЗ;  перем res: целМЗ );
		перем bool, n,err: цел32;  pos: цел64;  poslow, poshigh: цел32;  large: Kernel32.LargeInteger;
		нач {единолично}
			если (block < 0) или (num < 1) или (block + num > size) то res := BlockNumberInvalid;  возврат;  всё;

			pos := устарПреобразуйКБолееШирокомуЦел( block ) * устарПреобразуйКБолееШирокомуЦел( blockSize );

			poslow := устарПреобразуйКБолееУзкомуЦел( pos );  poshigh := устарПреобразуйКБолееУзкомуЦел( арифмСдвиг( pos, -32 ) );

			large.LowPart := poslow;  large.HighPart := poshigh;

			если ~LockVolume(handle) то всё;

			bool := SetFilePointer( handle, poslow, poshigh, Kernel32.FileBegin );
			если bool = -1 то res := BlockNumberInvalid;  СТОП( 101 );  возврат;  всё;

			если op = Disks.Read то
				bool := Kernel32.ReadFile( handle, data[ofs], num * blockSize, n, НУЛЬ );
				если (bool > 0) и (num * blockSize = n) то
					res := Disks.Ok;
				иначе
					res := Error;
				всё;
			аесли op = Disks.Write то
				если Disks.ReadOnly в flags то
					ЛогЯдра.пСтроку8("Write attempt on read-only mounted drive "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пВК_ПС;
					res := Disks.Ok;   (* readonly *)
				иначе
					bool := Kernel32.WriteFile(handle,data[ofs],num*blockSize,n,НУЛЬ);
					если (bool # 0)  и (num * blockSize = n)  то
						res := Disks.Ok;
					иначе
						res := Error;
						err := Kernel32.GetLastError();
						ЛогЯдра.пСтроку8("last error = "); ЛогЯдра.пЦел64(err,1); ЛогЯдра.пВК_ПС;
					всё;
				всё;
			иначе res := Disks.Unsupported;
			всё;

			если ~UnlockVolume(handle) то всё;

			если Disks.Stats то
				если op = Disks.Read то
					увел (NnofReads);
					если (res = Disks.Ok) то увел (NbytesRead, num * blockSize);
					иначе увел (NnofErrors);
					всё;
				аесли op = Disks.Write то
					увел (NnofWrites);
					если (res = Disks.Ok) то увел (NbytesWritten, num * blockSize);
					иначе увел (NnofErrors);
					всё;
				иначе
					увел (NnofOthers);
				всё;
			всё;
		кон Transfer;

		проц {перекрыта}GetSize*( перем size: цел32; перем res: целМЗ );
		нач
			size := сам.size;  res := Disks.Ok;
		кон GetSize;

		проц {перекрыта}Handle*( перем msg: Disks.Message;  перем res: целМЗ );
		нач
			res := Disks.Unsupported;
		кон Handle;

		проц & New*( handle: Kernel32.HANDLE;  конст diskname: массив из симв8; drive: цел32; flags: мнвоНаБитахМЗ;  blockSize, blocks: цел32 );
		нач
			утв( handle > 0 );  сам.handle := handle;  сам.blockSize := blockSize;  сам.size := blocks;  SetName( diskname );  desc := "Windows Disk ";
			сам.drive := drive;
			сам.flags := flags;
		кон New;

	кон VirtualDisk;

перем
	disks: VirtualDisk;   (* to enable cleanup when unloading module *)

	проц AddDisk( vd: VirtualDisk );
	нач {единолично}
		vd.next := disks;  disks := vd;
	кон AddDisk;

	проц RemoveDisk( vd: VirtualDisk );
	перем d: VirtualDisk;
	нач {единолично}
		если disks = vd то disks := disks.next;
		иначе
			d := disks;
			нцПока (d # НУЛЬ ) и (d.next # vd) делай d := d.next;  кц;
			если (d # НУЛЬ ) то d.next := d.next.next;  всё;
		всё;
	кон RemoveDisk;

	проц IsMounted( dev: Disks.Device ): булево;
	перем i: размерМЗ;
	нач
		если dev.table # НУЛЬ то
			нцДля i := 0 до длинаМассива( dev.table ) - 1 делай
				если Disks.Mounted в dev.table[i].flags то возврат истина всё
			кц
		всё;
		возврат ложь
	кон IsMounted;

(** Remove virtual disk *)
	проц Uninstall*(context : Commands.Context);   (** diskname ~ *)
	перем diskname: Plugins.Name;  plugin: Plugins.Plugin;  drive: цел32; v: VirtualDisk;
	нач
		context.arg.ПропустиБелоеПоле;
		context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(diskname);

		plugin := Disks.registry.Get( diskname );
		если plugin = НУЛЬ то (* try to map disk name *)
			если diskname[1] = ":" то
				drive := кодСимв8(ASCII_вЗаглавную(diskname[0]))-кодСимв8("A");
				v := disks;
				нцПока(v#НУЛЬ) и (v.drive # drive) делай
					v := v.next;
				кц;
				plugin := v;
			всё;
		всё;

		если plugin # НУЛЬ то
			если ~IsMounted( plugin( VirtualDisk ) ) то
				Disks.registry.Remove( plugin );  RemoveDisk( plugin( VirtualDisk ) );
				context.out.пСтроку8( diskname );  context.out.пСтроку8( " removed" );  context.out.пВК_ПС;
			иначе
				context.error.пСтроку8( diskname );  context.error.пСтроку8( " is mounted." );  context.error.пВК_ПС;
			всё;
		иначе
			context.error.пСтроку8( diskname );  context.error.пСтроку8( " not found" );  context.error.пВК_ПС;
			context.result := Commands.CommandError;
		всё;
	кон Uninstall;

	проц Cleanup;
	перем res: целМЗ;
	нач {единолично}
		нцПока (disks # НУЛЬ ) делай disks.Finish( res );  Disks.registry.Remove( disks );  disks := disks.next;  кц;
	кон Cleanup;

	проц ReportDiskGeometry( перем pdg: DISK_GEOMETRY; out : Потоки.Писарь );
	перем size: вещ64;
	нач
		out.пСтроку8( "Disk type: " );
		просей pdg.MediaType из
		| Unknown:
				out.пСтроку8( "unknown" );
		| RemovableMedia:
				out.пСтроку8( "removable media" );
		| FixedMedia:
				out.пСтроку8( "fixed media" );
		иначе out.пСтроку8( "floppy" );
		всё;
		out.пВК_ПС;  out.пСтроку8( "Cylinders = " );  out.пЦел64( устарПреобразуйКБолееУзкомуЦел( pdg.Cylinders ), 1 );  out.пВК_ПС;
		out.пСтроку8( "TracksPerCylinder = " );  out.пЦел64( pdg.TracksPerCylinder, 8 );  out.пВК_ПС;
		out.пСтроку8( "SectorsPerTrack = " );  out.пЦел64( pdg.SectorsPerTrack, 8 );  out.пВК_ПС;
		out.пСтроку8( "BytesPerSector = " );  out.пЦел64( pdg.BytesPerSector, 8 );  out.пВК_ПС;
		size := pdg.Cylinders;  size := size * pdg.TracksPerCylinder * pdg.SectorsPerTrack * pdg.BytesPerSector;
		out.пСтроку8( "DiskSize = " );  OutSize( size, out );  out.пВК_ПС;
	кон ReportDiskGeometry;

	проц GetDiskGeometry( handle: Kernel32.HANDLE;  перем pdg: DISK_GEOMETRY ): булево;
	перем done, returned: цел32;
	нач
		done := DeviceIoControl( handle, IOCTL_DISK_GET_DRIVE_GEOMETRY, НУЛЬ , 0, pdg, размер16_от( DISK_GEOMETRY ), returned, НУЛЬ );  возврат done > 0;
	кон GetDiskGeometry;

	(* lock, unlock and dismount only works on volumes specified as "X:". It does not work with PhysicalDrive.

	 *)
	проц LockVolume(handle: Kernel32.HANDLE): булево;
	перем done,returned: цел32;
	нач
		done := DeviceIoControl(handle, FSCTL_LOCK_VOLUME, НУЛЬ,0, НУЛЬ, 0, returned, НУЛЬ);
		возврат done > 0;
	кон LockVolume;

	проц UnlockVolume(handle: Kernel32.HANDLE): булево;
	перем done,returned: цел32;
	нач
		done := DeviceIoControl(handle, FSCTL_UNLOCK_VOLUME, НУЛЬ,0, НУЛЬ, 0, returned, НУЛЬ);
		возврат done > 0;
	кон UnlockVolume;

	проц DismountVolume(handle: Kernel32.HANDLE): булево;
	перем done,returned: цел32;
	нач
		done := DeviceIoControl(handle, FSCTL_DISMOUNT_VOLUME, НУЛЬ,0, НУЛЬ, 0, returned, НУЛЬ);
		возврат done > 0;
	кон DismountVolume;


	проц AppendInt( перем name: массив из симв8;  i: цел32 );
	перем str: массив 8 из симв8;
	нач
		Strings.IntToStr( i, str );  Strings.Append( name, str );
	кон AppendInt;

	проц OpenVolume( перем handle: Kernel32.HANDLE;  flags: мнвоНаБитахМЗ; конст name: массив из симв8; context : Commands.Context ): булево;
	перем devname: массив 256 из симв8;  tflags: INTEGERSET; errorcode : цел32;
	нач
		Strings.Concat( "\\.\", name, devname );
		если Disks.ReadOnly в flags то tflags := {Kernel32.GenericRead} иначе tflags := {(*2 (* Kernel32.GenericDelete *), *)Kernel32.GenericWrite,Kernel32.GenericRead} всё;
		handle := Kernel32.CreateFile( devname, tflags, {Kernel32.FileShareRead, Kernel32.FileShareWrite}, НУЛЬ, Kernel32.OpenExisting, {}, Kernel32.NULL );
		если (handle = Kernel32.InvalidHandleValue) то
			errorcode := Kernel32.GetLastError();
			context.error.пСтроку8("Could not open '"); context.error.пСтроку8(devname); context.error.пСтроку8("' : ");
			просей errorcode из
				|Kernel32.ErrorFileNotFound:
					context.error.пСтроку8('Drive or physical volume not found (Use a drive specification like "A:" or a phyiscal volume like PhysicalDrive0)');
				|Kernel32.ErrorAccessDenied:
					context.error.пСтроку8("Access denied (Administrator privileges required)");
			иначе
				context.error.пСтроку8("Windows Error Code: "); context.error.пЦел64(errorcode, 0);
			всё;
			context.error.пВК_ПС; context.result := Commands.CommandError;
		всё;
		возврат handle # Kernel32.InvalidHandleValue;
	кон OpenVolume;

	проц OutSize( f: вещ64; out : Потоки.Писарь );
	нач
		если f > 1.E9 то out.пВещ64_ФФТ( f / 1024 / 1024 / 1024, 4, 3, 0 );  out.пСтроку8( " GiB" );
		аесли f > 1.E6 то out.пВещ64_ФФТ( f / 1024 / 1024, 4, 3, 0 );  out.пСтроку8( " MiB" );
		аесли f > 1.E3 то out.пВещ64_ФФТ( f / 1024, 4, 3, 0 );  out.пСтроку8( " KiB" );
		иначе out.пВещ64_ФФТ( f, 4, 3, 0 );  out.пСтроку8( " B" );
		всё;
	кон OutSize;

	проц GetPhysicalDrive( перем handle: Kernel32.HANDLE; flags: мнвоНаБитахМЗ;  перем name: массив из симв8; context : Commands.Context );
	перем done, returned: цел32;  extents: VOLUME_DISK_EXTENTS;  drive: цел32;  first, last: вещ64;  bps: вещ64;  pdg: DISK_GEOMETRY;   (* number of first and last block used *)
	нач
		done := DeviceIoControl( handle, VOLUME_GET_VOLUME_DISK_EXTENTS, НУЛЬ , 0, extents, размер16_от( VOLUME_DISK_EXTENTS ), returned, НУЛЬ );
		если done > 0 то
			если extents.NumberOfDiskExtents = 0 то
				context.error.пСтроку8( "no disk extents used, probably the drive is physical already " );  context.error.пВК_ПС;
				context.result := Commands.CommandError;
			аесли extents.NumberOfDiskExtents = 1 то
				drive := extents.extents[0].DiskNumber;
				если GetDiskGeometry( handle, pdg ) то bps := pdg.BytesPerSector иначе bps := 512 всё;
				first := extents.extents[0].StartingOffset / bps;  last := extents.extents[0].ExtentLength / bps;  last := first + last;
				context.out.пСтроку8( "Partition from Block " );  context.out.пЦел64( округлиВниз( first ), 1 );  context.out.пСтроку8( " to " );  context.out.пЦел64( округлиВниз( last ), 1 );
				context.out.пСтроку8( " in physical drive # " );  context.out.пЦел64( drive, 1 );  OutSize( extents.extents[0].ExtentLength, context.out);  context.out.пВК_ПС;
				name := "PhysicalDrive";  AppendInt( name, drive );  context.out.пСтроку8( "Mapping to drive : " );  context.out.пСтроку8( name );  context.out.пВК_ПС;
				если ~OpenVolume( handle, flags,name, context) то
					context.error.пСтроку8( "volume could not be opened. " ); context.error.пВК_ПС;
					context.result := Commands.CommandError;
				всё;
			иначе
				context.error.пСтроку8( "cannot handle volumes with more than one extent (yet) " ); context.error.пВК_ПС;
				context.result := Commands.CommandError;
			всё;
		иначе
			context.error.пСтроку8( "GetPhysicalDrive: no success, probably the drive is already physical" ); context.error.пВК_ПС;
			context.result := Commands.CommandError;
		всё;
	кон GetPhysicalDrive;

(** Add file as virtual disk *)
	проц Install*(context : Commands.Context);   (** diskname filename [blocksize]  ~ *)
	перем
		diskname, flagss: массив 256 из симв8;  flags: мнвоНаБитахМЗ;
		res: целМЗ; handle: Kernel32.HANDLE;  pdg: DISK_GEOMETRY;  size: цел32;  vd: VirtualDisk;  drive: цел32;
		i: цел32;
	нач
		context.arg.ПропустиБелоеПоле;
		context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(diskname);
		context.arg.ПропустиБелоеПоле;
		flagss := ""; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(flagss);

		flags := {Disks.ReadOnly};
		 i := 0;
		нцПока(flagss[i] # 0X) делай
			если flagss[i] = "W" то
				исключиИзМнваНаБитах(flags,Disks.ReadOnly);
			всё;
			увел(i);
		кц;

		если diskname[1] = ":" то drive := кодСимв8(ASCII_вЗаглавную(diskname[0]))-кодСимв8("A"); иначе drive := -1 всё;
		если OpenVolume( handle, flags, diskname, context) то
			если (diskname[1] = ":") и DismountVolume(handle) то
			всё;
			GetPhysicalDrive( handle, flags,diskname, context );
			если ~GetDiskGeometry( handle, pdg ) то
				неважно Kernel32.CloseHandle( handle );
				context.error.пСтроку8( "Could not determine disk geometry " );  context.error.пВК_ПС;
				context.result := Commands.CommandError;
			иначе
				ReportDiskGeometry( pdg, context.out);
				если pdg.MediaType = RemovableMedia то включиВоМнвоНаБитах(flags,Disks.Removable) всё;
				если pdg.Cylinders > матМаксимум( цел32 ) то
					СТОП( 100 )
				иначе
					size := устарПреобразуйКБолееУзкомуЦел( pdg.Cylinders ) * pdg.TracksPerCylinder * pdg.SectorsPerTrack;
				всё;

				нов( vd, handle, diskname, drive, flags, pdg.BytesPerSector, size );  Disks.registry.Add( vd, res );
				если res = Plugins.Ok то
					AddDisk( vd );
					context.out.пСтроку8( diskname );  context.out.пСтроку8( " registered." ); context.out.пВК_ПС;
				иначе
					неважно Kernel32.CloseHandle( handle );
					context.error.пСтроку8( "Could not register disk, res: " ); context.error.пЦел64( res, 0 ); context.error.пВК_ПС;
					context.result := Commands.CommandError;
				всё;
			всё;
		всё;
	кон Install;

	проц Notification(type: цел32; drives: мнвоНаБитахМЗ);
	перем v: VirtualDisk; res: целМЗ;
	нач
		если type = WinFS.deviceArrival то
		аесли type = WinFS.deviceRemove то
			v := disks;
			нцПока(v # НУЛЬ) делай
				если (v.drive >= 0) и (v.drive в drives) то
					если IsMounted (v) то
						ЛогЯдра.пСтроку8("Warning: Disk mounted but forcefully removed !"); ЛогЯдра.пВК_ПС;
					всё;
					Disks.registry.Remove( v ); v.Finish(res);  RemoveDisk( v );
					ЛогЯдра.пСтроку8( v.name );  ЛогЯдра.пСтроку8( " removed" );  ЛогЯдра.пВК_ПС;
					v := disks;
				иначе
					v := v.next;
				всё;
			кц;
		всё;
	кон Notification;

	проц Init;
	перем mod: Kernel32.HMODULE;
		str: массив 64 из симв8;
	нач
		str := "Kernel32.DLL";  mod := Kernel32.LoadLibrary( str );  str := "DeviceIoControl";  Kernel32.GetProcAddress( mod, str, НИЗКОУР.подмениТипЗначения( адресВПамяти, DeviceIoControl ) );  str := "SetFilePointer";
		Kernel32.GetProcAddress( mod, str, НИЗКОУР.подмениТипЗначения( адресВПамяти, SetFilePointer ) );
		если DeviceIoControl = НУЛЬ то СТОП( 100 ) всё;
		если SetFilePointer = НУЛЬ то СТОП( 100 ) всё;
		Modules.InstallTermHandler( Cleanup );
		WinFS.RegisterNotification(Notification);
	кон Init;

нач
	Init();
кон WinDisks.


short description

WinDisks is a module to access volumes and partitions under WinAos.
One purpose is the access of file systems that are supported by Aos but not by Windows. The other is the partitioning of hard disks within WinAos.
It may thus also be used to install an entire native Aos system on a partition on the hard drive or USB memory stick etc.

To add a windows disk to the Aos system use the command

	WinDisks.Install DriveName ["RW"]

where
- DriveName can be one of "A:" to "Z:" or "PhysicalDriveX" where X has to be replaced by the physical drive number.
A drive name such as "C:" is matched to a PhysicalDriveX name, if appropriate.
It is better to use the "X:" format because the system can in general then perform an automatic unregistering if the device is becoming unavailable in Windows.
- If "PhysicalDriveX" is used to specify the drive, the logical volumes associated with it will not be dismounted, which can mean that write operations are unsuccesful
- A volume is inserted read-only unless the optional parameter "RW" is provided.

To access the partitions of the drive you may use the Partition Tool.

To uninstall an installed volume in Aos, use the command

WinDisks.Uninstall DriveName .

~

Examples

WinDisks.Install "f:"
WinDisks.Install "c:"
WinDisks.Install "PhysicalDrive1" "RW"
WinDisks.Uninstall "C:" ~
WinDisks.Uninstall "PhysicalDrive0" ~
WinDisks.Uninstall "F:" ~
WinDisks.Install "f:" "RW" ~

System.Free WinDisks ~

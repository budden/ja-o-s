(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль TestServer; (** AUTHOR "pjm"; PURPOSE "TCP test server (echo, discard, chargen, daytime)"; *)

(* TCP Echo (RFC 862), Discard (RFC 863), Daytime (RFC 867) and Chargen (RFC 864) services for Aos. *)

использует Modules, ЛогЯдра, TCP, TCPServices, Потоки, Clock;

конст
	EchoPort = 7;
	EchoBufSize = 4096;

	DiscardPort = 9;
	DiscardBufSize = 4096;

	ChargenPort = 19;
	ChargenFirstChar = 32; ChargenNumChars = 95;
	ChargenLineLength = 72; ChargenLineSize = 74;
	CharGenBufSize = ChargenLineSize * ChargenNumChars;

	DayTimePort = 13;

	Ok = TCP.Ok;

	Trace = истина;

тип
	DiscardAgent = окласс (TCPServices.Agent)
		перем len: размерМЗ; res: целМЗ; buf: массив DiscardBufSize из симв8;

	нач {активное}
		нцДо
			client.ПрочтиИзПотока(buf, 0, длинаМассива(buf), длинаМассива(buf), len, res)
		кцПри res # Ok;
		если Trace то
			ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Discard result "); ЛогЯдра.пЦел64(res, 1); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
		всё;
		Terminate
	кон DiscardAgent;

тип
	EchoAgent = окласс (TCPServices.Agent)
		перем len: размерМЗ; res: целМЗ; buf: массив EchoBufSize из симв8;

	нач {активное}
		нц
			client.ПрочтиИзПотока(buf, 0, длинаМассива(buf), 1, len, res);
			если res # Ok то прервиЦикл всё;
			client.ЗапишиВПоток(buf, 0, len, ложь, res);
			если res # Ok то прервиЦикл всё
		кц;
		если Trace то
			ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Echo result "); ЛогЯдра.пЦел64(res, 1); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
		всё;
		Terminate
	кон EchoAgent;

тип
	ChargenAgent = окласс (TCPServices.Agent)
		перем res: целМЗ;

	нач {активное}
		нц
			client.ЗапишиВПоток(chargenbuf^, 0, CharGenBufSize, ложь, res);
			если res # Ok то прервиЦикл всё
		кц;
		если Trace то
			ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Chargen result "); ЛогЯдра.пЦел64(res, 1); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
		всё;
		Terminate
	кон ChargenAgent;

тип
	DayTimeAgent = окласс (TCPServices.Agent)
		перем time, date: цел32; w: Потоки.Писарь;

	нач {активное}
		Потоки.НастройПисаря(w, client.ЗапишиВПоток);
		Clock.Get(time, date);
		w.пДатуОберона822(time, date, Clock.tz);
		w.пВК_ПС;
		w.ПротолкниБуферВПоток;
		Terminate
	кон DayTimeAgent;

перем
	discard, echo, chargen, daytime: TCPServices.Service;
	chargenbuf: укль на массив CharGenBufSize из симв8;

проц InitChargenBuf;
перем i, j, k: цел32;
нач
	k := 0; нов(chargenbuf);
	нцДля i := 1 до ChargenNumChars делай
		нцДля j := 0 до ChargenLineLength-1 делай
			chargenbuf[k] := симв8ИзКода(ChargenFirstChar + (i+j) остОтДеленияНа ChargenNumChars); увел(k)
		кц;
		chargenbuf[k] := 0DX; chargenbuf[k+1] := 0AX; увел(k, 2)
	кц;
	утв(k = CharGenBufSize)
кон InitChargenBuf;

проц Open*;
перем res : целМЗ;
нач
	нов(discard, DiscardPort, NewDiscardAgent, res);
	нов(echo, EchoPort, NewEchoAgent, res);
	нов(chargen, ChargenPort, NewChargenAgent, res);
	нов(daytime, DayTimePort, NewDayTimeAgent, res);
кон Open;

проц Close*;
нач
	discard.Stop; discard := НУЛЬ;
	echo.Stop; echo := НУЛЬ;
	chargen.Stop; chargen := НУЛЬ;
	daytime.Stop; daytime := НУЛЬ;
кон Close;

проц NewDiscardAgent(c: TCP.Connection; s: TCPServices.Service): TCPServices.Agent;
перем a: DiscardAgent;
нач
	нов(a, c, s); возврат a
кон NewDiscardAgent;

проц NewEchoAgent(c: TCP.Connection; s: TCPServices.Service): TCPServices.Agent;
перем a: EchoAgent;
нач
	нов(a, c, s); возврат a
кон NewEchoAgent;

проц NewChargenAgent(c: TCP.Connection; s: TCPServices.Service): TCPServices.Agent;
перем a: ChargenAgent;
нач
	нов(a, c, s); возврат a
кон NewChargenAgent;

проц NewDayTimeAgent(c: TCP.Connection; s: TCPServices.Service): TCPServices.Agent;
перем a: DayTimeAgent;
нач
	нов(a, c, s); возврат a
кон NewDayTimeAgent;

проц Cleanup;
нач
	Close;
кон Cleanup;

нач
	InitChargenBuf;
	discard := НУЛЬ; echo := НУЛЬ; chargen := НУЛЬ; daytime := НУЛЬ;
	Modules.InstallTermHandler(Cleanup)	(* there is still a race with System.Free *)
кон TestServer.

System.Free TestServer ~

System.OpenKernelLog

Aos.Call TestServer.Open
Aos.Call TestServer.Close

модуль SdDisks;
(**
	AUTHOR Timothée Martiel, 12/2015
	PURPOSE Disk interface for the SD card driver
*)

использует
	НИЗКОУР,
	Disks, Строки8,
	Sd, SdEnvironment,
	Трассировка;

конст
	(** Base name for SD block devices *)
	BaseDevName = "SD";
	(** Number of entries used in the cache *)
	CacheSize = 32;

тип
	(** SD Block Device *)
	BlockDevice * = укль на BlockDeviceDesc;
	BlockDeviceDesc * = запись (Disks.BlockDeviceDesc)
		card: Sd.Card;
		bufferSize: цел32;
		cache: укль на массив из CacheEntry;
		nextSd: BlockDevice;
	кон;

	(** SD cache entry. *)
	CacheEntry = запись
		buffer: укль на массив из симв8;
		au: цел32;
		active, mod: булево;
	кон;

	(** Create a new block device for the specified card *)
	проц InitBlockDevice * (d: BlockDevice; card: Sd.Card);
	перем
		name, id: массив 32 из симв8;
		ignore, i: цел32;
	нач
		name := BaseDevName;
		Строки8.ПишиЦел64_вСтроку(devId, id);
		увел(devId);
		Строки8.ПодклейВСтрокуХвост(name, id);
		Disks.InitBlockDevice(d, name);
		(* на самом деле скорее всего здесь ещё нужно проверять, что мы в QEMU, но 
			у нас пока (простая карта) === (мы находимся в QEMU) *)
		если Sd.ProstajaSD_KartaLi то 
			если card.sdStatus.auSize = 0 то
				Трассировка.пСтроку8("card.sdStatus.auSize is zero. Set to other value");
				d.bufferSize := 1 (* 16 * 1024 DIV Sd.BlockSize *);
			иначе
				d.bufferSize := card.sdStatus.auSize DIV Sd.BlockSize
			всё;
		иначе
			d.bufferSize := card.sdStatus.auSize DIV Sd.BlockSize;
		всё;
		нов(d.cache, CacheSize);
		нцДля i := 0 до CacheSize - 1 делай нов(d.cache[i].buffer, d.bufferSize * Sd.BlockSize) кц;

		d.blockSize := Sd.BlockSize;
		d.Read := Read;
		d.Write := Write;
		d.Open := Open;
		d.Close := Close;
		d.Reset := Reset;
		d.GetSize := GetSize;
		d.Handle := Handle;
		d.card := card
	кон InitBlockDevice;

	(** Read blocks from SD card *)
	проц Read (dev: Disks.BlockDevice;  block, num: цел32; перем data: массив из НИЗКОУР.октет; ofs: цел32; перем res: цел32);
	перем
		au, auOfs, idx, size, s: цел32;
	нач
		просейТип dev: BlockDevice делай
			size := num * Sd.BlockSize;
			нцПока size > 0 делай
				au := block DIV dev.bufferSize;
				auOfs := block остОтДеленияНа dev.bufferSize;
				idx := au остОтДеленияНа CacheSize;
				если ~UpdateCacheEntry(dev, dev.cache[idx], au, res) то возврат всё;
				s := матМинимум(size, (dev.bufferSize - auOfs) * Sd.BlockSize);
				НИЗКОУР.копируйПамять(адресОт(dev.cache[idx].buffer[auOfs * Sd.BlockSize]), адресОт(data[ofs]), s);
				умень(size, s);
				увел(ofs, s);
				увел(block, s DIV Sd.BlockSize)
			кц
		всё
	кон Read;

	(** Write blocks to SD card *)
	проц Write (dev: Disks.BlockDevice;  block, num: цел32; перем data: массив из НИЗКОУР.октет; ofs: цел32; перем res: цел32);
	перем
		au, auOfs, idx, size, s: цел32;
	нач
		просейТип dev: BlockDevice делай
			size := num * Sd.BlockSize;
			нцПока size > 0 делай
				au := block DIV dev.bufferSize;
				auOfs := block остОтДеленияНа dev.bufferSize;
				idx := au остОтДеленияНа CacheSize;
				если ~UpdateCacheEntry(dev, dev.cache[idx], au, res) то возврат всё;
				dev.cache[idx].mod := истина;
				s := матМинимум(size, (dev.bufferSize - auOfs) * Sd.BlockSize);
				утв(ofs + s <= длинаМассива(data));
				утв(auOfs * Sd.BlockSize + s <= длинаМассива(dev.cache[idx].buffer));
				НИЗКОУР.копируйПамять(адресОт(data[ofs]), адресОт(dev.cache[idx].buffer[auOfs * Sd.BlockSize]), s);
				умень(size, s);
				увел(ofs, s);
				увел(block, s DIV Sd.BlockSize)
			кц
		всё
	кон Write;

	(** Update cache entry to make sure it contains the data for the given au *)
	проц UpdateCacheEntry (dev: BlockDevice; перем cache: CacheEntry; au: цел32; перем res: цел32): булево;
	перем
		ignore: булево;
	нач
		если ~cache.active или (cache.au # au) то
			если cache.active и cache.mod то
				если ~Sd.Write(dev.card, cache.au * dev.bufferSize, dev.bufferSize * Sd.BlockSize, cache.buffer^, 0, res) то
					возврат ложь
				всё;
			всё;
			если ~Sd.Read(dev.card, au * dev.bufferSize, dev.bufferSize * Sd.BlockSize, cache.buffer^, 0, res) то
				возврат ложь
			всё;
			cache.active := истина;
			cache.mod := ложь;
			cache.au := au
		всё;
		возврат истина
	кон UpdateCacheEntry;

	(** Open a device: turn LED on *)
	проц Open (dev: Disks.BlockDevice; перем res: цел32);
	нач
		Sd.SetLedState(dev(BlockDevice).card.hc, истина)
	кон Open;

	(** Close a device: turn off LED and sync cache *)
	проц Close (dev: Disks.BlockDevice; перем res: цел32);
	перем
		i: цел32;
	нач
		просейТип dev: BlockDevice делай
			(* Sync cache *)
			нцДля i := 0 до CacheSize - 1 делай
				если dev.cache[i].active и dev.cache[i].mod то
					если ~Sd.Write(dev.card, dev.cache[i].au * dev.bufferSize, dev.bufferSize * Sd.BlockSize, dev.cache[i].buffer^, 0, res) то возврат всё;
				всё
			кц;

			(* Turn LED off *)
			Sd.SetLedState(dev.card.hc, ложь)
		всё
	кон Close;

	(** Reset SD card -- not implemented yet *)
	проц Reset (dev: Disks.BlockDevice; перем res: цел32);
	нач
	кон Reset;

	(** Get size of device *)
	проц GetSize (dev: Disks.BlockDevice;  перем size, res: цел32);
	нач
			size := цел32(dev(BlockDevice).card.csd.capacity);
			если size < 0 то size := матМаксимум(цел32) всё;
			res := Disks.Ok
	кон GetSize;

	(** Handle messages *)
	проц Handle (dev: Disks.BlockDevice; msg: цел32; par1, par2: цел32; перем res: цел32);
	нач
		res := Disks.Ok
	кон Handle;

	(** Handle SD Controller Events: create & register a new disk on card insertion, remove disk on card removal *)
	проц HandleSdEvent * (card: Sd.Card; event: цел32; param: динамическиТипизированныйУкль);
	перем
		dev, prev: BlockDevice;
	нач
		просей event из
			 Sd.OnInitialization:
				нов(dev);
				InitBlockDevice(dev, card);
				включиВоМнвоНаБитах(dev.flags, Disks.Removable);
				Disks.Register(dev);

				увел(devId);
				dev.nextSd := devices;
				devices := dev;
				SdEnvironment.String("Disk ");
				SdEnvironment.String(dev.name);
				SdEnvironment.String(" is now available");
				SdEnvironment.Ln

			|Sd.OnRemoval:
				утв(devices # НУЛЬ);
				если devices.card = card то
					SdEnvironment.String("Removed disk ");
					SdEnvironment.String(devices.name);
					SdEnvironment.Ln;
					devices := devices.nextSd
				иначе
					dev := devices;
					нцПока (dev # НУЛЬ) и (dev.card # card) делай
						prev := dev;
						dev := dev.nextSd
					кц;
					утв(dev # НУЛЬ);
					SdEnvironment.String("[SD] ERROR: device ");
					SdEnvironment.String(dev.name);
					SdEnvironment.String(" was removed");
					SdEnvironment.Ln;
					prev.nextSd := dev.nextSd
				всё;
		всё
	кон HandleSdEvent;

перем
	devId: цел32; (** Identifier number for next SD device *)
	devices: BlockDevice; (** List of SD block devices *)
кон SdDisks.

модуль WMTextTool;	(** AUTHOR "TF"; PURPOSE "Text Tool"; *)

использует
	Modules, Потоки, Commands, Texts, Строки8, WMComponents, WMRestorable, WMEditors, WMPopups, WMRectangles,
	WMGraphics, WMMessages, WMStandardComponents,
	WM := WMWindowManager, XML, XMLObjects;

конст
	WindowWidth = 122; WindowHeight = 220;

	(* field parameter for Change procedure *)
	ChangeFont = {0};
	ChangeSize = {1};
	ChangeStyle = {2};
	ChangeFgColor = {3};
	ChangeBgColor = {4};

	(* mode parameter for Change procedure *)
	Absolute = 0;
	IncrementBy = 1;
	DecrementBy = 2;

	(* Linefeed character *)
	LF = 0AX;

тип

	ChangeInfo = окласс(Texts.Attributes);
	перем
		name : массив 128 из симв8; 	(* font name *)
		fgColor, bgColor : WMGraphics.Color;		(* foreground and background color *)
		deltaSize : цел32;			(* new font size, interpretation depends on deltaSizeMode field *)
		deltaSizeMode : цел32;		(* Absolute, IncrementBy or DecrementBy *)
		style : мнвоНаБитахМЗ;						(* font style *)
		fields : мнвоНаБитахМЗ;						(* What should be changed? *)
	кон ChangeInfo;

тип

	KillerMsg = окласс
	кон KillerMsg;

	Window* = окласс (WMComponents.FormWindow)
	перем
		bold, lock, comment, stupid, assert, preferred, debug, normal, incSize, decSize, get, apply: WMStandardComponents.Button;
		famEdit, sizeEdit, styleEdit, colorEdit, bgColEdit: WMEditors.TextField;
		famCheck, sizeCheck, styleCheck, colorCheck, bgColCheck: WMStandardComponents.Checkbox;
		styleB, colB, bgColB : WMStandardComponents.Button;
		popup : WMPopups.Popup;

		проц CreateForm(): WMComponents.VisualComponent;
		перем
			label : WMStandardComponents.Label;
			panel : WMStandardComponents.Panel;
			toolbar: WMStandardComponents.Panel;
			manager : WM.WindowManager;
			windowStyle : WM.WindowStyle;

			проц AB(panel : WMStandardComponents.Panel; btn: WMStandardComponents.Button);
			нач
				btn.alignment.Set(WMComponents.AlignLeft);
				btn.fillColor.Set(0FFFFFFFFH);
				btn.clDefault.Set(windowStyle.bgColor);
				btn.clTextDefault.Set(WMGraphics.Black);
				btn.bounds.SetWidth(WindowWidth DIV 2); panel.AddContent(btn)
			кон AB;

			проц AL(panel : WMStandardComponents.Panel; lbl : WMStandardComponents.Label);
			нач
				lbl.alignment.Set(WMComponents.AlignLeft); lbl.bounds.SetWidth(31); label.textColor.Set(0000000FFH);
				panel.AddContent(lbl)
			кон AL;

			проц AC(panel : WMStandardComponents.Panel; chk : WMStandardComponents.Checkbox);
			нач
				chk.bounds.SetWidth(16); chk.state.Set(1); chk.bearing.Set(WMRectangles.MakeRect(2, 2, 2, 2));
				chk.alignment.Set(WMComponents.AlignRight); chk.bounds.SetWidth(20);
				panel.AddContent(chk)
			кон AC;

			проц AE(panel : WMStandardComponents.Panel; edtr : WMEditors.TextField);
			нач
				edtr.alignment.Set(WMComponents.AlignClient); edtr.fillColor.Set(0FFFFFF88H);
				panel.AddContent(edtr)
			кон AE;

			проц AD(panel : WMStandardComponents.Panel; btn : WMStandardComponents.Button);
			нач
				btn.alignment.Set(WMComponents.AlignRight); btn.bounds.SetWidth(17); panel.AddContent(btn)
			кон AD;

		нач
			manager := WM.GetDefaultManager();
			windowStyle := manager.GetStyle();
			если (windowStyle.bgColor = 0) то windowStyle.bgColor := WMGraphics.White; всё;

			нов(panel); panel.bounds.SetExtents(WindowWidth, WindowHeight); panel.takesFocus.Set(истина);

			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);
			нов(bold); bold.caption.SetAOC("Bold"); AB(toolbar, bold);

			нов(lock); lock.caption.SetAOC("Lock"); AB(toolbar, lock);

			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);
			нов(comment); comment.caption.SetAOC("Comment"); AB(toolbar, comment);
			нов(debug); debug.caption.SetAOC("Debug"); AB(toolbar, debug);

			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);
			нов(stupid); stupid.caption.SetAOC("Stupid"); AB(toolbar, stupid);
			нов(assert); assert.caption.SetAOC("Assert"); AB(toolbar, assert);

			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);
			нов(preferred); preferred.caption.SetAOC("Preferred"); AB(toolbar, preferred);
			нов(normal); normal.caption.SetAOC("Normal"); AB(toolbar, normal);

			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);
			нов(incSize); incSize.caption.SetAOC("Inc Size"); AB(toolbar, incSize);
			нов(decSize); decSize.caption.SetAOC("Dec Size"); AB(toolbar, decSize);

			(* Get/Apply *)
			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);
			нов(get); get.caption.SetAOC("Get"); AB(toolbar, get);
			get.clDefault.Set(088000088H); get.clTextDefault.Set(WMGraphics.White);
			нов(apply); apply.caption.SetAOC("Apply"); AB(toolbar, apply);
			apply.clDefault.Set(088000088H); apply.clTextDefault.Set(WMGraphics.White);

			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			toolbar.fillColor.Set(windowStyle.bgColor);
			panel.AddContent(toolbar);
			нов(label); label.caption.SetAOC("Font:"); AL(toolbar, label);
			нов(famCheck); AC(toolbar, famCheck);
			нов(famEdit); famEdit.SetAsString(Texts.defaultAttributes.fontInfo.name); AE(toolbar, famEdit);

			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			toolbar.fillColor.Set(windowStyle.bgColor);
			panel.AddContent(toolbar);
			нов(label); label.caption.SetAOC("Size:"); AL(toolbar, label);
			нов(sizeCheck); AC(toolbar, sizeCheck);
			нов(sizeEdit); sizeEdit.SetAsString("14"); AE(toolbar, sizeEdit);

			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			toolbar.fillColor.Set(windowStyle.bgColor);
			panel.AddContent(toolbar);
			нов(label); label.caption.SetAOC("Style:"); AL(toolbar, label);
			нов(styleCheck); AC(toolbar, styleCheck);
			нов(styleB); styleB.caption.SetAOC("+"); AD(toolbar, styleB);
			нов(styleEdit); styleEdit.SetAsString("Regular"); AE(toolbar, styleEdit);

			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			toolbar.fillColor.Set(windowStyle.bgColor);
			panel.AddContent(toolbar);
			нов(label); label.caption.SetAOC("Color:"); AL(toolbar, label);
			нов(colorCheck); AC(toolbar, colorCheck);
			нов(colB); colB.caption.SetAOC("+"); AD(toolbar, colB);
			нов(colorEdit); colorEdit.SetAsString("000000FF"); colorEdit.onChanged.Add(UpdateColors);
			AE(toolbar, colorEdit);

			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			toolbar.fillColor.Set(windowStyle.bgColor);
			panel.AddContent(toolbar);
			нов(label); label.caption.SetAOC("BCol:"); AL(toolbar, label);
			нов(bgColCheck); AC(toolbar, bgColCheck);
			нов(bgColB); bgColB.caption.SetAOC("+"); AD(toolbar, bgColB);
			нов(bgColEdit); bgColEdit.SetAsString("00000000"); bgColEdit.onChanged.Add(UpdateColors);
			AE(toolbar, bgColEdit);

			UpdateColors(НУЛЬ, НУЛЬ);

			возврат panel
		кон CreateForm;

		проц &New*(c : WMRestorable.Context);
		перем vc : WMComponents.VisualComponent;
		нач
			scaling := истина;
			IncCount;
			vc := CreateForm();
			bold.onClick.Add(SetStyle);
			lock.onClick.Add(SetStyle);
			comment.onClick.Add(SetStyle);
			debug.onClick.Add(SetStyle);
			stupid.onClick.Add(SetStyle);
			assert.onClick.Add(SetStyle);
			preferred.onClick.Add(SetStyle);
			normal.onClick.Add(SetStyle);
			incSize.onClick.Add(SetStyle);
			decSize.onClick.Add(SetStyle);
			get.onClick.Add(GetStyle);
			apply.onClick.Add(SetCustomStyle);
			styleB.SetExtPointerDownHandler(StyleDrop);
			colB.SetExtPointerDownHandler(ColorHandler);
			bgColB.SetExtPointerDownHandler(BGColorHandler);

			Init(vc.bounds.GetWidth(), vc.bounds.GetHeight(), ложь);
			SetContent(vc);
			SetTitle(Строки8.ЯвиУСтроку("Text Styles"));
			SetIcon(WMGraphics.LoadImage("WMIcons.tar://WMTextTool.png", истина));

			если c # НУЛЬ то
				если c.appData # НУЛЬ то
					DisableUpdate;
					LoadData(c.appData(XML.Element));
					EnableUpdate;
				всё;
				vc.Invalidate;
				WMRestorable.AddByContext(сам, c)
			иначе
				WM.ExtAddWindow(сам, 50, 120, {WM.FlagStayOnTop, WM.FlagFrame, WM.FlagClose, WM.FlagMinimize})
			всё;
		кон New;

		проц GetStyle(sender, data : динамическиТипизированныйУкль);
		перем
			text : Texts.Text; from, to : Texts.TextPosition;
			utilreader : Texts.TextReader; tempString : массив 256 из симв8;
			a, b: размерМЗ; ch : симв32;
		нач
			если Texts.GetLastSelection(text, from, to) то
				text.AcquireWrite;
				a := матМинимум(from.GetPosition(), to.GetPosition());
				b := матМаксимум(from.GetPosition(), to.GetPosition());
				нов(utilreader, text);
				utilreader.SetPosition(a);
				utilreader.ReadCh(ch);
				если utilreader.attributes = НУЛЬ то
					famEdit.SetAsString(Texts.defaultAttributes.fontInfo.name);
					Строки8.ПишиЦел64_вСтроку(Texts.defaultAttributes.fontInfo.size, tempString);
					sizeEdit.SetAsString(tempString);
					styleEdit.SetAsString("regular");
					Строки8.ПишиЦел64_16_ричноВСтроку(Texts.defaultAttributes.color, 7, tempString);
					colorEdit.SetAsString(tempString);
					Строки8.ПишиЦел64_16_ричноВСтроку(Texts.defaultAttributes.bgcolor, 7, tempString);
					bgColEdit.SetAsString(tempString);
				иначе
					famEdit.SetAsString(utilreader.attributes.fontInfo.name);
					Строки8.ПишиЦел64_вСтроку(utilreader.attributes.fontInfo.size, tempString);
					sizeEdit.SetAsString(tempString);
					если utilreader.attributes.fontInfo.style = {} то
						styleEdit.SetAsString("Regular");
					аесли utilreader.attributes.fontInfo.style = {0} то
						styleEdit.SetAsString("Bold");
					аесли utilreader.attributes.fontInfo.style = {1} то
						styleEdit.SetAsString("Italic");
					аесли utilreader.attributes.fontInfo.style = {0,1} то
						styleEdit.SetAsString("Bold Italic");
					иначе
						styleEdit.SetAsString("Regular");
					всё;
					Строки8.ПишиЦел64_16_ричноВСтроку(utilreader.attributes.color, 7, tempString);
					colorEdit.SetAsString(tempString);
					Строки8.ПишиЦел64_16_ричноВСтроку(utilreader.attributes.bgcolor, 7, tempString);
					bgColEdit.SetAsString(tempString);
				всё;
				text.ReleaseWrite
			всё;
		кон GetStyle;

		проц SetStyle(sender, data : динамическиТипизированныйУкль);
		перем changeInfo : ChangeInfo;
		нач
			нов(changeInfo);
			если sender = bold то
				changeInfo.style := {WMGraphics.FontBold};
				changeInfo.fgColor := WMGraphics.RGBAToColor(0, 0, 0, 0FFH);
				changeInfo.fields := ChangeStyle + ChangeFgColor;
			аесли sender = lock то
				changeInfo.style := {};
				changeInfo.fgColor := WMGraphics.RGBAToColor(0FFH, 0, 0FFH, 0FFH);
				changeInfo.fields := ChangeStyle + ChangeFgColor;
			аесли sender = preferred то
				changeInfo.style := {WMGraphics.FontBold};
				changeInfo.fgColor := WMGraphics.RGBAToColor(0FFH, 0, 0FFH, 0FFH);
				changeInfo.fields := ChangeStyle + ChangeFgColor;
			аесли sender = assert то
				changeInfo.style := {WMGraphics.FontBold};
				changeInfo.fgColor := WMGraphics.RGBAToColor(0, 0, 0FFH, 0FFH);
				changeInfo.fields := ChangeStyle + ChangeFgColor;
			аесли sender = comment то
				changeInfo.style := {};
				changeInfo.fgColor := WMGraphics.RGBAToColor(80H, 80H, 080H, 0FFH);
				changeInfo.fields := ChangeStyle + ChangeFgColor;
			аесли sender = debug то
				changeInfo.style := {};
				changeInfo.fgColor := WMGraphics.RGBAToColor(0H, 0H, 0FFH, 0FFH);
				changeInfo.fields := ChangeStyle + ChangeFgColor;
			аесли sender = stupid то
				changeInfo.style := {};
				changeInfo.fgColor := WMGraphics.RGBAToColor(0FFH, 0H, 0H, 0FFH);
				changeInfo.fields := ChangeStyle + ChangeFgColor;
			аесли sender = normal то
				changeInfo.style := {};
				changeInfo.fgColor := WMGraphics.RGBAToColor(0H, 0H, 0H, 0FFH);
				changeInfo.fields := ChangeStyle + ChangeFgColor;
			аесли sender = incSize то
				changeInfo.deltaSize := 1;
				changeInfo.deltaSizeMode := IncrementBy;
				changeInfo.fields := ChangeSize;
			аесли sender = decSize то
				changeInfo.deltaSize := 1;
				changeInfo.deltaSizeMode := DecrementBy;
				changeInfo.fields := ChangeSize;
			всё;
			ApplyChange(changeInfo);
		кон SetStyle;

		проц SetCustomStyle(sender, data: динамическиТипизированныйУкль);
		перем
			changeInfo : ChangeInfo;
			string: массив 32 из симв8;
			res : целМЗ;
		нач
			нов(changeInfo);
			если (famCheck.state.Get() = 1) то
				famEdit.GetAsString(string); копируйСтрокуДо0(string, changeInfo.name);
				changeInfo.fields := changeInfo.fields + ChangeFont;
			всё;
			если (sizeCheck.state.Get() = 1) то
				sizeEdit.GetAsString(string); Строки8.ПрочтиЦел32_изСтроки(string, changeInfo.deltaSize);  changeInfo.deltaSizeMode := Absolute;
				changeInfo.fields := changeInfo.fields + ChangeSize;
			всё;
			если (styleCheck.state.Get() = 1) то
				styleEdit.GetAsString(string); Строки8.СтрокуВНижнийРегистрASCII(string);
				если (string = "0") или (string = "regular") то	changeInfo.style := 	{};
				аесли (string = "1") или (string = "bold") то changeInfo.style := {0};
				аесли (string = "2") или (string = "italic") то changeInfo.style := {1};
				аесли (string = "3") или (string = "bold italic") то changeInfo.style := {0,1};
				иначе changeInfo.style := {};
				всё;
				changeInfo.fields := changeInfo.fields + ChangeStyle;
			всё;
			если (colorCheck.state.Get() = 1) то
				colorEdit.GetAsString(string); Строки8.ПрочтиЦел32_16_ричноИзСтроки(string, changeInfo.fgColor, res);
				changeInfo.fields := changeInfo.fields + ChangeFgColor;
			всё;
			если (bgColCheck.state.Get() = 1) то
				bgColEdit.GetAsString(string); Строки8.ПрочтиЦел32_16_ричноИзСтроки(string, changeInfo.bgColor, res);
				changeInfo.fields := changeInfo.fields + ChangeBgColor;
			всё;
			ApplyChange(changeInfo);
		кон SetCustomStyle;

		проц StyleDrop(x, y : размерМЗ; keys : мнвоНаБитахМЗ; перем handled : булево);
		нач
			нов(popup);
			popup.Add("Regular", StylePopupHandler);
			popup.Add("Bold", StylePopupHandler);
			popup.Add("Italic", StylePopupHandler);
			popup.Add("Bold Italic", StylePopupHandler);
			handled := истина;

			popup.Popup(bounds.r-120, bounds.t+180);
		кон StyleDrop;

		проц StylePopupHandler(sender, data: динамическиТипизированныйУкль);
		перем button: WMStandardComponents.Button;
			tempString: Строки8.уСтрока;
		нач
			popup.Close;
			если sender суть WMStandardComponents.Button то
				button := sender(WMStandardComponents.Button);
				tempString := button.caption.Get();
				если (tempString^ = "Regular") то
					styleEdit.SetAsString("Regular");
				аесли (tempString^ = "Bold") то
					styleEdit.SetAsString("Bold");
				аесли (tempString^ = "Italic") то
					styleEdit.SetAsString("Italic");
				аесли (tempString^ = "Bold Italic") то
					styleEdit.SetAsString("Bold Italic");
				иначе
					styleEdit.SetAsString("Regular");
				всё;
			всё;
		кон StylePopupHandler;

		проц ColorHandler(x, y : размерМЗ; keys : мнвоНаБитахМЗ; перем handled : булево);
		перем colorPanel : WMPopups.ColorSwatchPopup;
		нач
			нов(colorPanel);
			colorPanel.onColorChosen := ColorPopupHandler;
			colorPanel.Popup(bounds.r-190, bounds.t+200);

			handled := истина;
		кон ColorHandler;

		проц ColorPopupHandler(result: WMGraphics.Color);
		перем
			colorString: массив 16 из симв8;
		нач
			Строки8.ПишиЦел64_16_ричноВСтроку(result, 7, colorString);
			colorEdit.SetAsString(colorString);
			colB.clDefault.Set(result);
		кон ColorPopupHandler;

		проц BGColorHandler(x, y : размерМЗ; keys : мнвоНаБитахМЗ; перем handled : булево);
		перем colorPanel: WMPopups.ColorSwatchPopup;
		нач
			нов(colorPanel);
			colorPanel.onColorChosen := BGColorPopupHandler;
			colorPanel.Popup(bounds.r-190, bounds.t+220);

			handled := истина;
		кон BGColorHandler;

		проц BGColorPopupHandler(result: WMGraphics.Color);
		перем
			colorString: массив 16 из симв8;
		нач
			Строки8.ПишиЦел64_16_ричноВСтроку(result, 7, colorString);
			bgColEdit.SetAsString(colorString);
			bgColB.clDefault.Set(result);
		кон BGColorPopupHandler;

		проц UpdateColors(sender, data : динамическиТипизированныйУкль);
		перем colorString : массив 16 из симв8; caption : массив 2 из симв8;  color: цел32; res: целМЗ;
		нач
			colorEdit.GetAsString(colorString);
			Строки8.ПрочтиЦел32_16_ричноИзСтроки(colorString, color, res);
			если (res = Строки8.Успех) то caption := "+"; иначе caption := "E"; всё;
			colB.caption.SetAOC(caption);
			colB.clDefault.Set(color);
			bgColEdit.GetAsString(colorString);
			Строки8.ПрочтиЦел32_16_ричноИзСтроки(colorString, color, res);
			если (res = Строки8.Успех) то caption := "+"; иначе caption := "E"; всё;
			bgColB.caption.SetAOC(caption);
			bgColB.clDefault.Set(color);
		кон UpdateColors;

		проц {перекрыта}Close*;
		нач
			Close^;
			colorEdit.onChanged.Remove(UpdateColors);
			bgColEdit.onChanged.Remove(UpdateColors);
			DecCount;
		кон Close;

		проц LoadData(elem: XML.Element);
		перем i: цел32; str: массив 128 из симв8;
		нач
			WMRestorable.LoadLongint(elem, "famCheck", i); famCheck.state.Set(i);
			WMRestorable.LoadLongint(elem, "sizeCheck", i); sizeCheck.state.Set(i);
			WMRestorable.LoadLongint(elem, "styleCheck", i); styleCheck.state.Set(i);
			WMRestorable.LoadLongint(elem, "colorCheck", i); colorCheck.state.Set(i);
			WMRestorable.LoadLongint(elem, "bgColCheck", i); bgColCheck.state.Set(i);
			WMRestorable.LoadString(elem,"famEdit", str); famEdit.SetAsString(str);
			WMRestorable.LoadString(elem,"sizeEdit", str); sizeEdit.SetAsString(str);
			WMRestorable.LoadString(elem,"styleEdit", str); styleEdit.SetAsString(str);
			WMRestorable.LoadString(elem,"colorEdit", str); colorEdit.SetAsString(str);
			WMRestorable.LoadString(elem,"bgColEdit", str); bgColEdit.SetAsString(str);
		кон LoadData;


		проц StoreData(): XML.Element;
		перем elem: XML.Element; string: массив 128 из симв8;
		нач
			нов(elem); elem.SetName("Style");
			WMRestorable.StoreLongint(elem, "famCheck", famCheck.state.Get());
			famEdit.GetAsString(string);
			WMRestorable.StoreString(elem, "famEdit", string);

			WMRestorable.StoreLongint(elem, "sizeCheck", sizeCheck.state.Get());
			sizeEdit.GetAsString(string);
			WMRestorable.StoreString(elem, "sizeEdit", string);

			WMRestorable.StoreLongint(elem, "styleCheck", styleCheck.state.Get());
			styleEdit.GetAsString(string);
			WMRestorable.StoreString(elem, "styleEdit", string);

			WMRestorable.StoreLongint(elem, "colorCheck", colorCheck.state.Get());
			colorEdit.GetAsString(string);
			WMRestorable.StoreString(elem, "colorEdit", string);

			WMRestorable.StoreLongint(elem, "bgColCheck", bgColCheck.state.Get());
			bgColEdit.GetAsString(string);
			WMRestorable.StoreString(elem, "bgColEdit", string);
			возврат elem;
		кон StoreData;


		проц {перекрыта}Handle*(перем x: WMMessages.Message);
		перем data: XML.Element;
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) то
				если (x.ext суть KillerMsg) то Close
				аесли (x.ext суть WMRestorable.Storage) то
					data := StoreData();
					x.ext(WMRestorable.Storage).Add("WMTextTool", "WMTextTool.Restore", сам, data)
				иначе Handle^(x)
				всё
			иначе Handle^(x)
			всё
		кон Handle;

	кон Window;

перем
	nofWindows : цел32;

(* Actually, this is a hack... but for now, do it.  *)
проц GetNewSize(конст fontname : массив из симв8; mode, value : цел32; currentSize : размерМЗ; перем newSize : размерМЗ);
нач
	утв((mode = Absolute) или (mode = IncrementBy) или (mode = DecrementBy));
	если (mode = Absolute) то
		newSize := value;
	иначе
		если (fontname = "Oberon") то
			если (mode = IncrementBy) то
				если (currentSize = 8) то newSize := 10;
				аесли (currentSize = 10) то newSize := 12;
				аесли (currentSize = 12) то newSize := 14;
				аесли (currentSize = 14) то newSize := 16;
				аесли (currentSize = 16) то newSize := 20;
				аесли (currentSize = 20) то newSize := 24;
				аесли (currentSize = 24) то newSize := 24;
				иначе (* go to default *)
					newSize := 12; (* max. size of Oberon font *)
				всё;
			иначе
				если (currentSize = 8) то newSize := 8;
				аесли (currentSize = 10) то newSize := 8;
				аесли (currentSize = 12) то newSize := 10;
				аесли (currentSize = 14) то newSize := 12;
				аесли (currentSize = 16) то newSize := 14;
				аесли (currentSize = 20) то newSize := 16;
				аесли (currentSize = 24) то newSize := 20;
				иначе
					newSize := 12;
				всё;
			всё;
		аесли (fontname = "Courier") то
			если (mode = IncrementBy) то
				если (currentSize = 10) то newSize := 12;
				иначе
					newSize := 12;
				всё;
			иначе
				если (currentSize = 12) то newSize := 10;
				иначе
					newSize := 12;
				всё;
			всё;
		иначе
			если (mode = IncrementBy) то newSize := currentSize + value; иначе newSize := currentSize - value; всё;
		всё;
	всё;
	если (newSize < 8) то newSize := 8; всё;
кон GetNewSize;

проц EnsureAttribute(перем attr : Texts.Attributes);
нач
	если (attr = НУЛЬ) то
		attr := Texts.defaultAttributes.Clone();
	всё
кон EnsureAttribute;

проц ChangeAttribute(перем attr : Texts.Attributes; userData : динамическиТипизированныйУкль);
перем changeInfo : ChangeInfo;
нач
	если (userData # НУЛЬ) и (userData суть ChangeInfo) то
		changeInfo := userData (ChangeInfo);
		EnsureAttribute(attr);

		если (changeInfo.fields * ChangeFont # {}) то (* font change *)
			копируйСтрокуДо0(changeInfo.name, attr.fontInfo.name);
		всё;

		если (changeInfo.fields * ChangeSize # {}) то (* font size change *)
			GetNewSize(attr.fontInfo.name, changeInfo.deltaSizeMode, changeInfo.deltaSize, attr.fontInfo.size, attr.fontInfo.size);
		всё;

		если (changeInfo.fields * ChangeFgColor # {}) то attr.color := changeInfo.fgColor; всё;
		если (changeInfo.fields * ChangeBgColor # {}) то attr.bgcolor := changeInfo.bgColor; всё;
		если (changeInfo.fields * ChangeStyle # {}) то attr.fontInfo.style := changeInfo.style; всё;

		attr.fontInfo.fontcache := НУЛЬ;
	всё;
кон ChangeAttribute;

(* Apply text formatting changes described by <changeInfo> to the currently selected text *)
проц ApplyChange(changeInfo : ChangeInfo);
перем
	text : Texts.Text;
	from, to : Texts.TextPosition;
	utilreader : Texts.TextReader;
	a, b : размерМЗ;
	ch : Texts.Char32;
нач
	утв(changeInfo # НУЛЬ);
	если Texts.GetLastSelection(text, from, to) то
		text.AcquireWrite;
		a := матМинимум(from.GetPosition(), to.GetPosition());
		b := матМаксимум(from.GetPosition(), to.GetPosition());

		нов(utilreader, text);
		utilreader.SetPosition(a);
		utilreader.ReadCh(ch);

		text.UpdateAttributes(a, b - a, ChangeAttribute, changeInfo);
		text.ReleaseWrite;
	всё;
кон ApplyChange;

(* Set the font size of the currently selected text either relativ or absolute *)
проц SetFontSize*(context : Commands.Context); (** ("Absolute"|"IncrementBy" |"DecrementBy") [value] ~*)
перем changeInfo : ChangeInfo; modeStr : массив 16 из симв8; mode, value : цел32;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(modeStr);
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(value, ложь);
	Строки8.СтрокуВВерхнийРегистрASCII(modeStr);
	если (modeStr = "ABSOLUTE") то mode := Absolute;
	аесли (modeStr = "INCREMENTBY") то mode := IncrementBy;
	аесли (modeStr = "DECREMENTBY") то mode := DecrementBy;
	иначе
		context.error.пСтроку8("WMTextStyleTool.SetFontSize: Unknown mode parameter"); context.error.пВК_ПС;
		возврат;
	всё;
	нов(changeInfo);
	changeInfo.fields := ChangeSize;
	changeInfo.deltaSizeMode := mode;
	changeInfo.deltaSize := value;
	если (mode # Absolute) и (value = 0) то changeInfo.deltaSize := 1; (* default increment/ decrement *) всё;
	ApplyChange(changeInfo);
кон SetFontSize;

(** Set the font style of the currently selected text. Default: Normal  *)
проц SetFontStyle*(context : Commands.Context);
перем styleStr : массив 16 из симв8; style : мнвоНаБитахМЗ; changeInfo : ChangeInfo;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(styleStr);
	Строки8.СтрокуВВерхнийРегистрASCII(styleStr);
	если (styleStr = "BOLD") то style := {WMGraphics.FontBold};
	аесли (styleStr = "ITALIC") то style := {WMGraphics.FontItalic};
	аесли (styleStr = "NORMAL") или (styleStr = "") то style := {};
	иначе
		context.error.пСтроку8("WMTextStyleTool.SetFontStyle: Unknown font style parameter."); context.error.пВК_ПС;
		возврат;
	всё;
	нов(changeInfo);
	changeInfo.fields := ChangeStyle;
	changeInfo.style := style;
	ApplyChange(changeInfo);
кон SetFontStyle;

(** Set the font color of the currently selected text. If not parameter is specified, fgColor is black, bgColor is unchanged *)
проц SetFontColor*(context : Commands.Context); (** [fgColor] [bgColor] ~ *)
перем fgColor, bgColor : цел32; changeInfo : ChangeInfo;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(fgColor, истина);
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(bgColor, истина);
	нов(changeInfo);
	changeInfo.fields := ChangeFgColor;
	changeInfo.fgColor := fgColor;
	если (context.arg.кодВозвратаПоследнейОперации = Потоки.Успех) то
		changeInfo.bgColor := bgColor;
		changeInfo.fields := changeInfo.fields + ChangeBgColor;
	всё;
	ApplyChange(changeInfo);
кон SetFontColor;

(** Set the font for the currently selected text. Default: default font name *)
проц SetFontName*(context : Commands.Context); (** [fontname] ~ *)
перем name : массив 128 из симв8; changeInfo : ChangeInfo;
нач
	если ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name) то копируйСтрокуДо0(Texts.defaultAttributes.fontInfo.name, name ); всё;
	нов(changeInfo);
	копируйСтрокуДо0(name, changeInfo.name);
	changeInfo.fields := ChangeFont;
	ApplyChange(changeInfo);
кон SetFontName;

проц CountWords*(context : Commands.Context);
перем wordCount : цел32; ch : симв8;

	проц SkipWord(r : Потоки.Чтец);
	перем ch : симв8;
	нач
		нцДо
			ch := r.чИДайСимв8();
		кцПри (r.кодВозвратаПоследнейОперации # Потоки.Успех) или (кодСимв8(ch) <= 32);
	кон SkipWord;

нач
	wordCount := 0;
	context.arg.ПропустиБелоеПоле;
	ch := context.arg.чИДайСимв8();
	нцПока (ch # 0X) и (context.arg.кодВозвратаПоследнейОперации = Потоки.Успех) делай
		увел(wordCount);
		SkipWord(context.arg);
		context.arg.ПропустиБелоеПоле;
		ch := context.arg.ПодглядиСимв8();
	кц;
	context.out.пСтроку8("Number of words: "); context.out.пЦел64(wordCount, 0); context.out.пВК_ПС;
кон CountWords;

проц CountLines*(context : Commands.Context);
перем nofLines : цел32; ch : симв8;
нач
	nofLines := 1;
	нцДо
		ch := context.arg.чИДайСимв8();
		если (ch = LF) то увел(nofLines); всё;
	кцПри (context.arg.кодВозвратаПоследнейОперации # Потоки.Успех);
	context.out.пСтроку8("Number of lines: "); context.out.пЦел64(nofLines, 0); context.out.пВК_ПС;
кон CountLines;

проц CountCharacters*(context : Commands.Context);
перем nofCharacters : цел32; ch : симв8;
нач
	nofCharacters := 0;
	нцДо
		ch := context.arg.чИДайСимв8();
		если (ch # 0X) то увел(nofCharacters); всё;
	кцПри (context.arg.кодВозвратаПоследнейОперации # Потоки.Успех);
	context.out.пСтроку8("Number of characters: "); context.out.пЦел64(nofCharacters, 0); context.out.пВК_ПС;
кон CountCharacters;

проц CountAll*(context : Commands.Context);
нач
	CountCharacters(context);
	context.arg.ПерейдиКМестуВПотоке(0); CountWords(context);
	context.arg.ПерейдиКМестуВПотоке(0); CountLines(context);
кон CountAll;

проц Open*;
перем winstance : Window;
нач
	нов(winstance, НУЛЬ);
кон Open;

проц Restore*(context : WMRestorable.Context);
перем w : Window;
нач
	нов(w, context)
кон Restore;

проц IncCount;
нач {единолично}
	увел(nofWindows)
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows)
кон DecCount;

проц Cleanup;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WM.WindowManager;
нач {единолично}
	нов(die);
	msg.ext := die;
	msg.msgType := WMMessages.MsgExt;
	m := WM.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0)
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон WMTextTool.

System.Free WMTextTool ~

WMTextTool.Open  ~

WMTextTool.SetFontSize Absolute 20 ~	WMTextTool.SetFontSize Absolute 12 ~

WMTextTool.SetFontStyle normal ~	WMTextTool.SetFontStyle bold ~

WMTextTool.SetFontName Courier ~		WMTextTool.SetFontName Oberon ~

WMTextTool.SetFontColor 0FF0000FFH ~	WMTextTool.SetFontColor 0FFH ~

WMTextTool.CountLines ^ ~

WMTextTool.CountWords ^ ~

WMTextTool.CountCharacters ^ ~

WMTextTool.CountAll ^ ~

(**
	AUTHOR: "Alexey Morozov";
	PURPOSE: "ActiveCells AXI-4 Stream input/output interface for Xilinx Zynq platform";
*)
модуль Channels;

использует
	НИЗКОУР;

конст
	MasterAxiGp0Base = 07F000000H; (* base address for Master AXI General Purpose interface 0 *)
	MasterAxiGp1Base = 0BF000000H; (* base address for Master AXI General Purpose interface 1 *)

	ChanOffset  = 256*4; (* ActiveCells AXI4 Stream channel offset in bytes *)
(*
	OutDataOffset = 0;
	OutReadyOffset = SIZEOF(SIGNED32);

	InpAvailableOffset = 0;
	InpDataOffset = 2*SIZEOF(SIGNED32);
*)

тип
	(* AXI4 Stream logical port descriptor *)
	PortDesc = запись
		lockAddr: адресВПамяти; (** address of the associated spin lock *)
		portAddr: адресВПамяти; (** address of the logical channel port;  *)
	кон;

	Output* = портОбменаДанными дляВывода; (** AXI4 Stream output port *)
	Input* = портОбменаДанными в; (** AXI4 Stream input port *)

перем
	inputs: массив 2 из массив 256 из PortDesc;
	outputs: массив 2 из массив 256 из PortDesc;

	locks: массив 2 из цел32; (* locks for each physical channel *)

(**
	Writing to and reading from a port can be described by the following high-level code:

	PROCEDURE WriteToPort(out: Output; data: SIGNED32);
	VAR
		portDesc: POINTER {UNSAFE} TO PortDesc;
		outDesc: OutputDesc;
	BEGIN
		portDesc := SYSTEM.VAL(ADDRESS,out);
		outDesc := portDesc.portAddr;
		outDesc.data := data;
	END WriteToPort;

	PROCEDURE ReadFromPort(inp: Input; VAR data: SIGNED32);
	VAR
		portDesc: POINTER {UNSAFE} TO PortDesc;
		inpDesc: InputDesc;
	BEGIN
		portDesc := SYSTEM.VAL(ADDRESS,inp);
		inpDesc := portDesc.portAddr;
		data := inpDesc.data;
	END ReadFromPort;

	where OutputDesc and InputDesc are defined as follows:

	(** AXI4 Stream output port descriptor *)
	OutputDesc* = POINTER{UNSAFE,UNTRACED} TO RECORD
		data: SIGNED32; (** output data, write only *)
		ready: BOOLEAN; (** TRUE when output is available for sending data *)
		padding: ARRAY 3 OF CHAR;
	END;

	(** AXI4 Stream input port descriptor *)
	InputDesc* = POINTER{UNSAFE,UNTRACED} TO RECORD
		available: BOOLEAN; (** TRUE when input data is available *)
		padding: ARRAY 7 OF CHAR;
		data: SIGNED32; (** input data, read only *)
	END;
*)

	(**
		Get AXI4 Stream input port

		physicalPortNum: physical port number
		logicalPortNum: logical port number
	*)
	проц GetInput*(physicalPortNum, logicalPortNum: цел32; перем port: Input): булево;
	перем
		portDesc: укль {опасныйДоступКПамяти} на PortDesc;
		portAddr: адресВПамяти;
	нач
		просей physicalPortNum из
			|0: portAddr := MasterAxiGp0Base + logicalPortNum*ChanOffset;
			|1: portAddr := MasterAxiGp1Base + logicalPortNum*ChanOffset;
		иначе
			возврат ложь;
		всё;

		portDesc := адресОт(inputs[physicalPortNum,logicalPortNum]);
		portDesc.portAddr := portAddr;
		portDesc.lockAddr := адресОт(locks[physicalPortNum]);
		port := НИЗКОУР.подмениТипЗначения(Input,portDesc);

		возврат истина;
	кон GetInput;

	(**
		Get AXI4 Stream output port

		physicalPortNum: physical port number
		logicalPortNum: logical port number
	*)
	проц GetOutput*(physicalPortNum, logicalPortNum: цел32; перем port: Output): булево;
	перем
		portDesc: укль {опасныйДоступКПамяти} на PortDesc;
		portAddr: адресВПамяти;
	нач
		просей physicalPortNum из
			|0: portAddr := MasterAxiGp0Base + logicalPortNum*ChanOffset;
			|1: portAddr := MasterAxiGp1Base + logicalPortNum*ChanOffset;
		иначе
			возврат ложь;
		всё;

		portDesc := адресОт(outputs[physicalPortNum,logicalPortNum]);
		portDesc.portAddr := portAddr;
		portDesc.lockAddr := адресОт(locks[physicalPortNum]);
		port := НИЗКОУР.подмениТипЗначения(Output,portDesc);

		возврат истина;
	кон GetOutput;

	проц GetOutputDataAddr*(out: Output): адресВПамяти;
	перем portDesc: укль {опасныйДоступКПамяти} на PortDesc;
	нач
		portDesc := НИЗКОУР.подмениТипЗначения(адресВПамяти,out);
		утв(portDesc # НУЛЬ);
		возврат portDesc.portAddr;
	кон GetOutputDataAddr;

	проц GetInputDataAddr*(inp: Input): адресВПамяти;
	перем portDesc: укль {опасныйДоступКПамяти} на PortDesc;
	нач
		portDesc := НИЗКОУР.подмениТипЗначения(адресВПамяти,inp);
		утв(portDesc # НУЛЬ);
		возврат portDesc.portAddr+8;
	кон GetInputDataAddr;

	(*
		Acquire exclusive access to a resource

		R1: lock address

		R0, R5 are used in addition

		Based on the code presented in "Barrier Litmus Tests and Cookbook" by Richard Grisenthwaite, ARM, 26.11.2009,
	*)
	проц {NOPAF} -AcquireResource;
	машКод
		MOV R0, #1
	Loop:
		LDREX R5, R1 ; read lock
		CMP R5, #0 ; check if 0
		WFENE ; sleep if the lock is held

		STREXEQ R5, R0, R1 ; attempt to store new value
		CMPEQ R5, #0 ; test if store suceeded
		BNE Loop ; retry if not

		DMB ; ensures that all subsequent accesses are observed after the gaining of the lock is observed

		; loads and stores in the critical region can now be performed
	кон AcquireResource;

	(*
		Release exclusive access to a resource

		R1: lock address

		R0 is used in addition

		Based on the code presented in "Barrier Litmus Tests and Cookbook" by Richard Grisenthwaite, ARM, 26.11.2009,
	*)
	проц {NOPAF} -ReleaseResource;
	машКод
		MOV R0, #0
		DMB ; ensure all previous accesses are observed before the lock is cleared
		STR R0, [R1, #0]
		; clear the lock.
		DSB ; ensure completion of the store that cleared the lock before sending the event
		SEV
	кон ReleaseResource;

	(*PROCEDURE AcquireObject(lockAddr: ADDRESS);
	BEGIN
		CODE
			LDR R1, [FP, #lockAddr] ; R1 := address of lock
		END;
		AcquireResource;
	END AcquireObject;

	PROCEDURE ReleaseObject(lockAddr: ADDRESS);
	BEGIN
		CODE
			LDR R1, [FP, #lockAddr] ; R1 := address of lock
		END;
		ReleaseResource
	END ReleaseObject;*)

	(**
		Returns TRUE if the given output port is ready to accept new data
	*)
	проц Ready*(out: Output): булево;
	перем b: булево;
	(*VAR p: POINTER {UNSAFE} TO PortDesc;*)
	нач
		машКод
			LDR R3, [FP, #out] ; R3 := address of PortDesc.lockAddr
			LDR R1, [R3, #0] ; R1 := PortDesc.lockAddr
		кон;
		AcquireResource;
		машКод
			LDR R3, [R3, #4] ; R3 := PortDesc.portAddr
			LDR R4, [R3, #4] ; R4 := out.Ready
			STRB R4, [FP, #b] ; b := R4
		кон;
		ReleaseResource;

		(*p := SYSTEM.VAL(ADDRESS,out);
		AcquireObject(p.lockAddr);
		b := SYSTEM.VAL(BOOLEAN,SYSTEM.GET32(p.portAddr+OutReadyOffset));
		ReleaseObject(p.lockAddr);*)

		возврат b;
	кон Ready;

	(** Returns number of data elements available to read from an input port *)
	проц Available*(inp: Input): цел32;
	перем available: цел32;
	(*VAR p: POINTER {UNSAFE} TO PortDesc;*)
	нач
		машКод
			LDR R3, [FP, #inp] ; R3 := address of PortDesc.lockAddr
			LDR R1, [R3, #0] ; R1 := PortDesc.lockAddr
		кон;
		AcquireResource;
		машКод
			LDR R3, [R3, #4] ; R3 := PortDesc.portAddr
			LDR R4, [R3, #0] ; R4 := inp.Available
			STR R4, [FP, #available] ; available := R4
		кон;
		ReleaseResource;

		(*p := SYSTEM.VAL(ADDRESS,inp);
		AcquireObject(p.lockAddr);
		available := SYSTEM.GET32(p.portAddr+InpAvailableOffset);
		ReleaseObject(p.lockAddr);*)

		возврат available;
	кон Available;

	(** Send data to an output port (blocking version) *)
	проц Send*(out: Output; x: цел32);
	(*VAR p: POINTER {UNSAFE} TO PortDesc;*)
	нач
		машКод
			LDR R3, [FP, #out] ; R3 := address of PortDesc.lockAddr
			LDR R1, [R3, #0] ; R1 := PortDesc.lockAddr
		кон;
		AcquireResource;
		машКод
			LDR R2, [FP, #x] ; R2 := x
			LDR R3, [R3, #4] ; R3 := PortDesc.portAddr
			STR R2, [R3, #0] ; out.Data := R2
		кон;
		ReleaseResource;

		(*p := SYSTEM.VAL(ADDRESS,out);
		AcquireObject(p.lockAddr);
		SYSTEM.PUT32(p.portAddr+OutDataOffset,x);
		ReleaseObject(p.lockAddr);*)
	кон Send;

	(** Send data to an output port (non-blocking version) *)
	проц SendNonBlocking*(out: Output; x: цел32): булево;
	перем b: булево;
	(*VAR p: POINTER {UNSAFE} TO PortDesc;*)
	нач
		машКод
			LDR R3, [FP, #out] ; R3 := address of PortDesc.lockAddr
			LDR R1, [R3, #0] ; R1 := PortDesc.lockAddr
		кон;
		AcquireResource;
		машКод
			LDR R2, [FP, #x] ; R2 := x
			LDR R3, [R3, #4] ; R3 := PortDesc.portAddr
			LDR R4, [R3, #4] ; R4 := out.Ready
			STRB R4, [FP, #b] ; b := R4

			CMP R4, #0
			BEQ Exit

			STR R2, [R3, #0] ; out.Data := R2
		Exit:
		кон;
		ReleaseResource;

		(*p := SYSTEM.VAL(ADDRESS,out);
		AcquireObject(p.lockAddr);
		b := SYSTEM.VAL(BOOLEAN,SYSTEM.GET32(p.portAddr+OutReadyOffset));
		IF b THEN SYSTEM.PUT32(p.portAddr+OutDataOffset,x); END;
		ReleaseObject(p.lockAddr);*)

		возврат b;
	кон SendNonBlocking;

	операция "<<"*(out: Output; x: цел32);
	нач
		Send(out,x);
	кон "<<";

	операция ">>"*(x: цел32; out: Output);
	нач
		Send(out,x);
	кон ">>";

	операция "<<?"*(out: Output; x: цел32): булево;
	нач
		возврат SendNonBlocking(out,x);
	кон "<<?";

	операция ">>?"*(x: цел32; out: Output): булево;
	нач
		возврат SendNonBlocking(out,x);
	кон ">>?";

	(** Receive data from an input port (blocking version) *)
	проц Receive*(inp: Input; перем x: цел32);
	(*VAR p: POINTER {UNSAFE} TO PortDesc;*)
	нач
		машКод
			LDR R3, [FP, #inp] ; R3 := address of PortDesc.lockAddr
			LDR R1, [R3, #0] ; R1 := PortDesc.lockAddr
		кон;
		AcquireResource;
		машКод
			LDR R2, [FP, #x] ; R2 := address of x
			LDR R3, [R3, #4] ; R3 := PortDesc.portAddr
			LDR R4, [R3, #8] ; R4 := inp.Data
			STR R4, [R2, #0] ; x := R4
		кон;
		ReleaseResource;

		(*p := SYSTEM.VAL(ADDRESS,inp);
		AcquireObject(p.lockAddr);
		x := SYSTEM.GET32(p.portAddr+InpDataOffset);
		ReleaseObject(p.lockAddr);*)
	кон Receive;

	(** Receive data from an input port (non-blocking version) *)
	проц ReceiveNonBlocking*(inp: Input; перем x: цел32): булево;
	перем b: булево;
	(*VAR p: POINTER {UNSAFE} TO PortDesc;*)
	нач
		машКод
			LDR R3, [FP, #inp] ; R3 := address of PortDesc.lockAddr
			LDR R1, [R3, #0] ; R1 := PortDesc.lockAddr
		кон;
		AcquireResource;
		машКод
			LDR R3, [R3, #4] ; R3 := PortDesc.portAddr
			LDR R4, [R3, #0] ; R4 := inp.Available
			STRB R4, [FP, #b] ; b := R4

			CMP R4, #0
			BEQ Exit

			LDR R2, [FP, #x] ; R2 := address of x
			LDR R4, [R3, #8] ; R4 := inp.Data
			STR R4, [R2, #0] ; x := R4
		Exit:
		кон;
		ReleaseResource;

		(*p := SYSTEM.VAL(ADDRESS,inp);
		AcquireObject(p.lockAddr);
		b := SYSTEM.VAL(BOOLEAN,SYSTEM.GET32(p.portAddr+InpAvailableOffset));
		IF b THEN x := SYSTEM.GET32(p.portAddr+InpDataOffset); END;
		ReleaseObject(p.lockAddr);*)

		возврат b;
	кон ReceiveNonBlocking;

	операция ">>"*(inp: Input; перем x: цел32);
	нач
		Receive(inp,x);
	кон ">>";

	операция "<<"*(перем x: цел32; inp: Input);
	нач
		Receive(inp,x);
	кон "<<";

	операция ">>?"*(inp: Input; перем x: цел32): булево;
	нач
		возврат ReceiveNonBlocking(inp,x);
	кон ">>?";

	операция "<<?"*(перем x: цел32; inp: Input): булево;
	нач
		возврат ReceiveNonBlocking(inp,x);
	кон "<<?";

	проц SendMultiple(portAddr, dataAddr: адресВПамяти; numElements: цел32);
	машКод
		LDR R0, [FP,#portAddr]
		LDR R1, [FP,#dataAddr]
		LDR R2, [FP,#numElements]

		CMP R2, #8
		BLT CheckLoop4

	Loop8: ; numElements >= 8, coalescing of 8 transfers
		LDR R3, [R1,#0]
		LDR R4, [R1,#4]
		LDR R5, [R1,#8]
		LDR R6, [R1,#12]
		LDR R7, [R1,#16]
		LDR R8, [R1,#20]
		LDR R9, [R1,#24]
		LDR R10, [R1,#28]

		STR R3, [R0,#0]
		STR R4, [R0,#0]
		STR R5, [R0,#0]
		STR R6, [R0,#0]
		STR R7, [R0,#0]
		STR R8, [R0,#0]
		STR R9, [R0,#0]
		STR R10, [R0,#0]

		ADD R1, R1, #32
		SUBS R2, R2, #8
		BGT Loop8

	CheckLoop4:
		CMP R2, #4
		BLT CheckLoop1

	Loop4: ; numElements >= 4, coalescing of 4 transfers
		LDR R3, [R1,#0]
		LDR R4, [R1,#4]
		LDR R5, [R1,#8]
		LDR R6, [R1,#12]

		STR R3, [R0,#0]
		STR R4, [R0,#0]
		STR R5, [R0,#0]
		STR R6, [R0,#0]

		ADD R1, R1, #16
		SUBS R2, R2, #4
		BGT Loop4

	CheckLoop1:
		CMP R2, #1
		BLT Exit

	Loop1: ; numElements >= 1, transfer element by element
		LDR R3, [R1,#0]
		STR R3, [R0,#0]

		ADD R1, R1, #4
		SUBS R2, R2, #1
		BGT Loop1

	Exit:

	кон SendMultiple;

	проц ReceiveMultiple(portAddr, dataAddr: адресВПамяти; numElements: цел32);
	машКод
		LDR R0, [FP,#portAddr]
		ADD R0, R0, #8
		LDR R1, [FP,#dataAddr]
		LDR R2, [FP,#numElements]

		CMP R2, #8
		BLT CheckLoop4

	Loop8: ; numElements >= 8, coalescing of 8 transfers
		LDR R3, [R0,#0]
		LDR R4, [R0,#0]
		LDR R5, [R0,#0]
		LDR R6, [R0,#0]
		LDR R7, [R0,#0]
		LDR R8, [R0,#0]
		LDR R9, [R0,#0]
		LDR R10, [R0,#0]

		STR R3, [R1,#0]
		STR R4, [R1,#4]
		STR R5, [R1,#8]
		STR R6, [R1,#12]
		STR R7, [R1,#16]
		STR R8, [R1,#20]
		STR R9, [R1,#24]
		STR R10, [R1,#28]

		ADD R1, R1, #32
		SUBS R2, R2, #8
		BGT Loop8

	CheckLoop4:
		CMP R2, #4
		BLT CheckLoop1

	Loop4: ; numElements >= 4, coalescing of 4 transfers
		LDR R3, [R0,#0]
		LDR R4, [R0,#0]
		LDR R5, [R0,#0]
		LDR R6, [R0,#0]

		STR R3, [R1,#0]
		STR R4, [R1,#4]
		STR R5, [R1,#8]
		STR R6, [R1,#12]

		ADD R1, R1, #16
		SUBS R2, R2, #4
		BGT Loop4

	CheckLoop1:
		CMP R2, #1
		BLT Exit

	Loop1: ; numElements >= 1, transfer element by element
		LDR R3, [R0,#0]
		STR R3, [R1,#0]

		ADD R1, R1, #4
		SUBS R2, R2, #1
		BGT Loop1

	Exit:

	кон ReceiveMultiple;

	операция "<<"*(port: Output; x: мнвоНаБитах32); нач Send(port,НИЗКОУР.подмениТипЗначения(цел32,x)); кон "<<";
	операция ">>"*(x: мнвоНаБитах32; port: Output); нач Send(port,НИЗКОУР.подмениТипЗначения(цел32,x)); кон ">>";
	операция "<<?"*(port: Output; x: мнвоНаБитах32): булево; нач возврат SendNonBlocking(port,НИЗКОУР.подмениТипЗначения(цел32,x)); кон "<<?";
	операция ">>?"*(x: мнвоНаБитах32; port: Output): булево; нач возврат SendNonBlocking(port,НИЗКОУР.подмениТипЗначения(цел32,x)); кон ">>?";
	операция ">>"*(port: Input; перем x: мнвоНаБитах32); нач Receive(port,НИЗКОУР.подмениТипЗначения(цел32,x)); кон ">>";
	операция "<<"*(перем x: мнвоНаБитах32; port: Input); нач Receive(port,НИЗКОУР.подмениТипЗначения(цел32,x)); кон "<<";
	операция ">>?"*(port: Input; перем x: мнвоНаБитах32): булево; нач возврат ReceiveNonBlocking(port,НИЗКОУР.подмениТипЗначения(цел32,x)); кон ">>?";
	операция "<<?"*(перем x: мнвоНаБитах32; port: Input): булево; нач возврат ReceiveNonBlocking(port,НИЗКОУР.подмениТипЗначения(цел32,x)); кон "<<?";

	операция "<<"*(port: Output; x: вещ32); нач Send(port,НИЗКОУР.подмениТипЗначения(цел32,x)); кон "<<";
	операция ">>"*(x: вещ32; port: Output); нач Send(port,НИЗКОУР.подмениТипЗначения(цел32,x)); кон ">>";
	операция "<<?"*(port: Output; x: вещ32): булево; нач возврат SendNonBlocking(port,НИЗКОУР.подмениТипЗначения(цел32,x)); кон "<<?";
	операция ">>?"*(x: вещ32; port: Output): булево; нач возврат SendNonBlocking(port,НИЗКОУР.подмениТипЗначения(цел32,x)); кон ">>?";
	операция ">>"*(port: Input; перем x: вещ32); нач Receive(port,НИЗКОУР.подмениТипЗначения(цел32,x)); кон ">>";
	операция "<<"*(перем x: вещ32; port: Input); нач Receive(port,НИЗКОУР.подмениТипЗначения(цел32,x)); кон "<<";
	операция ">>?"*(port: Input; перем x: вещ32): булево; нач возврат ReceiveNonBlocking(port,НИЗКОУР.подмениТипЗначения(цел32,x)); кон ">>?";
	операция "<<?"*(перем x: вещ32; port: Input): булево; нач возврат ReceiveNonBlocking(port,НИЗКОУР.подмениТипЗначения(цел32,x)); кон "<<?";

кон Channels.


модуль WMApplications; (** AUTHOR "staubesv"; PURPOSE "Application Launcher"; *)

использует
	ЛогЯдра,
	Потоки, Modules, Commands, Inputs, Строки8, XML, XMLObjects, Repositories,
	WMGraphics, WMComponents, WMMessages, WMRestorable, WMWindowManager;

конст
	Ok = 0;
	RepositoryNotFound = 1;
	ApplicationNotFound = 2;
	ParseError = 3;

	DefaultWidth = 640;
	DefaultHeight = 480;
	DefaultAlpha = ложь;

	(* Default window flags *)
	FlagFrame = истина;
	FlagClose = истина;
	FlagMinimize = истина;
	FlagStayOnTop = ложь;
	FlagStayOnBottom = ложь;
	FlagNoFocus = ложь;
	FlagNavigation = ложь;
	FlagHidden = ложь;
	FlagNoResizing = ложь;

	Mode_Standard = 0;
	Mode_Move = 1;
	Mode_ResizeLeft = 2;
	Mode_ResizeTopLeft = 3;
	Mode_ResizeTop = 4;
	Mode_ResizeTopRight = 5;
	Mode_ResizeRight = 6;
	Mode_ResizeBottomRight = 7;
	Mode_ResizeBottom = 8;
	Mode_ResizeBottomLeft = 9;

	ResizeAreaSize = 10;

	MinimumWidth = 20;
	MinimumHeight = 20;

тип

	WindowInfo = запись
		title : Строки8.уСтрока;
		icon : Строки8.уСтрока;
		width, height : цел32;
		alpha : булево;
		flags : мнвоНаБитахМЗ;
		alternativeMoveResize : булево;
	кон;

	ApplicationInfo = запись
		repository : Repositories.Repository; (* {repository # NIL} *)
		name : Repositories.Name;
		content : WMComponents.VisualComponent;
		window : WindowInfo;
	кон;

тип

	KillerMsg = окласс
	кон KillerMsg;

	Window = окласс (WMComponents.FormWindow)
	перем
		name : Repositories.Name;
		repository : Repositories.Repository;

		проц &New(конст info : ApplicationInfo; context : WMRestorable.Context);
		нач
			утв(info.repository # НУЛЬ);
			repository := info.repository;
			копируйСтрокуДо0(info.name, name);

			если (context = НУЛЬ) то
				Init(info.window.width, info.window.height, info.window.alpha);
			иначе
				Init(context.r - context.l, context.b - context.t, info.window.alpha);
			всё;

			если (info.content # НУЛЬ) то
				info.content.alignment.Set(WMComponents.AlignClient);
				SetContent(info.content);
			всё;

			если (info.window.title # НУЛЬ) то
				SetTitle(info.window.title);
			всё;

			если (info.window.icon # НУЛЬ) то
				SetIcon(WMGraphics.LoadImage(info.window.icon^, истина));
			всё;

			если (context # НУЛЬ) то
				WMRestorable.AddByContext(сам, context);
			иначе
				WMWindowManager.ExtAddWindow(сам, 100, 100, info.window.flags)
			всё;
			IncCount;
		кон New;

		проц {перекрыта}Close*;
		нач
			Close^;
			DecCount;
		кон Close;

		проц {перекрыта}Handle*(перем x : WMMessages.Message);
		перем data : XML.Element;
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) то
				если (x.ext суть WMRestorable.Storage) то
					нов(data); data.SetName("Data");
					data.SetAttributeValue("repository", repository.name);
					data.SetAttributeValue("name", repository.name);
					x.ext(WMRestorable.Storage).Add(name, "WMApplications.Restore", сам, data);
				аесли (x.ext суть KillerMsg) то
					Close;
				иначе
					Handle^(x)
				всё
			иначе
				Handle^(x)
			всё
		кон Handle;

	кон Window;

тип

	GadgetWindow = окласс(Window);
	перем
		lastX, lastY : размерМЗ;
		mode : целМЗ;
		resized : булево;
		viewport : WMWindowManager.ViewPort;

		проц &{перекрыта}New(конст info : ApplicationInfo; context : WMRestorable.Context);
		нач
			New^(info, context);
			mode := Mode_Standard;
			resized := ложь;
			viewport := WMWindowManager.GetDefaultView();
			утв(viewport # НУЛЬ);
		кон New;

		проц GetMode(x, y : размерМЗ) : целМЗ;
		перем mode: целМЗ; width, height : размерМЗ;
		нач
			width := bounds.r - bounds.l; height := bounds.b - bounds.t;
			если (x <= ResizeAreaSize) то
				если (y <= ResizeAreaSize) то
					mode := Mode_ResizeTopLeft;
				аесли (y >= height - ResizeAreaSize) то
					mode := Mode_ResizeBottomLeft;
				иначе
					mode := Mode_ResizeLeft;
				всё;
			аесли (x >= width - ResizeAreaSize) то
				если (y <= ResizeAreaSize) то
					mode := Mode_ResizeTopRight;
				аесли (y >= height - ResizeAreaSize) то
					mode := Mode_ResizeBottomRight;
				иначе
					mode := Mode_ResizeRight;
				всё;
			аесли (y <= ResizeAreaSize) то
				mode := Mode_ResizeTop;
			аесли (y >= height - ResizeAreaSize) то
				mode := Mode_ResizeBottom;
			иначе
				mode := Mode_Move;
			всё;
			если (WMWindowManager.FlagNoResizing в flags) и (mode # Mode_Standard) то
				mode := Mode_Move;
			всё;
			возврат mode;
		кон GetMode;

		проц UpdatePointerInfo(mode : цел32);
		перем info : WMWindowManager.PointerInfo;
		нач
			просей mode из
				| Mode_ResizeLeft: info := manager.pointerLeftRight;
				| Mode_ResizeRight: info := manager.pointerLeftRight;
				| Mode_ResizeTop: info := manager.pointerUpDown;
				| Mode_ResizeBottom: info := manager.pointerUpDown;
				| Mode_ResizeTopLeft: info := manager.pointerULDR;
				| Mode_ResizeTopRight: info := manager.pointerURDL;
				| Mode_ResizeBottomLeft: info := manager.pointerURDL;
				| Mode_ResizeBottomRight: info := manager.pointerULDR;
				| Mode_Move: info := manager.pointerMove;
			иначе
				info := manager.pointerStandard;
			всё;
			SetPointerInfo(info);
		кон UpdatePointerInfo;

		проц MoveOrResize() : булево;
		перем state : мнвоНаБитахМЗ;
		нач
			viewport.GetKeyState(state);
			возврат (Inputs.LeftCtrl в state);
		кон MoveOrResize;

		проц {перекрыта}PointerDown*(x, y : размерМЗ; keys : мнвоНаБитахМЗ);
		нач
			PointerDown^(x, y, keys);
			lastX := bounds.l + x; lastY:=bounds.t + y;
			если (0 в keys) и MoveOrResize() и (mode = Mode_Standard) то
				mode := GetMode(x, y);
				UpdatePointerInfo(mode);
			всё;
		кон PointerDown;

		проц {перекрыта}PointerMove*(x, y : размерМЗ; keys : мнвоНаБитахМЗ);
		перем dx, dy, width, height, newWidth, newHeight, moveX, moveY: размерМЗ; tempMode : целМЗ;
		нач
			PointerMove^(x, y, keys);
			если MoveOrResize() и (mode = Mode_Standard) то
				tempMode := GetMode(x, y);
				если (tempMode # mode) то UpdatePointerInfo(tempMode); всё;
			всё;

			если (mode # Mode_Standard) то
				width := GetWidth(); height := GetHeight();
				newWidth := width; newHeight := height;
				x := bounds.l + x; y := bounds.t + y;
				dx := x - lastX; dy := y - lastY;
				lastX := lastX + dx; lastY := lastY + dy;
				moveX := 0; moveY := 0;
				если (mode = Mode_Move) то
					moveX := dx; moveY := dy;
				иначе
					просей mode из
						| Mode_ResizeLeft: newWidth := width - dx; moveX := dx;
						| Mode_ResizeRight: newWidth := width + dx;
						| Mode_ResizeTop: newHeight := height - dy; moveY := dy;
						| Mode_ResizeBottom: newHeight := height + dy;
						| Mode_ResizeTopLeft:
							newWidth := width - dx; moveX := dx;
							newHeight := height - dy; moveY := dy;
						| Mode_ResizeTopRight:
							newWidth := width + dx;
							newHeight := height - dy; moveY := dy;
						| Mode_ResizeBottomLeft:
							newWidth := width - dx; moveX := dx;
							newHeight := height + dy;
						| Mode_ResizeBottomRight:
							newWidth := width + dx;
							newHeight := height + dy;
					иначе
						newWidth := width; newHeight := height;
					всё;
				всё;
				если (newWidth < MinimumWidth) то
					если (moveX # 0) то moveX := moveX - (newWidth - MinimumWidth); всё;
					newWidth := MinimumWidth;
				всё;
				если (newHeight < MinimumHeight) то
					если (moveY # 0) то moveY := moveY - (newHeight - MinimumHeight); всё;
					newHeight := MinimumHeight;
				всё;
				если (newWidth # width) или (newHeight # height) то
					manager.SetWindowSize(сам, newWidth, newHeight); resized := истина;
				всё;
				если (moveX # 0) или (moveY # 0) то
					manager.SetWindowPos(сам, bounds.l + moveX, bounds.t + moveY);
				всё;
			всё;
		кон PointerMove;

		проц {перекрыта}Handle*(перем m : WMMessages.Message);
		нач
			если MoveOrResize() и (m.msgType = WMMessages.MsgPointer) то
				если m.msgSubType = WMMessages.MsgSubPointerMove то PointerMove(m.x, m.y, m.flags)
				аесли m.msgSubType = WMMessages.MsgSubPointerDown то PointerDown(m.x, m.y, m.flags)
				аесли m.msgSubType = WMMessages.MsgSubPointerUp то PointerUp(m.x, m.y, m.flags)
				аесли m.msgSubType = WMMessages.MsgSubPointerLeave то PointerLeave
				всё
			иначе
				Handle^(m);
			всё;
		кон Handle;

		проц {перекрыта}PointerUp*(x, y:размерМЗ; keys:мнвоНаБитахМЗ);
		нач
			PointerUp^(x, y, keys);
			если (mode # Mode_Standard) и ~(0 в keys) то
				mode := Mode_Standard;
				UpdatePointerInfo(mode);
				если resized то
					Resized(GetWidth(), GetHeight());
				всё;
			всё;
		кон PointerUp;

		проц {перекрыта}PointerLeave*;
		нач
			PointerLeave^;
		кон PointerLeave;

	кон GadgetWindow;

перем
	nofWindows : цел32;

проц Open*(context : Commands.Context);
перем
	fullName, repositoryName, applicationName : массив 128 из симв8;
	refNum: размерМЗ; res: целМЗ;
	info : ApplicationInfo;
	window : Window;
	gadgetWindow : GadgetWindow;
нач
	fullName := "";
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fullName);
	если Repositories.SplitName(fullName, repositoryName, applicationName, refNum) то
		res := GetApplicationInfo(repositoryName, applicationName, info);
		если (res = Ok) то
			если ~info.window.alternativeMoveResize то
				нов(window, info, НУЛЬ);
			иначе
				нов(gadgetWindow, info, НУЛЬ);
			всё;
		аесли (res = RepositoryNotFound) то
			context.error.пСтроку8("Repository "); context.error.пСтроку8(repositoryName);
			context.error.пСтроку8(" not found."); context.error.пВК_ПС;
		аесли (res = ApplicationNotFound) то
			context.error.пСтроку8("Application "); context.error.пСтроку8(applicationName);
			context.error.пСтроку8(" not found in repository "); context.error.пСтроку8(repositoryName);
			context.error.пВК_ПС;
		аесли (res = ParseError) то
			context.error.пСтроку8("Application parse error."); context.error.пВК_ПС;
		иначе
			context.error.пСтроку8("res = "); context.error.пЦел64(res, 0); context.error.пВК_ПС;
		всё;
	иначе
		context.error.пСтроку8(fullName); context.error.пСтроку8(" not valid."); context.error.пВК_ПС;
	всё;
кон Open;

проц Restore*(context : WMRestorable.Context);
перем
	repositoryName, applicationName : Repositories.Name; string : Строки8.уСтрока;
	window : Window;
	info : ApplicationInfo;
	writer : Потоки.Писарь;
	res : целМЗ;
нач
	утв((context # НУЛЬ) и (context.appData # НУЛЬ));
	string := context.appData.GetAttributeValue("repository");
	если (string # НУЛЬ) то
		копируйСтрокуДо0(string^, repositoryName);
		string := context.appData.GetAttributeValue("name");
		если (string # НУЛЬ) то
			копируйСтрокуДо0(string^, applicationName);
			res := GetApplicationInfo(repositoryName, applicationName, info);
			если (res = Ok) то
				нов(window, info, context);
			иначе
				нов(writer, ЛогЯдра.ЗапишиВПоток, 128);
				ShowRes(res, repositoryName, applicationName, writer);
			всё;
		иначе
			ЛогЯдра.пСтроку8("WMApplications.Restore failed"); ЛогЯдра.пВК_ПС;
		всё;
	иначе
		ЛогЯдра.пСтроку8("WMApplications.Restore failed"); ЛогЯдра.пВК_ПС;
	всё;
кон Restore;

проц ShowRes(res : целМЗ; конст repositoryName, applicationName : массив из симв8; out : Потоки.Писарь);
нач
	утв(out # НУЛЬ);
	просей res из
		| Ok:
			out.пСтроку8("Ok");
		| RepositoryNotFound:
			out.пСтроку8("Repository "); out.пСтроку8(repositoryName); out.пСтроку8(" not found."); out.пВК_ПС;
		| ApplicationNotFound:
			out.пСтроку8("Application "); out.пСтроку8(applicationName);
			out.пСтроку8(" not found in repository "); out.пСтроку8(repositoryName); out.пВК_ПС;
		| ParseError:
			out.пСтроку8("Application parse error."); out.пВК_ПС;
	иначе
		out.пСтроку8("res = "); out.пЦел64(res, 0); out.пВК_ПС;
	всё;
	out.ПротолкниБуферВПоток;
кон ShowRes;

проц GetApplicationInfo(конст repositoryName, applicationName : массив из симв8; перем info : ApplicationInfo) : целМЗ;
перем repository : Repositories.Repository; application : XML.Element; res : целМЗ;
нач
	repository := Repositories.ThisRepository(repositoryName);
	если (repository # НУЛЬ) то
		application := GetApplication(repository, applicationName);
		если (application # НУЛЬ) то
			если ParseApplication(applicationName, application, info) то info.repository := repository; res := Ok;
			иначе res := ParseError;
			всё;
		иначе res := ApplicationNotFound;
		всё;
	иначе res := RepositoryNotFound;
	всё;
	возврат res;
кон GetApplicationInfo;

проц ParseApplication(конст applicationName : массив из симв8; конст application : XML.Element; перем info : ApplicationInfo) : булево;
перем enumerator : XMLObjects.Enumerator; ptr : динамическиТипизированныйУкль; string : Строки8.уСтрока; component : Repositories.Component; res : целМЗ;

	проц GetFlags(element : XML.Element) : мнвоНаБитахМЗ;
	перем flags : мнвоНаБитахМЗ; value : булево;
	нач
		утв(element # НУЛЬ);
		flags := {};

		если ~GetBoolean(element, "frame", value) то value := FlagFrame; всё;
		если value то включиВоМнвоНаБитах(flags, WMWindowManager.FlagFrame); всё;

		если ~GetBoolean(element, "closeBtn", value) то value := FlagClose; всё;
		если value то включиВоМнвоНаБитах(flags, WMWindowManager.FlagClose); всё;

		если ~GetBoolean(element, "minimizeBtn", value) то value := FlagMinimize; всё;
		если value то включиВоМнвоНаБитах(flags, WMWindowManager.FlagMinimize); всё;

		если ~GetBoolean(element, "stayOnTop", value) то value := FlagStayOnTop; всё;
		если value то включиВоМнвоНаБитах(flags, WMWindowManager.FlagStayOnTop); всё;

		если ~GetBoolean(element, "nofocus", value) то value := FlagNoFocus; всё;
		если value то включиВоМнвоНаБитах(flags, WMWindowManager.FlagNoFocus); всё;

		если ~GetBoolean(element, "stayOnBottom", value) то value := FlagStayOnBottom; всё;
		если value то включиВоМнвоНаБитах(flags, WMWindowManager.FlagStayOnBottom); всё;

		если ~GetBoolean(element, "navigation", value) то value := FlagNavigation; всё;
		если value то включиВоМнвоНаБитах(flags, WMWindowManager.FlagNavigation); всё;

		если ~GetBoolean(element, "hidden", value) то value := FlagHidden; всё;
		если value то включиВоМнвоНаБитах(flags, WMWindowManager.FlagHidden); всё;

		если ~GetBoolean(element, "noResizing", value) то value := FlagNoResizing; всё;
		если value то включиВоМнвоНаБитах(flags, WMWindowManager.FlagNoResizing); всё;

		возврат flags;
	кон GetFlags;

нач
	утв(application # НУЛЬ);
	копируйСтрокуДо0(applicationName, info.name);
	enumerator := application.GetContents();
	нцПока enumerator.HasMoreElements() делай
		ptr := enumerator.GetNext();
		если (ptr # НУЛЬ) то
			если (ptr суть XML.Element) то
				string := ptr(XML.Element).GetName();
				если (string # НУЛЬ) и (string^ = "Window") то
					info.window.title := ptr(XML.Element).GetAttributeValue("title");
					info.window.icon := ptr(XML.Element).GetAttributeValue("icon");
					если ~GetInteger(ptr(XML.Element), "width", info.window.width) то info.window.width := DefaultWidth; всё;
					если ~GetInteger(ptr(XML.Element), "height", info.window.height) то info.window.height := DefaultHeight; всё;
					если ~GetBoolean(ptr(XML.Element), "alpha", info.window.alpha) то info.window.alpha := DefaultAlpha; всё;
					info.window.flags := GetFlags(ptr(XML.Element));
					если ~GetBoolean(ptr(XML.Element), "alternativeMoveResize", info.window.alternativeMoveResize) то
						info.window.alternativeMoveResize := ложь;
					всё;
				иначе
					ЛогЯдра.пСтроку8("He2_??"); ЛогЯдра.пВК_ПС;
				всё;
			аесли (ptr суть XML.Chars) то
				string := ptr(XML.Chars).GetStr();
				если (string # НУЛЬ) то
					Repositories.GetComponentByString(string^, component, res);
					если (res = Repositories.Ok) то
						если (component суть WMComponents.VisualComponent) то
							info.content := component(WMComponents.VisualComponent);
						иначе
							ЛогЯдра.пСтроку8("Component is not a visual components."); ЛогЯдра.пВК_ПС;
						всё;
					иначе
						ЛогЯдра.пСтроку8("Component not found"); ЛогЯдра.пВК_ПС;
					всё;
				всё;
			иначе
				ЛогЯдра.пСтроку8("HEEE??"); ЛогЯдра.пВК_ПС;
			всё;
		всё;
	кц;
	возврат info.content # НУЛЬ;
кон ParseApplication;

проц GetApplication(repository : Repositories.Repository; конст applicationName : массив из симв8) : XML.Element;
перем result : XML.Element; enumerator : XMLObjects.Enumerator; ptr : динамическиТипизированныйУкль; string : Строки8.уСтрока;
нач
	утв(repository # НУЛЬ);
	result := НУЛЬ;
	enumerator := repository.GetApplicationEnumerator();
	если (enumerator # НУЛЬ) то
		нцПока (result = НУЛЬ) и enumerator.HasMoreElements() делай
			ptr := enumerator.GetNext();
			если (ptr # НУЛЬ) и (ptr суть XML.Element) то
				string := ptr(XML.Element).GetName();
				если (string # НУЛЬ) и (string^ = "Application") то
					string := ptr(XML.Element).GetAttributeValue("name");
					если (string # НУЛЬ) и (string^ = applicationName) то
						result := ptr(XML.Element);
					всё;
				всё;
			всё;
		кц;
	всё;
	возврат result;
кон GetApplication;

проц GetInteger(element : XML.Element; конст attributeName : массив из симв8; перем value : цел32) : булево;
перем string : Строки8.уСтрока;
нач
	утв(element # НУЛЬ);
	string := element.GetAttributeValue(attributeName);
	если (string # НУЛЬ) то
		Строки8.ПрочтиЦел32_изСтроки(string^, value);
	всё;
	возврат (string # НУЛЬ);
кон GetInteger;

проц GetBoolean(element : XML.Element; конст attributeName : массив из симв8; перем value : булево) : булево;
перем string : Строки8.уСтрока;
нач
	утв(element # НУЛЬ);
	string := element.GetAttributeValue(attributeName);
	если (string # НУЛЬ) то
		Строки8.БулевоИзСтрокиПоˉанглийски(string^, value);
	всё;
	возврат (string # НУЛЬ);
кон GetBoolean;

проц IncCount;
нач {единолично}
	увел(nofWindows)
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows)
кон DecCount;

проц Cleanup;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WMWindowManager.WindowManager;
нач {единолично}
	нов(die);
	msg.ext := die;
	msg.msgType := WMMessages.MsgExt;
	m := WMWindowManager.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0)
кон Cleanup;

нач
	nofWindows := 0;
	Modules.InstallTermHandler(Cleanup);
кон WMApplications.

WMApplications.Open RepositoryName::ApplicationName ~

WMApplications.Open test:test:0 ~

WMApplications.Open System:ObjectTracker ~

System.Free WMApplications ~

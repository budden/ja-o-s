(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль MathCplx;   (** AUTHOR "adf"; PURPOSE "Complex math functions"; *)

(*	Refs: M. Abramowitz and I. A. Stegun, Handbook of Mathematical Functions with Formulas, Graphs, and Mathematical
				Tables, National Bureau of Standards, Applied Mathematics Series, Vol. 55, 1964.
			R. V. Churchill, J. W. Brown and R. F. Verhey, Complex Variables and Applications, 3rd ed., McGraw-Hill, 1974.
			P. Midy and Y. Yakovlev, Mathematics and Computers in Simulation, Vol. 33, 1991, 33-49. *)

использует NbrInt, NbrRe, NbrCplx, DataErrors, MathRe;

перем
	ln2, ln2Inv, ln10, ln10Inv: NbrRe.Real;

	(** Returns a pseudo-random number  z = r exp(if)  uniformly distributed over the unit circle,  r N (0,1),  f N (-p,p)). *)
	проц Random*( ): NbrCplx.Complex;
	перем abs, arg: NbrRe.Real;  z: NbrCplx.Complex;
	нач
		abs := MathRe.Sqrt( MathRe.Random() );  arg := NbrRe.Pi * (2 * MathRe.Random() - 1);
		NbrCplx.Set( abs, arg, z );  возврат z
	кон Random;

(** Power Functions *)
	проц Sqrt*( z: NbrCplx.Complex ): NbrCplx.Complex;
	перем abs, arg, im, re: NbrRe.Real;  sqrt: NbrCplx.Complex;
	нач
		im := NbrCplx.Im( z );
		если im = 0 то
			re := NbrCplx.Re( z );
			(* Handle the branch cut along the negative real axis. *)
			если re < 0 то NbrCplx.Set( 0, MathRe.Sqrt( -re ), sqrt )
			аесли re = 0 то sqrt := 0
			иначе NbrCplx.Set( MathRe.Sqrt( re ), 0, sqrt )
			всё
		иначе abs := NbrCplx.Abs( z );  arg := NbrCplx.Arg( z );  NbrCplx.SetPolar( MathRe.Sqrt( abs ), arg / 2, sqrt )
		всё;
		возврат sqrt
	кон Sqrt;

	проц IntPower*( z: NbrCplx.Complex;  n: NbrInt.Integer ): NbrCplx.Complex;
	перем abs, arg: NbrRe.Real;  power: NbrCplx.Complex;
	нач
		если n = 0 то
			если z # 0 то power := 1 иначе DataErrors.Error( "Both argument and exponent cannot be zero." );  power := 0 всё
		аесли z = 0 то
			если n > 0 то power := 0
			иначе DataErrors.IntError( n, "Exponent cannot be negative when argument is zero." );  power := 0
			всё
		иначе
			abs := MathRe.IntPower( NbrCplx.Abs( z ), n );  arg := n * NbrCplx.Arg( z );
			NbrCplx.SetPolar( abs, arg, power )
		всё;
		возврат power
	кон IntPower;

	проц LnMod( z: NbrCplx.Complex ): NbrRe.Real;
	(* Algorithm of Midy and Yakovlev. *)
	перем r, r2, im, im2, lower, upper, result: NbrRe.Real;
	нач
		lower := 0.82;  upper := 1.22;  r := NbrRe.Abs( NbrCplx.Re( z ) );  r2 := r * r;
		im := NbrRe.Abs( NbrCplx.Im( z ) );  im2 := im * im;
		если r > im то
			если (2 * im2 < r2) и (lower < r) и (r < upper) то
				result := MathRe.Ln( r ) + MathRe.ArcTanh( im2 / (2 * r2 + im2) )
			иначе result := MathRe.Ln( r2 + im2 ) / 2
			всё
		иначе
			если (2 * r2 < im2) и (lower < im) и (im < upper) то
				result := MathRe.Ln( im ) + MathRe.ArcTanh( r2 / (2 * im2 + r2) )
			иначе result := MathRe.Ln( r2 + im2 ) / 2
			всё
		всё;
		возврат result
	кон LnMod;

	проц RealPower*( z: NbrCplx.Complex;  x: NbrRe.Real ): NbrCplx.Complex;
	перем abs, arg: NbrRe.Real;  power: NbrCplx.Complex;
	нач
		abs := NbrCplx.Abs( z );
		если x = 0 то
			если abs > 0 то power := 1 иначе DataErrors.Error( "Both argument and exponent cannot be zero." );  power := 0 всё
		аесли abs = 0 то
			если x > 0 то power := 0
			иначе DataErrors.ReError( x, "Exponent cannot be negative when argument is zero." );  power := 0
			всё
		иначе abs := MathRe.Exp( x * LnMod( z ) );  arg := x * NbrCplx.Arg( z );  NbrCplx.SetPolar( abs, arg, power )
		всё;
		возврат power
	кон RealPower;

	проц Power*( zc, ze: NbrCplx.Complex ): NbrCplx.Complex;
	перем abs, absC, absE, arg, im, lnRho, phi, re: NbrRe.Real;  power: NbrCplx.Complex;
		(* Formula listed in Midy and Yakovlev. *)
	нач
		absC := NbrCplx.Abs( zc );  absE := NbrCplx.Abs( ze );
		если absE = 0 то
			если absC > 0 то power := 1
			иначе DataErrors.Error( "Both argument and exponent cannot be zero." );  power := 0
			всё
		аесли absC = 0 то power := 0
		иначе
			re := NbrCplx.Re( ze );  im := NbrCplx.Im( ze );  lnRho := LnMod( zc );  phi := NbrCplx.Arg( zc );
			abs := MathRe.Exp( lnRho * re - phi * im );  arg := lnRho * im + phi * re;  NbrCplx.SetPolar( abs, arg, power )
		всё;
		возврат power
	кон Power;

	(** Logarithmic Functions *)
(** Exp(z) = Exp(z 1 2pi), i.e., it is periodic with imaginary period  2pi. *)
	проц Exp*( z: NbrCplx.Complex ): NbrCplx.Complex;
	перем coef, re, real, im, imag: NbrRe.Real;  exp: NbrCplx.Complex;
	нач
		(* Euler's formula *)
		re := NbrCplx.Re( z );  im := NbrCplx.Im( z );  coef := MathRe.Exp( re );  real := coef * MathRe.Cos( im );
		imag := coef * MathRe.Sin( im );  NbrCplx.Set( real, imag, exp );  возврат exp
	кон Exp;

	проц Exp2*( z: NbrCplx.Complex ): NbrCplx.Complex;
	нач
		возврат Exp( ln2 * z )
	кон Exp2;

	проц Exp10*( z: NbrCplx.Complex ): NbrCplx.Complex;
	нач
		возврат Exp( ln10 * z )
	кон Exp10;

(** Ln(z) returns its principal value where Im(Ln(z)) N [-p, p]. *)
	проц Ln*( z: NbrCplx.Complex ): NbrCplx.Complex;
	перем re, im: NbrRe.Real;  ln: NbrCplx.Complex;
	нач
		re := LnMod( z );  im := NbrCplx.Arg( z );  NbrCplx.Set( re, im, ln );  возврат ln
	кон Ln;

	проц Log2*( z: NbrCplx.Complex ): NbrCplx.Complex;
	перем log: NbrCplx.Complex;
	нач
		log := ln2Inv * Ln( z );  возврат log
	кон Log2;

	проц Log*( z: NbrCplx.Complex ): NbrCplx.Complex;
	перем log: NbrCplx.Complex;
	нач
		log := ln10Inv * Ln( z );  возврат log
	кон Log;

	(* Using the Exp and Ln functions from above to define all functions below
		implies that they will return principal value(s) in their periodic component(s). *)

(** Trigonometric Functions *)
	проц Sin*( z: NbrCplx.Complex ): NbrCplx.Complex;
	перем sin: NbrCplx.Complex;
	нач
		sin := (Exp( NbrCplx.I * z ) - Exp( -NbrCplx.I * z )) / (2 * NbrCplx.I);  возврат sin
	кон Sin;

	проц Cos*( z: NbrCplx.Complex ): NbrCplx.Complex;
	перем cos: NbrCplx.Complex;
	нач
		cos := (Exp( NbrCplx.I * z ) + Exp( -NbrCplx.I * z )) / 2;  возврат cos
	кон Cos;

	проц Tan*( z: NbrCplx.Complex ): NbrCplx.Complex;
	(* Algorithm of Midy and Yakovlev. *)
	перем d, w, w2, x, x2, y, y2, re, real, im, imag, four, limit: NbrRe.Real;  tan: NbrCplx.Complex;
	нач
		limit := 0.65;  re := NbrCplx.Re( z );  im := NbrCplx.Im( z );  x := MathRe.Tan( re );  x2 := x * x;
		если NbrRe.Abs( im ) < limit то
			y := MathRe.Tanh( im );  y2 := y * y;  d := 1 + x2 * y2;  real := x * (1 - y2) / d;  imag := y * (1 + x2) / d
		иначе
			four := 4;  y := MathRe.Exp( MathRe.Ln( four ) - 2 * NbrRe.Abs( im ) );  w := y / four;  w2 := w * w;
			d := 1 + 2 * w + w2 + (1 - 2 * w + w2) * x2;  real := x * y / d;
			imag := NbrRe.Sign( im ) * (1 - w2) * (1 + x2) / d
		всё;
		NbrCplx.Set( real, imag, tan );  возврат tan
	кон Tan;

	проц ArcSin*( z: NbrCplx.Complex ): NbrCplx.Complex;
	перем asin: NbrCplx.Complex;
	нач
		если NbrCplx.Im( z ) = 0 то asin := MathRe.ArcSin( NbrCplx.Re( z ) )
		иначе asin := -NbrCplx.I * Ln( NbrCplx.I * z + Sqrt( 1 - z * z ) )
		всё;
		возврат asin
	кон ArcSin;

	проц ArcCos*( z: NbrCplx.Complex ): NbrCplx.Complex;
	перем acos: NbrCplx.Complex;
	нач
		если NbrCplx.Im( z ) = 0 то acos := MathRe.ArcCos( NbrCplx.Re( z ) )
		иначе acos := -NbrCplx.I * Ln( z + NbrCplx.I * Sqrt( 1 - z * z ) )
		всё;
		возврат acos
	кон ArcCos;

	проц ArcTan*( z: NbrCplx.Complex ): NbrCplx.Complex;
	перем im: NbrRe.Real;  atan: NbrCplx.Complex;
	нач
		если NbrCplx.Re( z ) = 0 то
			(* Address the branch cuts along the imaginary axis. *)
			im := NbrCplx.Im( z );
			если NbrRe.Abs( im ) < 1 то atan := NbrCplx.I * (MathRe.Ln( (1 + im) / (1 - im) ) / 2)
			иначе DataErrors.CplxError( z, "Argument lies outside the admissible range for this function." );  atan := 0
			всё
		иначе atan := NbrCplx.I * Ln( (NbrCplx.I + z) / (NbrCplx.I - z) ) / 2
		всё;
		возврат atan
	кон ArcTan;

(** Hyperbolic functions *)
	проц Sinh*( z: NbrCplx.Complex ): NbrCplx.Complex;
	перем sinh: NbrCplx.Complex;
	нач
		sinh := (Exp( z ) - Exp( -z )) / 2;  возврат sinh
	кон Sinh;

	проц Cosh*( z: NbrCplx.Complex ): NbrCplx.Complex;
	перем cosh: NbrCplx.Complex;
	нач
		cosh := (Exp( z ) + Exp( -z )) / 2;  возврат cosh
	кон Cosh;

	проц Tanh*( z: NbrCplx.Complex ): NbrCplx.Complex;
	перем tanh: NbrCplx.Complex;
	нач
		tanh := -NbrCplx.I * Tan( NbrCplx.I * z );  возврат tanh
	кон Tanh;

	проц ArcSinh*( z: NbrCplx.Complex ): NbrCplx.Complex;
	перем abs, im: NbrRe.Real;  asinh: NbrCplx.Complex;
	нач
		если NbrCplx.Re( z ) = 0 то
			(* Address the branch cut along the imaginary axis. *)
			im := NbrCplx.Im( z );  abs := NbrRe.Abs( im );
			если abs < 1 то asinh := Ln( z + MathRe.Sqrt( 1 - im * im ) )
			аесли abs = 1 то asinh := 0
			иначе DataErrors.CplxError( z, "Argument lies outside the admissible range for this function." );  asinh := 0
			всё
		иначе asinh := Ln( z + Sqrt( z * z + 1 ) )
		всё;
		возврат asinh
	кон ArcSinh;

	проц ArcCosh*( z: NbrCplx.Complex ): NbrCplx.Complex;
	перем acosh: NbrCplx.Complex;
	нач
		если NbrCplx.Im( z ) = 0 то acosh := MathRe.ArcCosh( NbrCplx.Re( z ) ) иначе acosh := Ln( z + Sqrt( z * z - 1 ) ) всё;
		возврат acosh
	кон ArcCosh;

	проц ArcTanh*( z: NbrCplx.Complex ): NbrCplx.Complex;
	перем atanh: NbrCplx.Complex;
	нач
		если NbrCplx.Im( z ) = 0 то atanh := MathRe.ArcTanh( NbrCplx.Re( z ) ) иначе atanh := Ln( (1 + z) / (1 - z) ) / 2 всё;
		возврат atanh
	кон ArcTanh;

нач
	ln2 := MathRe.Ln( 2 );  ln2Inv := 1/ln2;  ln10 := MathRe.Ln( 10 );  ln10Inv := 1/ln10
кон MathCplx.

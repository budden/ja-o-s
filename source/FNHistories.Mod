модуль FNHistories; (** AUTHOR "Fabian Nart"; PURPOSE "Unbounded general-purpose history datastructure"; *)

использует
	ЛогЯдра;
тип
	HistoryItem = укль на запись
		item : динамическиТипизированныйУкль;
		previous, next : HistoryItem
	кон;

	History* = окласс
	перем
		start, current : HistoryItem;

		проц & Init*;
		нач
			start := НУЛЬ;
			current := НУЛЬ
		кон Init;

		(** return TRUE if no items have been inserted yet *)
		проц IsEmpty*() : булево;
		нач
			возврат current = НУЛЬ
		кон IsEmpty;

		(** insert new item as current *)
		проц Insert*(item : динамическиТипизированныйУкль);
		перем new : HistoryItem;
		нач
			нов(new);
			new.item := item;
			new.previous := current;
			если IsEmpty() то
				start := new;
			иначе
				current.next := new;
			всё;
			current := new
		кон Insert;

		(** get current item in history or NIL if there is no current one *)
		проц GetCurrent*() : динамическиТипизированныйУкль;
		нач
			если IsEmpty() то
				возврат НУЛЬ
			иначе
				возврат current.item
			всё
		кон GetCurrent;

		(** turn history one step back. return FALSE if there is no previous item.*)
		проц Back*() : булево;
		нач
			если (current = НУЛЬ) или (current.previous = НУЛЬ) то
				возврат ложь
			иначе
				current := current.previous;
				возврат истина
			всё
		кон Back;

		(** turn history back as many steps as necessairy to make item the current one. return FALSE if item cannot be found *)
		проц BackTo*(item : динамическиТипизированныйУкль) : булево;
		нач
			нцПока current # НУЛЬ делай
				если current.item = item то возврат истина всё;
				current := current.previous
			кц;
			возврат ложь
		кон BackTo;

		(** turn history one step forward. return FALSE if current item is the latest one *)
		проц Forward*() : булево;
		нач
			если (current = НУЛЬ) или (current.next = НУЛЬ) то
				возврат ложь
			иначе
				current := current.next;
				возврат истина
			всё
		кон Forward;

		(** turn history forward as many steps as necessairy to make item the current one. return FALSE if item cannot be found *)
		проц ForwardTo*(item : динамическиТипизированныйУкль) : булево;
		нач
			нцПока current # НУЛЬ делай
				если current.item = item то возврат истина всё;
				current := current.next
			кц;
			возврат ложь
		кон ForwardTo;

		проц Dump*;
		перем i : цел32;
			p : HistoryItem;
		нач
			i := 0;
			p := current;
			нцПока p # НУЛЬ делай
				ЛогЯдра.пСтроку8("[");
				если p.item = НУЛЬ то ЛогЯдра.пСтроку8("NIL") иначе ЛогЯдра.пСтроку8("item") всё;
				ЛогЯдра.пСтроку8("] --> ");
				p := p.previous;
				увел(i)
			кц;
			ЛогЯдра.пСтроку8("start -- ");
			ЛогЯдра.пЦел64(i, 0); ЛогЯдра.пСтроку8(" items in the history"); ЛогЯдра.пВК_ПС;
		кон Dump;

	кон History;
кон FNHistories.

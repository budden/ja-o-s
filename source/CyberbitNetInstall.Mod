модуль CyberbitNetInstall; (** AUTHOR "PL"; PURPOSE "Downloads Cyberbit Unicode Truetype Font from Netscape FTP"; *)

использует
	Потоки, ЛогЯдра, Files, Strings, WMDialogs, WMComponents, WMWindowManager, Commands,
	WMGraphics, WMGraphicUtilities, WMStandardComponents, WMRectangles, FTPClient;

конст
	user = "anonymous";
	pass = "anonymous@the.net";
	address = "ftp.netscape.com";
	path = "/pub/communicator/extras/fonts/windows/";
	file = "Cyberbit.ZIP";

	BufSize = 16*1024;

тип
	Downloader = окласс

		проц &New*;
		перем res : целМЗ; temp : массив 16 из симв8;
			r : Потоки.Чтец; w : Files.Writer;
			buf: массив BufSize из симв8; size : цел32;
			msg, s : массив 256 из симв8; len, i, j : размерМЗ;
			ftpConnection : FTPClient.FTPClient;
			errorStr : массив 512 из симв8;
			f : Files.File;
			status : StatusWindow; transCount : размерМЗ;
		нач
			нов(ftpConnection);
			(* open connection to the ftp *)
			ftpConnection.Open(address, user, pass, 21, res);
			если (res # 0) то
				Strings.IntToStr(res, temp);
				копируйСтрокуДо0("Connection Failed with Error: ", errorStr); Strings.Append(errorStr, temp);
				WMDialogs.Error("CyberbitNetInstall  Error", errorStr);
				возврат
			всё;
			(* change into correct directory *)
			ftpConnection.ChangeDir(path, res);
			если (res # 0) то
				Strings.IntToStr(res, temp);
				копируйСтрокуДо0("Directory not found: ", errorStr); Strings.Append(errorStr, path);
				WMDialogs.Error("CyberbitNetInstall Error", errorStr);
				ftpConnection.Close(res);
				возврат
			всё;
			(* open the remote file *)
			ftpConnection.OpenGet(file, r, res);
			если (res # 0) то
				Strings.IntToStr(res, temp);
				копируйСтрокуДо0("Couldn't open file: ", errorStr); Strings.Append(errorStr, file);
				WMDialogs.Error("CyberbitNetInstall Error", errorStr);
				ftpConnection.Close(res);
				возврат
			всё;
			(* get the size of the file *)
			копируйСтрокуДо0(ftpConnection.msg, msg); j := 0;
			нцДля i := Strings.Pos("(", msg) до Strings.Pos("bytes", msg)-2 делай
				temp[j] := msg[i+1];
				увел(j)
			кц;
			temp[j] := 0X;
			Strings.StrToInt(temp, size);
			(* open the local file *)
			res := -1;
			если (Files.Old(file) = НУЛЬ) или (WMDialogs.Confirmation("Confirm overwriting", file) = WMDialogs.ResYes) то
				f := Files.New(file);
				если f # НУЛЬ то
					Files.OpenWriter(w, f, 0);
					res := 0
				всё
			иначе
				возврат
			всё;
			(* create the status window *)
			нов(status);
			status.fileSize := size;
			status.nofFiles := 1;
			(* transfer the bytes and update the statuswindow *)
			если res = 0 то
				status.Show; transCount := 0;
				нцДо
					r.чБайты(buf, 0, BufSize, len); w.пБайты(buf, 0, len);
					увел(transCount, len);
					status.UpdateStatus(1, transCount);
				кцПри r.кодВозвратаПоследнейОперации # 0;
				ftpConnection.CloseGet(res);
				(* register/update file *)
				w.ПротолкниБуферВПоток;
				f.Update;
				Files.Register(f);
				status.Hide
			всё;
			(* close FTP connection *)
			ftpConnection.Close(res);
			(* extract File *)
			копируйСтрокуДо0("Unzip.ExtractAll ", s); Strings.Append(s, file);
			Commands.Call(s, {}, res, msg);
			если res # 0 то
				ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС
			иначе
				ЛогЯдра.пСтроку8("CyberbitNetInstall finished"); ЛогЯдра.пВК_ПС;
			всё

		кон New;

	кон Downloader;

	StatusWindow = окласс(WMComponents.FormWindow)
	перем
		nofFiles* : цел32;
		currentFile* : цел32;
		fileSize* : цел32;
		transferred* : цел32;
		currentFilename* : массив 256 из симв8;

		shown : булево;
		windowWidth, windowHeight : цел32;
		px, py : цел32;
		bar : WMStandardComponents.Panel;

		проц &New*;
		перем vc : WMComponents.VisualComponent;
		нач
			manager := WMWindowManager.GetDefaultManager();
			px := 300; py := 200; shown := ложь;
			windowWidth := 350; windowHeight := 100;
			vc := CreateForm();
			Init(vc.bounds.GetWidth(), vc.bounds.GetHeight(), ложь);
			SetContent(vc);
			SetTitle(Strings.NewString("Status Window"));
		кон New;

		проц CreateForm*(): WMComponents.VisualComponent;
		перем form, panel : WMStandardComponents.Panel;
			back : BevelPanel;
			label : WMStandardComponents.Label;
			rect : WMRectangles.Rectangle;
			windowStyle : WMWindowManager.WindowStyle;
		нач
			windowStyle := manager.GetStyle();

			нов(form);
			form.bounds.SetExtents(windowWidth, windowHeight);
			form.fillColor.Set(windowStyle.bgColor);

			нов(panel);
			panel.alignment.Set(WMComponents.AlignClient);
			rect := WMRectangles.MakeRect(20, 10, 20, 20); panel.bearing.Set(rect);
			form.AddContent(panel);

			нов(label);
			label.bounds.SetHeight(20); label.alignment.Set(WMComponents.AlignTop);
			label.textColor.Set(windowStyle.fgColor); label.alignH.Set(WMGraphics.AlignCenter);
			label.caption.SetAOC("Transfering...");
			panel.AddContent(label);

			нов(back); back.bevelWidth := 2; back.bevelColor := windowStyle.bgColor;
			back.bounds.SetHeight(20);
			back.alignment.Set(WMComponents.AlignTop); (* back.fillColor.Set(0FFFFFFFFH); *)
			rect := WMRectangles.MakeRect(0, 10, 0, 10);
			back.bearing.Set(rect);
			panel.AddContent(back);

			нов(bar);
			bar.bounds.SetWidth(0);
			bar.alignment.Set(WMComponents.AlignLeft); bar.fillColor.Set(0FFFFH);
			back.AddContent(bar);

			возврат form

		кон CreateForm;

		проц Show*;
		нач
			если ~shown то
				shown := истина;
				WMWindowManager.ExtAddWindow(сам, px, py,
					{WMWindowManager.FlagFrame, WMWindowManager.FlagStayOnTop, WMWindowManager.FlagClose, WMWindowManager.FlagMinimize});
			всё;
		кон Show;

		проц Hide*;
		нач
			manager.Remove(сам);
			shown := ложь
		кон Hide;

		проц UpdateStatus*(file, size : размерМЗ);
		нач
			bar.bounds.SetWidth(округлиВниз((windowWidth-20)/fileSize*size));
		кон UpdateStatus;

	кон StatusWindow;

	BevelPanel* = окласс(WMStandardComponents.Panel);
	перем
		bevelWidth* : цел32;
		bevelColor* : WMGraphics.Color;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		нач
			если ~visible.Get() то возврат всё;
			Acquire;
			WMGraphicUtilities.DrawBevelPanel(canvas, GetClientRect(), bevelWidth, истина, bevelColor, 0 );
			Release;
		кон DrawBackground;
	кон BevelPanel;

проц Start*;
перем loader : Downloader;
нач
	нов(loader);
кон Start;

кон CyberbitNetInstall.

------------------------------------------------------------------------------------------

System.Free CyberbitNetInstall~
CyberbitNetInstall.Start~

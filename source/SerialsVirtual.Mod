модуль SerialsVirtual; (** AUTHOR "staubesv"; PURPOSE "Virtual serial port driver"; *)
(**
 * This driver creates two virtual serial port instances that are linked using a virtual null-modem cable, i.e. the data sent to one
 * port is received by the other and vice versa.
 * Idea: One of the ports can be used by the application under development and the other is used to send data to this application, for example,
 * simulated output of a serial port device.
 *
 * Usage:
 *
 *	SerialsVirtual.Install ~ creates two virtual serial port instances that are cross-linked and registers them at Serials
 *
 *	SerialsVirtual.SendFile portNbr filename [Loop] ~ 	sends the content of the specified file to the specified virtual serial port.
 *														The data is then received by its companion port.
 *	SerialsVirtual.StopSendFile portNbr ~				stops sending a file for the specified port
 *
 *	SerialsVirtual.InstallSniffer ~ installs a virtal serial port that acts as proxy for the specified port
 *
 *	System.Free SerialsVirtual ~ Unregisters virtual serial ports at Serials
 *
 * History:
 *
 *	20.06.2006	Created (staubesv)
 *	26.06.2006	Speed emulation (staubesv)
 *	27.06.2006	Implemented PortSniffer (staubesv)
 *)

использует
	ЛогЯдра, Строки8, Modules, Commands, Потоки, Files, Kernel, Random,
	Serials;

конст

	Verbose = истина;

	BufferSize = 1024;

	(* If TRUE, the SendChar procedure is artificially slowed down to the speed approx. bps *)
	EnableSendSpeedLimitation = истина;

	ModuleName = "SerialsVirtual";

тип

	SendProcedure = проц {делегат} (ch : симв8; перем res : целМЗ);

	(** Virtual serial port the can be linked to other virtual serial port *)
	VirtualPort = окласс (Serials.Port);
	перем
		buffer : массив BufferSize из симв8;
		head, tail : цел32;

		open : булево;
		bps, data, parity, stop : цел32;
		mc : мнвоНаБитахМЗ;

		sender : SendProcedure;

		(* Send speed emulation fields *)
		eachNCharacters, waitForMs : цел32;
		timer : Kernel.Timer;

		(** Virtual Port Interface *)

		проц PutChar(ch : симв8; перем res : целМЗ);
		нач {единолично}
			если ~open то
				res := Serials.Closed;
			иначе
				дождись(((tail + 1) остОтДеленияНа BufferSize # head) или ~open); (* Wait until buffer is not full *)
				если open то
					buffer[tail] := ch;
					tail := (tail + 1) остОтДеленияНа BufferSize;
					res := Serials.Ok;
				иначе
					res := Serials.Closed;
				всё;
			всё;
		кон PutChar;

		(** Serial Port Interface *)

		проц {перекрыта}Open*(bps, data, parity, stop : цел32; перем res: целМЗ);
		нач {единолично}
			если open то
				если Verbose то ShowModule; ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(" already open"); ЛогЯдра.пВК_ПС; всё;
				res := Serials.PortInUse;
				возврат
			всё;
			SetPortState(bps, data, parity, stop, res);
			если res = Serials.Ok то
				open := истина; head := 0; tail := 0;
				charactersSent := 0; charactersReceived := 0;
				если Verbose то ShowModule; ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(" opened"); ЛогЯдра.пВК_ПС; всё;
			всё;
		кон Open;

		проц {перекрыта}Закрой*;
		нач {единолично}
			open := ложь;
			tail := -1;
			если Verbose то ShowModule; ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(" closed"); ЛогЯдра.пВК_ПС; всё;
		кон Закрой;

		проц {перекрыта}SendChar*(ch: симв8; перем res : целМЗ);
		перем wait: булево;
		нач
			нач{единолично}
				если ~open то res := Serials.Closed; всё;
			кон;

			если (errorRate > 0) и (random.Uniform() < errorRate) то возврат; ch := симв8ИзКода(кодСимв8(ch) + random.Dice(20)) всё;
			если sender # НУЛЬ то
				нач{единолично}
					увел(charactersSent);
					если EnableSendSpeedLimitation и (waitForMs # 0) и (charactersSent остОтДеленияНа eachNCharacters = 0) то
						timer.Sleep(waitForMs)
					всё;
				кон;
				sender(ch, res);
			всё;
		кон SendChar;

		(** Wait for the next character is received in the input buffer. The buffer is fed by HandleInterrupt *)
		проц {перекрыта}ReceiveChar*(перем ch: симв8; перем res: целМЗ);
		нач {единолично}
			если ~open то res := Serials.Closed; возврат всё;
			дождись((tail # head) или ~open);
			если ~open или (tail = -1) то
				res := Serials.Closed;
			иначе
				ch := buffer[head]; head := (head+1) остОтДеленияНа BufferSize;
				увел(charactersReceived);
				res := Serials.Ok;
			всё
		кон ReceiveChar;

		проц {перекрыта}Available*(): размерМЗ;
		нач {единолично}
			возврат (tail - head) остОтДеленияНа BufferSize
		кон Available;

		(* Set the port state: speed in bps, no. of data bits, parity, stop bit length. *)
		проц SetPortState(bps, data, parity, stop : цел32; перем res: целМЗ);
		нач
			сам.bps := bps; сам.data := data; сам.parity := parity; сам.stop := stop;
			res := Serials.Ok;
			если EnableSendSpeedLimitation то
				GetSlowdownValues(bps, eachNCharacters, waitForMs, res);
			всё;
		кон SetPortState;

		(** Get the port state: state (open, closed), speed in bps, no. of data bits, parity, stop bit length. *)
		проц {перекрыта}GetPortState*(перем openstat : булево; перем bps, data, parity, stop : цел32);
		нач {единолично}
			openstat := open;
			bps := сам.bps; data := сам.data; parity := сам.parity; stop := сам.stop;
		кон GetPortState;

		(** Clear the specified modem control lines.  s may contain DTR, RTS & Break. *)
		проц {перекрыта}ClearMC*(s: мнвоНаБитахМЗ);
		нач {единолично}
			mc := mc - s;
		кон ClearMC;

		(** Set the specified modem control lines.  s may contain DTR, RTS & Break. *)
		проц {перекрыта}SetMC*(s: мнвоНаБитахМЗ);
		нач {единолично}
			mc := mc + s;
		кон SetMC;

		(** Return the state of the specified modem control lines.  s contains
			the current state of DSR, CTS, RI, DCD & Break Interrupt. *)
		проц {перекрыта}GetMC*(перем s: мнвоНаБитахМЗ);
		нач {единолично}
			s := mc;
		кон GetMC;

		проц &Init*;
		нач
			нов(timer);
		кон Init;

	кон VirtualPort;

тип

	(* Note: If logging to the Kernel Log, be sure that associated real serial port is not the one which KernelLog uses *)
	PortSniffer = окласс(Serials.Port)
	перем
		port : Serials.Port;
		in, out : Потоки.Писарь;

		проц {перекрыта}Open*(bps, data, parity, stop : цел32; перем res: целМЗ);
		нач {единолично}
			port.Open(bps, data, parity, stop, res);
			если res = Serials.Ok то
				charactersSent := 0; charactersReceived := 0;
			всё;
		кон Open;

		проц {перекрыта}Закрой*;
		нач {единолично}
			port.Закрой;
		кон Закрой;

		проц {перекрыта}SendChar*(ch: симв8; перем res : целМЗ);
		нач {единолично}
			port.SendChar(ch, res);
			если res = Serials.Ok то
				если out # НУЛЬ то
					out.пСимв8(ch); out.ПротолкниБуферВПоток;
				иначе
					если Verbose то ЛогЯдра.пСимв8(ch); всё;
				всё;
				увел(charactersSent);
			иначе
				если Verbose то
					ShowModule; ЛогЯдра.пСтроку8("Error while sending '"); ЛогЯдра.пСимв8(ch); ЛогЯдра.пСтроку8("': ");
					ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;
				всё;
			всё;
		кон SendChar;

		(** Wait for the next character is received in the input buffer. The buffer is fed by HandleInterrupt *)
		проц {перекрыта}ReceiveChar*(перем ch: симв8; перем res: целМЗ);
		нач {единолично}
			port.ReceiveChar(ch, res);
			если res = Serials.Ok то
				если in # НУЛЬ то
					in.пСимв8(ch); in.ПротолкниБуферВПоток;
				иначе
					если Verbose то ЛогЯдра.пСимв8(ch); всё;
				всё;
				увел(charactersReceived);
			иначе
				если Verbose то ShowModule; ЛогЯдра.пСтроку8("Error while receiving: "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС; всё;
			всё;
		кон ReceiveChar;

		проц {перекрыта}Available*(): размерМЗ;
		нач {единолично}
			возврат port.Available();
		кон Available;

		(** Get the port state: state (open, closed), speed in bps, no. of data bits, parity, stop bit length. *)
		проц {перекрыта}GetPortState*(перем openstat : булево; перем bps, data, parity, stop : цел32);
		нач {единолично}
			port.GetPortState(openstat, bps, data, parity, stop);
		кон GetPortState;

		(** Clear the specified modem control lines. s may contain DTR, RTS & Break. *)
		проц {перекрыта}ClearMC*(s: мнвоНаБитахМЗ);
		нач {единолично}
			port.ClearMC(s);
		кон ClearMC;

		(** Set the specified modem control lines. s may contain DTR, RTS & Break. *)
		проц {перекрыта}SetMC*(s: мнвоНаБитахМЗ);
		нач {единолично}
			port.SetMC(s);
		кон SetMC;

		(** Return the state of the specified modem control lines. s contains the current state of DSR, CTS, RI, DCD & Break Interrupt. *)
		проц {перекрыта}GetMC*(перем s: мнвоНаБитахМЗ);
		нач {единолично}
			port.GetMC(s);
		кон GetMC;

		проц &Init*(port : Serials.Port; in, out : Потоки.Писарь);
		нач
			утв(port # НУЛЬ);
			сам.port := port; сам.in := in; сам.out := out;
		кон Init;

	кон PortSniffer;

перем
	active : массив Serials.MaxPorts+1 из булево;
	errorRate: вещ64;
	random: Random.Generator;

проц ShowModule;
нач
	ЛогЯдра.пСтроку8(ModuleName); ЛогЯдра.пСтроку8(": ");
кон ShowModule;

проц GetSlowdownValues(bps : цел32; перем eachNCharacters, waitForMs: цел32; перем res : целМЗ);
нач
	res := Serials.Ok;
	waitForMs := 1;
	если bps = 0 то waitForMs := 0; (* Don't limit speed *)
	аесли bps = 300 то eachNCharacters := 1; waitForMs := 4;
	аесли bps = 600 то eachNCharacters := 1; waitForMs := 2;
	аесли bps = 1200 то eachNCharacters := 1;
	аесли bps = 2400 то eachNCharacters := 2;
	аесли bps = 4800 то eachNCharacters := 4;
	аесли bps = 9600 то eachNCharacters := 8;
	аесли bps = 19200 то eachNCharacters := 16;
	аесли bps = 38400 то eachNCharacters := 32;
	аесли bps = 115200 то eachNCharacters := 100;
	аесли bps = 230400 то eachNCharacters := 200;
	аесли bps = 460800 то eachNCharacters := 400;
	аесли bps = 921600 то eachNCharacters := 800;
	иначе
		res := Serials.WrongBPS;
	всё;
кон GetSlowdownValues;

проц IsValidPortNumber(portNbr : цел32) : булево;
нач
	возврат (1 <= portNbr) и (portNbr <= Serials.MaxPorts);
кон IsValidPortNumber;

проц SendFileIntern(portNbr : цел32; конст filename : массив из симв8; loop : булево; context : Commands.Context);
перем
	port : Serials.Port;
	file : Files.File;
	len: размерМЗ; res : целМЗ;
	in : Files.Reader; out : Потоки.Писарь;
	buffer : массив BufferSize из симв8;
нач
	нач {единолично}
		если active[portNbr] то
			context.out.пСтроку8("Port is already used for data generation"); context.out.пВК_ПС;
			возврат;
		иначе
			active[portNbr] := истина;
		всё;
	кон;
	port := Serials.GetPort(portNbr);
	если port # НУЛЬ то
		file := Files.Old(filename);
		если file # НУЛЬ то
			port.Open(600, 8, 2, 2, res);
			если res = Serials.Ok то
				context.out.пСтроку8("Sending file "); context.out.пСтроку8(filename); context.out.пСтроку8(" to serial port "); context.out.пЦел64(portNbr, 0);
				если loop то context.out.пСтроку8(" [LOOP MODE]"); всё; context.out.пСтроку8("... ");
				нов(out, port.ЗапишиВПоток, BufferSize);
				Files.OpenReader(in, file, 0);
				нцДо
					in.чБайты(buffer, 0, BufferSize, len); out.пБайты(buffer, 0, len); out.ПротолкниБуферВПоток;
					если loop и (in.кодВозвратаПоследнейОперации = Потоки.КонецФайла) то Files.OpenReader(in, file, 0); всё;
				кцПри (in.кодВозвратаПоследнейОперации # Потоки.Успех) или (out.кодВозвратаПоследнейОперации # Потоки.Успех) или (active[portNbr] = ложь);
				context.out.пСтроку8("done."); context.out.пВК_ПС;
			иначе context.out.пСтроку8("Could not open port "); context.out.пЦел64(portNbr, 0); context.out.пСтроку8(", res: "); context.out.пЦел64(res, 0); context.out.пВК_ПС;
			всё;
			port.Закрой;
		иначе context.out.пСтроку8("Could not open file "); context.out.пСтроку8(filename); context.out.пВК_ПС;
		всё;
	иначе context.out.пСтроку8("Could not get serial port "); context.out.пЦел64(portNbr, 0); context.out.пВК_ПС;
	всё;
	нач {единолично}
		если active[portNbr] то active[portNbr] := ложь; всё;
	кон;
кон SendFileIntern;

(** Send the content of the specified file to the specified serial port. If the Loop parameter is used, the file is sent
	in a endless loop. Sending can be stopped using the StopSendFile commands *)
проц SendFile*(context : Commands.Context); (** portNbr filename [Loop] ~ *)
перем portNbr : цел32; filename, parString : массив Files.NameLength из симв8; loop : булево;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦел32(portNbr, ложь) и IsValidPortNumber(portNbr) то
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename) то
			если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(parString) и Строки8.ПодходитЛиПодМаскуИмениФайла¿(parString, "Loop") то loop := истина; всё;
			SendFileIntern(portNbr, filename, loop, context);
			context.out.пСтроку8("Started generator on port "); context.out.пЦел64(portNbr, 0);
			context.out.пСтроку8(" (File: "); context.out.пСтроку8(filename); context.out.пСтроку8(")"); context.out.пВК_ПС;
		иначе
			context.out.пСтроку8("Expected portNbr filename parameters. Could not read filename."); context.out.пВК_ПС;
		всё;
	иначе
		context.out.пСтроку8("Invalid port number"); context.out.пВК_ПС;
	всё;
кон SendFile;

(** Stop sending a file for the specified port *)
проц StopSendFile*(context : Commands.Context); (** portNbr ~ *)
перем portNbr : цел32;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦел32(portNbr, ложь) и IsValidPortNumber(portNbr) то
		нач {единолично}
			если active[portNbr] то
				active[portNbr] := ложь;
				context.out.пСтроку8("Stopped generator on port "); context.out.пЦел64(portNbr, 0); context.out.пВК_ПС;
			иначе
				context.out.пСтроку8("No generator running on port "); context.out.пЦел64(portNbr, 0); context.out.пВК_ПС;
			всё;
		кон;
	иначе
		context.out.пСтроку8("Invalid port number"); context.out.пВК_ПС;
	всё;
кон StopSendFile;

(** Installs two virtual serial ports which are linked to each other. Data sent by one port is received by the other and vice versa *)
проц Install*(context : Commands.Context); (** ~ *)
перем port1, port2 : VirtualPort; description : массив 128 из симв8;
нач
	нов(port1); нов(port2);
	port1.sender := port2.PutChar;
	port2.sender := port1.PutChar;

	description := "Virtual Serial Port";
	Serials.RegisterPort(port1, description);

	Строки8.ПодклейВСтрокуХвост(description, " (Linked to "); Строки8.ПодклейВСтрокуХвост(description, port1.name); Строки8.ПодклейВСтрокуХвост(description, ")");
	Serials.RegisterPort(port2, description);
кон Install;

(** Install a virtual sniffer port as proxy for the specified serial port *)
проц InstallSniffer*(context : Commands.Context); (** [portNbr] ~ *)
перем
	portSniffer : PortSniffer; port : Serials.Port;
	portNbr : цел32;
	description : массив 128 из симв8;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦел32(portNbr, ложь) и IsValidPortNumber(portNbr) то
		port := Serials.GetPort(portNbr);
		если port # НУЛЬ то
			нов(portSniffer, port, НУЛЬ, НУЛЬ);
			description := "Virtual Serial Port (Sniffer linked to ";
			Строки8.ПодклейВСтрокуХвост(description, port.name); Строки8.ПодклейВСтрокуХвост(description, ")");
			Serials.RegisterPort(portSniffer, description);
			context.out.пСтроку8("Registered serial port sniffer for port "); context.out.пЦел64(portNbr, 0); context.out.пВК_ПС;
		иначе
			context.out.пСтроку8("Port "); context.out.пЦел64(portNbr, 0); context.out.пСтроку8(" not found."); context.out.пВК_ПС;
		всё;
	иначе
		context.out.пСтроку8("Invalid port number"); context.out.пВК_ПС;
	всё;
кон InstallSniffer;

проц Cleanup;
перем portNbr : цел32;
нач
	нцДля portNbr := 1 до Serials.MaxPorts делай
		active[portNbr] := ложь;
	кц;
кон Cleanup;

проц SetErrorRate*(context: Commands.Context);
перем rate, divisor: цел32;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦел32(rate,ложь) и context.arg.ПропустиБелоеПолеИЧитайЦел32(divisor,ложь) то
		errorRate := rate  / divisor;
	всё;
кон SetErrorRate;


нач
	Modules.InstallTermHandler(Cleanup);
	нов(random); errorRate := 0;
кон SerialsVirtual.

SerialsVirtual.SetErrorRate 0 1 ~
SerialsVirtual.SetErrorRate 1 100000 ~

модуль W3dClusterWatch;	(** AUTHOR "TF"; PURPOSE "Simple 3d viewer"; *)


использует
(* Low level *)
	ЛогЯдра, Kernel, MathL, Потоки, Modules, Files, Commands,
	Строки8,
(* Window Manager *)
	WM := WMWindowManager, Rect := WMRectangles, Raster, WMGraphics,
(* Network *)
	 IP, DNS, TCP,
(* 3d framework *)
	Classes := TFClasses, Vectors := W3dVectors, Matrix := W3dMatrix,
	AbstractWorld := W3dAbstractWorld, World := W3dWorld, ObjectGenerator := W3dObjectGenerator,
(* XML framework *)
	XML, Scanner := XMLScanner, XMLParser, Objects := XMLObjects;

конст CATPort = 9999;
	NoCAT = 0;
	AosCAT = 1;
	LinuxCAT = 2;
	WindowsCAT = 3;
	SuseCAT = 4;
	UnknownCAT = 5;

	BoxDistance = 250;

перем OSColor : массив 6 из цел32;
	OSImg : массив 6 из Raster.Image;

тип
	UpdateProc = проц {делегат};
	Info = окласс
	перем
		pos : Vectors.TVector3d;
		host : массив 64 из симв8;
		os, oldos : массив 32 из симв8;
		load : массив 32 из симв8;
		running, oldrunning : цел32;
		obj : AbstractWorld.Object;
		timer : Kernel.Timer;
		interval : цел32;
		world : AbstractWorld.World;
		connection : TCP.Connection;

		alive : булево;

		index : цел32;

		render : UpdateProc;

		проц &Init*(world : AbstractWorld.World; pos : Vectors.TVector3d; host : массив из симв8; interval : цел32;
			update: UpdateProc);
		нач
			копируйСтрокуДо0(host, сам.host); сам.pos := pos; сам.world := world; сам.interval := interval; сам.render := update;
			нов(timer)
		кон Init;

		проц RefreshInfo(host: массив из симв8; перем osName, osLoad: массив из симв8) : булево;
		перем res: целМЗ;
			fip: IP.Adr;
			in : Потоки.Чтец;
			out : Потоки.Писарь;
		нач
			нов(connection);
			DNS.HostByName(host, fip, res);
			если res # 0 то возврат ложь всё;
			connection.Open(TCP.NilPort, fip, CATPort, res);
			если res # 0 то connection.Закрой; возврат ложь всё;
			Потоки.НастройЧтеца(in, connection.ПрочтиИзПотока); Потоки.НастройПисаря(out, connection.ЗапишиВПоток);
			(* query os *)
			out.пСтроку8("getOS"); out.пВК_ПС; out.ПротолкниБуферВПоток; in.чСтроку8ДоКонцаСтрокиТекстаВключительно(osName);
			(* query load *)
			out.пСтроку8("getLoad"); out.пВК_ПС; out.ПротолкниБуферВПоток; in.чСтроку8ДоКонцаСтрокиТекстаВключительно(osLoad);

			если connection # НУЛЬ то connection.Закрой всё;
			возврат in.кодВозвратаПоследнейОперации = 0
		кон RefreshInfo;

		проц Update;
		нач
			если RefreshInfo(host, os, load) то
				если MatchI(os, "Aos") то running := AosCAT
				аесли MatchI(os, "Linux") или MatchI(os, "RH_Linux") то running := LinuxCAT
				аесли MatchI(os, "WinNT") то running := WindowsCAT
				аесли MatchI(os, "SUSE") то running := SuseCAT
				иначе running := UnknownCAT; ЛогЯдра.пСтроку8("FAH : "); ЛогЯдра.пСтроку8(os); ЛогЯдра.пВК_ПС;
				всё
			иначе running := NoCAT
			всё;

			если running # oldrunning то
				если obj = НУЛЬ то obj := world.CreateObject(); obj.SetIndex(index); world.AddObject(obj) всё; obj.Clear;

				если OSImg[running] # НУЛЬ то
					ObjectGenerator.TexBox(Matrix.Translation4x4(pos.x, pos.y, pos.z),
							100, 100, 100, obj, OSColor[running], obj.AddTexture(OSImg[running]));
				иначе
					ObjectGenerator.Box(Matrix.Translation4x4(pos.x, pos.y, pos.z),
							100, 100, 100, obj, OSColor[running]);
				всё;
				oldrunning := running;
				render
			всё
		кон Update;

		проц Kill;
		нач
			alive := ложь;
			если connection # НУЛЬ то connection.Закрой всё;
			timer.Wakeup
		кон Kill;

	нач {активное}
		oldrunning := -1;
		alive := истина;
		нцПока alive делай
			Update;
			если alive то timer.Sleep(interval) всё
		кц;
	кон Info;

	Window = окласс ( WM.BufferWindow )
		перем
			alive, dead:булево;
			timer : Kernel.Timer;
			interval : цел32;

			(* Navigation *)
			lookat: Vectors.TVector3d;
			radius, angle, height : вещ64;
			mouseKeys : мнвоНаБитахМЗ;
			oldX, oldY : размерМЗ;

			(* 3d World *)
			world : World.World;
			mx, my, mz : вещ64;

			infoList : Classes.List;
			index : цел32;

		проц ParseLine(line : XML.Element; pos: Vectors.TVector3d);
		перем cont : Objects.Enumerator; p : динамическиТипизированныйУкль; el : XML.Element;s : Строки8.уСтрока;
			x: Info;
		нач
			cont := line.GetContents(); cont.Reset();
			нцПока cont.HasMoreElements() делай
				p := cont.GetNext();
				el := p(XML.Element);
				s := el.GetName(); если s^ = "Entry" то
					s := el.GetAttributeValue("Host");
					нов(x, world, pos, s^, interval, Render); x.index := index; увел(index); w.infoList.Add(x);
					x.pos := pos; mx := матМаксимум(pos.x, mx);
					pos.x := pos.x + BoxDistance
				всё
			кц
		кон ParseLine;

		проц ParseLayer(layer : XML.Element; pos : Vectors.TVector3d);
		перем cont : Objects.Enumerator; p : динамическиТипизированныйУкль; el : XML.Element;s : Строки8.уСтрока;
		нач
			cont := layer.GetContents(); cont.Reset();
			нцПока cont.HasMoreElements() делай
				p := cont.GetNext();
				el := p(XML.Element);
				s := el.GetName(); если s^ = "Line" то
					ParseLine(el, pos); mz := матМаксимум(pos.z, mz);
					pos.z := pos.z + BoxDistance
				всё
			кц
		кон ParseLayer;

		проц Load(filename: массив из симв8);
		перем f : Files.File;
			scanner : Scanner.Scanner;
			parser : XMLParser.Parser;
			reader : Files.Reader;
			doc : XML.Document;
			p : динамическиТипизированныйУкль;
			root: XML.Element;
			el : XML.Content;
			s : Строки8.уСтрока;
			cont : Objects.Enumerator;
			pos : Vectors.TVector3d;
		нач
			index := 1;
			f := Files.Old(filename);
			если f # НУЛЬ то
				нов(reader, f, 0);
				нов(scanner, reader); нов(parser, scanner); doc := parser.Parse()
			всё;

			root := doc.GetRoot();
			cont := root.GetContents(); cont.Reset();
			нцПока cont.HasMoreElements() делай
				p := cont.GetNext();
				el := p(XML.Element);
				если el суть XML.Element то
					s := el(XML.Element).GetName(); если s^ = "Layer" то
						ParseLayer(el(XML.Element), pos); my := матМаксимум(pos.z, my);
						pos.y := pos.y + BoxDistance;
					всё
				всё
			кц;
			lookat := Vectors.Vector3d(mx / 2, my / 2, mz / 2)
		кон Load;

		проц &New*(interval: цел32; fileName: массив из симв8);
		перем xpos, ypos : цел32;
				w, h : цел32;
		нач
			w := 400; h := 400;
			xpos := 20; ypos := 30;
			Init(w, h, ложь);

			сам.interval := interval;
			manager := WM.GetDefaultManager();
			manager.Add(xpos, ypos, сам, {WM.FlagFrame});
			manager.SetWindowTitle(сам, WM.NewString("Cluster Watch 3d"));

			(* Init navigation parameters *)
			radius := 2000; angle := 0; height := 0;

			(* Setup the 3d World *)
			нов(world, w, h, 0);
			world.quality := 1;
			нов(infoList);
			Load(fileName);

			(* Background box *)
			нов(timer)
		кон New;

		проц {перекрыта}Close*;
		перем i : размерМЗ; o : динамическиТипизированныйУкль; info : Info;
		нач {единолично}
			infoList.Lock;
			нцДля i := 0 до infoList.GetCount() - 1 делай o := infoList.GetItem(i); info := o(Info); info.Kill кц;
			infoList.Unlock;
			infoList.Clear;
			manager.Remove(сам);
			(*alive:=FALSE; timer.Wakeup *)
		кон Close;

		(* BEGIN Navigation and Rendering *)
		проц Render;
		перем pos, dir, up : Vectors.TVector3d;
		нач {единолично}
			pos := Vectors.VAdd3(lookat, Vectors.Vector3d(MathL.cos(angle) * radius, 0, MathL.sin(angle) * radius)); pos.y := height;
			lookat := Vectors.Vector3d(lookat.x, height, lookat.z);
			dir := Vectors.VNormed3(Vectors.VSub3(lookat, pos));
			up := Vectors.Vector3d(0, 1, 0);
			world.SetCamera(pos, dir, up); world.Render(img, ложь);
			Invalidate(Rect.MakeRect(0,0,img.width, img.height))
		кон Render;

		проц {перекрыта}PointerDown*(x, y:размерМЗ; keys:мнвоНаБитахМЗ);
		нач
			mouseKeys := (keys * {0, 1, 2});
			oldX := x; oldY := y;
			если mouseKeys = {1} то
				ЛогЯдра.пЦел64(world.GetOwnerIndex(x, y), 8); ЛогЯдра.пВК_ПС
			всё
		кон PointerDown;

		проц {перекрыта}PointerMove*(x, y: размерМЗ; keys : мнвоНаБитахМЗ);
		нач
			если mouseKeys * {0} # {} то
				если mouseKeys * {2} # {} то
					radius := radius - (y - oldY) * 10; если radius < 10 то radius := 10 всё;
				иначе
					height := height + (y - oldY)
				всё;
				angle := angle - (x - oldX) / img.width * 3.141;
				Render
			всё;
			oldX := x; oldY := y
		кон PointerMove;

		проц {перекрыта}PointerUp*(x, y:размерМЗ; keys:мнвоНаБитахМЗ);
		нач
			mouseKeys := (keys * {0, 1, 2});
		кон PointerUp;
		(* END Navigation and Rendering *)
	кон Window;

перем
	w: Window;
	timg: Raster.Image;
	mode : Raster.Mode;

	проц MatchI(перем buf: массив из симв8; with: массив из симв8): булево;
	перем i: цел32;
	нач
		i := 0; нцПока (with[i] # 0X) и (ASCII_вЗаглавную(buf[i]) = ASCII_вЗаглавную(with[i])) делай увел(i) кц;
		возврат with[i] = 0X
	кон MatchI;

проц Watch*(context : Commands.Context);
перем
	name : массив 100 из симв8;
	i, interval : цел32;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name);
	(* steps *)
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(interval, ложь);
	если interval = 0 то interval := 30000; всё;
	нов(w, interval, name);
кон Watch;

проц Cleanup;
кон Cleanup;

нач
	timg := WMGraphics.LoadImage("W3dClusterWatchIcons.tar://tux.bmp", истина);
	если timg # НУЛЬ то
		нов(OSImg[LinuxCAT]); Raster.Create(OSImg[LinuxCAT], timg.width, timg.height, Raster.BGR565);
		Raster.InitMode(mode, Raster.srcCopy);
		Raster.Copy(timg, OSImg[LinuxCAT], 0, 0, timg.width, timg.height, 0, 0, mode)
	всё;

	timg := WMGraphics.LoadImage("W3dClusterWatchIcons.tar://windows.bmp", истина);
	если timg # НУЛЬ то
		нов(OSImg[WindowsCAT]); Raster.Create(OSImg[WindowsCAT], timg.width, timg.height, Raster.BGR565);
		Raster.InitMode(mode, Raster.srcCopy);
		Raster.Copy(timg, OSImg[WindowsCAT], 0, 0, timg.width, timg.height, 0, 0, mode)
	всё;

	timg := WMGraphics.LoadImage("W3dClusterWatchIcons.tar://aos.gif", истина);
	если timg # НУЛЬ то
		нов(OSImg[AosCAT]); Raster.Create(OSImg[AosCAT], timg.width, timg.height, Raster.BGR565);
		Raster.InitMode(mode, Raster.srcCopy);
		Raster.Copy(timg, OSImg[AosCAT], 0, 0, timg.width, timg.height, 0, 0, mode)
	всё;

	timg := WMGraphics.LoadImage("W3dClusterWatchIcons.tar://suse.bmp", истина);
	если timg # НУЛЬ то
		нов(OSImg[SuseCAT]); Raster.Create(OSImg[SuseCAT], timg.width, timg.height, Raster.BGR565);
		Raster.InitMode(mode, Raster.srcCopy);
		Raster.Copy(timg, OSImg[SuseCAT], 0, 0, timg.width, timg.height, 0, 0, mode)
	всё;

	timg := WMGraphics.LoadImage("W3dClusterWatchIcons.tar://nocat.bmp", истина);
	если timg # НУЛЬ то
		нов(OSImg[NoCAT]); Raster.Create(OSImg[NoCAT], timg.width, timg.height, Raster.BGR565);
		Raster.InitMode(mode, Raster.srcCopy);
		Raster.Copy(timg, OSImg[NoCAT], 0, 0, timg.width, timg.height, 0, 0, mode)
	всё;

	timg := WMGraphics.LoadImage("W3dClusterWatchIcons.tar://unknowncat.bmp", истина);
	если timg # НУЛЬ то
		нов(OSImg[UnknownCAT]); Raster.Create(OSImg[UnknownCAT], timg.width, timg.height, Raster.BGR565);
		Raster.InitMode(mode, Raster.srcCopy);
		Raster.Copy(timg, OSImg[UnknownCAT], 0, 0, timg.width, timg.height, 0, 0, mode)
	всё;

	OSColor[NoCAT] := 0AAAAAAH; OSColor[AosCAT] := 0FF0000H; OSColor[LinuxCAT] := 0FFFF00H;
	OSColor[WindowsCAT] := 008080H; OSColor[SuseCAT] := 0FFH; OSColor[UnknownCAT] := 0FFFFFFH;
	Modules.InstallTermHandler(Cleanup)
кон W3dClusterWatch.

(*
Aos.Call W3dClusterWatch.Watch Cluster.XML 60000~

Compiler.Compile \s TFVectors.Mod TFMatrix.Mod TFGeometry.Mod TFAbstractWorld.Mod TFObjectGenerator.Mod
Float.TFRasterizer3d.Mod TFWorld3d.Mod TFExplorer.Mod ClusterWatch3d.Mod~

PC.Compile \s TFVectors.Mod TFMatrix.Mod TFGeometry.Mod TFAbstractWorld.Mod TFObjectGenerator.Mod
Float.TFRasterizer3d.Mod TFWorld3d.Mod TFExplorer.Mod ClusterWatch3d.Mod ~
~

System.Free W3dClusterWatch W3dWorld W3dRasterizer W3dObjectGenerator W3dAbstractWorld W3dGeometry W3dMatrix W3dVectors~

(* FILES *)
Cluster.XML ClusterWatch3d.Mod TFAbstractWorld.Mod TFWorld3d.Mod TFObjectGenerator.Mod

EditTools.OpenUnix Cluster.XML
*)~

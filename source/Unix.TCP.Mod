(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль TCP;   (** AUTHOR "pjm, mvt, G.F."; PURPOSE "TCP protocol"; *)


использует Out := ЛогЯдра, IP, Потоки,  Unix, Sockets, Objects;

конст
	NilPort* = 0;

	(** Error codes *)
	Ok* = 0;
	ConnectionRefused* = 3701;
	ConnectionReset* = 3702;
	WrongInterface* = 3703;
	TimedOut* = 3704;
	NotConnected* = 3705;
	NoInterface* = 3706;
	InterfaceClosed* = 3707;


	(** TCP connection states *)
	NumStates* = 4;
	Closed* = 0;
	Listen* = 1;
	Established* = 2;
	Unused* = 4;   (* no real state, only used in this implementation *)

	OpenStates* = {Listen, Established};
	ClosedStates* = {Unused, Closed};
	HalfClosedStates* = ClosedStates + {};
	FinStates* = {Unused, Closed};

перем
	trace: булево;

тип
	Sender = окласс
		перем
			conn: Connection;

			проц &New( c: Connection );
			нач
				conn := c
			кон New;

			проц SendPacket( конст data: массив из симв8;  ofs, len: размерМЗ ): булево;
			перем n: размерМЗ;
			нач {единолично}
				нцПока len > 0 делай
					n := len;
					если ~Sockets.Send( conn.socket, data, ofs, n ) то  возврат ложь  всё;
					умень( len, n );  увел( ofs, n )
				кц;
				возврат истина
			кон SendPacket;

	кон Sender;


тип
	(** Connection object.
		NOTE: Only one process should access a Connection!  *)
	Connection* = окласс (Потоки.ФункциональныйИнтерфейсКПотоку)
			перем
				int-	: IP.Interface;	(*! Unix port: dummy, only 'int.localAdr' contains valid data *)

				lport-	: цел32;
				fip-	: IP.Adr;  (* foreign protocol address *)
				fport-	: цел32;
				state*	: цел8;   (* TCP state *)

				socket	: цел32;
				sender: Sender;

				(* the next variables are for interface compatibility only *)
				irs-		: цел32;	(* initial receive sequence number *)
				rcvnxt-	: цел32;	(* receive next *)
				iss-		: цел32;	(* initial send sequence number *)
				sndnxt-	: цел32;	(* send next *)


				проц & Init*;
				нач
					state := Unused;
					нов( sender, сам );
					irs := 0; iss := 0;  rcvnxt := 0;  sndnxt := 0
				кон Init;



				(** Open a TCP connection (only use once per Connection instance).
					Use TCP.NilPort for lport to automatically assign an unused local port.*)
				проц Open*( lport: цел32;  fip: IP.Adr;  fport: цел32;  перем res: целМЗ );
				перем
					localAdr, foreignAdr: Sockets.SocketAdr;
				нач {единолично}
					утв( (state = Unused) и (lport >= 0) и (lport < 10000H) и (fport >= 0) и (fport < 10000H) );
					если trace то  Out.пСтроку8( "Open connection " )  всё;
					socket := Sockets.Socket( Unix.AFINET, Unix.SockStream, Unix.IpProtoTCP );
					если socket # 0 то
						если (~IP.IsNilAdr( fip )) и (fport # NilPort) то
							если trace то  Out.пСтроку8( "(inout) " )  всё;
							(* active open (connect) *)
							foreignAdr := Sockets.NewSocketAdr( fip, fport );
							если Sockets.Connect( socket, foreignAdr ) то
								неважно Sockets.SetLinger( socket );
								сам.fip := fip;  сам.fport := fport;
								localAdr := Sockets.GetSockName( socket );
								сам.lport := Sockets.GetPortNumber( localAdr );
								state := Established;
								res := Ok
							иначе
								Out.пСтроку8( "connect failed" ); Out.пВК_ПС;
								Sockets.Close( socket );  res := ConnectionRefused
							всё
						иначе
							если trace то  Out.пСтроку8( "(listen) " )  всё;
							(* passive open (listen) *)
							утв( (fport = NilPort) и (IP.IsNilAdr( fip )) );
							localAdr := Sockets.NewSocketAdr( IP.NilAdr, lport );
							если Sockets.Bind( socket, localAdr ) то
								localAdr := Sockets.GetSockName( socket );
								сам.lport := Sockets.GetPortNumber( localAdr );
								если Sockets.Listen( socket ) то
									неважно Sockets.SetLinger( socket );
									сам.fip := IP.NilAdr;
									state := Listen;  res := Ok
								иначе  Sockets.Close( socket );  res := NotConnected
								всё
							иначе  Sockets.Close( socket );  res := NotConnected
							всё
						всё
					иначе
						Out.пСтроку8( "open socket failed" );  Out.пВК_ПС;
						res := NotConnected
					всё;
					если res = Ok то
						(* create a dummy interface with correct local IP-adr *)
						нов( int, Sockets.SockAdrToIPAdr( localAdr ) )
					всё;
					если trace то
						если res = Ok то
							Out.пСтроку8( "socket=" );  Out.пЦел64( socket, 0 );
							Out.пСтроку8( ", locport=" );  Out.пЦел64( сам.lport, 0 );
							Out.пСтроку8( " done." )
						иначе
							Out.пСтроку8( " failed." )
						всё;
						Out.пВК_ПС
					всё;
				кон Open;


				(** Send data on a TCP connection. *)
				проц {перекрыта}ЗапишиВПоток*( конст data: массив из симв8;  ofs, len: размерМЗ;  propagate: булево;  перем res: целМЗ );
				нач
					если trace то  Out.пСтроку8( "Send: socket=" );  Out.пЦел64( socket, 0 )  всё;
					если state = Established то
						если sender.SendPacket( data, ofs, len ) то
							res := Ok
						иначе
							res := ConnectionReset
						всё
					иначе
						res := NotConnected (* Send on a Connection with state=Listen *)
					всё;
					увел( sndnxt )
				кон ЗапишиВПоток;

				(** Receive data on a TCP connection. The data parameter specifies the buffer.
					The ofs parameters specify the position in the buffer where data should be received (usually 0),
					and the size parameters specifies how many bytes of data can be received in the buffer.
					The min parameter specifies the minimum number of bytes to receive before Receive returns
					and must by <= size. The len parameter returns the number of bytes received, and the
					res parameter returns 0 if ok, or a non-zero error code otherwise (e.g. if the connection is closed
					by the communication partner, or by a call of the Close method). *)

				проц {перекрыта}ПрочтиИзПотока*( перем data: массив из симв8;  ofs, size, min: размерМЗ;  перем len: размерМЗ; перем res: целМЗ );
				перем p, x: размерМЗ;
				нач {единолично}
					утв( (ofs >= 0) и (ofs + size <= длинаМассива( data )) и (min <= size) );   (* parameter consistency check *)
					если trace то
						Out.пСтроку8( "Receive: socket=" );  Out.пЦел64( socket, 0 );
						Out.пСтроку8( " min=" );  Out.пЦел64( min, 0 );
						p := ofs
					всё;
					len := 0;  res := Ok;
					если size = 0 то  возврат  всё;
					если state в {Listen, Established} то
						нц
							x := size;
							если Sockets.Recv( socket, data, ofs, x, 0 ) то
								если x > 0 то
									умень( size, x );  увел( len, x );  увел( ofs, x );
									если len >= min то
										увел( rcvnxt );
										возврат
									всё
								иначе
									(* x = 0: closed by peer *)
									Sockets.Close( socket );  state := Closed;
									res := NotConnected;  возврат
								всё
							иначе
								Sockets.Close( socket );  state := Closed;
								res := NotConnected;  возврат
							всё
						кц; (* loop *)
					иначе
						res := NotConnected
					всё;
					увел( rcvnxt )
				кон ПрочтиИзПотока;



				(** Enable or disable delayed send (Nagle algorithm).
					If enabled, the sending of a segment is delayed if it is not filled by one call to Send, in order to be able
					to be filled by further calls to Send. This is the default option.
					If disabled, a segment is sent immediatly after a call to Send, even if it is not filled.
					This option is normally chosen by applications like telnet or VNC client, which send verly little data but
					shall not be delayed.*)
				проц DelaySend*( enable: булево );
				перем ignore: булево;
				нач {единолично}
					ignore := Sockets.NoDelay( socket, ~enable )
				кон DelaySend;

				(** Enable or disable keep-alive. (default: disabled) *)
				проц KeepAlive*( enable: булево );
				перем ignore: булево;
				нач {единолично}
					ignore := Sockets.KeepAlive( socket, enable )
				кон KeepAlive;


				(** Return number of bytes that may be read without blocking. *)
				проц Available*( ): цел32;
				перем available: цел32;
				нач {единолично}
					если state в {Established, Listen} то
						если Sockets.Requested( socket ) то
							available := Sockets.Available( socket );
							если available >= 0 то
								возврат available
							всё;
						всё
					всё;
					возврат  0
				кон Available;

				(** Return connection state. *)
				проц State*( ): цел32;
				нач
					возврат state
				кон State;

				(** Wait until the connection state is either in the good or bad set, up to "ms" milliseconds. *)
				проц AwaitState*( good, bad: мнвоНаБитахМЗ;  ms: цел32;  перем res: целМЗ );
				нач
					нцПока (ms > 0) и ~(state в (good+bad)) делай  Objects.Sleep( 10 );  умень( ms, 10 )  кц;
					если state в good то
						res := Ok
					аесли state в bad то
						res := NotConnected
					иначе
						res := TimedOut
					всё
				кон AwaitState;

				(** Close a TCP connection (half-close). *)
				проц {перекрыта}Закрой*;
				нач
					Sockets.Close( socket );  state := Closed;
				кон Закрой;

				(** Discard a TCP connection (shutdown). *)
				проц Discard*;
				нач
					Sockets.Close( socket );  state := Closed;
				кон Discard;

				(** Accept a client waiting on a listening connection. Blocks until a client is available or the
					  connection is closed. *)
				проц Accept*( перем client: Connection;  перем res: целМЗ );
				перем newsocket: цел32;  peerAdr: Sockets.SocketAdr;
				нач {единолично}
					если trace то
						Out.пСтроку8( "Accept: socket=" ); Out.пЦел64( socket, 0 );  Out.пСтроку8( " ... " )
					всё;
					если state = Listen то
						newsocket := Sockets.Accept( socket );
						если newsocket > 0 то
							peerAdr := Sockets.GetPeerName( newsocket );
							нов( client );
							client.int := int;
							client.socket := newsocket;
							client.state := Established;
							client.fip := Sockets.SockAdrToIPAdr( peerAdr );
							client.fport := Sockets.GetPortNumber( peerAdr );
							если trace то
								Out.пСтроку8( "Accept done, client socket=" );  Out.пЦел64( newsocket, 0 );  Out.пВК_ПС
							всё;
							res := Ok
						иначе
							res := NotConnected ;
							если trace то  Out.пСтроку8( "Accept failed." );  Out.пВК_ПС  всё
						всё;
					иначе
						res := NotConnected ;
						если trace то  Out.пСтроку8( "Accept failed (state # Listen)." );  Out.пВК_ПС  всё
					всё;
				кон Accept;

				(** Return TRUE iff a listening connection has clients waiting to be accepted. *)
				проц Requested*( ): булево;
				нач {единолично}
					возврат (state = Listen) и Sockets.Requested( socket )
				кон Requested;


			кон Connection;




	(** Aos command - display all errors *)
	проц DisplayErrors*( par: динамическиТипизированныйУкль ): динамическиТипизированныйУкль;
	нач
		возврат НУЛЬ;
	кон DisplayErrors;


	(** Aos command - discard and finalize all connections *)
	проц DiscardAll*( par: динамическиТипизированныйУкль ): динамическиТипизированныйУкль;
	нач
		возврат НУЛЬ;
	кон DiscardAll;


	(** Temporary trace procedure. *)
	проц ToggleTrace*;
	нач
		trace := ~trace;
		Out.ЗахватВЕдиноличноеПользование;
		Out.пСтроку8( "TCP trace " );
		если trace то  Out.пСтроку8( "on" )  иначе  Out.пСтроку8( "off" )  всё;
		Out.ОсвобождениеИзЕдиноличногоПользования
	кон ToggleTrace;


кон TCP.


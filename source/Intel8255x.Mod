модуль Intel8255x;

(*
Aos driver for Intel 8255x Ethernet Controllers

Reference: Intel, "Intel 8255x 10/100 Mbps Ethernet Controller Family, Open Source Software Developer Manual,
Revision 1.0, January 2003"
*)

использует НИЗКОУР, Kernel, ЭВМ, PCI, Objects, Modules, Plugins, Network, ЛогЯдра;

конст
	Name = "Intel8255x#";
	Desc = "Intel 8255x Ethernet Driver";
	K = 1024;
	MaxETHFrameSize = 1514;
	RxRingSize = 100;
	TxRingSize = 100;
	MaxTxTrials = 5;

	SizeOfConfigCmdHdr = 32;
	SizeOfNOPCmdHdr = 8;
	SizeOfIASetupCmdHdr = 16;
	SizeOfTxCmdHdr = 16;
	SizeOfRFDHdr = 16;

	keepMasks = истина;

	(* device registers *)
	SCBStatus = 0H;
	SCBCommand = 2H;
	SCBGenPtr = 4H;
	SCBEeprom = 0EH;
	PORTReg = 8H;

	(* ack bits *)
	CX = {15};
	FR = {14};
	CNA = {13};
	RNR = {12};
	MDI = {11};
	SWI = {10};

	(* interrupt masks *)
	CXMask = {31};
	FRMask = {30};
	CNAMask = {29};
	RNRMask = {28};
	ERMask = {27};
	FCPMask = {26};
	MaskAllIntr = {24};
	UnMaskAllIntr = {};

	(* generate software interrupt *)
	SI = {25};

	(* CU Commands *)
	CUNop = {};
	CUStart = {20};
	CUResume = {21};
	CULoadBase = {21, 22};

	(* RU Commands *)
	RUNop = {};
	RUStart = {16};
	RUResume = {17};
	RUAbort = {18};
	RULoadBase = {17, 18};

	(* CU States *)
	CUIdle = {};
	CUSuspended = {6};

	(* RU States *)
	RUIdle = {};
	RUSuspended = {2};
	RUReady = {4};

	(* PORT Selection Function *)
	Reset = {};
	SelectiveReset = {1};

	(* Action Command Opcodes in Control Block List (CBL) *)
	ActionCmdNOP = {};
	ActionCmdIASetup = {16};
	ActionCmdConfig = {17};
	ActionCmdTx = {18};

	(* More Command Bits in CBL *)
	LastBlock = {31};	(* EL Bit *)
	Suspend = {30};	(* S Bit *)
	Interrupt = {29};	(* I Bit *)

перем
	nCUWaitActive: цел32;

тип
	ByteField = укль на массив из симв8;

	DataBlock = укль на запись
		next: DataBlock;
		size: цел32;
		data: ByteField;
	кон;

	LinkDevice = окласс(Network.LinkDevice)
		перем
			ctrl: Controller;
			txTrials: цел32;

		(*
			send a frame
			padding and checksum are inserted directly by the device
		*)
		проц {перекрыта}DoSend*(dst: Network.LinkAdr; type: цел32; конст l3hdr, l4hdr, data: массив из симв8; h3len, h4len, dofs, dlen: цел32);
		конст
			DataOfs = 10H;	(* data block offset in transmit command block *)
			C = 15;
		перем
			actAdr, prevAdr: адресВПамяти; txLen, i: цел32;
			byteCount, EOF, TxThreshold, state, cmdHdr, sendStatus: мнвоНаБитахМЗ;
		нач {единолично}
			(* if C Bit in next TxCmd Block is not set, the transmit command has not yet finished processing all bytes
				=> buffer overflow (wait)
			*)
			ctrl.ExecCmd(MaskAllIntr, ~keepMasks);

			нцДо
				sendStatus := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, НИЗКОУР.прочти32битаПоАдресу(адресОт(ctrl.actTxCmd.next.data[0])));
			кцПри (C в sendStatus);

			txLen := 14 + h3len + h4len + dlen;	(* number of bytes to transmit *)
			prevAdr := адресОт(ctrl.actTxCmd.data[0]);

			ctrl.actTxCmd := ctrl.actTxCmd.next;
			actAdr := адресОт(ctrl.actTxCmd.data[0]);

			(* setup cmd hdr *)
			НИЗКОУР.запиши32битаПоАдресу(actAdr, Suspend + ActionCmdTx);

			(* in simplified mode the TBD array address should be set to all ones *)
			НИЗКОУР.запиши32битаПоАдресу(actAdr + 08H, 0FFFFFFFFH);

			(* set TxThreshold, EOF Bit, Tx Command Block Byte Count *)
			byteCount := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, txLen) * {0..13};
			EOF := {15};
			TxThreshold := {16};
			НИЗКОУР.запиши32битаПоАдресу(actAdr + 0CH, byteCount + EOF + TxThreshold);

			(* set data field *)
			НИЗКОУР.копируйПамять(адресОт(dst[0]), actAdr + DataOfs, 6);	(* the first 6 bytes of data field are dst address *)
			НИЗКОУР.копируйПамять(адресОт(local[0]), actAdr + DataOfs + 6, 6);
			НИЗКОУР.запиши16битПоАдресу(actAdr + DataOfs + 12, вращБит(НИЗКОУР.подмениТипЗначения(цел16, устарПреобразуйКБолееУзкомуЦел(type)), 8));
			i := 14;
			если h3len > 0 то НИЗКОУР.копируйПамять(адресОт(l3hdr[0]), actAdr + DataOfs + i, h3len); увел(i, h3len) всё;
			если h4len > 0 то НИЗКОУР.копируйПамять(адресОт(l4hdr[0]), actAdr + DataOfs + i, h4len); увел(i, h4len) всё;
			если i+dlen < MaxETHFrameSize то
				НИЗКОУР.копируйПамять(адресОт(data[0])+dofs, actAdr + DataOfs + i, dlen);
				увел(i, dlen);
			всё;

			(* delete Suspended Bit from previous TxCmd *)
			state := ctrl.GetCUState();
			нцПока (state # CUIdle) и (state # CUSuspended) делай
				ЭВМ.атомарноУвел(nCUWaitActive);
				state := ctrl.GetCUState();
			кц;
			cmdHdr := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, НИЗКОУР.прочти32битаПоАдресу(prevAdr)) - Suspend;
			НИЗКОУР.запиши32битаПоАдресу(prevAdr, cmdHdr);

			state := ctrl.GetCUState();
			если state = CUIdle то
				actAdr := ЭВМ.ДайФизическийАдресДиапазонаПамяти(actAdr, ctrl.actTxCmd.size);
				ctrl.WriteSCBGenPtr(actAdr(ЭВМ.Address32));
				ctrl.ExecCmd(CUStart, keepMasks);
			аесли state = CUSuspended то
				ctrl.ExecCmd(CUResume, keepMasks);
			всё;

			увел(sendCount);
			ctrl.ExecCmd(UnMaskAllIntr, ~keepMasks);
			(* ctrl.ExecCmd(CXMask + CNAMask + ERMask, ~keepMasks); *)
		кон DoSend;

		проц ReceiveData(перем data: массив из симв8; ofs, size: цел32);
		нач
			утв(size <= ctrl.rcvSize);	(* enough data left *)
			утв((size >= 0) и (ofs+size <= длинаМассива(data)));	(* index check *)
			НИЗКОУР.копируйПамять(ctrl.rcvAdr, адресОт(data[ofs]), size);
			увел(ctrl.rcvAdr, size);
			умень(ctrl.rcvSize, size);
		кон ReceiveData;

		проц {перекрыта}Finalize(connected: булево);
		нач
			ctrl.Finalize;
			Finalize^(connected);
		кон Finalize;

	кон LinkDevice;

	Controller = окласс
		перем
			next: Controller;
			base: адресВПамяти; irq: цел32;
			dev: LinkDevice;
			actRFD, lastRFD: DataBlock;
			actTxCmd: DataBlock;
			rcvAdr, rcvSize: цел32;

		проц &Init*(dev: LinkDevice; base: адресВПамяти; irq: цел32);
		перем res: целМЗ; i: цел32;
			configCmd, iASetupCmd: DataBlock;
		нач
			сам.next := installedControllers; installedControllers := сам;

			сам.base := base;
			сам.irq := irq;
			сам.dev := dev;
			dev.ctrl := сам;

			(* set Ethernet Broadcast Address *)
			нцДля i := 0 до 5 делай
				dev.broadcast[i] := 0FFX;
			кц;

			WritePORT(Reset); Delay(1);

			ExecCmd(MaskAllIntr, ~keepMasks);	(* mask all interrupts, do not keep any old masks *)

			WriteSCBGenPtr(0);
			ExecCmd(CULoadBase, keepMasks);	(* set CU base, keeping all interrupt masks *)

			WriteSCBGenPtr(0);
			ExecCmd(RULoadBase, keepMasks);	(* set RU base, keeping all interrupt masks *)

			(* configure the device *)
			MakeBlock(configCmd, SizeOfConfigCmdHdr);
			SetCmdHdr(LastBlock + ActionCmdConfig, configCmd);
			SetByteMap8255x(configCmd);
			StartActionCmd(configCmd);

			(* load the device with the individual address (MAC address) *)
			MakeBlock(iASetupCmd, SizeOfIASetupCmdHdr);
			SetCmdHdr(LastBlock + ActionCmdIASetup, iASetupCmd);
			SetMACAddress(iASetupCmd);
			StartActionCmd(iASetupCmd);

			SetupRxRing();
			StartRxUnit();

			SetupTxRing();

			(* install interrupt handler *)
			Objects.InstallHandler(сам.HandleInterrupt, ЭВМ.IRQ0+irq);

			(* mask interrupts *)
			(* ExecCmd(UnMaskAllIntr, ~keepMasks); *)
			ExecCmd(CXMask + CNAMask + ERMask, ~keepMasks);

			(* register with Network *)
			Network.registry.Add(dev, res); утв(res = Plugins.Ok);
			увел(installed);

			ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пСтроку8("  "); Network.OutLinkAdr(dev.local, 6); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
		кон Init;

		(*
			setup the IASetup Command and put the MAC in dev.local
			if the MAC address is aabbccddeeffH:
			dev.local[0] is aa
			dev.local[1] is bb
			dev.local[2] is cc
			etc.
		*)
		проц SetMACAddress(перем cmd: DataBlock);
		конст
			MacAdrBase = 0;
		перем reg, i: цел16;
		нач
			(* read permanent MAC address *)
			нцДля i := 0 до 2 делай
				ReadEEPROM(MacAdrBase + i, reg);	(* EEPROM registers are 2 bytes *)
				НИЗКОУР.запиши16битПоАдресу(адресОт(dev.local[2*i]), reg);	(* put MAC in dev.local *)
				НИЗКОУР.запиши16битПоАдресу(адресОт(cmd.data[8 + 2*i]), reg);	(* put MAC in Action Command Block *)
			кц;
		кон SetMACAddress;

		проц SetByteMap8255x(перем cmd: DataBlock);
		перем
			byteArray: массив 22 из симв8;

			проц ToChar(s: мнвоНаБитахМЗ): симв8;
			нач
				возврат НИЗКОУР.подмениТипЗначения(симв8, устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(НИЗКОУР.подмениТипЗначения(цел32, s))));
			кон ToChar;

		нач
			byteArray[0] := 16X;
			byteArray[1] := 8X;
			byteArray[2] := 00X;
			byteArray[3] := 00X;	(* MWI disable *)
			byteArray[4] := 00X;
			byteArray[5] := 00X;
			byteArray[6] := ToChar({5, 4, 1});
			byteArray[7] := ToChar({1, 0});	(* discard short frames (frames < 64 bytes) *)
			byteArray[8] := 01X;
			byteArray[9] := 00X;
			byteArray[10] := ToChar({5, 3, 2, 1});	(* NO src adr insertion (from internal dev IA) *)
			byteArray[11] := 00X;
			byteArray[12] := ToChar({6, 5, 0});
			byteArray[13] := 00X;	(* default *)
			byteArray[14] := 0F2X;	(* default *)
			byteArray[15] := ToChar({3});
			byteArray[16] := 00X;
			byteArray[17] := ToChar({6});	(* for compatibility reason *)
			byteArray[18] := ToChar({7, 6, 5, 4, 1});	(* enable padding *)
			byteArray[19] := ToChar({7});
			byteArray[20] := ToChar({0..5});	(* prio field in byte #31 in flow control frame *)
			byteArray[21] := ToChar({2, 0});

			НИЗКОУР.копируйПамять(адресОт(byteArray[0]), адресОт(cmd.data[0]) + 08H, 22);
		кон SetByteMap8255x;

		проц SetupTxRing;
		перем
			r: цел32;
			adr, physAdr: адресВПамяти;
			txCmd, prev: DataBlock;
		нач
			нцДля r := 0 до TxRingSize - 1 делай
				MakeBlock(txCmd, SizeOfTxCmdHdr + MaxETHFrameSize);

				adr := адресОт(txCmd.data[0]);
				НИЗКОУР.запиши32битаПоАдресу(adr, {15});	(* set C Bit for LinkDevice.Send => no C Bit means Send Buffer Overflow *)
				если prev # НУЛЬ то
					prev.next := txCmd;
					physAdr := ЭВМ.ДайФизическийАдресДиапазонаПамяти(adr, txCmd.size); утв(physAdr # ЭВМ.НулевойАдрес);
					НИЗКОУР.запиши32битаПоАдресу(адресОт(prev.data[0]) + 04H, physAdr);	(* set link address to physical address *)
				иначе
					actTxCmd := txCmd;	(* set first TxCmd *)
				всё;
				prev := txCmd;
			кц;
			(* link last TxCmd to first TxCmd *)
			txCmd.next := actTxCmd;
			physAdr := ЭВМ.ДайФизическийАдресДиапазонаПамяти(адресОт(actTxCmd.data[0]), actTxCmd.size);
			утв(physAdr # ЭВМ.НулевойАдрес);
			adr := адресОт(txCmd.data[0]);
			НИЗКОУР.запиши32битаПоАдресу(adr + 04H, physAdr(ЭВМ.Address32));
		кон SetupTxRing;

		проц SetupRxRing;
		перем
			r: цел32;
			adr, physAdr: адресВПамяти;
			rxFrame, prev: DataBlock;
		нач
			нцДля r := 0 до RxRingSize - 1 делай
				MakeBlock(rxFrame, SizeOfRFDHdr + MaxETHFrameSize);
				(* configure RFD *)
				adr := адресОт(rxFrame.data[0]);
				НИЗКОУР.запиши32битаПоАдресу(adr, 0);
				НИЗКОУР.запиши32битаПоАдресу(adr + 0CH, 0);
				НИЗКОУР.запиши16битПоАдресу(adr + 0CH + 2H, устарПреобразуйКБолееУзкомуЦел(rxFrame.size));
				если prev # НУЛЬ то
					prev.next := rxFrame;
					physAdr := ЭВМ.ДайФизическийАдресДиапазонаПамяти(adr, rxFrame.size); утв(physAdr # ЭВМ.НулевойАдрес);
					НИЗКОУР.запиши32битаПоАдресу(адресОт(prev.data[0]) + 4H, physAdr);	(* set link address to physical address *)
				иначе
					actRFD := rxFrame;	(* set first RFD *)
				всё;
				prev := rxFrame;
			кц;
			lastRFD := rxFrame;
			(* link last RFD TO first RFD *)
			lastRFD.next := actRFD;
			physAdr := ЭВМ.ДайФизическийАдресДиапазонаПамяти(адресОт(actRFD.data[0]), actRFD.size); утв(physAdr # ЭВМ.НулевойАдрес);
			adr := адресОт(lastRFD.data[0]);
			НИЗКОУР.запиши32битаПоАдресу(adr + 4H, physAdr(ЭВМ.Address32));
			НИЗКОУР.запиши32битаПоАдресу(adr, Suspend);	(* after having received the last block, suspend reception of further frames *)
		кон SetupRxRing;

		проц StartRxUnit;
		перем
			adr: адресВПамяти;
		нач
			adr := ЭВМ.ДайФизическийАдресДиапазонаПамяти(адресОт(actRFD.data[0]), actRFD.size); утв(adr # ЭВМ.НулевойАдрес);
			WriteSCBGenPtr(adr(ЭВМ.Address32));
			утв(GetRUState() # RUReady);
			ExecCmd(RUStart, keepMasks);
		кон StartRxUnit;

		проц SetCmdHdr(bits: мнвоНаБитахМЗ; перем cmd: DataBlock);
		нач
			НИЗКОУР.запиши32битаПоАдресу(адресОт(cmd.data[0]), bits * {16..31});	(* set status word bits to 0 *)
		кон SetCmdHdr;

		проц MakeBlock(перем cmd: DataBlock; dataSize: цел32);
		нач
			нов(cmd);
			cmd.size := dataSize;
			нов(cmd.data, cmd.size);
		кон MakeBlock;

		проц AckIntr(interrupts: мнвоНаБитахМЗ);
		нач
			НИЗКОУР.запиши16битПоАдресу(base + SCBStatus, устарПреобразуйКБолееУзкомуЦел(НИЗКОУР.подмениТипЗначения(цел32, interrupts)));
		кон AckIntr;

		проц GetStatus():мнвоНаБитахМЗ;
		нач
			возврат НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, устарПреобразуйКБолееШирокомуЦел(НИЗКОУР.прочти16битПоАдресу(base + SCBStatus)));
		кон GetStatus;

		проц GetCUState(): мнвоНаБитахМЗ;
		нач
			возврат GetStatus() * {6, 7};
		кон GetCUState;

		проц GetRUState(): мнвоНаБитахМЗ;
		нач
			возврат GetStatus() * {2..5};
		кон GetRUState;

		(*
			set the SCB command word
			keep indicates if interrupt masks of upper byte are deleted
		*)
		проц ExecCmd(cmd: мнвоНаБитахМЗ; keep: булево);
		перем masks: мнвоНаБитахМЗ;
		нач
			cmd := cmd * {16..31};	(* delete status part of cmd *)
			если keep то
				masks := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, НИЗКОУР.прочти32битаПоАдресу(base + SCBStatus));	(* get interrupt masks *)
				masks := masks * {24..31};	(* delete all but the mask bits *)
				cmd := cmd + masks;	(* merge cmd with interrupt mask *)
			всё;
			НИЗКОУР.запиши32битаПоАдресу(base + SCBStatus, cmd);	(* writing zeros to status word has no effect *)
			нцПока (НИЗКОУР.прочти8битПоАдресу(base + SCBCommand) # 0) делай кц;	(* wait for command done *)
		кон ExecCmd;

		проц WaitForActionCmd(перем cmd: DataBlock);
		конст
			C = 15;
			OK = 13;
			SecsToWait = 10;
		перем
			t: Kernel.MilliTimer;
			status: мнвоНаБитахМЗ;
		нач
			(* check command completion *)
			status := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, НИЗКОУР.прочти32битаПоАдресу(адресОт(cmd.data[0])));
			Kernel.SetTimer(t, SecsToWait * 1000);
			нцПока ~(C в status) и ~Kernel.Expired(t) делай
				status := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, НИЗКОУР.прочти32битаПоАдресу(адресОт(cmd.data[0])));
			кц;
			утв(C в status);

			(* check command ok *)
			status := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, НИЗКОУР.прочти32битаПоАдресу(адресОт(cmd.data[0])));
			Kernel.SetTimer(t, SecsToWait * 1000);
			нцПока ~(OK в status) и ~Kernel.Expired(t) делай
				status := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, НИЗКОУР.прочти32битаПоАдресу(адресОт(cmd.data[0])));
			кц;
			утв(OK в status);
		кон WaitForActionCmd;

		проц ContainsRxData(перем rxFrame: DataBlock): булево;
		конст
			C = 15;
			OK = 13;
			EOF = 15;
		перем
			adr: адресВПамяти;
			status: мнвоНаБитахМЗ;
		нач
			adr := адресОт(rxFrame.data[0]);
			status := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, НИЗКОУР.прочти32битаПоАдресу(adr));
			если (C в status) и (OK в status) то
				возврат EOF в НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, НИЗКОУР.прочти32битаПоАдресу(adr + 0CH));	(* data placing completed ? *)
			всё;
			возврат ложь;
		кон ContainsRxData;

		проц StartActionCmd(перем cmd: DataBlock);
		конст
			ActiveState = 7;
		перем
			adr: адресВПамяти;
		нач
			adr := ЭВМ.ДайФизическийАдресДиапазонаПамяти(адресОт(cmd.data[0]), cmd.size); утв(adr # ЭВМ.НулевойАдрес);
			WriteSCBGenPtr(adr(ЭВМ.Address32));
			утв(~(ActiveState в GetStatus()));	(* CU must not be in active state *)
			ExecCmd(CUStart, keepMasks);
			WaitForActionCmd(cmd);	(* todo: if we know the last cmd, then check for cmd done BEFORE execCmd *)
		кон StartActionCmd;

		проц WritePORT(p: мнвоНаБитахМЗ);
		нач
			НИЗКОУР.запиши32битаПоАдресу(base + PORTReg, p);
		кон WritePORT;

		проц WriteSCBGenPtr(val: цел32);
		нач
			НИЗКОУР.запиши32битаПоАдресу(base + SCBGenPtr, val);
		кон WriteSCBGenPtr;

		проц ReadEEPROM(reg: цел16; перем res: цел16);
		конст
			EESK = 0;
			EECS = 1;
			EEDI = 2;
			EEDO = 3;
			ReadOpcode = 6;
		перем
			x: мнвоНаБитахМЗ;
			bits: цел16;

			проц RaiseClk(перем x: мнвоНаБитахМЗ);
			перем dummy: цел32;
			нач
				включиВоМнвоНаБитах(x, EESK);
				НИЗКОУР.запиши16битПоАдресу(base + SCBEeprom, устарПреобразуйКБолееУзкомуЦел(НИЗКОУР.подмениТипЗначения(цел32, x)));
				dummy := НИЗКОУР.прочти16битПоАдресу(base + SCBStatus);
				Delay(1);
			кон RaiseClk;

			проц LowerClk(перем x: мнвоНаБитахМЗ);
			перем dummy: цел32;
			нач
				исключиИзМнваНаБитах(x, EESK);
				НИЗКОУР.запиши16битПоАдресу(base + SCBEeprom, устарПреобразуйКБолееУзкомуЦел(НИЗКОУР.подмениТипЗначения(цел32, x)));
				dummy := НИЗКОУР.прочти16битПоАдресу(base + SCBStatus);
				Delay(1);
			кон LowerClk;

			проц ShiftOutBits(data, count: цел16);
			перем
				mask: цел16;
				x: мнвоНаБитахМЗ;
				dummy: цел32;
			нач
				mask := логСдвиг(1, count-1);
				x := GetEEPROMReg();
				исключиИзМнваНаБитах(x, EEDO); исключиИзМнваНаБитах(x, EEDI);
				нцДо
					исключиИзМнваНаБитах(x, EEDI);
					если (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, устарПреобразуйКБолееШирокомуЦел(data)) * НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, устарПреобразуйКБолееШирокомуЦел(mask)) # {}) то
						включиВоМнвоНаБитах(x, EEDI);
					всё;
					НИЗКОУР.запиши16битПоАдресу(base + SCBEeprom, устарПреобразуйКБолееУзкомуЦел(НИЗКОУР.подмениТипЗначения(цел32, x)));
					dummy := НИЗКОУР.прочти16битПоАдресу(base + SCBStatus);
					Delay(1);
					RaiseClk(x);
					LowerClk(x);
					mask := логСдвиг(mask, -1);
				кцПри mask = 0;

				исключиИзМнваНаБитах(x, EEDI);
				НИЗКОУР.запиши16битПоАдресу(base + SCBEeprom, устарПреобразуйКБолееУзкомуЦел(НИЗКОУР.подмениТипЗначения(цел32, x)));
			кон ShiftOutBits;

			проц ShiftInBits(): цел16;
			перем
				x: мнвоНаБитахМЗ;
				d, i: цел16;
			нач
				x := GetEEPROMReg();
				исключиИзМнваНаБитах(x, EEDO); исключиИзМнваНаБитах(x, EEDI);
				d := 0;

				нцДля i := 0 до 15 делай
					d := логСдвиг(d, 1);
					RaiseClk(x);

					x := GetEEPROMReg();

					исключиИзМнваНаБитах(x, EEDI);
					если (EEDO в x) и (~нечётноеЛи¿(d)) то
						d := d + 1;
					всё;
					LowerClk(x);
				кц;

				возврат d;
			кон ShiftInBits;

			проц GetEEPROMReg(): мнвоНаБитахМЗ;
				нач
					возврат НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, устарПреобразуйКБолееШирокомуЦел(НИЗКОУР.прочти16битПоАдресу(base + SCBEeprom)));
			кон GetEEPROMReg;

			(*
				returns number of bits in eeprom address
				typically 6 or 8 bits according to eeprom size of 64 or 256 registers
			*)
			проц GetEEPROMAdrSize(): цел16;
			перем
				x: мнвоНаБитахМЗ;
				size: цел16;
				dummy: цел32;
				err: булево;
			нач
				err := ложь;

				(* enable eeprom by setting EECS *)
				x := GetEEPROMReg();
				исключиИзМнваНаБитах(x, EEDI); исключиИзМнваНаБитах(x, EEDO); исключиИзМнваНаБитах(x, EESK);
				включиВоМнвоНаБитах(x, EECS);
				НИЗКОУР.запиши16битПоАдресу(base + SCBEeprom, устарПреобразуйКБолееУзкомуЦел(НИЗКОУР.подмениТипЗначения(цел32, x)));

				ShiftOutBits(ReadOpcode, 3);	(* opcodes are 3 bits *)

				x := GetEEPROMReg();

				нцДо
					увел(size);
					включиВоМнвоНаБитах(x, EEDO);
					исключиИзМнваНаБитах(x, EEDI);
					НИЗКОУР.запиши16битПоАдресу(base + SCBEeprom, устарПреобразуйКБолееУзкомуЦел(НИЗКОУР.подмениТипЗначения(цел32, x)));
					dummy := НИЗКОУР.прочти16битПоАдресу(base + SCBStatus);
					Delay(1);
					RaiseClk(x);
					LowerClk(x);

					если size > 8 то	(* max address size is 8 bits *)
						size := 0;
						err := истина;
					всё;
					x := GetEEPROMReg();
				кцПри ~(EEDO в x) или err;

				dummy := ShiftInBits();
				CleanupEEPROM();

				возврат size;
			кон GetEEPROMAdrSize;

			проц CleanupEEPROM;
			перем x: мнвоНаБитахМЗ;
			нач
				x := GetEEPROMReg();

				исключиИзМнваНаБитах(x, EECS); исключиИзМнваНаБитах(x, EEDI);
				НИЗКОУР.запиши16битПоАдресу(base + SCBEeprom, устарПреобразуйКБолееУзкомуЦел(НИЗКОУР.подмениТипЗначения(цел32, x)));

				RaiseClk(x);
				LowerClk(x);
			кон CleanupEEPROM;

		нач
			bits := GetEEPROMAdrSize();
			x := GetEEPROMReg();
			исключиИзМнваНаБитах(x, EEDI); исключиИзМнваНаБитах(x, EEDO); исключиИзМнваНаБитах(x, EESK);
			включиВоМнвоНаБитах(x, EECS);
			НИЗКОУР.запиши16битПоАдресу(base + SCBEeprom, устарПреобразуйКБолееУзкомуЦел(НИЗКОУР.подмениТипЗначения(цел32, x)));

			ShiftOutBits(ReadOpcode, 3);	(* opcodes are 3 bits *)
			ShiftOutBits(reg, bits);

			res := ShiftInBits();

			CleanupEEPROM();
		кон ReadEEPROM;

		проц HandleInterrupt;
		перем
			status, ack: мнвоНаБитахМЗ;
		нач
			status := GetStatus();
			ack := {};

			если IsIn(CX, status) то
				(* this interrupt indicates that the CU finished executing a command *)
				ack := ack + CX;
			всё;

			если IsIn(FR, status) то
				(* this interrupt indicates that the RU has finished receiving a frame *)
				ack := ack + FR;
				ReadFrame();
			всё;

			если IsIn(CNA, status) то
				(* this interrupt indicates that the CU has left the active state or has entered the idle state *)
				ack := ack + CNA;
			всё;

			если IsIn(RNR, status) то
				(* this interrupt indicates that the RU leaves the ready state -> no more place in the RxRing !!! *)
				ЛогЯдра.пСтроку8("Intel8255x: RNR Interrupt: RxRing too small."); ЛогЯдра.пВК_ПС;
				ack := ack + RNR;
			всё;

			если IsIn(MDI, status) то
				(* this interrupt indicates when an MDI read or write cycle has completed *)
				ack := ack + MDI;
			всё;

			если IsIn(SWI, status) то
				(* used for software generated interrupts *)
				ack := ack + SWI;
			всё;

			AckIntr(ack);
		кон HandleInterrupt;

		проц IncLastRFD;
		перем
			prevStatus: мнвоНаБитахМЗ;
			prevAdr, adr: адресВПамяти;
		нач
			prevAdr := адресОт(lastRFD.data[0]);
			lastRFD := lastRFD.next;
			adr := адресОт(lastRFD.data[0]);
			НИЗКОУР.запиши32битаПоАдресу(adr, Suspend);	(* set Suspend Bit *)
			НИЗКОУР.запиши32битаПоАдресу(adr + 0CH, 0);	(* delete EOF Bit *)
			НИЗКОУР.запиши16битПоАдресу(adr + 0CH + 2H, устарПреобразуйКБолееУзкомуЦел(lastRFD.size));

			prevStatus := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, НИЗКОУР.прочти32битаПоАдресу(prevAdr));
			НИЗКОУР.запиши32битаПоАдресу(prevAdr, prevStatus * {0..29, 31});	(* delete Suspend Bit (30) in previous RFD *)

			если GetRUState() = RUSuspended то
				ExecCmd(RUResume, keepMasks);
			всё;
		кон IncLastRFD;

		проц ReadFrame;
		конст
			DataOfs = 10H;	(* receive buffer offset *)
		перем
			frameAdr: адресВПамяти; type, actualCount: цел32;
			srcAdr (* , dstAdr *) : Network.LinkAdr;
			(* handler: Network.Receiver; *)
			buf: Network.Buffer;
		нач
			нцПока ContainsRxData(actRFD) делай
				frameAdr := адресОт(actRFD.data[0]);
				actualCount := НИЗКОУР.подмениТипЗначения(цел32, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, НИЗКОУР.прочти32битаПоАдресу(frameAdr + 0CH)) * {0..13});

				(*SYSTEM.MOVE(frameAdr + DataOfs, ADDRESSOF(dstAdr), 6);*)
				НИЗКОУР.копируйПамять(frameAdr + DataOfs + 6, адресОт(srcAdr), 6);
				type := Network.GetNet2(actRFD.data^, DataOfs + 12);	(* Endianess ! *)
				(*
				dev.GetReceiver(type, handler, hSize);
				rcvAdr := frameAdr + DataOfs + 14 + hSize;
				rcvSize := actualCount - 14 - hSize;

				INC(dev.recvCount);
				handler(dev, SYSTEM.VAL(Network.RecvHdr, actRFD.data[DataOfs + 14]), actualCount - 14, type, srcAdr);
				*)
				buf := Network.GetNewBuffer();
				если buf # НУЛЬ то
					buf.ofs := 0;
					buf.len := actualCount - 14;
					buf.src := srcAdr;
					Network.Copy(actRFD.data^, buf.data, DataOfs + 14, 0, actualCount - 14);
					dev.QueueBuffer(buf, type);
				всё;

				(* delete C, OK and EOF Bits in RFD *)
				НИЗКОУР.запиши32битаПоАдресу(frameAdr, 0);
				НИЗКОУР.запиши16битПоАдресу(frameAdr + 0CH, 0);

				actRFD := actRFD.next;
				IncLastRFD();
			кц;
		кон ReadFrame;

		проц Finalize;
		нач
			Objects.RemoveHandler(сам.HandleInterrupt, ЭВМ.IRQ0 + irq);
			Network.registry.Remove(dev);
			dev.ctrl := НУЛЬ;
			dev := НУЛЬ;
		кон Finalize;

	кон Controller;

перем
	installedControllers: Controller;
	installed: цел32;

проц Install*;
нач {единолично}
	если installed = 0 то
		ScanPCI(8086H, 2449H);
		ScanPCI(8086H, 1029H);	(* 82559 Ethernet Controller *)
		ScanPCI(8086H, 1031H);	(* ICH3 *)
		ScanPCI(8086H, 1032H);
		ScanPCI(8086H, 1033H);
		ScanPCI(8086H, 1034H);	(* Reserved *)
		ScanPCI(8086H, 1035H);
		ScanPCI(8086H, 1036H);	(* Reserved *)
		ScanPCI(8086H, 1037H);	(* Reserved *)
		ScanPCI(8086H, 1038H);	(* Reserved *)
		ScanPCI(8086H, 103DH);	(* In VAIO *)
		ScanPCI(8086H, 1064H); 	(* 82562ET/EZ/GT/GZ - Pro/100 VE (LOM) *)
		ScanPCI(8086H, 1209H);	(* 82559ER *)
		ScanPCI(8086H, 1229H);	(* Ethernet Pro 100 *)
	всё;
кон Install;

проц Remove*;
перем table: Plugins.Table; i: размерМЗ;
нач {единолично}
	Network.registry.GetAll(table);
	если table # НУЛЬ то
		нцДля i := 0 до длинаМассива(table)-1 делай
			если table[i] суть LinkDevice то table[i](LinkDevice).Finalize(истина) всё
		кц
	всё;
	installed := 0;
кон Remove;

проц ScanPCI(vendor, device: цел32);
перем
	index, bus, dev, fct, irq, i: цел32; res: целМЗ;
	base: адресВПамяти;
	d: LinkDevice;
	c: Controller;
	name: Plugins.Name;
нач
	index := 0;
	нцПока (PCI.FindPCIDevice(device, vendor, index, bus, dev, fct) = PCI.Done) и (installed < 10) делай
		res := PCI.ReadConfigDword(bus, dev, fct, PCI.Adr0Reg, i); утв(res = PCI.Done);
		base := i; утв(~нечётноеЛи¿(base)); 	(* memory mapped *)
		умень(base, base остОтДеленияНа 16);
		ЭВМ.MapPhysical(base, 4*K, base);

		res := PCI.ReadConfigByte(bus, dev, fct, PCI.IntlReg, irq); утв(res = PCI.Done);
		нов(d, Network.TypeEthernet, MaxETHFrameSize - 14, 6);
		name := Name;
		i := 0; нцПока name[i] # 0X делай увел(i) кц;
		name[i] := симв8ИзКода(кодСимв8("0") + installed);
		name[i+1] := 0X;
		d.SetName(name);
		d.desc := Desc;

		нов(c, d, base, irq);	(* increments "installed" when successful *)
		увел(index)
	кц

кон ScanPCI;

проц IsIn(subset, set: мнвоНаБитахМЗ): булево;
нач
	возврат ((subset * set) = subset);
кон IsIn;

проц Cleanup;
нач
	(*WHILE installedControllers # NIL DO installedControllers.Finalize; installedControllers := installedControllers.next END;*)
	если Modules.shutdown = Modules.None то	(* module is being freed *)
		Remove;
	всё
кон Cleanup;

(*
	busy wait for ms milliseconds
*)
проц Delay(ms: цел32);
перем t: Kernel.MilliTimer;
нач
	Kernel.SetTimer(t, ms);
	нцДо кцПри Kernel.Expired(t);
кон Delay;

нач
	Modules.InstallTermHandler(Cleanup);
кон Intel8255x.


PC.Compile \s Intel8255x.Mod ~
System.OpenKernelLog ~
TestNet.Mod
System.Free TestNet ~
TestNet.SetDevice "Intel8255x#0" ~
TestNet.ShowDevices ~
TestNet.SendBroadcast ~
TestNet.SendBroadcastVar 1499 ~
TestNet.SendTest ^ 1 10 100 1000 ~
Intel8255x.Test
Decoder.Decode Intel8255x.Obx ~

System.Free Intel8255x ~
System.Free TestNet ~

System.State Intel8255x ~

Aos.Call Intel8255x.Install
Aos.Call Intel8255x.Remove

модуль ЭВМ;	(** AUTHOR "pjm, G.F."; PURPOSE "Bootstrapping, configuration and machine interface"; *)

использует S := НИЗКОУР, Трассировка, Unix, Glue;

конст
	DefaultConfig = "Color 0  StackSize 128";

	#если I386 то
		Version = "A2 Gen. 32-bit, ";
		DefaultObjectFileExtension* = ".GofU";
	#аесли AMD64 то
		Version = "A2 Gen. 64-bit, ";
		РасширениеОбъектногоФайлаПоУмолч* = ".GofUu";
	#аесли ARM то
		Version = "A2 Gen. ARM, ";
		DefaultObjectFileExtension* = ".GofA";	
	#кон

	ЧастотаКвантовВремени˛Гц* = 1000; (* frequency of ticks increments in Hz *)

	(** bits in features variable *)
	MTTR* = 12;  MMX* = 23;


	AddressSize = размер16_от(адресВПамяти);
	StaticBlockSize = 8 * AddressSize;	(* static heap block size *)

	MemBlockSize* = 64*1024*1024;

	ЯдернаяБлокировкаДляВыводаТрассировки* = 0;	(* Trace output *)
	ЯдернаяБлокировкаДляРаботыСПамятью* = 1;		(*!  Virtual memory management, stack and page allocation,  not used in UnixAos *)
	X11* = 2;				(* XWindows I/O *)
	ЯдернаяБлокировкаДляРаботыСКучей* = 3;   		(* Storage allocation and Garbage collection *)
	ЯдернаяБлокировкаДляОбработкиПрерываний* = 4;		(*!  Interrupt handling,  not used in UnixAos *)
	ЯдернаяБлокировкаСпискаМодулей* = 5;		(* Module list *)
	ЯдернаяБлокировкаОчередиГотовности* = 6;		(*!  Ready queue,  not used in UnixAos *)
	ЯдернаяБлокировкаДляМежпроцессорногоПрерывания* = 7;	(*!  Interprocessor interrupts,  not used in UnixAos *)
	ЯдернаяБлокировкаДляЛогаЯдра* = 8;		(* Atomic output *)
	квоЯдерныхБлокировок* = 9;   (* { <= 32 } *)

	МаксКвоПроцессоров* = 4;
	StrongChecks = истина;

тип
	Поставщик* = массив 13 из симв8;

	УкльНаЗаголовокБлокаПамяти* = укль {опасныйДоступКПамяти, неОтслСборщиком} на ЗаголовокБлокаПамяти;
	ЗаголовокБлокаПамяти* = запись
		следщ- : УкльНаЗаголовокБлокаПамяти;
		адресЭтогоБлока-: адресВПамяти; 		(* sort key in linked list of memory blocks *)
		разм-: размерМЗ;
		адресНачалаДанных-, адресЗаКонцомДанных-: адресВПамяти
	кон;

	(** processor state *)
	СостояниеПроцессора* = запись
		PC*, BP*, SP*: адресВПамяти
	кон;


перем
	mtx	: массив квоЯдерныхБлокировок из Unix.Mutex_t;
	taken: массив квоЯдерныхБлокировок из адресВПамяти; (* for lock order check *)

	версияЯОС-: массив 64 из симв8;	(** Aos version *)

	свойстваПроцессора_1-, свойстваПроцессора_2 : мнвоНаБитахМЗ;
	MMX_поддерживаетсяЛи¿-	: булево;
	SSE_поддерживаетсяЛи¿-	: булево;
	SSE2_поддерживаетсяЛи¿-	: булево;
	SSE3_поддерживаетсяЛи¿-	: булево;
	SSSE3_поддерживаетсяЛи¿-	: булево;
	SSE41_поддерживаетсяЛи¿-	: булево;
	SSE42_поддерживаетсяЛи¿-	: булево;
	SSE5_поддерживаетсяЛи¿-	: булево;
	AVX_поддерживаетсяЛи¿-		: булево;

	ticks-: цел32;	(** timer ticks. Use Kernel.GetTicks() to read, don't write *)

	prioLow-, prioHigh-: целМЗ;	(* permitted thread priorities *)

	значПоУмолчРегистраУправленияОперациямиСПлавающейТочкой-: мнвоНаБитахМЗ;	(** default floating-point control register value (default rounding mode is towards -infinity, for ENTIER) *)
	timerFrequency-: цел64;	(** clock rate of GetTimer in Hz *)

	gcThreshold-: размерМЗ;
	первыйБлокПамятиДляКучи-{неОтслСборщиком}, последнийБлокПамятиДляКучи-{неОтслСборщиком}: УкльНаЗаголовокБлокаПамяти; (* head and tail of sorted list of memory blocks *)

	config: массив 2048 из симв8;	(* config strings *)

	logname: массив 32 из симв8;
	logfile: целМЗ;
	traceHeap: булево;

	timer0	: цел64;

	(** Return current processor ID (0 to MaxNum-1). *)
	проц  НомерТекущегоПроцессора* (): цел32;
	нач
		возврат 0
	кон НомерТекущегоПроцессора;

	#если ARM то
	проц Syscall(nr: адресВПамяти; p1, p2, p3: адресВПамяти);
	машКод
		ldr r7, [fp, #nr]
		ldr r0, [fp, #p1]
		ldr r1, [fp, #p2]
		ldr r2, [fp, #p3]
		swi #0
	кон Syscall;
	#кон

	(**
	 * Flush Data Cache for the specified virtual address range. If len is negative, flushes the whole cache.
	 * This is used on some architecture to interact with DMA hardware (e.g. Ethernet and USB. It can be
	 * left empty on Intel architecture.
	 *)
	проц СбросьКешДанныхДляДиапазона * (adr: адресВПамяти; len: размерМЗ);
	#если ARM то
	нач
		Syscall(0xF0002, adr, adr+len, 0 (*7*) ); (* 983042 = ARM cacheflush syscall *)
	#кон
	кон СбросьКешДанныхДляДиапазона;

	(**
	 * Invalidate Data Cache for the specified virtual address range. If len is negative, flushes the whole cache.
	 * This is used on some architecture to interact with DMA hardware (e.g. Ethernet and USB. It can be
	 * left empty on Intel architecture.
	 *)
	проц ИнвалидируйКешДанныхДляДиапазона * (adr: адресВПамяти; len: размерМЗ);
	кон ИнвалидируйКешДанныхДляДиапазона;

	(**
	 * Invalidate Instruction Cache for the specified virtual address range. If len is negative, flushes the whole cache.
	 * This is used on some architecture to interact with DMA hardware (e.g. Ethernet and USB. It can be
	 * left empty on Intel architecture.
	 *)
	проц ИнвалидируйКешИнструкцийДляДиапазона * (adr: адресВПамяти; len: размерМЗ);
	кон ИнвалидируйКешИнструкцийДляДиапазона;

	(* insert given memory block in sorted list of memory blocks, sort key is startAdr field - called during GC *)
	проц InsertMemoryBlock(memBlock: УкльНаЗаголовокБлокаПамяти);
	перем cur {неОтслСборщиком}, prev {неОтслСборщиком}: УкльНаЗаголовокБлокаПамяти;
	нач
		cur := первыйБлокПамятиДляКучи;
		prev := НУЛЬ;
		нцПока (cur # НУЛЬ) и (адресВПамяти из cur^ < адресВПамяти из memBlock^) делай
			prev := cur;
			cur := cur.следщ
		кц;
		если prev = НУЛЬ то (* insert at head of list *)
			memBlock.следщ := первыйБлокПамятиДляКучи;
			первыйБлокПамятиДляКучи := memBlock
		иначе (* insert in middle or at end of list *)
			prev.следщ := memBlock;
			memBlock.следщ := cur;
		всё;
		если cur = НУЛЬ то
			последнийБлокПамятиДляКучи := memBlock
		всё
	кон InsertMemoryBlock;


	(* Free unused memory block - called during GC *)
	проц ОчистьБлокПамяти*(вхБлок: УкльНаЗаголовокБлокаПамяти);
	перем cur {неОтслСборщиком}, prev {неОтслСборщиком}: УкльНаЗаголовокБлокаПамяти;
	нач
		cur := первыйБлокПамятиДляКучи;
		prev := НУЛЬ;
		нцПока (cur # НУЛЬ) и (cur # вхБлок) делай
			prev := cur;
			cur := cur.следщ
		кц;
		если cur = вхБлок то
			если traceHeap то
				Трассировка.пСтроку8( "Release memory block " );  Трассировка.пАдресВПамяти( вхБлок.адресЭтогоБлока );  Трассировка.пВК_ПС
			всё;
			если prev = НУЛЬ то
				первыйБлокПамятиДляКучи := cur.следщ
			иначе
				prev.следщ := cur.следщ;
				если cur.следщ = НУЛЬ то
					последнийБлокПамятиДляКучи := prev
				всё
			всё;
			Unix.free( вхБлок.адресЭтогоБлока )
		иначе
			СТОП(535)	(* error in memory block management *)
		всё;
	кон ОчистьБлокПамяти;



	(* expand heap by allocating a new memory block *)
	проц ВыделиЕщёПамятьДляКучи*( Мусор: цел32; Размер˛НеМенее˛байт: размерМЗ; перем выхБлок: УкльНаЗаголовокБлокаПамяти; перем выхАдресНачалаДанных, выхАдресЗаКонцомДанных: адресВПамяти );
	перем mBlock: УкльНаЗаголовокБлокаПамяти;  alloc: размерМЗ;  adr: адресВПамяти;
	нач
		утв(размер16_от(ЗаголовокБлокаПамяти) <= StaticBlockSize); (* make sure MemoryBlock contents fits into one StaticBlock *)
		alloc := Размер˛НеМенее˛байт + StaticBlockSize;
		если alloc < MemBlockSize то alloc := MemBlockSize всё;

		утв((Unix.PageSize > StaticBlockSize) и (Unix.PageSize остОтДеленияНа StaticBlockSize = 0));  (* alignment to Unix.PageSize implies alignment to StaticBlockSize *)
		увел( alloc, (-alloc) остОтДеленияНа Unix.PageSize );

		если Unix.posix_memalign( adr, Unix.PageSize, alloc ) # 0 то
			Unix.Perror( "Machine.ExpandHeap: posix_memalign" );
			выхАдресНачалаДанных := 0;
			выхАдресЗаКонцомДанных := 0;
			выхБлок := НУЛЬ;
		иначе
			если Unix.mprotect( adr, alloc, 7 (* READ WRITE EXEC *) ) # 0 то
				Unix.Perror( "Machine.ExpandHeap: mprotect" )
			всё;
			mBlock := adr;
			mBlock.следщ := НУЛЬ;
			mBlock.адресЭтогоБлока := adr;
			mBlock.разм := alloc;

			выхАдресНачалаДанных := adr + StaticBlockSize;
			выхАдресЗаКонцомДанных := выхАдресНачалаДанных + alloc - StaticBlockSize;

			mBlock.адресНачалаДанных := выхАдресНачалаДанных;
			mBlock.адресЗаКонцомДанных := выхАдресНачалаДанных; (* block is still empty -- Heaps module will set the upper bound *)

			InsertMemoryBlock( mBlock );
			если traceHeap то TraceHeap( mBlock )  всё;

			выхБлок := mBlock;
		всё
	кон ВыделиЕщёПамятьДляКучи;

	(* Set memory block end address *)
	проц УстАдресЗаКонцомДанныхВЗаголовкеБлокаПамяти*(Заголовок: УкльНаЗаголовокБлокаПамяти; вхАдресЗаКонцомДанных: адресВПамяти);
	нач
		утв(вхАдресЗаКонцомДанных >= Заголовок.адресНачалаДанных);
		Заголовок.адресЗаКонцомДанных := вхАдресЗаКонцомДанных
	кон УстАдресЗаКонцомДанныхВЗаголовкеБлокаПамяти;

	проц TraceHeap( new: УкльНаЗаголовокБлокаПамяти );
	перем cur{неОтслСборщиком}: УкльНаЗаголовокБлокаПамяти;
	нач
		Трассировка.пВК_ПС;
		Трассировка.пСтроку8( "Heap expanded" );  Трассировка.пВК_ПС;
		Трассировка.пАдресВПамяти(Glue.baseAdr); Трассировка.пСтроку8(" - "); Трассировка.пАдресВПамяти(Glue.endAdr); Трассировка.пСтроку8(" (Initial Heap)");
		Трассировка.пВК_ПС;
		cur := первыйБлокПамятиДляКучи;
		нцПока cur # НУЛЬ делай
			Трассировка.пАдресВПамяти( cur.адресЭтогоБлока );  Трассировка.пСтроку8( " - " );  Трассировка.пАдресВПамяти( cur.адресЭтогоБлока + cur.разм );
			если cur = new то  Трассировка.пСтроку8( " (new)" )  всё;
			Трассировка.пВК_ПС;
			cur := cur.следщ
		кц
	кон TraceHeap;

	(** Get first memory block and first free address, the first free address is identical to memBlockHead.endBlockAdr *)
	проц ДайИзначальныйДиапазонПамятиДляКучи*(перем выхАдресНачалаДанных, выхАдресЗаКонцомДанных, выхАдресСвободногоБлока: адресВПамяти);
	нач
		выхАдресНачалаДанных := НУЛЬ; выхАдресЗаКонцомДанных := НУЛЬ; выхАдресСвободногоБлока := НУЛЬ;
	кон ДайИзначальныйДиапазонПамятиДляКучи;


	(* returns if an address is a currently allocated heap address *)
	проц АдресНаКучеЛи¿*( Адр: адресВПамяти ): булево;
	перем mb: УкльНаЗаголовокБлокаПамяти;
	нач
		если (Адр>=Glue.baseAdr) и (Адр<=Glue.endAdr) то возврат истина всё;
		mb := первыйБлокПамятиДляКучи;
		нцПока mb # НУЛЬ делай
			если (Адр >= mb.адресНачалаДанных) и (Адр <= mb.адресЗаКонцомДанных) то  возврат истина  всё;
			mb := mb.следщ;
		кц;
		возврат ложь;
	кон АдресНаКучеЛи¿;


	(** Return information on free memory in Kbytes. *)
	проц ДайСведенияОКуче*(перем выхВсего˛Кб, выхСвободноМин˛Кб, выхСвободноМак˛Кб: размерМЗ);
	нач
		(*! meaningless in Unix port, for interface compatibility only *)
		выхВсего˛Кб := 0;
		выхСвободноМин˛Кб := 0;
		выхСвободноМак˛Кб := 0
	кон ДайСведенияОКуче;



	(** Fill "size" bytes at "destAdr" with "filler". "size" must be multiple of 4. *)
	проц  ЗаполниДиапазонБайтовПредставлениемЦел32* (destAdr: адресВПамяти; size: размерМЗ; filler: цел32);
	машКод
	#если I386 то
		MOV EDI, [EBP+destAdr]
		MOV ECX, [EBP+size]
		MOV EAX, [EBP+filler]
		TEST ECX, 3
		JZ ok
		PUSH 8	; ASSERT failure
		INT 3
	ok:
		SHR ECX, 2
		CLD
		REP STOSD
	#аесли AMD64 то
		MOV RDI, [RBP + destAdr]
		MOV RCX, [RBP + size]
		MOV EAX, [RBP + filler]
		TEST RCX, 3
		JZ ok
		PUSH 8	; ASSERT failure
		INT 3
	ok:
		SHR RCX, 2
		CLD
		REP STOSD
	#аесли ARM то
		LDR	R0, [FP, #filler]
		LDR	R1, [FP, #size]
		LDR	R3, [FP, #destAdr]
		ADD	R4, R1, R3				; R4 = size + destAdr

		AND	R5, R3, #3				; R5 := R3 MOD 4
		CMP	R5, #0					; ASSERT(R5 = 0)
		BEQ	CheckSize
		SWI	#8

	CheckSize:
		AND	R5, R1, #3				; R5 := R1 MOD 4
		CMP	R5, #0					; ASSERT(R5 = 0)
		BEQ	Loop
		SWI	#8

	Loop:
		CMP	R4, R3
		BLS		Exit
		STR	R0, [R3, #0]			; put(destAdr + counter, filler)
		ADD	R3, R3, #4				; R3 := R3 + 4
		B		Loop
	Exit:	
	#иначе
		unimplemented
	#кон
	кон ЗаполниДиапазонБайтовПредставлениемЦел32;

проц  чПорт8*(port: цел32; перем val: симв8);
кон чПорт8;

проц  чПорт16*(port: цел32; перем val: цел16);
кон чПорт16;

проц  чПорт32*(port: цел32; перем val: цел32);
кон чПорт32;

проц  пПорт8*(port: цел32; val: симв8);
кон пПорт8;

проц  пПорт16*(port: цел32; val: цел16);
кон пПорт16;

проц  пПорт32*(port: цел32; val: цел32);
кон пПорт32;


(** -- Atomic operations -- *)

(** Atomic INC(x). *)
проц -атомарноУвел*(перем x: цел32);
машКод
#если I386 то
	POP EAX
	LOCK
	INC DWORD [EAX]
#аесли AMD64 то
	POP RAX
	LOCK
	INC DWORD [RAX]
#аесли ARM то
		LDR	R0, [SP], #4
	loop:
		LDREX	R1, R0
		ADD	R1, R1, #1
		STREX	R2, R1, R0
		CMP	R2, #0
		BNE	loop
#иначе
	unimplemented
#кон
кон атомарноУвел;

(** Atomic DEC(x). *)
проц -атомарноУмень*(перем x: цел32);
машКод
#если I386 то
	POP EAX
	LOCK
	DEC DWORD [EAX]
#аесли AMD64 то
	POP RAX
	LOCK
	DEC DWORD [RAX]
#аесли ARM то 
	LDR	R0, [SP], #4
loop:
	LDREX	R1, R0
	SUB	R1, R1, #1
	STREX	R2, R1, R0
	CMP	R2, #0
	BNE	loop
#иначе
	unimplemented
#кон
кон атомарноУмень;

(** Atomic INC(x, y). *)
проц -атомарноУвелНа*(перем x: цел32; y: цел32);
машКод
#если I386 то
	POP EBX
	POP EAX
	LOCK
	ADD DWORD [EAX], EBX
#аесли AMD64 то
	POP RBX
	POP RAX
	LOCK
	ADD DWORD [RAX], EBX
#аесли ARM то
	LDR R3, [SP, #y]	; R3 := y
	LDR R0, [SP, #x]	; R0 := ADR(x)
loop:
	LDREX R1, R0		; R1 := x
	ADD R1, R1, R3	; increment x
	STREX R2, R1, R0
	CMP R2, #0
	BNE loop			; if store failed, try again, else exit
	ADD SP, SP, #8
#иначе
	unimplemented
#кон
кон атомарноУвелНа;


#если I386 или AM64 то
(** Atomic EXCL. *)
проц AtomicExcl* (перем s: SET; bit: SIGNED32);
машКод
#если I386 то
	MOV EAX, [EBP+bit]
	MOV EBX, [EBP+s]
	LOCK
	BTR [EBX], EAX
#аесли AMD64 то
	MOV EAX, [RBP + bit]
	MOV RBX, [RBP + s]
	LOCK
	BTR [RBX], EAX
#иначе
	unimplemented
#кон
кон AtomicExcl;
#кон

(** Atomic test-and-set. Set x = TRUE and return old value of x. *)
проц -атомарноПрочитайИЗапиши*(перем x: булево): булево;
машКод
#если I386 то
	POP EBX
	MOV AL, 1
	XCHG [EBX], AL
#аесли AMD64 то
	POP RBX
	MOV AL, 1
	XCHG [RBX], AL
#аесли ARM то
	LDR	R3, [SP, #x]			; R3 := ADDRESSOF(x)
	MOV	R1, #0				; R1 := FALSE
	MOV	R2, #1				; R2 := TRUE
	ADD	SP, SP, #4				; pop variable from stack

loop:
	LDREXB	R0, R3					; load excl x
	CMP	R0, R1
	BNE	exit						; x # old -> exit
	STREXB	R4, R2, R3				; x = old -> store excl new -> x
	CMP	R4, #0
	BNE	loop					; store exclusive failed: retry

exit:
#иначе
	unimplemented
#кон
кон атомарноПрочитайИЗапиши;

(* Atomic compare-and-swap. Set x = new if x = old and return old value of x *)
проц  -атомарноСравниИЗамени* (перем x: цел32; old, new: цел32): цел32;
машКод
#если I386 то
	POP EBX		; new
	POP EAX		; old
	POP ECX		; address of x
	LOCK CMPXCHG [ECX], EBX; atomicly compare x with old and set it to new if equal
#аесли AMD64 то
	POP RBX		; new
	POP RAX		; old
	POP RCX		; address of x
	LOCK CMPXCHG [RCX], EBX	; atomicly compare x with old and set it to new if equal
#аесли ARM то 
	LDR	R3, [SP, #x]			; R3 := ADDRESSOF(x)
	LDR	R1, [SP, #old]			; R1 := old
	LDR	R2, [SP, #new]			; R2 := new
	ADD	SP, SP, #12				; pop variable from stack

loop:
	LDREX	R0, R3					; load excl x
	CMP	R0, R1
	BNE	exit						; x # old -> exit
	STREX	R4, R2, R3				; x = old -> store excl new -> x
	CMP	R4, #0
	BNE	loop					; store exclusive failed: retry

exit:
#иначе
	unimplemented
#кон
кон атомарноСравниИЗамени;

(** This procedure should be called in all spin loops as a hint to the processor (e.g. Pentium 4). *)
проц  -ПодсказкаОСпинКонструкциях*;
машКод
#если I386 то
	PAUSE
#аесли AMD64 то
	PAUSE
#аесли ARM то
	MOV R0, R0
#иначе
	unimplemented
#кон
кон ПодсказкаОСпинКонструкциях;

(* Return current instruction pointer *)
проц  ТекСчётчикКоманд* (): адресВПамяти;
машКод
#если I386 то
	MOV EAX, [EBP+4]
#аесли AMD64 то
	MOV RAX, [RBP + 8]
#аесли ARM то 
	MOV R0, PC
#иначе
	unimplemented
#кон
кон ТекСчётчикКоманд;

#если I386 то
проц -GetTimer* (): SIGNED64;
машКод
	RDTSC	; set EDX:EAX
кон GetTimer;
#аесли AMD64 то
проц -ДайКвоТактовПроцессораСМоментаПерезапуска* (): цел64;
машКод
	XOR RAX, RAX
	RDTSC	; set EDX:EAX
	SHL RDX, 32
	OR RAX, RDX
кон ДайКвоТактовПроцессораСМоментаПерезапуска;
#аесли ARM то
проц GetTimer* (): SIGNED64;
перем t: Unix.Timespec;
нач
	если Unix.clock_gettime(Unix.CLOCK_MONOTONIC, адресВПамяти из t) = 0 то
		возврат SIGNED64(t.tv_sec)*1000000000 + SIGNED64(t.tv_nsec);
	иначе
		возврат 0;
	кон;
кон GetTimer;
#иначе
	unimplemented
#кон


	(** -- Configuration and bootstrapping -- *)

	(** Return the value of the configuration string specified by parameter name in parameter val. Returns val = "" if the string was not found, or has an empty value. *)
	проц ДайЗначениеКлючаКонфигурацииЯОС* (конст Ключ: массив из симв8; перем Знач: массив из симв8);
	перем i, src: цел32; ch: симв8;
	нач
		утв (Ключ[0] # "=");	(* no longer supported, use GetInit instead *)

		src := -1;  Знач := "";
		нц
			нцДо
				увел( src );  ch := config[src];
				если ch = 0X то прервиЦикл всё;
			кцПри ch > ' ';
			i := 0;
			нц
				ch := config[src];
				если (ch # Ключ[i]) или (Ключ[i] = 0X) то прервиЦикл всё;
				увел (i); увел (src)
			кц;
			если (ch <= ' ') и (Ключ[i] = 0X) то	(* found *)
				i := 0;
				нцДо
					увел (src); ch := config[src]; Знач[i] := ch; увел (i);
					если i = длинаМассива(Знач) то Знач[i - 1] := 0X; возврат всё	(* val too short *)
				кцПри ch <= ' ';
				если ch = ' ' то Знач[i -1] := 0X всё;
				возврат
			иначе
				нцПока ch > ' ' делай	(* skip to end of name *)
					увел (src); ch := config[src]
				кц;
				увел (src);
				нцДо	(* skip to end of value *)
					ch := config[src]; увел (src)
				кцПри ch <= ' '
			всё
		кц;
		если (Ключ = "ObjectFileExtension") и (Знач = "") то
			Знач := РасширениеОбъектногоФайлаПоУмолч
		всё;
	кон ДайЗначениеКлючаКонфигурацииЯОС;


	(** Convert a string to an integer. Parameter i specifies where in the string scanning should begin (usually 0 in the first call). Scanning stops at the first non-valid character, and i returns the updated position. Parameter s is the string to be scanned. The value is returned as result, or 0 if not valid. Syntax: number = ["-"] digit {digit} ["H" | "h"] . digit = "0" | ... "9" | "A" .. "F" | "a" .. "f" . If the number contains any hexdecimal letter, or if it ends in "H" or "h", it is interpreted as hexadecimal. *)
	проц СтрВЦел32* (перем НачинаяСПозиции: размерМЗ; конст Строка: массив из симв8): цел32;
	перем vd, vh, sgn, d: цел32; hex: булево;
	нач
		vd := 0; vh := 0; hex := ложь;
		если Строка[НачинаяСПозиции] = "-" то sgn := -1; увел (НачинаяСПозиции) иначе sgn := 1 всё;
		нц
			если (Строка[НачинаяСПозиции] >= "0") и (Строка[НачинаяСПозиции] <= "9") то d := кодСимв8 (Строка[НачинаяСПозиции])-кодСимв8 ("0")
			аесли (ASCII_вЗаглавную (Строка[НачинаяСПозиции]) >= "A") и (ASCII_вЗаглавную (Строка[НачинаяСПозиции]) <= "F") то d := кодСимв8 (ASCII_вЗаглавную (Строка[НачинаяСПозиции]))-кодСимв8 ("A") + 10; hex := истина
			иначе прервиЦикл
			всё;
			vd := 10*vd + d; vh := 16*vh + d;
			увел (НачинаяСПозиции)
		кц;
		если ASCII_вЗаглавную (Строка[НачинаяСПозиции]) = "H" то hex := истина; увел (НачинаяСПозиции) всё;	(* optional H *)
		если hex то vd := vh всё;
		возврат sgn * vd
	кон СтрВЦел32;


	(* function returning the number of processors that are available to Aos *)
	проц ДайКвоПроцессоров*( ): размерМЗ;
	нач
		возврат Unix.getnprocs();
	кон ДайКвоПроцессоров;

	(*! non portable code, for native Aos only *)
	проц SetNumberOfProcessors*( num: цел32 );
	нач
		(* numberOfProcessors := num; *)
	кон SetNumberOfProcessors;

	(* function for changing byte order *)
	проц ПоменяйПорядокБайтЦел32* (n: цел32): цел32;
	машКод
	#если I386 то
		MOV EAX, [EBP+n]				; load n in eax
		BSWAP EAX						; swap byte order
	#аесли AMD64 то
		MOV EAX, [RBP+n]				; load n in eax
		BSWAP EAX						; swap byte order
	#аесли ARM то
		LDR R0, [SP, #n] ; R0 := n
		EOR R3, R0, R0, ROR #16
		LSR R3, R3, #8
		BIC R3, R3, #65280
		EOR R0, R3, R0, ROR #8 ; result is returned in R0
	#иначе
		unimplemented
	#кон
	кон ПоменяйПорядокБайтЦел32;


	(* Send and print character *)
	проц TraceChar *(c: симв8);
	нач
		Трассировка.пСимв8( c )
	кон TraceChar;


	#если I386 или AMD64 то
	(** CPU identification *)

	проц ДайИдентификаторПроцессора*( перем vendor: Поставщик;  перем version: цел32;  перем features1,features2: мнвоНаБитахМЗ );
	машКод
	#если I386 то
		PUSH	ECX
		MOV	EAX, 0
		CPUID
		CMP	EAX, 0
		JNE	ok
		MOV	ESI, [EBP+vendor]
		MOV	[ESI], AL	;  AL = 0
		MOV	ESI, [EBP+version]
		MOV	[ESI], EAX	;  EAX = 0
		MOV	ESI, [EBP+features1]
		MOV	[ESI], EAX
		MOV	ESI, [EBP+features2]
		MOV	[ESI], EAX
		JMP	end
		ok:
		MOV	ESI, [EBP+vendor]
		MOV	[ESI], EBX
		MOV	[ESI+4], EDX
		MOV	[ESI+8], ECX
		MOV	BYTE [ESI+12], 0
		MOV	EAX, 1
		CPUID
		MOV	ESI, [EBP+version]
		MOV	[ESI], EAX
		MOV	ESI, [EBP+features1]
		MOV	[ESI], EDX
		MOV	ESI, [EBP+features2]
		MOV	[ESI], ECX
		end:
		POP	ECX
	#аесли AMD64 то
		PUSH	RCX
		MOV	EAX, 0
		CPUID
		CMP	EAX, 0
		JNE	ok
		MOV	RSI, [RBP+vendor]
		MOV	[RSI], AL	;  AL = 0
		MOV	RSI, [RBP+version]
		MOV	[RSI], EAX	;  EAX = 0
		MOV	RSI, [RBP+features1]
		MOV	[RSI], EAX
		MOV	RSI, [RBP+features2]
		MOV	[RSI], EAX
		JMP	end
		ok:
		MOV	RSI, [RBP+vendor]
		MOV	[RSI], EBX
		MOV	[RSI+4], EDX
		MOV	[RSI+8], ECX
		MOV	BYTE [RSI+12], 0
		MOV	EAX, 1
		CPUID
		MOV	RSI, [RBP+version]
		MOV	[RSI], EAX
		MOV	RSI, [RBP+features1]
		MOV	[RSI], EDX
		MOV	RSI, [RBP+features2]
		MOV	[RSI], RCX
		end:
		POP	RCX
	#иначе
		unimplemented
	#кон
	кон ДайИдентификаторПроцессора;


	(* If the CPUID instruction is supported, the ID flag (bit 21) of the EFLAGS register is r/w *)
	проц CpuIdSupported( ) : булево;
	машКод
	#если I386 то
		PUSHFD				; save EFLAGS
		POP EAX				; store EFLAGS in EAX
		MOV EBX, EAX		; save EBX for later testing
		XOR EAX, 00200000H	; toggle bit 21
		PUSH EAX				; push to stack
		POPFD					; save changed EAX to EFLAGS
		PUSHFD				; push EFLAGS to TOS
		POP EAX				; store EFLAGS in EAX
		CMP EAX, EBX		; see if bit 21 has changed
		SETNE AL;			; return TRUE if bit 21 has changed, FALSE otherwise
	#аесли AMD64 то
		PUSHFQ				; save RFLAGS
		POP RAX				; store RFLAGS in EAX
		MOV RBX, RAX		; save RBX for later testing
		XOR RAX, 00200000H	; toggle bit 21
		PUSH RAX				; push to stack
		POPFQ					; save changed EAX to EFLAGS
		PUSHFQ				; push EFLAGS to TOS
		POP RAX				; store EFLAGS in EAX
		CMP RAX, RBX		; see if bit 21 has changed
		SETNE AL;			; return TRUE if bit 21 has changed, FALSE otherwise
	#иначе
		unimplemented
	#кон
	кон CpuIdSupported;


	(* setup MMX, SSE and SSE2..SSE5 and AVX extension *)

	проц SetupSSE2Ext;
	конст
		MMXFlag=23;(*IN features from EBX*)
		FXSRFlag = 24;
		SSEFlag = 25;
		SSE2Flag = 26;
		SSE3Flag = 0; (*IN features2 from ECX*) (*PH 04/11*)
		SSSE3Flag =9;
		SSE41Flag =19;
		SSE42Flag =20;
		SSE5Flag = 11;
		AVXFlag = 28;
	нач
		MMX_поддерживаетсяЛи¿ := MMXFlag в свойстваПроцессора_1;
		SSE_поддерживаетсяЛи¿ := SSEFlag в свойстваПроцессора_1;
		SSE2_поддерживаетсяЛи¿ := SSE_поддерживаетсяЛи¿ и (SSE2Flag в свойстваПроцессора_1);
		SSE3_поддерживаетсяЛи¿ := SSE2_поддерживаетсяЛи¿ и (SSE3Flag в свойстваПроцессора_2);
		SSSE3_поддерживаетсяЛи¿ := SSE3_поддерживаетсяЛи¿ и (SSSE3Flag в свойстваПроцессора_2); (* PH 04/11*)
		SSE41_поддерживаетсяЛи¿ := SSE3_поддерживаетсяЛи¿ и (SSE41Flag в свойстваПроцессора_2);
		SSE42_поддерживаетсяЛи¿ := SSE3_поддерживаетсяЛи¿ и (SSE42Flag в свойстваПроцессора_2);
		SSE5_поддерживаетсяЛи¿ := SSE3_поддерживаетсяЛи¿ и (SSE5Flag в свойстваПроцессора_2);
		AVX_поддерживаетсяЛи¿ := SSE3_поддерживаетсяЛи¿ и (AVXFlag в свойстваПроцессора_2);

		если SSE_поддерживаетсяЛи¿ и (FXSRFlag в свойстваПроцессора_1) то
			(* InitSSE(); *) (*! not privileged mode in Windows and Unix, not allowed *)
		всё;
	кон SetupSSE2Ext;



	(** -- Processor initialization -- *)
	проц -SetFCR( s: мнвоНаБитахМЗ );
	машКод
	#если I386 то
		FLDCW	[ESP]	;  parameter s
		POP	EAX
	#аесли AMD64 то
		FLDCW	WORD [RSP]	; parameter s
		POP	RAX
	#иначе
		unimplemented
	#кон
	кон SetFCR;

	проц -FCR( ): мнвоНаБитахМЗ;
	машКод
	#если I386 то
		PUSH	0
		FNSTCW	[ESP]
		FWAIT
		POP	EAX
	#аесли AMD64 то
		PUSH	0
		FNSTCW	WORD [RSP]
		FWAIT
		POP	RAX
	#иначе
		unimplemented
	#кон
	кон FCR;

	проц -InitFPU;
	машКод
	#если I386 то
		FNINIT
	#аесли AMD64 то
		FNINIT
	#аесли ARM то
		MRC p15, 0, R0, C1, C0, 2;
		ORR R0, R0, #0x00f00000;
		MCR p15, 0, R0, C1, C0, 2;
		ISB
		MOV R0, #0x40000000;
		VMSR FPEXC, R0;

		VMRS R0, FPSCR
		BIC R0, R0, #0x0c00000 ; round to nearest as the default
		; remark: if we put round to minus infinity as the default, we can spare quite some instructions in emission of ENTIER
		VMSR FPSCR, R0;	
	#иначе
		unimplemented
	#кон
	кон InitFPU;

	
	(** Setup FPU control word of current processor. *)
	проц УстРегистрУправленияСопроцессором*;
	нач
		InitFPU;  
		SetFCR( значПоУмолчРегистраУправленияОперациямиСПлавающейТочкой )
	кон УстРегистрУправленияСопроцессором;
	#кон


	(* Initialize locks. *)
	проц InitLocks;
	перем i: цел32;
	нач
		i := 0;
		нцПока i < квоЯдерныхБлокировок делай  mtx[i] := Unix.NewMtx( );  taken[i] := НУЛЬ; увел( i )  кц;
	кон InitLocks;

	проц СбросьВсеЯдерныеБлокировки*;
	перем i: цел32;
	нач
		i := 0;
		нцПока i < квоЯдерныхБлокировок делай  Unix.MtxDestroy( mtx[i] );  увел( i ) кц;
	кон СбросьВсеЯдерныеБлокировки;

	(** Acquire a spin-lock. *)
	проц  ЗапросиБлокировку*( Уровень: цел32 );   (* non reentrant lock *)
	перем i: целМЗ;
	нач
		Unix.MtxLock( mtx[Уровень] );
		если StrongChecks то
			утв(taken[Уровень] = НУЛЬ);
			taken[Уровень] := Unix.ThrThis( );
			нцДля i := 0 до Уровень-1 делай
				утв(taken[i] # Unix.ThrThis( )); (*! lock order *)
			кц;
		всё;
	кон ЗапросиБлокировку;

	(** Release a spin-lock. *)
	проц  ОтпустиБлокировку*( Уровень: цел32 );
	перем i: целМЗ;
	нач
		если StrongChecks то
			утв(taken[Уровень] = Unix.ThrThis( ));
			taken[Уровень] := НУЛЬ;
			нцДля i := 0 до Уровень-1 делай
				утв(taken[i] # Unix.ThrThis( )); (*! lock order *)
			кц;
		всё;
		Unix.MtxUnlock( mtx[Уровень] );
	кон ОтпустиБлокировку;


	проц ОстановиЯОС*( reboot: булево );
	перем r: цел32;  logstat: Unix.Status;
	нач
		если logfile > 0 то
			r := Unix.fstat( logfile, logstat );
			r := Unix.close( logfile );
			если logstat.size = 0 то  r := Unix.unlink( адресОт( logname) )  всё;
		всё;
		если reboot то  Unix.exit( 0 )  иначе  Unix.exit( 1 )  всё;
	кон ОстановиЯОС;




	(* Set machine-dependent parameter gcThreshold *)
	проц УстановиПорогСборкиМусора*;
	нач
		gcThreshold := 10*1024*1024; (* 10 MB *)
	кон УстановиПорогСборкиМусора;

	проц InitConfig;
	перем a: адресВПамяти;  i: цел32;  c: симв8;
	нач
		a := Unix.getenv( адресОт( "AOSCONFIG" ) );
		если a = 0 то  config := DefaultConfig
		иначе
			нцДо
				S.прочтиОбъектПоАдресу( a, c );  увел( a );  config[i] := c;  увел( i )
			кцПри c = 0X
		всё
	кон InitConfig;


	проц UpdateTicks*;
	нач
		если timerFrequency # 0 то 
			ticks := устарПреобразуйКБолееУзкомуЦел( (ДайКвоТактовПроцессораСМоментаПерезапуска() - timer0) *1000 DIV (timerFrequency) );
		всё;
	кон UpdateTicks;


	проц InitThreads;
	перем res: булево;
	нач
		res := Unix.ThrInitialize( prioLow, prioHigh );
		если ~res то
			Трассировка.StringLn( "Machine.InitThreads: no threads support in boot environment.  teminating" );
			Unix.exit( 1 )
		всё;
		если Glue.debug # {} то
			Трассировка.пСтроку8( "Threads initialized, priorities low, high: " );
			Трассировка.пЦел64( prioLow, 0 ); Трассировка.пСтроку8( ", " ); Трассировка.пЦел64( prioHigh, 0 );
			Трассировка.пВК_ПС
		всё
	кон InitThreads;

	проц CPUSpeed;
	перем t0, t1: цел64;
	нач
		t0 := ДайКвоТактовПроцессораСМоментаПерезапуска();  Unix.ThrSleep( 100 );  t1 := ДайКвоТактовПроцессораСМоментаПерезапуска();
		timerFrequency := (t1 - t0) * 10;	
		если Glue.debug # {} то
			Трассировка.пСтроку8( "Timer Frequency: ~" );  Трассировка.пЦел64( устарПреобразуйКБолееУзкомуЦел( timerFrequency ), 0);  Трассировка.пСтроку8( " Hz" );  Трассировка.пВК_ПС
		всё
	кон CPUSpeed;

	проц Log( c: симв8 );
	перем ignore: размерМЗ;
	нач
		ignore := Unix.write( 1, адресОт( c ), 1 );
		ignore := Unix.write( logfile, адресОт( c ), 1 );
	кон Log;

	проц LogFileOnly( c: симв8 );
	перем ignore: размерМЗ;
	нач
		ignore := Unix.write( logfile, адресОт( c ), 1 );
	кон LogFileOnly;


	проц InitLog;
	перем pid, i: цел32;
	нач
		если logfile > 0 то возврат всё;
		logname := "AOS.xxxxx.Log";
		pid := Unix.getpid();  i := 8;
		нцДо
			logname[i] := симв8ИзКода( pid остОтДеленияНа 10 + кодСимв8( '0' ) );  умень( i );
			pid := pid DIV 10;
		кцПри i = 3;
		logfile := Unix.open( адресОт( logname ), Unix.rdwr + Unix.creat + Unix.trunc, Unix.rwrwr );
	кон InitLog;

	проц SilentLog*;
	нач
		InitLog;
		Трассировка.пСимв8 := LogFileOnly
	кон SilentLog;

	проц VerboseLog*;
	нач
		InitLog;
		Трассировка.пСимв8 := Log
	кон VerboseLog;


	проц Append( перем a: массив из симв8; конст this: массив из симв8 );
	перем i, j: цел32;
	нач
		i := 0;  j := 0;
		нцПока a[i] # 0X делай  увел( i )  кц;
		нцПока (i < длинаМассива( a ) - 1) и (this[j] # 0X) делай a[i] := this[j];  увел( i );  увел( j )  кц;
		a[i] := 0X
	кон Append;


	проц ИнициализируйЯОС;
	перем vendor: Поставщик; ver: цел32;
	нач
		копируйСтрокуДо0( Unix.Version, версияЯОС );  Append( версияЯОС, Version ); Append(версияЯОС, S.Date);

		timer0 := ДайКвоТактовПроцессораСМоментаПерезапуска( );  ticks := 0;
		InitThreads;
		InitLocks;
		traceHeap := 1 в Glue.debug;
		InitConfig;
		CPUSpeed;
		#если I386 или AMD64 то
		если CpuIdSupported() то
			ДайИдентификаторПроцессора( vendor, ver, свойстваПроцессора_1, свойстваПроцессора_2 );	 SetupSSE2Ext
		всё;
		значПоУмолчРегистраУправленияОперациямиСПлавающейТочкой := (FCR() - {0,2,3,10,11}) + {0..5,8,9};	(* default FCR RC=00B *)
		#кон
	кон ИнициализируйЯОС;

	проц {INITIAL} Init0*;
	нач
		ИнициализируйЯОС;
	кон Init0;

кон ЭВМ.

(*
03.03.1998	pjm	First version
30.06.1999	pjm	ProcessorID moved to AosProcessor
*)

(**
Notes

This module defines an interface to the boot environment of the system. The facilities provided here are only intended for the lowest levels of the system, and should never be directly imported by user modules (exceptions are noted below). They are highly specific to the system hardware and firmware architecture.

Typically a machine has some type of firmware that performs initial testing and setup of the system. The firmware initiates the operating system bootstrap loader, which loads the boot file. This module is the first module in the statically linked boot file that gets control.

There are two more-or-less general procedures in this module: GetConfig and StrToInt. GetConfig is used to query low-level system settings, e.g., the location of the boot file system. StrToInt is a utility procedure that parses numeric strings.

Config strings:

ExtMemSize	Specifies size of extended memory (above 1MB) in MB. This value is not checked for validity. Setting it false may cause the system to fail, possible after running for some time. The memory size is usually detected automatically, but if the detection does not work for some reason, or if you want to limit the amount of memory detected, this string can be set. For example, if the machine has 64MB of memory, this value can be set as ExtMemSize="63".
*)


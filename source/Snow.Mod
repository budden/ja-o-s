модуль Snow; (** AUTHOR "TF"; PURPOSE "Let it snow"; *)

использует
	Modules, Commands, WMWindowManager, WMGraphics, Kernel, Random;

конст
	DefaultNofFlakes = 20;

	Idle = 0;
	Running = 1;
	Terminating = 2;
	Terminated = 3;

перем
	state : цел32;

проц Snow*(context : Commands.Context); (** [nofFlakes] ~ *)
перем
	flakes : укль на массив из WMWindowManager.BufferWindow;
	flakePos : укль на массив из запись x, y : цел32 кон;
	nofFlakes, i, j : цел32;
	f, f1, f2 : WMGraphics.Image;
	timer : Kernel.Timer;
	random : Random.Generator;
нач
	нач {единолично}
		если (state # Idle) то
			возврат;
		иначе
			state := Running;
		всё;
	кон;
	если ~context.arg.ПропустиБелоеПолеИЧитайЦел32(nofFlakes, ложь) или (nofFlakes <= 0) то
		nofFlakes := DefaultNofFlakes;
	всё;
	нов(flakes, nofFlakes);
	нов(flakePos, nofFlakes);
	нов(random); random.InitSeed(Kernel.GetTicks());
	нов(timer);
	f1 := WMGraphics.LoadImage("xmas04.tar://Flake1.png", истина);
	f2 := WMGraphics.LoadImage("xmas04.tar://Flake2.png", истина);
	нцДля i := 0 до nofFlakes - 1 делай
		flakePos[i].x := random.Dice(1280);
		flakePos[i].y := -random.Dice(1000);
		если random.Dice(2) = 1 то f := f1 иначе f := f2 всё;
		нов(flakes[i], f.width, f.height, истина);
		flakes[i].pointerThreshold := 255;
		flakes[i].canvas.DrawImage(0, 0, f, WMGraphics.ModeCopy)
	кц;

	нцДля i := 0 до nofFlakes - 1 делай
		WMWindowManager.ExtAddWindow(flakes[i], flakePos[i].x, flakePos[i].y, {WMWindowManager.FlagStayOnTop});
	кц;

	j := 0;
	нц
		увел(j);
		если (state # Running) или (j >= 12000) то прервиЦикл; всё;
		i := 0;
		нц
			flakePos[i].x := flakePos[i].x + random.Dice(5)-2;
			увел(flakePos[i].y);
			если random.Dice(2) = 1 то увел(flakePos[i].y) всё;
			если (flakePos[i].y > 1024) и (j < 10000) то flakePos[i].y := -64 - random.Dice(10) всё;
			flakes[i].manager.SetWindowPos(flakes[i], flakePos[i].x, flakePos[i].y);
			увел(i);
			если (state # Running) или (i >= nofFlakes) то прервиЦикл; всё;
		кц;
		если (state = Running) то timer.Sleep(10); всё;
	кц;

	нцДля i := 0 до nofFlakes - 1 делай
		flakes[i].manager.Remove(flakes[i]);
	кц;
	нач {единолично}
		если (state = Running) то
			state := Idle;
		иначе
			state := Terminated;
		всё;
	кон;
кон Snow;

проц Cleanup;
нач {единолично}
	если (state = Running) то
		state := Terminating;
		дождись(state = Terminated);
	всё;
кон Cleanup;

нач
	state := Idle;
	Modules.InstallTermHandler(Cleanup);
кон Snow.Snow

System.Free Snow ~

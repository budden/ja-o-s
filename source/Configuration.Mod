(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Configuration; (** AUTHOR "pjm"; PURPOSE "XML-based configuration"; *)

использует ЛогЯдра, Строки8, Files, XMLObjects, XML, XMLScanner, XMLParser, Потоки, Ю32;

конст
	Ok* = 0;
	ElementNotFound* = 10001;
	AttributeNotFound* = 10002;
	WrongType* = 10003;
	Error* = 10004;

	SaveConfigFile = "Save.Configuration.XML";
	MyConfigFile = "MyConfiguration.XML";
	ConfigFile = "Configuration.XML";

	(* element and attribute names - must match DTD in ConfigFile *)
	Section = "Section"; Setting = "Setting";
	NameAttr = "name"; ValueAttr = "value";

перем
	config*: XML.Document;	(** internalized config file *)
	error : булево;

(** In the children of element "parent", find an element with name "type" and "NameAttr" attribute "name". *)
проц GetNamedElement*(parent: XML.Element; конст type, name: массив из симв8): XML.Element;
перем enum: XMLObjects.Enumerator; p: динамическиТипизированныйУкль; e: XML.Element; s: XML.String;
нач
	enum := parent.GetContents();
	нцПока enum.HasMoreElements() делай
		p := enum.GetNext();
		если p суть XML.Element то
			e := p(XML.Element); s := e.GetName();
			если (s # НУЛЬ) и (s^ = type) то	(* correct element name *)
				s := e.GetAttributeValue(NameAttr);
				если (s # НУЛЬ) и (s^ = name) то	(* correct element name attribute *)
					возврат e
				всё
			всё
		всё
	кц;
	возврат НУЛЬ
кон GetNamedElement;

проц GetSection*(конст key : массив из симв8) : XML.Element;
нач {единолично}
	возврат GetElementX(Section, key);
кон GetSection;

проц GetSetting*(конст key : массив из симв8) : XML.Element;
нач {единолично}
	возврат GetElementX(Setting, key);
кон GetSetting;

проц GetElementX(конст type, key : массив из симв8) : XML.Element;
перем e : XML.Element; name : массив 64 из симв8; i, j : цел32;
нач
	если (config # НУЛЬ) то
		i := 0; e := config.GetRoot();
		нцПока (e # НУЛЬ) делай
			j := 0; нцПока (key[i] # 0X) и (key[i] # ".") делай name[j] := key[i]; увел(i); увел(j) кц;
			name[j] := 0X;
			если key[i] = 0X то	(* look for setting *)
				e := GetNamedElement(e, type, name);
				если e # НУЛЬ то	(* found *)
					возврат e;
				всё
			иначе	(* look for section *)
				увел(i);	(* skip '.' *)
				e := GetNamedElement(e, Section, name);
			всё;
		кц;
	всё;
	возврат НУЛЬ;
кон GetElementX;

(** 	Find the setting specified by the key, which is a path name through the sections to the setting, and return its value. 
  См. также GetJQ *)
проц Get*(конст key: массив из симв8; перем val: массив из симв8; перем res : целМЗ);
перем e: XML.Element; s: Строки8.уСтрока; a: XML.Attribute;
нач {единолично}
	e := GetElementX(Setting, key);
	если (e # НУЛЬ) то
		s := e.GetName();
		если (s # НУЛЬ) и (s^ = Setting) то
			a := e.GetAttribute(ValueAttr);
			если (a # НУЛЬ) то
				s := a.GetValue();
				если (s # НУЛЬ) то
					если (Строки8.КвоБайтБезЗавершающего0(s^) < длинаМассива(val)) то
						копируйСтрокуДо0(s^, val);
						res := Ok;
					иначе
						res := Error;
					всё;
				иначе
					res := Error;
				всё
			иначе
				res := AttributeNotFound;
			всё;
		иначе
			res := Error;
		всё;
	иначе
		res := ElementNotFound;
	всё;
кон Get;


(** 	GetJQ - это версия Get для StringJQ *)
проц GetJQ*(конст key: Ю32.Строка; перем val: Ю32.Строка; перем res : целМЗ);
перем valA : укль на массив из симв8;
нач 
	нов(valA, длинаМассива(val)*6);
	Get(Ю32.СтрокуВСвежуюЮ8(key)^, valA^, res);
	Ю32.КопируйЮ8ВСтроку˛обрезая(valA^, val) кон GetJQ;


проц GetBoolean*(конст key : массив из симв8; перем value : булево; перем res : целМЗ);
перем string : массив 8 из симв8;
нач
	Get(key, string, res);
	если (res = Ok) то
		Строки8.СтрокуВВерхнийРегистрASCII(string);
		Строки8.ОтрежьПробелыСДвухСторон(string);
		если (string = "TRUE") то value := истина;
		аесли (string = "FALSE") то value := ложь;
		иначе
			res := WrongType;
		всё;
	всё;
кон GetBoolean;

проц GetColor*(конст key : массив из симв8; перем value : цел32; перем res : целМЗ);
перем string : массив 16 из симв8; tempres : целМЗ;
нач
	Get(key, string, res);
	если (res = Ok) то
		Строки8.ОтрежьПробелыСДвухСторон(string);
		Строки8.ПрочтиЦел32_16_ричноИзСтроки(string, value, tempres);
		если tempres # Строки8.Успех то res := Error; всё;
	всё;
кон GetColor;


проц GetInteger*(конст key : массив из симв8; перем value : цел32; перем res : целМЗ);
перем string : массив 16 из симв8;
нач
	Get(key, string, res);
	если (res = Ok) то
		Строки8.ОтрежьПробелыСДвухСторон(string);
		Строки8.ПрочтиЦел32_изСтроки(string, value);
	всё;
кон GetInteger;

(** update (or insert if necessairy) the setting specified by the key. if the specified section/setting is not yet existing, it will be created *)
проц Put*(конст key, val : массив из симв8; перем res : целМЗ);
перем e, child : XML.Element; i, j : цел32; name : массив 64 из симв8;
нач {единолично}
	res := Ok;
	если config # НУЛЬ то
		i := 0; e := config.GetRoot();
		нцПока key[i] # 0X делай
			j := 0; нцПока (key[i] # 0X) и (key[i] # '.') делай name[j] := key[i]; увел(i); увел(j) кц;
			если key[i] = '.' то (* section *)
				child := GetNamedElement(e, Section, name);
				если child = НУЛЬ то (* create section *)
					нов(child); e.AddContent(child);
					child.SetName(Section); child.SetAttributeValue("name", name)
				всё;
				e := child; увел(i) (* skip '.' *)
			иначе (* setting *)
				child := GetNamedElement(e, Setting, name);
				если child = НУЛЬ то (* create setting *)
					нов(child); e.AddContent(child);
					child.SetName(Setting); child.SetAttributeValue("name", name)
				всё;
				child.SetAttributeValue("value", val)
			всё
		кц;
		WriteConfig();
	иначе
		res := Error;
	всё
кон Put;

проц PutBoolean*(конст key : массив из симв8; value : булево; перем res : целМЗ);
нач
	если value то Put(key, "TRUE", res) иначе Put(key, "FALSE", res); всё;
кон PutBoolean;

проц PutColor*(конст key : массив из симв8; value : цел32; перем res : целМЗ);
перем string : массив 9 из симв8;
нач
	Строки8.ПишиЦел64_16_ричноВСтроку(value, 8, string);
	Put(key, string, res);
кон PutColor;

проц PutInteger*(конст key : массив из симв8; value : цел32; перем res : целМЗ);
перем string : массив 16 из симв8;
нач
	Строки8.ПишиЦел64_вСтроку(value, string);
	Put(key, string, res);
кон PutInteger;

проц WriteConfig;
перем f : Files.File; out : Files.Writer;
нач
	если config # НУЛЬ то
		f := Files.New(ConfigFile);
		Files.OpenWriter(out, f, 0);
		config.Write(out, НУЛЬ, 0);
		out.ПротолкниБуферВПоток();
		Files.Register(f)
	всё
кон WriteConfig;

проц TrapHandler(pos, line, row: Потоки.ТипМестоВПотоке; конст msg: массив из симв8);
нач
	если ~error то
		ЛогЯдра.пСтроку8("Error in ");
		ЛогЯдра.пСтроку8(ConfigFile);
		ЛогЯдра.пСтроку8(" at position ");
		ЛогЯдра.пСтроку8("pos= "); ЛогЯдра.пЦел64(pos, 0); ЛогЯдра.пСтроку8(" line= "); ЛогЯдра.пЦел64(line, 0); ЛогЯдра.пСтроку8(" row= "); ЛогЯдра.пЦел64(row, 0);
		ЛогЯдра.пСтроку8(" switching to "); ЛогЯдра.пСтроку8(SaveConfigFile); ЛогЯдра.пСтроку8(" !"); ЛогЯдра.пВК_ПС
	иначе
		ЛогЯдра.пСтроку8("Error in ");
		ЛогЯдра.пСтроку8(SaveConfigFile); ЛогЯдра.пСтроку8(" giving up!"); ЛогЯдра.пВК_ПС;
	всё;
	error := истина; config := НУЛЬ
кон TrapHandler;

(** Internalize the config file. *)
проц Init*;
перем f: Files.File; scanner: XMLScanner.Scanner; parser: XMLParser.Parser; r: Files.Reader;
нач {единолично}
	error := ложь;
	config := НУЛЬ;
	f := Files.Old(MyConfigFile);
	если f = НУЛЬ то
		f := Files.Old(ConfigFile);
	всё;
	если f # НУЛЬ то
		нов(r, f, 0);
		нов(scanner, r); нов(parser, scanner); parser.reportError := TrapHandler; config := parser.Parse();
		если error то
			f := Files.Old(SaveConfigFile);
			если f # НУЛЬ то
				нов(r, f, 0);
				нов(scanner, r); нов(parser, scanner); parser.reportError := TrapHandler; config := parser.Parse()
			всё
		всё
	всё;
кон Init;

нач
	Init;
кон Configuration.



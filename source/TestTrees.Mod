модуль TestTrees;	(** AUTHOR "TF"; PURPOSE "Testing Tree Component"; *)

использует
	Strings, Modules,
	WMStandardComponents, WMComponents, WMGraphics, WMTrees, WMEditors,
	WM := WMWindowManager;

тип
	String = Strings.String;

	Window = окласс (WMComponents.FormWindow)
	перем
		panel : WMStandardComponents.Panel;
		tree : WMTrees.TreeView;
		delete, add : WMStandardComponents.Button;
		node : WMTrees.TreeNode;
		editor : WMEditors.Editor;

		проц &New*;
		нач
			нов(panel);
			panel.bounds.SetExtents(640, 420);
			panel.fillColor.Set(WMGraphics.RGBAToColor(255, 255, 255, 255));

			нов(tree);
			tree.bounds.SetWidth(200);
			tree.alignment.Set(WMComponents.AlignLeft);
			tree.onSelectNode.Add(NodeSelected);
			panel.AddContent(tree);

			нов(delete);
			delete.bounds.SetExtents(200, 20);
			delete.caption.SetAOC("Delete node");
			delete.alignment.Set(WMComponents.AlignTop);
			delete.onClick.Add(DeleteNode);
			panel.AddContent(delete);

			нов(add);
			delete.bounds.SetExtents(200, 20);
			add.caption.SetAOC("Add sub-node");
			add.alignment.Set(WMComponents.AlignTop);
			add.onClick.Add(AddNode);
			panel.AddContent(add);

			нов(editor);		(* A single line editor *)
			editor.bounds.SetHeight(30);
			editor.alignment.Set(WMComponents.AlignTop);
			editor.multiLine.Set(ложь);
			editor.tv.textAlignV.Set(WMGraphics.AlignCenter);
			editor.onEnter.Add(RenameNode);
			panel.AddContent(editor);

			FillTree;

			(* create the form window with panel size *)
			Init(panel.bounds.GetWidth(), panel.bounds.GetHeight(), ложь);
			SetContent(panel);
			editor.SetAsString("Sub-Element");

			SetTitle(Strings.NewString("Hierarchy editor"));
			WM.DefaultAddWindow(сам)
		кон New;

		проц NodeSelected(sender, data : динамическиТипизированныйУкль);
		перем t : WMTrees.Tree; caption : String;
		нач
			если (data # НУЛЬ) и (data суть WMTrees.TreeNode) то
				node := data(WMTrees.TreeNode);
				t := tree.GetTree();
				t.Acquire;
				caption := t.GetNodeCaption(node);
				t.Release;
				если caption # НУЛЬ то editor.SetAsString(caption^) всё
			всё
		кон NodeSelected;

		проц DeleteNode(sender, data : динамическиТипизированныйУкль);
		перем t : WMTrees.Tree;
		нач
			если node # НУЛЬ то
				t := tree.GetTree();
				t.Acquire;
				t.RemoveNode(node);
				node := НУЛЬ;
				t.Release
			всё
		кон DeleteNode;

		проц RenameNode(sender, data : динамическиТипизированныйУкль);
		перем t : WMTrees.Tree; string : массив 64 из симв8;
		нач
			если node # НУЛЬ то
				t := tree.GetTree();
				t.Acquire;
				editor.GetAsString(string);
				t.SetNodeCaption(node, Strings.NewString(string));
				t.Release
			всё
		кон RenameNode;

		проц AddNode(sender, data : динамическиТипизированныйУкль);
		перем t : WMTrees.Tree; new : WMTrees.TreeNode; string : массив 64 из симв8;
		нач
			если node # НУЛЬ то
				t := tree.GetTree();
				t.Acquire;
				нов(new);
				t.AddChildNode(node, new);
				editor.GetAsString(string);
				t.SetNodeCaption(new, Strings.NewString(string));
				t.Release
			всё
		кон AddNode;

		проц FillTree;
		перем t : WMTrees.Tree;
			root , sub : WMTrees.TreeNode;
		нач
			t := tree.GetTree();
			t.Acquire;
			нов(root);
			t.SetRoot(root);
			t.InclNodeState(root, WMTrees.NodeAlwaysExpanded);
			t.SetNodeCaption(root, Strings.NewString("Root"));

			нов(sub);
			t.AddChildNode(root, sub);
			t.SetNodeCaption(sub, Strings.NewString("Sub"));
			t.Release
		кон FillTree;

		проц {перекрыта}Close*;
		нач
			Close^;
			winstance := НУЛЬ
		кон Close;

	кон Window;

перем
	winstance : Window;

проц Open*;
нач
	если winstance = НУЛЬ то нов(winstance) всё;
кон Open;

проц Cleanup;
нач
	если winstance # НУЛЬ то winstance.Close всё
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон TestTrees.

TestTrees.Open ~
System.Free TestTrees WMTrees ~

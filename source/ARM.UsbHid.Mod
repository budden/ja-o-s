модуль UsbHid; (** AUTHOR "staubesv"; PURPOSE "HID device class specific requests"; *)
(**
 * Base class for HID class driver.
 * Implements the HID class-specific requests and parsing of the HID descriptor.
 *
 * References:
 *	Device Class Definition for Human Interface Devices (HID), version 1.11, 27.06.2001, www.usb.org
 *
 * History:
 *
 *	20.11.2005	First Release (staubesv)
 *	09.01.2006	Adapted to Usb.Mod changes (staubesv)
 *	05.07.2006	Adapted to Usbdi (staubesv)
 *	23.11.2006	Removed interfaceNumber parameter, added GetHidDescriptor, cleanup (staubesv)
 *)

использует ЛогЯдра, Usbdi;

конст

	(** HID Descriptors Types *)
	DescriptorHID* = 21H;
	DescriptorReport* = 22H;
	DescriptorPhysical* = 23H;

	(** HID Report Types *)
	ReportInput* =  01H;
	ReportOutput* = 02H;
	ReportFeature* = 03H;

	(** HID Protocol Types *)
	BootProtocol* = 0;
	ReportProtocol* = 1;

	(* USB HID Class Specific Request Codes, HID p. 51 *)
	HrGetReport = 01H;
	HrGetIdle = 02H;
	HrGetProtocol = 03H;
	HrSetReport = 09H;
	HrSetIdle = 0AH;
	HrSetProtocol = 0BH;
	SrGetDescriptor = 6;

	HidSetRequest = Usbdi.ToDevice + Usbdi.Class + Usbdi.Interface;
	HidGetRequest = Usbdi.ToHost + Usbdi.Class + Usbdi.Interface;

тип

	(* HID descriptor according to the Device Class Definition for HID, p. 22 *)
	HidDescriptor* = укль на запись
		bLength- : цел32;
		bDescriptorType- : цел32;
		bcdHID- : цел32;
		bCountryCode- : цел32;
		bNumDescriptors- : цел32;
		bClassDescriptorType- : цел32;
		wDescriptorLength- : цел32;
		optionalDescriptors- : укль на массив из OptionalDescriptor;
	кон;

	OptionalDescriptor* = запись
		bDescriptorType- : цел32;
		wDescriptorLength- : цел32;
	кон;

тип

	(* Base class of HID device drivers. Provides the HID class specific requests. *)
	HidDriver* = окласс(Usbdi.Driver);

		(** HID class specific device requests *)

		(**
		 * The SetIdle request silinces a particular report on the Interrupt In pipe until a new event occurs or
		 * the specified amount of time passes (HID, p.52)
		 * @param interface Related USB device interface
		 * @param duration: 0: infinite, 1-255: value * 4ms
		 * @reportId 0: idle rate applies to all reports, otherwise it applies only to reports with the corresponding reportId
		 * @return TRUE, if requests succeeds, FALSE otherwise
		 *)
		проц SetIdle*(reportId, duration : цел32) : булево;
		нач
			утв(interface.bInterfaceClass = 3H);
			возврат device.Request(HidSetRequest, HrSetIdle, reportId + duration*100H, interface.bInterfaceNumber, 0, Usbdi.NoData) = Usbdi.Ok;
		кон SetIdle;

		(**
		 * The GetIdle request reads the current idle rate for a particular input report. See HID p. 52
		 * @param interface Related USB device interface
		 * @param reportId
		 * @param idle Idle rate; 0: infinite duration, otherwise: idle rate in milliseconds; Only valid then request succeeded!
		 * @return TRUE, if request succeeded, FALSE otherwise
		 *)
		проц GetIdle*(reportId : цел32; перем idle : цел32) : булево;
		перем buffer : Usbdi.BufferPtr;
		нач
			утв(interface.bInterfaceClass = 3H);
			нов(buffer, 1);
			если device.Request(HidGetRequest, HrGetIdle, reportId, interface.bInterfaceNumber, 1, buffer) = Usbdi.Ok то
				idle := 4*кодСимв8(buffer[0]);
				возврат истина;
			всё;
			возврат ложь;
		кон GetIdle;

		(**
		 * The SetProtocol request switches between the boot protocol and the report protocol (HID p. 54).
		 * This request is only supported by devices in the boot subclass. Default is the Report Protocol.
		 * @param interface the request should be applied to
		 * @param protocol 0: Boot Protocol, 1: Report Protocol
		 * @return TRUE, if request succeeded, FALSE otherwise
		 *)
		проц SetProtocol*(protocol : цел32) : булево;
		нач
			утв(interface.bInterfaceClass = 3H);
			утв(((protocol = BootProtocol) или (protocol = ReportProtocol)) и (interface.bInterfaceSubClass = 1));
			возврат device.Request(HidSetRequest, HrSetProtocol, protocol, interface.bInterfaceNumber, 0, Usbdi.NoData) = Usbdi.Ok ;
		кон SetProtocol;

		(**
		 * The Getprotocol requests reads which protocol is currently active (HID, p. 54).
		 * This request is only supported by devices in the boot subclass.
		 * @param interface the request should be applied to
		 * @param protocol 0: Boot Protocol, 1: Report Protocol (Only valid if request succeeds!)
		 * @return TRUE, if request succeeded, FALSE otherwise
		 *)
		проц GetProtocol*(перем protocol : цел32) : булево;
		перем buffer : Usbdi.BufferPtr;
		нач
			если (interface.bInterfaceClass # 3H) или (interface.bInterfaceSubClass #  1) то
				трассируй(interface.bInterfaceClass, interface.bInterfaceSubClass); возврат ложь
			всё;
			утв((interface.bInterfaceClass = 3H) и (interface.bInterfaceSubClass=  1));
			нов(buffer, 1);
			если device.Request(HidGetRequest, HrGetProtocol, 0, interface.bInterfaceNumber, 1, buffer) = Usbdi.Ok то
				protocol := кодСимв8(buffer[0]);
				возврат истина;
			всё;
			возврат ложь;
		кон GetProtocol;

		(**
		 * The SetReport request allows the host to send a report to the device, possibly setting the state of input,
		 * output, or feature controls (HID, p. 52).
		 * @param interface the request should be applied to
		 * @param type of the report the host sends
		 * @param id of the report the host sends
		 * @param buffer: Buffer containing the report
		 * @param len: Lenght of the report
		 * @return TRUE, if request succeeded, FALSE otherwise
		 *)
		проц SetReport*(type, id : цел32; перем buffer: Usbdi.Buffer; len : цел32) : булево;
		нач
			утв(interface.bInterfaceClass = 3H);
			возврат device.Request(HidSetRequest, HrSetReport, id + type*100H, interface.bInterfaceNumber, len, buffer) = Usbdi.Ok;
		кон SetReport;

		(**
		 * The GetReport request allows the host to receive a report via the Control pipe (HID, p.51)
		 * @param interface the request should be applied to
		 * @param type Type of the report we want
		 * @param id of the report we want
		 * @param buffer: Buffer to put the report into
		 * @param len: Expected length of the report
		 * @return TRUE, if request succeeded, FALSE otherwise
		*)
		проц GetReport*(type, id : цел32; перем buffer: Usbdi.Buffer; len : цел32) : булево;
		нач
			утв(длинаМассива(buffer) >= len);
			утв(interface.bInterfaceClass = 3H);
			возврат device.Request(HidGetRequest, HrGetReport, id + type*100H, interface.bInterfaceNumber, len, buffer) = Usbdi.Ok;
		кон GetReport;

		(** This request returns the specified descriptor if the descriptor exists *)
		проц GetDescriptor*(descriptor, index, wIndex, len : цел32; перем buffer : Usbdi.Buffer) : булево;
		нач
			утв(длинаМассива(buffer) >= len);
			возврат device.Request(Usbdi.ToHost + Usbdi.Standard + Usbdi.Interface, SrGetDescriptor, index + descriptor*100H, wIndex, len, buffer) = Usbdi.Ok;
		кон GetDescriptor;

		(** Returns the HID descriptor of this interface or NIL if not present *)
		проц GetHidDescriptor*() : HidDescriptor;
		перем ud : Usbdi.UnknownDescriptor; hidDescriptor : HidDescriptor;
		нач
			утв(interface.bInterfaceClass = 3H);
			(* The HID descriptor is part of the configuration descriptor and therefore already pre-parsed by the USB system *)
			ud := interface.unknown;
			нцПока (ud # НУЛЬ) и (ud.bDescriptorType # DescriptorHID) делай ud := ud.next; кц;
			если ud # НУЛЬ то
				hidDescriptor := ParseHidDescriptor(ud.descriptor);
			всё;
			возврат hidDescriptor;
		кон GetHidDescriptor;


	кон HidDriver;

(* Load and parse the HID descriptor correspondig to the drivers interface *)
проц ParseHidDescriptor(descriptor : Usbdi.BufferPtr) : HidDescriptor;
перем hid : HidDescriptor;  i : цел32;
нач
	если (descriptor # НУЛЬ) и (длинаМассива(descriptor) >= 8) то
		нов(hid);
		hid.bLength := кодСимв8(descriptor[0]);
		hid.bDescriptorType := кодСимв8(descriptor[1]);
		hid.bcdHID := кодСимв8(descriptor[2]) + 100H*кодСимв8(descriptor[3]);
		hid.bCountryCode := кодСимв8(descriptor[4]);
		hid.bNumDescriptors := кодСимв8(descriptor[5]);
		hid.bClassDescriptorType := кодСимв8(descriptor[6]);
		hid.wDescriptorLength := кодСимв8(descriptor[7]);

		(* Parse the optional descriptors if there are some *)
		если hid.bNumDescriptors > 1 то
			если длинаМассива(descriptor) >= (3 * (hid.bNumDescriptors-2)) + 7 то
				ЛогЯдра.пСтроку8("UsbHid: Warning: HID descriptor too short"); ЛогЯдра.пВК_ПС;
				возврат hid;
			всё;
			нов(hid.optionalDescriptors, hid.bNumDescriptors-1);
			нцДля i := 0 до hid.bNumDescriptors-2 делай
				hid.optionalDescriptors[i].bDescriptorType := кодСимв8(descriptor[(3 * i) + 6]);
				hid.optionalDescriptors[i].wDescriptorLength := кодСимв8(descriptor[(3 * i) + 7]);
			кц;
		всё;
	всё;
	возврат hid;
кон ParseHidDescriptor;

проц ShowHidDescriptor*(hd : HidDescriptor);
перем i : цел32;
нач
	ЛогЯдра.пСтроку8("HID Descriptor: ");
	если hd = НУЛЬ то ЛогЯдра.пСтроку8("NIL"); всё; ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   bLength: "); ЛогЯдра.пЦел64(hd.bLength, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   bDescriptorType: "); ЛогЯдра.пЦел64(hd.bDescriptorType, 0);
	ЛогЯдра.пСтроку8("   bcdHID: "); ЛогЯдра.п16ричное(hd.bcdHID, 0); ЛогЯдра.пСимв8("H"); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   bCountryCode: "); ЛогЯдра.п16ричное(hd.bCountryCode, 0); ЛогЯдра.пСимв8("H"); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   bNumDescriptors: "); ЛогЯдра.пЦел64(hd.bNumDescriptors, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   bClassDescriptorType: "); ЛогЯдра.пЦел64(hd.bClassDescriptorType, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   wDescriptorLength: "); ЛогЯдра.пЦел64(hd.wDescriptorLength, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Optional descriptors: ");
	если (hd.optionalDescriptors = НУЛЬ) то
		ЛогЯдра.пСтроку8("None"); ЛогЯдра.пВК_ПС;
	иначе
		нцДля i := 0 до длинаМассива(hd.optionalDescriptors)-1 делай
			ЛогЯдра.пСтроку8("      bDescriptorType: "); ЛогЯдра.пЦел64(hd.optionalDescriptors[i].bDescriptorType, 0);
			ЛогЯдра.пСтроку8(", wDescriptorLength: "); ЛогЯдра.пЦел64(hd.optionalDescriptors[i].wDescriptorLength, 0);
			ЛогЯдра.пВК_ПС;
		кц;
	всё;
кон ShowHidDescriptor;

кон UsbHid.

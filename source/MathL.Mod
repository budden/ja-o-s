(* ETH Oberon, Copyright 2001 ETH Zuerich Institut fuer Computersysteme, ETH Zentrum, CH-8092 Zuerich.
Refer to the "General ETH Oberon System Source License" contract available at: http://www.oberon.ethz.ch/ *)

модуль MathL;	(** portable *)
(** AUTHOR "?"; PURPOSE "Math utility module (FLOAT64)"; *)

(* Aos version - requires floating-point instruction support. *)

использует НИЗКОУР;

конст
	e* = 2.7182818284590452354D0;
	pi* = 3.14159265358979323846D0;
	ln2* = 0.693147180559945309417232121458D0;
	eps = 2.2D-16;

тип
	Value = вещ64;

проц Expo (x: Value): бцел32;
нач
	возврат бцел32(арифмСдвиг(НИЗКОУР.подмениТипЗначения(бцел64, x), -52)) остОтДеленияНа 2048
кон Expo;

проц Mantissa (x: Value): бцел64;
нач
	возврат НИЗКОУР.подмениТипЗначения(бцел64, НИЗКОУР.подмениТипЗначения(мнвоНаБитах64, x) * {0 .. 51})
кон Mantissa;

проц Equal (x, y: Value): булево;
нач
	если x > y то
		x := x - y
	иначе
		x := y - x
	всё;
	возврат x < eps
кон Equal;

проц sin*(x: Value): Value;
перем
	k: целМЗ;
	xk, prev, res: Value;
нач
#если I386 то
	машКод
		FLD QWORD [EBP + x]
		FSIN
		#если ~LEGACY то
			FSTP QWORD [EBP + x]
			MOVSD XMM0, [EBP + x]
		#кон
	кон;
	возврат результат;
#аесли AMD64 то
	машКод
		FLD QWORD [RBP + x]
		FSIN
		#если ~LEGACY то
			FSTP QWORD [RBP + x]
			MOVSD XMM0, [RBP + x]
		#кон
	кон;
	возврат результат;
#иначе
	нцПока x >= 2 * pi делай x := x - 2*pi кц;
	нцПока x < 0 делай x := x + 2*pi кц;
	res := x;
	xk := x;
	k := 1;
	нцДо
		prev := res;
		xk := -xk * x * x / (2 * k) / (2 * k + 1);
		res := res + xk;
		увел(k)
	кцПри Equal(prev, res) или (k = 5000);
	возврат res
#кон
кон sin;

проц cos*(x: Value): Value;
перем
	k: целМЗ;
	prev, res, xk: Value;
нач
#если I386 то
	машКод
		FLD QWORD [EBP + x]
		FCOS
		#если ~LEGACY то
			FSTP QWORD [EBP + x]
			MOVSD XMM0, [EBP + x]
		#кон
	кон;
	возврат результат;
#аесли AMD64 то
	машКод
		FLD QWORD [RBP + x]
		FCOS
		#если ~LEGACY то
			FSTP QWORD [RBP + x]
			MOVSD XMM0, [RBP + x]
		#кон
	кон;
	возврат результат;
#иначе
	нцПока x >= 2 * pi делай x := x - 2*pi кц;
	нцПока x < 0 делай x := x + 2*pi кц;
	res := 1.0;
	xk := 1.0;
	k := 1;
	нцДо
		prev := res;
		xk := -xk * x * x / (2 * k - 1) / (2 * k);
		res := res + xk;
		увел(k, 1)
	кцПри Equal(xk, 0.0) или Equal(prev, res) или (k = 5000);
	возврат res
#кон
кон cos;

проц arctan*(x: Value): Value;
перем
	k: целМЗ;
	prev, res, term, xk: Value;
нач
#если I386 то
	машКод
		FLD QWORD [EBP + x]
		FLD1
		FPATAN
		#если ~LEGACY то
			FSTP QWORD [EBP + x]
			MOVSD XMM0, [EBP + x]
		#кон
	кон;
	возврат результат;
#аесли AMD64 то
	машКод
		FLD QWORD [RBP + x]
		FLD1
		FPATAN
		#если ~LEGACY то
			FSTP QWORD [RBP + x]
			MOVSD XMM0, [RBP + x]
		#кон
	кон;
	возврат результат;
#иначе
	если (x = 1) или (x = -1) то
		возврат x * pi / 4
	аесли (x > 1) или (x < -1) то
		возврат pi / 2 - arctan(1 / x)
	иначе
		(* atan(x) = sum_k (-1)^(k) x^{2 k + 1} / (2 k + 1), |x| < 1 *)
		prev := pi / 2;
		res := 0.0;
		xk := x;
		k := 0;
		нцДо
			prev := res;

			term := 1 / (2 * k + 1) * xk;
			если нечётноеЛи¿(k) то
				res := res - term
			иначе
				res := res + term
			всё;
			xk := xk * x * x;
			увел(k)
		кцПри Equal(prev, res) или (k = 50000);
		возврат res
	всё
#кон
кон arctan;

проц sqrt*(x: Value): Value;
нач
	если x <= 0 то
		если x = 0 то возврат 0 иначе СТОП(80) всё;
	всё;
#если I386 то
	машКод
		FLD QWORD [EBP + x]
		FSQRT
		#если ~LEGACY то
			FSTP QWORD [EBP + x]
			MOVSD XMM0, [EBP + x]
		#кон
	кон;
	возврат результат;
#аесли AMD64 то
	машКод
		FLD QWORD [RBP + x]
		FSQRT
		#если ~LEGACY то
			FSTP QWORD [RBP + x]
			MOVSD XMM0, [RBP + x]
		#кон
	кон;
	возврат результат;
#иначе
	возврат exp(0.5 * ln(x));
#кон
кон sqrt;

проц ln*(x: Value): Value;
перем
	k: целМЗ;
	res, y, yk: Value;
	mantissa: бцел64;
нач
	если x <= 0 то
		СТОП(80);
	всё;
#если I386 то
	машКод
		FLD1
		FLDL2E
		FDIVP
		FLD QWORD [EBP + x]
		FYL2X
		#если ~LEGACY то
			FSTP QWORD [EBP + x]
			MOVSD XMM0, [EBP + x]
		#кон
	кон;
	возврат результат;
#аесли AMD64 то
	машКод
		FLD1
		FLDL2E
		FDIVP
		FLD QWORD [RBP + x]
		FYL2X
		#если ~LEGACY то
			FSTP QWORD [RBP + x]
			MOVSD XMM0, [RBP + x]
		#кон
	кон;
	возврат результат;
#иначе
	если x < 1.0 то
		возврат -ln(1.0 / x)
	аесли x >= 2.0 то
		(*
			algorithm idea from http://stackoverflow.com/questions/10732034/how-are-logarithms-programmed
			and https://en.wikipedia.org/wiki/Natural_logarithm (Newton's method)

			ln(m * 2^e) = e ln(2) + ln(m)
		*)
		mantissa := Mantissa(x) + 3FF0000000000000H;
		возврат (Expo(x) - 1023) * ln2 + ln(НИЗКОУР.подмениТипЗначения(Value, mantissa))
	иначе
		(* ln(x) = 2 * sum_k 1/(2 k + 1) y^k, where y = (x - 1) / (x + 1), x real *)
		y := (x - 1) / (x + 1);
		yk := y;
		res := y;
		k := 1;
		нцДо
			yk := yk * y * y;
			res := res + yk / (2 * k + 1);
			увел(k)
		кцПри Equal(yk, 0.0) или (k = 5000);
		возврат 2.0 * res;
	всё
#кон
кон ln;

проц exp*(x: Value): Value;
перем
	k: целМЗ;
	prev, res, xk: Value;
нач
#если I386 то
	машКод
		FLD QWORD [EBP + x]
		FLDL2E
		FMULP
		FLD ST0
		FRNDINT
		FXCH ST1
		FSUB ST0, ST1
		F2XM1
		FLD1
		FADDP
		FSCALE
		FSTP ST1
		#если ~LEGACY то
			FSTP QWORD [EBP + x]
			MOVSD XMM0, [EBP + x]
		#кон
	кон;
	возврат результат;
#аесли AMD64 то
	машКод
		FLD QWORD [RBP + x]
		FLDL2E
		FMULP
		FLD ST0
		FRNDINT
		FXCH ST1
		FSUB ST0, ST1
		F2XM1
		FLD1
		FADDP
		FSCALE
		FSTP ST1
		#если ~LEGACY то
			FSTP QWORD [RBP + x]
			MOVSD XMM0, [RBP + x]
		#кон
	кон;
	возврат результат;
#иначе
	если x < 0.0 то
		возврат 1.0 / exp(-x)
	иначе
		(* exp(x) = sum_k x^(k) / k! *)
		prev := 0.0;
		res := 1.0;

		k := 1;
		xk := 1;
		нцДо
			prev := res;

			xk := xk / k * x;
			res := res + xk;
			увел(k, 1)
		кцПри Equal(xk, 0.0) или (k = 5000);
		возврат res
	всё
#кон
кон exp;

кон MathL.

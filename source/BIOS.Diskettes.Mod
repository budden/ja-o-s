(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Diskettes; (** non-portable *) (** AUTHOR "pjm"; PURPOSE "Diskette device driver"; *)
(**
 * Based on Native Oberon.
 *
 * Usage:
 *	Diskettes.Install ~ will load the floppy disk device driver
 *	System.Free Diskettes ~ will unload the driver
 *
 * History:
 *	01.03.2005	Adapted to Aos, FormatDisk removed (afi)
 *	08.12.2005	Move InstallDevices logic to module body, removed UnInstall & InstallDevices (staubesv)
 *	10.08.2006	Added performance monitoring functionality (staubesv)
 *	26.03.2007	Added NnofReads, NnofWrites, NnofOthers and NnofErrors (staubesv)
 *)

использует НИЗКОУР, ЭВМ, Kernel, Modules, ЛогЯдра, Plugins, Disks;

конст
	MaxDevices = 2;
	BS = 512;

	Read = Disks.Read;  Write = Disks.Write;  Format = 2;  Verify = 3;	(* operations *)
	Ready = 0;  Reset = 1;  Recal = 2;	(* states *)
	T0 = 0;  T720 = 1;  T1440 = 2;  T2880 = 3;	(* drive/media types *)

	Ok = Disks.Ok;

	InvalidDrive = 1011; (* Prevent trap when floppy disk device is not available but driver is loaded *)

тип
	Device* = окласс (Disks.Device)
		перем
			drive: цел32;
			locked: булево;	(* must be locked before access is allowed *)
			type, media: цел8;	(* drive type & current media *)
				(* current parameters *)
			size, sectors, heads, tracks: цел32;
			gap, rate, spec1, spec2, fgap: симв8;

		проц {перекрыта}Transfer*(op, start, num: цел32;  перем buf: массив из симв8;  ofs: размерМЗ;  перем res: целМЗ);
		нач
			Transfer1(сам, op, start, num, buf, ofs, res);
			если Disks.Stats то
				нач {единолично}
					если (op = Read) то
						увел (NnofReads);
						если (res = Disks.Ok) то увел (NbytesRead, num * blockSize);
						иначе увел (NnofErrors);
						всё;
					аесли (op = Write) то
						увел (NnofWrites);
						если (res = Disks.Ok) то увел (NbytesWritten, num * blockSize);
						иначе увел (NnofErrors);
						всё;
					иначе
						увел (NnofOthers);
					всё;
				кон;
			всё;
		кон Transfer;

		проц {перекрыта}GetSize*(перем size: цел32; перем res: целМЗ);
		нач
			GetSize1(сам, size, res)
		кон GetSize;

		проц {перекрыта}Handle*(перем msg: Disks.Message;  перем res: целМЗ);
		нач
			Handle1(сам, msg, res)
		кон Handle;

	кон Device;

перем
	device: массив MaxDevices из Device;
	curdrive: цел32;
	curtrack: цел32;
	state: цел8;
	result: массив 7 из мнвоНаБитахМЗ;
	errors: массив 3 из мнвоНаБитахМЗ;
	dmabufvirt, dmabufphys: адресВПамяти; dmabufsize: цел32;
	motor, interrupt : булево;
	trace: цел8;

(* Device driver *)

(* Error - Report an error *)

проц Error(msg: массив из симв8);
перем error, reason: массив 32 из симв8;  i: цел8;  r0, r1, r2: мнвоНаБитахМЗ;
нач
	копируйСтрокуДо0(msg, error);  r0 := errors[0];  r1 := errors[1];  r2 := errors[2];
	если (0 в r1) или (0 в r2) то reason := "Missing address mark"
	аесли 1 в r1 то reason := "Write protected"
	аесли 2 в r1 то reason := "Sector not found"
	аесли 4 в r1 то reason := "Over- or Underrun"
	аесли (5 в r1) или (5 в r2) то reason := "CRC error"
	аесли 7 в r1 то reason := "Sector past end"
	аесли (1 в r2) или (4 в r2) то reason := "Bad track"
	аесли 6 в r2 то reason := "Bad mark"
	аесли r0 * {6,7} = {6} то reason := "Command not completed"
	аесли r0 * {6,7} = {7} то reason := "Invalid command"
	иначе reason := ""
	всё;
	ЛогЯдра.пВК_ПС;  ЛогЯдра.пСтроку8("Diskette: ");  ЛогЯдра.пСтроку8(error);
	ЛогЯдра.пСтроку8(". ");  ЛогЯдра.пСтроку8(reason);  ЛогЯдра.пВК_ПС;
	если trace > 0 то
		нцДля i := 0 до 2 делай ЛогЯдра.п16ричное(НИЗКОУР.подмениТипЗначения(цел32, result[i]), 9) кц;
		ЛогЯдра.пВК_ПС;
		нцДля i := 0 до 2 делай ЛогЯдра.п16ричное(НИЗКОУР.подмениТипЗначения(цел32, errors[i]), 9) кц;
		ЛогЯдра.пВК_ПС
	всё;
	нцДля i := 0 до 6 делай result[i] := {} кц;
	нцДля i := 0 до 2 делай errors[i] := {} кц;
	state := Reset
кон Error;

(* SetupDMA - Start a DMA operation *)

проц SetupDMA(read: булево; chan, len: цел32);
перем adr: адресВПамяти; page, mode: цел32;
нач
	adr := dmabufphys;
	утв(len <= dmabufsize);
	если read то
		mode := 44H	(* IO->memory, no autoinit, increment, single mode *)
	иначе
		mode := 48H	(* memory->IO, no autoinit, increment, single mode *)
	всё;
	умень(len);
	утв((adr > 0) и (adr+len <= 1000000H));
	утв(adr DIV 65536 = (adr+len-1) DIV 65536);	(* same 64KB region *)
	просей chan из
		0:  page := 87H
		|1:  page := 83H
		|2:  page := 81H
		|3:  page := 82H
	всё;  (* CASE *)
	ЭВМ.пПорт8(0AH, симв8ИзКода(chan + 4));	(* disable DMA *)
	ЭВМ.пПорт8(0CH, 0X);	(* clear flip-flop *)
	ЭВМ.пПорт8(0BH, симв8ИзКода(chan + mode));	(* set mode *)
	ЭВМ.пПорт8(page, симв8ИзКода(арифмСдвиг(adr, -16)));	(* set page register *)
	ЭВМ.пПорт8(chan*2, симв8ИзКода(adr));	(* set address *)
	ЭВМ.пПорт8(chan*2, симв8ИзКода(арифмСдвиг(adr, -8)));
	ЭВМ.пПорт8(chan*2+1, симв8ИзКода(len));	(* set length *)
	ЭВМ.пПорт8(chan*2+1, симв8ИзКода(арифмСдвиг(len, -8)));
	ЭВМ.пПорт8(0AH, симв8ИзКода(chan))	(* enable DMA *)
кон SetupDMA;

(* PutByte - Send byte to controller *)

проц PutByte(b: симв8);
перем t: Kernel.MilliTimer;  s: мнвоНаБитахМЗ;
нач
	если state # Reset то
		Kernel.SetTimer(t, 500);	(* 0.5s *)
		нцДо
			ЭВМ.чПорт8(3F4H, НИЗКОУР.подмениТипЗначения(симв8, s));
			если s * {6,7} = {7} то	(* ready for write *)
				ЭВМ.пПорт8(3F5H, b);
				возврат	(* done *)
			всё
		кцПри Kernel.Expired(t);
		state := Reset;  если trace > 0 то ЛогЯдра.пСтроку8("~response ") всё
	всё
кон PutByte;

(* GetResults - Get results from controller, returns length of result *)

проц GetResults(): цел16;
перем t: Kernel.MilliTimer;  s: мнвоНаБитахМЗ;  i: цел8;
нач
	если state # Reset то
		i := 0;  s := {};
		Kernel.SetTimer(t, 500);	(* 0.5s *)
		нцДо
			ЭВМ.чПорт8(3F4H, НИЗКОУР.подмениТипЗначения(симв8, s));
			если s * {4,6,7} = {7} то	(* ready for write (end) *)
				если trace > 0 то ЛогЯдра.пСимв8("=");  ЛогЯдра.пЦел64(i, 1) всё;
				возврат i
			аесли s * {6,7} = {6,7} то	(* ready for read *)
				ЭВМ.чПорт8(3F5H, НИЗКОУР.подмениТипЗначения(симв8, s));  result[i] := s;
				если i < 3 то errors[i] := errors[i] + result[i] всё;
				увел(i)
			иначе (* skip *)
			всё
		кцПри Kernel.Expired(t);
		state := Reset;  если trace > 0 то ЛогЯдра.пСтроку8("~response ") всё
	всё;
	возврат -1
кон GetResults;

(* InterruptHandler - Handle floppy interrupt *)

проц InterruptHandler(перем state: ЭВМ.СостояниеПроцессора);
нач
	ЭВМ.Sti();  interrupt := истина
кон InterruptHandler;

(* WaitInterrupt - Wait for an interrupt *)

проц WaitInterrupt;
перем t: Kernel.MilliTimer;
нач
	если state # Reset то
		Kernel.SetTimer(t, 2000);	(* 2s *)
		нцДо кцПри interrupt или Kernel.Expired(t);
		если ~interrupt то если trace > 0 то ЛогЯдра.пСтроку8("~interrupt ") всё; state := Reset всё;
		interrupt := ложь
	всё
кон WaitInterrupt;

(* SetParams - Set parameters depending on drive type and media *)

проц SetParams(p: Device);
нач
	просей p.media из
		T720:
			если trace > 0 то ЛогЯдра.пСтроку8("720k ") всё;
			p.sectors := 9;  p.heads := 2;  p.tracks := 80;
			p.gap := 1BX;  p.rate := 2X;  (* transfer rate 250k/s *)
			p.spec1 := 0E1X;  (* step rate 4ms, head unload 32ms *)
			p.spec2 := 6X;  (* head load 12ms, DMA mode *)
			p.fgap := 50X	(* format gap size *)
		|T1440:
			если trace > 0 то ЛогЯдра.пСтроку8("1.44M ") всё;
			p.sectors := 18;  p.heads := 2;  p.tracks := 80;
			p.gap := 1BX;  p.rate := 0X;  (* transfer rate 500k/s *)
			p.spec1 := 0C1X;  (* step rate 4ms, head unload 16ms *)
			p.spec2 := 6X;  (* head load 6ms, DMA mode *)
			p.fgap := 6CX	(* format gap size *)
	всё;
	p.size := p.sectors * p.heads * p.tracks;
	state := Reset
кон SetParams;

(* CycleMedia - Skip to next media for a drive *)

проц CycleMedia(перем p: Device);
нач
	просей p.type из
		T0: СТОП(99)	(* no such drive *)
		|T720:	(* 720k drive can only handle 720k media *)
			просей p.media из
				T0: p.media := T720
				|T720: p.media := T0
			всё
		|T1440:	(* 1.44M drive first tries 1.44M & then 720k *)
			просей p.media из
				T0: p.media := T1440
				|T1440: p.media := T720
				|T720: p.media := T0
			всё
		|T2880:	(* 2.88M drive first tries 1.44M & then 720k (2.88M not handled yet) *)
			просей p.media из
				T0: p.media := T1440
				|T1440: p.media := T720
				|T720: p.media := T0
			всё
	всё; (* CASE *)
	если p.media # T0 то SetParams(p) всё	(* now set params according to media *)
кон CycleMedia;

(* Do - Perform a floppy operation *)

проц Do(dev: Device; op, sector, head, track, num: цел32;  перем buf: массив из НИЗКОУР.октет): цел32;
конст MaxLoops = 18;  MaxTries = 3;
перем s: мнвоНаБитахМЗ;  i, loops, try: цел32;  t: Kernel.MilliTimer;  ok: булево;  media: цел8;
нач
	нцДля i := 0 до 2 делай errors[i] := {} кц;
	если (num < 1) или (num > 126) то Error("Bad number of sectors"); возврат 1003 всё;
	если (track < 0) или (track >= dev.tracks) то Error("Invalid track"); возврат 1004 всё;
	если (head < 0) или (head >= dev.heads) то Error("Invalid head"); возврат 1005 всё;
	если curdrive # dev.drive то state := Reset;  curdrive := dev.drive всё;
	loops := 0;  try := 0;  media := dev.media;
	нц	(* two EXIT's at end of CASE state = Ready *)
		если trace > 0 то
			просей state из
				Ready: ЛогЯдра.пСтроку8("Ready ")
				|Reset: ЛогЯдра.пСтроку8("Reset ")
				|Recal: ЛогЯдра.пСтроку8("Recal ")
				иначе ЛогЯдра.пСтроку8("Unknown ")
			всё
		всё;
			(* select the drive & send power to the motor *)
		s := мнвоНаБитахМЗ({2,3,dev.drive+4}) + НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, dev.drive);
		ЭВМ.пПорт8(3F2H, НИЗКОУР.подмениТипЗначения(симв8, s));
		если (op в {Write, Format}) и ~motor то	(* motor was not running, wait for it to spin up *)
			Kernel.SetTimer(t, 500);	(* 0.5s *)
			нцДо кцПри Kernel.Expired(t)
		всё;
		motor := истина;  ok := истина;
		просей state из
			Ready:
				если trace > 0 то
					ЛогЯдра.пВК_ПС;
					просей op из
						Read: ЛогЯдра.пСтроку8("Read(")
						|Write: ЛогЯдра.пСтроку8("Write(")
						|Format: ЛогЯдра.пСтроку8("Format(")
						|Verify: ЛогЯдра.пСтроку8("Verify(")
					всё;
					ЛогЯдра.пЦел64(track, 1);  ЛогЯдра.пСимв8(",");
					ЛогЯдра.пЦел64(head, 1);  ЛогЯдра.пСимв8(",");
					ЛогЯдра.пЦел64(sector, 1);  ЛогЯдра.пСимв8(",");
					ЛогЯдра.пЦел64(num, 1);  ЛогЯдра.пСтроку8(") ")
				всё;
				если curtrack # track то	(* seek to right track *)
					PutByte(0FX);  PutByte(симв8ИзКода(арифмСдвиг(head, 2) + dev.drive));  PutByte(симв8ИзКода(track));	(* seek *)
					WaitInterrupt;
					PutByte(8X);  i := GetResults();	(* sense *)
					если (i < 1) или (result[0] * {3..7} # {5}) то
						если trace > 0 то ЛогЯдра.пСтроку8("~seek ") всё;  state := Reset
					иначе
						curtrack := track
					всё
				всё;
				если state # Reset то
					просей op из
						Read, Verify:
							SetupDMA(истина, 2, num*512);
							PutByte(0E6X)
						|Write:
							НИЗКОУР.копируйПамять(адресОт(buf[0]), dmabufvirt, num*512);
							SetupDMA(ложь, 2, num*512);
							PutByte(0C5X)
						|Format:
							нцДля i := 0 до num-1 делай
								НИЗКОУР.запишиОбъектПоАдресу(dmabufvirt+i*4+0, симв8ИзКода(track));
								НИЗКОУР.запишиОбъектПоАдресу(dmabufvirt+i*4+1, симв8ИзКода(head));
								НИЗКОУР.запишиОбъектПоАдресу(dmabufvirt+i*4+2, симв8ИзКода(i+1));
								НИЗКОУР.запишиОбъектПоАдресу(dmabufvirt+i*4+3, симв8ИзКода(2))
							кц;
							SetupDMA(ложь, 2, num*4);
							PutByte(4DX);  PutByte(симв8ИзКода(арифмСдвиг(head, 2) + dev.drive));
							PutByte(2X);  PutByte(симв8ИзКода(num));
							PutByte(dev.fgap);  PutByte(0F6X)
					всё;
					если op в {Read, Write, Verify} то	(* standard parameters *)
						PutByte(симв8ИзКода(арифмСдвиг(head, 2) + dev.drive));  PutByte(симв8ИзКода(track));	(* drive, head, track *)
						PutByte(симв8ИзКода(head));  PutByte(симв8ИзКода(sector));	(* head, sector *)
						PutByte(2X);	(* 512 byte sector *)
						PutByte(симв8ИзКода(dev.sectors));	(* last sector *)
						PutByte(dev.gap);	(* gap length *)
						PutByte(0FFX)	(* sector size (unused) *)
					всё;
					WaitInterrupt;
					если (GetResults() < 7) или (result[0] * {6,7} # {}) то
						если trace > 0 то ЛогЯдра.пСтроку8("~op ") всё;  state := Reset
					всё
				всё;
				если state = Reset то
					увел(try);  если trace > 0 то ЛогЯдра.пЦел64(try, 1);  ЛогЯдра.пСтроку8("-try ") всё;
					если try = MaxTries то
						если op в {Read, Write} то
							try := 0;  CycleMedia(dev);	(* advance to next media type *)
							если dev.media # T0 то
								прервиЦикл	(* EXIT: media type changed *)
							всё
						всё;
						если op в {Read, Verify} то Error("Read failed"); возврат 1006
						иначе Error("Write failed"); возврат 1007
						всё
					всё
				иначе
					если op = Read то
						НИЗКОУР.копируйПамять(dmabufvirt, адресОт(buf[0]), num*512)
					всё;
					прервиЦикл	(* EXIT: operation successful *)
				всё

			|Reset:
				curtrack := -1;  interrupt := ложь;  (* reset possible late interrupt *)
				ЭВМ.чПорт8(3F2H, НИЗКОУР.подмениТипЗначения(симв8, s));  исключиИзМнваНаБитах(s, 2);
				ЭВМ.пПорт8(3F2H, НИЗКОУР.подмениТипЗначения(симв8, s));
				Kernel.SetTimer(t, 1); нцДо кцПри Kernel.Expired(t);	(* > 50us *)
				включиВоМнвоНаБитах(s, 2);  ЭВМ.пПорт8(3F2H, НИЗКОУР.подмениТипЗначения(симв8, s));
				state := Recal;  WaitInterrupt;
				PutByte(8X);	(* sense *)
				если GetResults() < 1 то Error("Reset failed"); возврат 1008 всё;
				PutByte(3X);	(* specify (step rate, head load/unload) *)
				PutByte(dev.spec1);  PutByte(dev.spec2);
				если state = Reset то Error("Specify failed"); возврат 1009 всё;
				ЭВМ.пПорт8(3F7H, dev.rate);	(* data rate *)

			|Recal:
				PutByte(7X);  PutByte(симв8ИзКода(dev.drive));	(* recalibrate *)
				WaitInterrupt;
				PutByte(8X);  i := GetResults();	(* sense *)
				если (i < 1) или (result[0] * {6..7} # {}) то
					(*Error("Recalibrate failed")*)
				иначе
					state := Ready;  curtrack := 0
				всё

		всё; (* CASE *)
		увел(loops);  если loops = MaxLoops то Error("Too many retries"); возврат 1010 всё;
		если dev.media # media то возврат Disks.MediaChanged всё	(* trying new media type *)
	кц;
	если dev.media = media то возврат Ok иначе возврат Disks.MediaChanged всё
кон Do;

проц Transfer0(d: Disks.Device;  op, start, num: цел32;  перем buf: массив из симв8;  ofs: размерМЗ;  перем res: целМЗ);
перем dev: Device; sector, head, track, s, n, max, start0, num0: цел32; ofs0: размерМЗ;
нач
	dev := d(Device);
	если dev.locked то
		утв((op = Read) или (op = Write));
		если dev.type = T0 то Error("Invalid drive"); res := InvalidDrive; возврат; всё;
		если dev.media = T0 то CycleMedia(dev) всё;
		start0 := start;  num0 := num;  ofs0 := ofs;
		нцДо
			s := start;  sector := (s остОтДеленияНа dev.sectors) + 1;
			s := s DIV dev.sectors;  head := s остОтДеленияНа dev.heads;
			track := s DIV dev.heads;
			max := dev.sectors - sector + 1;	(* sectors left on track *)
			если (head = 0) и (dev.heads > 1) то
				увел(max, dev.sectors)	(* multi-track *)
			всё;
			если max > dmabufsize DIV BS то max := dmabufsize DIV BS всё;
			если num > max то n := max иначе n := num всё;
			res := Do(dev, op, sector, head, track, n, buf[ofs]);
			если res = Ok то
				умень(num, n);  увел(start, n);  увел(ofs, n*512)
			аесли res = Disks.MediaChanged то	(* media type changed, start over *)
				start := start0;  num := num0;  ofs := ofs0;  res := Ok
			иначе
				(* skip *)
			всё
		кцПри (num = 0) или (res # Ok)
	иначе
		res := Disks.MediaMissing	(* must be locked for transfer *)
	всё
кон Transfer0;

проц Transfer1(d: Disks.Device;  op, start, num: цел32;  перем buf: массив из симв8;  ofs: размерМЗ;  перем res: целМЗ);
нач {единолично}
	Transfer0(d, op, start, num, buf, ofs, res)
кон Transfer1;

проц GetSize1(d: Disks.Device;  перем size: цел32; перем res: целМЗ);
перем dev: Device;  buf: массив BS из симв8;
нач {единолично}
	dev := d(Device);
	Transfer0(dev, Read, 0, 1, buf, 0, res);
	если res = Disks.Ok то size := dev.size иначе size := 0 всё
кон GetSize1;

проц Handle1(d: Disks.Device;  перем msg: Disks.Message;  перем res: целМЗ);
перем dev: Device;  buf: массив BS из симв8;
нач {единолично}
	dev := d(Device);
	если msg суть Disks.GetGeometryMsg то
		Transfer0(dev, Read, 0, 1, buf, 0, res);
		если res = Disks.Ok то
			просейТип msg: Disks.GetGeometryMsg делай
				msg.cyls := dev.tracks;  msg.hds := dev.heads;  msg.spt := dev.sectors
			всё
		всё
	аесли msg суть Disks.LockMsg то
		если ~dev.locked то
			dev.locked := истина; res := Disks.Ok
		иначе
			res := 1001	(* already locked *)
		всё
	аесли msg суть Disks.UnlockMsg то
		если dev.locked то
			dev.locked := ложь; res := Disks.Ok;
			StopMotor(dev.drive)
		иначе
			res := 1002	(* was not locked *)
		всё
	иначе
		res := Disks.Unsupported
	всё
кон Handle1;

(* StopMotor - Switch off diskette motor *)

проц StopMotor(drive: цел32);
нач
	device[drive].media := T0;	(* reset media type *)
	ЭВМ.пПорт8(3F2H, 0CX);	(* all motors off *)
	motor := ложь
кон StopMotor;

проц StrToInt(s: массив из симв8): цел32;
перем i: цел8;  v: цел32;
нач
	v := 0;  i := 0;
	нцПока s[i] # 0X делай v := v*10+(кодСимв8(s[i])-48); увел(i) кц;
	возврат v
кон StrToInt;

проц Init;
перем s: массив 12 из симв8;  b10, b14: цел16;
нач
	ЭВМ.ДайЗначениеКлючаКонфигурацииЯОС("TraceDiskette", s);
	если s[0] # 0X то trace := устарПреобразуйКБолееУзкомуЦел(кодСимв8(s[0])-кодСимв8("0")) иначе trace := 0 всё;
	curdrive := -1;  curtrack := -1;  motor := ложь;  interrupt := ложь;  state := Reset;
	ЭВМ.ДайЗначениеКлючаКонфигурацииЯОС("Diskette", s);
	если s = "" то
		b10 := кодСимв8(ЭВМ.GetNVByte(10H));
		b14 := кодСимв8(ЭВМ.GetNVByte(14H))
	иначе
		b10 := устарПреобразуйКБолееУзкомуЦел(StrToInt(s) остОтДеленияНа 100H);
		b14 := цел16(арифмСдвиг(StrToInt(s), -8))
	всё;
	если trace > 0 то
		ЛогЯдра.пСтроку8("Diskette config:");  ЛогЯдра.п16ричное(b10, -3);
		ЛогЯдра.п16ричное(b14, -3);  ЛогЯдра.пВК_ПС
	всё;
		(* look at drive 0 setup *)
	нов(device[0]);  device[0].drive := 0;
	просей арифмСдвиг(b10, -4) из
		3: device[0].type := T720
		|4: device[0].type := T1440
		|5: device[0].type := T2880
		иначе device[0].type := T0
	всё;
	device[0].media := T0;
		(* look at drive 1 setup, if present *)
	если нечётноеЛи¿(арифмСдвиг(b14, -6)) то
		нов(device[1]);  device[1].drive := 1;
		просей b10 остОтДеленияНа 16 из
			3: device[1].type := T720
			|4: device[1].type := T1440
			|5: device[1].type := T2880
			иначе device[1].type := T0
		всё;
		device[1].media := T0
	(*ELSE device[1].type := T0*)
	всё
кон Init;

проц Register;
перем i: цел32; res: целМЗ; dev: Device; name: Plugins.Name;
нач
	нцДля i := 0 до MaxDevices-1 делай
		dev := device[i];
		если dev # НУЛЬ то
			name := "Diskette0"; name[8] := симв8ИзКода(48 + i);
			dev.SetName(name); dev.desc := "Standard Diskette";
			dev.blockSize := BS; dev.flags := {Disks.Removable};
			Disks.registry.Add(dev, res);
			утв(res = Plugins.Ok)
		всё
	кц
кон Register;

проц Cleanup;
перем i: цел32;
нач {единолично}
	если Modules.shutdown = Modules.None то
		нцДля i := 0 до MaxDevices-1 делай
			если device[i] # НУЛЬ то
				Disks.registry.Remove(device[i]);
				StopMotor(device[i].drive);
				device[i] := НУЛЬ
			всё
		кц;
		ЭВМ.RemoveHandler(InterruptHandler, ЭВМ.IRQ0+6);
	всё;
кон Cleanup;

(** Install the diskette devices.  Automatically executed when the module is loaded. *)
проц Install*;
кон Install;

нач
	dmabufsize := НИЗКОУР.подмениТипЗначения (цел32, ЭВМ.dmaSize);
	если dmabufsize > 0 то
		dmabufphys := ЭВМ.lowTop;
		ЭВМ.MapPhysical(dmabufphys, dmabufsize, dmabufvirt);
		если dmabufphys # ЭВМ.НулевойАдрес то
			Init;
			ЭВМ.пПорт8(3F2H, 0CX); (* motors off, select drive 0, clear reset *)
			ЭВМ.InstallHandler(InterruptHandler, ЭВМ.IRQ0+6);
			Register;
		всё;
	всё;
	Modules.InstallTermHandler(Cleanup);
кон Diskettes.

(*
Results
-5	Disks.MediaMissing, transfer attempted on unlocked device
0	Disks.Ok, no error
1001	already locked
1002	was not locked
1003	bad number of sectors
1004	invalid track
1005	invalid head
1006	read failed
1007	write failed
1008	reset failed
1009	specify failed
1010	too many retries
1011	Invalid drive

Diskettes.Install ~

System.Free Diskettes ~

Partitions.Show

to do:
o clean up Format
*)

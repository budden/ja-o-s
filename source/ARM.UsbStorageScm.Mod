модуль UsbStorageScm; (** AUTHOR "cplattner/staubesv"; PURPOSE "SCM transport layer of USB mass storage driver"; *)
(**
 * SCSI commands borrowed from SCSI.Mod.
 *
 * History:
 *
 *	09.02.2006	First release (staubesv)
 *)

использует
	НИЗКОУР, ЛогЯдра, Kernel,
	Usbdi, Base := UsbStorageBase, Debug := UsbDebug, UsbStorageCbi;

конст

	(* Constans for the SCM USB-ATAPI Shuttle device *)
	ScmATA = 40X; ScmISA = 50X;

	(* Data registers *)
	ScmUioEpad = 80X; ScmUioCdt = 40X; ScmUio1 = 20X; ScmUio0 = 10X;

	(* User i/o enable registers *)
	ScmUioDrvrst = 80X; ScmUioAckd = 40X; ScmUioOE1 = 20X; ScmUioOE0 = 10X;

тип

	(* SCM Shuttle Transport Layer  *)
	SCMTransport* = окласс(UsbStorageCbi.CBITransport) (* same Reset procedure as CBITransport -> inherit it *)
	перем
		(* these buffers will be re-used; they are created in &Init *)
		command : Usbdi.BufferPtr;
		buffer : Usbdi.BufferPtr;
		timer : Kernel.Timer;

		проц ScmShortPack(p1, p2 : симв8) : цел16;
		нач
			возврат НИЗКОУР.подмениТипЗначения(цел16, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, кодСимв8(p2)*256) + НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, кодСимв8(p1)));
		кон ScmShortPack;

		проц ScmSendControl(dir : мнвоНаБитахМЗ; req, reqtyp, value, index : цел32; перем buffer : Usbdi.Buffer;
			ofs, bufferlen, timeout : цел32) : цел32;
		перем status : Usbdi.Status; 	ignore : цел32;
		нач
			если Debug.Trace и Debug.traceScTransfers то
				ЛогЯдра.пСтроку8("UsbStorage: Sending SCM Control:"); ЛогЯдра.пСтроку8(" Direction: ");
				если dir = Base.DataIn то ЛогЯдра.пСтроку8("In");
				аесли dir = Base.DataOut то ЛогЯдра.пСтроку8("Out");
				иначе ЛогЯдра.пСтроку8("Unknown");
				всё;
				ЛогЯдра.пСтроку8(" Bufferlen: "); ЛогЯдра.пЦел64(bufferlen, 0); ЛогЯдра.пСтроку8(" Offset: "); ЛогЯдра.пЦел64(ofs, 0); ЛогЯдра.пВК_ПС;
			всё;
			если (bufferlen > 0) и (bufferlen+ofs > длинаМассива(buffer)) то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: ScmSendControl: Buffer underrun"); ЛогЯдра.пВК_ПС; всё;
				возврат Base.ResFatalError;
			всё;

			если device.Request(НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, reqtyp), req, value, index, bufferlen, buffer) # Usbdi.Ok то
				status := defaultPipe.GetStatus(ignore);
				если status = Usbdi.Stalled то
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: Stall on Transport SCM Control"); ЛогЯдра.пВК_ПС; всё;
					если defaultPipe.ClearHalt() то
						возврат Base.ResError;
					иначе
						если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: Failure on Transport SCM clear halt on Controlpipe"); ЛогЯдра.пВК_ПС; всё;
						возврат Base.ResFatalError;
					всё;
				всё;
				если status = Usbdi.InProgress то
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: Timeout on Transport SCM Control"); ЛогЯдра.пВК_ПС; всё;
					возврат Base.ResTimeout;
				аесли status = Usbdi.Disconnected то
					возврат Base.ResDisconnected;
				аесли status # Usbdi.Ok то
					если Debug.Level >= Debug.Errors  то ЛогЯдра.пСтроку8("UsbStorage: Failure on Transport SCM Control"); ЛогЯдра.пВК_ПС; всё;
					возврат Base.ResFatalError;
				всё;
			всё;
			возврат Base.ResOk;
		кон ScmSendControl;

		проц ScmBulkTransport(dir : мнвоНаБитахМЗ; перем buffer : Usbdi.Buffer; ofs, bufferlen : цел32;
			перем tlen : цел32; timeout : цел32) : цел32;
		перем status : Usbdi.Status;
		нач
			если Debug.Trace и Debug.traceScTransfers то
				ЛогЯдра.пСтроку8("UsbStorage: Transfering SCM Data: Direction: ");
				если dir = Base.DataIn то ЛогЯдра.пСтроку8("IN");
				аесли dir =  Base.DataOut то ЛогЯдра.пСтроку8("OUT");
				иначе ЛогЯдра.пСтроку8("Unknown");
				всё;
				ЛогЯдра.пСтроку8(" Bufferlen: "); ЛогЯдра.пЦел64(bufferlen, 0); ЛогЯдра.пСтроку8(" Offset: "); ЛогЯдра.пЦел64(ofs, 0);
				ЛогЯдра.пВК_ПС;
			всё;
			tlen := 0;
			если bufferlen = 0 то возврат Base.ResOk всё;

			если bufferlen + ofs > длинаМассива(buffer) то
				если Debug.Level >= Debug.Errors то
					ЛогЯдра.пСтроку8("UsbStorage: ScmBulkTransport: Buffer underrun");
					ЛогЯдра.пСтроку8(" (buffer length: "); ЛогЯдра.пЦел64(длинаМассива(buffer), 0); ЛогЯдра.пСтроку8(")"); ЛогЯдра.пВК_ПС;
				всё;
				возврат Base.ResFatalError;
			всё;

			если dir = Base.DataIn то
				bulkInPipe.SetTimeout(timeout);
				status := bulkInPipe.Transfer(bufferlen, ofs, buffer);
				tlen := bulkInPipe.GetActLen();
			аесли dir = Base.DataOut то
				bulkOutPipe.SetTimeout(timeout);
				status := bulkOutPipe.Transfer(bufferlen, ofs, buffer);
				tlen := bulkOutPipe.GetActLen();
			иначе
				СТОП(301);
			всё;

			(* clear halt if STALL occured, but do not abort!!! *)
			если status = Usbdi.Stalled то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: Stall on SCM data phase"); ЛогЯдра.пВК_ПС; всё;
				(* only abort if clear halt fails *)
				если ((dir=Base.DataIn) и ~bulkInPipe.ClearHalt()) или ((dir=Base.DataOut) и ~bulkOutPipe.ClearHalt())  то
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: Failure on SCM bulk clear halt"); ЛогЯдра.пВК_ПС; всё;
					возврат Base.ResFatalError;
				всё;
			всё;

			если status = Usbdi.InProgress то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: Timeout on SCM data phase"); ЛогЯдра.пВК_ПС; всё;
				возврат Base.ResTimeout;
			аесли status = Usbdi.Disconnected то
				возврат Base.ResDisconnected;
			аесли status = Usbdi.Error то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: Failure on SCM bulk"); ЛогЯдра.пВК_ПС; всё;
				возврат Base.ResFatalError;
			всё;

			если tlen # bufferlen то
				если Debug.Level >= Debug.Errors то
					ЛогЯдра.пСтроку8("UsbStorage: ScmBulkTransport short read: ");
					ЛогЯдра.пЦел64(bufferlen, 0); ЛогЯдра.пСимв8("/"); ЛогЯдра.пЦел64(tlen, 0); ЛогЯдра.пВК_ПС;
				всё;
				возврат Base.ResShortTransfer;
			всё;
			возврат Base.ResOk;
			кон ScmBulkTransport;

		проц ScmWaitNotBusy(timeout : цел32) : цел32;
		перем status : симв8; res : цел32;
		нач
			нц
				res := ScmRead(ScmATA, 17X, status, 1000);
				если res # Base.ResOk то
					если (res = Base.ResDisconnected) или (res = Base.ResFatalError) то возврат res иначе возврат Base.ResError всё;
				аесли (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, status) * {0}) # {} то
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: ScmWaitNotBusy: check condition"); ЛогЯдра.пВК_ПС; всё;
					возврат Base.ResError;
				аесли (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, status) * {5}) # {} то
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: ScmWaitNotBusy: device fault"); ЛогЯдра.пВК_ПС; всё;
					возврат Base.ResFatalError;
				аесли (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, status) * {7}) = {} то
					если Debug.Trace и Debug.traceScTransfers то ЛогЯдра.пСтроку8("UsbStorage: ScmWaitNotBusy: good"); ЛогЯдра.пВК_ПС; всё;
					возврат Base.ResOk;
				всё;
				если timeout # -1 то
					timeout  := timeout - 10; если timeout < 0 то прервиЦикл всё;
				всё;
				Wait(10);
			кц;
			если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: ScmWaitNotBusy: Timeout"); ЛогЯдра.пВК_ПС; всё;
			возврат Base.ResTimeout;
		кон ScmWaitNotBusy;

		проц ScmReadUserIO(перем dataflags: симв8; timeout : цел32) : цел32;
		перем res : цел32;
		нач
			res := ScmSendControl(Base.DataIn, 82H, 0C0H, 0, 0, buffer, 0, 1, timeout);
			dataflags := buffer[0];
			возврат res;
		кон ScmReadUserIO;

		проц ScmWriteUserIO(enableflags, dataflags: симв8; timeout : цел32) : цел32;
		нач
			возврат ScmSendControl(Base.DataOut, 82H, 40H, ScmShortPack(enableflags, dataflags), 0, Usbdi.NoData, 0, 0, timeout);
		кон ScmWriteUserIO;

		проц ScmRead(access, reg : симв8; перем content: симв8; timeout : цел32) : цел32;
		перем res : цел32;
		нач
			res := ScmSendControl(Base.DataIn, кодСимв8(access), 0C0H, кодСимв8(reg), 0, buffer, 0, 1, timeout);
			content := buffer[0];
			возврат res;
		кон ScmRead;

		проц ScmWrite(access, reg, content : симв8; timeout : цел32) : цел32;
		нач
			access := НИЗКОУР.подмениТипЗначения(симв8, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, access) + {0});
			возврат ScmSendControl(Base.DataOut, кодСимв8(access), 040H, ScmShortPack(reg, content), 0, Usbdi.NoData, 0, 0, timeout);
		кон ScmWrite;

		проц ScmMultipleWrite(access : симв8; перем registers, dataout: массив из симв8; numregs,  timeout : цел32) : цел32;
		перем res, i, tlen : цел32;
		нач
			если numregs > 7 то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: ScmMultipleWrite: Numregs > 7."); ЛогЯдра.пВК_ПС;всё;
				возврат Base.ResFatalError;
			всё;
			command[0] := 40X;
			command[1] := НИЗКОУР.подмениТипЗначения(симв8, (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, access)+ {0,1,2}));
			command[2] := 0X;
			command[3] := 0X;
			command[4] := 0X;
			command[5] := 0X;
			command[6] := симв8ИзКода(numregs*2);
			command[7] := симв8ИзКода(логСдвиг(numregs*2, -8));

			нцДля i:= 0 до numregs - 1 делай
				buffer[i*2] := registers[i];
				buffer[(i*2)+1] := dataout[i];
			кц;

			res := ScmSendControl(Base.DataOut, 80H, 040H, 0, 0, command, 0, 8, timeout);
			если res # Base.ResOk то возврат res всё;

			res := ScmBulkTransport(Base.DataOut, buffer, 0, numregs*2, tlen, timeout);
			если res # Base.ResOk то возврат res всё;

			возврат ScmWaitNotBusy(timeout);
		кон ScmMultipleWrite;

		проц ScmReadBlock(access, reg : симв8; перем content : Usbdi.Buffer; ofs, len: цел32; перем tlen : цел32; timeout : цел32): цел32;
		перем res : цел32;
		нач
			command[0] := 0C0X;
			command[1] := НИЗКОУР.подмениТипЗначения(симв8, (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, access)+ {1}));
			command[2] := reg;
			command[3] := 0X;
			command[4] := 0X;
			command[5] := 0X;
			command[6] := симв8ИзКода(len);
			command[7] := симв8ИзКода(логСдвиг(len, -8));

			tlen := 0;
			res := ScmSendControl(Base.DataOut, 80H, 40H, 0, 0, command, 0, 8, timeout);
			если res # Base.ResOk то возврат res всё;
			res := ScmBulkTransport(Base.DataIn, content, ofs, len, tlen, timeout);
			возврат res;
		кон ScmReadBlock;

		проц ScmWriteBlock(access, reg : симв8; перем content : Usbdi.Buffer; ofs, len : цел32; перем tlen : цел32; timeout : цел32): цел32;
		перем res : цел32;
		нач
			command[0] := 40X;
			command[1] := НИЗКОУР.подмениТипЗначения(симв8, (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, access)+ {0,1}));
			command[2] := reg;
			command[3] := 0X;
			command[4] := 0X;
			command[5] := 0X;
			command[6] := симв8ИзКода(len);
			command[7] := симв8ИзКода(логСдвиг(len, -8));

			tlen := 0;
			res := ScmSendControl(Base.DataOut, 80H, 40H, 0, 0, command, 0, 8, timeout);
			если res # Base.ResOk то возврат res всё;

			res := ScmBulkTransport(Base.DataOut, content, ofs, len, tlen, timeout);
			если res # Base.ResOk то возврат res всё;
			возврат ScmWaitNotBusy(timeout);
		кон ScmWriteBlock;

		проц ScmRWBlockTest(access : симв8; перем registers, dataout : массив из симв8;
			numregs :цел16; datareg, statusreg, atapitimeout, qualifier : симв8; dir : мнвоНаБитахМЗ; перем content : Usbdi.Buffer;
			ofs, contentlen: цел32; перем tlen : цел32; timeout : цел32): цел32;
		перем
			tmpreg : симв8;
			status : симв8;
			i, msgindex, msglen : цел16;
			tmplen : цел32;
			res : цел32;
		нач
			если numregs > 19 то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: ScmRWBlockTest too many registers"); ЛогЯдра.пВК_ПС; всё;
				возврат Base.ResFatalError;
			всё;
			утв(длинаМассива(command)>=16);
			command[0] := 40X;
			command[1] := НИЗКОУР.подмениТипЗначения(симв8, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ,access) + {0,1,2});
			command[2] := 7X;
			command[3] := 17X;
			command[4] := 0FCX;
			command[5] := 0E7X;
			command[6] := симв8ИзКода(numregs*2);
			command[7] := симв8ИзКода(логСдвиг(numregs*2, -8));
			если dir = Base.DataOut то
				command[8] := 40X;
				command[9] := НИЗКОУР.подмениТипЗначения(симв8, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, access) + {0,2});
			аесли dir = Base.DataIn то
				command[8] := 0C0X;
				command[9] := НИЗКОУР.подмениТипЗначения(симв8, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, access) + {2});
			иначе
				СТОП(303)
			всё;
			command[10] := datareg;
			command[11] := statusreg;
			command[12] := atapitimeout;
			command[13] := qualifier;
			command[14] := симв8ИзКода(contentlen);
			command[15] := симв8ИзКода(логСдвиг(contentlen, -8));

			нцДля i:=0 до numregs -1 делай
				buffer[i*2] := registers[i]; buffer[(i*2)+1] := dataout[i];
			кц;

			tlen := 0;

			нцДля i := 0 до 19 делай

				если i = 0 то msgindex := 0; msglen := 16 иначе msgindex := 8; msglen := 8 всё;

				res := ScmSendControl(Base.DataOut, 80H, 40H, 0, 0, command, msgindex, msglen, 1000);
				если res # Base.ResOk то
					если (res = Base.ResFatalError) или (res = Base.ResTimeout) или (res = Base.ResDisconnected) то возврат res иначе возврат Base.ResError всё;
				всё;

				если i = 0 то
					res := ScmBulkTransport(Base.DataOut, buffer, 0, numregs*2, tmplen, 1000);
					если res # Base.ResOk то
						если (res = Base.ResFatalError) или (res = Base.ResTimeout) или (res = Base.ResDisconnected)  то возврат res иначе возврат Base.ResError всё;
					всё;
				всё;

				res := ScmBulkTransport(dir, content, 0, contentlen, tlen, timeout);
				если res = Base.ResShortTransfer то
					если (dir = Base.DataIn) и (i=0) то (* hm. makes somehow no sense, but that's life *)
						если ~bulkOutPipe.ClearHalt() то
							если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: ScmRWBlockTest clear halt failed"); ЛогЯдра.пВК_ПС; всё;
						всё
					всё;

				если dir = Base.DataOut то tmpreg := 17X; иначе tmpreg := 0EX; всё;

				res := ScmRead(ScmATA, tmpreg, status, 1000);
				если res # Base.ResOk то
					если (res = Base.ResFatalError) или (res = Base.ResDisconnected) или (res = Base.ResTimeout)  то возврат res иначе возврат Base.ResError всё;
					аесли (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, status) * {0}) # {} то
						если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: ScmRWBlockTest: check condition"); ЛогЯдра.пВК_ПС; всё;
						возврат Base.ResError;
					аесли (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, status) * {5}) # {} то
						если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: ScmRWBlockTest: device fault"); ЛогЯдра.пВК_ПС; всё;
						возврат Base.ResFatalError;
					всё;
				иначе
					возврат ScmWaitNotBusy(timeout);
				всё;
			кц;
			если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: ScmRWBlockTest failed 20 times!"); ЛогЯдра.пВК_ПС; всё;
			возврат Base.ResError;
		кон ScmRWBlockTest;

		проц ScmSelectAndTestRegisters() : булево;
		перем selector : цел16; status : симв8;
		нач
			нцДля selector := 0A0H до 0B0H шаг 10H делай (* actually, test 0A0H and 0B0H *)
				если ScmWrite(ScmATA, 16X, симв8ИзКода(selector), 1000) # Base.ResOk то возврат ложь всё;
				если ScmRead(ScmATA, 17X, status, 1000) # Base.ResOk то возврат ложь всё;
				если ScmRead(ScmATA, 16X, status, 1000) # Base.ResOk то возврат ложь всё;
				если ScmRead(ScmATA, 14X, status, 1000) # Base.ResOk то возврат ложь всё;
				если ScmRead(ScmATA, 15X, status, 1000) # Base.ResOk то возврат ложь всё;
				если ScmWrite(ScmATA, 14X, 55X, 1000) # Base.ResOk то возврат ложь всё;
				если ScmWrite(ScmATA, 15X, 0AAX, 1000) # Base.ResOk то возврат ложь всё;
				если ScmRead(ScmATA, 14X, status, 1000) # Base.ResOk то возврат ложь всё;
				если ScmRead(ScmATA, 15X, status, 1000) # Base.ResOk то возврат ложь всё;
			кц;
			возврат истина;
		кон ScmSelectAndTestRegisters;

		проц ScmSetShuttleFeatures(externaltrigger, eppcontrol, maskbyte, testpattern, subcountH, subcountL : симв8) : булево;
		нач
			command[0] := 40X;
			command[1] := 81X;
			command[2] := eppcontrol;
			command[3] := externaltrigger;
			command[4] := testpattern;
			command[5] := maskbyte;
			command[6] := subcountL;
			command[7] := subcountH;
			если ScmSendControl(Base.DataOut, 80H, 40H, 0, 0, command, 0, 8, 1000) # Base.ResOk то
				возврат ложь;
			иначе
				возврат истина;
			всё;
		кон ScmSetShuttleFeatures;

		проц {перекрыта}Initialization*() : булево;
		перем status : симв8; res : цел32;
		нач
			если Debug.Trace и Debug.traceScInit то ЛогЯдра.пСтроку8("UsbStorage: Initializing SCM USB-ATAPI Shuttle... "); ЛогЯдра.пВК_ПС; всё;
			res := ScmWriteUserIO(НИЗКОУР.подмениТипЗначения(симв8, (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUioOE0) + НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUioOE1))),
				НИЗКОУР.подмениТипЗначения(симв8, (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUioEpad) + НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUio1))), 1000);
			если res # Base.ResOk то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM Init error, step 1"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			Wait(2000);

			res := ScmReadUserIO(status, 1000);
			если res # Base.ResOk то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM Init error, step 2"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			res := ScmReadUserIO(status, 1000);
			если res # Base.ResOk то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM Init error, step 3"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			res := ScmWriteUserIO(НИЗКОУР.подмениТипЗначения(симв8, (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUioDrvrst) + НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUioOE0)
				+ НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUioOE1))), НИЗКОУР.подмениТипЗначения(симв8, (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUioEpad)
				+ НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUio1))), 1000);
			если res # Base.ResOk то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM Init error, step 4"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			res := ScmWriteUserIO(НИЗКОУР.подмениТипЗначения(симв8, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUioOE0) + НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUioOE1)),
				 НИЗКОУР.подмениТипЗначения(симв8, (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUioEpad) + НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUio1))), 1000);
			если res # Base.ResOk то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM Init error, step 5"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			Wait(250);

			res := ScmWrite(ScmISA, 03FX, 080X, 1000);
			если res # Base.ResOk то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM Init error, step 6"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;
			res := ScmRead(ScmISA, 027X, status, 1000);
			если res # Base.ResOk то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM Init error, step 7"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;
			res := ScmReadUserIO(status, 1000);
			если res # Base.ResOk то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM Init error, step 8"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;
			если ~ScmSelectAndTestRegisters() то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM Init error, step 9"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			res := ScmReadUserIO(status, 1000);
			если res # Base.ResOk то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM Init error, step 10"); ЛогЯдра.пВК_ПС; всё;
				 возврат ложь;
			всё;

			res := ScmWriteUserIO(НИЗКОУР.подмениТипЗначения(симв8, (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUioAckd) + НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUioOE0)
				+ НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUioOE1))), НИЗКОУР.подмениТипЗначения(симв8, (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUioEpad)
				+ НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ScmUio1))), 1000);
			если res # Base.ResOk то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM Init error, step 11"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			res := ScmReadUserIO(status, 1000);
			если res # Base.ResOk то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM Init error, step 12"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			Wait(1400);

			res := ScmReadUserIO(status, 1000);
			если res # Base.ResOk то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM Init error, step 13"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;
			если ~ScmSelectAndTestRegisters() то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM Init error, step 14"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;
			если ~ScmSetShuttleFeatures(83X, 0X, 88X, 08X, 15X, 14X) то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM Init error, step 15"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;
			если  Debug.Trace и Debug.traceScInit то ЛогЯдра.пСтроку8("UsbStorage: Initialization done."); ЛогЯдра.пВК_ПС; всё;
			возврат истина;
		кон Initialization;

		проц {перекрыта}Transport*(cmd : массив из симв8; cmdlen : цел32; dir : мнвоНаБитахМЗ;
			перем buffer : массив из симв8; ofs, bufferlen : цел32; перем tlen : цел32; timeout : цел32) : цел32;
		перем
			registers, data : массив 32 из симв8;
			i, res : цел32;
			status : симв8;
			atapilen, tmplen, sector, transfered : цел32;
			j : цел32;
			c, b: Usbdi.Buffer;
		нач
			если Debug.Trace и Debug.traceScTransfers то
				ЛогЯдра.пСтроку8("UsbStorage: Transport:");
				ЛогЯдра.пСтроку8(" Direction: ");
				если dir = Base.DataIn то ЛогЯдра.пСтроку8("In");
				аесли dir = Base.DataOut то ЛогЯдра.пСтроку8("Out");
				иначе ЛогЯдра.пСтроку8("Unknown");
				всё;
				ЛогЯдра.пСтроку8(" Bufferlen: "); ЛогЯдра.пЦел64(bufferlen, 0); ЛогЯдра.пСтроку8(" Offset: "); ЛогЯдра.пЦел64(ofs, 0);
				ЛогЯдра.пСтроку8(" Cmd: ");
				если cmdlen = 0 то ЛогЯдра.пСтроку8("None");
				иначе
					нцДля j := 0 до cmdlen-1 делай ЛогЯдра.пЦел64(кодСимв8(cmd[j]), 0); ЛогЯдра.пСимв8(" "); кц;
				всё;
				ЛогЯдра.пВК_ПС;
			всё;
			registers[0] := 11X;
			registers[1] := 12X;
			registers[2] := 13X;
			registers[3] := 14X;
			registers[4] := 15X;
			registers[5] := 16X;
			registers[6] := 17X;

			data[0] := 0X;
			data[1] := 0X;
			data[2] := 0X;
			data[3] := симв8ИзКода(bufferlen);
			data[4] := симв8ИзКода(логСдвиг(bufferlen, -8));
			data[5] := 0B0X;
			data[6] := 0A0X;

			нцДля i:= 7 до 18 делай
				registers[i] := 010X;
				если (i - 7) >= cmdlen то data[i] := 0X; иначе data[i] := cmd[i-7]; всё;
			кц;

			tlen := 0;

			нов(b, bufferlen);
			нцДля i := 0 до bufferlen - 1 делай b[i] := buffer[ofs + i] кц;

			нов(c, cmdlen);
			нцДля i := 0 до cmdlen - 1 делай c[i] := cmd[i] кц;

			если dir = Base.DataOut то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM DataOut not supported!!!"); ЛогЯдра.пВК_ПС; всё;
				возврат Base.ResUnsupported;
			всё;

			если bufferlen > 65535 то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: Too large request for SCM USB-ATAPI Shuttle"); ЛогЯдра.пВК_ПС;	всё;
				возврат Base.ResUnsupported;
			всё;

			если cmd[0] = симв8ИзКода(Base.UfiRead10) то
				если bufferlen < 10000H то
					если Debug.Trace и Debug.traceScTransfers то ЛогЯдра.пСтроку8("UsbStorage: Doing SCM single read"); ЛогЯдра.пВК_ПС; всё;
					res := ScmRWBlockTest(ScmATA, registers, data, 19, 10X, 17X, 0FDX, 30X, Base.DataIn, b, ofs, bufferlen, tlen, timeout);
					возврат res;
				иначе
					если Debug.Trace и Debug.traceScTransfers то ЛогЯдра.пСтроку8("UsbStorage: Doing SCM multi read"); ЛогЯдра.пВК_ПС; всё;
					tmplen := (65535 DIV sdevs.blockSize) * sdevs.blockSize;
					sector := логСдвиг(ScmShortPack(data[10], data[9]), 16) + ScmShortPack(data[12], data[11]);
					transfered := 0;
					нцПока transfered # bufferlen делай
						если tmplen > (bufferlen - transfered) то tmplen := bufferlen - transfered всё;
						data[3] := симв8ИзКода(tmplen);
						data[4] := симв8ИзКода(логСдвиг(tmplen, -8));
						data[9] := симв8ИзКода(логСдвиг(sector, -24));
						data[10] := симв8ИзКода(логСдвиг(sector, -16));
						data[11] := симв8ИзКода(логСдвиг(sector, -8));
						data[12] := симв8ИзКода(sector);
						data[14] := симв8ИзКода(логСдвиг(tmplen DIV sdevs.blockSize, -8));
						data[15] := симв8ИзКода(tmplen DIV sdevs.blockSize);
						res := ScmRWBlockTest(ScmATA, registers, data, 19, 10X, 17X, 0FDX, 30X, Base.DataIn, b, ofs+transfered, tmplen, atapilen, timeout);
						transfered := transfered + atapilen; tlen := transfered;
						sector := sector + (tmplen DIV sdevs.blockSize);
						если res # Base.ResOk то возврат res всё;
					кц;
					возврат Base.ResOk;
				всё;
			всё;

			если Debug.Trace и Debug.traceScTransfers то ЛогЯдра.пСтроку8("UsbStorage: Sending SCM registers"); ЛогЯдра.пВК_ПС; всё;
			res := ScmMultipleWrite(ScmATA, registers, data, 7, 1000);
			если (res = Base.ResDisconnected) то
				возврат res;
			аесли (res # Base.ResOk) то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM register setup failed"); ЛогЯдра.пВК_ПС; всё;
				возврат Base.ResError
			всё;

			если cmdlen # 12 то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM command len # 12"); ЛогЯдра.пВК_ПС; всё;
				возврат Base.ResFatalError;
			всё;

			если Debug.Trace и Debug.traceScTransfers то ЛогЯдра.пСтроку8("UsbStorage: Sending SCM command"); ЛогЯдра.пВК_ПС; всё;

			res := ScmWriteBlock(ScmATA, 10X, c, 0, 12, tmplen, timeout);
			если (res = Base.ResDisconnected) то
				возврат res;
			аесли res # Base.ResOk то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: SCM command transfer failed"); ЛогЯдра.пВК_ПС; всё;
				возврат Base.ResError
			всё;

			если (bufferlen # 0) и (dir = Base.DataIn) то
				если Debug.Trace и Debug.traceScTransfers то ЛогЯдра.пСтроку8("UsbStorage: SCM  data transfer"); ЛогЯдра.пВК_ПС; всё;
				res := ScmRead(ScmATA, 014X, status, 1000);
				если (res = Base.ResDisconnected) то
					возврат res;
				аесли res # Base.ResOk то
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: ScmRead failed"); ЛогЯдра.пВК_ПС; всё;
					возврат Base.ResError
				всё;
				atapilen := кодСимв8(status);
				если bufferlen > 255 то
					res := ScmRead(ScmATA, 015X, status, 1000);
					если (res = Base.ResDisconnected) то
						возврат res;
					аесли res # Base.ResOk то
						если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: ScmRead2 failed"); ЛогЯдра.пВК_ПС; всё;
						возврат res;
					всё;
					atapilen := atapilen + (кодСимв8(status) * 256);
				всё;
				если Debug.Trace и Debug.traceScTransfers то
					ЛогЯдра.пСтроку8("UsbStorage: Scm Transfer: Want: "); ЛогЯдра.пЦел64(bufferlen, 0);
					ЛогЯдра.пСтроку8(" / have: "); ЛогЯдра.пЦел64(atapilen, 0); ЛогЯдра.пВК_ПС;
				всё;
				tmplen := atapilen;
				если atapilen < bufferlen то
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: Scm has FEWER bytes in the atapi buffer"); ЛогЯдра.пВК_ПС; всё;
				аесли atapilen > bufferlen то
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: Scm has MORE bytes in the atapi buffer"); ЛогЯдра.пВК_ПС; всё;
					tmplen := bufferlen;
				всё;

				res := ScmReadBlock(ScmATA, 10X, b, ofs, tmplen, tlen, timeout);
				если Debug.Trace и Debug.traceScTransfers то
					если (res = Base.ResOk) или (res = Base.ResShortTransfer) то
						ЛогЯдра.пСтроку8("UsbStorage: wanted: "); ЛогЯдра.пЦел64(tmplen, 0);
						ЛогЯдра.пСтроку8(" / got: "); ЛогЯдра.пЦел64(tlen, 0); ЛогЯдра.пВК_ПС;
					всё;
				всё;
				если (res = Base.ResOk) и (atapilen < bufferlen) то res := Base.ResShortTransfer всё;
				если (res # Base.ResOk) и (res # Base.ResShortTransfer) то
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorage: ScmReadBlock failed"); ЛогЯдра.пВК_ПС; всё;
					возврат res;
				всё;
			иначе
				tlen := 0;
			всё;

			возврат Base.ResOk;
		кон Transport;

		проц Wait(ms : цел32);
		нач
			timer.Sleep(ms);
		кон Wait;

		проц &{перекрыта}Init*;
		нач
			Init^; нов(command, 16); нов(buffer, 64); нов(timer);
		кон Init;

	кон SCMTransport;

кон UsbStorageScm.

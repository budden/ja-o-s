модуль FoxTRMAssembler; (** AUTHOR ""; PURPOSE ""; *)

использует InstructionSet := FoxTRMInstructionSet, FoxAssembler, D := Debugging, Scanner := FoxScanner, Diagnostics, Basic := FoxBasic;

конст Trace=FoxAssembler.Trace;

тип
	Register* = цел32; (* index for InstructionSet.registers *)
	Operand* = InstructionSet.Operand;

тип
	Assembler*= окласс (FoxAssembler.Assembler)
	перем capabilities-: мнвоНаБитахМЗ;
		instructionSet: InstructionSet.InstructionSet;

		проц &Init2*(diagnostics: Diagnostics.Diagnostics; l0capabilities: мнвоНаБитахМЗ; l1instructionSet: InstructionSet.InstructionSet);
		нач
			сам.capabilities := l0capabilities;
			сам.instructionSet:=l1instructionSet;
			Init(diagnostics);
		кон Init2;

		проц {перекрыта}Instruction*(конст mnemonic: массив из симв8);
		перем i,numberOperands,mnem: цел32; перем operands: массив 3 из Operand; instruction: InstructionSet.Instruction;
			pos: Basic.Position;

			проц ParseOperand;
			(* stub, must be overwritten by implementation *)
			перем operand: InstructionSet.Operand;
				result: FoxAssembler.Result;
				register1,register2: цел8;
				stop,memory: булево;
			нач
				stop := ложь;
				register1 := InstructionSet.None;
				register2 := InstructionSet.None;
				result.type := -1;
				result.value := 0;

				если numberOperands >= 2 то Error(errorPosition,"too many operands")
				иначе
					memory := ThisSymbol(Scanner.ОткрывающаяКвадратнаяСкобка);
					если (token.видЛексемы = Scanner.Идентификатор) и GetRegister(token.строкаИдентификатора,register1) то
						NextToken;
						stop := ~ThisSymbol(Scanner.ЗнакПлюс);
					всё;
					если ~stop то
						если (token.видЛексемы = Scanner.Идентификатор) то
							если GetRegister(token.строкаИдентификатора,register2) то
								NextToken;
							аесли GetNonConstant(errorPosition,token.строкаИдентификатора, result) то
								NextToken;
							аесли Expression(result,ложь) то
							всё;
						аесли Expression(result,ложь) то
						всё;
					всё;
					если memory и ExpectSymbol(Scanner.ЗакрывающаяКвадратнаяСкобка) то
						instructionSet.InitMemory(operand,register1,цел32(result.value));
					аесли register1 # -1 то
						instructionSet.InitRegister(operand,register1);
					иначе
						instructionSet.InitImmediate(operand,result.sizeInBits(цел32),цел32(result.value));
					всё;
					если result.fixup # НУЛЬ то
						instructionSet.AddFixup(operand,result.fixup);
					всё;
					operands[numberOperands] := operand;
				всё;
			кон ParseOperand;

		нач
			если Trace то
				D.String("Instruction: "); D.String(mnemonic);  D.String(" "); D.Ln;
			всё;
			pos := errorPosition;
			mnem := instructionSet.FindMnemonic(mnemonic);
			если mnem >= 0 то
				нцДля i := 0 до 2 делай instructionSet.InitOperand(operands[i]) кц;
				numberOperands := 0;
				если token.видЛексемы # Scanner.Ln то
					нцДо
						ParseOperand;
						увел(numberOperands);
					кцПри error или ~ThisSymbol(Scanner.Запятая);
				всё;
				если ~error то
					instructionSet.MakeInstruction(instruction,mnem,operands[0],operands[1]);
					если instruction.format = InstructionSet.None то
						ErrorSS(pos,"operand instruction format mismatch",mnemonic);
					аесли instructionSet.instructionFormats[instruction.format].capabilities > capabilities то
						Error(pos,"instruction not supported");
					иначе
						если pass < FoxAssembler.MaxPasses то
							(* not last pass: only increment the current PC by a unit *)
							section.resolved.SetPC(section.resolved.pc + 1)
						иначе
							instructionSet.EmitInstruction(instruction, mnem, section.resolved);
						всё;
					всё;
				всё
			иначе
				ErrorSS(pos,"unknown instruction ",mnemonic)
			всё
		кон Instruction;

		проц GetRegister(конст ident: массив из симв8; перем register: цел8): булево;
		нач
			register := instructionSet.FindRegister(ident);
			возврат register # InstructionSet.None
		кон GetRegister;
	кон Assembler;

кон FoxTRMAssembler.

System.Free FoxTRMAssembler FoxTRMInstructionSet ~

модуль UsbEhciZynq; (** AUTHOR "Timothée Martiel"; PURPOSE "Instal EHCI driver on Zynq systems."; *)
(**
 * The main purpose of this module is to install the EHCI driver on Zynq SoCs.
 * Call
 * 		UsbEhciZynq.Install ~
 * to install the driver and unload this module ro remove it.
 *
 * This module also patches the behavior of the EHCI driver to the Zynq host controller. The host controller is
 * not fully compliant with the EHCI specifications and has On-The-Go features that require additional steps in
 * the initialization. These differences are accounted for in the EnhancedHostController object provided here.
 *)
использует НИЗКОУР, Platform, BootConfig, ЭВМ, Objects, Kernel, ЛогЯдра, Debug := UsbDebug, Usbdi, Usb, UsbHcdi, UsbEhci, UsbEhciPhy;

конст
	HcUlpiViewport = 30H;
	HcOtgSc = 64H;
	HcUsbMode = 68H;
	TTCtrl * = 1CH;

	(* HcUsbMode bits *)
	ModeHost = {0, 1};
	ModeDevice = {1};
	ModeIdle = {};

	(* HcOtgSc bits *)
	OtgScId = 8;
	OtgScIdPu = 5;

	(* HcPortSc bits *)
	PortScPortSpeed = {26, 27};
	PortScPortSpeedLow = {26};
	PortScPortSpeedFull = {};
	PortScPortSpeedHigh = {27};

тип
	(** Zynq-specific EHCI driver. Uses the main implementation and adds Zynq-specific initialization code where needed. *)
	EnhancedHostController = окласс (UsbEhci.EnhancedHostController)
	перем
		phyResetGpio: цел32;

		проц {перекрыта}GetPortStatus * (port : цел32; ack : булево) : мнвоНаБитахМЗ;
		перем
			status, dword: мнвоНаБитахМЗ;
		нач
			status := GetPortStatus^(port, ack);
			dword := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, НИЗКОУР.прочти32битаПоАдресу(ports[port]));
			если UsbHcdi.PortStatusEnabled * status # {} то
				если dword * PortScPortSpeed = PortScPortSpeedHigh то
					status := status + UsbHcdi.PortStatusHighSpeed
				аесли dword * PortScPortSpeed = PortScPortSpeedLow то
					status := status + UsbHcdi.PortStatusLowSpeed
				аесли dword * PortScPortSpeed = PortScPortSpeedFull то
					status := status + UsbHcdi.PortStatusFullSpeed
				всё
			всё;
			возврат status
		кон GetPortStatus;

		проц {перекрыта}HasCompanion * (): булево;
		нач
			возврат ложь
		кон HasCompanion;

		проц {перекрыта}ResetAndEnablePort (port: цел32): булево;
		перем
			dword: мнвоНаБитахМЗ;
			res: булево;
		нач
			res := ResetAndEnablePort^(port);
			если res то
				dword := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, НИЗКОУР.прочти32битаПоАдресу(iobase + HcUsbMode));
				dword := dword + ModeHost;
				НИЗКОУР.запиши32битаПоАдресу(iobase + HcUsbMode, dword);
			всё;
			возврат res
		кон ResetAndEnablePort;

		(* Reset the host controller. Note: This will NOT assert reset on the USB downstream ports. *)
		проц {перекрыта}HardwareReset() : булево;
		перем
			viewportInit: массив 32 из симв8;
			viewport: цел32;
			dword: мнвоНаБитахМЗ;
			res: булево;
		нач
			(* Set mode to host *)
			НИЗКОУР.прочтиОбъектПоАдресу(iobase + HcUsbMode, dword);
			НИЗКОУР.запишиОбъектПоАдресу(iobase + HcUsbMode, dword + ModeHost);

			res := HardwareReset^();

			если res то
				(* Set mode to host *)
				НИЗКОУР.прочтиОбъектПоАдресу(iobase + HcUsbMode, dword);
				НИЗКОУР.запишиОбъектПоАдресу(iobase + HcUsbMode, dword + ModeHost);
				НИЗКОУР.прочтиОбъектПоАдресу(iobase + HcOtgSc, dword);
				включиВоМнвоНаБитах(dword, 7);
				включиВоМнвоНаБитах(dword, 5);
				НИЗКОУР.запишиОбъектПоАдресу(iobase + HcOtgSc, dword);

				(* Try putting port in full-speed mode *)
				НИЗКОУР.запишиОбъектПоАдресу(iobase + UsbEhci.HcPortSc, {8});
			всё;
			если ~res то
				возврат ложь
			всё;
			ЭВМ.ДайЗначениеКлючаКонфигурацииЯОС('UsbViewportInit', viewportInit);
			если viewportInit = '0' то
				viewport := 0
			иначе
				viewport := iobase + HcUlpiViewport
			всё;
			возврат UsbEhciPhy.Init(viewport, phyResetGpio)
		кон HardwareReset;

		(*
		 * Start the host controller.
		 * This will:
		 * - enable interrupts for the host controller and install a interrupt handler
		 * - set the addresses for the periodic and asynchronous lists
		 * - turn the host controller on
		 * - route all ports to the EHCI controller
		 * - power on all ports of the root hub
	  	 *)
		проц {перекрыта}Start():булево;
		перем dword : мнвоНаБитахМЗ;
			res: булево;
		нач
			res := Start^();
			если ~res то возврат ложь всё;

			(* Clear interrupts *)
			НИЗКОУР.запиши32битаПоАдресу(iobase + UsbEhci.HcUsbSts, 0);

			(* Enable all interrupts except the frame list rollover interrupt *)
			dword := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, НИЗКОУР.прочти32битаПоАдресу(iobase + UsbEhci.HcUsbIntr));
			interruptsEnabled := dword + {0 .. 5} - UsbEhci.StsFrameListRollover + {19} (* UPI: triggered by iTD IOC *);
			НИЗКОУР.запиши32битаПоАдресу(iobase + UsbEhci.HcUsbIntr, interruptsEnabled);

			(* Set the TT HubAddress to 7FH *)
			(*SYSTEM.PUT32(iobase + 1CH, SYSTEM.VAL(SET, SYSTEM.GET32(iobase + (* TTCTRL *)1CH)) + {24 .. 30});*)

			возврат истина
		кон Start;

		проц {перекрыта}Schedule * (transfer: UsbHcdi.TransferToken);
		перем
			address: цел32;
		нач
			если (transfer.pipe.speed = UsbHcdi.LowSpeed) или (transfer.pipe.speed = UsbHcdi.FullSpeed) то
				address := transfer.pipe.device(Usb.UsbDevice).ttAddress;
				если address = 0 то
					address := transfer.pipe.address
				всё;
			всё;
			НИЗКОУР.запиши32битаПоАдресу(iobase + TTCtrl, логСдвиг(address, 24));
			Schedule^(transfer);
		кон Schedule;
	кон EnhancedHostController;

перем
	i: цел32;

	(**
	 * Initializes and install the EHCI host controller driver on the host controller mapped at address iobase
	 * and using interrupt number irq.
	 *)
	проц Init(irq, phyResetGpio: цел32; iobase: адресВПамяти);
	конст
		(* Some part of the EHCI driver is PCI-related. These are dummy values for it. *)
		bus = 0;
		device = 0;
		function = 0;
	перем
		hostController: EnhancedHostController;
	нач
		нов(hostController, bus, device, function);
		hostController.phyResetGpio := phyResetGpio;
		если hostController.Init(iobase, irq) то
			ЛогЯдра.пСтроку8("UsbEhci: Initialised USB Enhanced Host Controller."); ЛогЯдра.пВК_ПС;
			UsbHcdi.RegisterHostController(hostController, UsbEhci.Description);
		иначе
			ЛогЯдра.пСтроку8("UsbEhci: Cannot init USB Enhanced Host Controller."); ЛогЯдра.пВК_ПС;
		всё;
	кон Init;

	проц Install*;
	(* Load module *)
	кон Install;

нач
	если BootConfig.GetBoolValue("UsbEnable0") то
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Initializing USB"); ЛогЯдра.пЦел64(0, 0); ЛогЯдра.пСтроку8(", Address = "); ЛогЯдра.пАдресВПамяти(Platform.UsbBase[0]); ЛогЯдра.пСтроку8(", IRQ = "); ЛогЯдра.пЦел64(Platform.UsbIrq[0], 0); ЛогЯдра.пСтроку8(", PHY Reset = "); ЛогЯдра.пЦел64(BootConfig.GetIntValue("UsbPhyRstGpio0"), 0); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
		Init(Platform.UsbIrq[0], BootConfig.GetIntValue("UsbPhyRstGpio0"), Platform.UsbBase[0])
	всё;
	если BootConfig.GetBoolValue("UsbEnable1") то
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Initializing USB"); ЛогЯдра.пЦел64(1, 0); ЛогЯдра.пСтроку8(", Address = "); ЛогЯдра.пАдресВПамяти(Platform.UsbBase[1]); ЛогЯдра.пСтроку8(", IRQ = "); ЛогЯдра.пЦел64(Platform.UsbIrq[1], 0); ЛогЯдра.пСтроку8(", PHY Reset = "); ЛогЯдра.пЦел64(BootConfig.GetIntValue("UsbPhyRstGpio1"), 0); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
		Init(Platform.UsbIrq[1], BootConfig.GetIntValue("UsbPhyRstGpio1"), Platform.UsbBase[1])
	всё
кон UsbEhciZynq.

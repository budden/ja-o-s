
 модуль GenericLinker;	(* AUTHOR "negelef"; PURPOSE "Generic Object File Linker"; *)

использует ObjectFile, Потоки, Diagnostics, Строки8, НИЗКОУР;

тип Address* = адресВПамяти;

конст
	InvalidAddress* = -1 (* MAX (Address) *);

конст
	(* priorities -- do not coincide with ObjecFile section types *)
	Fixed* = 0; (* placed accroding to placement *)
	EntryCode*= 1; (* must be placed before all other code *)
	InitCode*=2;
	ExitCode*=3; (* must be placed after initcode but before code *)
	BodyCode* = 4;
	Code* = 5;
	Data* = 6;
	Const* = 7;
	Empty* = 8; (* must be placed last *)

	(* refers to priority classes, not to ObjectFile section types *)
	UseAll *= {Fixed .. Empty};
	UseInitCode*={Fixed .. ExitCode};
	UseAllButInitCode*={Fixed, BodyCode..Empty};

тип
	HashEntrySegmentedName = запись
		key: ObjectFile.SegmentedName; (* key[0]= MIN(SIGNED32) <=> empty *)
		value: Block;
	кон;
	HashSegmentedNameArray = укль на массив из HashEntrySegmentedName;

	HashTableSegmentedName* = окласс
	перем
		table: HashSegmentedNameArray;
		size: размерМЗ;
		used-: размерМЗ;
		maxLoadFactor: вещ32;

		(* Interface *)

		проц & Init* (initialSize: размерМЗ);
		нач
			утв(initialSize > 2);
			нов(table, initialSize);
			size := initialSize;
			used := 0;
			maxLoadFactor := 0.75;
			Clear;
		кон Init;

		проц Put*(конст key: ObjectFile.SegmentedName; value: Block);
		перем hash: размерМЗ;
		нач
			утв(used < size);
			hash := HashValue(key);
			если table[hash].key[0] < 0 то
				увел(used, 1);
			всё;
			table[hash].key := key;
			table[hash].value := value;
			если (used / size) > maxLoadFactor то Grow всё;
		кон Put;

		проц Get*(конст key: ObjectFile.SegmentedName):Block;
		перем hashValue : размерМЗ;
		нач
			hashValue := HashValue(key);
			если (table[hashValue].key = key) и (table[hashValue].value # unused) то
				возврат table[hashValue].value;
			иначе
				возврат НУЛЬ
			всё;
		кон Get;

		проц Clear;
		перем i: размерМЗ;
		нач
			нцДля i := 0 до size - 1 делай table[i].key[0] := -1; кц;
		кон Clear;

		(* Internals *)
		проц Hash(конст name: ObjectFile.SegmentedName): размерМЗ;
		перем fp,i: размерМЗ;
		нач
			fp := name[0]; i := 1;
			нцПока (i<длинаМассива(name)) и (name[i] >= 0) делай
				fp:=НИЗКОУР.подмениТипЗначения(размерМЗ, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, вращБит(fp, 7)) / НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, name[i]));
				увел(i);
			кц;
			возврат fp
		кон Hash;

		проц HashValue(конст key: ObjectFile.SegmentedName):размерМЗ;
		перем value, h,i: размерМЗ;
		нач
			утв(key[0] >= 0);
			h := Hash(key);
			i := 0;
			нцДо
				value := (h + i) остОтДеленияНа size;
				увел(i);
			кцПри((table[value].key[0] < 0) или (table[value].key = key) или (i > size));
			утв((table[value].key[0] <0 ) или (table[value].key = key));
			возврат value;
		кон HashValue;

		проц Grow;
		перем oldTable: HashSegmentedNameArray; oldSize, i: размерМЗ; key: ObjectFile.SegmentedName;
		нач
			oldSize := size;
			oldTable := table;
			Init(size*2);
			нцДля i := 0 до oldSize-1 делай
				key := oldTable[i].key;
				если key[0] # матМинимум(цел32) то
					если oldTable[i].value # НУЛЬ то
						Put(key, oldTable[i].value);
					всё;
				всё;
			кц;
		кон Grow;

	кон HashTableSegmentedName;

тип Arrangement* = окласс

	проц Preallocate* (конст section: ObjectFile.Section);
	кон Preallocate;

	проц Allocate* (конст section: ObjectFile.Section): Address;
	кон Allocate;

	проц Patch* (pos, value: Address; offset, bits, unit: ObjectFile.Bits);
	кон Patch;

	проц CheckReloc*(target: Address; pattern: ObjectFile.Pattern; конст patch: ObjectFile.Patch);
	нач
		(* to be able to provide relocation information in an image*)
	кон CheckReloc;

кон Arrangement;

тип Block* = укль на запись (ObjectFile.Section)
	next: Block;
	address*: Address;
	aliasOf*: Block;
	referenced, used: булево;
	priority: целМЗ;
кон;

тип Linker* = окласс
перем
	diagnostics: Diagnostics.Diagnostics;
	usedCategories: мнвоНаБитахМЗ;
	error-: булево;
	log-: Потоки.Писарь;
	code, data: Arrangement;
	firstBlock, firstLinkedBlock: Block;
	linkRoot: ObjectFile.SectionName;
	hash: HashTableSegmentedName;
	offers-, requires-: ObjectFile.NameList;

	проц &InitLinker* (diagnostics: Diagnostics.Diagnostics; log: Потоки.Писарь; useCategories: мнвоНаБитахМЗ; code, data: Arrangement);
	нач
		сам.diagnostics := diagnostics; сам.log := log; сам.usedCategories := useCategories;
		error := ложь; сам.code := code; сам.data := data; firstBlock := НУЛЬ; firstLinkedBlock := НУЛЬ;
		linkRoot := "";
		нов(hash,64);

	кон InitLinker;

	проц SetLinkRoot*(конст root: массив из симв8);
	нач копируйСтрокуДо0(root, linkRoot)
	кон SetLinkRoot;

	проц SetOffers*(конст offers: ObjectFile.NameList);
	нач
		сам.offers := offers;
	кон SetOffers;

	проц SetRequires*(конст requires: ObjectFile.NameList);
	нач
		сам.requires := requires;
	кон SetRequires;


	проц Error* (конст source, message: массив из симв8);
	нач diagnostics.Error (source, Потоки.НевернаяПозицияВПотоке, message); error := истина;
	кон Error;

	проц Warning* (конст source, message: массив из симв8);
	нач diagnostics.Warning (source, Потоки.НевернаяПозицияВПотоке, message);
	кон Warning;

	проц ErrorP*(конст pooledName: ObjectFile.SegmentedName; конст message: массив из симв8);
	перем source: массив ObjectFile.SegmentedNameLen из симв8;
	нач
		ObjectFile.SegmentedNameToString(pooledName, source); Error(source, message);
	кон ErrorP;

	проц Information* (конст source, message: массив из симв8);
	нач если log#НУЛЬ  то log.пСтроку8(source); log.пСтроку8(":"); log.пСтроку8(message); log.пВК_ПС всё;
	кон Information;

	проц InformationP*(конст pooledName: ObjectFile.SegmentedName; конст message: массив из симв8);
	перем source: массив ObjectFile.SegmentedNameLen из симв8;
	нач
		ObjectFile.SegmentedNameToString(pooledName, source); Information(source, message);
	кон InformationP;

	проц FindBlock* (конст identifier: ObjectFile.Identifier): Block;
	нач
		возврат hash.Get(identifier.name);
	кон FindBlock;

	проц ImportBlock*(конст fixup: ObjectFile.Fixup): Block;
	нач
		возврат НУЛЬ
	кон ImportBlock;

	проц ExportBlock*(block: Block);
	нач
		(* can be overwritten by implementers, for example for hashing the block *)
	кон ExportBlock;

	проц GetArrangement (block: Block): Arrangement;
	нач если ObjectFile.IsCode (block.type) то возврат code; иначе возврат data; всё;
	кон GetArrangement;

	(* this procedure may be overwritten by implementations of the linker that need a special ordering, as, for example, the bodycode in the front or so *)
	проц Precedes* (left, right: Block): булево;
	нач возврат left.priority < right.priority
	кон Precedes;

	проц RemoveBlock* (block: Block);
	перем current: Block;
	нач
		если block = firstBlock то
			firstBlock := block.next
		иначе
			current := firstBlock;
			нцПока (current.next # НУЛЬ) и (current.next # block) делай
				current := current.next;
			кц;
			утв(current # НУЛЬ);
			current.next := block.next;
		всё;
		hash.Put(current.identifier.name, unused);
	кон RemoveBlock;

	проц RemovePrefixed*(prefix: ObjectFile.SegmentedName);
	перем block: Block;
	нач
		нцПока (firstBlock # НУЛЬ) и ObjectFile.IsPrefix(prefix, firstBlock.identifier.name) делай
			если hash.Get(firstBlock.identifier.name) # НУЛЬ то hash.Put(firstBlock.identifier.name, unused) всё;
			firstBlock := firstBlock.next;
		кц;
		если firstBlock = НУЛЬ то
			возврат
		всё;
		block := firstBlock;
		(* invariant: block # NIL & block.identifier.name not prefixed by prefix *)
		нцПока (block.next # НУЛЬ) делай
			если ObjectFile.IsPrefix(prefix, block.next.identifier.name) то
				если hash.Get(block.next.identifier.name) # НУЛЬ то hash.Put(block.next.identifier.name, unused) всё;
				block.next := block.next.next;
			иначе
				block := block.next;
			всё;
		кц;
	кон RemovePrefixed;


	проц AddSection* (конст section: ObjectFile.Section);
	перем block, current, previous,newBlock: Block; name: массив ObjectFile.SegmentedNameLen из симв8; i: размерМЗ; alias: ObjectFile.Alias;
	нач
		если FindBlock (section.identifier) # НУЛЬ то ObjectFile.SegmentedNameToString(section.identifier.name,name); Error (name, "duplicated section"); возврат; всё;
		нов (block); ObjectFile.CopySection (section, block^); block.address := InvalidAddress; block.referenced := ложь; block.used := ложь;
		current := firstBlock; previous := НУЛЬ;
		block.priority := GetPriority(block);
		нцПока (current # НУЛЬ) и ~Precedes(block,current) делай previous := current; current := current.next; кц;
		если previous # НУЛЬ то previous.next := block; иначе firstBlock := block; всё; block.next := current;
		current := block;
		(* append all alias blocks after the block *)
		нцДля i := 0 до block.aliases-1 делай
			alias := block.alias[i];
			нов(newBlock);
			newBlock.identifier := alias.identifier;
			newBlock.address := alias.offset;
			newBlock.aliasOf := block;
			newBlock.used := block.used;
			newBlock.next := current.next;
			current.next := newBlock;
			current := newBlock;
		кц;
	кон AddSection;

	проц EnterBlocks*;
	перем current: Block;
	нач
		current := firstBlock;
		нцПока (current # НУЛЬ) делай
			hash.Put(current.identifier.name, current);
			ExportBlock(current);
			current := current.next;
		кц;
	кон EnterBlocks;


	проц Resolve*;
	перем block: Block; used: булево; name: массив ObjectFile.SegmentedNameLen из симв8;
	нач
		если ~error то block := firstBlock;
			нцПока (block # firstLinkedBlock) и ~error делай
				ObjectFile.SegmentedNameToString(block.identifier.name, name);
				used := (block.priority в usedCategories) или (linkRoot # "") и Строки8.НачинаетсяЛиСПриставкиПодстрока¿(linkRoot,0,name) или (block.aliases > 0);
				Reference (block, used); block := block.next;
			кц;
		всё;
	кон Resolve;

	проц PatchAlias*(block: Block);
	нач
		если block.aliasOf # НУЛЬ то увел(block.address, block.aliasOf.address) всё;
	кон PatchAlias;

	проц Link*;
	перем block: Block;
	нач
		(*
		IF ~error THEN block := firstBlock; WHILE block # firstLinkedBlock DO Aliases (block); block := block.next; END; END;
		*)
		Resolve;

		если ~error то block := firstBlock; нцПока block # firstLinkedBlock делай если block.used и (block.aliasOf=НУЛЬ) то Prearrange (block); всё; block := block.next; кц; всё;
		если ~error то block := firstBlock; нцПока block # firstLinkedBlock делай если block.used и (block.aliasOf=НУЛЬ) то Arrange (block); всё; block := block.next; кц; всё;
		если ~error то block := firstBlock; нцПока block # firstLinkedBlock делай PatchAlias (block); block := block.next; кц; всё;

		если ~error то block := firstBlock; нцПока block # firstLinkedBlock делай если block.used и (block.aliasOf = НУЛЬ) то Patch (block); всё; block := block.next; кц; всё;
		если ~error то firstLinkedBlock := firstBlock; всё;
		если ~error и (log # НУЛЬ) то block := firstBlock; нцПока block # НУЛЬ делай Diagnose (block); block := block.next; кц; всё;
	кон Link;

	проц Reference (block: Block; used: булево);
	перем i: размерМЗ;

		проц ReferenceFixup (конст fixup: ObjectFile.Fixup);
		перем reference: Block; str,name: массив ObjectFile.SegmentedNameLen из симв8;
		нач
			reference := FindBlock (fixup.identifier);
			если reference = НУЛЬ то reference := ImportBlock(fixup) всё;

			если reference = НУЛЬ то
				ObjectFile.SegmentedNameToString(fixup.identifier.name,str); Строки8.ПодклейВСтрокуХвост(str," in " );
				ObjectFile.SegmentedNameToString(block.identifier.name,name);
				Строки8.ПодклейВСтрокуХвост(str,  name);
				Error(str, "unresolved");
			аесли (reference.identifier.fingerprint # 0) и (fixup.identifier.fingerprint # 0) и (reference.identifier.fingerprint # fixup.identifier.fingerprint) то
				ObjectFile.SegmentedNameToString(fixup.identifier.name,str); Строки8.ПодклейВСтрокуХвост(str," in " );
				ObjectFile.SegmentedNameToString(block.identifier.name,name);
				Строки8.ПодклейВСтрокуХвост(str,  name);
				Error (str, "incompatible");
			иначе Reference (reference, block.used); всё;
		кон ReferenceFixup;

	нач
		если used и ~block.used то block.used := истина;
		аесли block.referenced то возврат; всё; block.referenced := истина;
		если ~used то возврат всё;
		нцДля i := 0 до block.fixups - 1 делай
			ReferenceFixup (block.fixup[i]);
			если error то возврат всё;
		кц;
	кон Reference;

	проц Prearrange (block: Block);
	перем arrangement: Arrangement;
	нач
		утв (block.used);
		arrangement := GetArrangement (block);
		arrangement.Preallocate (block^);
	кон Prearrange;

	проц Arrange (block: Block);
	перем arrangement: Arrangement;
	нач
		утв (block.used);
		arrangement := GetArrangement (block);
		block.address := arrangement.Allocate (block^);
		если block.address = InvalidAddress то ErrorP (block.identifier.name, "failed to allocate"); возврат; всё;
		если block.fixed то если block.address # block.alignment то ErrorP (block.identifier.name, "address allocation problem"); возврат всё;
		иначе утв ((block.alignment = 0) или (block.address остОтДеленияНа block.alignment = 0)); всё;
	кон Arrange;

	проц Patch (block: Block);
	перем arrangement: Arrangement; i: размерМЗ;

		проц PatchFixup (конст fixup: ObjectFile.Fixup);
		перем reference: Block; target, address: Address; i: размерМЗ;

			проц PatchPattern (конст pattern: ObjectFile.FixupPattern);
			нач arrangement.Patch (target, address, pattern.offset, pattern.bits, block.unit); address := арифмСдвиг (address, -pattern.bits);
			кон PatchPattern;

			проц CheckBits(pattern: ObjectFile.Pattern; offset: ObjectFile.Unit);
			перем i: размерМЗ; nobits, remainder: ObjectFile.Bits; minval, maxval: ObjectFile.Unit; name: ObjectFile.SectionName; number: массив 32 из симв8;
			нач
				nobits := 0;
				нцДля i := 0 до pattern.patterns-1 делай
					увел(nobits,pattern.pattern[i].bits);
				кц;
				remainder := ObjectFile.Bits(арифмСдвиг(address,-nobits));

				если  (nobits <32) и ((remainder > 0) или (remainder < -1)) то
					если pattern.mode = ObjectFile.Relative то (* negative values allowed *)
						maxval := арифмСдвиг(1,nobits-1)-1; minval := -maxval-1
					иначе
						minval := 0; maxval := арифмСдвиг(1,nobits);
					всё;
					ObjectFile.SegmentedNameToString(block.identifier.name,name);
					Строки8.ПодклейВСтрокуХвост(name,":");
					Строки8.ПишиЦел64_вСтроку(offset,number);
					Строки8.ПодклейВСтрокуХвост(name,number);
					Error(name,"fixup out of range");
				всё;
			кон CheckBits;

			проц ApplyPatch(pattern: ObjectFile.Pattern; конст patch: ObjectFile.Patch);
			перем j: размерМЗ;
			нач
				target := block.address + patch.offset;
				address := reference.address + patch.displacement;
				если pattern.mode = ObjectFile.Relative то
					умень(address,target)
				всё;
				address := арифмСдвиг (address, pattern.scale);
				CheckBits(pattern, patch.offset);
				нцДля j := 0 до pattern.patterns-1 делай PatchPattern(pattern.pattern[j]) кц;
			кон ApplyPatch;

		нач
			reference := FindBlock (fixup.identifier);
			если reference = НУЛЬ то reference := ImportBlock(fixup) всё;
			утв (reference # НУЛЬ);
			нцДля i := 0 до fixup.patches-1 делай
				ApplyPatch(fixup.pattern, fixup.patch[i]);
				arrangement.CheckReloc(block.address, fixup.pattern, fixup.patch[i])
			кц;
		кон PatchFixup;

	нач
		утв (block.used);
		arrangement := GetArrangement (block);
		нцДля i := 0 до block.fixups - 1 делай
			PatchFixup (block.fixup[i])
		кц;
	кон Patch;

	проц Diagnose (block: Block);
	перем source, num,name: массив ObjectFile.SegmentedNameLen из симв8; msg: массив 512 из симв8;
	нач
		если block.used то
			Строки8.ПишиЦел64_16_ричноВСтроку(block.address, 8, num);
			source := "";
			Строки8.ПодклейВСтрокуХвост(source,"0");
			Строки8.ПодклейВСтрокуХвост(source, num);
			Строки8.ПодклейВСтрокуХвост(source,"H");

			msg := "";

			ObjectFile.SegmentedNameToString(block.identifier.name, name);

			если ObjectFile.IsCode(block.type) то msg := " code "
			иначе msg := " data "
			всё;

			Строки8.ПодклейВСтрокуХвост(msg, name);

			если block.bits # НУЛЬ то
				Строки8.ПодклейВСтрокуХвост(msg, " to ");
				Строки8.ПишиЦел64_16_ричноВСтроку(block.address+block.bits.GetSize() DIV block.unit-1, 8, num);
				Строки8.ПодклейВСтрокуХвост(msg,"0");
				Строки8.ПодклейВСтрокуХвост(msg, num);
				Строки8.ПодклейВСтрокуХвост(msg,"H");
				(*Strings.IntToStr(block.address+block.bits.GetSize() DIV block.unit-1, num);
				Strings.Append(msg,num);
				*)
			всё;
			(*
			Strings.IntToStr(block.address, num);
			Strings.Append(msg," ("); Strings.Append(msg,num); Strings.Append(msg,")");
			*)
			Information (source, msg);
		иначе InformationP (block.identifier.name, "unused"); всё;
	кон Diagnose;

кон Linker;

перем unused: Block;

проц GetPriority(block: Block): целМЗ;
нач
	если block.fixed то возврат Fixed всё;
	если block.type = ObjectFile.InitCode то возврат InitCode всё;
	если block.type = ObjectFile.EntryCode то возврат EntryCode всё;
	если block.type = ObjectFile.ExitCode то возврат ExitCode всё;
	если (block.bits = НУЛЬ) или (block.bits.GetSize () = 0) то возврат Empty всё;
	если block.type = ObjectFile.BodyCode то возврат Code всё;
	если block.type = ObjectFile.Code то возврат Code всё;
	если block.type = ObjectFile.Data то возврат Code всё;
	если block.type = ObjectFile.Const то возврат Code всё;
	СТОП(100); (* undefined type *)
кон GetPriority;

проц ReadHeader(reader: Потоки.Чтец; linker: Linker; перем binary: булево; перем poolMap: ObjectFile.PoolMap; перем offers, requires: ObjectFile.NameList);
перем ch: симв8; version: целМЗ; string: массив 32 из симв8;
нач
	reader.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(string);
	binary := string="FoxOFB";
	если ~binary то утв(string="FoxOFT") всё;
	reader.ПропустиБелоеПоле;
	reader.чСимв8(ch); утв(ch='v');
	reader.чЦел32(version,ложь);
	если (version # ObjectFile.Version) и (linker # НУЛЬ) то linker.Error("","invalid object file version encountered. Recompile sources.") всё;
	reader.чСимв8(ch); утв(ch='.');
	если ~binary то reader.ПропустиБелоеПоле
	иначе
		нов(poolMap,64);
		poolMap.Read(reader);
	всё;

	offers := НУЛЬ;
	requires := НУЛЬ;
	если ~binary то
		reader.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(string);
		утв(string = "offers");
		ObjectFile.ReadNameList(reader, offers, binary, poolMap);
		reader.ПропустиБелоеПоле;
		reader.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(string);
		утв(string = "requires");
		ObjectFile.ReadNameList(reader, requires, binary, poolMap);
		reader.ПропустиБелоеПоле;
	иначе
		ObjectFile.ReadNameList(reader, offers, binary, poolMap);
		ObjectFile.ReadNameList(reader, requires, binary, poolMap);
	всё
кон ReadHeader;

проц OffersRequires*(reader: Потоки.Чтец; перем offers, requires: ObjectFile.NameList);
перем binary: булево; poolMap: ObjectFile.PoolMap;
нач
	ReadHeader(reader, НУЛЬ, binary, poolMap, offers, requires);
кон OffersRequires;

проц Load* (reader: Потоки.Чтец; linker: Linker);
перем section: ObjectFile.Section; binary: булево; poolMap: ObjectFile.PoolMap; offers, requires: ObjectFile.NameList;
нач
	ReadHeader(reader, linker, binary, poolMap, offers, requires);
	linker.SetOffers(offers);
	linker.SetRequires(requires);
	нцПока reader.ПодглядиСимв8 () # 0X делай
		ObjectFile.ReadSection (reader, section,binary,poolMap);
		reader.ПропустиБелоеПоле;
		если reader.кодВозвратаПоследнейОперации = Потоки.Успех то linker.AddSection (section); всё;
	кц;
кон Load;

нач
	нов(unused);
кон GenericLinker.

Compiler.Compile GenericLinker.Mod   ~~~

модуль CryptoCSPRNG;	(** AUTHOR "GF"; PURPOSE "Cryptographically Secure Pseudo-Random Generator."*)


использует  ЭВМ, Clock, Кучи, SHA3 := CryptoSHA3(*, Out := KernelLog*);


	проц CSRand*( перем rand: массив из симв8; bits: размерМЗ );
	перем
		seed: цел64; i, j, len: размерМЗ;
		h: SHA3.Hash;
		buf: массив 64 из симв8;
	нач
		утв( (bits остОтДеленияНа 8 = 0) и (bits DIV 8 <= длинаМассива( rand )) );
		seed := Noise();
		нцДля i := 0 до 7 делай
			buf[i] := симв8ИзКода( seed остОтДеленияНа 100H );  seed := seed DIV 100H
		кц;
		нов( h );  h.SetNameAndSize( "", 64 );
		h.Update( buf, 0, 8 );
		h.GetHash( buf, 0 );
		i := 0;  j := 0;  len := bits DIV 8;
		нцПока j < len делай
			если i = 64 то
				h.Initialize;
				h.Update( buf, 0, 64 );
				h.GetHash( buf, 0 );
				i := 0
			всё;
			rand[j] := buf[i];  увел( j ); увел( i )
		кц
	кон CSRand;

	проц Noise( ): цел64;
	перем tm, dt: цел64; t, d: цел32;  total, free, largest: размерМЗ;
	нач
		tm := ЭВМ.ДайКвоТактовПроцессораСМоментаПерезапуска( );
		Clock.Get( t, d );
		dt := устарПреобразуйКБолееШирокомуЦел( d ) * 1000000H + t;
		Кучи.ПосчитайОбъёмыКучи( total, free, largest );
		возврат  (tm + 4*dt + Кучи.Nmark) * (Кучи.квоЗапусковСборщикаМусораСоСтартаСистемы + 1) + (total - free + largest )
	кон Noise;

	(*
	PROCEDURE Test*;
	CONST HT = 09X;
	VAR rand: ARRAY 512 OF CHAR; i: SIZE;
	BEGIN
		CSRand( rand, 2048 );
		Out.Ln;
		FOR i := 0 TO 255 DO
			Out.Hex( ORD( rand[i] ), -2 );
			IF (i+1) MOD 4 = 0 THEN  Out.Char( HT )  END;
			IF (i+1) MOD 32 = 0 THEN  Out.Ln  END
		END;
		Out.Ln
	END Test;
	*)


кон CryptoCSPRNG.


	CryptoCSPRNG.Test ~

	System.Free CryptoCSPRNG ~

(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Caches; (** AUTHOR "pjm"; PURPOSE "Generic disk cache"; *)

использует ЛогЯдра, Objects, Disks;

(** Caching. *)

конст
	LockedBit = 0;  DirtyBit = 1;	(* Buffer state flags *)

	CacheUpdateTime = 5*1000;	(* in ms *)

	Trace = истина;

тип
	Buffer* = окласс	(** all fields read-only *)
		перем
			data*: укль на массив из симв8;
			dev*: Disks.Device;
			block*: цел32;
			state: мнвоНаБитахМЗ;
			nextHash, prevHash, nextLRU, prevLRU, nextDirty: Buffer;

		проц &Init*(size: цел32);
		нач
			нов(data, size)
		кон Init;

	кон Buffer;

	Cache* = окласс	(** all fields read-only *)
		перем
			blockSize*: цел32;
			hashTable: укль на массив из Buffer;
			lru: Buffer;	(* LRU list of released buffers (only dirty buffers may be locked) *)
			lruClean: цел32;	(* number of non-dirty buffers in lru *)
			syncNow: булево;
			timer: Objects.Timer;

		(* exports: Acquire, Release, Synchronize *)

		(** Acquire a buffer for the specified device block.  If it is in the cache, its buffer is locked and
			returned with valid = TRUE, otherwise an unlocked non-dirty buffer is waited for, locked and returned
			with valid = FALSE. *)

		проц Acquire*(dev: Disks.Device;  block: цел32;  перем buffer: Buffer;  перем valid: булево);
		перем done: булево;  buf: Buffer;  n, m: цел32;
		нач {единолично}
			утв(dev # НУЛЬ);	(* NIL device is used for initialization *)
			нцДо
				n := Hash(dev, block);  buf := hashTable[n];
				нцПока (buf # НУЛЬ) и ((buf.block # block) или (buf.dev # dev)) делай
					buf := buf.nextHash
				кц;
				если buf # НУЛЬ то
					дождись(~(LockedBit в buf.state));
						(* buf could have been re-used *)
					done := (buf.dev = dev) и (buf.block = block);
					valid := истина
				иначе
					дождись(lruClean # 0);
					buf := lru.nextLRU;	(* find candidate and re-use *)
					нцПока DirtyBit в buf.state делай syncNow := истина;  buf := buf.nextLRU кц;
					утв(buf # lru);	(* never re-use sentinel *)
					m := Hash(buf.dev, buf.block);
					если m # n то MoveBuffer(buf, m, n) всё;
					buf.dev := dev;  buf.block := block;
					done := истина;  valid := ложь
				всё
			кцПри done;
			buf.prevLRU.nextLRU := buf.nextLRU;  buf.nextLRU.prevLRU := buf.prevLRU;	(* remove from lru *)
			если ~(DirtyBit в buf.state) то умень(lruClean) всё;
			включиВоМнвоНаБитах(buf.state, LockedBit);
			buffer := buf
		кон Acquire;

		(** Release a buffer with valid data for use by another. *)

		проц Release*(buffer: Buffer;  modified, written: булево);
		нач {единолично}
			исключиИзМнваНаБитах(buffer.state, LockedBit);
			если written то исключиИзМнваНаБитах(buffer.state, DirtyBit);  увел(lruClean)
			аесли modified то включиВоМнвоНаБитах(buffer.state, DirtyBit)
			аесли ~(DirtyBit в buffer.state) то увел(lruClean)
			иначе (* skip *)
			всё;
				(* Put(lru, buffer), and the buffer remains in the same hash list *)
			buffer.prevLRU := lru.prevLRU;  buffer.nextLRU := lru;
			buffer.prevLRU.nextLRU := buffer;  buffer.nextLRU.prevLRU := buffer
		кон Release;

		(** Synchronize all momentarily dirty buffers that are not locked. *)

		проц Synchronize*;
		перем list, buf: Buffer; res: целМЗ; num, count: цел32;
		нач
			AcquireDirty(list); count := 0;
			нцПока list # НУЛЬ делай
				buf := list;  list := buf.nextDirty;  buf.nextDirty := НУЛЬ;
				утв(blockSize остОтДеленияНа buf.dev.blockSize = 0);
				num := blockSize DIV buf.dev.blockSize;
(*
				KernelLog.Enter; KernelLog.String("Synchronize ");  KernelLog.String(buf.dev.name);  KernelLog.Char(" ");
				KernelLog.Int(buf.block, 1);  KernelLog.Char(" ");  KernelLog.Int(num, 1);  KernelLog.Exit;
*)
				buf.dev.Transfer(Disks.Write, buf.block, num, buf.data^, 0, res);
				если res # Disks.Ok то ReportError(buf.dev, buf.block, num, res) всё;
				ReleaseDirty(buf); увел(count)
			кц;
			если Trace и (count # 0) то
				ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Caches: "); ЛогЯдра.пСтроку8(buf.dev.name);
				ЛогЯдра.пСимв8(" "); ЛогЯдра.пЦел64(count, 1); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
			всё
		кон Synchronize;

		(* Auxiliary procedures *)

		(* Acquire a list of unlocked dirty buffers and lock them for synchronization. *)

		проц AcquireDirty(перем list: Buffer);
		перем buf, tail: Buffer;
		нач {единолично}
			list := НУЛЬ;  tail := НУЛЬ;  buf := lru.nextLRU;
			нцПока buf # lru делай
				если buf.state * {LockedBit, DirtyBit} = {DirtyBit} то
					если list = НУЛЬ то list := buf иначе tail.nextDirty := buf всё;
					tail := buf;  buf.nextDirty := НУЛЬ;
					включиВоМнвоНаБитах(buf.state, LockedBit)
					(* to preserve ordering, buf is not removed from lru *)
				всё;
				buf := buf.nextLRU
			кц
		кон AcquireDirty;

		(* Release a dirty buffer on the lru list after synchronization. *)

		проц ReleaseDirty(buffer: Buffer);
		нач {единолично}
			утв(buffer.state * {LockedBit, DirtyBit} = {LockedBit, DirtyBit});
			buffer.state := buffer.state - {LockedBit, DirtyBit};
			увел(lruClean)
		кон ReleaseDirty;

		(* Wait until a periodic synchronize is due. *)

		проц AwaitSync;
		нач {единолично}
			дождись(syncNow);  syncNow := ложь
		кон AwaitSync;

		(* Hash function. *)

		проц Hash(dev: Disks.Device;  block: цел32): цел32;
		нач
			возврат block остОтДеленияНа длинаМассива(hashTable)(цел32)	(* good candidate for inlining *)
		кон Hash;

		(* Move buffer from one hash list to another. *)

		проц MoveBuffer(buf: Buffer;  from, to: цел32);
		нач
				(* remove *)
			если buf.prevHash # НУЛЬ то
				buf.prevHash.nextHash := buf.nextHash
			иначе
				hashTable[from] := buf.nextHash
			всё;
			если buf.nextHash # НУЛЬ то buf.nextHash.prevHash := buf.prevHash всё;
				(* add in front *)
			buf.prevHash := НУЛЬ;  buf.nextHash := hashTable[to];  hashTable[to] := buf;
			если buf.nextHash # НУЛЬ то buf.nextHash.prevHash := buf всё
		кон MoveBuffer;

		проц HandleTimeout;
		нач {единолично}
			syncNow := истина;
			Objects.SetTimeout(timer, сам.HandleTimeout, CacheUpdateTime)
		кон HandleTimeout;

		(* Initialize the cache with specified size and hash size. *)

		проц &Init*(blockSize, hashSize, cacheSize: цел32);
		перем buf: Buffer;  i, n: цел32;
		нач
			утв(hashSize <= cacheSize);
			нов(hashTable, hashSize);
			нов(lru, 0);  lru.dev := НУЛЬ;  lru.block := -1;	(* sentinel *)
			lru.nextLRU := lru;  lru.prevLRU := lru;
			lruClean := cacheSize;  syncNow := ложь;
			сам.blockSize := blockSize;
			нцДля i := 0 до cacheSize-1 делай
				нов(buf, blockSize);
				buf.dev := НУЛЬ;  buf.block := i;
				buf.state := {};  buf.nextDirty := НУЛЬ;
					(* add to hash table *)
				n := Hash(buf.dev, buf.block);	(* spread buffers of NIL device across hash table *)
				buf.prevHash := НУЛЬ;  buf.nextHash := hashTable[n];  hashTable[n] := buf;
				если buf.nextHash # НУЛЬ то buf.nextHash.prevHash := buf всё;
					(* Put(lru, buffer) *)
				buf.prevLRU := lru.prevLRU;  buf.nextLRU := lru;
				buf.prevLRU.nextLRU := buf;  buf.nextLRU.prevLRU := buf
			кц;
			нов(timer); Objects.SetTimeout(timer, сам.HandleTimeout, CacheUpdateTime)
		кон Init;

	нач {активное, SAFE}	(* cache periodically synchronizes automatically *)
		нц AwaitSync;  Synchronize кц
	кон Cache;

(* Report an error during asynchronous disk access. *)

проц ReportError(dev: Disks.Device; block, num: цел32; res: целМЗ);
нач
	ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Caches: Error "); ЛогЯдра.пЦел64(res, 1);
	ЛогЯдра.пСтроку8(" on disk "); ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пЦел64(num, 1);
	ЛогЯдра.пСтроку8(" blocks at "); ЛогЯдра.пЦел64(block, 1); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
кон ReportError;

кон Caches.

модуль WPM; (** AUTHOR "TF"; PURPOSE "WebPageMaker"; *)

использует
	Commands, Texts, TextUtils := TextUtilities, UTF8Strings, ЛогЯдра, Dates, Строки8;

тип Text = Texts.Text;

конст
	contentBegin = "<!-- start -->";
	contentEnd = "<!-- stop -->";
	titleBegin = "<title>";
	titleEnd = "</title>";

	titlePlace = "%title%";
	contentPlace = "%content%";
	changedatePlace = "%changedate%";

проц ReplaceString(text : Text; search : массив из симв8; replace : Text; sfrom, slen : размерМЗ);
перем pos, len : размерМЗ;
	xs : массив 128 из симв32;
нач
	text.AcquireWrite;
	pos := 0; UTF8Strings.UTF8toUnicode(search, xs, pos);
	len := TextUtils.UCS32StrLength(xs);
	нцДо
		pos := TextUtils.Pos(xs, 0, text);
		если pos >= 0 то text.Delete(pos, len); text.CopyFromText(replace, sfrom, slen, pos) всё
	кцПри pos < 0;
	text.ReleaseWrite
кон ReplaceString;

проц UTFUCS(src : массив из симв8; перем dst : массив из симв32);
перем pos : размерМЗ;
нач
	pos := 0; UTF8Strings.UTF8toUnicode(src, dst, pos)
кон UTFUCS;

проц MergeWithTemplate(template, src, date : Text; перем dst : Text);
перем tb, te, cb, ce: размерМЗ; str : массив 32 из симв32;
нач
	нов(dst);
	template.AcquireRead; src.AcquireRead; dst.AcquireWrite;

	(* copy template to dst *)
	dst.CopyFromText(template, 0, template.GetLength(), 0);

	(* find title *)
	UTFUCS(titleBegin, str); tb := TextUtils.Pos(str, 0, src); если tb > 0 то увел(tb, TextUtils.UCS32StrLength(str)) всё;
	UTFUCS(titleEnd, str); te := TextUtils.Pos(str, 0, src);
	если (tb >= 0) и (tb < te) то ReplaceString(dst, titlePlace, src, tb, te - tb) всё;

	(* find content *)
	UTFUCS(contentBegin, str); cb := TextUtils.Pos(str, 0, src); если cb > 0 то увел(cb, TextUtils.UCS32StrLength(str)) всё;
	UTFUCS(contentEnd, str); ce := TextUtils.Pos(str, 0, src);
	если (cb >= 0) и (cb < ce) то ReplaceString(dst, contentPlace, src, cb, ce - cb) всё;

	если date # НУЛЬ то date.AcquireRead; ReplaceString(dst, changedatePlace, date, 0, date.GetLength()); date.ReleaseRead всё;
	template.ReleaseRead; src.ReleaseRead; dst.ReleaseWrite;
кон MergeWithTemplate;

(** template srcdir dstdir name {name} *)
проц Replace*(context : Commands.Context);
перем srcdir, dstdir, src, dst, name, template, tdate : массив 128 из симв8;
	templateT, srcT, dstT, dateT : Text;
	res : целМЗ;
нач
	context.arg.чЦепочкуСимв8ДоБелогоПоля(template); context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимв8ДоБелогоПоля(srcdir); context.arg.ПропустиБелоеПоле;
	context.arg.чЦепочкуСимв8ДоБелогоПоля(dstdir); context.arg.ПропустиБелоеПоле;
	нов(templateT);
	TextUtils.LoadUTF8(templateT, template, res); утв(res = 0);
	Строки8.ПишиДатуВСтроку(Dates.Now(), tdate); нов(dateT); TextUtils.StrToText(dateT, 0, tdate);
	context.arg.чЦепочкуСимв8ДоБелогоПоля(name); context.arg.ПропустиБелоеПоле;
	нцПока context.arg.кодВозвратаПоследнейОперации = 0 делай
		ЛогЯдра.пСтроку8("Processing "); ЛогЯдра.пСтроку8(name);
		нов(srcT); нов(dstT);

		копируйСтрокуДо0(srcdir, src); Строки8.ПодклейВСтрокуХвост(src, name);
		копируйСтрокуДо0(dstdir, dst); Строки8.ПодклейВСтрокуХвост(dst, name);
		TextUtils.LoadUTF8(srcT, src, res);
		если res = 0 то
			MergeWithTemplate(templateT, srcT, dateT, dstT);
			TextUtils.ExportUTF8(dstT, dst, res); утв(res = 0);
			ЛогЯдра.пСтроку8(" --> "); ЛогЯдра.пСтроку8(dst); ЛогЯдра.пСтроку8(" done."); ЛогЯдра.пВК_ПС;
		иначе
			ЛогЯдра.пСтроку8('"'); ЛогЯдра.пСтроку8(src); ЛогЯдра.пСтроку8('"'); ЛогЯдра.пСтроку8(" not found."); ЛогЯдра.пВК_ПС
		всё;
		context.arg.чЦепочкуСимв8ДоБелогоПоля(name); context.arg.ПропустиБелоеПоле
	кц;
кон Replace;

кон WPM.


System.Free WPM TextUtilities Texts ~

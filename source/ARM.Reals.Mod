(* ETH Oberon, Copyright 2001 ETH Zuerich Institut fuer Computersysteme, ETH Zentrum, CH-8092 Zuerich.
Refer to the "General ETH Oberon System Source License" contract available at: http://www.oberon.ethz.ch/ *)

модуль Reals;	(** portable, except where noted *)
(** AUTHOR "bmoesli"; PURPOSE "Real number manipulation"; *)

(** Implementation of the non-portable components of IEEE FLOAT32 and
FLOAT64 manipulation. The routines here are required to do conversion
of reals to strings and back.
Implemented by Bernd Moesli, Seminar for Applied Mathematics,
Swiss Federal Institute of Technology Zürich.
*)

(** Simple port to ARM, without platform-dependent code by Timothée Martiel *)

использует НИЗКОУР, ЭВМ;

(* Bernd Moesli
	Seminar for Applied Mathematics
	Swiss Federal Institute of Technology Zurich
	Copyright 1993

	Support module for IEEE floating-point numbers

	Please change constant definitions of H, L depending on byte ordering
	Use bm.TestReals.Do for testing the implementation.

	Expo, ExpoL return the shifted binary exponent (0 <= e < 256 (2048 resp.))
	SetExpo, SetExpoL set the shifted binary exponent
	Real, RealL convert hexadecimals to reals
	Int, IntL convert reals to hexadecimals
	Ten returns 10^e (e <= 308, 308 < e delivers NaN)

	1993.4.22	IEEE format only, 32-bits LONGINTs only
	30.8.1993	mh: changed RealX to avoid compiler warnings;
	7.11.1995	jt: dynamic endianess test
	22.01.97	pjm: NaN stuff (using quiet NaNs only to avoid traps)
	05.01.98	prk: NaN with INF support
*)

перем
	DefaultFCR*: мнвоНаБитахМЗ;
	tene: массив 23 из вещ64; (* e = 0..22: exact values of 10^e *)
	ten: массив 27 из вещ64;
	eq, gr: массив 20 из мнвоНаБитахМЗ;
	H, L: цел16;

(** Returns the shifted binary exponent (0 <= e < 256). *)
проц Expo* (x: вещ32): цел32;
нач
	возврат арифмСдвиг(НИЗКОУР.подмениТипЗначения(цел32, x), -23) остОтДеленияНа 256
кон Expo;

(** Returns the shifted binary exponent (0 <= e < 2048). *)
проц ExpoL* (x: вещ64): цел32;
	перем i: цел32;
нач
	НИЗКОУР.прочтиОбъектПоАдресу(адресОт(x) + H, i); возврат арифмСдвиг(i, -20) остОтДеленияНа 2048
кон ExpoL;

(** Returns the mantissa *)
проц Mantissa* (x: вещ32): цел32;
нач
	возврат НИЗКОУР.подмениТипЗначения(цел32, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, x) * {0 .. 22})
кон Mantissa;

(** Sets the shifted binary exponent. *)
проц SetExpo* (e: цел32; перем x: вещ32);
	перем i: цел32;
нач
	НИЗКОУР.прочтиОбъектПоАдресу(адресОт(x), i);
	i:= арифмСдвиг(арифмСдвиг(арифмСдвиг(i, -31), 8) + e остОтДеленияНа 256, 23) + i остОтДеленияНа арифмСдвиг(1, 23);
	НИЗКОУР.запишиОбъектПоАдресу(адресОт(x), i)
кон SetExpo;

(** Sets the shifted binary exponent. *)
проц SetExpoL* (e: цел32; перем x: вещ64);
	перем i: цел32;
нач
	НИЗКОУР.прочтиОбъектПоАдресу(адресОт(x) + H, i);
	i:= арифмСдвиг(арифмСдвиг(арифмСдвиг(i, -31), 11) + e остОтДеленияНа 2048, 20) + i остОтДеленияНа арифмСдвиг(1, 20);
	НИЗКОУР.запишиОбъектПоАдресу(адресОт(x) + H, i)
кон SetExpoL;

(** Convert hexadecimal to FLOAT32. *)
проц Real* (h: цел32): вещ32;
	перем x: вещ32;
нач НИЗКОУР.запишиОбъектПоАдресу(адресОт(x), h); возврат x
кон Real;

(** Convert hexadecimal to FLOAT64. h and l are the high and low parts.*)
проц RealL* (h, l: цел32): вещ64;
	перем x: вещ64;
нач НИЗКОУР.запишиОбъектПоАдресу(адресОт(x) + H, h); НИЗКОУР.запишиОбъектПоАдресу(адресОт(x) + L, l); возврат x
кон RealL;

(** Convert FLOAT32 to hexadecimal. *)
проц Int* (x: вещ32): цел32;
	перем i: цел32;
нач НИЗКОУР.запишиОбъектПоАдресу(адресОт(i), x); возврат i
кон Int;

(** Convert FLOAT64 to hexadecimal. h and l are the high and low parts. *)
проц IntL* (x: вещ64; перем h, l: цел32);
нач НИЗКОУР.прочтиОбъектПоАдресу(адресОт(x) + H, h); НИЗКОУР.прочтиОбъектПоАдресу(адресОт(x) + L, l)
кон IntL;

(** Returns 10^e (e <= 308, 308 < e delivers IEEE-code +INF). *)
проц Ten* (e: цел32): вещ64;
	перем E: цел32; r: вещ64;
нач
	если e < -307 то возврат 0 аесли 308 < e то возврат RealL(2146435072, 0) всё;
	увел(e, 307); r:= ten[e DIV 23] * tene[e остОтДеленияНа 23];
	если e остОтДеленияНа 32 в eq[e DIV 32] то возврат r
	иначе
		E:= ExpoL(r); SetExpoL(1023+52, r);
		если e остОтДеленияНа 32 в gr[e DIV 32] то r:= r-1 иначе r:= r+1 всё;
		SetExpoL(E, r); возврат r
	всё
кон Ten;

(** Returns the NaN code (0 <= c < 8399608) or -1 if not NaN/Infinite. *)
проц NaNCode* (x: вещ32): цел32;
нач
	если арифмСдвиг(НИЗКОУР.подмениТипЗначения(цел32, x), -23) остОтДеленияНа 256 = 255 то	(* Infinite or NaN *)
		возврат НИЗКОУР.подмениТипЗначения(цел32, x) остОтДеленияНа 800000H	(* lowest 23 bits *)
	иначе
		возврат -1
	всё
кон NaNCode;

(** Returns the NaN code (0 <= h < 1048576, MIN(SIGNED32) <= l <= MAX(SIGNED32)) or (-1,-1) if not NaN/Infinite. *)
проц NaNCodeL* (x: вещ64;  перем h, l: цел32);
нач
	НИЗКОУР.прочтиОбъектПоАдресу(адресОт(x) + H, h); НИЗКОУР.прочтиОбъектПоАдресу(адресОт(x) + L, l);
	если арифмСдвиг(h, -20) остОтДеленияНа 2048 = 2047 то	(* Infinite or NaN *)
		h := h остОтДеленияНа 100000H	(* lowest 20 bits *)
	иначе
		h := -1;  l := -1
	всё
кон NaNCodeL;

(** Returns TRUE iff x is NaN/Infinite. *)
проц IsNaN* (x: вещ32): булево;
нач
	возврат арифмСдвиг(НИЗКОУР.подмениТипЗначения(цел32, x), -23) остОтДеленияНа 256 = 255
кон IsNaN;

(** Returns TRUE iff x is NaN/Infinite. *)
проц IsNaNL* (x: вещ64): булево;
перем h: цел32;
нач
	НИЗКОУР.прочтиОбъектПоАдресу(адресОт(x) + H, h);
	возврат арифмСдвиг(h, -20) остОтДеленияНа 2048 = 2047
кон IsNaNL;

(** Returns NaN with specified code (0 <= l < 8399608). *)
проц NaN* (l: цел32): вещ32;
перем x: вещ32;
нач
	НИЗКОУР.запишиОбъектПоАдресу(адресОт(x), (l остОтДеленияНа 800000H) + 7F800000H);
	возврат x
кон NaN;

(** Returns NaN with specified code (0 <= h < 1048576, MIN(SIGNED32) <= l <= MAX(SIGNED32)). *)
проц NaNL* (h, l: цел32): вещ64;
перем x: вещ64;
нач
	h := (h остОтДеленияНа 100000H) + 7FF00000H;
	НИЗКОУР.запишиОбъектПоАдресу(адресОт(x) + H, h);
	НИЗКОУР.запишиОбъектПоАдресу(адресОт(x) + L, l);
	возврат x
кон NaNL;

(*
PROCEDURE fcr(): SET;
CODE {SYSTEM.i386, SYSTEM.FPU}
	PUSH 0
	FSTCW [ESP]
	FWAIT
	POP EAX
END fcr;
*)

(** Return state of the floating-point control register. *)
проц FCR*(): мнвоНаБитахМЗ;
кон FCR;

(** Set state of floating-point control register.  Traps reset this to the default.  Note that changing the rounding mode affects rounding of imprecise results as well as the ENTIER operation. *)

проц SetFCR*(s: мнвоНаБитахМЗ);
кон SetFCR;

(** Round x to an integer using the current rounding mode. *)

проц Round*(x: вещ32): цел32;	(** non-portable *)
кон Round;

(** Round x to an integer using the current rounding mode. *)

проц RoundL*(x: вещ64): цел32;	(** non-portable *)
кон RoundL;

проц RealX (hh, hl: цел64; adr: адресВПамяти);
перем h,l: цел32;
нач
	h := устарПреобразуйКБолееУзкомуЦел(hh); l := устарПреобразуйКБолееУзкомуЦел(hl);
	НИЗКОУР.запишиОбъектПоАдресу(adr + H, h); НИЗКОУР.запишиОбъектПоАдресу(adr + L, l);
кон RealX;

проц InitHL;
	перем i: адресВПамяти; dmy: цел16; littleEndian: булево;
нач
	DefaultFCR := ЭВМ.значПоУмолчРегистраУправленияОперациямиСПлавающейТочкой;

	dmy := 1; i := адресОт(dmy);
	НИЗКОУР.прочтиОбъектПоАдресу(i, littleEndian);	(* indirection via i avoids warning on SUN cc -O *)
	если littleEndian то H := 4; L := 0 иначе H := 0; L := 4 всё
кон InitHL;

нач InitHL;
	RealX(03FF00000H, 0, адресОт(tene[0]));
	RealX(040240000H, 0, адресОт(tene[1])); (* 1 *)
	RealX(040590000H, 0, адресОт(tene[2])); (* 2 *)
	RealX(0408F4000H, 0, адресОт(tene[3])); (* 3 *)
	RealX(040C38800H, 0, адресОт(tene[4])); (* 4 *)
	RealX(040F86A00H, 0, адресОт(tene[5])); (* 5 *)
	RealX(0412E8480H, 0, адресОт(tene[6])); (* 6 *)
	RealX(0416312D0H, 0, адресОт(tene[7])); (* 7 *)
	RealX(04197D784H, 0, адресОт(tene[8])); (* 8 *)
	RealX(041CDCD65H, 0, адресОт(tene[9])); (* 9 *)
	RealX(04202A05FH, 020000000H, адресОт(tene[10])); (* 10 *)
	RealX(042374876H, 0E8000000H, адресОт(tene[11])); (* 11 *)
	RealX(0426D1A94H, 0A2000000H, адресОт(tene[12])); (* 12 *)
	RealX(042A2309CH, 0E5400000H, адресОт(tene[13])); (* 13 *)
	RealX(042D6BCC4H, 01E900000H, адресОт(tene[14])); (* 14 *)
	RealX(0430C6BF5H, 026340000H, адресОт(tene[15])); (* 15 *)
	RealX(04341C379H, 037E08000H, адресОт(tene[16])); (* 16 *)
	RealX(043763457H, 085D8A000H, адресОт(tene[17])); (* 17 *)
	RealX(043ABC16DH, 0674EC800H, адресОт(tene[18])); (* 18 *)
	RealX(043E158E4H, 060913D00H, адресОт(tene[19])); (* 19 *)
	RealX(04415AF1DH, 078B58C40H, адресОт(tene[20])); (* 20 *)
	RealX(0444B1AE4H, 0D6E2EF50H, адресОт(tene[21])); (* 21 *)
	RealX(04480F0CFH, 064DD592H, адресОт(tene[22])); (* 22 *)

	RealX(031FA18H, 02C40C60DH, адресОт(ten[0])); (* -307 *)
	RealX(04F7CAD2H, 03DE82D7BH, адресОт(ten[1])); (* -284 *)
	RealX(09BF7D22H, 08322BAF5H, адресОт(ten[2])); (* -261 *)
	RealX(0E84D669H, 05B193BF8H, адресОт(ten[3])); (* -238 *)
	RealX(0134B9408H, 0EEFEA839H, адресОт(ten[4])); (* -215 *)
	RealX(018123FF0H, 06EEA847AH, адресОт(ten[5])); (* -192 *)
	RealX(01CD82742H, 091C6065BH, адресОт(ten[6])); (* -169 *)
	RealX(0219FF779H, 0FD329CB9H, адресОт(ten[7])); (* -146 *)
	RealX(02665275EH, 0D8D8F36CH, адресОт(ten[8])); (* -123 *)
	RealX(02B2BFF2EH, 0E48E0530H, адресОт(ten[9])); (* -100 *)
	RealX(02FF286D8H, 0EC190DCH, адресОт(ten[10])); (* -77 *)
	RealX(034B8851AH, 0B548EA4H, адресОт(ten[11])); (* -54 *)
	RealX(0398039D6H, 065896880H, адресОт(ten[12])); (* -31 *)
	RealX(03E45798EH, 0E2308C3AH, адресОт(ten[13])); (* -8 *)
	RealX(0430C6BF5H, 026340000H, адресОт(ten[14])); (* 15 *)
	RealX(047D2CED3H, 02A16A1B1H, адресОт(ten[15])); (* 38 *)
	RealX(04C98E45EH, 01DF3B015H, адресОт(ten[16])); (* 61 *)
	RealX(0516078E1H, 011C3556DH, адресОт(ten[17])); (* 84 *)
	RealX(05625CCFEH, 03D35D80EH, адресОт(ten[18])); (* 107 *)
	RealX(05AECDA62H, 055B2D9EH, адресОт(ten[19])); (* 130 *)
	RealX(05FB317E5H, 0EF3AB327H, адресОт(ten[20])); (* 153 *)
	RealX(064794514H, 05230B378H, адресОт(ten[21])); (* 176 *)
	RealX(06940B8E0H, 0ACAC4EAFH, адресОт(ten[22])); (* 199 *)
	RealX(06E0621B1H, 0C28AC20CH, адресОт(ten[23])); (* 222 *)
	RealX(072CD4A7BH, 0EBFA31ABH, адресОт(ten[24])); (* 245 *)
	RealX(077936214H, 09CBD3226H, адресОт(ten[25])); (* 268 *)
	RealX(07C59A742H, 0461887F6H, адресОт(ten[26])); (* 291 *)

	eq[0]:= {0, 3, 4, 5, 9, 16, 23, 25, 26, 28, 31};
	eq[1]:= {2, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 23, 24, 25, 27, 28, 29, 30, 31};
	eq[2]:= {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28};
	eq[3]:= {0, 1, 2, 3, 5, 6, 7, 8, 9, 11, 14, 15, 16, 17, 18, 19, 20, 22, 27, 28, 29, 30, 31};
	eq[4]:= {0, 6, 7, 10, 11, 12, 13, 14, 15, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};
	eq[5]:= {0, 1, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};
	eq[6]:= {0, 1, 4, 5, 7, 8, 10, 14, 15, 16, 18, 20, 21, 23, 24, 25, 26, 28, 29, 30, 31};
	eq[7]:= {0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 18, 19, 23, 24, 26, 28, 29, 30, 31};
	eq[8]:= {0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 14, 16, 17, 18, 19, 20, 21, 24, 25, 26, 29};
	eq[9]:= {1, 2, 4, 6, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};
	eq[10]:= {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30};
	eq[11]:= {0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 27, 28, 29, 30};
	eq[12]:= {0, 1, 2, 3, 4, 5, 7, 8, 9, 10, 12, 14, 15, 16, 17, 18, 19, 20, 21, 23, 26, 27, 29, 30, 31};
	eq[13]:= {0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 13, 14, 15, 16, 17, 18, 20, 21, 23, 24, 27, 28, 29, 30, 31};
	eq[14]:= {0, 1, 2, 3, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};
	eq[15]:= {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 28};
	eq[16]:= {1, 2, 4, 11, 13, 16, 17, 18, 19, 22, 24, 25, 26, 27, 28, 29, 30, 31};
	eq[17]:= {1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 14, 15, 18, 19, 20, 21, 23, 25, 26, 27, 28, 29, 31};
	eq[18]:= {0, 2, 4, 5, 6, 8, 9, 11, 12, 13, 14, 16, 17, 19, 20, 22, 23, 24, 26, 27, 28, 29};
	eq[19]:= {2, 3, 4, 5, 6, 7};

	gr[0]:= {24, 27, 29, 30};
	gr[1]:= {0, 1, 3, 4, 7};
	gr[2]:= {29, 30, 31};
	gr[3]:= {4, 10, 12, 13, 21, 23, 24, 25, 26};
	gr[4]:= {1, 2, 3, 4, 5, 8, 9, 16, 17};
	gr[5]:= {2, 3, 4, 18};
	gr[6]:= {2, 3, 6, 9, 11, 12, 13, 17, 19, 22, 27};
	gr[7]:= {2};
	gr[8]:= {7, 12, 13, 15, 22, 23, 27, 28, 30, 31};
	gr[9]:= {0, 3, 5, 7, 8};
	gr[10]:= {};
	gr[11]:= {};
	gr[12]:= {11, 13, 22, 24, 25, 28};
	gr[13]:= {22, 25, 26};
	gr[14]:= {4, 5};
	gr[15]:= {10, 14, 27, 29, 30, 31};
	gr[16]:= {0, 3, 5, 6, 7, 8, 9, 10, 12, 14, 15, 20, 21, 23};
	gr[17]:= {0, 10, 12, 13, 16, 17, 22, 24, 30};
	gr[18]:= {};
	gr[19]:= {}
кон Reals.

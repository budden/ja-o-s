модуль EnetTftp;
(**
	AUTHOR Timothée Martiel, 2015
	PURPOSE Ethernet network stack, TFTP protocol.
*)
использует
	НИЗКОУР, T := Трассировка,
	EnetBase, EnetInterfaces, EnetStreams, EnetTiming, EnetTrace, EnetUdp;

	конст
		(* Error Status *)
		FileNotFound * = 1;
		AccessViolation * = 2;
		DiskFull * = 3;
		IllegalOperation * = 4;
		UnknownTransferId * = 5;
		FileAlreadyExists * = 6;
		NoSuchUser * = 7;
		TimeoutExpired * = 1000;
		TransferBusy * = 1001;		(** Transfer has not yet completed and cannot be used for another file *)
		TooMuchData * = 1002;		(** User tried to send more data as there are in the file *)

		(* TFTP transfer states *)
		Idle * = 0;						(** Transfer is idle and waiting for order *)
		WaitForData * = 1;			(** Read transfer is receiving data *)
		WaitForAck * = 2;				(** Write transfer is waiting for remote acknowledge *)
		Error * = 3;					(** Transfer was terminated with an error *)
		WaitForUser * = 4;			(** Write transfer is waiting for user to provide data *)

		(* Transfer modes *)
		ModeNetAscii * = 'netascii';	(** Text file transfer mode *)
		ModeOctet * = 'octet';		(** Binary file transfer mode *)
		ModeMail * = 'mail';			(** Mail file transfer mode *)

		(* Error handling *)
		Timeout * = 5000;				(** Acknowledge and waiting timeout, in ms *)
		MaxRetries * = 3;				(** Maximal number of retries for one transmission *)

		(* Opcodes *)
		OpRequestRead * = 1;		(** Transfer is from remote to local. Warning: used as header opcode, do not change value *)
		OpRequestWrite * = 2;		(** Transfer is from local to remote. Warnong: used as header opcode, do not change value *)
		OpData = 3;
		OpAck = 4;
		OpError = 5;

		(** Data transfer block size *)
		DataTxLen = 512;
		(** TFTP Header Length *)
		HeaderLength = 4;

		(** Server listening port *)
		TftpPort = 69;
		LocalPortMin = 60000;
		LocalPortMax = 65000;

		(** Produce Tracing *)
		Trace = ложь;

тип
	(**
		File transfer abstraction.
		Represents a unique file transfer. The datastructure can be reused multiple times.
	*)
	Transfer * = укль на TransferDesc;
	TransferDesc * = запись
		data: массив DataTxLen + HeaderLength из симв8;	(** Internal packet buffer. Holds last sent packet *)
		timeoutTask: EnetBase.TaskHandler;		(** Task for timeout handling *)
		socket: EnetUdp.Socket;					(** Underlying UDP socket *)
		remoteAdr: EnetBase.IpAddr;				(** Remote host address *)
		dataLength,								(** Length of last sent packet *)
		localPort,									(** Local UDP port on which the socket is listening *)
		remotePort: EnetBase.Int;					(** Remote host UDP port *)
		handler: DataReceiveHandler;				(** TFTP packet receiver *)
		handlerParam *: динамическиТипизированныйУкль;						(** Custom parameter for the packet receiver, set by the client *)
		block,										(** Current block ID *)
		remLength,								(** Remaining write transfer length (0 for read transfers *)
		retries,										(** Number of times current transfer has been retried *)
		op -,										(** OpRequestRead for read transfer or OpRequestWrite for write transfer *)
		res -,										(** Transfer result code *)
		state -: EnetBase.Int;						(** Transfer state *)
		next: Transfer;
	кон;

	(**
		TFTP Receiver.
		Is called by a receive transfer on each received data blocks.
		'transfer' is the transfer, 'buf', 'ofs' and 'len' represent the received data. 'res' is the result code and 'end' signals the last block of a transfer (error or transfer completed).
		'packet' points to the receive packet if no error occurred. If 'res' is not 0 then 'packet' is NIL.
	*)
	DataReceiveHandler * = проц {делегат} (transfer: Transfer; перем buf: массив из симв8; ofs, len: EnetBase.Int; res: EnetBase.ResultCode; packet: EnetBase.Packet; end: булево);
	Listener * = проц {делегат} (transfer: Transfer; конст file, mode: массив из симв8; packet: EnetBase.Packet; перем res: EnetBase.ResultCode): булево;

	(** Object wrapper for boolean for the simple blocking task handler *)
	Boolean = укль на запись value: булево кон;

перем
	(** List of all transfers. Used for finding transfers by sockets *)
	transfers: Transfer;
	blockingCompletion: EnetBase.TaskHandler;
	(** Last used local port *)
	lastPort: EnetBase.Int;
	timeout: EnetTiming.Time;

	проц SetListener * (listener: Listener; port: EnetBase.Int);
	нач
	кон SetListener;

	(**
		Start sending a file to host 'destination' with name 'name', mode 'mode'.
		The file is 'length' bytes long and its content is sent using 'SendData'.
	*)
	проц WriteFile * (перем transfer: Transfer; конст name, mode: массив из симв8; length: EnetBase.Int; конст destination: EnetBase.IpAddr; перем res: EnetBase.ResultCode): булево;
	нач
		(* Transfer must not be used *)
		если transfer = НУЛЬ то
			нов(transfer);
			transfer.next := transfers;
			transfers := transfer
		всё;
		если (transfer.state # Idle) и (transfer.state # Error) то res := TransferBusy; возврат ложь всё;

		(* Setup UDP layer *)
		transfer.localPort := GetLocalPort();
		если (transfer.socket = НУЛЬ) и ~EnetUdp.NewSocket(transfer.socket, transfer.localPort, res) то возврат ложь всё;
		transfer.remoteAdr := destination;
		transfer.remotePort := TftpPort;
		blockingCompletion := GetBlockingCompletion();
		если ~EnetUdp.SetRecvHandler(transfer.socket, HandlePacket, res) то возврат ложь всё;
		если ~EnetUdp.SetDestination(transfer.socket, transfer.remoteAdr, transfer.remotePort, blockingCompletion, res) то возврат ложь всё;
		если res = EnetBase.OpInProgress то
			нцПока ~blockingCompletion.param(Boolean).value и EnetInterfaces.UpdateAll(res) делай кц
		всё;
		если res # 0 то возврат ложь всё;

		transfer.remLength := length;
		transfer.block := 0;
		transfer.op := OpRequestWrite;

		(* Send WRQ *)
		WriteRequest(OpRequestWrite, name, mode, transfer.data, transfer.dataLength);
		transfer.state := WaitForAck;
		если ~EnetUdp.Send(transfer.socket, transfer.data, 0, transfer.dataLength, {}, НУЛЬ, res) то возврат ложь всё;
		возврат истина
	кон WriteFile;

	(**
		Start receiving file 'name' with mode 'mode' from host 'source'.
		The receiver 'handler' is called on all received data buffers.
	*)
	проц ReadFile * (перем transfer: Transfer; конст name, mode: массив из симв8; конст source: EnetBase.IpAddr; handler: DataReceiveHandler; handlerParam: динамическиТипизированныйУкль; перем res: EnetBase.ResultCode): булево;
	нач
		res := 0;
		(* Transfer must not be used *)
		если transfer = НУЛЬ то
			нов(transfer);
			нов(transfer.timeoutTask);
			transfer.next := transfers;
			transfers := transfer
		всё;
		transfer.handler := handler;
		transfer.handlerParam := handlerParam;
		если (transfer.state # Idle) и (transfer.state # Error) то res := TransferBusy; возврат ложь всё;

		(* Setup UDP layer *)
		transfer.remoteAdr := source;
		transfer.remotePort := TftpPort;
		если (transfer.socket = НУЛЬ) то
			transfer.localPort := GetLocalPort();
			если ~EnetUdp.NewSocket(transfer.socket, transfer.localPort, res) то возврат ложь всё;
			если ~EnetUdp.SetRecvHandler(transfer.socket, HandlePacket, res) то возврат ложь всё
		всё;
		blockingCompletion := GetBlockingCompletion();
		если ~EnetUdp.SetDestination(transfer.socket, transfer.remoteAdr, transfer.remotePort, blockingCompletion, res) то возврат ложь всё;
		если res = EnetBase.OpInProgress то
			нцПока ~blockingCompletion.param(Boolean).value и EnetInterfaces.UpdateAll(res) делай кц
		всё;
		если res # 0 то возврат ложь всё;
		если Trace то EnetTrace.StringLn("EnetTftp: Found read source, initiating transfer") всё;

		transfer.timeoutTask.param := transfer;
		transfer.timeoutTask.handle := TimeoutHandler;
		transfer.remLength := 0;
		transfer.block := 0;
		transfer.op := OpRequestWrite;

		(* Send RRQ *)
		WriteRequest(OpRequestRead, name, mode, transfer.data, transfer.dataLength);
		transfer.block := 1;
		transfer.state := WaitForData;
		transfer.retries := 0;
		если ~SendWithTimeout(transfer, res) то возврат ложь всё;
		возврат истина
	кон ReadFile;

	(**
		Send file data for the transfer.
		Can be called multiple times per transfer.
		Transfer is automatically terminated when its remaining length has reached 0
	*)
	проц SendData * (transfer: Transfer; конст buf: массив из симв8; ofs, len: EnetBase.Int; completionHandler: EnetBase.TaskHandler; перем res: EnetBase.ResultCode): булево;
	перем
		blockLen, i: EnetBase.Int;
	нач
		если len > transfer.remLength то NotifyError(transfer, TooMuchData, ложь, ложь); возврат ложь всё;
		нцДля i := 0 до len DIV DataTxLen делай
			нцПока (transfer.state = WaitForAck) и EnetInterfaces.UpdateAll(res) делай кц;
			если (res # 0) то
				NotifyError(transfer, res, ложь, ложь);
				возврат ложь
			аесли transfer.state # WaitForUser то
				утв(transfer.state = Error);
				res := transfer.res;
				возврат ложь
			всё;
			blockLen := матМинимум(DataTxLen, len);
			умень(transfer.remLength, blockLen);

			transfer.data[0] := симв8ИзКода(OpData DIV 100H остОтДеленияНа 100H);
			transfer.data[1] := симв8ИзКода(OpData остОтДеленияНа 100H);
			transfer.data[2] := симв8ИзКода(transfer.block DIV 100H остОтДеленияНа 100H);
			transfer.data[3] := симв8ИзКода(transfer.block остОтДеленияНа 100H);
			если blockLen # 0 то
				НИЗКОУР.копируйПамять(адресОт(buf[ofs]), адресОт(transfer.data[4]), blockLen)
			всё;
			transfer.dataLength := blockLen + HeaderLength;
			transfer.retries := 0;
			если ~SendWithTimeout(transfer, res) то NotifyError(transfer, res, ложь, ложь); возврат ложь всё;

			transfer.state := WaitForAck;
			увел(ofs, blockLen);
			умень(len, blockLen)
		кц;
		res := transfer.res;
		возврат transfer.state # Error
	кон SendData;

	(**
		Get error string from TFTP error code.
	*)
	проц GetErrorString * (code: EnetBase.Int; перем str: массив из симв8);
	нач
		просей code из
			 FileNotFound: str := "File not found"
			|AccessViolation: str := "Access violation"
			|DiskFull: str := "DiskFull"
			|IllegalOperation: str := "IllegalOperation"
			|UnknownTransferId: str := "Unknown transfer id"
			|FileAlreadyExists: str := "File already exists"
			|NoSuchUser: str := "No such user"
		иначе
			str := ""
		всё
	кон GetErrorString;

	(**
		Initialize a reader on a file transfer.
		'reader' is the reader. It must be allocated.
		'transfer' is the TFTP transfer.
		'name' is the file name.
		'mode' is the TFTP transfer mode (octet, netascii or mail).
		'source' is the TFTP server IP address.
		'bufferSize' is the stream internal buffer size.
	*)
	проц InitReader * (reader: EnetStreams.Reader; перем transfer: Transfer; конст name, mode: массив из симв8; конст source: EnetBase.IpAddr; bufferSize: EnetBase.Int): булево;
	перем
		ok: булево;
	нач
		утв(reader # НУЛЬ);
		EnetStreams.InitReader(reader^, bufferSize, transfer);
		ok := ReadFile(transfer, name, mode, source, StreamReceiveHandler, reader, reader.res);
		если ok то reader.res := 0 всё;
		возврат ok
	кон InitReader;

	(**
		Initialize a writer on a file transfer.
		'writer' is the stream. It must be allocated.
		'transfer' is the TFTP transfer descriptor.
		'name' is the file name.
		'mode' is the TFTP transfer mode.
		'dest' is the TFTP server address.
		'bufferSize' is the internal stream buffer size.
		'length' is the total length of the transmitted file.
	*)
	проц InitWriter * (writer: EnetStreams.Writer; перем transfer: Transfer; конст name, mode: массив из симв8; конст dest: EnetBase.IpAddr; bufferSize, length: EnetBase.Int): булево;
	нач
		утв(writer # НУЛЬ);
		EnetStreams.InitWriter(writer^, bufferSize, transfer, {}, SendFromWriter);
		возврат WriteFile(transfer, name, mode, length, dest, writer.res)
	кон InitWriter;

	(** Handle reception of a TFTP packet *)
	проц HandlePacket (socket: EnetUdp.Socket; конст remoteAdr: EnetBase.IpAddr; remotePort: EnetBase.Int; перем data: массив из симв8; dataOfs, dataLen: EnetBase.Int; packet: EnetBase.Packet);
	перем
		transfer: Transfer;
		res: EnetBase.ResultCode;
		opcode, block, sendLength: EnetBase.Int;
	нач
		(* Get transfer *)
		transfer := GetTransferBySocket(socket);
		утв(transfer # НУЛЬ);
		если Trace то EnetTrace.StringLn("EnetTftp: Received TFTP packet") всё;

		(* Check remote address and port consistency *)
		если remoteAdr # transfer.remoteAdr то
			если Trace то EnetTrace.StringLn("EnetTftp: Packet not from remote host") всё;
			возврат
		всё;
		если (transfer.remotePort # TftpPort) и (remotePort # transfer.remotePort) то
			(* Not the first data packet of a receive: not allowed to change port *)
			если Trace то EnetTrace.StringLn("EnetTftp: Packet not from remote port") всё;
			возврат
		всё;
		(* ACK of request specifies new transaction port *)
		если remotePort # transfer.remotePort то
			если Trace то EnetTrace.StringLn("EnetTftp: Updating remote port: " и transfer.remotePort и " -> " и remotePort) всё;
			transfer.remotePort := remotePort;
			blockingCompletion := GetBlockingCompletion();
			если ~EnetUdp.SetDestination(transfer.socket, transfer.remoteAdr, transfer.remotePort, blockingCompletion, res) то
				NotifyError(transfer, res, истина, ложь);
				возврат
			всё;
			если res = EnetBase.OpInProgress то
				нцПока ~blockingCompletion.param(Boolean).value и EnetInterfaces.UpdateAll(res) делай кц
			всё
		всё;

		(* Transfer must be waiting for data or for ack *)
		если (transfer.state # WaitForData) и (transfer.state # WaitForAck) то
			если Trace то EnetTrace.StringLn("EnetTftp: Tx is not waiting for packet (" и transfer.state и ")") всё;
			возврат
		всё;
		EnetBase.RemoveTask(transfer.socket.intf, transfer.timeoutTask);
		GetHeader(data, dataOfs, opcode, block);

		если opcode = OpError то
			если Trace то EnetTrace.StringLn("EnetTftp: Received error message: " и block) всё;
			NotifyError(transfer, block, transfer.state = WaitForData, ложь);
			возврат
		аесли transfer.state = WaitForData то
			если (opcode # OpData) то
				если Trace то EnetTrace.StringLn("EnetTftp: Opcode is not 'data': " и opcode) всё;
				NotifyError(transfer, IllegalOperation, истина, истина);
				возврат
			всё;
			если (block # transfer.block) то
				если Trace то EnetTrace.StringLn("EnetTftp: Block # is not as expected (" и block и " instead of " и transfer.block и ")") всё;
				NotifyError(transfer, IllegalOperation, истина, истина);
				возврат
			всё;

			WriteAck(transfer.block, transfer.data, sendLength);
			если dataLen - HeaderLength < DataTxLen то
				(* Last packet: send an ack without waiting for next packet *)
				если ~EnetUdp.Send(transfer.socket, transfer.data, 0, sendLength, {}, blockingCompletion, res) то
					NotifyError(transfer, res, истина, ложь);
					возврат
				всё;
				transfer.state := Idle;
			иначе
				(* Other packets to receive: use timeout *)
				transfer.dataLength := sendLength;
				transfer.retries := 0;
				если ~SendWithTimeout(transfer, res) то
					NotifyError(transfer, res, истина, ложь);
					возврат
				всё;
				увел(transfer.block)
			всё;
			(* Call handler *)
			увел(packet.payloadOffs, HeaderLength);
			transfer.handler(transfer, data, dataOfs + HeaderLength, dataLen - HeaderLength, 0, packet, dataLen - HeaderLength < DataTxLen)

		иначе
			(* Waiting for ACK *)
			если opcode # OpAck то
				если Trace то EnetTrace.StringLn("EnetTftp: Opcode is not 'ack' (" и opcode и ")") всё;
				NotifyError(transfer, IllegalOperation, ложь, истина);
				возврат
			всё;
			если (block # transfer.block) то
				если Trace то EnetTrace.StringLn("EnetTftp: Block # is not as expected (" и block и " instead of " и transfer.block и ")") всё;
				NotifyError(transfer, IllegalOperation, ложь, истина);
				возврат
			всё;

			если transfer.remLength > 0 то
				если Trace то EnetTrace.StringLn("EnetTftp: Still " и transfer.remLength и " B to transfer") всё;
				transfer.state := WaitForUser
			иначе
				если Trace то EnetTrace.StringLn("EnetTftp: Transfer finished") всё;
				transfer.state := Idle
			всё;
			увел(transfer.block)
		всё
	кон HandlePacket;

	(** Handle Timeout *)
	проц TimeoutHandler (handler: EnetBase.TaskHandler);
	перем
		transfer: Transfer;
		res: EnetBase.ResultCode;
	нач
		transfer := handler.param(Transfer);
		если ((transfer.state = WaitForData) или (transfer.state = WaitForAck)) то
			если Trace то EnetTrace.StringLn("EnetTftp: Timeout") всё;
			если transfer.retries = MaxRetries то
				если Trace то EnetTrace.StringLn("EnetTftp: Max number of retries exceeded, transfer error") всё;
				NotifyError(transfer, TimeoutExpired, истина, ложь);
				возврат
			всё;
			увел(transfer.retries);
			если ~SendWithTimeout(transfer, res) то
				NotifyError(transfer, TimeoutExpired, истина, ложь);
				возврат
			всё
		всё
	кон TimeoutHandler;

	(** Sends a packet for a transfer with a timeout for reception *)
	проц SendWithTimeout (transfer: Transfer; перем res: EnetBase.ResultCode): булево;
	нач
		если Trace то EnetTrace.StringLn("EnetTftp: Sending packet (" и transfer.dataLength и " B)") всё;
		EnetBase.ScheduleTask(transfer.socket.intf, transfer.timeoutTask, ложь, timeout);
		возврат EnetUdp.Send(transfer.socket, transfer.data, 0, transfer.dataLength, {}, НУЛЬ, res)
	кон SendWithTimeout;

	(** Sets the transfer error state to 'res'. *)
	проц NotifyError (transfer: Transfer; res: EnetBase.ResultCode; doHandle, sendErrorMsg: булево);
	перем
		ignoreRes: EnetBase.ResultCode;
		ignore: булево;
	нач
		transfer.res := res;
		transfer.state := Error;
		если doHandle то transfer.handler(transfer, transfer.data, 0, 0, res, НУЛЬ, истина) всё;
		если sendErrorMsg то
			transfer.data[0] := симв8ИзКода(OpError DIV 100H остОтДеленияНа 100H);
			transfer.data[1] := симв8ИзКода(OpError остОтДеленияНа 100H);
			transfer.data[2] := симв8ИзКода(res DIV 100H остОтДеленияНа 100H);
			transfer.data[3] := симв8ИзКода(res остОтДеленияНа 100H);
			transfer.data[4] := 0X;
			ignore := EnetUdp.Send(transfer.socket, transfer.data, 0, 5, {}, НУЛЬ, ignoreRes)
		всё
	кон NotifyError;

	проц GetHeader(конст data: массив из симв8; ofs: EnetBase.Int; перем opcode, block: EnetBase.Int);
	нач
		opcode := кодСимв8(data[ofs]) * 100H + кодСимв8(data[ofs + 1]); увел(ofs, 2);
		block := кодСимв8(data[ofs]) * 100H + кодСимв8(data[ofs + 1])
	кон GetHeader;

	проц WriteRequest (opcode: EnetBase.Int; конст filename, mode: массив из симв8; перем packet: массив из симв8; перем length: EnetBase.Int);
	нач
		утв((opcode = OpRequestRead) или (opcode = OpRequestWrite));
		утв(filename # '');
		утв((mode = ModeNetAscii) или (mode = ModeOctet) или (mode = ModeMail));
		length := 0;
		packet[length] := симв8ИзКода(opcode DIV 100H остОтДеленияНа 100H); увел(length);
		packet[length] := симв8ИзКода(opcode остОтДеленияНа 100H); увел(length);
		CopyString(filename, packet, length);
		packet[length] := 0X; увел(length);
		CopyString(mode, packet, length);
		packet[length] := 0X; увел(length)
	кон WriteRequest;

	проц WriteAck (block: EnetBase.Int; перем packet: массив из симв8; перем length: EnetBase.Int);
	нач
		length := 0;
		packet[length] := симв8ИзКода(OpAck DIV 100H остОтДеленияНа 100H);
		packet[length + 1] := симв8ИзКода(OpAck остОтДеленияНа 100H); увел(length, 2);
		packet[length] := симв8ИзКода(block DIV 100H остОтДеленияНа 100H);
		packet[length + 1] := симв8ИзКода(block остОтДеленияНа 100H); увел(length, 2)
	кон WriteAck;

	проц GetTransferBySocket (socket: EnetUdp.Socket): Transfer;
	перем
		cur: Transfer;
	нач
		cur := transfers;
		нцПока (cur # НУЛЬ) и (cur.socket # socket) делай cur := cur.next кц;
		возврат cur
	кон GetTransferBySocket;

	проц CopyString (конст src: массив из симв8; перем dest: массив из симв8; перем destOfs: EnetBase.Int);
	перем
		len: EnetBase.Int;
	нач
		len := 0;
		нцПока src[len] # 0X делай увел(len) кц;
		если len >= длинаМассива(dest) то len := длинаМассива(dest) всё;
		НИЗКОУР.копируйПамять(адресОт(src[0]), адресОт(dest[destOfs]), len);
		увел(destOfs, len)
	кон CopyString;

	проц GetBlockingCompletion (): EnetBase.TaskHandler;
	перем
		b: Boolean;
	нач
		если blockingCompletion = НУЛЬ то
			нов(blockingCompletion);
			нов(b);
			blockingCompletion.handle := BlockingCompletion;
			blockingCompletion.param := b;
		всё;
		blockingCompletion.param(Boolean).value := ложь;
		возврат blockingCompletion
	кон GetBlockingCompletion;

	проц GetLocalPort (): цел32;
	перем
		port: цел32;
	нач
		port := lastPort;
		увел(lastPort);
		если lastPort > LocalPortMax то lastPort := LocalPortMin всё;
		возврат port
	кон GetLocalPort;

	проц BlockingCompletion (t: EnetBase.TaskHandler);
	нач
		t.param(Boolean).value := истина
	кон BlockingCompletion;

	проц StreamReceiveHandler (transfer: Transfer; перем buffer: массив из симв8; ofs, len: EnetBase.Int; res: EnetBase.ResultCode; packet: EnetBase.Packet; end: булево);
	нач
		если end то
			transfer.handlerParam(EnetStreams.Reader).enetEndOfStream := истина
		всё;
		если res = 0 то
			packet.ownedByUser := истина;
			утв(EnetBase.PacketFifoPut(transfer.handlerParam(EnetStreams.Reader).enetPackets, packet))
		всё
	кон StreamReceiveHandler;

	проц SendFromWriter (access: динамическиТипизированныйУкль; конст buf: массив из симв8; ofs, len: EnetBase.Int; flags: мнвоНаБитахМЗ; перем res: EnetBase.ResultCode);
	перем
		ignore: булево;
	нач
		ignore := SendData(access(Transfer), buf, ofs, len, НУЛЬ, res)
	кон SendFromWriter;

	проц Init;
	нач
		lastPort := LocalPortMin;
		timeout := EnetTiming.fromMilli(Timeout)
	кон Init;
нач 
	Init
кон EnetTftp.

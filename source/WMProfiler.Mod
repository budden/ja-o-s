модуль WMProfiler; (** AUTHOR "staubesv"; PURPOSE "GUI for hierarchical profiler"; *)

использует
	Modules, Kernel, Strings, HierarchicalProfiler, WMGraphics, WMMessages, WMRestorable,
	WMWindowManager, WMComponents, WMStandardComponents, WMEditors, WMTrees, WMErrors, WMProgressComponents;

конст
	DefaultTime = 30;

тип

	KillerMsg = окласс
	кон KillerMsg;

	Window* = окласс (WMComponents.FormWindow)
	перем
		startBtn, stopBtn,continueBtn, getProfileBtn, flattenBtn, filterBtn : WMStandardComponents.Button;
		timeEdit, typeEdit, infoEdit, filterMaskEdit, filterCountEdit : WMEditors.Editor;

		tree : WMTrees.Tree;
		treeView : WMTrees.TreeView;

		lastSelectedNode : HierarchicalProfiler.Node;
		lastState : цел32;

		statusLabel : WMStandardComponents.Label;

		progressBar : WMProgressComponents.ProgressBar;

		profile : HierarchicalProfiler.Profile;

		alive, dead : булево;
		timer : Kernel.Timer;

		проц HasVisibleChildren(node : HierarchicalProfiler.Node) : булево;
		перем child : HierarchicalProfiler.Node;
		нач
			утв(node # НУЛЬ);
			child := node.child;
			нцПока (child # НУЛЬ) и (child.marked # истина) делай child := child.sibling; кц;
			возврат (child # НУЛЬ);
		кон HasVisibleChildren;

		проц AddChildNodes(parent : WMTrees.TreeNode; node : HierarchicalProfiler.Node; expand : булево);
		перем n : WMTrees.TreeNode; temp : HierarchicalProfiler.Node;
		нач
			утв(parent # НУЛЬ);
			temp := node.child;
			node.extern := истина;
			нцПока (temp # НУЛЬ) делай
				нов(n);
				tree.AddChildNode(parent, n);
				tree.SetNodeCaption(n, temp.GetCaption());
				tree.SetNodeData(n, temp);
				если (temp.marked) то
					tree.ExclNodeState(n, WMTrees.NodeHidden);
					если HasVisibleChildren(temp) то tree.InclNodeState(n, WMTrees.NodeSubnodesOnExpand); всё;
				иначе
					tree.InclNodeState(n, WMTrees.NodeHidden);
				всё;
				если expand то
					tree.ExpandToRoot(n);
					AddChildNodes(n, temp, ложь);
				всё;
				temp := temp.sibling;
			кц;
		кон AddChildNodes;

		проц ClearExternField(node : HierarchicalProfiler.Node);
		нач
			утв(node # НУЛЬ);
			node.extern := ложь;
		кон ClearExternField;

		проц UpdateTree;
		перем root : WMTrees.TreeNode;
		нач
			tree.Acquire;
			нов(root);
			tree.SetRoot(root);
			если (profile # НУЛЬ) то
				profile.Visit(ClearExternField);
				tree.SetNodeCaption(root, profile.nodes.GetCaption());
				tree.SetNodeData(root, profile);
				AddChildNodes(root, profile.nodes, истина);
			иначе
				tree.SetNodeCaption(root, Strings.NewString("No profile data"));
			всё;
			tree.Release;
			treeView.SetFirstLine(0, истина);
		кон UpdateTree;

		проц UpdateStatusBar(forceUpdate : булево);
		перем
			state: целМЗ; currentNofSamples, maxNofSamples : размерМЗ;
			caption : массив 256 из симв8; number : массив 16 из симв8;
		нач
			state := HierarchicalProfiler.GetState(currentNofSamples, maxNofSamples);
			если (state # lastState) или forceUpdate то
				если (state = HierarchicalProfiler.Running) то
					progressBar.SetRange(0, maxNofSamples);
				всё;

				просей state из
					|HierarchicalProfiler.Running: caption := "Profiler is running...";
					|HierarchicalProfiler.NotRunningNoDataAvailable: caption := "Profiler is not running. No profile data available.";
					|HierarchicalProfiler.NotRunningDataAvailable: caption := "Profiler is not running.";
				иначе
					caption := "Profiler is in undefined state.";
				всё;
				если (profile # НУЛЬ) то
					Strings.Append(caption, " [Loaded profile: ");
					Strings.IntToStr(profile.nofSamples, number); Strings.Append(caption, number);
					Strings.Append(caption, " samples on ");
					Strings.IntToStr(profile.nofProcessors, number); Strings.Append(caption, number);
					Strings.Append(caption, " processor(s), ");
					Strings.IntToStr(profile.nofSamplesNotStored, number); Strings.Append(caption, number);
					Strings.Append(caption, " discarded, ");
					Strings.IntToStr(profile.nofRunsTooDeep, number); Strings.Append(caption, number);
					Strings.Append(caption, " call chain too deep]");
				иначе
					Strings.Append(caption, " [No profile loaded]");
				всё;
				statusLabel.caption.SetAOC(caption);
				lastState := state;
			всё;
			если (state = HierarchicalProfiler.Running) то
				progressBar.SetCurrent(currentNofSamples);
			всё;
		кон UpdateStatusBar;

		проц HandleNodeClicked(sender, data : динамическиТипизированныйУкль);
		перем treeNode : WMTrees.TreeNode; node : HierarchicalProfiler.Node; ptr : динамическиТипизированныйУкль;
		нач
			если (data # НУЛЬ) и (data суть WMTrees.TreeNode) то
				tree.Acquire;
				treeNode := data (WMTrees.TreeNode);
				ptr := tree.GetNodeData(treeNode);
				если (ptr # НУЛЬ) и (ptr суть HierarchicalProfiler.Node) то
					node := ptr (HierarchicalProfiler.Node);
					если ~node.extern то
						AddChildNodes(treeNode, node, ложь);
					всё;
				всё;
				tree.Release;
			всё;
		кон HandleNodeClicked;

		проц HandleNodeSelected(sender, data : динамическиТипизированныйУкль);
		перем ptr : динамическиТипизированныйУкль;
		нач
			если (data # НУЛЬ) и (data суть WMTrees.TreeNode) то
				tree.Acquire;
				ptr := tree.GetNodeData(data(WMTrees.TreeNode));
				tree.Release;
				если (ptr # НУЛЬ) и (ptr суть HierarchicalProfiler.Node) то
					lastSelectedNode := ptr (HierarchicalProfiler.Node);
				всё;
			всё;
		кон HandleNodeSelected;

		проц GetTypeAndInfo(перем type, info : цел32);
		перем string : массив 16 из симв8;
		нач
			type := HierarchicalProfiler.Hierarchical;
			typeEdit.GetAsString(string);
			Strings.StrToInt(string, type);

			info := HierarchicalProfiler.None;
			infoEdit.GetAsString(string);
			Strings.StrToInt(string, info);
		кон GetTypeAndInfo;

		проц GetFilterMask(перем mask : массив из симв8; перем minPercent : цел32);
		перем number : массив 16 из симв8;
		нач
			filterMaskEdit.GetAsString(mask);
			filterCountEdit.GetAsString(number);
			Strings.StrToInt(number, minPercent);
		кон GetFilterMask;

		проц HandleButtons(sender, data : динамическиТипизированныйУкль);
		перем
			tempProfile : HierarchicalProfiler.Profile;
			mask : массив 128 из симв8; timeStr : массив 16 из симв8;
			minPercent : цел32;
			time, type, info: цел32; res: целМЗ;
			root : WMTrees.TreeNode;
		нач
			если (sender = startBtn) то
				time := 0;
				timeEdit.GetAsString(timeStr);
				Strings.StrToInt(timeStr, time);
				если (time > 0) то
					HierarchicalProfiler.StartProfiling(time, res);
					если (res # HierarchicalProfiler.Ok) то WMErrors.Show(res); всё;
				иначе
					WMErrors.ShowMessage("Invalid sampling time");
				всё;
			аесли (sender = stopBtn) то
				HierarchicalProfiler.StopProfiling(res);
				если (res # HierarchicalProfiler.Ok) то WMErrors.Show(res); всё;
			аесли (sender = continueBtn) то
				HierarchicalProfiler.ContinueProfiling(res);
				если (res # HierarchicalProfiler.Ok) то WMErrors.Show(res); всё;
			аесли (sender = flattenBtn) то
				если (lastSelectedNode # НУЛЬ) то
					если (profile # НУЛЬ) то
						profile.Flatten(lastSelectedNode);
						UpdateTree;
					иначе
						WMErrors.ShowMessage("No profile");
					всё;
				иначе
					WMErrors.ShowMessage("No node selected");
				всё;
			аесли (sender = getProfileBtn) то
				GetTypeAndInfo(type, info);
				HierarchicalProfiler.GetProfile(type, info, tempProfile, res);
				если (res = HierarchicalProfiler.Ok) то
					profile := tempProfile;
					UpdateTree;
					UpdateStatusBar(истина);
				иначе
					WMErrors.Show(res);
				всё;
			аесли (sender = filterBtn) то
				если (profile # НУЛЬ) то
					GetFilterMask(mask, minPercent);
					profile.Mark(mask, minPercent);
					tree.Acquire;
					root := tree.GetRoot();
					tree.Release;
					UpdateTree;
				иначе
					WMErrors.ShowMessage("No profile");
				всё;
			всё;
		кон HandleButtons;

		проц CreateForm() : WMComponents.VisualComponent;
		перем
			panel, toolbar, filterbar, statusbar : WMStandardComponents.Panel;
			label : WMStandardComponents.Label;
			timeStr : массив 16 из симв8;
		нач
			нов(panel); panel.alignment.Set(WMComponents.AlignClient); panel.fillColor.Set(WMGraphics.White);

			нов(toolbar); toolbar.alignment.Set(WMComponents.AlignTop); toolbar.bounds.SetHeight(20);
			panel.AddContent(toolbar);

			нов(startBtn); startBtn.alignment.Set(WMComponents.AlignLeft);
			startBtn.caption.SetAOC("Start");
			startBtn.onClick.Add(HandleButtons);
			toolbar.AddContent(startBtn);

			нов(stopBtn); stopBtn.alignment.Set(WMComponents.AlignLeft);
			stopBtn.caption.SetAOC("Stop");
			stopBtn.onClick.Add(HandleButtons);
			toolbar.AddContent(stopBtn);

			нов(continueBtn); continueBtn.alignment.Set(WMComponents.AlignLeft);
			continueBtn.caption.SetAOC("Continue");
			continueBtn.onClick.Add(HandleButtons);
			toolbar.AddContent(continueBtn);

			нов(getProfileBtn); getProfileBtn.alignment.Set(WMComponents.AlignLeft);
			getProfileBtn.bounds.SetWidth(80);
			getProfileBtn.caption.SetAOC("Get Profile");
			getProfileBtn.onClick.Add(HandleButtons);
			toolbar.AddContent(getProfileBtn);

			нов(label); label.alignment.Set(WMComponents.AlignLeft); label.bounds.SetWidth(120);
			label.caption.SetAOC(" Max. sampling time: ");
			toolbar.AddContent(label);

			нов(timeEdit); timeEdit.alignment.Set(WMComponents.AlignLeft); timeEdit.bounds.SetWidth(40);
			timeEdit.tv.textAlignV.Set(WMGraphics.AlignCenter);
			timeEdit.multiLine.Set(ложь);
			timeEdit.tv.showBorder.Set(истина);
			Strings.IntToStr(DefaultTime, timeStr);
			timeEdit.SetAsString(timeStr);
			toolbar.AddContent(timeEdit);

			нов(label); label.alignment.Set(WMComponents.AlignLeft); label.bounds.SetWidth(45);
			label.caption.SetAOC(" seconds");
			toolbar.AddContent(label);

			нов(label); label.alignment.Set(WMComponents.AlignLeft); label.bounds.SetWidth(35);
			label.caption.SetAOC(" Type: ");
			toolbar.AddContent(label);

			нов(typeEdit); typeEdit.alignment.Set(WMComponents.AlignLeft); typeEdit.bounds.SetWidth(40);
			typeEdit.tv.textAlignV.Set(WMGraphics.AlignCenter);
			typeEdit.multiLine.Set(ложь);
			typeEdit.tv.showBorder.Set(истина);
			Strings.IntToStr(HierarchicalProfiler.Hierarchical, timeStr);
			typeEdit.SetAsString(timeStr);
			toolbar.AddContent(typeEdit);

			нов(label); label.alignment.Set(WMComponents.AlignLeft); label.bounds.SetWidth(30);
			label.caption.SetAOC(" Info: ");
			toolbar.AddContent(label);

			нов(infoEdit); infoEdit.alignment.Set(WMComponents.AlignLeft); infoEdit.bounds.SetWidth(40);
			infoEdit.tv.textAlignV.Set(WMGraphics.AlignCenter);
			infoEdit.multiLine.Set(ложь);
			infoEdit.tv.showBorder.Set(истина);
			Strings.IntToStr(0, timeStr);
			infoEdit.SetAsString(timeStr);
			toolbar.AddContent(infoEdit);

			нов(flattenBtn); flattenBtn.alignment.Set(WMComponents.AlignRight);
			flattenBtn.caption.SetAOC("Flatten");
			flattenBtn.onClick.Add(HandleButtons);
			toolbar.AddContent(flattenBtn);

			нов(statusbar); statusbar.alignment.Set(WMComponents.AlignBottom); statusbar.bounds.SetHeight(20);
			statusbar.fillColor.Set(0CCCCCCFFH);
			panel.AddContent(statusbar);

			нов(progressBar); progressBar.alignment.Set(WMComponents.AlignRight);
			progressBar.bounds.SetWidth(80);
			progressBar.color.Set(WMGraphics.Blue);
			progressBar.textColor.Set(WMGraphics.White);
			progressBar.SetRange(0, 100);
			progressBar.SetCurrent(0);
			progressBar.showPercents.Set(истина);
			statusbar.AddContent(progressBar);

			нов(statusLabel); statusLabel.alignment.Set(WMComponents.AlignClient);
			statusbar.AddContent(statusLabel);

			нов(filterbar); filterbar.alignment.Set(WMComponents.AlignBottom); filterbar.bounds.SetHeight(20);
			panel.AddContent(filterbar);

			нов(label); label.alignment.Set(WMComponents.AlignLeft); label.bounds.SetWidth(110);
			label.caption.SetAOC(" Filter mask: Name: ");
			filterbar.AddContent(label);

			нов(filterBtn); filterBtn.alignment.Set(WMComponents.AlignRight);
			filterBtn.caption.SetAOC("Apply");
			filterBtn.onClick.Add(HandleButtons);
			filterbar.AddContent(filterBtn);

			нов(filterCountEdit); filterCountEdit.alignment.Set(WMComponents.AlignRight); filterCountEdit.bounds.SetWidth(40);
			filterCountEdit.multiLine.Set(ложь);
			filterCountEdit.tv.textAlignV.Set(WMGraphics.AlignCenter);
			filterCountEdit.tv.showBorder.Set(истина);
			filterCountEdit.SetAsString("0");
			filterbar.AddContent(filterCountEdit);

			нов(label); label.alignment.Set(WMComponents.AlignRight); label.bounds.SetWidth(40);
			label.caption.SetAOC(" Min %: ");
			filterbar.AddContent(label);

			нов(filterMaskEdit); filterMaskEdit.alignment.Set(WMComponents.AlignClient);
			filterMaskEdit.tv.textAlignV.Set(WMGraphics.AlignCenter);
			filterMaskEdit.multiLine.Set(ложь);
			filterMaskEdit.tv.showBorder.Set(истина);
			filterMaskEdit.SetAsString("*");
			filterbar.AddContent(filterMaskEdit);

			нов(treeView); treeView.alignment.Set(WMComponents.AlignClient);
			treeView.onSelectNode.Add(HandleNodeSelected);
			tree := treeView.GetTree();
			tree.beforeExpand.Add(HandleNodeClicked);
			panel.AddContent(treeView);

			возврат panel;
		кон CreateForm;

		проц &New*(context : WMRestorable.Context);
		нач
			нов(timer);
			alive := истина; dead := ложь;
			lastSelectedNode := НУЛЬ;
			lastState := -1; (* invalid state *)
			profile := НУЛЬ;
			Init(640, 480, ложь);
			SetContent(CreateForm());
			UpdateStatusBar(истина);
			SetTitle(Strings.NewString("Profiler"));
			если (context # НУЛЬ) то
				WMRestorable.AddByContext(сам, context);
				Resized(context.r - context.l, context.b - context.t);
			иначе
				WMWindowManager.DefaultAddWindow(сам)
			всё;
			IncCount;
		кон New;

		проц {перекрыта}Handle*(перем x : WMMessages.Message);
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) то
				если (x.ext суть KillerMsg) то Close
				аесли (x.ext суть WMRestorable.Storage) то
					x.ext(WMRestorable.Storage).Add("WMProfiler", "WMProfiler.Restore", сам, НУЛЬ);
				иначе Handle^(x)
				всё
			иначе Handle^(x)
			всё
		кон Handle;

		проц {перекрыта}Close*;
		нач
			Close^;
			alive := ложь;
			timer.Wakeup;
			нач {единолично} дождись(dead); кон;
			DecCount;
		кон Close;

	нач {активное}
		нцПока alive делай
			UpdateStatusBar(ложь);
			timer.Sleep(500);
		кц;
		нач {единолично} dead := истина; кон;
	кон Window;

перем
	nofWindows : цел32;

проц Open*;
перем window : Window;
нач
	нов(window, НУЛЬ);
кон Open;

проц Restore*(context : WMRestorable.Context);
перем window : Window;
нач
	нов(window, context);
кон Restore;

проц IncCount;
нач {единолично}
	увел(nofWindows)
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows)
кон DecCount;

проц Cleanup;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WMWindowManager.WindowManager;
нач {единолично}
	нов(die);
	msg.ext := die;
	msg.msgType := WMMessages.MsgExt;
	m := WMWindowManager.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0)
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон WMProfiler.


WMProfiler.Open ~

System.Free WMProfiler ~

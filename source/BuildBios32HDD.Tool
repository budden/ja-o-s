# Постройка образа ЯОС по инструкции Ярослава, 
# https://forum.oberoncore.ru/viewtopic.php?f=22&t=63867
# Порядок выполнения:
#   Выполнить 3 команды - появится образ ЯОС:BIOS32/A2IDE.img
#   В Linux - запустить в QEMU с помощью скрипта ЯОС:BIOS32/a2-hdd.bash 
#...В Windows - с помощью BIOS32/a2.bat
#  Альтернативная команда запуска для Win от Ярослава:
#  "c:\Program Files\qemu\qemu-system-x86_64.exe" -drive file=A2IDE.img,format=raw -m 1024 -machine pc -chardev serial,id=com1,path=com1
#
# Также можно запустить под VirtualBox, для этого нужно преобразовать образ
# /mnt/c/prf/qemu-20200221/qemu-img.exe convert -f raw -O vmdk A2IDE.img A2IDE.vmdk
# и далее подсунуть его в virtualBox - архив с какой-то сборкой может находиться на яндекс-диске, см.
# http://вече.программирование-по-русски.рф/viewtopic.php?f=5&t=244
# 
# Для подсовывания диска нужно сначала удалить старый диск, удалить его в 
# менеджере носителей VirtualBox, а уже потом добавлять новый диск.
# Затем для передачи на другой компьютер нужно через Файл/экспорт конфигураций
# выгрузить машину в формате ova.
#
# Дополнительная информация - см. BIOS32/ПРОЧТИМЯ.md

# 1. Создаём директорию 
FSTools.CreateDirectory ЯОС:NewBios32HDD ~

# 2. Очищаем старое и собираем
System.DoCommands
	FSTools.DeleteFiles -i ЯОС:NewBios32HDD/* ~
  СборщикВыпускаЯОС.Build --lis --path="ЯОС:NewBios32HDD/" --zip --build --xml Bios32 ~~

# 3. Подготовка диска
System.DoCommands

Compiler.Compile -p=Bios32 --destPath=ЯОС:NewBios32HDD/ BIOS.PCI.Mod UsbEhci.Mod BIOS.UsbEhciPCI.Mod ~

PCAAMD64.Assemble OBLUnreal.Asm ~
PartitionsLib.SetBootLoaderFile OBLUnreal.Bin ~
PCAAMD64.Assemble BootManager.Asm ~
BootManager.Split BootManager.Bin ~
System.Timer start ~

FSTools.DeleteFiles -i ЯОС:NewBios32HDD/A2IDE.img ~

VirtualDisks.Create ЯОС:NewBios32HDD/A2IDE.img 320000 512 ~
VirtualDisks.Install -b=512 VDISK0 ЯОС:NewBios32HDD/A2IDE.img ~

Partitions.WriteMBR VDISK0#0 OBEMBR.BIN ~
Partitions.InstallBootManager VDISK0#0 BootManagerMBR.Bin BootManagerTail.Bin ~
Partitions.Create VDISK0#1 76 150 ~

Linker.Link --path=ЯОС:NewBios32HDD/ --displacement=100000H --fileName=ЯОС:NewBios32HDD/IDE.Bin Kernel Traps
ATADisks DiskVolumes DiskFS Loader BootConsole ~

Partitions.Format VDISK0#1 AosFS -1 ЯОС:NewBios32HDD/IDE.Bin ~ (* -1 makes sure that actual boot file size is taken as offset for AosFS *)
FSTools.Mount TEMP AosFS VDISK0#1 ~

ZipTool.ExtractAll --prefix=TEMP: --sourcePath=ЯОС:NewBios32HDD/ --overwrite --silent
Kernel.zip System.zip Drivers.zip ApplicationsNano.zip ApplicationsMini.zip Applications.zip Compiler.zip CompilerSrc.zip
GuiApplicationsMini.zip GuiApplications.zip Fun.zip Contributions.zip Build.zip EFI.zip
Oberon.zip OberonGadgets.zip OberonApplications.zip OberonDocumentation.zip
KernelSrc.zip SystemSrc.zip DriversSrc.zip ApplicationsMiniSrc.zip ApplicationsSrc.zip GuiApplicationsMiniSrc.zip GuiApplicationsSrc.zip FunSrc.zip BuildSrc.zip
ScreenFonts.zip CjkFonts.zip TrueTypeFonts.zip ~

FSTools.DeleteFiles TEMP:Configuration.XML ~
FSTools.CopyFiles BIOS.Configuration.XML => TEMP:Configuration.XML ~

FSTools.Unsafe ~
FSTools.DeleteFiles -i TEMP:*.перев.XML ~

FSTools.CopyFiles WORK:*.xym => TEMP:*.xym ~
FSTools.CopyFiles *.перев.XML => TEMP:/*.XML ~
FSTools.Safe ~

FSTools.Watch TEMP ~
FSTools.Unmount TEMP ~

Partitions.SetConfig VDISK0#1
TraceMode="4"
TracePort="1"
TraceBPS="115200"
BootVol1="AOS AosFS IDE0#1"
AosFS="DiskVolumes.New DiskFS.NewFS"
CacheSize="1000"
ExtMemSize="512"
MaxProcs="-1"
ATADetect="legacy"
Init="117"
Boot="ЗагрузиПереводыЭлементовКода.ИзВсехФайлов"
Boot1="V24.Install;ShellSerial.Open 1 115200 no 1 8"
Boot2="DisplayLinear.Install"
Boot3="Keyboard.Install;MousePS2.Install"
Boot4="DriverDatabase.Enable;UsbHubDriver.Install;UsbEhciPCI.Install;UsbUhci.Install;UsbOhci.Install"
Boot5="WindowManager.Install"
Boot6="Autostart.Run"
~
VirtualDisks.Uninstall VDISK0 ~

System.Show HDD image build time: ~ System.Timer elapsed ~

FSTools.CloseFiles ЯОС:NewBios32HDD/A2IDE.img ~

FSTools.DeleteFiles -i ЯОС:BIOS32/A2IDE.img ~

FSTools.CopyFiles ЯОС:NewBios32HDD/A2IDE.img => ЯОС:BIOS32/A2IDE.img ~

~

модуль UsbBuffers; (** AUTHOR ""; PURPOSE ""; *)

конст
	Align = 32;

тип
	BufferPtr * = Buffer;
	Buffer * = окласс
	перем
		data: укль на массив из симв8;
		ofs: цел32;

		проц & SetSize * (size: цел32);
		нач
			нов(data, size + Align);
			ofs := Align - адресОт(data[0]) остОтДеленияНа Align;
			утв(адресОт(data[ofs]) остОтДеленияНа Align = 0)
		кон SetSize;

		проц ToArray * (): укль на массив из симв8;
		перем
			ptr: укль на массив из симв8;
			i: цел32;
		нач
			нов(ptr, длинаМассива(data) - Align);
			нцДля i := 0 до длинаМассива(ptr) - 1 делай ptr[i] := data[ofs + i] кц;
			возврат ptr
		кон ToArray;

		проц ToArrayOfs * (): цел32;
		нач
			возврат ofs
		кон ToArrayOfs;

		операция "[]" * (idx: цел32): симв8;
		нач
			утв(idx >= 0, 7);
			утв(idx < длинаМассива(data) - Align, 7);
			возврат data[ofs + idx]
		кон "[]";

		операция "[]" * (idx: цел32; val: симв8);
		нач
			утв(idx >= 0, 7);
			утв(idx < длинаМассива(data) - Align, 7);
			data[ofs + idx] := val
		кон "[]";
	кон Buffer;

	проц GetDataAddress * (buffer: Buffer): адресВПамяти;
	нач
		утв(адресОт(buffer.data[buffer.ofs]) # 0);
		возврат адресОт(buffer.data[buffer.ofs])
	кон GetDataAddress;

	операция "длинаМассива" * (buffer: Buffer): цел32;
	нач
		возврат длинаМассива(buffer.data) - Align
	кон "длинаМассива";
кон UsbBuffers.

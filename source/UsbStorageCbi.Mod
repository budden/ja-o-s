модуль UsbStorageCbi;  (** AUTHOR "cplattner/staubesv"; PURPOSE " CB/I transport layer of USB mass storage driver"; *)
(**
 * References:
 *
 *	- 	[1] Universal Serial Bus Mass Storage Class Control/Bulk/Interrupt (CBI) Transport, Revision 1.1, June 23, 2003
 *		www.usb.org
 *
 * History:
 *
 *	09.02.2006	First release (staubesv)
 *	05.07.2006	Adapted to Usbdi (staubesv)
 *	07.08.2006	Cleanups, improved error case in transport, fixed transfer offset ignored  (staubesv)
 *)

использует
	ЛогЯдра,
	Base := UsbStorageBase, Usbdi, Debug := UsbDebug;

конст

	(* Interrupt Data Block coding, 3.4.3.1.1 in [1] *)
	Pass = 0;
	Fail = 1;
	PhaseError = 2;
	PersistentFailure = 3;

тип

	(** USB Mass Storage Class Control/Bulk/Interrupt (CBI) and Control/Bulk (CB) transport layer *)
	CBITransport* = окласс(Base.StorageDriver);

		(**
		 * The Accept Device-Specific Command class-specific request is uses by the CBI Command Transport
		 * Protocol to send a command block from a host to a device.
		 * @param cmdLen Length of the command
		 * @param cmd Command
		 * @param timeout in milliseconds
		 * @return transport status of command block
		 *)
		проц  AcceptCommand(cmd : Usbdi.Buffer; cmdlen: размерМЗ; timeout : цел32) : Usbdi.Status;
		нач
			утв(длинаМассива(cmd) >= cmdlen);
			defaultPipe.SetTimeout(timeout);
			возврат device.Request(Usbdi.ToDevice + Usbdi.Class + Usbdi.Interface, 0, 0, interface.bInterfaceNumber, cmdlen, cmd);
		кон AcceptCommand;

		проц {перекрыта}Reset*(timeout : цел32) : целМЗ;
		перем buffer, interruptData : Usbdi.BufferPtr; status : Usbdi.Status; i : цел32;
		нач
			если Debug.Trace и Debug.traceScRequests то ЛогЯдра.пСтроку8("UsbStorageCbi: Sending CB/I reset ControlTransfer"); ЛогЯдра.пВК_ПС; всё;
			нов(buffer, 12);
			buffer[0] := симв8ИзКода(1DH);
			buffer[1] := симв8ИзКода(4);
			нцДля i := 2 до 11 делай buffer[i] := симв8ИзКода(255) кц;
			status := AcceptCommand(buffer^, 12, timeout);
			если (status = Usbdi.Disconnected) то
				возврат Base.ResDisconnected;
			аесли (status # Usbdi.Ok) то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorageCbi: Failure on TransportCB/I-Reset Control"); ЛогЯдра.пВК_ПС; всё;
				возврат Base.ResFatalError;
			всё;
			если transportMethod = Base.MethodCBI то
				если Debug.Trace и Debug.traceScRequests то ЛогЯдра.пСтроку8("UsbStorageCbi: Sending CB/I reset InterruptTransfer"); ЛогЯдра.пВК_ПС; всё;
				нов(interruptData, 8);
				interruptPipe.SetTimeout(timeout);
				status := interruptPipe.Transfer(2, 0, interruptData^);
				если status = Usbdi.Stalled то
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorageCbi: Stall on TransportCB/I-Reset Interrupt"); ЛогЯдра.пВК_ПС; всё;
					если ~interruptPipe.ClearHalt() то возврат Base.ResError всё;
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorageCbi: Failure on TransportCB/I-Reset clear halt on Interruptpipe"); ЛогЯдра.пВК_ПС; всё;
					возврат Base.ResFatalError;
				аесли status = Usbdi.InProgress то
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorageCbi: Timeout on TransportCB/I-Reset Interrupt"); ЛогЯдра.пВК_ПС; всё;
					возврат Base.ResTimeout;
				аесли status = Usbdi.Disconnected то
					возврат Base.ResDisconnected;
				аесли status # Usbdi.Ok то
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorageCbi: Failure on TransportCB/I-Reset Interrupt"); ЛогЯдра.пВК_ПС; всё;
					возврат Base.ResFatalError;
				всё;
			всё;
			(* After a Command Block Reset, the Stall condition and data toggle of the device's endpoints are undefined (2.2 in [1]) *)
			если ~bulkInPipe.ClearHalt() или ~bulkOutPipe.ClearHalt() то
				если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorageCbi: Failure on CB/I reset ClearHalt"); ЛогЯдра.пВК_ПС; всё;
				возврат Base.ResFatalError;
			всё;
			если Debug.Trace и Debug.traceScRequests то ЛогЯдра.пСтроку8("UsbStorageCbi: CB/I reset OK"); ЛогЯдра.пВК_ПС; всё;
			возврат Base.ResOk;
		кон Reset;

		проц {перекрыта}Transport*(cmd : массив из симв8; cmdlen : размерМЗ; dir : мнвоНаБитахМЗ;
			перем buffer : массив из симв8; ofs, bufferlen : размерМЗ; перем tlen : размерМЗ; timeout : цел32) : целМЗ;
		перем status : Usbdi.Status;	 interruptData : Usbdi.BufferPtr; blockStatus : цел32;
		нач
			если Debug.Trace и Debug.traceScTransfers то ЛогЯдра.пСтроку8("UsbStorageCbi: Sending TransportCB/I Control"); ЛогЯдра.пВК_ПС; всё;
			status := AcceptCommand(cmd, cmdlen, timeout);
			если status = Usbdi.Stalled то
				если Debug.Level >= Debug.Warnings то ЛогЯдра.пСтроку8("UsbStorageCbi: Stall on TransportCB/I Control"); ЛогЯдра.пВК_ПС; всё;
				возврат Base.ResError; (* sense device *)
			аесли status = Usbdi.InProgress то
				если Debug.Level >= Debug.Warnings то ЛогЯдра.пСтроку8("UsbStorageCbi: Timeout on TransportCB/I Control"); ЛогЯдра.пВК_ПС; всё;
				возврат Base.ResTimeout;
			аесли status = Usbdi.Disconnected то
				возврат Base.ResDisconnected;
			аесли status # Usbdi.Ok то
				если Debug.Level >= Debug.Warnings то
					ЛогЯдра.пСтроку8("UsbStorageCbi: Failure on TransportCB/I Control, status :"); ЛогЯдра.пЦел64(status, 0); ЛогЯдра.пВК_ПС;
				всё;
				возврат Base.ResError; (* sense device *)
			всё;

			если (bufferlen # 0) то

				если dir = Base.DataIn то
					если Debug.Trace и Debug.traceScTransfers то
						ЛогЯдра.пСтроку8("UsbStorageCbi: Get "); ЛогЯдра.пЦел64(bufferlen, 0); ЛогЯдра.пСтроку8(" bytes from device"); ЛогЯдра.пВК_ПС;
					всё;
					bulkInPipe.SetTimeout(timeout);
					status := bulkInPipe.Transfer(bufferlen, ofs, buffer);
					tlen := bulkInPipe.GetActLen();
				аесли dir = Base.DataOut то
					если Debug.Trace и Debug.traceScTransfers то
						ЛогЯдра.пСтроку8("UsbStorageCbi: Send "); ЛогЯдра.пЦел64(bufferlen, 0); ЛогЯдра.пСтроку8(" bytes to device"); ЛогЯдра.пВК_ПС;
					всё;
					bulkOutPipe.SetTimeout(timeout);
					status := bulkOutPipe.Transfer(bufferlen, ofs, buffer);
					tlen := bulkOutPipe.GetActLen();
				иначе СТОП(303);
				всё;

				если status = Usbdi.Stalled то
					если Debug.Level >= Debug.Warnings то ЛогЯдра.пСтроку8("UsbStorageCbi: Stall on TransportCB/I Bulk"); ЛогЯдра.пВК_ПС; всё;
					если ((dir = Base.DataIn) и ~bulkInPipe.ClearHalt()) или ((dir = Base.DataOut) и ~bulkOutPipe.ClearHalt()) то
						если Debug.Level >= Debug.Warnings то ЛогЯдра.пСтроку8("UsbStorage: Failure on TransportCB/I clear halt on Bulkpipe"); ЛогЯдра.пВК_ПС; всё;
						возврат Base.ResFatalError
					всё;
					возврат Base.ResError; (* sense device *)
				аесли status = Usbdi.InProgress то
					если Debug.Level >= Debug.Warnings то ЛогЯдра.пСтроку8("UsbStorageCbi: Timeout on TransportCB/I Bulk"); ЛогЯдра.пВК_ПС; всё;
					возврат Base.ResTimeout;
				аесли status = Usbdi.Disconnected то
					возврат Base.ResDisconnected;
				аесли status # Usbdi.Ok то
					если Debug.Level >= Debug.Warnings то ЛогЯдра.пСтроку8("UsbStorageCbi: Failure on TransportCB/I Bulk"); ЛогЯдра.пВК_ПС; всё;
					возврат Base.ResError; (* sense device *)
				всё;
			иначе
				tlen := 0;
			всё;

			если transportMethod = Base.MethodCBI то
				если Debug.Trace и Debug.traceScRequests то ЛогЯдра.пСтроку8("UsbStorageCbi: Sending TransportCB/I Interrupt"); ЛогЯдра.пВК_ПС; всё;

				нов(interruptData, 2);
				interruptPipe.SetTimeout(timeout);
				status := interruptPipe.Transfer(2, 0, interruptData^);

				если status = Usbdi.Stalled то
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorageCbi: Stall on TransportCB/I Interrupt"); ЛогЯдра.пВК_ПС; всё;
					если interruptPipe.ClearHalt() то возврат Base.ResError всё;
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorageCbi: Failure on TransportCB/I clear halt on Interruptpipe"); ЛогЯдра.пВК_ПС; всё;
					возврат Base.ResFatalError
				аесли status = Usbdi.InProgress то
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorageCbi: Timeout on TransportCB/I Interrupt"); ЛогЯдра.пВК_ПС; всё;
					возврат Base.ResTimeout;
				аесли status = Usbdi.Disconnected то
					возврат Base.ResDisconnected;
				аесли status # Usbdi.Ok то
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorageCbi: Failure on TransportCB/I Interrupt"); ЛогЯдра.пВК_ПС; всё;
					возврат Base.ResFatalError;
				всё;

				если (transportProtocol = Base.ProtocolUFI) то
					если (cmd[0] = 12X) или (cmd[0] = 03X) то
						(* UFI Inquiry + Sense do not change the sense data, so we cannot be sure that those commands succeded!!! *)
						(* just go on and hope the best! *)
					аесли (interruptData[0] # 0X) то
						если Debug.Level >= Debug.Errors то
							ЛогЯдра.пСтроку8("UsbStorageCbi: Error on CBI/UFI, asc = "); ЛогЯдра.п16ричное(кодСимв8(interruptData[0]), 0);
							ЛогЯдра.пСтроку8(" ascq = "); ЛогЯдра.п16ричное(кодСимв8(interruptData[1]), 0); ЛогЯдра.пВК_ПС;
						всё;
						возврат Base.ResSenseError; (* just retry *)
					всё;
					(* go on *)
				аесли interruptData[0] # 0X то
					если Debug.Level >= Debug.Errors то ЛогЯдра.пСтроку8("UsbStorageCbi: CBI returned invalid interupt data block"); ЛогЯдра.пВК_ПС; всё;
					возврат Base.ResSenseError; (* try to recover by manual sensing *)
				иначе
					(* Command completion interrupt. Error handling according 3.4.3.1.1. in [1] *)
					blockStatus := кодСимв8(interruptData[1]) остОтДеленияНа 4;
					просей blockStatus из
						|Pass: (* command status ok *)
						|Fail: возврат Base.ResError;
						|PhaseError: возврат Base.ResFatalError; (* reset device *)
						|PersistentFailure: возврат Base.ResSenseError; (* request sense *)
					иначе
						СТОП(99);
					всё;
				всё;
			всё;
			если tlen # bufferlen то возврат Base.ResShortTransfer; всё;
			возврат Base.ResOk;
		кон Transport;

		проц &{перекрыта}Init*;
		нач
			Init^;
		кон Init;

	кон CBITransport;

кон UsbStorageCbi.

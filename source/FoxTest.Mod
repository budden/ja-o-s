модуль FoxTest;	(** AUTHOR "fof"; PURPOSE "Fox tester"; *)
(* (c) fof ETH Zürich, 2008 *)

использует Basic := FoxBasic, TestSuite, Diagnostics, Потоки, Commands, Shell, Options, Files, Strings, Versioning, CompilerInterface, Texts, TextUtilities, Modules, ЛогЯдра;

тип
	Command = массив 256 из симв8;

	Tester = окласс (TestSuite.Tester)
	перем
		log: Потоки.Писарь;
		fileLog: Потоки.Писарь;

		mayTrap: булево;
		commandFlags: мнвоНаБитахМЗ;
		command, prolog, epilog: Command;
		fileName: Files.FileName;
		dots: цел32;

		проц &InitTester (l0log, logFileWriter: Потоки.Писарь;  l1diagnostics: Diagnostics.Diagnostics; l2mayTrap: булево; конст l3prolog, l4command, l5epilog: Command; конст l6fileName: массив из симв8);
		нач
			Init (l1diagnostics); сам.log := l0log; сам.mayTrap := l2mayTrap; сам.fileLog := logFileWriter;
			копируйСтрокуДо0(l3prolog, сам.prolog);
			копируйСтрокуДо0(l5epilog, сам.epilog);
			копируйСтрокуДо0(l4command, сам.command);
			копируйСтрокуДо0(l6fileName, сам.fileName);
			commandFlags := {Commands.Wait};
			если l0log = НУЛЬ то включиВоМнвоНаБитах(commandFlags, Commands.Silent) всё;
		кон InitTester;

		проц {перекрыта}Handle* (r: Потоки.Чтец; position: Потоки.ТипМестоВПотоке; конст name: массив из симв8; type: TestSuite.TestType): целМЗ;
		перем result: цел16; msg: массив 128 из симв8; res: целМЗ; f: Files.File; w: Files.Writer; ch: симв8;
		нач
			result := TestSuite.Failure;
			если log # НУЛЬ то log.пСтроку8 ("testing: "); log.пСтроку8 (name); log.пСтроку8("@"); log.пЦел64(position,0); log.пВК_ПС; log.ПротолкниБуферВПоток; всё;

			(* prepare tester input as a file for all test cases *)
			f := Files.New(fileName);
			нов(w,f,0);
			нцПока r.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() > 0 делай
				r.чСимв8(ch); w.пСимв8(ch)
			кц;
			w.ПротолкниБуферВПоток;
			Files.Register(f);

			если log = НУЛЬ то ЛогЯдра.пСимв8("."); увел(dots); если dots остОтДеленияНа 256 = 0 то ЛогЯдра.пВК_ПС всё; всё;

			res := Commands.Ok;
			если prolog # "" то
				Commands.Call(prolog, commandFlags, res, msg);
				если (res # Commands.Ok) и (log # НУЛЬ)  то
					log.пСтроку8("prolog failed: "); log.пСтроку8(msg); log.пВК_ПС;
				всё;
			всё;

			если (command # "") и (res = Commands.Ok) то
				Commands.Call(command,  commandFlags, res, msg);
				если res = Commands.Ok то
					result := TestSuite.Positive
				аесли (res < 3500) и (res >= 3440) то (* loader error *)
					result := TestSuite.Failure
				аесли ~mayTrap и (res = Commands.CommandTrapped) то (* command error, trap *)
					result := TestSuite.Failure
				иначе
					result := TestSuite.Negative
				всё;
				если (result # type) и (log # НУЛЬ) то
					log.пСтроку8 (msg); log.пВК_ПС;
				всё;
			аесли (command # "") то result := TestSuite.Failure
			всё;

			если epilog # "" то
				Commands.Call(epilog,  commandFlags, res, msg);
			всё;

			если fileLog # НУЛЬ то
				если result = type то
					fileLog.пСтроку8("success: ")
				иначе
					fileLog.пСтроку8("failure: ")
				всё;
				fileLog.пСтроку8(name); fileLog.пВК_ПС;
			всё;
		выходя
			возврат result;
		кон Handle;

	кон Tester;

	проц GetOptions(): Options.Options;
	перем options: Options.Options;
	нач
		нов(options);
		options.Add("p","prolog", Options.String);
		options.Add("e","epilog", Options.String);
		options.Add("c","command", Options.String);
		options.Add("v","verbose",Options.Flag);
		options.Add("t","mayTrap",Options.Flag);
		options.Add("f","fileName",Options.String);
		options.Add("l","logFile",Options.String);
		options.Add("r","result",Options.String);
		возврат options
	кон GetOptions;

	проц DriveTest (options: Options.Options; diagnostics: Diagnostics.Diagnostics; reader: Потоки.Чтец; error, writer: Потоки.Писарь): булево;
	перем
		tester: Tester; prolog, epilog, command: Command;
		verbose, mayTrap: булево; report: TestSuite.StreamReport; fileName, logFileName: Files.FileName; logFileWriter, log:Потоки.Писарь;
		testname, resultname: Files.FileName;
		baseOptions: Options.Options; ch: симв8; string: массив 256 из симв8; stringReader: Потоки.ЧтецИзСтроки;
	нач
		reader.ПерейдиКМестуВПотоке(0);
		нцПока reader.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() >0 делай
			reader.ПропустиБелоеПоле;
			reader.чСимв8(ch);
			если (ch = "#") то
				если reader.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(string) и Strings.StartsWith("options",0,string) то
					reader.чСтроку8ДоКонцаСтрокиТекстаВключительно(string);
					нов(stringReader, длинаМассива(string));
					stringReader.ПримиСтроку8ДляЧтения(string);
					baseOptions := GetOptions();
					если baseOptions.Parse(stringReader, error) то
						Options.Merge(options, baseOptions);
					иначе
						возврат ложь;
					всё;
				иначе
					reader.ПропустиДоКонцаСтрокиТекстаВключительно()
				всё;
			иначе
				reader.ПропустиДоКонцаСтрокиТекстаВключительно()
			всё;
		кц;

		если ~options.GetString("p", prolog) то prolog := "" всё;
		если ~options.GetString("c",  command) то command := "" всё;
		если ~options.GetString("e", epilog) то epilog := "" всё;
		если ~options.GetString("f", fileName) то fileName := "TesterInput.txt" всё;
		mayTrap := options.GetFlag("t");
		verbose := options.GetFlag("verbose");

		если options.GetString("l",logFileName) то
			ЛогЯдра.пСтроку8(logFileName);
			logFileWriter := Versioning.NewPoliteLogWriter(logFileName, "Test",testname);
			logFileWriter.пВК_ПС;
			logFileWriter.пСтроку8("prolog= "); logFileWriter.пСтроку8(prolog); logFileWriter.пВК_ПС;
			logFileWriter.пСтроку8("command= "); logFileWriter.пСтроку8(command); logFileWriter.пВК_ПС;
			logFileWriter.пВК_ПС;
		всё;
		если ~options.GetString("r",resultname) то resultname := "" всё;

		если verbose то log := writer иначе log := НУЛЬ всё;
		нов (tester, log, logFileWriter, diagnostics, mayTrap, prolog, command, epilog, fileName);

		нов (report, writer);
		reader.ПерейдиКМестуВПотоке(0);
		если ~TestSuite.DriveByReader(reader, error, resultname, tester) то возврат ложь всё;
		tester.Print (report);
		если logFileWriter # НУЛЬ то
			нов(report, logFileWriter);
			tester.Print(report);
			logFileWriter.ПротолкниБуферВПоток;
			writer.пСтроку8("testing logged in "); writer.пСтроку8(logFileName); writer.пВК_ПС;
		всё;
		writer.ПротолкниБуферВПоток;
		возврат истина (* report.failed = 0 *);
	кон DriveTest;

	проц Compile* (context: Commands.Context);
	перем writer: Потоки.Писарь; options: Options.Options;diagnostics: Diagnostics.StreamDiagnostics; testname: Files.FileName; test: Files.File;
		reader: Files.Reader;
	нач
		если (context.caller # НУЛЬ) и (context.caller суть Shell.Оболочка) то
			writer := context.out
		иначе
			writer := Basic.GetDebugWriter("Oberon Compiler Test Results")
		всё;
		options := GetOptions();
		если options.Parse(context.arg, context.error) то
			нов (diagnostics, writer);
			если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках (testname) то
				test := Files.Old (testname);
				если test = НУЛЬ то
					context.error.пСтроку8 ("Failed to open test file "); context.error.пСтроку8 (testname); context.error.пВК_ПС;
					возврат;
				всё;
			иначе
				context.result := Commands.CommandParseError;
			всё;
			Files.OpenReader(reader, test, 0);
			если ~DriveTest (options, diagnostics, reader, context.error, writer) то
				context.result := Commands.CommandError;
			всё;
		иначе
			context.result := Commands.CommandError;
		всё;
	кон Compile;

	проц GetTextReader(text: Texts.Text): Потоки.Чтец;
	перем
		buffer : укль на массив из симв8;
		length: размерМЗ; reader: Потоки.ЧтецИзСтроки;
	нач
		утв((text # НУЛЬ));
		text.AcquireRead;
		length := text.GetLength();
		text.ReleaseRead;
		если length = 0 то length := 1 всё;
		нов(buffer, length);
		TextUtilities.TextToStr(text, buffer^);
		(* prepare the reader *)
		нов(reader, длинаМассива(buffer)); reader.ПримиСрезСтроки8ВСтрокуДляЧтения(buffer^, 0, длинаМассива(buffer));
		возврат reader
	кон GetTextReader;

проц RunTests(
	text : Texts.Text;
	конст source: массив из симв8;
	pos: цел32; (* ignore *)
	конст pc,opt: массив из симв8;
	log: Потоки.Писарь; diagnostics : Diagnostics.Diagnostics; перем error: булево);
перем
	reader: Потоки.Чтец;
	options: Options.Options;
	optionReader: Потоки.ЧтецИзСтроки;
нач
	утв((text # НУЛЬ) и (diagnostics # НУЛЬ));
	reader := GetTextReader(text);
	options := GetOptions();
	нов(optionReader, длинаМассива(opt));
	optionReader.ПримиСтроку8ДляЧтения(opt);
	если options.Parse(optionReader, log) то
		error := ~DriveTest (options, diagnostics, reader, log, log);
	иначе
		error := истина;
	всё;
кон RunTests;

проц Cleanup;
нач
	CompilerInterface.Unregister("TestTool");
кон Cleanup;

нач
	CompilerInterface.Register("TestTool", "Run test cases against Fox compiler", "Test", RunTests);
	Modules.InstallTermHandler(Cleanup);
кон FoxTest.

System.Free FoxTest TestSuite Versioning ~

	FoxTest.Compile	Oberon.Execution.Test Oberon.Execution.AMD64TestDiff ~

	FoxTest.Compile	Oberon.Compilation.Test Oberon.Compilation.AMD64TestDiff ~

	FoxTest.Compile	MathVectors.Test MathVectors.Test.Diff ~

	FoxTest.Compile
		--verbose
		--fileName="TesterInput.Mod"
		--prolog="Compiler.Compile TesterInput.Mod"
		--command="System.Free Test Dummy B A;System.Load Test"
		--logFile="FoxExecutionTest.Log"
	MathVectors.Test MathVectors.Test.Diff ~

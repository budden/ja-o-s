модуль LisFrontend; (**  AUTHOR "fof"; PURPOSE "Oberon Compiler: Common frontend module";  **)

использует
	Потоки, Diagnostics,  SyntaxTree := LisSyntaxTree, Строки8;

конст 
	(* см. где используется - должно быть в ряду других флагов. Но пока какая-то дичь выходит *)
	расширяяМакросы*=25;

тип

	Frontend* = окласс
	перем
		flags-: мнвоНаБитахМЗ;
		defs-: Строки8.уСтрока;

		проц & InitFrontEnd*;
		нач
			Initialize(НУЛЬ, {}, НУЛЬ, "", "", "", 0);
		кон InitFrontEnd;

		(* initialize frontend for usage *)
		проц Initialize*(diagnostics: Diagnostics.Diagnostics; l0flags: мнвоНаБитахМЗ; reader: Потоки.Чтец;	
			конст fileName, definitions, вхДиректорияДляМакрорасширенногоФайла: массив из симв8; pos: цел32);
		нач
			сам.flags := l0flags;
			defs := Строки8.ЯвиУСтроку(definitions);
		кон Initialize;

		проц Parse*(): SyntaxTree.ДеревоМодуля;
		нач возврат НУЛЬ
		кон Parse;

		проц Done*(): булево;
		нач возврат истина;
		кон Done;

		проц Error*(): булево;
		нач возврат истина;
		кон Error;
		
	кон Frontend;

	проц GetDummy*():Frontend;
	перем frontend: Frontend;
	нач
		нов(frontend);
		возврат frontend;
	кон GetDummy;

	проц GetFrontendByName*(конст name: массив из симв8): Frontend;
	перем
		factory: проц (): Frontend;
		frontend: Frontend;
	нач
		frontend := НУЛЬ;
		если Строки8.КвоБайтБезЗавершающего0(name) > 0 то
			дайПроцПоИмени(name,"Get", factory); (* try long name for example -F=OCERAFrontend *)

			(* Отключаем это, потому что мы оберонщики и нам не нужен лишний способ решить задачу, 
			   сэкономив 7 букв, который к тому же усложняет анализ системы, и всё ради экономии 7 букв.
			IF factory = NIL THEN (* try short name for example -F=ERA*)
				procname := "Fox";
				Strings.Append(procname, name);
				Strings.Append(procname, "Frontend");
				GETPROCEDURE(procname,"Get", factory);
			END; *)

			если factory # НУЛЬ то
				frontend := factory();
				Assert(frontend # НУЛЬ,"Фабрика фронтэндов компилятора вернула NIL");
			всё;
		всё;
		возврат frontend
	кон GetFrontendByName;

	проц Assert(b: булево; конст reason: массив из симв8);
	нач
		утв(b);
	кон Assert;

кон LisFrontend.

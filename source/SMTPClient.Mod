модуль SMTPClient;
(** AUTHOR "TF"; PURPOSE "SMTP client for sending mail"; *)

(* SMTP RFC 821 client *)

использует
	Mail, IP, DNS, TCP, Потоки, ЛогЯдра;

конст
	Trace = ложь;
	MaxRecipients* = 20;

	Ok* = 0;
	NotConnected* = 1;
	SendFailed* = 101;
	TooManyRecipients* = 5001;

тип
	SMTPSession* = окласс(Mail.Sender)
	перем
		connection : TCP.Connection;
		sendReady, open : булево;
		r : Потоки.Чтец;
		w* : Потоки.Писарь;

		проц &Init*;
		нач sendReady := ложь; open := ложь
		кон Init;

		проц GetSendReady*():булево;
		нач возврат sendReady
		кон GetSendReady;

		проц GetReplyCode*(перем code: цел32; перем res: целМЗ);
		перем msg : массив 256 из симв8;
		нач
			r.чСтроку8ДоКонцаСтрокиТекстаВключительно(msg);
			code := кодСимв8(msg[0]) - кодСимв8("0"); code := code * 10 + кодСимв8(msg[1]) - кодСимв8("0"); code := code * 10 + кодСимв8(msg[2]) - кодСимв8("0");
			если Trace то ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8(msg); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования всё;
			нцПока (msg[3] = "-") и (r.кодВозвратаПоследнейОперации = Потоки.Успех) делай
				r.чСтроку8ДоКонцаСтрокиТекстаВключительно(msg);
				если Trace то ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8(msg); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования всё
			кц;
			если r.кодВозвратаПоследнейОперации = Потоки.Успех то res := Ok иначе res := r.кодВозвратаПоследнейОперации всё
		кон GetReplyCode;

		проц SendCommand*(конст cmd, arg : массив из симв8; перем res:целМЗ);
		нач
			если Trace то
				ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("CMD:"); ЛогЯдра.пСтроку8(cmd); ЛогЯдра.пСтроку8(" "); ЛогЯдра.пСтроку8(arg); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;
			w.пСтроку8(cmd); w.пСтроку8(" "); w.пСтроку8(arg); w.пВК_ПС; w.ПротолкниБуферВПоток;
			если w.кодВозвратаПоследнейОперации = Потоки.Успех то res := Ok иначе res := w.кодВозвратаПоследнейОперации всё
		кон SendCommand;

		проц Open*(конст server, thisHost : массив из симв8; port: цел32; перем result : целМЗ);
		перем fip : IP.Adr;
				res: целМЗ; reply : цел32;
		нач
			result := NotConnected;
			DNS.HostByName(server, fip, res);
			если res = DNS.Ok то
				нов(connection);
				connection.Open(TCP.NilPort, fip, port, res);
				если res = TCP.Ok то
					open := истина;
					Потоки.НастройЧтеца(r, connection.ПрочтиИзПотока);
					Потоки.НастройПисаря(w, connection.ЗапишиВПоток);
					GetReplyCode(reply, res);
					если (res = Потоки.Успех) и (reply >= 200) и (reply < 300) то
						SendCommand("HELO", thisHost, res);
						если res = Потоки.Успех то
							GetReplyCode(reply, res);
							если (res = Потоки.Успех) и (reply >= 200) и (reply < 300) то
								sendReady := истина;
								result := Ok
							всё
						всё
					иначе
						Close
					всё
				всё
			всё
		кон Open;

		проц Close*;
		перем res : целМЗ;
		нач
			если open то
				sendReady := ложь; open := ложь;
				SendCommand("QUIT", "", res);
				connection.Закрой
			всё
		кон Close;

		проц StartMailFrom*(конст fromAddr : массив из симв8) : булево;
		перем reply: цел32; res: целМЗ;
		нач
			w.пСтроку8("MAIL FROM:<");  w.пСтроку8(fromAddr); w.пСтроку8(">"); w.пВК_ПС; w.ПротолкниБуферВПоток;
			если w.кодВозвратаПоследнейОперации = Потоки.Успех то
				GetReplyCode(reply, res);
				возврат (res = Ok) и (reply = 250)
			иначе возврат ложь
			всё;
		кон StartMailFrom;

		проц SendTo*(конст toAddr : массив из симв8) :булево;
		перем reply: цел32; res: целМЗ;
		нач
			w.пСтроку8("RCPT TO:<");  w.пСтроку8(toAddr); w.пСтроку8(">"); w.пВК_ПС; w.ПротолкниБуферВПоток;
			если w.кодВозвратаПоследнейОперации = Потоки.Успех то
				GetReplyCode(reply, res);
				возврат (res = Ok) и (reply = 250)
			иначе возврат ложь
			всё;
		кон SendTo;

		проц StartData*() : булево;
		перем reply: цел32; res: целМЗ;
		нач
			SendCommand("DATA", "", res);
			если res = Ok то
				GetReplyCode(reply, res);
				возврат ((res = Ok) и (reply = 354))
			иначе возврат ложь
			всё
		кон StartData;

		проц PrepareToSend*(m: Mail.Message; перем result : цел32);
		перем name, address : Mail.MailAddress; i: размерМЗ;
		нач
			result := SendFailed;
			утв(m # НУЛЬ);
			(* FROM *)
			 m.GetFrom(name, address);
			(* TO *)
			 если StartMailFrom(address) то
				нцДля i := 0 до m.GetNofTo() - 1 делай
					m.GetTo(i, name, address); если ~SendTo(address) то Close; возврат всё
				кц;
				нцДля i := 0 до m.GetNofCc() - 1 делай
					m.GetCc(i, name, address); если ~SendTo(address) то Close; возврат всё
				кц;
				нцДля i := 0 до m.GetNofBcc() - 1 делай
					m.GetBcc(i, name, address); если ~SendTo(address) то Close; возврат всё
				кц;
			иначе Close; возврат
			всё;
			(* DATA *)
			если StartData() то result := Ok иначе Close всё;
		кон PrepareToSend;

		проц SendRawLine*(конст s : массив из симв8);
		нач
			w.пСтроку8(s); w.пВК_ПС
		кон SendRawLine;

		проц FinishSendRaw*() : булево;
		перем reply: цел32; res: целМЗ;
		нач
			w.ПротолкниБуферВПоток;
			GetReplyCode(reply, res);
			возврат (res = Ok) и (reply = 250)
		кон FinishSendRaw;

		проц SendComplete*(m: Mail.Message; перем result : цел32);
		перем i: размерМЗ;
			name, address : Mail.MailAddress;
			date, id : массив 64 из симв8;
			subject, content : массив 256 из симв8;
			l : Mail.Line;
		нач {единолично}
			PrepareToSend(m, result);
			если result = 0 то
				m.GetDate(date);
				если date # "" то w.пСтроку8("Date : "); w.пСтроку8(date); w.пВК_ПС всё;

				m.GetSubject(subject);
				если subject # "" то w.пСтроку8("Subject : "); w.пСтроку8(subject); w.пВК_ПС всё;

				m.GetFrom(name, address);
				w.пСтроку8("From:");
				если name # "" то
					w.пСтроку8(name); w.пСтроку8(" <");
					w.пСтроку8(address); w.пСтроку8(">");
				иначе
					w.пСтроку8(address);
				всё;
				w.пВК_ПС;

				m.GetSender(name, address);
				если address # "" то
					w.пСтроку8("Sender:");
					если name # "" то
						w.пСтроку8(name); w.пСтроку8(" <");
						w.пСтроку8(address); w.пСтроку8(">");
					иначе
						w.пСтроку8(address);
					всё;
					w.пВК_ПС
				всё;

				если m.GetNofReplyTo() > 0 то
					w.пСтроку8("Reply-To:");
					нцДля i := 0 до m.GetNofReplyTo() - 1 делай
						m.GetReplyTo(i, name, address);
						если name # "" то
							w.пСтроку8(name); w.пСтроку8(" <");
							w.пСтроку8(address); w.пСтроку8(">");
						иначе
							w.пСтроку8(address);
						всё;
						если i < m.GetNofReplyTo() - 1 то w.пСтроку8(",") всё;
						w.пВК_ПС;
					кц
				всё;

				w.пСтроку8("To:");
				нцДля i := 0 до m.GetNofTo() - 1 делай
					m.GetTo(i, name, address);
						w.пСимв8(" ");
					если name # "" то
						w.пСтроку8(name); w.пСтроку8(" <");
						w.пСтроку8(address); w.пСтроку8(">");
					иначе
						w.пСтроку8(address);
					всё;
					если i < m.GetNofTo() - 1 то w.пСтроку8(",") всё;
					w.пВК_ПС;
				кц;

				если m.GetNofCc() > 0 то
					w.пСтроку8("Cc:");
					нцДля i := 0 до m.GetNofCc() - 1 делай
						m.GetCc(i, name, address);
						w.пСимв8(" ");
						если name # "" то
							w.пСтроку8(name); w.пСтроку8(" <");
							w.пСтроку8(address); w.пСтроку8(">");
						иначе
							w.пСтроку8(address);
						всё;
						если i < m.GetNofCc() - 1 то w.пСтроку8(",") всё;
						w.пВК_ПС;
					кц
				всё;

				если m.GetNofBcc() > 0 то
					w.пСтроку8("Bcc:");
					нцДля i := 0 до m.GetNofBcc() - 1 делай
						m.GetBcc(i, name, address);
						w.пСимв8(" ");
						если name # "" то
							w.пСтроку8(name); w.пСтроку8(" <");
							w.пСтроку8(address); w.пСтроку8(">");
						иначе
							w.пСтроку8(address);
						всё;
						если i < m.GetNofBcc() - 1 то w.пСтроку8(",") всё;
						w.пВК_ПС;
					кц;
				всё;

				если m.GetNofHeaders() > 0 то
					нцДля i := 0 до m.GetNofHeaders() - 1 делай
						m.GetHeader(i, id, content);
						w.пСтроку8(id); w.пСтроку8(" : "); w.пСтроку8(content); w.пВК_ПС;
					кц;
				всё;

				w.пВК_ПС;

				нцДля i := 0 до m.GetNofLines() - 1 делай
					m.GetLine(i, l);
					если l.data # НУЛЬ то w.пСтроку8(l.data^) всё; w.пВК_ПС;
				кц;
				w.пВК_ПС; w.пСтроку8("."); w.пВК_ПС;
				если FinishSendRaw() то result := Ok всё
			всё;
		кон SendComplete;
	кон SMTPSession;

кон SMTPClient.

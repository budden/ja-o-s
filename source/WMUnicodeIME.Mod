модуль WMUnicodeIME; (** AUTHOR "tf, pl"; PURPOSE "Unicode input mode editor"; *)

использует
	ЛогЯдра, Modules, Strings, UTF8Strings,
	WMInputMethods, WMMessages,
	(* visual part *)
	WMRectangles, WMWindowManager, WMComponents, WMStandardComponents, WMEditors,
	WMGraphics;

конст
	imeName* = "Unicode";

тип
	IMEWindow*  = окласс (WMComponents.FormWindow)
	перем edit : WMEditors.Editor;
		curEditStr : массив 64 из симв8;
		uniChar : WMStandardComponents.Label;
		font : WMGraphics.Font;

		ime : IME;

		проц CreateForm(): WMComponents.VisualComponent;
		перем
			panel : WMStandardComponents.Panel;
			ep, sb, sr, gb, gr, d : WMStandardComponents.Panel;
		нач
			нов(panel); panel.bounds.SetExtents(200, 104); panel.fillColor.Set(0); panel.takesFocus.Set(истина);

			(* right shadow *)
			нов(sr); sr.bounds.SetWidth(4); sr.alignment.Set(WMComponents.AlignRight); sr.fillColor.Set(0);
			panel.AddContent(sr);

			нов(d); d.bounds.SetHeight(4); d.alignment.Set(WMComponents.AlignTop); d.fillColor.Set(0);
			sr.AddContent(d);

			нов(gr); gr.alignment.Set(WMComponents.AlignClient); gr.fillColor.Set(080H);
			sr.AddContent(gr);

			(* bottom shadow *)
			нов(sb); sb.bounds.SetHeight(4); sb.alignment.Set(WMComponents.AlignBottom); sb.fillColor.Set(0);
			panel.AddContent(sb);

			нов(d); d.bounds.SetWidth(4); d.alignment.Set(WMComponents.AlignLeft); d.fillColor.Set(0);
			sb.AddContent(d);

			нов(gb); gb.alignment.Set(WMComponents.AlignClient); gb.fillColor.Set(080H);
			sb.AddContent(gb);

			(* edit panel *)
			нов(ep); ep.alignment.Set(WMComponents.AlignClient); ep.fillColor.Set(цел32(0DDDD00EEH));
			panel.AddContent(ep);

			нов(edit); edit.bounds.SetHeight(20); edit.alignment.Set(WMComponents.AlignTop); edit.tv.showBorder.Set(истина);
			edit.tv.defaultTextBgColor.Set(0);

			edit.tv.borders.Set(WMRectangles.MakeRect(3, 3, 2, 2));
			edit.allowIME := ложь;
			edit.multiLine.Set(ложь);
			edit.tv.textAlignV.Set(WMGraphics.AlignCenter);
			ep.AddContent(edit);

			(* preview Unicode Char *)
			нов(uniChar); uniChar.alignment.Set(WMComponents.AlignClient);
			uniChar.textColor.Set(0000000FFH); uniChar.alignH.Set(WMGraphics.AlignCenter);
			uniChar.caption.SetAOC(""); uniChar.alignV.Set(WMGraphics.AlignTop);
			ep.AddContent(uniChar);

			возврат panel
		кон CreateForm;

		проц &New*(ime : IME; x, y :размерМЗ; конст text : массив из симв8);
		перем vc : WMComponents.VisualComponent;
		нач
			vc := CreateForm();
			сам.ime := ime;
			edit.onEnter.Add(Ok);
			edit.tv.SetExtKeyEventHandler(EditKeyPressed);

			Init(vc.bounds.GetWidth(), vc.bounds.GetHeight(), истина);
			SetContent(vc);

			font := WMGraphics.GetFont("Cyberbit", 60, {});
			если font # НУЛЬ то uniChar.SetFont(font) всё;

			manager := WMWindowManager.GetDefaultManager();
			manager.Add(x, y, сам, {});
			manager.SetFocus(сам);
			edit.SetAsString(text);
			edit.SetFocus;
			edit.text.onTextChanged.Add(TextChanged);
		кон New;

		проц EditKeyPressed(ucs : размерМЗ; flags : мнвоНаБитахМЗ; перем keySym : размерМЗ; перем handled : булево);
		нач
			handled := истина;
			если keySym = 20H то (* space *)
				если curEditStr = "" то ScheduleHide
				иначе WriteSelected;
					(* private change, dont need to evaluate anything *)
					edit.text.onTextChanged.Remove(TextChanged);
					edit.SetAsString("");
					curEditStr := "";
					edit.text.onTextChanged.Add(TextChanged);
					ClearSelection
				всё
			аесли keySym = 0FF08H то (* Inputs.KsBackSpace *)
				если curEditStr = "" то ScheduleHide
				иначе edit.KeyPressed(ucs, flags, keySym, handled)
				всё
			аесли keySym = 0FF54H то (* Inputs.DownArrow *)
			аесли (keySym >= 48) и (keySym <= 57) то
				edit.KeyPressed(ucs, flags, keySym, handled)
			аесли keySym = 0FF0DH то (* enter *)
				edit.KeyPressed(ucs, flags, keySym, handled)
			всё;
		кон EditKeyPressed;

		проц ScheduleHide;
		перем msg : WMMessages.Message;
		нач
			msg.msgType := WMMessages.MsgExt;
			msg.ext := сам;
			если ~sequencer.Add(msg) то ЛогЯдра.пСтроку8("IME Editor out of sync") всё;
		кон ScheduleHide;

		проц WriteSelected;
		перем uni : Strings.String;
		нач
			uni := uniChar.caption.Get();
			ime.InsertUTF8String(uni^)
		кон WriteSelected;

		проц ClearSelection;
		нач
			uniChar.caption.SetAOC("")
		кон ClearSelection;

		проц Ok*(sender, data:динамическиТипизированныйУкль);
		нач
			WriteSelected;
			ScheduleHide
		кон Ok;

		проц TextChanged*(sender, data:динамическиТипизированныйУкль);
		перем кодСимв: цел32; ch : симв32; no : размерМЗ; charString : массив 16 из симв8;

		нач
			(* avoid recursion *)
			edit.text.onTextChanged.Remove(TextChanged);

			(* find representation for the unicode *)
			edit.GetAsString(curEditStr);
			если Strings.Length(curEditStr) > 8 то
				uniChar.caption.SetAOC("overflow");
			иначе
				uniChar.caption.SetAOC("");
				Strings.StrToInt(curEditStr, кодСимв);
				ch := симв32ИзКода(кодСимв);

				no := 0;
				если UTF8Strings.EncodeChar(ch, charString, no) то
					uniChar.caption.SetAOC(charString)
				иначе
					uniChar.caption.SetAOC("error")
				всё;

			всё;

			edit.text.onTextChanged.Add(TextChanged)
		кон TextChanged;

		проц {перекрыта}FocusLost*;
		нач
			FocusLost^;
			ScheduleHide
		кон FocusLost;

		проц {перекрыта}FocusGot*;
		нач
			manager.SetFocus(сам)
		кон FocusGot;

		проц Hide;
		нач
			manager := WMWindowManager.GetDefaultManager();
			manager.Remove(сам);
			ime.w := НУЛЬ;
		кон Hide;

		проц {перекрыта}Handle*(перем x: WMMessages.Message);
		нач
			если (x.msgType = WMMessages.MsgExt) то
				если (x.ext = сам) то Hide
				всё
			иначе Handle^(x)
			всё
		кон Handle;

	кон IMEWindow;

тип
	IME* = окласс(WMInputMethods.IME)
	перем
		w : IMEWindow;

		проц {перекрыта}GetName*() : Strings.String;
		нач
			возврат Strings.NewString(imeName);
		кон GetName;

		проц {перекрыта}KeyEvent*(ucs : размерМЗ; flags : мнвоНаБитахМЗ; keysym : размерМЗ);
		перем x, y: размерМЗ; pos : размерМЗ; str : массив 8 из симв8;
		нач
			если (ucs >= 48) и (ucs <= 57) то
				GetCursorScreenPosition(x, y);
				pos := 0; если UTF8Strings.EncodeChar(симв32ИзКода(ucs), str, pos) то всё;
				нов(w, сам, x, y, str);
			иначе InsertChar(симв32ИзКода(ucs))
			всё
		кон KeyEvent;

		проц {перекрыта}Finalize*;
		кон Finalize;

	кон IME;

(* ----------------------------------------- *)

(* installs the Unicode IME *)
проц Install*;
перем ime : IME;
нач
	нов(ime);
	WMInputMethods.InstallIME(ime);
кон Install;

проц Cleanup;
нач
	если (WMInputMethods.defaultIME # НУЛЬ) и (WMInputMethods.defaultIME суть IME) то
		WMInputMethods.defaultIME(IME).Finalize
	всё;
	WMInputMethods.InstallIME(НУЛЬ)
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон WMUnicodeIME.

-------------------------------------------------

System.Free WMUnicodeIME ~
WMUnicodeIME.Install ~

(* Runtime support for awaiting interrupts *)
(* Copyright (C) Florian Negele *)


(** The Interrupts module provides a hardware-independent synchronisation primitive for awaiting the occurrence of interrupts. *)
(** Activities waiting for an interrupt are suspended and resumed as soon as the interrupt occurred. *)
модуль Interrupts;

использует Activities, Counters, CPU, Queues;

(** Represents an interrupt. *)
тип Interrupt* = запись
	timestamp: размерМЗ;
	index: размерМЗ;
кон;

перем timestamps: массив CPU.Interrupts из размерМЗ;
перем awaitingQueues: массив CPU.Interrupts из Queues.AlignedQueue;
перем previousHandlers: массив CPU.Interrupts из CPU.InterruptHandler;
перем processors: массив CPU.Interrupts из Activities.Activity;

(** Installs an interrupt to wait for. *)
(** The actual meaning of the specified interrupt number identifying the interrupt depends on the hardware. *)
проц Install- (перем interrupt: Interrupt; index: размерМЗ);
перем processor: Activities.Activity;
нач {безКооперации, безОбычныхДинПроверок}
	(* check for valid arguments *)
	утв (index < CPU.Interrupts);

	(* install the interrupt handler if necessary *)
	если сравнениеСОбменом (processors[index], НУЛЬ, НУЛЬ) = НУЛЬ то
		processor := Activities.CreateVirtualProcessor ();
		если сравнениеСОбменом (processors[index], НУЛЬ, processor) = НУЛЬ то
			previousHandlers[index] := CPU.InstallInterrupt (Handle, index);
		иначе
			удалиИзПамяти (processor);
		всё;
	всё;

	(* setup the interrupt *)
	interrupt.index := index;
	interrupt.timestamp := сравнениеСОбменом (timestamps[index], 0, 0);
кон Install;

(** Waits for the specified interrupt to occur. *)
(** This procedure returns as soon as the interrupt has to be handled, or if the wait was cancelled by a call to the Interrupts.Cancel procedure. *)
проц Await- (перем interrupt: Interrupt);
перем nextActivity: Activities.Activity;
нач {безКооперации, безОбычныхДинПроверок}
	(* check for valid argument *)
	утв (interrupt.index < CPU.Interrupts);

	(* check whether an interrupt was missed *)
	если сравнениеСОбменом (timestamps[interrupt.index], 0, 0) = interrupt.timestamp то

		(* this is a workaround for a buggy USB driver that sends too many interrupts *)
		если interrupt.index # CPU.IRQ0 (* timer interrupt *) то CPU.EnableIRQ(interrupt.index) всё;

		(* inform the scheduler that there is at least one activity awaiting interrupts *)
		Counters.Inc (Activities.awaiting);

		(* wait for the actual interrupt *)
		нцПока сравнениеСОбменом (timestamps[interrupt.index], 0, 0) = interrupt.timestamp делай
			нцДо кцПри Activities.Select (nextActivity, Activities.IdlePriority);
			Activities.SwitchTo (nextActivity, Enqueue, операцияАдресОт interrupt);
			Activities.FinalizeSwitch;
		кц;

		(* inform the scheduler that there is one less activity awaiting interrupts *)
		Counters.Dec (Activities.awaiting);

		(* notify next waiting activity *)
		NotifyNext (операцияАдресОт awaitingQueues[interrupt.index]);
	всё;

	(* update the local timestamp *)
	interrupt.timestamp := сравнениеСОбменом (timestamps[interrupt.index], 0, 0);
кон Await;

(* This procedure is a switch finalizer and is executed by a different activity. *)
проц Enqueue (previous {неОтслСборщиком}: Activities.Activity; interrupt {неОтслСборщиком}: укль {опасныйДоступКПамяти} на Interrupt);
перем item: Queues.Item; index, timestamp: размерМЗ;
нач {безКооперации, безОбычныхДинПроверок}
	index := interrupt.index; timestamp := interrupt.timestamp;
	Queues.Enqueue (previous, awaitingQueues[index]);
	если сравнениеСОбменом (timestamps[index], 0, 0) = timestamp то возврат всё;
	нцПока Queues.Dequeue (item, awaitingQueues[index]) делай
		Activities.Resume (item(Activities.Activity));
	кц;
кон Enqueue;

(* Handler for a specific interrupt. *)
проц Handle (index: размерМЗ);
нач {безКооперации, безОбычныхДинПроверок}
	(* forward the interrupt *)
	если previousHandlers[index] # НУЛЬ то previousHandlers[index] (index) всё;

	(* update the global timestamp *)
	увел (timestamps[index]);

	(* this is a workaround for a buggy USB driver that sends too many interrupts *)
	если index # CPU.IRQ0 (* timer interrupt *) то CPU.DisableIRQ(index) всё;

	(* this handler might have interrupted the execution of code in an uncooperative block *)
	(* executing the very same code more than once is semantically only valid when done by a different processor *)
	(* the same effect can be achieved if the current activity temporarily pretends to be executed by a different processor *)
	Activities.CallVirtual (NotifyNext, операцияАдресОт awaitingQueues[index], processors[index]);
кон Handle;

(* Notifies the next activity in the awaiting queue of an interrupt. *)
проц NotifyNext (awaitingQueue {неОтслСборщиком}: укль {опасныйДоступКПамяти} на Queues.Queue);
перем item: Queues.Item;
нач {безКооперации, безОбычныхДинПроверок}
	если Queues.Dequeue (item, awaitingQueue^) то
		Activities.Resume (item(Activities.Activity));
	всё;
кон NotifyNext

(** Resume all activities that are waiting for the specified interrupt. *)
проц Cancel- (перем interrupt: Interrupt);
перем item: Queues.Item;
нач {безКооперации, безОбычныхДинПроверок}
	утв (interrupt.index < CPU.Interrupts);
	interrupt.timestamp := сравнениеСОбменом (timestamps[interrupt.index], 0, 0) - 1;
	нцПока Queues.Dequeue (item, awaitingQueues[interrupt.index]) делай
		Activities.Resume (item(Activities.Activity));
	кц;
кон Cancel;

(** Terminates the module and disposes all resources associated with interrupts. *)
(** @topic Runtime Call *)
проц Terminate-;
перем index: размерМЗ;
нач {безКооперации, безОбычныхДинПроверок}
	нцДля index := 0 до CPU.Interrupts - 1 делай
		CPU.DisableInterrupt (index);
		Queues.Dispose (awaitingQueues[index]);
		если processors[index] # НУЛЬ то удалиИзПамяти (processors[index]) всё;
	кц;
кон Terminate;

кон Interrupts.

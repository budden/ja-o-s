модуль TeletextViewer;	(** AUTHOR "oljeger@student.ethz.ch"; PURPOSE "Aos Viewer for teletext pages"; *)

использует
	Modules, Standard := WMStandardComponents,
	WM := WMWindowManager, Base := WMComponents, Messages := WMMessages, Graphics := WMGraphics,
	Editor:= WMEditors, Dates, Строки8, TextUtilities, Texts, TVChannels, TeletextDecoder,
	TeletextFont, TeletextBrowser, XML, WMRestorable, WMTextView;

конст
	Width = 300;
	ButtonWidth = 100;
	ButtonHeight = 30;
	ButtonsPerRow = 3;

	ModeNoRefresh = 0;
	ModeFastRefreshSame = 1;
	ModeSlowCycleSubs = 2;

	CaptionNoRefresh = "auto-refresh is off";
	CaptionFastRefreshSame = "immediate refresh";
	CaptionSlowCycleSubs = "30 sec. subpage cycle";

тип
	(** Extension of the generic TeletextPage type *)
	TeletextPage = окласс(TeletextBrowser.TeletextPage)
	перем
		text*: Texts.Text
	кон TeletextPage;

	(** Adapted browser for Aos Teletext view *)
	Browser = окласс(TeletextBrowser.TeletextBrowser)
		перем
			content: TeletextPage;
			colors : массив 8 из цел32;

		проц &{перекрыта}Init*(suite: TeletextDecoder.TeletextSuite);
		нач
			Init^(suite);
			нов(content);
			loadProc := LoadPage;
			colors[0] := цел32(0FF0000FFH);	(* Red *)
			colors[1] := 0FF00FFH;		(* Green *)
			colors[2] := 0FFFFH;			(* Blue *)
			colors[3] := цел32(0FFFF00FFH);	(*Yellow *)
			colors[4] := цел32(0FF00FFFFH);	(* Magenta *)
			colors[5] := 0FFFFFFH;		(* Cyan *)
			colors[6] := цел32(0FFFFFFFFH);	(* White *)
			colors[7] := 0FFH			   (* Black *)
		кон Init;

		(** Create an Texts.Text instance with colored text *)
		проц LoadPage() : TeletextBrowser.TeletextPage;
		перем
			attr, fg, bg : цел8;
			boxed : булево;
			i, begin, pos : размерМЗ;
			nl: массив 3 из симв32;
			attributes: Texts.Attributes;
			text : Texts.Text;
		нач
			если content = НУЛЬ то
				нов(content);
				pgData := НУЛЬ
			всё;

			(* Display an alternative page if the requested page is not in cache *)
			если pgData = НУЛЬ то
				нов(content.text);
				text := content.text;
				text.AcquireWrite;
				TextUtilities.StrToText(text, 0, "The requested page is not available.");
				(* Yellow text *)
				нов(attributes);
				attributes.color := цел32(0FFFF00FFH);
				attributes.bgcolor := 0FFH;
				pos := text.GetLength();
				text.SetAttributes(0, pos, attributes);
				nl[0] := Texts.NewLineChar;
				nl[1] := Texts.NewLineChar;
				nl[2] := Texts.Симв32Нулевой; 
				text.InsertUCS32(pos, nl);
				pos := text.GetLength();
				TextUtilities.StrToText(text, pos, "Try another page number.");
				(* White text *)
				нов(attributes);
				attributes.color := цел32(0FFFFFFFFH);
				attributes.bgcolor := 0FFH;
				text.SetAttributes(pos, 24, attributes);
				text.ReleaseWrite;
				возврат content
			всё;

			(* Markup real teletext page *)
			нов(content.text);
			text := content.text;
			text.AcquireWrite;
			pgData.text.AcquireRead;
			text.CopyFromText(pgData.text, 0, pgData.text.GetLength(), 0);
			pgData.text.ReleaseRead;
			text.ReleaseWrite;

			(* Extract attributes and apply them to the text *)
			begin := 0;
			attr := 52;	(* Compressed form of: fg = White, bg = Black, flashing = FALSE *)
			нцДля i := 0 до 41*24-1 делай
				если (pgData.attributes[i] # attr) или (i = 41*24-1) то
					(* The 'boxed' flag is coded by the sign bit *)
					boxed := attr < 0;
					attr := матМодуль(attr);
					(* Foreground is coded in the least 3 bits *)
					fg := attr остОтДеленияНа 8;
					bg := (attr DIV 8) остОтДеленияНа 8;
					нов(attributes);
					attributes.color := colors[fg];
					если transparent и ~boxed то
						attributes.bgcolor := 0H;
					иначе
						attributes.bgcolor := colors[bg];
					всё;
					text.AcquireWrite;
					text.SetAttributes (begin, i-begin, attributes);
					text.ReleaseWrite;
					attr := pgData.attributes[i];
					begin := i
				всё
			кц;

			(* Hide first line when in transparent mode *)
			если transparent то
				нов(attributes);
				attributes.color := 0H;
				attributes.bgcolor := 0H;
				text.AcquireWrite;
				text.SetAttributes (0, 41, attributes);
				text.ReleaseWrite
			всё;
			возврат content
		кон LoadPage;

	кон Browser;

	(** Channel selector window for Teletext *)
	ChannelSwitcher = окласс (Base.FormWindow)
		перем
			nofChannels: размерМЗ;
			buttons: укль на массив из Standard.Button;
			browser : Browser;
			viewer: TeletextViewer;

		проц &New *(browser : Browser; viewer: TeletextViewer);
		перем
			i: размерМЗ; left, top: размерМЗ;
			panel: Standard.Panel;
			panels: укль на массив из Standard.Panel;
			channel: TVChannels.TVChannel;
			chName : массив 33 из симв8;
			now : Dates.DateTime;
		нач
			сам.browser := browser;
			сам.viewer := viewer;
			nofChannels := TVChannels.channels.GetCount();
			(* add a panel *)
			нов (panel);
			panel.fillColor.Set(0FFH);
			если nofChannels остОтДеленияНа ButtonsPerRow = 0 то
				panel.bounds.SetHeight ((nofChannels DIV ButtonsPerRow)(цел32) * ButtonHeight)
			иначе
				panel.bounds.SetHeight ((nofChannels DIV ButtonsPerRow+1)(цел32) * ButtonHeight)
			всё;

			(* Create columns for the buttons *)
			нов (panels, ButtonsPerRow);
			нцДля i := 0 до ButtonsPerRow-1 делай
				нов(panels[i]);
				panels[i].alignment.Set (Base.AlignLeft);
				panels[i].bounds.SetWidth (ButtonWidth);
				panel.AddContent (panels[i])
			кц;

			(* Set the content width *)
			если ButtonsPerRow < 3 то
				panel.bounds.SetWidth (Width)
			иначе
				panel.bounds.SetWidth (длинаМассива(panels)(цел32) * ButtonWidth)
			всё;

			(* Create buttons for channel selection *)
			now := Dates.Now();
			нов (buttons, nofChannels);
			нцДля i := 0 до nofChannels-1 делай
				нов (buttons[i]);
				buttons[i].bounds.SetHeight (ButtonHeight);
				buttons[i].alignment.Set (Base.AlignTop);
				channel := TVChannels.channels.GetItem(i);
				копируйСтрокуДо0(channel.name, chName);
				buttons[i].caption.SetAOC (chName);
				если ~channel.HasRecentData() то
					buttons[i].clTextDefault.Set(0747200FFH)
				всё;
				buttons[i].onClick.Add (OnPushButton);
				panels[i остОтДеленияНа ButtonsPerRow].AddContent (buttons[i])
			кц;

			(* create the form window with panel size *)
			Init(panel.bounds.GetWidth(), panel.bounds.GetHeight(), истина);
			SetContent(panel);

			(* open the window *)
			manager := WM.GetDefaultManager();
			SetTitle(WM.NewString("Teletext Channel Selector"));
			left := viewer.bounds.l + viewer.GetWidth() + 30;
			top := viewer.bounds.t;
			manager.Add(left, top, сам, {WM.FlagFrame});
		кон New;

		(** Determine which button has been pressed for correct action *)
		проц FindButton (button: Standard.Button): размерМЗ;
		перем i: размерМЗ;
		нач
			i := 0;
			нцПока (i < длинаМассива(buttons)) и (buttons[i] # button) делай
				увел(i)
			кц;
			возврат i
		кон FindButton;

		(** Switch channel and show index page *)
		проц OnPushButton (sender, data: динамическиТипизированныйУкль);
		перем
			button: Standard.Button;
			buttonNo: размерМЗ;
			channel: TVChannels.TVChannel;
		нач
			button := sender(Standard.Button);
			buttonNo := FindButton(button);
			channel := TVChannels.channels.GetItem(buttonNo);
			browser.SetSuiteFromFreq(channel.freq);
			viewer.ShowPage(browser.GetPage(100))
		кон OnPushButton;

		(** Close the channel selector *)
		проц {перекрыта}Close;
		нач
			viewer.channelSwitcher := НУЛЬ;
			Close^
		кон Close;

	кон ChannelSwitcher;

	(** Teletext viewer window *)
	TeletextViewer* = окласс (Base.FormWindow)
	перем
		panel, magazines, navi : Standard.Panel;
		back, forward, prevSub, nextSub, refresh, transparent, channel, refreshMode : Standard.Button;
		refreshLabel: Standard.Label;
		timer : Standard.Timer;
		mag : массив 8 из Standard.Button;
		nrEditor, teleText: Editor.Editor;
		browser-: Browser;
		channelSwitcher : ChannelSwitcher;
		l, t : размерМЗ; refrMode : цел32;
		next: TeletextViewer;

		проц &New*;
		перем
			i : цел32;
			magCaption : массив 4 из симв8;
			ch : TVChannels.TVChannel;
			suite : TeletextDecoder.TeletextSuite;
		нач
			(* create the background panel *)
			нов(panel);
			panel.bounds.SetWidth(700); panel.bounds.SetHeight(510);

			(* create a toolbar for navigation *)
			нов(navi); navi.bounds.SetHeight(25); navi.alignment.Set(Base.AlignTop);

			(* back button *)
			нов(back); back.bounds.SetHeight(25); back.alignment.Set(Base.AlignLeft);
			back.bounds.SetWidth(30);
			back.imageName.SetAOC("prev.png");
			back.onClick.Add(OnBack);
			navi.AddContent(back);

			(* add an editor for the page number *)
			нов(nrEditor);
			nrEditor.bounds.SetWidth(60);
			nrEditor.alignment.Set(Base.AlignLeft); nrEditor.multiLine.Set(ложь);
			nrEditor.tv.textAlignV.Set(Graphics.AlignCenter);
			nrEditor.onEnter.Add(OnLoad);
			navi.AddContent(nrEditor);

			(* forward button *)
			нов(forward); forward.bounds.SetHeight(25); forward.alignment.Set(Base.AlignLeft);
			forward.bounds.SetWidth(30);
			forward.imageName.SetAOC("next.png");
			forward.onClick.Add(OnForward);
			navi.AddContent(forward);

			(* previous subpage button *)
			нов(prevSub); prevSub.bounds.SetHeight(25); prevSub.alignment.Set(Base.AlignLeft);
			prevSub.bounds.SetWidth(50); prevSub.caption.SetAOC("sub -");
			prevSub.onClick.Add(OnPrevSub);
			navi.AddContent(prevSub);

			(* next subpage button *)
			нов(nextSub); nextSub.bounds.SetHeight(25); nextSub.alignment.Set(Base.AlignLeft);
			nextSub.bounds.SetWidth(50); nextSub.caption.SetAOC("sub +");
			nextSub.onClick.Add(OnNextSub);
			navi.AddContent(nextSub);

			(* refresh button *)
			нов(refresh); refresh.bounds.SetHeight(25); refresh.alignment.Set(Base.AlignLeft);
			refresh.bounds.SetWidth(75); refresh.caption.SetAOC("   Refresh");
			refresh.imageName.SetAOC("refresh.png");
			refresh.onClick.Add(OnRefresh);
			navi.AddContent(refresh);

			(* transparent button *)
			нов(transparent); transparent.alignment.Set(Base.AlignLeft);
			transparent.bounds.SetWidth(90); transparent.caption.SetAOC("Transparent");
			transparent.onClick.Add(OnTransp);
			navi.AddContent(transparent);

			(* refresh mode button *)
			нов(refreshMode); refreshMode.bounds.SetHeight(25); refreshMode.alignment.Set(Base.AlignLeft);
			refreshMode.bounds.SetWidth(100); refreshMode.caption.SetAOC("Refresh mode");
			refreshMode.onClick.Add(OnRefreshMode);
			navi.AddContent(refreshMode);

			(* refresh mode label *)
			нов(refreshLabel); refreshLabel.bounds.SetHeight(25); refreshLabel.alignment.Set(Base.AlignLeft);
			refreshLabel.bounds.SetWidth(125); refreshLabel.caption.SetAOC(CaptionNoRefresh);
			refreshLabel.fillColor.Set(053FFH); refreshLabel.textColor.Set(0FFFF00FFH);
			refreshLabel.alignH.Set(Graphics.AlignCenter);
			navi.AddContent(refreshLabel);

			refrMode := ModeNoRefresh;

			(* channel switch button *)
			нов(channel); channel.bounds.SetHeight(25); channel.alignment.Set(Base.AlignLeft);
			channel.bounds.SetWidth(90); channel.caption.SetAOC("TV Channels");
			channel.onClick.Add(OnChannelSwitch);
			navi.AddContent(channel);

			panel.AddContent(navi);

			(* add a toolbar for magazine pages (n*100) *)
			нов(magazines); magazines.bounds.SetHeight(300); magazines.alignment.Set(Base.AlignLeft);
			magazines.bounds.SetWidth(120);
			magazines.fillColor.Set (0H);

			(* magazine buttons *)
			magCaption := "000";
			нцДля i := 0 до 7 делай
				нов(mag[i]);
				mag[i].bounds.SetHeight(25);
				mag[i].alignment.Set(Base.AlignTop);
				magCaption[0] := симв8ИзКода((кодСимв8('1') + i));
				mag[i].caption.SetAOC(magCaption);
				mag[i].clDefault.Set(009933AFH);
				mag[i].onClick.Add (OnMagClick);
				magazines.AddContent(mag[i]);
			кц;

			panel.AddContent(magazines);

			(* add viewer for the teletext data *)
			нов(teleText);
			teleText.alignment.Set(Base.AlignClient);
			teleText.tv.wrapMode.Set(WMTextView.NoWrap);
			teleText.allowScrollbars.Set(ложь);
			panel.AddContent(teleText);

			(* create the form window with panel size *)
			Init(panel.bounds.GetWidth(), panel.bounds.GetHeight(), истина);
			SetContent(panel);
			teleText.tv.SetFont(TeletextFont.bimbofont);
			teleText.fillColor.Set(0H);
			panel.fillColor.Set(0FFH);

			(* open the window *)
			manager := WM.GetDefaultManager();
			SetTitle(WM.NewString("Teletext - No data available"));

			l := 100; t := 100;
			manager.Add(l, t, сам, {WM.FlagFrame});

			(* Switch to first available TV channel *)
			ch := TVChannels.channels.GetItem(0);
			если ch # НУЛЬ то
				suite := TeletextDecoder.SelectTeletextSuite(ch.freq);
				нов(browser, suite);
				ShowPage(browser.GetPage(100))
			всё;

			нов(timer);

			next := window;
			window := сам
		кон New;

		(** Handle window messages *)
		проц {перекрыта}Handle(перем m : Messages.Message);
		перем
			data: XML.Element;
			str: массив 10 из симв8;
		нач
			если (m.msgType = Messages.MsgExt) и (m.ext # НУЛЬ) то
				если (m.ext суть WMRestorable.Storage) то
					нов(data);  data.SetName("TeletextViewerData");
					Строки8.ПишиЦел64_вСтроку(browser.suite.channel.freq, str);
					data.SetAttributeValue("tvFreq", str);
					Строки8.ПишиЦел64_вСтроку(browser.page, str);
					data.SetAttributeValue("page", str);
					если channelSwitcher # НУЛЬ то
						data.SetAttributeValue("switchWindow", "true")
					иначе
						data.SetAttributeValue("switchWindow", "false")
					всё;
					m.ext(WMRestorable.Storage).Add("TeletextViewer", "TeletextViewer.Restore", сам, data)
				иначе Handle^(m)
				всё
			иначе Handle^(m)
			всё
		кон Handle;

		(** Close the Viewer window *)
		проц {перекрыта}Close;
		нач
			если timer # НУЛЬ то
				timer.Stop(НУЛЬ, НУЛЬ)
			всё;
			если channelSwitcher # НУЛЬ то
				channelSwitcher.Close
			всё;
			FreeWindow(сам);
			Close^
		кон Close;

		(** Switch to the given TV frequency *)
		проц Switch*(tvFreq: цел32);
		нач
			browser.SetSuiteFromFreq(tvFreq);
			ShowPage(browser.GetPage(100))
		кон Switch;

		(** Switch to the given magazine (page X*100) *)
		проц OnMagClick (sender, data: динамическиТипизированныйУкль);
		перем
			button: Standard.Button;
			buttonNo, pageNo: размерМЗ;
		нач
			ClearNumberField;
			button := sender (Standard.Button);
			buttonNo := FindButton (button);
			pageNo := 100*buttonNo;
			ShowPage (browser.GetPage(pageNo + 100))
		кон OnMagClick;

		(** Find magazine button for correct action *)
		проц FindButton (button: Standard.Button): цел32;
		перем i: цел32;
		нач
			i := 0;
			нцПока (i < 8) и (mag[i] # button) делай
				увел(i)
			кц;
			возврат i
		кон FindButton;

		(** Display the teletext page on the screen *)
		проц ShowPage(pg : TeletextBrowser.TeletextPage);
		перем
			page: TeletextPage;
		нач
			если pg = НУЛЬ то
				SetText(НУЛЬ);
				возврат
			всё;
			page := pg(TeletextPage);
			SetText(page.text)
		кон ShowPage;

		(** Go back one page *)
		проц OnBack (sender, data : динамическиТипизированныйУкль);
		нач
			ClearNumberField;
			ResetTimer();
			ShowPage(browser.GetPreviousPage())
		кон OnBack;

		(** Go to the next page *)
		проц OnForward (sender, data : динамическиТипизированныйУкль);
		нач
			ClearNumberField;
			ResetTimer();
			ShowPage(browser.GetNextPage())
		кон OnForward;

		(** Go to the previous subpage *)
		проц OnPrevSub (sender, data : динамическиТипизированныйУкль);
		нач
			ClearNumberField;
			ResetTimer();
			ShowPage(browser.GetPreviousSubpage())
		кон OnPrevSub;

		(** Go to the next subpage *)
		проц OnNextSub (sender, data : динамическиТипизированныйУкль);
		нач
			ClearNumberField;
			ResetTimer();
			ShowPage(browser.GetNextSubpage())
		кон OnNextSub;

		(** Toggle window transparency mode *)
		проц OnTransp (sender, data : динамическиТипизированныйУкль);
		перем
			i: цел32;
		нач
			browser.transparent := ~browser.transparent;
			если browser.transparent то
				(* Set the window transparent *)
				transparent.caption.SetAOC("Opaque");
				нцДля i := 0 до 7 делай
					mag[i].clDefault.Set(0H);
					mag[i].clTextDefault.Set(0H);
					mag[i].clTextHover.Set(0FFH)
				кц;
				panel.fillColor.Set(0H);
				(* Assume subtitle mode => Enable automatic page refresh *)
				нцПока refrMode # ModeFastRefreshSame делай
					OnRefreshMode(НУЛЬ,НУЛЬ)
				кц;
				timer.Start(sender, data)
			иначе
				(* Set the window opaque *)
				transparent.caption.SetAOC("Transparent");
				нцДля i := 0 до 7 делай
					mag[i].clDefault.Set(009933AFH);
					mag[i].clTextDefault.Set(0FFFF00FFH);
					mag[i].clTextHover.Set(0FFFF00FFH);
				кц;
				panel.fillColor.Set(0FFH);
				timer.Stop(sender, data)
			всё;
			ShowPage(browser.ReloadPage())
		кон OnTransp;

		(** Load the page number which has been entered in the input field *)
		проц OnLoad (sender, data : динамическиТипизированныйУкль);
		перем
			pageNo: цел32;
			nr : массив 10 из симв8;
		нач
			ResetTimer();
			nrEditor.GetAsString(nr);
			Строки8.ПрочтиЦел32_изСтроки(nr, pageNo);
			ClearNumberField;
			ShowPage(browser.GetPage(pageNo))
		кон OnLoad;

		(** Reload the current page *)
		проц OnRefresh (sender, data : динамическиТипизированныйУкль);
		нач
			ResetTimer();
			ShowPage(browser.ReloadPage())
		кон OnRefresh;

		(** Change the refresh mode *)
		проц OnRefreshMode (sender, data : динамическиТипизированныйУкль);
		нач
			refrMode := (refrMode+1) остОтДеленияНа 3;
			просей refrMode из
				ModeNoRefresh:
					(* No automatic refresh *)
					timer.Stop(НУЛЬ, НУЛЬ);
					refreshLabel.caption.SetAOC(CaptionNoRefresh)
			|	ModeFastRefreshSame:
				(* Reload same page 2x per second. Used for subtitles etc. *)
				timer.interval.Set(500);
				если timer.onTimer.HasListeners() то
					timer.onTimer.Remove(OnNextSub)
				всё;
				timer.onTimer.Add(OnRefresh);
				timer.Start(НУЛЬ, НУЛЬ);
				refreshLabel.caption.SetAOC(CaptionFastRefreshSame)
			|	ModeSlowCycleSubs:
				(* Go to the next subpage after a delay of 30 seconds *)
				timer.interval.Set(30000);
				если timer.onTimer.HasListeners() то
					timer.onTimer.Remove(OnRefresh)
				всё;
				timer.onTimer.Add(OnNextSub);
				timer.Start(НУЛЬ, НУЛЬ);
				refreshLabel.caption.SetAOC(CaptionSlowCycleSubs)
			всё
		кон OnRefreshMode;

		(** Display the given text in the viewer *)
		проц SetText (text: Texts.Text);
		перем
			title: массив 50 из симв8;
			pnum: массив 10 из симв8;
		нач
			копируйСтрокуДо0(browser.channel, title);
			Строки8.ПодклейВСтрокуХвост (title, " Teletext - ");
			если text # НУЛЬ то
				Строки8.ПишиЦел64_вСтроку (browser.page+100, pnum);
				Строки8.ПодклейВСтрокуХвост (title, pnum);
				SetTitle(WM.NewString(title))
			иначе
				Строки8.ПодклейВСтрокуХвост (title, "<Page does not exist>");
				SetTitle(WM.NewString(title));
				нов(text)
			всё;
			teleText.Acquire;
			teleText.SetText(text);
			teleText.Release
		кон SetText;

		(** Clear the page number input field *)
		проц ClearNumberField;
		нач
			nrEditor.SetAsString("")
		кон ClearNumberField;

		(** Open the channel selector window *)
		проц OnChannelSwitch (sender, data : динамическиТипизированныйУкль);
		нач
			если channelSwitcher # НУЛЬ то
				manager := WM.GetDefaultManager();
				manager.ToFront(channelSwitcher);
				manager.SetWindowPos(channelSwitcher, l+GetWidth() + 30, t)
			иначе
				нов(channelSwitcher, browser, сам)
			всё
		кон OnChannelSwitch;

		(** Reset the timer for automatic page reloading *)
		проц ResetTimer;
		нач
			если timer.onTimer.HasListeners() то
				timer.Stop(НУЛЬ, НУЛЬ);
				timer.Start(НУЛЬ, НУЛЬ)
			всё
		кон ResetTimer;

		(** Overwrite standard 'Draw' routine: Move the channel selector window
			   together with the viewer window. *)
		проц {перекрыта}Draw*(canvas : Graphics.Canvas; w, h : размерМЗ; q : целМЗ);
		нач
			Draw^(canvas, w, h, q);
			если (channelSwitcher # НУЛЬ) и ((bounds.l # l) или (bounds.t # t)) то
				l := bounds.l;
				t := bounds.t;
				manager := WM.GetDefaultManager();
				manager.SetWindowPos(channelSwitcher, l+GetWidth() + 30, t)
			всё
		кон Draw;

	кон TeletextViewer;

перем window: TeletextViewer;

(* Remove the window from the internal list (Finalizer) *)
проц FreeWindow(wnd: TeletextViewer);
перем
	w: TeletextViewer;
нач
	если wnd = НУЛЬ то
		возврат
	аесли wnd = window то
		(* wnd is first list element *)
		window := window.next
	иначе
		w := window;
		нцПока (w # НУЛЬ) и (w.next # wnd) делай
			w := w.next
		кц;
		если w # НУЛЬ то
			(* wnd found: remove it from the list *)
			w.next := wnd.next
		всё
	всё;
кон FreeWindow;

(** Open a teletext viewer window *)
проц Open*;
перем
	w: TeletextViewer;
нач
	нов(w);
кон Open;

(** Restore the windows *)
проц Restore*(c : WMRestorable.Context);
перем
	w: TeletextViewer;
	manager: WM.WindowManager;
	xml: XML.Element;
	s: Строки8.уСтрока;
	freq, page: цел32;
	suite: TeletextDecoder.TeletextSuite;
нач
	нов(w);
	(* restore the desktop *)
	если c.appData # НУЛЬ то
		xml := c.appData(XML.Element);
		(* Read the TV frequency to restore the channel *)
		s := xml.GetAttributeValue("tvFreq");
		если s # НУЛЬ то
			Строки8.ПрочтиЦел32_изСтроки(s^, freq);
			suite := TeletextDecoder.SelectTeletextSuite(freq);
			если suite # НУЛЬ то
				w.browser.SetSuite(suite)
			всё;
		всё;
		(* Read the page number *)
		s := xml.GetAttributeValue("page");
		если s # НУЛЬ то
			Строки8.ПрочтиЦел32_изСтроки(s^, page);
			w.ShowPage(w.browser.GetPage(page+100));
		всё;
		(* Check if the channel selector window was open *)
		s := xml.GetAttributeValue("switchWindow");
		если s # НУЛЬ то
			если s^ = "true" то
				w.OnChannelSwitch(НУЛЬ, НУЛЬ)
			всё
		всё;
	всё;
	(* Show the restored window *)
	manager := WM.GetDefaultManager();
	manager.Remove(w);
	WMRestorable.AddByContext(w, c);
кон Restore;

(** Finalizer routine *)
проц Cleanup;
перем
	w: TeletextViewer;
нач
	w := window;
	нцПока w # НУЛЬ делай
		w.Close;
		w := w.next
	кц;
	window := НУЛЬ
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон TeletextViewer.

TeletextViewer.Open ~
System.Free TeletextViewer ~

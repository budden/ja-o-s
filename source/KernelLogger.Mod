модуль KernelLogger; (** AUTHOR "TF"; PURPOSE "Periodically copy kernel log buffer into text"; *)

использует
	ЛогЯдра, Texts, TextUtilities, Kernel, Modules;

конст
	BufSize = цел32(8192) * 8; (* Kernel buffer size *)
	DeleteSize= BufSize DIV 8;
	UpdateInterval = 200;	(* ms *)
	LocalBuf = цел32(4096) * 2;

	MaxLogSize = 4*BufSize;

тип
	(* periodically poll the kernel log buffer *)
	Logger = окласс(Modules.ЛХА)
	перем
		timer : Kernel.Timer;
		alive, dead, added : булево;
		buf : массив LocalBuf из симв8;
		bufPos : размерМЗ;
		ch : симв8;
		tw : TextUtilities.TextWriter;

		limitCounter, n: размерМЗ;

		проц &Open;
		нач
			ПримиВ_ЛХА(Modules.ключЛХА_имяОбъекта, Modules.ЯвиКонтейнерСтроки8("KernelLogger.Logger"));
			dead := ложь; alive := истина;
			нов(timer);
			нов(tw, kernelLog);
		кон Open;

		проц Close;
		нач {единолично}
			ЛогЯдра.ВпредьПишиВОсновнойБуферТрассировки;
			alive := ложь; timer.Wakeup;
			дождись(dead)
		кон Close;

		проц Get() : симв8;
		перем res : симв8;
		нач
			если (bufPos >= LocalBuf) или (buf[bufPos] = 0X) то
				bufPos := 0;
				ЛогЯдра.ДайСодержимоеБуфераТрассировки(buf)
			всё;
			res := buf[bufPos];
			увел(bufPos);
			возврат res
		кон Get;

	нач {активное}
		(* pre loading the fonts so traps can be displayed even when the disk is causing the trap *)
		tw.SetFontName("Courier"); tw.пСтроку8("Log started");
		tw.SetFontName("Oberon"); tw.пВК_ПС;
		нцПока alive делай
			bufPos := 0; added := ложь;
			нц
				ch := Get();
				если ch # 0X то
					если ch = 0EX то tw.SetFontName("Courier"); tw.SetFontColor(цел32(0800000FFH));
					аесли ch = 0FX то tw.SetFontName("Oberon"); tw.SetFontColor(0FFH);
					аесли ch = 0DX то (* ignore CR character - this approximates the CRLF -> LF *)
					иначе tw.пСимв8(ch); added := истина;
					всё;
				всё;	(* 0X (end), 0DX (CR), 0AX (LF), 0EX (FixedFont), 0FX (NormalFont) *)
				если (ch = 0X) или ~alive то прервиЦикл всё;

				увел(limitCounter);

				если limitCounter >= DeleteSize то
					kernelLog.AcquireWrite;
					n := kernelLog.GetLength();
					если n > MaxLogSize то
						kernelLog.Delete(0,n-MaxLogSize);
					всё;
					kernelLog.ReleaseWrite;
					limitCounter := 0;
				всё;
			кц;
			если added то
				tw.ПротолкниБуферВПоток;

				kernelLog.AcquireWrite;
				n := kernelLog.GetLength();
				если n > MaxLogSize+DeleteSize то
					kernelLog.Delete(0,n-MaxLogSize);
				всё;
				kernelLog.ReleaseWrite;
			всё;
			timer.Sleep(UpdateInterval);
		кц;
		нач {единолично} dead := истина кон;
	кон Logger;

перем
	logger : Logger;
	buf : укль на массив из симв8;
	kernelLog- : Texts.Text;

проц Start*;
конст OberonKernel = "Oberon-Kernel"; OberonSystem = "Oberon-System";
перем kernelLockOberon, kernelUnlockOberon, systemStopLog : проц;
нач {единолично}
	если logger # НУЛЬ то ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Logger already running! "); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования; возврат всё;

	ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Starting logger"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
	нов(buf, BufSize);
	если ~ЛогЯдра.ВпредьПишиВАльтернативныйБуферТрассировки(адресОт(buf[0]), длинаМассива(buf)) то
		(* Kill Oberon Logger *)
		если Modules.ModuleByName (OberonKernel) # НУЛЬ то
			дайПроцПоИмени (OberonKernel, "LockOberon", kernelLockOberon);
			дайПроцПоИмени (OberonKernel, "UnlockOberon", kernelUnlockOberon);
		всё;
		если Modules.ModuleByName (OberonSystem) # НУЛЬ то
			дайПроцПоИмени (OberonSystem, "StopLog", systemStopLog);
		всё;
		если (kernelLockOberon # НУЛЬ) и (kernelUnlockOberon # НУЛЬ) и (systemStopLog # НУЛЬ) то
			kernelLockOberon; systemStopLog; kernelUnlockOberon;
			ЛогЯдра.ВпредьПишиВОсновнойБуферТрассировки; 	если ЛогЯдра.ВпредьПишиВАльтернативныйБуферТрассировки(адресОт(buf[0]), длинаМассива(buf)) то
				ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Oberon KernelLog stopped. New buffer installed"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
			всё
		всё;
	всё;
	нов(logger);
кон Start;

проц Stop*;
нач {единолично}
	если logger # НУЛЬ то
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Stopping logger"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
		logger.Close; logger := НУЛЬ;
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Logger stopped"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
	всё;
кон Stop;

проц Cleanup;
нач
	если logger # НУЛЬ то
		ЛогЯдра.ВпредьПишиВОсновнойБуферТрассировки;
		logger.Close
	всё
кон Cleanup;

нач
	нов(kernelLog);
	Start;
	Modules.InstallTermHandler(Cleanup);
кон KernelLogger.

KernelLogger.Start ~
KernelLogger.Stop ~
System.Free WMKernelLog KernelLogger ~

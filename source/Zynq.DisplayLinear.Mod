модуль DisplayLinear;

использует НИЗКОУР, Displays, Plugins, ЭВМ, Kernel, Commands, Options,
	PsConfig, Channels, Video := AcStreamVideoOut, AcAxiDma, Трассировка;

конст
	MaxWidth = 1920;
	MaxHeight = 1080;

	CacheLineSize = 32; (* cache line size in bytes *)
	DmaBurstLen = 16;

	(*
		Video settings for 1024 x 768 @ 62 Hz
	*)
	Width* = 1024;
	Height* = 768;

	PlClkDiv0* = 10;
	PlClkDiv1* = 15;
	PlClkDiv2* = 3;

	HorizFrontPorch* = 24;
	HorizSyncWidth* = 136;
	HorizBackPorch* = 160;
	HorizSyncPolarity* = истина;

	VertFrontPorch* = 3;
	VertSyncWidth* = 6;
	VertBackPorch* = 29;
	VertSyncPolarity* = истина;

(*
	(*
		Video settings for 800 x 480 @ 65 Hz
	*)
	Width* = 800;
	Height* = 480;

	PlClkDiv0* = 10;
	PlClkDiv1* = 15*2;
	PlClkDiv2* = 3*2;

	HorizFrontPorch* = 40;
	HorizSyncWidth* = 48;
	HorizBackPorch* = 88;
	HorizSyncPolarity* = TRUE;

	VertFrontPorch* = 13;
	VertSyncWidth* = 3;
	VertBackPorch* = 32;
	VertSyncPolarity* = TRUE;
*)

	DefaultColor* = цел32(0FFFFFFFFH); (** the color of the booting screen  *)

тип

	Display = окласс(Displays.Display)
	конст
		Format = 4;

		(** Transfer a block of pixels in "raw" display format to (op = set) or from (op = get) the display.  Pixels in the rectangular area are transferred from left to right and top to bottom.  The pixels are transferred to or from "buf", starting at "ofs".  The line byte increment is "stride", which may be positive, negative or zero. *)
		проц {перекрыта}Transfer*(перем buf: массив из симв8; ofs, stride, x, y, w, h: размерМЗ; op: целМЗ);
		перем bufadr, buflow, bufhigh, dispadr,w0,b,d: адресВПамяти;
		нач

			если w > 0 то
				утв(fbadr # 0);
				bufadr := адресОт(buf[ofs]);
				dispadr := fbadr + y * fbstride + x * Format;
				если Displays.reverse то
					dispadr := fbadr + (height-y-1) * fbstride + (width-x-1) * Format;
				всё;

				утв((dispadr >= fbadr) и ((y+h-1)*fbstride + (x+w-1)*Format <=  fbsize));	(* display index check *)
				w := w * Format;	(* convert to bytes *)
				просей op из
					Displays.set:
						если Displays.reverse то
							нцПока h > 0 делай
								w0 := w DIV Format; b:= bufadr; d := dispadr;
								нцПока w0 > 0 делай
									НИЗКОУР.копируйПамять(b, d, Format);
									увел(b,Format);
									умень(d, Format);
									умень(w0);
								кц;
								увел(bufadr, stride); умень(dispadr, fbstride);
								умень(h)
							кц
						иначе
							w0 := w DIV Format;
							нцПока h > 0 делай
								Copy32(bufadr,dispadr,w0); (*SYSTEM.MOVE(bufadr, dispadr, w);*) ЭВМ.СбросьКешДанныхДляДиапазона(dispadr,w);
								увел(bufadr, stride); увел(dispadr, fbstride);
								умень(h)
							кц
						всё;
					|Displays.get:
						если Displays.reverse то
							buflow := адресОт(buf[0]); bufhigh := buflow + длинаМассива(buf);
							нцПока h > 0 делай
								утв((bufadr >= buflow) и (bufadr+w <= bufhigh));	(* index check *)
								w0 := w DIV Format; b:= bufadr; d := dispadr;
								нцПока w0 > 0 делай
									НИЗКОУР.копируйПамять(d, b, Format);
									увел(b,Format);
									умень(d, Format);
									умень(w0);
								кц;
								увел(bufadr, stride); умень(dispadr, fbstride);
								умень(h)
							кц;
						иначе
							buflow := адресОт(buf[0]); bufhigh := buflow + длинаМассива(buf);
							нцПока h > 0 делай
								утв((bufadr >= buflow) и (bufadr+w <= bufhigh));	(* index check *)
								НИЗКОУР.копируйПамять(dispadr, bufadr, w);
								увел(bufadr, stride); увел(dispadr, fbstride);
								умень(h)
							кц;
						всё;
					иначе (* skip *)
				всё
			всё
		кон Transfer;

	кон Display;

перем
	display: Display;
	vout: Video.Controller;

	pixelClock: вещ32; (* pixel clock in Hz *)

	buf: укль на массив из симв8;
	bufAddr: адресВПамяти;

	rCfgCmd, rCfgData: портОбменаДанными дляВывода;
	rStatus: портОбменаДанными в;
	videoCfg: портОбменаДанными дляВывода;
	rdma: AcAxiDma.ReadController;

	(*
		Reset programming logic

		polarity: reset signal polarity, TRUE for active high and FALSE for active low
	*)
	проц ResetPl(polarity: булево);
	перем res, t: цел32;
	нач
		если polarity то (* active high *)
			утв(PsConfig.SetPlResets({},res));
			t := Kernel.GetTicks();
			нцПока Kernel.GetTicks() - t < 1 делай кц;
			утв(PsConfig.SetPlResets({0..3},res));
		иначе (* active low *)
			утв(PsConfig.SetPlResets({0..3},res));
			t := Kernel.GetTicks();
			нцПока Kernel.GetTicks() - t < 1 делай кц;
			утв(PsConfig.SetPlResets({},res));
		всё;
	кон ResetPl;

	(*
		Setup clocks required for video streaming
	*)
	проц SetupClocks;
	перем
		res: цел32;
		freq: цел64;
	нач
		(*
			Setup DMA frequency
		*)
		freq := PsConfig.GetPllClockFrequency(PsConfig.IoPll,res);
		Трассировка.пСтроку8("IO PLL frequency is "); Трассировка.пЦел64(freq,0); Трассировка.пСтроку8(" Hz"); Трассировка.пВК_ПС;
		утв(PsConfig.SetPlResets({0,1,2,3},res));
		если PsConfig.SetPlClock(0,PsConfig.IoPll,PlClkDiv0,1,res) то
			Трассировка.пСтроку8("FPGA clock 0 frequency has been changed to "); Трассировка.пЦел64(PsConfig.GetPlClockFrequency(0,res),0); Трассировка.пСтроку8(" Hz"); Трассировка.пВК_ПС;
		иначе Трассировка.пСтроку8("Error while setting FPGA clock 0 frequency, res="); Трассировка.пЦел64(res,0); Трассировка.пВК_ПС;
		всё;

		(*
			Setup display clocks
		*)
		(* pixel clock *)
		pixelClock := вещ32(freq)/PlClkDiv1;
		если PsConfig.SetPlClock(1,PsConfig.IoPll,PlClkDiv1,1,res) то
			Трассировка.пСтроку8("FPGA clock 1 frequency has been changed to "); Трассировка.пЦел64(PsConfig.GetPlClockFrequency(1,res),0); Трассировка.пСтроку8(" Hz"); Трассировка.пВК_ПС;
		иначе Трассировка.пСтроку8("Error while setting FPGA clock 1 frequency, res="); Трассировка.пЦел64(res,0); Трассировка.пВК_ПС;
		всё;
		(* clock used for serialization *)
		если PsConfig.SetPlClock(2,PsConfig.IoPll,PlClkDiv2,1,res) то
			Трассировка.пСтроку8("FPGA clock 2 frequency has been changed to "); Трассировка.пЦел64(PsConfig.GetPlClockFrequency(2,res),0); Трассировка.пСтроку8(" Hz"); Трассировка.пВК_ПС;
		иначе Трассировка.пСтроку8("Error while setting FPGA clock 2 frequency, res="); Трассировка.пЦел64(res,0); Трассировка.пВК_ПС;
		всё;
	кон SetupClocks;

	проц Init;
	перем
		res: целМЗ;
		d: цел32;
	нач
		SetupClocks;

		(*
			Reset the programming logic
		*)
		ResetPl(ложь);

		(*
			Setup ActiveCells components ports
		*)
		утв(Channels.GetOutput(0,0, rCfgCmd));
		утв(Channels.GetOutput(0,1, rCfgData));
		утв(Channels.GetInput(0,0, rStatus));
		утв(Channels.GetOutput(0,2, videoCfg));

		(*
			Allocate frame buffer
		*)
		нов(buf,CacheLineSize*((MaxHeight*MaxWidth*4+CacheLineSize-1) DIV CacheLineSize)+CacheLineSize);
		bufAddr := адресОт(buf[0]);
		bufAddr := bufAddr + (CacheLineSize - bufAddr остОтДеленияНа CacheLineSize); (* align to cache line size boundary *)
		Трассировка.пСтроку8("DisplayLinear: bufAddr0="); Трассировка.п16ричное(адресОт(buf[0]),-8); Трассировка.пВК_ПС;
		Трассировка.пСтроку8("DisplayLinear: bufAddr="); Трассировка.п16ричное(bufAddr,-8); Трассировка.пВК_ПС;
		утв(bufAddr остОтДеленияНа CacheLineSize = 0);
		утв(адресОт(buf[длинаМассива(buf)-1]) >= bufAddr+MaxHeight*MaxWidth*4-1);

		(*
			Setup video streaming
		*)
		Video.InitController(vout,videoCfg,pixelClock);

		Video.SetHorizActiveSize(vout,Width);
		Video.SetHorizFrontPorch(vout,HorizFrontPorch);
		Video.SetHorizSyncWidth(vout,HorizSyncWidth);
		Video.SetHorizBackPorch(vout,HorizBackPorch);
		Video.SetHorizSyncPolarity(vout,HorizSyncPolarity);

		Video.SetVertActiveSize(vout,Height);
		Video.SetVertFrontPorch(vout,VertFrontPorch);
		Video.SetVertSyncWidth(vout,VertSyncWidth);
		Video.SetVertBackPorch(vout,VertBackPorch);
		Video.SetVertSyncPolarity(vout,VertSyncPolarity);

		(*
			Setup AXI DMA for transfering data from the frame buffer to the video output
		*)

		(* configure read channel of S_AXI_HP0 as a 32-bit interface *)
		d := НИЗКОУР.прочти32битаПоАдресу(0xF8008000); НИЗКОУР.запиши32битаПоАдресу(0xF8008000,НИЗКОУР.MSK(d,0xFFFFFFFE)+1);

		ЭВМ.ЗаполниДиапазонБайтовПредставлениемЦел32(bufAddr,Width*Height*4,DefaultColor); (* fill the framebuffer with the default color *)
		ЭВМ.СбросьКешДанныхДляДиапазона(bufAddr,Width*Height*4);

		нов(rdma,rCfgCmd,rCfgData,rStatus,4,16);
		rdma.SetBurstLen(DmaBurstLen);

		(* configure read DMA transfer *)
		rdma.SetAddr(bufAddr);
		rdma.SetCount(Width*Height);
		rdma.SetWrap(истина); (* recurring transfer *)

		(*
			Enable video output
		*)
		Video.Enable(vout,истина);
		rdma.Start;

		(*
			Install the display
		*)
		нов(display);
		display.width := Width;
		display.height := Height;
		display.offscreen := 0;
		display.format := 4;
		display.unit := 10000;
		display.InitFrameBuffer(bufAddr,Width*Height*4,Width*4);
		display.desc := "Linear framebuffer driver for Zynq";
		display.Update;
		Displays.registry.Add(display,res);
		утв(res = Plugins.Ok);
	кон Init;

	проц Install*(context: Commands.Context);
	перем options: Options.Options;
	нач
		если context # НУЛЬ то
			нов(options);
			options.Add("r", "reverse", Options.Flag);
			если options.Parse(context.arg, context.error) то
				если options.GetFlag("r") то Displays.Reverse() всё;
			всё;
		всё;
	кон Install;

	проц -Copy32(sadr: адресВПамяти; dadr: адресВПамяти; len: цел32);
	машКод
		LDR	R0, [SP, #dadr]
		LDR	R1, [SP, #len]
		LDR	R2, [SP, #sadr]
		MOV	R3, #0
	loop:
		CMP	R3, R1
		BGE	end
		LDR	R4, [R2, #0]
		STR	R4, [R0, #0]
		ADD	R0, R0, #4
		ADD	R2, R2, #4
		ADD	R3, R3, #1
		B loop
	end:
		ADD SP, SP, 12
	(*BEGIN
		SYSTEM.MOVE(sadr, dadr, 4*len)*)
	кон Copy32;

нач
	Init;
кон DisplayLinear.

System.DoCommands

WinDisks.Install I: RW ~
FSTools.Mount AOS AosFS PhysicalDrive4#3 ~

System.DoCommands

FSTools.CopyFiles -o WORK:/build/DisplayLinear.Gof => AOS:/DisplayLinear.Gof ~
FSTools.CopyFiles -o WORK:/build/DisplayLinear.Sym => AOS:/DisplayLinear.Sym ~

FSTools.Unmount AOS ~
WinDisks.Uninstall PhysicalDrive4 ~
~

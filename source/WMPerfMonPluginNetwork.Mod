модуль WMPerfMonPluginNetwork; (** AUTHOR "staubesv"; PURPOSE "Performance Monitor network performance plugin"; *)

использует
	WMPerfMonPlugins, Network, Plugins, Modules;

конст
	PluginName = "NetworkSpeed";
	ModuleName = "WMPerfMonPluginNetwork";

тип

	NetParameter* = укль на запись (WMPerfMonPlugins.Parameter);
		device* : Network.LinkDevice;
	кон;

тип

	NetworkSpeed* = окласс(WMPerfMonPlugins.Plugin)
	перем
		l : Network.LinkDevice;

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		перем ds : WMPerfMonPlugins.DatasetDescriptor;
		нач
			p.name := PluginName; p.description := "Network Send Performance";
			сам.l := p(NetParameter).device;
			WMPerfMonPlugins.GetNameDesc(l, p.devicename);
			p.modulename := ModuleName;
			p.autoMax := истина; p.perSecond := истина; p.showSum := истина; p.minDigits := 5;
			нов(ds, 3);
			ds[0].name := "Total"; включиВоМнвоНаБитах(ds[0].flags, WMPerfMonPlugins.Sum); ds[0].unit := "KB";
			ds[1].name := "Send"; ds[1].unit := "KB";
			ds[2].name := "Receive"; ds[2].unit := "KB";
			p.datasetDescriptor := ds;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		перем sentKB, receivedKB : вещ32;
		нач
			sentKB := l.sendCount / 1024;
			receivedKB := l.recvCount / 1024;
			dataset[0] := sentKB + receivedKB;
			dataset[1] := sentKB;
			dataset[2] := receivedKB;
		кон UpdateDataset;

	кон NetworkSpeed;

проц AddPlugin(dev :Network.LinkDevice);
перем ns : NetworkSpeed; npar : NetParameter;
нач {единолично}
	нов(npar); npar.device := dev; нов(ns, npar);
кон AddPlugin;

проц RemovePlugin(dev : Network.LinkDevice);
перем devicename : WMPerfMonPlugins.DeviceName;
нач {единолично}
	WMPerfMonPlugins.GetNameDesc(dev, devicename);
	WMPerfMonPlugins.updater.RemoveByName(PluginName, devicename);
кон RemovePlugin;

проц EventHandler(event : целМЗ; plugin : Plugins.Plugin);
нач
	если event = Plugins.EventAdd то
		AddPlugin(plugin (Network.LinkDevice))
	аесли event = Plugins.EventRemove то
		RemovePlugin(plugin (Network.LinkDevice));
	всё;
кон EventHandler;

проц InitPlugins;
перем table : Plugins.Table; i : размерМЗ; res: целМЗ;
нач
	Network.registry.AddEventHandler(EventHandler, res);
	Network.registry.GetAll(table);
	если table # НУЛЬ то нцДля i := 0 до длинаМассива(table)-1 делай AddPlugin(table[i] (Network.LinkDevice)); кц; всё;
кон InitPlugins;

проц Install*;
кон Install;

проц Cleanup;
перем res : целМЗ;
нач
	Network.registry.RemoveEventHandler(EventHandler, res);
	WMPerfMonPlugins.updater.RemoveByModuleName(ModuleName);
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
	InitPlugins;
кон WMPerfMonPluginNetwork.

WMPerfMonPluginNetwork.Install ~  System.Free WMPerfMonPluginNetwork ~

модуль WebBrowserComponents;	(** AUTHOR "Simon L. Keel"; PURPOSE "components used by the WebBrowser-modules"; *)

использует
	Strings, WMStandardComponents, WMGraphics, WMRectangles, WMEvents, WebHTTP, NewHTTPClient, Потоки, Files,
	Raster, Codecs, ЛогЯдра, WMComponents, WMTextView, Texts, TextUtilities, WMWindowManager,
	XML, SVG, SVGLoader, TCP;

конст
	verbose = истина;
	MaxHTTPConnections = 16;
	MaxHTTPConnectionPerServer = 3;

тип
	String = Strings.String;

	SVGPanel* = окласс(WMComponents.VisualComponent)
	перем
		img : SVG.Document;

		проц & New*(svg: XML.Element);
		нач
			Init;

			если verbose то ЛогЯдра.пСтроку8("---Rendering SVG... "); всё;

			img := SVGLoader.LoadSVGEmbedded(svg);

			если img # НУЛЬ то
				bounds.SetExtents(img.width, img.height);
				если verbose то ЛогЯдра.пСтроку8("done."); ЛогЯдра.пВК_ПС(); всё;
			иначе
				bounds.SetExtents(15, 15);
				если verbose то ЛогЯдра.пСтроку8("failed."); ЛогЯдра.пВК_ПС(); всё;
			всё
		кон New;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		перем
			w, h : размерМЗ;
		нач
			DrawBackground^(canvas);
			w := bounds.GetWidth();
			h := bounds.GetHeight();
			если img # НУЛЬ то
				canvas.ScaleImage(img, WMRectangles.MakeRect(0, 0, img.width, img.height), WMRectangles.MakeRect(0, 0, w, h), WMGraphics.ModeCopy, 2)
			иначе
				canvas.Line(0, 0, w-1, 0, 0FFH, WMGraphics.ModeSrcOverDst);
				canvas.Line(0, 0, 0, h-1, 0FFH, WMGraphics.ModeSrcOverDst);
				canvas.Line(0, h-1, w-1, h-1, 0FFH, WMGraphics.ModeSrcOverDst);
				canvas.Line(w-1, 0, w-1, h-1, 0FFH, WMGraphics.ModeSrcOverDst);
				canvas.Line(0, 0, w-1, h-1, 0FFH, WMGraphics.ModeSrcOverDst);
				canvas.Line(0, h-1, w-1, 0, 0FFH, WMGraphics.ModeSrcOverDst);
			всё;
		кон DrawBackground;

	кон SVGPanel;

	SVGLinkPanel* = окласс(SVGPanel)
	перем
		onClick : WMEvents.EventSource;
		msg : динамическиТипизированныйУкль;

		проц & Create*(svg: XML.Element; loadLink : WMEvents.EventListener; msg : динамическиТипизированныйУкль);
		нач
			New(svg);
			нов(onClick, сам, НУЛЬ, НУЛЬ, сам.StringToCompCommand);
			events.Add(onClick);
			onClick.Add(loadLink);
			сам.msg := msg;
			SetPointerInfo(manager.pointerLink);
		кон Create;

		проц {перекрыта}PointerDown*(x, y: размерМЗ; keys : мнвоНаБитахМЗ);
		нач
			onClick.Call(msg);
			PointerDown^(x, y, keys);
		кон PointerDown;
	кон SVGLinkPanel;

	(** just shows an image, stretches the image to fit to the property "bounds" (initial size = image size) *)
	StretchImagePanel* = окласс(WMComponents.VisualComponent)
	перем
		img : WMGraphics.Image;

		проц & New*(rc : ResourceConnection; url : String; x, y : размерМЗ);
		нач
			Init;
			если rc = НУЛЬ то
				rc := GetResourceConnection(url);
			всё;
			если (rc # НУЛЬ) и (rc.reader # НУЛЬ) то
				если verbose то ЛогЯдра.пСтроку8("---Loading StretchImagePanel. Url: "); ЛогЯдра.пСтроку8(rc.url^); ЛогЯдра.пСтроку8("... "); всё;
				если ~Strings.StartsWith2("image/", rc.mimeType^) то rc.mimeType := GetMimeType(rc.url^) всё;
				img := LoadImage(rc.reader, rc.mimeType^, rc.url^);
				если img # НУЛЬ то
					если (x < 0) или (y < 0) то
						x := img.width;
						y := img.height;
					всё;
					bounds.SetExtents(x, y);
				всё;
				если verbose то ЛогЯдра.пСтроку8("done."); ЛогЯдра.пВК_ПС(); всё;
			всё;
			если (rc = НУЛЬ) или (rc.reader = НУЛЬ) или (img = НУЛЬ) то
				fillColor.Set(0FFFFFFFFH);
				если (x < 0) или (y < 0) то
					x := 15;
					y := 15;
				всё;
				bounds.SetExtents(x, y);
			всё;
			если rc # НУЛЬ то 
				rc.Release() всё;
		кон New;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		перем
			w, h : размерМЗ;
		нач
			DrawBackground^(canvas);
			w := bounds.GetWidth();
			h := bounds.GetHeight();
			если img # НУЛЬ то
				canvas.ScaleImage(img, WMRectangles.MakeRect(0, 0, img.width, img.height), WMRectangles.MakeRect(0, 0, w, h), WMGraphics.ModeCopy, 2)
			иначе
				canvas.Line(0, 0, w-1, 0, 0FFH, WMGraphics.ModeSrcOverDst);
				canvas.Line(0, 0, 0, h-1, 0FFH, WMGraphics.ModeSrcOverDst);
				canvas.Line(0, h-1, w-1, h-1, 0FFH, WMGraphics.ModeSrcOverDst);
				canvas.Line(w-1, 0, w-1, h-1, 0FFH, WMGraphics.ModeSrcOverDst);
				canvas.Line(0, 0, w-1, h-1, 0FFH, WMGraphics.ModeSrcOverDst);
				canvas.Line(0, h-1, w-1, 0, 0FFH, WMGraphics.ModeSrcOverDst);
			всё;
		кон DrawBackground;

	кон StretchImagePanel;

	StretchImageLinkPanel* = окласс(StretchImagePanel)
	перем
		onClick : WMEvents.EventSource;
		msg : динамическиТипизированныйУкль;

		проц & Create*(rc : ResourceConnection; url : String; x, y : размерМЗ; loadLink : WMEvents.EventListener; msg : динамическиТипизированныйУкль);
		нач
			New(rc, url, x, y);
			нов(onClick, сам, НУЛЬ, НУЛЬ, сам.StringToCompCommand);
			events.Add(onClick);
			onClick.Add(loadLink);
			сам.msg := msg;
			SetPointerInfo(manager.pointerLink);
		кон Create;

		проц {перекрыта}PointerDown*(x, y: размерМЗ; keys : мнвоНаБитахМЗ);
		нач
			onClick.Call(msg);
			PointerDown^(x, y, keys);
		кон PointerDown;

	кон StretchImageLinkPanel;

	TileImagePanel* = окласс(WMComponents.VisualComponent)
	перем
		img : WMGraphics.Image;

		проц & New*(rc : ResourceConnection; url : String);
		нач
			Init;
			alignment.Set(WMComponents.AlignClient);
			если rc = НУЛЬ то
				rc := GetResourceConnection(url);
			всё;
			если (rc # НУЛЬ) и (rc.reader # НУЛЬ) то
				если verbose то ЛогЯдра.пСтроку8("---Loading TileImagePanel. Url: "); ЛогЯдра.пСтроку8(rc.url^); ЛогЯдра.пСтроку8("... "); всё;
				если ~Strings.StartsWith2("image/", rc.mimeType^) то rc.mimeType := GetMimeType(rc.url^) всё;
				img := LoadImage(rc.reader, rc.mimeType^, rc.url^);
				если verbose то ЛогЯдра.пСтроку8("done."); ЛогЯдра.пВК_ПС(); всё;
			всё;
			если rc # НУЛЬ то rc.Stop(); rc.Release() всё;
		кон New;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		перем
			w, h, i, j, wCnt, hCnt : размерМЗ;
		нач
			DrawBackground^(canvas);
			если img # НУЛЬ то
				w := bounds.GetWidth();
				h := bounds.GetHeight();
				wCnt := w DIV img.width;
				если (wCnt * img.width) # w то увел(wCnt); всё;
				hCnt := h DIV img.height;
				если (hCnt * img.height) # h то увел(hCnt); всё;
				нцДля i := 1 до wCnt делай
					нцДля j := 1 до hCnt делай
						canvas.DrawImage((i-1)*img.width, (j-1)*img.height, img, WMGraphics.ModeSrcOverDst);
					кц;
				кц;
			всё
		кон DrawBackground;

	кон TileImagePanel;

	HR* = окласс(WMComponents.VisualComponent)
	перем
		x : размерМЗ;

		проц & New*(x : размерМЗ);
		нач
			Init;
			ParentTvWidthChanged(x);
		кон New;

		проц ParentTvWidthChanged*(x : размерМЗ);
		нач
			умень(x, 30);
			если x < 10 то x := 10 всё;
			сам.x := x;
			bounds.SetExtents(x, 2);
		кон ParentTvWidthChanged;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		нач
			DrawBackground^(canvas);
			canvas.Line(0, 0, x-1, 0, цел32(0808080FFH), WMGraphics.ModeSrcOverDst);
			canvas.Line(0, 1, x-1, 1, цел32(0C0C0C0FFH), WMGraphics.ModeSrcOverDst);
		кон DrawBackground;
	кон HR;

	ShortText* = окласс(WMComponents.VisualComponent)
	перем
		textView : WMTextView.TextView;

		проц & New*(txt : массив из симв8);
		перем
			errorText : Texts.Text;
			textWriter : TextUtilities.TextWriter;
		нач
			Init;
			нов(textView);
			textView.alignment.Set(WMComponents.AlignClient);
			нов(errorText);
			нов(textWriter, errorText);
			textWriter.SetFontSize(20);
			textWriter.SetVerticalOffset(20);
			textWriter.пСтроку8(txt);
			textWriter.ПротолкниБуферВПоток;
			textView.SetText(errorText);
			AddContent(textView);
			alignment.Set(WMComponents.AlignClient);
		кон New;

	кон ShortText;

	TextPanel* = окласс(WMComponents.VisualComponent)

		проц & New*(rc : ResourceConnection; url : String);
		перем
			vScrollbar : WMStandardComponents.Scrollbar;
			hScrollbar : WMStandardComponents.Scrollbar;
			tv : WMTextView.TextView;
			isoDecoder : Codecs.TextDecoder;
			result : целМЗ;
		нач
			Init;
			takesFocus.Set(ложь);
			alignment.Set(WMComponents.AlignClient);
			нов(vScrollbar);
			vScrollbar.alignment.Set(WMComponents.AlignRight);
			AddContent(vScrollbar);
			нов(hScrollbar);
			hScrollbar.alignment.Set(WMComponents.AlignBottom);
			hScrollbar.vertical.Set(ложь);
			AddContent(hScrollbar);
			если rc = НУЛЬ то
				rc := GetResourceConnection(url);
			всё;
			если (rc # НУЛЬ) и (rc.reader # НУЛЬ) то
				если verbose то ЛогЯдра.пСтроку8("---Loading TextPanel. Url: "); ЛогЯдра.пСтроку8(rc.url^); ЛогЯдра.пСтроку8("... "); всё;
				нов(tv);
				tv.alignment.Set(WMComponents.AlignClient);
				tv.SetScrollbars(hScrollbar, vScrollbar);
				AddContent(tv);
				isoDecoder := Codecs.GetTextDecoder("ISO8859-1");
				isoDecoder.Open(rc.reader, result);
				tv.SetText(isoDecoder.GetText());
				tv.cursor.SetPosition(0);
				если verbose то ЛогЯдра.пСтроку8("done."); ЛогЯдра.пВК_ПС(); всё;
			всё;
			если rc # НУЛЬ то rc.Stop(); rc.Release() всё;
		кон New;

	кон TextPanel;

	HTTPConnectionPool* = окласс
	перем
		connection : массив MaxHTTPConnections из ResourceConnection;
		conCnt : цел32;
(*logW : MultiLogger.LogWindow;
log : Streams.Writer;*)
		проц & Init*;
		нач
			conCnt := 0;
(*NEW(logW, "HTTPConnectionPool", log);*)
		кон Init;

		проц Get*(url : String) : ResourceConnection;
		нач {единолично}
			возврат PrivateGet(url);
		кон Get;

		проц PrivateGet(url : String) : ResourceConnection;
		перем
			server, newServer : String;
			index : размерМЗ;
			i : размерМЗ;
			con : ResourceConnection;
			charset: WebHTTP.AdditionalField;
			reader : Потоки.Чтец;
			result : целМЗ;
			cnt : размерМЗ;
		нач
			server := GetServer(url^);

			дождись((conCnt < MaxHTTPConnections) и (ServerCnt(server) < MaxHTTPConnectionPerServer));

			index := -1; i := 0;
			нцПока((index = -1) и (i < MaxHTTPConnections)) делай
				(* search for non-busy connection of the same server... *)
				(* Это не работает под Win32, но работает в BIOS. Пока отключим переиспользование соединений вовсе *)
				если ложь и (connection[i] # НУЛЬ) и (connection[i].server^ = server^) и (~connection[i].busy) то
					трассируй("повторно использую соединение к тому же серверу");
					index := i;
				всё;
				(* ... if none is found, search for empty index *)
				если (i = MaxHTTPConnections - 1) и (index = -1) то
					i := 0;
					нцПока((index = -1) и (i < MaxHTTPConnections)) делай
						если connection[i] = НУЛЬ то
							трассируй("использую ячейку, в крой нет соединения");
							index := i;
						всё;
						увел(i);
					кц;
					(* ... if none is found, take the next non-busy connection. *)
					если index = -1 то
						i := 0;
						нцПока((index = -1) и (i < MaxHTTPConnections)) делай
							(* и это глючит *)
							если ложь и ~connection[i].busy то
								трассируй("использую какое-то незанятое соединение");
								index := i;
							всё;
							увел(i);
						кц;
					всё;
					утв(index >= 0);
				всё;
				увел(i);
			кц;
			утв(index >= 0);
(*log.String("Get: "); log.String(url^); log.String(" Index: "); log.Int(index, 0); log.Ln(); log.Update();*)
			con := connection[index];
			если con = НУЛЬ то
				нов(connection[index]);
				con := connection[index];
				con.index := index;
				con.server := server;
				нов(con.http);
				con.http.requestHeader.useragent := "BimBrowser (bluebottle.ethz.ch)";
				если Strings.Pos("google.", url^) # -1 то (* only to get UTF-8 charset *)
					con.http.requestHeader.useragent := "Mozilla/5.0";
				всё;
				нов(charset);
				charset.key := "Accept-Charset:";
				charset.value := "UTF-8,ISO-8859-1";
				con.http.requestHeader.additionalFields := charset;
			всё;
			con.busy := истина;

			cnt := 0;
			нц
				con.http.Get(url^, истина, reader, result);
				если (cnt < 16) и (result = 0) и ((con.http.responseHeader.statuscode >= 301) и (con.http.responseHeader.statuscode <= 303)) то (* "Moved" or "See Other" *)
					если Strings.Pos("://", con.http.responseHeader.location) = -1 то
						если ~Strings.StartsWith2("/", con.http.responseHeader.location) то
							url := Strings.ConcatToNew(server^, "/");
							url := Strings.ConcatToNew(url^, con.http.responseHeader.location);
						иначе
							url := Strings.ConcatToNew(server^, con.http.responseHeader.location);
						всё;
					иначе
						url := Strings.NewString(con.http.responseHeader.location);
						newServer := GetServer(con.http.responseHeader.location);
						если server^ # newServer^ то
							con.busy := ложь;
							возврат PrivateGet(url);
						всё;
					всё;
					если verbose то ЛогЯдра.пСтроку8("---Redirecting to "); ЛогЯдра.пСтроку8(url^); ЛогЯдра.пВК_ПС(); всё;
					нцПока reader.чИДайСимв8() # 0X делай кц;
					увел(cnt);
				иначе
					прервиЦикл;
				всё;
			кц;
			con.url := url;
			con.mimeType := Strings.NewString(con.http.responseHeader.contenttype);
			Strings.TrimWS(con.mimeType^);
			con.reader := reader;
			con.released := ложь;
			если result # 0 то
				con.busy := ложь;
				возврат НУЛЬ;
			иначе
				увел(conCnt);
				возврат con;
			всё;
		кон PrivateGet;

		проц Release(rc : ResourceConnection);
		нач {единолично}
			(* мы ломаем здесь логику пулов соединений, поскольку они не работают. Пытаемся их отключить *)
			(* connection[rc.index].busy := ложь; *) (* это я закомментировал *) 
			connection[rc.index] := НУЛЬ; (* это тоже я добавил *)
			умень(conCnt);
		кон Release;

		проц ServerCnt(server : String) : размерМЗ;
		перем
			cnt, i : размерМЗ;
		нач
			cnt := 0;
			нцДля i := 0 до MaxHTTPConnections - 1 делай
				если (connection[i] # НУЛЬ) и (connection[i].server^ = server^) и (connection[i].busy) то увел(cnt) всё;
			кц;
			возврат cnt;
		кон ServerCnt;

		проц GetServer(перем url : массив из симв8) : String;
		перем
			end : размерМЗ;
		нач
			end := Strings.IndexOfByte('/', Strings.Pos("://", url) + 3, url);
			если end = -1 то end := Strings.Length(url) всё;
			возврат Strings.Substring(0, end, url);
		кон GetServer;

	кон HTTPConnectionPool;

	ResourceConnection* = окласс
	перем
		url- : String;
		mimeType- : String;
		reader- : Потоки.Чтец;
		http : NewHTTPClient.HTTPConnection;
		busy : булево;
		index : размерМЗ;
		server : String;
		released : булево;

		проц Stop*;
		нач {единолично}
			StopNoLock;
		кон Stop;

		проц StopNoLock;
		нач 
			если ~released и (http # НУЛЬ) то
				http.Close();
				server := Strings.NewString("");
			всё;
		кон StopNoLock;


		проц Release*;
		нач {единолично}
			released := истина;
			если http # НУЛЬ то
				StopNoLock();
				httpConnectionPool.Release(сам);
			всё;
		кон Release;

	кон ResourceConnection;


перем
	manager : WMWindowManager.WindowManager;
	httpConnectionPool* : HTTPConnectionPool;

проц GetResourceConnection*(url : String) : ResourceConnection;
перем
	pos : размерМЗ;
	protocol : String;
	filename : String;
	connection : ResourceConnection;
	file : Files.File;
	fileReader : Files.Reader;
нач
	Strings.TrimWS(url^);
	pos := Strings.Pos("://", url^);
	если pos = -1 то
		если verbose то ЛогЯдра.пСтроку8("Unknown Protocol: "); ЛогЯдра.пСтроку8(url^); ЛогЯдра.пВК_ПС(); всё;
		возврат НУЛЬ;
	всё;
	protocol := Strings.Substring(0, pos, url^);
	если (pos + 3) >= Strings.Length(url^) то
		если verbose то ЛогЯдра.пСтроку8("Bad URL: "); ЛогЯдра.пСтроку8(url^); ЛогЯдра.пВК_ПС(); всё;
		возврат НУЛЬ;
	всё;
	filename := Strings.Substring2(pos+3, url^);
	ClearFilename(filename^);

	если protocol^ = "http" то
		возврат httpConnectionPool.Get(url);
	аесли protocol^ = "file" то
		file := Files.Old(filename^);
		если file = НУЛЬ то
			если verbose то ЛогЯдра.пСтроку8("file not found: "); ЛогЯдра.пСтроку8(url^); ЛогЯдра.пВК_ПС(); всё;
			возврат НУЛЬ;
		всё;
		Files.OpenReader(fileReader, file, 0);
		нов(connection);
		connection.url := url;
		connection.mimeType := GetMimeType(filename^);
		connection.reader := fileReader;
		возврат connection;
	иначе
		если verbose то ЛогЯдра.пСтроку8("Unknown Protocol: "); ЛогЯдра.пСтроку8(protocol^); ЛогЯдра.пВК_ПС(); всё;
		возврат НУЛЬ;
	всё;
кон GetResourceConnection;

проц ClearFilename( перем name: массив из симв8 );  (* strip positioning info appended to filename *)
перем i: размерМЗ;
нач
	i := 0;
	нц
		если name[i] < '.' то  name[i] := 0X  всё;
		если name[i] = 0X то  прервиЦикл  всё;
		увел( i );
		если i >= длинаМассива( name ) то  прервиЦикл  всё
	кц
кон ClearFilename;

проц GetMimeType(перем filename : массив из симв8) : String;
перем
	dotPos : размерМЗ;
	appendix : String;
нач
	Strings.TrimWS(filename);
	dotPos := Strings.LastIndexOfByte2('.', filename);
	если  (dotPos = -1) или (dotPos = Strings.Length(filename) - 1) то
		возврат Strings.NewString(filename);
	всё;
	appendix := Strings.Substring2(dotPos + 1, filename);
	Strings.LowerCase(appendix^);
	если appendix^ = "html" то возврат Strings.NewString("text/html"); всё;
	если appendix^ = "htm" то возврат Strings.NewString("text/html"); всё;
	если appendix^ = "txt" то возврат Strings.NewString("text/plain"); всё;
	если appendix^ = "jpg" то возврат Strings.NewString("image/jpeg"); всё;
	если appendix^ = "jpeg" то возврат Strings.NewString("image/jpeg"); всё;
	если appendix^ = "jpe" то возврат Strings.NewString("image/jpeg"); всё;
	если appendix^ = "jp2" то возврат Strings.NewString("image/jp2"); всё;
	если appendix^ = "png" то возврат Strings.NewString("image/png"); всё;
	если appendix^ = "bmp" то возврат Strings.NewString("image/bmp"); всё;
	если appendix^ = "gif" то возврат Strings.NewString("image/gif"); всё;
	если appendix^ = "svg" то возврат Strings.NewString("image/svg+xml"); всё;
	если appendix^ = "xml" то возврат Strings.NewString("application/xml"); всё;
	если appendix^ = "pdf" то возврат Strings.NewString("application/pdf"); всё;
	возврат appendix;
кон GetMimeType;

проц LoadImage*(reader : Потоки.Чтец; mimeType : массив из симв8; name : массив из симв8): WMGraphics.Image;
перем
	img : WMGraphics.Image;
	res: целМЗ; w, h : размерМЗ; x : цел32;
	decoder : Codecs.ImageDecoder;
	ext : String;

	проц GetExtensionForMimeType(перем mimeType : массив из симв8) : String;
	нач
		если Strings.StartsWith2("image/jpeg", mimeType) то возврат Strings.NewString("JPG"); всё;
		если Strings.StartsWith2("image/jp2", mimeType) то возврат Strings.NewString("JP2"); всё;
		если Strings.StartsWith2("image/png", mimeType) то возврат Strings.NewString("PNG"); всё;
		если Strings.StartsWith2("image/bmp", mimeType) то возврат Strings.NewString("BMP"); всё;
		если Strings.StartsWith2("image/gif", mimeType) то возврат Strings.NewString("GIF"); всё;
		если Strings.StartsWith2("image/svg+xml", mimeType) то возврат Strings.NewString("SVG"); всё;
		возврат Strings.NewString("");
	кон GetExtensionForMimeType;

нач
	если reader = НУЛЬ то возврат НУЛЬ всё;
	ext := GetExtensionForMimeType(mimeType);
	decoder := Codecs.GetImageDecoder(ext^);
	если decoder = НУЛЬ то
		ЛогЯдра.пСтроку8("No decoder found for "); ЛогЯдра.пСтроку8(mimeType); ЛогЯдра.пВК_ПС;
		возврат НУЛЬ
	всё;
	decoder.Open(reader, res);
	если res = 0 то
		decoder.GetImageInfo(w, h, x, x);
		нов(img);
		Raster.Create(img, w, h, Raster.BGRA8888);
		decoder.Render(img);
		нов(img.key, длинаМассива(name)); копируйСтрокуДо0(name, img.key^);
	всё;
	возврат img
кон LoadImage;

нач
	manager := WMWindowManager.GetDefaultManager();
	нов(httpConnectionPool);
кон WebBrowserComponents.

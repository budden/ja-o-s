модуль TFTPServer; (** AUTHOR "be"; PURPOSE "TFTP server"; *)

использует IP, UDP, Files, Kernel, ЛогЯдра, Random;

конст
	Ok = UDP.Ok;

	(* General Settings *)
	TFTPPort = 69;
	MaxSocketRetries = 64;
	MaxRetries = 5;
	MaxWait = 3;
	BlockSize = 512;
	DataTimeout = 3000; (* ms *)
	AckTimeout = 3000; (* ms *)

	(* Packet Types *)
	RRQ = 1;
	WRQ = 2;
	DATA = 3;
	ACK = 4;
	ERROR = 5;

	RRQId = "TFTP RRQ: ";
	WRQId = "TFTP WRQ: ";
	TFTPId = "TFTP Server: ";

тип
	ErrorMsg = массив 32 из симв8;

	TFTP = окласс
		перем socket: UDP.Socket;
			fip: IP.Adr;
			lport, fport: цел32;
			res: целМЗ;
			dead: булево;
			buf: массив BlockSize + 4 из симв8;
			timer: Kernel.Timer;

		(* Log functions *)
		проц LogEnter(level: целМЗ);
		нач если (TraceLevel >= level) то ЛогЯдра.ЗахватВЕдиноличноеПользование всё
		кон LogEnter;

		проц LogExit(level: целМЗ);
		нач если (TraceLevel >= level) то ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования всё
		кон LogExit;

		проц Log(level: целМЗ; конст s: массив из симв8);
		нач если (TraceLevel >= level) то ЛогЯдра.пСтроку8(s) всё
		кон Log;

		проц LogInt(level: целМЗ; i: размерМЗ);
		нач если (TraceLevel >= level) то ЛогЯдра.пЦел64(i, 0) всё
		кон LogInt;

		(* Get2 - reads a (big endian) 16bit value from 'buf' at position 'ofs'..'ofs'+1 *)
		проц Get2(конст buf: массив из симв8; ofs: размерМЗ): цел32;
		нач возврат кодСимв8(buf[ofs])*100H + кодСимв8(buf[ofs+1])
		кон Get2;

		(* Put2 - writes a (big endian) 16bit value to 'buf' at position 'ofs'..'ofs'+1 *)
		проц Put2(перем buf: массив из симв8; ofs: размерМЗ; value: цел32);
		нач buf[ofs] := симв8ИзКода(value DIV 100H остОтДеленияНа 100H); buf[ofs+1] := симв8ИзКода(value остОтДеленияНа 100H)
		кон Put2;

		(* PacketType - returns the type of a packet *)
		проц PacketType(конст buf: массив из симв8): цел32;
		нач возврат Get2(buf, 0)
		кон PacketType;

		(* ExtractString - extracts a 0X terminated 8bit string from a buffer *)
		проц ExtractString(конст buf: массив из симв8; перем ofs: размерМЗ; перем s: массив из симв8);
		перем pos: цел32;
		нач
			нцПока (ofs < длинаМассива(buf)) и (buf[ofs] # 0X) делай
				если (pos < длинаМассива(s)-1) то s[pos] := buf[ofs]; увел(pos) всё;
				увел(ofs)
			кц;
			s[pos] := 0X; увел(ofs)
		кон ExtractString;

		(* SendAck - sends an ack packet *)
		проц SendAck(blockNr: цел32; перем res: целМЗ);
		перем ackHdr: массив 4 из симв8; retries: цел32;
		нач
			Put2(ackHdr, 0, ACK); Put2(ackHdr, 2, blockNr);
			нцДо
				увел(retries);
				socket.Send(fip, fport, ackHdr, 0, длинаМассива(ackHdr), res);
			кцПри (res = Ok) или (retries > MaxRetries)
		кон SendAck;

		(* SendError - sends an error packet *)
		проц SendError(errNo: цел16; s: ErrorMsg; перем res: целМЗ);
		перем errHdr: массив BlockSize+4 из симв8; p, retries: цел32;
		нач
			Put2(errHdr, 0, ERROR); Put2(errHdr, 2, errNo);
			если ((errNo = 0) и (s = "")) или ((errNo > 0) и (errNo < 8)) то s := errorMsg[errNo] всё;
			нцПока (p < BlockSize-1) и (s[p] # 0X) делай errHdr[4+p] := s[p]; увел(p) кц;
			errHdr[4+p] := 0X;
			нцДо
				увел(retries);
				socket.Send(fip, fport, errHdr, 0, p+4, res)
			кцПри (res = Ok) или (retries > MaxRetries)
		кон SendError;

		проц Die;
		нач { единолично }
			dead := истина
		кон Die;

		проц AwaitDeath;
		нач { единолично }
			дождись(dead)
		кон AwaitDeath;
	кон TFTP;

	TFTPRRQ = окласс(TFTP)
		перем
			ip: IP.Adr;
			ack: массив 4 из симв8;
			port, wait, retries, blockNr: цел32;
			len: размерМЗ;
			acked: булево;
			file: Files.File;
			r: Files.Rider;

		(* Init - constructor *)
		проц &Init*(fip: IP.Adr; fport: цел32; конст filename: Files.FileName; перем res: целМЗ);
		перем retries: цел32;
		нач сам.fip := fip; сам.fport := fport;
			file := Files.Old(filename);
			если (file # НУЛЬ) то
				нцДо
					увел(retries); lport := 1024 + generator.Integer() остОтДеленияНа 64512;
					нов(socket, lport, res);
				кцПри (res # UDP.PortInUse) или (retries > MaxSocketRetries)
			иначе
				res := -1
			всё
		кон Init;

	нач {активное}
		если (socket = НУЛЬ) то возврат всё;
		LogEnter(2); Log(2, RRQId); Log(2, "sending file on port "); LogInt(2, lport); Log(2, "..."); LogExit(2);
		file.Set(r, 0);
		Put2(buf, 0, 3); (* DATA packet *)
		blockNr := 0; acked := истина;
		нцПока ~r.eof и acked делай
			увел(blockNr);
			buf[2] := симв8ИзКода(blockNr DIV 100H); buf[3] := симв8ИзКода(blockNr остОтДеленияНа 100H);
			file.ReadBytes(r, buf, 4, BlockSize);
			retries := 0;
			нцДо
				увел(retries);
				LogEnter(3); Log(3, RRQId); Log(3, "sending block "); LogInt(3, blockNr);
				Log(3, " ("); LogInt(3, BlockSize-r.res); Log(3, " bytes) ");
				если (retries > 1) то Log(3, "(retry "); LogInt(3, retries); Log(3, ")") всё;
				LogExit(3);

				socket.Send(fip, fport, buf, 0, 4 + BlockSize - r.res, res);
				wait := 0;
				нцДо
					увел(wait);
					LogEnter(3); Log(3, RRQId); Log(3, "waiting for ack... ");
					если (wait > 1) то Log(3, "(retry "); LogInt(3, wait); Log(3, ")") всё;
					LogExit(3);
					acked := ложь;
					socket.Receive(ack, 0, 4, AckTimeout, ip, port, len, res);

					LogEnter(3); Log(3, RRQId);
					если (res = UDP.Timeout) то Log(3, "timeout")
					аесли (res = Ok) то
						acked := (res = Ok) и (PacketType(ack) = ACK) и (Get2(ack, 2) = blockNr) и (IP.AdrsEqual(ip, fip)) и (fport = port);
						если acked то Log(3, "got ack") иначе Log(3, "ack failed") всё
					иначе
						Log(3, "unknown error "); LogInt(3, цел32(res))
					всё;
					LogExit(3)
				кцПри acked или (res # Ok) или (wait > MaxWait)
			кцПри acked или (retries > MaxRetries)
		кц;
		LogEnter(2); Log(2, RRQId);
		если ~acked то Log(2, "file not completely sent")
		иначе Log(2, "file successfully sent")
		всё;
		LogExit(2);
		нов(timer);
		timer.Sleep(AckTimeout+500);
		Die
	кон TFTPRRQ;

	TFTPWRQ = окласс(TFTP)
		перем
			ip: IP.Adr;
			port, waitPacket, retries, blockNr: цел32;
			len: размерМЗ;
			Abort: булево;
			file: Files.File;
			r: Files.Rider;

		(* Init - constructor *)
		проц &Init*(fip: IP.Adr; fport: цел32; конст filename: Files.FileName; перем res: целМЗ);
		перем retries: цел32;
		нач
			сам.fip := fip; сам.fport := fport; res := 0;
			file := Files.Old(filename);
			если (file = НУЛЬ) то
				file := Files.New(filename);
				если (file = НУЛЬ) то
					LogEnter(1); Log(1, TFTPId); Log(1, "unexpected error: can't create '"); Log(1, filename); Log(1, "'"); LogExit(1);
					res := -1;
				иначе
					нцДо
						увел(retries); lport := 1024 + generator.Integer() остОтДеленияНа 64512;
						нов(socket, lport, res)
					кцПри (res # UDP.PortInUse) или (retries > MaxSocketRetries)
				всё
			иначе
				res := -1
			всё
		кон Init;

	нач {активное}
		если (socket = НУЛЬ) то возврат всё;
		LogEnter(2); Log(2, WRQId); Log(2, "receiving file on port "); LogInt(2, lport); Log(2, "..."); LogExit(2);
		file.Set(r, 0);
		Files.Register(file);
		blockNr := 0;
		SendAck(blockNr, res);
		если (res = Ok) то
			нцДо
				увел(blockNr);
				LogEnter(3); Log(3, WRQId); Log(3, " receiving block "); LogInt(3, blockNr);
				если (retries > 1) то Log(3, " (retry "); LogInt(3, retries); Log(3, ")") всё;
				LogExit(3);
					socket.Receive(buf, 0, длинаМассива(buf), DataTimeout, ip, port, len, res);
					если (res = Ok) то
						если IP.AdrsEqual(ip, fip) и (fport = port) то
							если (PacketType(buf) = DATA) то
								если (Get2(buf, 2) = blockNr) то
									file.WriteBytes(r, buf, 4, len-4);
									file.Update();
									если (r.res = 0) то
										SendAck(blockNr, res);
										Abort := res # Ok
									иначе
										LogEnter(3); Log(3, WRQId); Log(3, errorMsg[3]); LogExit(3);
										SendError(3, "", res);
										Abort := истина
									всё
								иначе (* bad block number, client must send packet again *)
									увел(waitPacket); len := BlockSize;
									LogEnter(3); Log(3, WRQId); Log(3, "Bad block number ("); LogInt(3, waitPacket); Log(3, ")"); LogExit(3)
								всё
							иначе (* wrong packet type *)
								LogEnter(3); Log(3, WRQId); Log(3, errorMsg[4]); LogExit(3);
								SendError(4, "", res);
								Abort := истина
							всё
						иначе (* wrong client ip/port *)
							LogEnter(3); Log(3, WRQId); Log(3, errorMsg[5]); LogExit(3);
							SendError(5,"", res)
						всё
					аесли (res = UDP.Timeout) то
						увел(waitPacket); len := BlockSize;
						LogEnter(3); Log(3, WRQId); Log(3, "Timeout ("); LogInt(3, waitPacket); Log(3, ")"); LogExit(3)
					иначе (* unknown error (UDP/IP error) *)
						LogEnter(3); Log(3, WRQId); Log(3, errorMsg[0]); LogExit(3);
						SendError(0, "", res);
						Abort := истина
					всё;
			кцПри Abort или (waitPacket > MaxWait) или (len < BlockSize);

			LogEnter(2); Log(2, WRQId);
			если (len < BlockSize) то
				file.Update();
				Log(2, "file successfully received")
			иначе
				Log(2, "file transfer aborted");
				если (waitPacket > MaxWait) то Log(2, " (timeout)") всё
			всё;
			LogExit(2)
		иначе
			LogEnter(2); Log(2, WRQId); Log(2, "can't send initial ack"); LogExit(2);
		всё;
		нов(timer);
		timer.Sleep(AckTimeout+500);
		socket.Close;
		Die
	кон TFTPWRQ;

	TFTPServer = окласс(TFTP)
		перем
			ofs,len: размерМЗ;
			ipstr, mode: массив 16 из симв8;
			filename: Files.FileName;
			Stop, allowWrite: булево;
			tftprrq: TFTPRRQ;
			tftpwrq: TFTPWRQ;

		проц &Init*(port: цел32; перем res: целМЗ);
		нач нов(socket, port, res); lport := port
		кон Init;

		проц WriteMode(allow: булево);
		нач allowWrite := allow
		кон WriteMode;

		проц Close;
		нач { единолично }
			socket.Close; Stop := истина
		кон Close;

	нач { активное }
		если (res = Ok) то
			LogEnter(1); Log(1, TFTPId); Log(1, "listening on port "); LogInt(1, lport); LogExit(1);
			нцДо
				socket.Receive(buf, 0, длинаМассива(buf), 1000, fip, fport, len, res);
				если (res = Ok) то
					IP.AdrToStr(fip, ipstr);
					LogEnter(2);
					Log(2, TFTPId); Log(2, "connected to "); Log(2, ipstr); Log(2, " on port "); LogInt(2, fport);
					LogExit(2);
					просей PacketType(buf) из
					| RRQ:
							ofs := 2;
							ExtractString(buf, ofs, filename); ExtractString(buf, ofs, mode);
							LogEnter(2);
							Log(2, TFTPId); Log(2, "read request for '"); Log(2, filename); Log(2, "', mode '");  Log(2, mode); Log(2, "' ");
							LogExit(2);
							нов(tftprrq, fip, fport, filename, res); tftprrq := НУЛЬ;
							если (res = -1) то
								LogEnter(2); Log(2, TFTPId); Log(2, "read request: "); Log(2, errorMsg[1]); LogExit(2);
								SendError(1, "", res)
							аесли (res # Ok) то
								LogEnter(2); Log(2, TFTPId); Log(2, "read request: "); Log(2, ": error "); LogInt(2, цел32(res)); LogExit(2);
								SendError(0, "", res)
							иначе
								LogEnter(2); Log(2, TFTPId); Log(2, "read request: "); Log(2, ": transfer started"); LogExit(2)
							всё
					| WRQ:
							ofs := 2;
							ExtractString(buf, ofs, filename); ExtractString(buf, ofs, mode);
							LogEnter(2);
							Log(2, TFTPId); Log(2, "write request for '"); Log(2, filename); Log(2, "', mode '"); Log(2, mode); Log(2, "' ");
							LogExit(2);
							если allowWrite то
								нов(tftpwrq, fip, fport, filename, res); tftpwrq := НУЛЬ;
								если (res = -1) то
									LogEnter(2); Log(2, TFTPId); Log(2, "write request: "); Log(2, errorMsg[6]); LogExit(2);
									SendError(6, "", res)
								аесли (res # Ok) то
									LogEnter(2); Log(2, TFTPId); Log(2, "write request: "); Log(2, ": error "); LogInt(2, цел32(res)); LogExit(2);
									SendError(0, "", res)
								иначе
									LogEnter(2); Log(2, TFTPId); Log(2, "write request: "); Log(2, ": transfer started"); LogExit(2)
								всё
							иначе
								LogEnter(2); Log(2, TFTPId); Log(2, "write request: "); Log(2, errorMsg[2]); LogExit(2);
								SendError(2, "", res)
							всё
					иначе LogEnter(2); Log(2, TFTPId); Log(2, "Invalid request"); LogExit(2)
					всё
				аесли (res = UDP.Timeout) то (* nothing *)
				иначе
					Stop := истина;
					LogEnter(2); Log(2, TFTPId); Log(2, "socket error "); LogInt(2, цел32(res)); LogExit(2);
				всё
			кцПри Stop;
		всё;
		Die
	кон TFTPServer;

перем
	tftpserver: TFTPServer;
	TraceLevel: целМЗ;
	errorMsg: массив 8 из ErrorMsg;
	generator: Random.Generator;

проц Start*;
перем res: целМЗ;
нач
	если (tftpserver = НУЛЬ) то
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Starting TFTP Server..."); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
		нов(tftpserver, TFTPPort, res);
		если (res # UDP.Ok) то
			tftpserver := НУЛЬ;
			ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("TFTP Server: UDP port not available"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
		всё
	иначе
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("TFTP Server: already running"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	всё
кон Start;

проц Stop*;
нач
	если (tftpserver # НУЛЬ) то
		tftpserver.Close; tftpserver.AwaitDeath; tftpserver := НУЛЬ;
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("TFTP Server stopped"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	иначе
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("TFTP Server not running"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	всё
кон Stop;

проц AllowWrite*;
нач
	если (tftpserver # НУЛЬ) то
		tftpserver.WriteMode(истина);
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("TFTP Server: writing allowed"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	иначе
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("TFTP Server: not running. use TFTPServer.Start"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	всё
кон AllowWrite;

проц DenyWrite*;
нач
	если (tftpserver # НУЛЬ) то
		tftpserver.WriteMode(ложь);
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("TFTP Server: writing denied"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
	иначе
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("TFTP Server: not running. use TFTPServer.Start"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	всё
кон DenyWrite;

проц TraceLevel0*;
нач TraceLevel := 0
кон TraceLevel0;

проц TraceLevel1*;
нач TraceLevel := 1
кон TraceLevel1;

проц TraceLevel2*;
нач TraceLevel := 2
кон TraceLevel2;

проц TraceLevel3*;
нач TraceLevel := 3
кон TraceLevel3;

нач
	errorMsg[0] := "Undefined error.";
	errorMsg[1] := "File not found.";
	errorMsg[2] := "Access violation.";
	errorMsg[3] := "Disk full.";
	errorMsg[4] := "Illegal TFTP operation.";
	errorMsg[5] := "Unknown transfer ID.";
	errorMsg[6] := "File already exists.";
	errorMsg[7] := "No such user.";
	TraceLevel := 2;
	нов(generator)
кон TFTPServer.


System.Free TFTPServer ~

TFTPServer.Start
TFTPServer.Stop

TFTPServer.AllowWrite
TFTPServer.DenyWrite

TFTPServer.TraceLevel0
TFTPServer.TraceLevel1
TFTPServer.TraceLevel2
TFTPServer.TraceLevel3

модуль SSHTerminal; 	(* GF  10.12.2020 *)

(* derived from WMVT100.Mod by ejz, modified to use an SSH connection instead of telnet *)


использует
	Kernel, Commands, Files, Inputs, Потоки, Log := ЛогЯдра, Beep,
	WMWindowManager, WMComponents, WMStandardComponents, WMG := WMGraphics,
	WMMessages, WMEditors, WMRectangles, 
	
	SSHAuthorize, SSHChannels;

конст
	TerminalWidth = 80;
	TerminalHeight = 24;

	Border = 2; BoxW = 8; BoxH = 18;

	Left = 0; Right = 2;
	Underscore = 0; Blink = 1;
	CursorKeyMode = 0; AppKeypadMode = 1; AutoWrapMode = 2;

	ESC = 1BX; DEL = 07FX; CR = 0DX; NL = 0AX;
	
	DefLogin = "SSH.DefaultLogin"
	
перем 
	lastHostname, lastUsername: массив 64 из симв8;

тип
	SSHChannel = SSHChannels.Channel;
	
	WindowCloser = проц {делегат};
	
	Attribute = укль на запись
		fnt: WMG.Font;
		bg, fg: WMG.Color;
		special: мнвоНаБитахМЗ (* 0: underscore *)
	кон;

	Char = запись
		attr: Attribute;
		char: цел32
	кон;

	Data = укль на массив из Char;

	Line = укль на запись
		data: Data;
		t, b: размерМЗ;
		next: Line
	кон;

	Position = запись
		line: Line; ofs: размерМЗ
	кон;



	Frame = окласс (WMComponents.VisualComponent)
		перем
			rows, cols, boxW, boxH, dX, dY: размерМЗ;
			chan: SSHChannel;
			w: Потоки.Писарь;
			mode: мнвоНаБитахМЗ;
			windowCloser: WindowCloser;

			first, top: Line; bg: WMG.Color;
			scrollTop, scrollBottom: Line;
			scrollBegin, scrollEnd: размерМЗ;

			tabs: укль на массив из булево;
			attr: Attribute;
			cursor: Position;
			old:	запись
						attr: Attribute;
						offs: размерМЗ;
						row: размерМЗ
					кон;
			sel:	запись
						beg, end: Position
					кон;



			проц GetCol(): размерМЗ;
			нач {единолично}
				возврат cursor.ofs
			кон GetCol;

			проц GetRow(): размерМЗ;
			перем l: Line; row: размерМЗ;
			нач {единолично}
				l := top;  row := 0;
				нцПока l # cursor.line делай
					l := l.next; увел( row )
				кц;
				возврат row
			кон GetRow;


			проц GetNewLine(): Line;
			перем line: Line; i: размерМЗ; ch: Char;
			нач
				нов( line ); line.next := НУЛЬ;
				нов( line.data, cols );
				ch.attr := attr; ch.char := 0;
				нцДля i := 0 до cols - 1 делай  line.data[i] := ch  кц;
				возврат line
			кон GetNewLine;


			проц AppendLine( pred: Line ): Line;
			перем line: Line;
			нач
				line := GetNewLine();
				если pred # НУЛЬ то
					line.next := pred.next;
					pred.next := line;
					если pred.b >= dY то  line.t := pred.b  иначе  line.t := dY  всё
				иначе
					line.t := dY;
				всё;
				line.b := line.t + boxH;
				возврат line
			кон AppendLine;



			проц UpdateBox(line: Line; ofs: размерМЗ);
			перем update: WMG.Rectangle;
			нач
				update.l := dX + ofs*boxW; update.r := update.l + boxW;
				update.t := line.t; update.b := line.b;
				InvalidateRect(update)
			кон UpdateBox;

			проц UpdateRect(al, bl: Line; aofs, bofs: размерМЗ; cur: мнвоНаБитахМЗ);
			перем tl: Line; tofs: размерМЗ; update: WMG.Rectangle; swapl, swapo: булево;
			нач
				swapl := ложь; swapo := ложь;
				если al # bl то
					tl := al;
					нцПока (tl # НУЛЬ) и (tl # bl) делай
						tl := tl.next
					кц;
					если tl = НУЛЬ то swapl := истина; tl := al; al := bl; bl := tl всё
				всё;
				если aofs > bofs то swapo := истина; tofs := aofs; aofs := bofs; bofs := tofs всё;
				update.l := dX + aofs*boxW; update.r := dX + bofs*boxW + boxW;
				update.t := al.t; update.b := bl.b;
				если cur # {} то
					если 1 в cur то
						если swapl то cursor.line := bl иначе cursor.line := al всё
					аесли 2 в cur то
						если swapl то cursor.line := al иначе cursor.line := bl всё
					всё;
					если 3 в cur то
						если swapo то cursor.ofs := bofs иначе cursor.ofs := aofs всё
					аесли 4 в cur то
						если swapo то cursor.ofs := aofs иначе cursor.ofs := bofs всё
					всё
				всё;
				InvalidateRect(update)
			кон UpdateRect;

			проц UpdateAll;
			перем update: WMG.Rectangle;
			нач
				update.l := 0; update.r := bounds.GetWidth();
				update.t := 0; update.b := bounds.GetHeight();
				InvalidateRect(update)
			кон UpdateAll;


			проц WriteChars( конст buf: массив из симв8; n: размерМЗ);
			перем prev, l: Line; i, ofs: размерМЗ; wrap: булево;
			нач {единолично}
				wrap := ложь;
				l := cursor.line;  ofs := cursor.ofs;  i := 0;
				нц
					нцПока (i < n) и (ofs < cols) делай
						l.data[ofs].attr := attr;
						l.data[ofs].char := кодСимв8( buf[i] );
						увел( ofs ); увел( i )
					кц;
					если (i < n) и (AutoWrapMode в mode) то
						prev := l;  l := l.next;  ofs := 0;  wrap := истина;
						если l = НУЛЬ то
							l := AppendLine( prev )
						всё
					иначе
						прервиЦикл
					всё
				кц;
				если wrap то
					cursor.ofs := ofs;
					UpdateRect( cursor.line, l, 0, cols-1, {2} )
				иначе
					UpdateRect( cursor.line, l, cursor.ofs, ofs, {4} )
				всё
			кон WriteChars;



			проц Delete;
			перем l: Line; ofs: размерМЗ;
			нач {единолично}
				l := cursor.line;  ofs := cursor.ofs;
				если ofs > 0 то
					умень( ofs );
					l.data[ofs].attr := attr;
					l.data[ofs].char := 0;
					UpdateRect( l, l, ofs, cursor.ofs, {3} )
				всё
			кон Delete;


			проц GetLine( n: размерМЗ ): Line;
			перем line: Line;
			нач
				line := top;
				нцПока (n > 0) и (line # НУЛЬ) делай  line := line.next;  умень( n )  кц;
				возврат line
			кон GetLine;

			проц GetLastLine( ): Line;
			перем line: Line;
			нач
				line := top;
				нцПока line.next # НУЛЬ делай  line := line.next  кц;
				возврат line
			кон GetLastLine;


			проц SetScrollRegion;
			нач
				scrollTop := GetLine( scrollBegin );
				scrollBottom := GetLine( scrollEnd );
			кон SetScrollRegion;

			проц Goto( row, col: размерМЗ );
			перем l: Line; hl, lines: размерМЗ;
			нач {единолично}
				если col < 0 то  col := 0  аесли col >= cols то  col := cols - 1  всё;

				l := first;  hl := 1;
				нцПока l # top делай  увел( hl );  l := l.next  кц;
				нцПока hl > 512 делай  first := first.next;  умень( hl )  кц;	(* limit history *)
				lines := 1;

				нцПока row > 0 делай
					если l.next = НУЛЬ то
						l := AppendLine( l );
					иначе
						l := l.next
					всё;
					умень( row );  увел( lines )
				кц;

				если lines > rows то
					top := top.next;
					cursor.line := l; cursor.ofs := 0;
					UpdateAll()
				иначе
					UpdateRect( cursor.line, l, cursor.ofs, col, {2, 4} )
				всё;
				SetScrollRegion;
				SetOffsets;
			кон Goto;


			проц SetOffsets;
			перем l: Line; y: размерМЗ;
			нач
				l := top; y := dY;
				нцДо
					l.t := y;  увел( y, BoxH );  l.b := y;
					l := l.next
				кцПри l = НУЛЬ
			кон SetOffsets;


			проц MoveLines( down: булево );
			перем prev, l, newtop: Line;
			нач
				l := first; prev := НУЛЬ;
				нцПока l # scrollTop делай  prev := l;  l := l.next  кц;
				если down то
					l := GetNewLine( );
					l.next := scrollTop;
					если prev # НУЛЬ то
						prev.next := l;
						если top = scrollTop то top := l  всё
					иначе  first := l;  top := l
					всё;
					нцПока (l # scrollBottom) и (l.next # НУЛЬ) делай  prev := l; l := l.next  кц;
					prev.next := l.next; (* unlink bottom line *)
				иначе	(* up *)
					нцПока (l # scrollBottom) и (l.next # НУЛЬ) делай  l := l.next  кц;
					l := AppendLine( l );
					newtop := scrollTop.next;
					prev.next := newtop;	(* unlink top line *)
					если top = scrollTop то  top := newtop  всё;
					если first = scrollTop то  first := newtop  всё;
				всё;
				SetScrollRegion;
				SetOffsets
			кон MoveLines;

			проц Scroll( down: булево );
			нач {единолично}
				MoveLines( down );
				если down то
					cursor.line := scrollTop; cursor.ofs := 0;
					UpdateAll
				иначе
					cursor.line := scrollBottom; cursor.ofs := 0;
					UpdateAll
				всё
			кон Scroll;


			проц SetMargins( beg, end: размерМЗ );
			нач {единолично}
				scrollBegin := beg - 1;
				scrollEnd := end - 1 ;
				SetScrollRegion
			кон SetMargins;


			проц RightTab;
			перем l: Line; ofs: размерМЗ; char: Char;
			нач {единолично}
				char.attr := attr; char.char := 020H;
				l := cursor.line;  ofs := cursor.ofs + 1;
				нцПока (ofs < cols) и ~tabs[ofs] делай
					l.data[ofs] := char;  увел( ofs )
				кц;
				если ofs = cursor.ofs то  возврат  всё;
				UpdateRect( l, l, cursor.ofs, ofs, {4} )
			кон RightTab;

			проц EraseLine( l: Line; from, to: размерМЗ );
			перем i: размерМЗ;
			нач
				i := from;
				нцПока i <= to делай
					l.data[i].attr := attr;  l.data[i].char := 0;
					увел( i )
				кц
			кон EraseLine;

			проц Erase( mode: симв8;  конст par: массив из цел32;  n: размерМЗ );
			нач {единолично}
				просей mode из
				|"J":
					sel.beg.line := НУЛЬ;
					top := GetLastLine();
					cursor.line := top; cursor.ofs := 0;
					EraseLine( top, 0, cols-1 );
					UpdateAll();
					SetScrollRegion;
				|"K":
					если n = 0 то
						EraseLine( cursor.line, cursor.ofs, cols-1 );
						UpdateRect( cursor.line, cursor.line, cursor.ofs, cols-1, {} )
					аесли (n = 1) и (par[0] = 1) то
						EraseLine( cursor.line, 0, cursor.ofs );
						UpdateRect( cursor.line, cursor.line, 0, cursor.ofs, {} )
					аесли (n = 1) и (par[0] = 2) то
						EraseLine( cursor.line, 0, cols-1 );
						UpdateRect( cursor.line, cursor.line, 0, cols-1, {} )
					всё
				всё
			кон Erase;

			проц NewAttr;
			перем f: Files.File;
			нач
				нов(attr); attr.special := {};
				f := Files.Old( "PTMono_bd.ttf" );
				если f # НУЛЬ то
					attr.fnt := WMG.GetFont( "PTMono", 13, {0} )
				иначе
					attr.fnt := WMG.GetFont( "Courier", 12, {} )
				всё;
				attr.bg := WMG.RGBAToColor( 255, 255, 255, 255 );
				attr.fg := WMG.RGBAToColor( 0, 0, 0, 255 )
			кон NewAttr;

			проц Bright;
			перем style: мнвоНаБитахМЗ;
			нач
				style := attr.fnt.style;
				если ~(WMG.FontBold в style) то
					включиВоМнвоНаБитах( style, WMG.FontBold );
					attr.fnt := WMG.GetFont( attr.fnt.name, attr.fnt.size, style )
				иначе
					(* Log.String("Bright"); Log.Ln() *)
				всё 
			кон Bright;

			проц Dim;
			перем style: мнвоНаБитахМЗ;
			нач
				style := attr.fnt.style;
				если WMG.FontBold в style то
					исключиИзМнваНаБитах( style, WMG.FontBold );
					attr.fnt := WMG.GetFont( attr.fnt.name, attr.fnt.size, style )
				иначе
					(* Log.String("Dim"); Log.Ln()	*)
				всё
			кон Dim;

			проц SetAttributes( конст attrs: массив из цел32; n: размерМЗ );
			перем c: WMG.Color; i: размерМЗ;
			нач {единолично}
				NewAttr();
				i := 0;
				нцПока i < n делай
					просей attrs[i] из
					|0: (* Reset *) NewAttr()
					|1: (* Bright *) Bright()
					|2: (* Dim *) Dim()
					|4: (* Underscore *) включиВоМнвоНаБитах( attr.special, Underscore )
					|5: (* Blink *) включиВоМнвоНаБитах( attr.special, Blink )
					|7: (* Reverse *) c := attr.bg; attr.bg := attr.fg; attr.fg := c
					|8: (* Hidden *) attr.fg := attr.bg
					иначе
						Log.пСтроку8("attr "); Log.пЦел64(attrs[i], 0); Log.пВК_ПС()
					всё;
					увел(i)
				кц
			кон SetAttributes;

			проц Draw*( canvas: WMG.Canvas );
			перем
				l: Line; i, j, dy, bottom: размерМЗ; attr: Attribute; char: Char;
				box: WMG.Rectangle;
			нач {единолично}
				canvas.Fill( canvas.clipRect, bg, WMG.ModeCopy );
				l := first;
				нцПока l # top делай
					l.t := матМинимум(цел16); l.b := матМинимум(цел16); l := l.next
				кц;
				attr := НУЛЬ; bottom := dY + rows*boxH;
				box.t := dY; box.b := dY + boxH; j := 0;

				нцПока (l # НУЛЬ) и (j < rows) и (box.b <= bottom) делай
					l.t := box.t; l.b := box.b;
					box.l := dX; box.r := dX + boxW; i := 0;
					нцПока i < cols делай
						char := l.data[i];
						если char.attr # attr то
							attr := char.attr;
							canvas.SetColor( attr.fg );
							canvas.SetFont( attr.fnt );
							dy := attr.fnt.GetDescent()
						всё;
						если attr.bg # bg то
							canvas.Fill( box, attr.bg, WMG.ModeCopy )
						всё;
						если char.char # 0 то
							attr.fnt.RenderChar( canvas, box.l, box.b-dy, симв32ИзКода(char.char) )
						всё;
						если Underscore в attr.special то
							canvas.Line( box.l, box.b-dy+1, box.r-1, box.b-dy+1, attr.fg, WMG.ModeCopy )
						всё;
						увел( i ); увел( box.l, boxW ); увел( box.r, boxW )
					кц;
					увел( j ); l := l.next;
					увел( box.t, boxH ); увел( box.b, boxH )
				кц;

				нцПока l # НУЛЬ делай
					l.t := матМаксимум(цел16); l.b := матМаксимум(цел16); l := l.next
				кц;

				если hasFocus и (cursor.ofs >= 0) и (cursor.ofs < cols) то
					l := cursor.line; box.t := l.t; box.b := l.b;
					если box.t < box.b то
						box.l := dX + cursor.ofs*boxW; box.r := box.l + boxW;
						canvas.Fill( box, WMG.RGBAToColor( 255, 0, 0, 192 ), WMG.ModeSrcOverDst )
					иначе
						FocusLost
					всё
				всё;
				если sel.beg.line # НУЛЬ то
					если sel.beg.line = sel.end.line то
						box.l := dX + sel.beg.ofs * boxW; box.r := dX + sel.end.ofs * boxW + boxW;
						box.t := sel.beg.line.t; box.b := sel.end.line.b;
						canvas.Fill( box, WMG.RGBAToColor( 0, 0, 255, 32 ), WMG.ModeSrcOverDst )
					иначе
						box.l := dX + sel.beg.ofs * boxW; box.r := dX + cols * boxW;
						box.t := sel.beg.line.t; box.b := sel.beg.line.b;
						canvas.Fill( box, WMG.RGBAToColor( 0, 0, 255, 32 ), WMG.ModeSrcOverDst );
						l := sel.beg.line.next;
						нцПока l # sel.end.line делай
							box.l := dX; box.r := dX + cols * boxW;
							box.t := l.t; box.b := l.b;
							canvas.Fill( box, WMG.RGBAToColor( 0, 0, 255, 32 ), WMG.ModeSrcOverDst );
							l := l.next
						кц;
						box.l := dX; box.r := dX + sel.end.ofs * boxW + boxW;
						box.t := sel.end.line.t; box.b := sel.end.line.b;
						canvas.Fill( box, WMG.RGBAToColor( 0, 0, 255, 32 ), WMG.ModeSrcOverDst )
					всё
				всё
			кон Draw;

			проц MoveCursor( dr, dc: размерМЗ );
			перем col, currrow: размерМЗ;
			нач
				col := GetCol() + dc;
				если col < 0 то  col := 0  всё;
				currrow := GetRow();
				если (currrow = scrollEnd) и (dr > 0) то
					если currrow < rows - 1 то  Scroll( ложь );  Goto( currrow, col )
					иначе Goto( currrow + 1, col )
					всё
				аесли (currrow = scrollBegin) и (dr < 0) то  Scroll( истина );  Goto( currrow, col )
				иначе  Goto( currrow + dr, col )
				всё
			кон MoveCursor;

			проц ESCSequence( ch: симв8; r: Потоки.Чтец );
			перем
				par: массив 4 из цел32; i, n: размерМЗ;
			нач
				r.чСимв8( ch );
				если ch = "[" то
					ch := r.ПодглядиСимв8(); n := 0;
					если ch = "?" то
						r.чСимв8( ch ); ch := r.ПодглядиСимв8();
						если (ch >= "0") и (ch <= "9") то
							нцДо
								r.чЦел32( par[n], ложь );  увел( n );
								r.чСимв8( ch )
							кцПри (n >= 4) или (ch # " ")
						всё
					аесли (ch >= "0") и (ch <= "9") то
						нцДо
							r.чЦел32( par[n], ложь ); увел( n );
							r.чСимв8( ch )
						кцПри (n >= 4) или (ch # ";")
					иначе
утв( ch < DEL );
						r.чСимв8( ch )
					всё;
					просей ch из
					|"A":
						если n = 1 то  MoveCursor( -par[0], 0 )  иначе  MoveCursor( -1, 0 )  всё
					|"B":
						если n = 1 то  MoveCursor( par[0], 0 )  иначе  MoveCursor( 1, 0 )  всё
					|"C":
						если n = 1 то  MoveCursor( 0, par[0] )  иначе  MoveCursor( 0, 1 )  всё
					|"D":
						если n = 1 то  MoveCursor( 0, -par[0] )  иначе  MoveCursor( 0, -1 )  всё
					|"H":
						если n = 2 то  Goto( par[0] - 1, par[1] - 1 )  иначе  Goto( 0, 0 )  всё
					|"J", "K":
						Erase( ch, par, n )
					|"h":
						если n = 1 то
							если par[0] = 1 то  включиВоМнвоНаБитах( mode, CursorKeyMode )
							аесли par[0] = 7 то  включиВоМнвоНаБитах( mode, AutoWrapMode )
							всё
						всё
					|"l":
						если n = 1 то
							если par[0] = 1 то  исключиИзМнваНаБитах( mode, CursorKeyMode )
							аесли par[0] = 7 то  исключиИзМнваНаБитах( mode, AutoWrapMode )
							всё
						всё
					|"m":
						SetAttributes( par, n )
					| "r":
						SetMargins( par[0], par[1] )
					иначе
Log.пВК_ПС;  Log.пСтроку8( "got unknown sequence ESC [ " );
i := 0;
нцПока i < n делай
	Log.пЦел64( par[i], 0 );  увел( i );
	если i < n то  Log.пСтроку8( " ; " )  всё
кц;
Log.пСимв8( ch );  Log.пВК_ПС;
					всё
				иначе
					просей ch из
					|"7":
						old.attr := attr;
						old.offs := GetCol();
						old.row := GetRow()
					|"8":
						если r.ПодглядиСимв8( ) = '#' то  r.чСимв8( ch )
						иначе  attr := old.attr;  Goto( old.row, old.offs )
						всё
					|"=":
						включиВоМнвоНаБитах( mode, AppKeypadMode )
					|">":
						исключиИзМнваНаБитах( mode, AppKeypadMode )
					|"D":
						если GetRow() = scrollEnd то  Scroll( ложь )
						иначе  Goto( GetRow() + 1, GetCol() )
						всё
					|"M":
						если GetRow() = scrollBegin то  Scroll( истина )
						иначе  Goto( GetRow() - 1, GetCol() )
						всё
					иначе
Log.пСтроку8("got unknown sequence ESC ");
если (ch >= ' ') и (ch <= '~') то  Log.пСимв8( "'" ); Log.пСимв8( ch ); Log.пСимв8( "'" )
иначе  Log.п16ричное( кодСимв8( ch ), 2 ); Log.пСимв8( 'X' )
всё;
Log.пВК_ПС;
					всё
				всё
			кон ESCSequence;


			проц Consume( ch: симв8; r: Потоки.Чтец );
			перем buf: массив 256 из симв8; i, n: размерМЗ;
			нач
				просей ch из
				|  0X: (* NUL *)
				|07X: Beep.Beep( 1000 )
				|08X: MoveCursor( 0, -1 )
				|09X: RightTab()
				|NL, 0BX, 0CX:
					MoveCursor( 1, -1000 )
				|CR:
					если r.ПодглядиСимв8() = NL то
						r.чСимв8( ch );
						MoveCursor( 1, -1000 )
					иначе
						MoveCursor( 0, -1000 )
					всё
				|ESC: ESCSequence( ch, r )
				|DEL: Delete()
				иначе (* iso-8859-1 *)
					buf[0] := ch;  i := 1;  n := r.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество();
					если n > 0 то
						если n > 127 то  n := 127  всё;
						ch := r.ПодглядиСимв8();
						нцПока (n > 0) и (ch >= ' ') и (ch <= '~') делай
							r.чСимв8( ch ); умень( n );
							buf[i] := ch; увел( i );
							если n > 0 то  ch := r.ПодглядиСимв8()  всё
						кц
					всё;
					WriteChars( buf, i )
				всё
			кон Consume;


			проц FocusReceived*;
			нач
				FocusReceived^();
				UpdateBox( cursor.line, cursor.ofs )
			кон FocusReceived;

			проц FocusLost*;
			нач
				FocusLost^();
				UpdateBox( cursor.line, cursor.ofs )
			кон FocusLost;

			проц LocateBox( x, y: размерМЗ; перем pos: Position );
			перем l: Line; ofs, i: размерМЗ;
			нач
				если x < dX то x := dX аесли x >= (dX + cols*boxW) то x := dX + cols*boxW-1 всё;
				если y < dY то y := dY аесли y >= (dY + rows*boxH) то y := dY + rows*boxH-1 всё;
				pos.line := НУЛЬ; pos.ofs := -1;
				l := top;
				нцПока (l # НУЛЬ) и ~((l.t <= y) и (l.b > y)) делай
					l := l.next
				кц;
				если l # НУЛЬ то
					ofs := 0; i := dX;
					нцПока (ofs < cols) и ~((i <= x) и ((i+boxW) > x)) делай
						увел(ofs); увел(i, boxW)
					кц;
					если ofs < cols то
						pos.line := l; pos.ofs := ofs
					всё
				всё
			кон LocateBox;



			проц PointerDown*( x, y: размерМЗ; keys: мнвоНаБитахМЗ );
			нач
				если (Left в keys) и hasFocus то
					LocateBox( x, y, sel.beg );  sel.end := sel.beg
				аесли Right в keys то
					ToWMCoordinates(x, y, x, y);
				иначе
					sel.beg.line := НУЛЬ;  sel.beg.ofs := -1;
					sel.end := sel.beg
				всё;
				UpdateAll()
			кон PointerDown;

			проц PointerMove*( x, y: размерМЗ; keys: мнвоНаБитахМЗ );
			перем pos: Position;
			нач
				если (Left в keys) и (sel.beg.line # НУЛЬ) то
					LocateBox(x, y, pos);
					если pos.line # НУЛЬ то
						если pos.line.t > sel.beg.line.t то
							sel.end := pos
						аесли (pos.line = sel.beg.line) и (pos.ofs >= sel.beg.ofs) то
							sel.end := pos
						всё;
						UpdateAll()
					всё
				всё
			кон PointerMove;


			проц PointerUp*( x, y: размерМЗ; keys: мнвоНаБитахМЗ );
			кон PointerUp;

			проц CursorKey( keySym: размерМЗ );
			нач
				w.пСимв8( ESC );
				если CursorKeyMode в mode то  w.пСимв8( "O" )
				иначе  w.пСимв8( "[" )
				всё;
				просей keySym из
				|0FF51H: w.пСимв8( "D" )
				|0FF52H: w.пСимв8( "A" )
				|0FF53H: w.пСимв8( "C" )
				|0FF54H: w.пСимв8( "B" )
				иначе
				всё;
				w.ПротолкниБуферВПоток()
			кон CursorKey;

			проц KeyEvent*( ucs: размерМЗ; flags: мнвоНаБитахМЗ; перем keySym: размерМЗ );
			нач
				если chan = НУЛЬ то  возврат  всё;

				если ~(Inputs.Release в flags) и hasFocus то
					если (keySym DIV 256) = 0 то
						w.пСимв8( симв8ИзКода( keySym ) );  w.ПротолкниБуферВПоток()
					аесли (keySym DIV 256) = 0FFH то
						просей keySym из
						|0FF51H .. 0FF54H:
							CursorKey(keySym)
						|0FF50H: (* Home *)
						|0FF55H: (* PgUp *)
						|0FF56H: (* PgDown *)
						|0FF57H: (* End *)
						|0FF63H: (* Insert *)
						|0FFFFH: (* Delete *)
						|0FF08H:
							w.пСимв8( DEL );  w.ПротолкниБуферВПоток()
						|0FF09H:
							w.пСимв8( 9X );  w.ПротолкниБуферВПоток()
						|0FF0DH:
							w.пСимв8( CR );  w.ПротолкниБуферВПоток()
						|0FF1BH:
							w.пСимв8( ESC );  w.ПротолкниБуферВПоток()
						|0FF8DH:
							если AppKeypadMode в mode то
								w.пСимв8( ESC ); w.пСимв8( "O" ); w.пСимв8( "M" )
							иначе
								w.пСимв8( CR )
							всё;
							w.ПротолкниБуферВПоток()
						иначе
						всё
					всё
				всё
			кон KeyEvent;

			проц Handle*( перем m : WMMessages.Message );
			нач
				если m.msgType = WMMessages.MsgKey то
					если m.y остОтДеленияНа 256 = 9 то  KeyEvent( m.x, m.flags, m.y )
					иначе  Handle^( m )
					всё;
				иначе Handle^( m )
				всё
			кон Handle;


			проц resized;
			перем l: Line; W, H, c, r, i: размерМЗ; d: Data; ch: Char;
			нач {единолично}
				W := bounds.GetWidth() - 2*Border;
				H := bounds.GetHeight() - 2*Border;
				c := W DIV BoxW; r := H DIV BoxH;
				boxW := W DIV c; boxH := H DIV r;
				dX := Border + (W - c*boxW) DIV 2;
				dY := Border + (H - r*boxH) DIV 2;

				SetOffsets;
				если c # cols то
					ch.attr := attr;  ch.char := 0;
					l := first;
					нцПока l # НУЛЬ делай
						нов( d, c );  i := 0;
						нцПока (i < c) и (i < cols) делай  d[i] := l.data[i];  увел( i )  кц;
						нцПока i < c делай  d[i] := ch;  увел( i )  кц;
						l.data := d; l := l.next
					кц
				всё;
				если (c # cols) или (r # rows) то
					если cursor.ofs >= c то  cursor.ofs := c - 1  всё;
					l := cursor.line;
					если l.b > (dY + r*boxH) то
						i := (l.b - (dY + r*boxH)) DIV boxH;
						l := top.next;
						нцПока (l # НУЛЬ) и (i > 0) делай  top := l;  l := l.next;  умень( i )  кц
					всё;
					если (rows # r) и (scrollEnd = rows - 1) то
						scrollEnd := r - 1;  scrollBottom := GetLine( r )
					всё;
					sel.beg.line := НУЛЬ;  cols := c;  rows := r;
				всё;
			кон resized;

			проц Resized*;
			нач
				Resized^();
				resized();
				если chan # НУЛЬ то  chan.WindowChange( cols, rows )  всё
			кон Resized;

			проц Initialize*;
			нач
				Initialize^();
				Resized;
				takesFocus.Set( истина );
				Invalidate()
			кон Initialize;

			проц SetChannel( c: SSHChannel );
			нач {единолично}
				chan := c;
				Потоки.НастройПисаря( w, chan.ЗапишиВПоток );	
				mode := {};
				chan.WindowChange( cols, rows )
			кон SetChannel;	
			
			
			проц &New*( col, row: размерМЗ; wc: WindowCloser );
			перем i: размерМЗ;
			нач
				Init();
				windowCloser := wc;
				rows := row;  cols := col;
				NewAttr();
				bg := WMG.RGBAToColor( 255, 255, 255, 255 );
				first := AppendLine( НУЛЬ );
				top := first;
				scrollBegin := 0;  scrollEnd := rows - 1;
				SetScrollRegion;
				cursor.line := top;  cursor.ofs := 0;
				boxW := 0;  boxH := 0;  dX := 0;  dY := 0;
				нов( tabs, cols + 1 );
				tabs[0] := ложь;  i := 1;
				нцПока i <= cols делай  tabs[i] := (i остОтДеленияНа 8) = 0;  увел( i )  кц;
			кон New;

			проц Setup;
			нач {единолично}
				дождись( chan # НУЛЬ );
			кон Setup;
			
			проц ChannelReader;
			перем ch: симв8;
				r: Потоки.Чтец;
			нач
				Потоки.НастройЧтеца( r, chan.ПрочтиИзПотока );
				r.чСимв8( ch ); 
				нцПока ch # 0X делай
					Consume( ch, r );
					r.чСимв8( ch );
				кц;
			кон ChannelReader;

		нач {активное}
			Setup();
			ChannelReader;
			windowCloser( )
		кон Frame;

	
		Window = окласс( WMComponents.FormWindow )
		перем
			toolbar: WMStandardComponents.Panel;
			address, user: WMEditors.Editor;
			connect, help : WMStandardComponents.Button;

			sshConn: SSHAuthorize.Connection;
			channel: SSHChannel;
			frame: Frame;

			проц &New;
			перем vc: WMComponents.VisualComponent;
			нач
				vc := CreateForm();
				Init( vc.bounds.GetWidth(), vc.bounds.GetHeight(), ложь );
				SetContent( vc );
				SetTitle( WMWindowManager.NewString( "SSH Terminal" ) );
				WMWindowManager.DefaultAddWindow( сам )
			кон New;


			проц CreateForm( ): WMComponents.VisualComponent;
			перем
				panel: WMStandardComponents.Panel;
				label : WMStandardComponents.Label;
			нач
				нов( panel );
					panel.bounds.SetWidth( 2*Border + TerminalWidth*BoxW );
					panel.bounds.SetHeight( 2*Border + TerminalHeight*BoxH + 20 );
					panel.fillColor.Set( цел32( 0FFFFFFFFH ) );

				нов( toolbar );
					toolbar.bounds.SetHeight( 20 );
					toolbar.alignment.Set( WMComponents.AlignTop );
					toolbar.fillColor.Set( цел32( 0CCCCCCFFH ) );

				нов( label );
					label.bounds.SetWidth( 40 );
					label.alignment.Set( WMComponents.AlignLeft );
					label.caption.SetAOC( "Host" );
					label.textColor.Set( 0000000FFH );
				toolbar.AddContent(label);

				нов( address );
					address.bounds.SetWidth( 250 );
					address.alignment.Set( WMComponents.AlignLeft );
					address.tv.textAlignV.Set(WMG.AlignCenter);
					address.multiLine.Set( ложь );
					address.fillColor.Set( цел32( 0FFFFFFFFH ) );
					address.tv.showBorder.Set( истина );
					address.tv.borders.Set( WMRectangles.MakeRect( 3,3,1,1 ) );
					address.onEnter.Add( ConnectHandler );
					address.SetAsString( lastHostname );
				toolbar.AddContent( address );

				нов( label );
					label.bounds.SetWidth( 40 );
					label.alignment.Set( WMComponents.AlignLeft );
					label.caption.SetAOC( "User" );
					label.textColor.Set( 0000000FFH );
				toolbar.AddContent( label );

				нов( user );
					user.bounds.SetWidth( 100 );
					user.alignment.Set( WMComponents.AlignLeft );
					user.tv.textAlignV.Set(WMG.AlignCenter);
					user.multiLine.Set( ложь );
					user.fillColor.Set( цел32( 0FFFFFFFFH ) );
					user.tv.showBorder.Set( истина );
					user.tv.borders.Set( WMRectangles.MakeRect( 3,3,1,1 ) );
					user.onEnter.Add( ConnectHandler );
					user.SetAsString( lastUsername );
				toolbar.AddContent( user );


				нов( connect );
					connect.bounds.SetWidth( 100 );
					connect.alignment.Set( WMComponents.AlignLeft );
					connect.caption.SetAOC( "Connect" );
					connect.onClick.Add( ConnectHandler );
				toolbar.AddContent( connect );

				нов( help );
					help.bounds.SetWidth( 100 );
					help.alignment.Set( WMComponents.AlignRight );
					help.caption.SetAOC( " Help " );
					help.onClick.Add( HelpHandler );

				toolbar.AddContent( help );
				panel.AddContent( toolbar );

				нов( frame, TerminalWidth, TerminalHeight, Close );
				frame.alignment.Set( WMComponents.AlignClient );
				panel.AddContent( frame );
				Init( panel.bounds.GetWidth(), panel.bounds.GetHeight(), ложь );

				возврат panel
			кон CreateForm;


			проц Connected( ): булево;
			нач
				возврат (channel # НУЛЬ) и (channel.state = SSHChannels.Open)
			кон Connected;


			проц ConnectHandler( sender, data: динамическиТипизированныйУкль );
			перем host, uid: массив 64 из симв8;
			нач
				address.GetAsString( host );
				если host = "" то
					Beep.Beep( 1000 );
					Log.пСтроку8( "no hostname specified" ); Log.пВК_ПС;  возврат 
				всё;
				копируйСтрокуДо0( host, lastHostname );
				user.GetAsString( uid );
				если uid = "" то
					Beep.Beep( 1000 );
					Log.пСтроку8( "user name missing" ); Log.пВК_ПС;  возврат 
				всё;
				копируйСтрокуДо0( uid, lastUsername );
				если Connected() то
					Beep.Beep( 1000 );
					Log.пСтроку8( "already connected" ); Log.пВК_ПС;  возврат 
				всё;
				SetDefaultLogin( host, uid );

				sshConn := SSHAuthorize.OpenConnection( host, uid );
				если sshConn # НУЛЬ то
					channel := SSHChannels.OpenSession( sshConn, истина (*interactive *) );
					если channel # НУЛЬ то 
						frame.SetChannel( channel );
					иначе
						sshConn.Disconnect( 11, "" );
					всё
				всё
			кон ConnectHandler;

			проц HelpHandler( sender, data: динамическиТипизированныйУкль );
			перем res: целМЗ; msg: массив 128 из симв8;
			нач
				Commands.Call( "PAR Notepad.Open SSH.Tool ~", {}, res, msg );
				если res # Commands.Ok то  Log.пСтроку8( msg ); Log.пВК_ПС  всё;
			кон HelpHandler;


			проц Close*;
			перем timer: Kernel.Timer;
			нач
				если sshConn # НУЛЬ то  
					нов( timer ); 
					если Connected( ) то  channel.Закрой  всё;
					sshConn.Disconnect( 11, "bye bye" );
					timer.Sleep( 200 );
				всё;
				Close^
			кон Close;

	кон Window;

	проц GetDefaultLogin;
	перем f: Files.File; r: Files.Reader;
	нач
		f := Files.Old( DefLogin );
		если f # НУЛЬ то
			Files.OpenReader( r, f, 0 );
			неважно r.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( lastHostname );
			неважно r.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( lastUsername )
		всё
	кон GetDefaultLogin;
	
	проц SetDefaultLogin( конст host, uid: массив из симв8 );
	перем f: Files.File; w: Files.Writer;
	нач
		f := Files.New( DefLogin ); Files.OpenWriter( w, f, 0 );
		w.пСтроку8( host ); w.пСимв8( ' ' ); w.пСтроку8( uid ); w.пВК_ПС;
		w.ПротолкниБуферВПоток;
		Files.Register( f ); f.Close
	кон SetDefaultLogin;

	проц Open*;
	перем inst: Window;
	нач
		GetDefaultLogin;
		нов( inst );
	кон Open;

нач
	lastHostname := "x02.math.uni-bremen.de";
	lastUsername := "fld"
кон SSHTerminal.



SSHGlobals.SetDebug 1 ~

SSHTerminal.Open ~

System.Free SSH SSHTerminal~



home, end, delete, insert, pageup, pagedown

emacs
pine
pico
lynx


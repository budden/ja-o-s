модуль UsbVarTdAlloc; (** AUTHOR "Timothée Martiel"; PURPOSE "Variable-size data-structure allocator for EHCI."; *)

использует ЭВМ;

конст
	AllocSize = 1024 * 1024;
	AllocAlign = 1024 * 1024;

тип
	(**
		Buffer descriptor
	 *)
	TdBuffer = окласс
	перем
		buffer: укль на массив из симв8;
		used: укль на массив из мнвоНаБитахМЗ;
		ofs: цел32;
		next: TdBuffer;

		проц & Init (block: цел32);
		перем
			bitmaskSize: цел32;
		нач
			нов(buffer, AllocSize + AllocAlign);
			ofs := AllocAlign - адресОт(buffer[0]) остОтДеленияНа AllocAlign;
			bitmaskSize := AllocSize DIV (block * SetSize);
			нов(used, bitmaskSize)
		кон Init;

		проц Used(block: цел32): булево;
		нач
			возврат (block остОтДеленияНа SetSize) в used[block DIV SetSize];
		кон Used;

		проц SetUsed(block: цел32);
		нач
			включиВоМнвоНаБитах(used[block DIV SetSize], block остОтДеленияНа SetSize);
		кон SetUsed;

		(* faster version of SetUsed for blocks
		PROCEDURE SetUsedR(from, to: SIGNED32);
		VAR startSet, stopSet, startBit, stopBit: SIGNED32;
		BEGIN
			IF to < from THEN RETURN END;
			startBit := from MOD SetSize;
			stopBit := to MOD SetSize;
			startSet := from DIV SetSize;
			stopSet := to DIV SetSize;
			IF startSet < stopSet THEN
				used[startSet] := used[startSet] + {startBit .. MAX(SET)};
				INC(startSet);
				WHILE startSet < stopSet DO
					used[startSet] := {MIN(SET)..MAX(SET)};
					INC(startSet);
				END;
				used[stopSet] := used[stopSet] + {MIN(SET) .. stopBit};
			ELSE
				used[stopSet] := used[stopSet] + {startBit .. stopBit};
			END;
		END SetUsedR;
		*)

		проц SetFree(block: цел32);
		нач
			исключиИзМнваНаБитах(used[block DIV SetSize], block остОтДеленияНа SetSize);
		кон SetFree;

		(* faster version of SetFree for blocks
		PROCEDURE SetFreeR(from, to: SIGNED32);
		VAR startSet, stopSet, startBit, stopBit: SIGNED32;
		BEGIN
			IF to < from THEN RETURN END;
			startBit := from MOD SetSize;
			stopBit := to MOD SetSize;
			startSet := from DIV SetSize;
			stopSet := to DIV SetSize;
			IF startSet < stopSet THEN
				used[startSet] := used[startSet] - {startBit .. MAX(SET)};
				INC(startSet);
				WHILE startSet < stopSet DO
					used[startSet] := {};
					INC(startSet);
				END;
				used[stopSet] := used[stopSet] - {MIN(SET) .. stopBit};
			ELSE
				used[stopSet] := used[stopSet] - {startBit .. stopBit};
			END;
		END SetFreeR;
		*)


	кон TdBuffer;

	(**
		Allocator.

		The allocator is created with a page size and a block size. It can then allocate memory blocks with a granularity of the block size.
		Each allocated block is guaranteed not to cross a page boundary.

		Allocated blocks must be freed manually.
	*)
	конст SetSize = размер16_от(мнвоНаБитахМЗ) * 8;

	тип Allocator * = окласс
	перем
		tdBuffers: TdBuffer;
		pageSize, blockSize, bitmaskSize: цел32;

		проц & Setup * (pageSize, blockSize: цел32);
		нач
			утв(SetSize = 32);
			утв((AllocSize остОтДеленияНа blockSize) остОтДеленияНа SetSize = 0);
			сам.pageSize := pageSize;
			сам.blockSize := blockSize;
			bitmaskSize := AllocSize DIV (blockSize * SetSize)
		кон Setup;

		(** Allocate memory for a TD or a QH of the given size. The size must be a multiple of 32. *)
		проц Allocate * (size: размерМЗ): адресВПамяти;
		перем
			buf: TdBuffer;
			start, pos, count: цел32;
			adr: адресВПамяти;

			(** Allocate a new TD buffer and mark as used the last 32-byte block before a 4kB page boundary. *)
			проц AllocateBuffer;
			перем
				buf: TdBuffer;
				count, mod: цел32;
			нач
				(* No buffer found: allocate a new one *)
				нов(buf, blockSize);
				(*NEW(buf.buffer, AllocSize + AllocAlign);
				NEW(buf.used, bitmaskSize);*)

				buf.next := tdBuffers;
				tdBuffers := buf;
				count := 0;
				mod := адресОт(buf.buffer[0]) остОтДеленияНа AllocAlign;
				если mod # 0 то
					buf.ofs := AllocAlign - mod
				всё;

				ЭВМ.DisableDCacheRange(адресОт(buf.buffer[buf.ofs]), AllocSize);

				(* Remove last 32-byte block before a 4kB page boundary from free blocks *)
				нц
					если count >= AllocSize DIV blockSize то прервиЦикл всё;
					если (адресОт(buf.buffer[buf.ofs + count * blockSize]) остОтДеленияНа pageSize) = pageSize - blockSize то
						buf.SetUsed(count);
					всё;
					увел(count)
				кц;
			кон AllocateBuffer;


		нач {единолично}
			утв(size остОтДеленияНа blockSize = 0);
			size := size DIV blockSize;

			buf := tdBuffers;
			нц
				если buf = НУЛЬ то
					AllocateBuffer;
					buf := tdBuffers;
					утв(buf # НУЛЬ)
				всё;
				count := 0;
				pos := 0;
				start := pos;
				нцПока (count < size) и (pos < bitmaskSize) делай
					если buf.Used(pos) то
						count := 0;
						start := pos + 1;
					иначе
						увел(count);
					всё;
					увел(pos);
				кц;

				если count = size то прервиЦикл всё;
				buf := buf.next
			кц;

			утв(buf # НУЛЬ);

			adr := адресОт(buf.buffer[buf.ofs + start*blockSize]);

			(* faster version:
			buf.SetUsedR(start, start+count-1);
			*)
			нцПока(count > 0) делай
				утв(~buf.Used(start));
				buf.SetUsed(start);
				увел(start); умень(count);
			кц;
			ЭВМ.ЗаполниДиапазонБайтовПредставлениемЦел32(adr, size * blockSize, 0);
			возврат adr;
		кон Allocate;

		(** Marks a TD as free, so that its memory can be used again *)
		проц Free * (td: адресВПамяти; size: размерМЗ);
		перем
			buf: TdBuffer;
			adr: адресВПамяти;
			slot: цел32;
		нач {единолично}
			утв(size остОтДеленияНа blockSize = 0);
			size := size DIV blockSize;

			buf := tdBuffers;
			нц
				если buf = НУЛЬ то прервиЦикл всё;
				adr := адресОт(buf.buffer[buf.ofs]);
				если (adr <= td) и (td < adr + AllocSize) то прервиЦикл всё;
				buf := buf.next
			кц;
			утв(buf # НУЛЬ); (* Not a TD *)

			slot := (td - adr) DIV blockSize;
			(* faster version:
			buf.SetFreeR(slot, slot+size-1);
			*)
			нцПока (size > 0) делай
				утв(buf.Used(slot));
				buf.SetFree(slot);
				увел(slot); умень(size);
			кц;
		кон Free;
	кон Allocator;


	StaticAllocator * = окласс
	перем
		buffers: TdBuffer;
		size: цел32;

		проц & Setup * (allocSize: цел32);
		нач
			size := allocSize
		кон Setup;

		проц Allocate * (): адресВПамяти;
		нач {единолично}
		кон Allocate;

		проц Free * (td: адресВПамяти);
		нач {единолично}
		кон Free;
	кон StaticAllocator;
(*VAR
	padding: POINTER TO ARRAY OF CHAR;
BEGIN
	NEW(padding, 1024 * 1024)*)
кон UsbVarTdAlloc.

(* test module, uncomment for running a randomized test

MODULE TestUsbVarTdAlloc; (** AUTHOR ""; PURPOSE ""; *)

IMPORT UsbVarTdAlloc, Random;

PROCEDURE Test*;
VAR allocator: UsbVarTdAlloc.Allocator; adr: POINTER TO ARRAY OF ADDRESS;
gen: Random.Generator; i,j, k: SIGNED32; size: POINTER TO ARRAY OF SIZE;
BEGIN
	NEW(allocator, 4096, 32);
	NEW(gen);
	NEW(adr, 1024); NEW(size, 1024);
	FOR j := 0 TO 100 DO
		FOR i := 0 TO LEN(adr)-1  DO
			size[i] := 32+gen.Dice(120)*32;
			adr[i] := allocator.Allocate(size[i]);
			ASSERT(adr[i] MOD 32 = 0);
			ASSERT(adr[i] DIV 4096 = (adr[i] + size[i]) DIV 4096);
			FOR k := 0 TO i-1 DO
				ASSERT(adr[k] # adr[i]);
			END;
		END;
		FOR i := 0 TO LEN(adr)-1 DO
			allocator.Free(adr[i], size[i]);
		END;
		TRACE(j);
	END;
	TRACE("done");
END Test;



END TestUsbVarTdAlloc.

System.Free TestUsbVarTdAlloc UsbVarTdAlloc ~

TestUsbVarTdAlloc.Test ~

*)

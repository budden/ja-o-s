модуль WebHTTPServer; (** AUTHOR "pjm/tf/be"; PURPOSE "HTTP/1.1 Server";*)

использует
	ЛогЯдра, ЭВМ, Kernel, Objects, WebHTTP, AosLog := TFLog, Modules, Потоки, Files,
	IP, TCP, TCPServices, Classes := TFClasses, Clock, Dates, Строки8;

конст
	Ok* = TCPServices.Ok;
	Error* = -1;

	Major* = 1; Minor* = 1;

	FileBufSize = 4096;
	ServerVersion* = "A2 HTTP Server/1.0";
	DocType* = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
	Tab = 09X;

	Timeout = 300*1000;  (* [ms] timeout for keep-alive *)

	MaxErrors = 10;

	Log = ложь;


тип
	Name* = массив 64 из симв8;

	(** abstract HTTP plugin *)
	HTTPPlugin* = окласс
	перем
		name*: Name;

		проц &Init*(конст name: Name);
		нач копируйСтрокуДо0(name, сам.name)
		кон Init;

		(** if CanHandle returns TRUE, the Handler procedure will be called *)
		проц CanHandle* (host: Host; перем header : WebHTTP.RequestHeader; secure: булево) : булево;
		нач СТОП(301);
			возврат ложь
		кон CanHandle;

		(** default LocateResource method *)
		проц LocateResource*(host: Host; перем header: WebHTTP.RequestHeader; перем reply: WebHTTP.ResponseHeader; перем f: Files.File);
		перем
			name, ext: Files.FileName; i, d, t: цел32; modsince: массив 32 из симв8;
			path : массив 1024 из симв8;

			проц Add(конст s: массив из симв8);
			перем j, k: размерМЗ; ch: симв8;
			нач
				j := 0; k := 0;
				нц
					если i = длинаМассива(name) то reply.statuscode := WebHTTP.RequestURITooLong; прервиЦикл всё;
					ch := s[j];
					если ch = "." то k := 0 всё;
					name[i] := ch; ext[k] := ch;
					если ch = 0X то прервиЦикл всё;
					увел(i); увел(j); увел(k)
				кц;
			кон Add;

		нач
			i := 0; reply.statuscode := WebHTTP.OK;
			Add(host.prefix); WebHTTP.GetPath(header.uri, path); Add(path);
			если (reply.statuscode = WebHTTP.OK) то
				f := Files.Old(name);
				если (f # НУЛЬ) и (Files.Directory в f.flags) то (* do not send directory offals *)
					Строки8.Склей("http://", header.host, reply.location);
					Строки8.ПодклейВСтрокуХвост(reply.location, header.uri);
					Строки8.ПодклейВСтрокуХвост(reply.location, "/");
					reply.statuscode := WebHTTP.ObjectMoved
				иначе
					если (name[i-1] = "/") то
						Строки8.Склей("http://", header.host, reply.contentlocation);
						Строки8.ПодклейВСтрокуХвост(reply.contentlocation, header.uri);
						Строки8.ПодклейВСтрокуХвост(reply.contentlocation, host.default);
						Add(host.default)
					всё;

					если (reply.statuscode = WebHTTP.OK) то
						f := Files.Old(name);
						если f # НУЛЬ то
							f.GetDate(t, d);
							Строки8.ПишиДатуВремяВСтроку(WebHTTP.DateTimeFormat, Dates.OberonToDateTime(d, t), reply.lastmodified);
							если WebHTTP.GetAdditionalFieldValue(header.additionalFields, "If-Modified-Since", modsince) и
								(modsince = reply.lastmodified)
							то
								reply.statuscode := WebHTTP.NotModified;
								f := НУЛЬ
							иначе
								(* TODO: move to Configuration.XML / separate plugins *)
								если ext = ".html" то копируйСтрокуДо0("text/html; charset=utf-8", reply.contenttype)
								аесли ext = ".txt" то копируйСтрокуДо0("text/plain", reply.contenttype)
								аесли ext = ".css" то копируйСтрокуДо0("text/css", reply.contenttype)
								аесли ext = ".gif" то копируйСтрокуДо0("image/gif", reply.contenttype)
								аесли ext = ".jpg" то копируйСтрокуДо0("image/jpeg", reply.contenttype)
								аесли ext = ".pdf" то копируйСтрокуДо0("application/pdf", reply.contenttype)
								аесли ext = ".xsl" то копируйСтрокуДо0("text/xsl", reply.contenttype)
								аесли ext = ".xml" то копируйСтрокуДо0("text/xml", reply.contenttype)
								иначе копируйСтрокуДо0("application/octet-stream", reply.contenttype)
								всё
							всё
						иначе
							reply.statuscode := WebHTTP.NotFound; копируйСтрокуДо0("text/html", reply.contenttype);
							f := Files.Old(host.error);
						всё
					всё
				всё
			всё
		кон LocateResource;

		(* handles a HTTP request *)
		проц Handle*(host: Host; перем request: WebHTTP.RequestHeader; перем reply: WebHTTP.ResponseHeader;
			перем in: Потоки.Чтец; перем out: Потоки.Писарь);
		нач СТОП(301)
		кон Handle;
	кон HTTPPlugin;

	(* default plugin for all hosts. Each host has this default plugin *)
	DefaultPlugin = окласс(HTTPPlugin)

		проц {перекрыта}CanHandle*(host : Host; перем header: WebHTTP.RequestHeader; secure : булево): булево;
		нач возврат истина
		кон CanHandle;

		проц {перекрыта}Handle*(host: Host; перем request: WebHTTP.RequestHeader; перем reply: WebHTTP.ResponseHeader;
			перем in: Потоки.Чтец; перем out: Потоки.Писарь);
		перем f: Files.File; fr: Files.Reader; c: WebHTTP.ChunkedOutStream; w: Потоки.Писарь;
		нач
			если (request.method в {WebHTTP.GetM, WebHTTP.HeadM}) то
				LocateResource(host, request, reply, f);
				если Log то
					WebHTTP.LogRequestHeader(log, request);
					WebHTTP.LogResponseHeader(log, reply)
				всё;

				если (reply.statuscode = WebHTTP.OK) или (reply.statuscode = WebHTTP.NotFound) то
					если (f # НУЛЬ) то
						reply.contentlength := f.Length()(цел32);
						WebHTTP.SendResponseHeader(reply, out);
						если (request.method = WebHTTP.GetM) то
							Files.OpenReader(fr, f, 0);
							SendData(fr, out)
						всё
					иначе
						reply.statuscode := WebHTTP.NotFound;
						(*WebHTTP.SendResponseHeader(reply, out);*)

						если (request.method = WebHTTP.GetM) то
							нов(c, w, out, request, reply);
							WebHTTP.SendResponseHeader(reply, out);
							WebHTTP.WriteHTMLStatus(reply, w);
							w.ПротолкниБуферВПоток;
							c.Close
						(* *) иначе WebHTTP.SendResponseHeader(reply, out);
						всё
					всё
				аесли (reply.statuscode = WebHTTP.NotModified) то
					WebHTTP.SendResponseHeader(reply, out)
				аесли (reply.statuscode = WebHTTP.ObjectMoved) то
					(*WebHTTP.SendResponseHeader(reply, out);*)
					если (request.method = WebHTTP.GetM) то
						нов(c, w, out, request, reply);
						WebHTTP.SendResponseHeader(reply, out);
						WebHTTP.WriteHTMLStatus(reply, w);
						w.ПротолкниБуферВПоток;
						c.Close
					(* *) иначе WebHTTP.SendResponseHeader(reply, out);
					всё
				всё
			иначе
				reply.statuscode := WebHTTP.NotImplemented;
				WebHTTP.WriteStatus(reply, out)
			всё
		кон Handle;
	кон DefaultPlugin;

	Statistics = окласс
	перем
		bucket : цел32;
		secondBuckets: массив 60 из цел32;
		timer : Kernel.Timer;
		avg : цел32;
		alive : булево;
		logCounter: цел32;

		проц Hit;
		нач {единолично}
			увел(secondBuckets[bucket]);
			увел(nofRequests)
		кон Hit;

		проц Update;
		нач {единолично}
			avg := avg + secondBuckets[bucket];
			bucket := (bucket + 1) остОтДеленияНа 60;
			avg := avg - secondBuckets[bucket];
			secondBuckets[bucket] := 0;
			requestsPerMinute := avg;

			logCounter := (logCounter + 1) остОтДеленияНа 40H;
			если (logCounter = 0) то
				FlushW3CLog
			всё
		кон Update;

		проц Kill;
		нач
			alive := ложь;
			timer.Wakeup
		кон Kill;

	нач {активное}
		нов(timer); alive := истина;
		нцПока alive делай
			timer.Sleep(1000);
			Update
		кц;
	кон Statistics;

	HostList* = окласс
	перем
		host*: Host;
		next*: HostList;
	кон HostList;

	Host* = окласс
	перем
		name-: Name;
		plugins : Classes.List;
		prefix-, default-, error-: Files.FileName;

		проц &Init*(конст name: массив из симв8);
		нач
			копируйСтрокуДо0(name, сам.name);
			копируйСтрокуДо0("", prefix);
			копируйСтрокуДо0("index.html", default);
			копируйСтрокуДо0("error.html", error);

			нов(plugins);
			(* install default plugin *)
			plugins.Add(defaultPlugin);
		кон Init;

		проц AddPlugin*(pi : HTTPPlugin);
		нач {единолично}
			если plugins.IndexOf(pi) >= 0 то ЛогЯдра.пСтроку8("Plugin already plugged in"); ЛогЯдра.пВК_ПС
			иначе
				plugins.Add(pi)
			всё
		кон AddPlugin;

		проц RemovePlugin*(pi : HTTPPlugin);
		нач {единолично}
			plugins.Remove(pi)
		кон RemovePlugin;

		проц SetPrefix*(конст Prefix: массив из симв8);
		нач {единолично}
			копируйСтрокуДо0(Prefix, prefix)
		кон SetPrefix;

		проц SetDefault*(конст Default: массив из симв8);
		нач {единолично}
			копируйСтрокуДо0(Default, default)
		кон SetDefault;

		проц SetError*(конст Error: массив из симв8);
		нач {единолично}
			копируйСтрокуДо0(Error, error)
		кон SetError;

		проц Handle*(
			перем request: WebHTTP.RequestHeader; перем reply: WebHTTP.ResponseHeader;
			перем in: Потоки.Чтец; перем out: Потоки.Писарь; secure : булево
		);
		перем i: размерМЗ; pi: HTTPPlugin; p: динамическиТипизированныйУкль; exit: булево;
		нач
			нач {единолично}
				exit := ложь;
				i := plugins.GetCount()-1;
				нцПока (i >= 0) и (~exit) делай
					p := plugins.GetItem(i);
					если p(HTTPPlugin).CanHandle(сам, request, secure) то pi := p(HTTPPlugin); exit := истина всё;
					умень(i);
				кц;
			кон;
			если pi # НУЛЬ то
				если Log то
					log.String("request handled by "); log.String(pi.name); log.Ln
				всё;
				pi.Handle(сам, request, reply, in, out);
			иначе
				СТОП(99)
			всё;
		кон Handle;

	кон Host;

	HTTPAgent = окласс (TCPServices.Agent)
	перем
		res: целМЗ;
		len : размерМЗ;
		body, closeRequested: булево;
		out: Потоки.Писарь; in, inR: Потоки.Чтец;
		o : динамическиТипизированныйУкль;
		h, th : Host;
		i : размерМЗ;
		request : WebHTTP.RequestHeader;
		reply: WebHTTP.ResponseHeader;
		value: массив 128 из симв8;
		timeout: Objects.Timer;
		dechunk: WebHTTP.ChunkedInStream;
		consecutiveErrors: размерМЗ;
		secure : булево;
		listenerProc : ListenerProc;

		проц HandleTimeout;
		нач client.Закрой
		кон HandleTimeout;

	нач {активное}
		нов(timeout);
		(* open streams *)
		Потоки.НастройЧтеца(in, client.ПрочтиИзПотока);
		Потоки.НастройПисаря(out, client.ЗапишиВПоток);
		ЭВМ.атомарноУвел(nofConnects);

		(* read request *)
		request.fadr := client.fip;
		request.fport := client.fport;

		consecutiveErrors := 0;

		нцДо
			Objects.SetTimeout(timeout, HandleTimeout, Timeout);
			WebHTTP.ParseRequest(in, request, res, log);
			если Log то WebHTTP.LogRequestHeader(log,request) всё;
			Objects.CancelTimeout(timeout);
			если (client.state = TCP.Established) то
				если (Строки8.НайдиПодстроку("hunked", request.transferencoding) > 0) то
					нов(dechunk, in, inR)
				иначе
					inR := in
				всё;

					(* handle request *)
				GetDefaultResponseHeader(request, reply);
				len := 0; body := ложь;
				hitStat.Hit;
				если (res = WebHTTP.OK) то
					i := 0; нцПока (request.host[i] # 0X) и (request.host[i] # ":") делай увел(i) кц;
					request.host[i] := 0X;
					h := defaultHost;
					hosts.Lock;
					i := hosts.GetCount()-1;
					нцПока (i >= 0) делай
						o := hosts.GetItem(i); th := o(Host);
						если Строки8.ПодходитЛиПодМаскуИмениФайла¿(th.name, request.host) то h := th; i := 0 всё;
						умень(i)
					кц;
					hosts.Unlock;
					если Log то
						log.String(request.uri); log.String(" handled by ");
						если (h.name = "") то log.String(" default host")
						иначе log.String(h.name)
						всё;
						log.Ln
					всё;
					h.Handle(request, reply, inR, out, secure);
					listenerProc := listener;
					если (listenerProc # НУЛЬ) то
						listenerProc(request, reply);
					всё;
				иначе
					reply.statuscode := цел32( res ); (*! result type *)
					WebHTTP.WriteStatus(reply, out)
				всё;

				out.ПротолкниБуферВПоток; (*PH*)(* ignore out.res *)

				если logEnabled то W3CLog(request, reply) всё;

				если WebHTTP.GetAdditionalFieldValue(request.additionalFields, "Connection", value) то
					closeRequested := Строки8.НайдиПодстроку("lose", value) > 0
				иначе
					closeRequested := ложь
				всё;

				если (reply.statuscode >= 400) то
					увел(consecutiveErrors);
					если (consecutiveErrors = MaxErrors) то client.Закрой всё
				иначе
					consecutiveErrors := 0
				всё;

			всё
		кцПри closeRequested или ((request.maj = 1) и (request.min = 0)) или (client.state # TCP.Established);
		Terminate
	кон HTTPAgent;

	ListenerProc* = проц {делегат} (request : WebHTTP.RequestHeader; response : WebHTTP.ResponseHeader);

перем
	http: TCPServices.Service;
	https: TCPServices.TLSService;
	hosts : Classes.List;
	log : AosLog.Log;
	hitStat : Statistics;
	nofRequests* : цел32;
	requestsPerMinute* : цел32;
	nofConnects* : цел32;
	defaultHost : Host;
	defaultPlugin: DefaultPlugin;

	logEnabled : булево;
	logWriter : Потоки.Писарь;
	logFile : Files.File;

	listener* : ListenerProc;

проц GetRequests*():цел32;
нач
	возврат nofRequests
кон GetRequests;

проц NewHTTPAgent(c: TCP.Connection; s: TCPServices.Service): TCPServices.Agent;
перем a: HTTPAgent;
нач
	нов(a, c, s); a.secure := ложь; возврат a
кон NewHTTPAgent;

проц NewHTTPSAgent(c: TCP.Connection; s: TCPServices.Service): TCPServices.Agent;
перем a: HTTPAgent;
нач
	нов(a, c, s); a.secure := истина; возврат a
кон NewHTTPSAgent;

проц OpenW3CLog(конст fn: массив из симв8);
перем w : Files.Writer;
нач
	logFile := Files.Old(fn);
	если logFile = НУЛЬ то
		logFile := Files.New(fn); Files.Register(logFile);
		Files.OpenWriter(w, logFile, 0);
		w.пСтроку8("#Version: 1.0"); w.пВК_ПС;
		w.пСтроку8("#Fields: date"); w.пСимв8(Tab);
		w.пСтроку8("time"); w.пСимв8(Tab);
		w.пСтроку8("cs-method"); w.пСимв8(Tab);
		w.пСтроку8("cs(host)"); w.пСимв8(Tab);
		w.пСтроку8("cs-uri"); w.пСимв8(Tab);
		w.пСтроку8("x-result"); w.пСимв8(Tab);
		w.пСтроку8("c-ip"); w.пСимв8(Tab);
		w.пСтроку8("cs(user-agent)"); w.пСимв8(Tab);
		w.пСтроку8("cs(referer)"); w.пВК_ПС
	иначе
		Files.OpenWriter(w, logFile, logFile.Length())
	всё;
	logWriter := w;
	logEnabled := истина
кон OpenW3CLog;

проц W3CLog(request : WebHTTP.RequestHeader; reply: WebHTTP.ResponseHeader);
перем time, date: цел32; s: массив 36 из симв8;
нач {единолично}
	Clock.Get(time, date);
	logWriter.пДатуОберонаISO( -1, date); logWriter.пСимв8(Tab);
	logWriter.пДатуОберонаISO(time, -1); logWriter.пСимв8(Tab);
	WebHTTP.GetMethodName(request.method,s); logWriter.пСтроку8(s);
	logWriter.пСимв8(Tab);
	если request.host # "" то logWriter.пСтроку8(request.host) иначе logWriter.пСтроку8("-") всё; logWriter.пСимв8(Tab);
	если request.uri # "" то logWriter.пСтроку8(request.uri) иначе logWriter.пСтроку8("-") всё; logWriter.пСимв8(Tab);
	logWriter.пЦел64(reply.statuscode, 1); logWriter.пСимв8(Tab);
	IP.AdrToStr(request.fadr, s); logWriter.пСтроку8(s); logWriter.пСимв8(Tab);
	если request.useragent # "" то logWriter.пСтроку8(request.useragent) иначе logWriter.пСтроку8("-") всё; logWriter.пСимв8(Tab);
	если request.referer # "" то  logWriter.пСтроку8(request.referer) иначе logWriter.пСтроку8( "-") всё; logWriter.пСимв8(Tab);
	logWriter.пВК_ПС
кон W3CLog;

проц FlushW3CLog*;
нач
	если logEnabled то
		logWriter.ПротолкниБуферВПоток; logFile.Update
	всё
кон FlushW3CLog;

проц GetDefaultResponseHeader*(перем r: WebHTTP.RequestHeader; перем h: WebHTTP.ResponseHeader);
нач
	h.maj := r.maj; h.min := r.min;
	копируйСтрокуДо0(ServerVersion, h.server);
	h.statuscode := WebHTTP.OK;
	Строки8.ПишиДатуВремяВСтроку(WebHTTP.DateTimeFormat, Dates.Now(), h.date);
	h.location := ""; h.contenttype := ""; h.contentlocation := ""; h.transferencoding := "";
	h.contentlength := -1; h.lastmodified := "";
	h.additionalFields := НУЛЬ
кон GetDefaultResponseHeader;

(** Sends all availabe data from src to dst *)
проц SendData*(src: Потоки.Чтец; dst: Потоки.Писарь);
перем len: размерМЗ; buf: массив FileBufSize из симв8;
нач
	нцПока (src.кодВозвратаПоследнейОперации = Потоки.Успех) делай
		src.чБайты(buf, 0, FileBufSize, len);
		dst.пБайты(buf, 0, len)
	кц
кон SendData;

(** Add a new virtual host *)
проц AddHost*(host: Host);
нач {единолично}
	hosts.Add(host)
кон AddHost;

(** get a list of matching hosts (wildcards permitted, "*" returns all hosts) *)
проц FindHosts*(конст host: массив из симв8): HostList;
перем i: размерМЗ; o: динамическиТипизированныйУкль; l, p, old: HostList;
нач {единолично}
	нов(l);
	если (host = "") то l.host := defaultHost;
	иначе
		p := l; old := НУЛЬ;
		нцДля i := 0 до hosts.GetCount()-1 делай
			o := hosts.GetItem(i);
			если Строки8.ПодходитЛиПодМаскуИмениФайла¿(host, o(Host).name) то
				p.host := o(Host); нов(p.next); old := p; p := p.next
			всё
		кц;
		если (old # НУЛЬ) то old.next := НУЛЬ всё
	всё;
	если (l.host = НУЛЬ) то l := НУЛЬ всё;
	возврат l
кон FindHosts;

(** remove the virtual host given by name *)
проц RemoveHost*(конст host : массив из симв8; перем res : целМЗ);
перем i : размерМЗ; o, h : динамическиТипизированныйУкль;
нач {единолично}
	hosts.Lock;
	нцДля i := 0 до hosts.GetCount() - 1 делай
		o := hosts.GetItem(i); если o(Host).name= host то h := o(Host) всё;
	кц;
	hosts.Unlock;
	если (h # НУЛЬ) то
		hosts.Remove(h); res := Ok;
	иначе
		res := Error; (* host not found *)
	всё
кон RemoveHost;

(** Start the basic Server functionality. *)
проц StartHTTP*(root : массив из симв8; конст logFile: массив из симв8; перем msg : массив из симв8; перем res : целМЗ);
нач {единолично}
	если (http = НУЛЬ) то
		Строки8.ОтрежьПолеСлеваИСправа(root, " "); defaultHost.SetPrefix(root);
		если (logFile # "") то OpenW3CLog(logFile) всё;

		нов(http, WebHTTP.HTTPPort,  NewHTTPAgent, res);
		если (res = TCPServices.Ok) то
			копируйСтрокуДо0("", msg);
			если Log то log.Enter; log.TimeStamp; log.String("Started"); log.Exit всё
		иначе
			http := НУЛЬ; копируйСтрокуДо0("TCP Error", msg);
		всё;
	иначе
		res := Error; копируйСтрокуДо0("HTTP server is already running", msg);
	всё;
кон StartHTTP;

(** Start the basic Server functionality. *)
проц StartHTTPS*(root : массив из симв8; конст  logFile: массив из симв8; перем msg : массив из симв8; перем res : целМЗ);
нач {единолично}
	если (https = НУЛЬ) то
		Строки8.ОтрежьПолеСлеваИСправа(root, " "); defaultHost.SetPrefix(root);
		если (logFile # "") то OpenW3CLog(logFile) всё;

		нов(https, WebHTTP.HTTPSPort,  NewHTTPSAgent, res);
		если (res = TCPServices.Ok) то
			копируйСтрокуДо0("", msg);
			если Log то log.Enter; log.TimeStamp; log.String("Started"); log.Exit всё
		иначе
			 https := НУЛЬ; копируйСтрокуДо0("TCP Error", msg);
		всё;
	иначе
		res := Error; копируйСтрокуДо0("HTTPS server is already running", msg);
	всё
кон StartHTTPS;

(** Stop the server *)
проц StopHTTP*(перем msg : массив из симв8; перем res : целМЗ);
нач {единолично}
	если (http # НУЛЬ) то
		res := Ok; копируйСтрокуДо0("", msg);
		http.Stop; http := НУЛЬ;
		defaultHost.SetPrefix("");
		если Log то log.Enter; log.TimeStamp; log.String("Stopped"); log.Exit всё
	иначе
		res := Error; копируйСтрокуДо0("HTTP server is not running", msg);
	всё;
кон StopHTTP;

(** Stop the server *)
проц StopHTTPS*(перем msg : массив из симв8; перем res : целМЗ);
нач {единолично}
	если (https # НУЛЬ) то
		res := Ok; копируйСтрокуДо0("", msg);
		https.Stop; https := НУЛЬ;
		defaultHost.SetPrefix("");
		если Log то log.Enter; log.TimeStamp; log.String("Stopped"); log.Exit всё
	иначе
		res := Error; копируйСтрокуДо0("HTTP server is not running", msg);
	всё;
кон StopHTTPS;

(** enumerate all installed hosts *)
проц ShowHosts*(out : Потоки.Писарь);
перем
	i : размерМЗ; o : динамическиТипизированныйУкль;

	проц PrintHost(h: Host);
	перем p: динамическиТипизированныйУкль; i: размерМЗ;
	нач
		out.пСтроку8("Host: ");
		если (h.name = "") то out.пСтроку8("default host")
		иначе out.пСтроку8(h.name)
		всё;
		out.пСтроку8("; root: '"); out.пСтроку8(h.prefix); out.пСтроку8("'; default: '"); out.пСтроку8(h.default);
		out.пСтроку8("'; error = '"); out.пСтроку8(h.error); out.пСимв8("'"); out.пВК_ПС;
		h.plugins.Lock;
		нцДля i := 0 до h.plugins.GetCount()-1 делай
			p := h.plugins.GetItem(i);
			out.пСтроку8("   plugin: "); out.пСтроку8(p(HTTPPlugin).name); out.пВК_ПС
		кц;
		h.plugins.Unlock
	кон PrintHost;

нач {единолично}
	утв(out # НУЛЬ);
	hosts.Lock;
	PrintHost(defaultHost);
	нцДля i := 0 до hosts.GetCount() - 1 делай
		o := hosts.GetItem(i);
		PrintHost(o(Host))
	кц;
	hosts.Unlock
кон ShowHosts;

проц Cleanup;
перем t: Kernel.Timer; msg : массив 32 из симв8; ignore : целМЗ;
нач
	hitStat.Kill;
	StopHTTP(msg, ignore);
	StopHTTPS(msg, ignore);
	hosts := НУЛЬ; defaultHost := НУЛЬ;
	FlushW3CLog;
	если Log то log.Close всё;
	нов(t); t.Sleep(100) (* avoid trap in Statistics; replace with Kernel.AwaitDeath *)
кон Cleanup;

нач
	если Log то
		нов(log, "WebHTTP Server");
		log.SetLogToOut(истина)
	всё;
	listener := НУЛЬ;
	нов(hosts); нов(hitStat);
	нов(defaultPlugin, "Default-Plugin");
	нов(defaultHost, "");
	http := НУЛЬ; https := НУЛЬ;
	Modules.InstallTermHandler(Cleanup)
кон WebHTTPServer.

(** INFO

The HTTP server is always listening to port 80. By default all requests are handled by the default host.
Content-Types are currently coded directly in HTTPPlugin.LocateResource (Types for .html .ssmp .txt .gif .jpg .pdf are known)

The server can be used for multi-hosting (several different domain names resolve to the same ip number but return
different pages for different domains). If a host is unknown or the request is not HTTP/1.1 compatible the default host is called.
Known host-names can be dynamically added and removed. See the WebHTTPServerTools.Mod for a multi-host setup.

Each host can support a number of "Plugins" that can handle special URIs like Form-Post / dynamically generated pages.
See WebWormWatch.Mod for some example plugins.

There is another (experimental) method for dynamically generated pages: "Server Side Modified Pages". Documents with
the name extension ".ssmp" are modified by the server. The patterns "&&"<methodName>" "[<Parameters>] are replaced
by the result of the respective method. See WebWormWatch.Mod WebHTTPServer.Mod WebDefaultSSMP.Mod for examples of SSMP methods.
See public.info.ssmp as an example of a ".ssmp" page.

(currently unavailable:)
There is a helper module that allows to use url-encoded form posts. See TFHTTPServerExample.Mod for a form-post example.
public.form.html contains the form.

The interfaces in all these modules may change.
*)


(* 
COMPILE THE SERVER AND EXAMPLES
PC.Compile \s TFLog.Mod WebHTTP.Mod WebHTTPServer.Mod WebSSMPPlugin.Mod WebDefaultSSMP.Mod~

START THE SERVER
Configuration.DoCommands
WebHTTPServerTools.Start \r:../httproot \l:WebHTTP.Log ~
WebHTTPServerTools.AddHost livepc.inf.ethz.ch \r:FAT:/httproot/test~
WebSSMPPlugin.Install~
WebDefaultSSMP.Install~
WebHTTPServerTools.ListHosts~
~

WebHTTPServerTools.Stop ~

WebFTPServerTools.Start \r:httproot \l:httproot/FTP.Log~

FREE THE SERVER
System.Free WebHTTPServerTools WebDefaultSSMP WebSSMPPlugin WebHTTPServer  WebHTTP~

System.State WebHTTPServer~
System.FreeDownTo WebHTTPServer ~

FILES
TFLog.Mod WebHTTP.Mod WebSSMPPlugin.Mod WebDefaultSSMP.Mod WebHTTPServer.Mod WebWormWatch.Mod public.form.html public.info.ssmp~

Statistics.Log

W3C Log File

#Version: 1.0
#Fields: date	time	cs-method	cs(host)	cs-uri	c-ip	cs(user-agent)	cs(referer)
WebHTTPServer.FlushW3CLog
EditTools.OpenAscii HTTP.Log ~
System.DeleteFiles HTTP.Log~

*)

модуль WMPerfMonAlertsUtils; (** AUTHOR "staubesv"; PURPOSE "Alert signaling"; *)
(**
 * History:
 *
 *	07.03.2007	First release (staubesv)
 *)

использует
	Modules, Kernel, Commands, Beep;

конст

	None* = 0;
	Information* = 1;
	Error* = 2;
	Critical* = 3;

	(* Beeper states *)
	Waiting = 0;
	SignalInformation = 1;
	SignalError = 2;
	SignalCritical = 3;
	Terminating = 8;
	Terminated = 9;

	FrequencyInformation = 1000;
	FrequencyError = 2000;
	FrequencyCritical = 3000;

	IntervalInformation = 10000;
	IntervalError = 5000;
	IntervalCritical = 500;

тип

	Beeper = окласс
	перем
		state : цел32;
		hz, intervall : цел32;
		timer : Kernel.Timer;

		проц Signal(type : цел32);
		нач {единолично}
			если type = None то
				state := Waiting;
			иначе
				если type > state то state := type; всё;
			всё;
			просей state из
				|SignalInformation: hz := FrequencyInformation; intervall := IntervalInformation;
				|SignalError: hz := FrequencyError; intervall := IntervalError;
				|SignalCritical: hz := FrequencyCritical; intervall := IntervalCritical;
			иначе
				hz := 0;
			всё;
			timer.Wakeup;
		кон Signal;

		проц Stop;
		нач
			Beep.Beep(0);
			нач {единолично} state := Terminating; кон;
			timer.Wakeup; timer.Wakeup;
			нач {единолично} дождись(state = Terminated); кон;
		кон Stop;

		проц &Init*;
		нач
			state := Waiting;
			нов(timer);
		кон Init;

	нач {активное}
		нцПока state # Terminating делай
			нач {единолично} дождись(state # Waiting); кон;
			если state # Terminating то
				Beep.Beep(hz);
				timer.Sleep(intervall DIV 2);
				Beep.Beep(0);
				если state # Terminating то timer.Sleep(intervall DIV 2); всё;
			всё;
		кц;
		нач {единолично} state := Terminated; кон;
	кон Beeper;

перем
	beeper : Beeper;

проц Signal*(type : цел32);
нач {единолично}
	если beeper = НУЛЬ то нов(beeper); всё;
	beeper.Signal(type);
кон Signal;

проц SignalCmd*(context : Commands.Context);
перем nbr : цел32;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦел32(nbr, ложь) то
		Signal(nbr);
	иначе
		context.result := Commands.CommandParseError;
	всё;
кон SignalCmd;

проц Cleanup;
нач
	если beeper # НУЛЬ то beeper.Stop; beeper := НУЛЬ; всё;
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
кон WMPerfMonAlertsUtils.

WMPerfMonAlertsUtils.SignalCmd 0 ~	System.Free WMPerfMonAlertsUtils ~
WMPerfMonAlertsUtils.SignalCmd 1 ~
WMPerfMonAlertsUtils.SignalCmd 2 ~
WMPerfMonAlertsUtils.SignalCmd 3 ~

модуль HostStrings; 

использует Ю16, Kernel32, НИЗКОУР;
	
тип StrokaWindowsUTF16DoNolja* = укль { опасныйДоступКПамяти, неОтслСборщиком } на массив матМаксимум(размерМЗ) из бцел16;

проц ZagruziStrokuIzWindows*(vkh : StrokaWindowsUTF16DoNolja): Ю16.уСтрока;
перем clipboardPos, clipboardLen : размерМЗ; res : Ю16.уСтрока;

нач
	если vkh # Kernel32.NULL то
		(* obrezhet slishkom dlinnuju strochku molcha *)
		нцПока (vkh[ clipboardLen ] # 0) и (clipboardLen < матМаксимум(размерМЗ)) делай 
			увел( clipboardLen ) кц;
		нов( res, clipboardLen + 1 );
		нцПока clipboardPos < clipboardLen делай
			res[clipboardPos] := Ю16.СимвИзКода(vkh[clipboardPos]);
			увел( clipboardPos ); кц всё; 
	возврат res кон ZagruziStrokuIzWindows;


(* Stroka budet sozdana s flagami { Kernel32.GMemMoveable, Kernel32.GMemDDEShare } *)
проц OtpravqStrokuVWindowsDljaPutClipboard*(vkh : Ю16.уСтрока; перем vYkh : StrokaWindowsUTF16DoNolja) : Kernel32.BOOL;
перем
	vYkhL: StrokaWindowsUTF16DoNolja;
	textLen : размерМЗ;
нач
	textLen := длинаМассива(vkh);
	vYkh := Kernel32.GlobalAlloc( { Kernel32.GMemMoveable, Kernel32.GMemDDEShare }, (textLen + 1 )*размер16_от(Ю16.Симв) );
	если vYkh = Kernel32.NULL то
		возврат Kernel32.False всё;
	vYkhL := Kernel32.GlobalLock( vYkh );
	НИЗКОУР.копируйПамять( адресОт( vkh[ 0 ] ), vYkhL, ( textLen )*размер16_от(Ю16.Симв) ); (* include NULL *)
	vYkhL[ textLen ] := 0; (* NULLNULL *)

	если Kernel32.GlobalUnlock( vYkh ) # Kernel32.True то
		возврат Kernel32.False всё;
	возврат Kernel32.True кон OtpravqStrokuVWindowsDljaPutClipboard;
	
кон HostStrings.


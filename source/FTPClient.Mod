модуль FTPClient;	(** AUTHOR "TF"; PURPOSE "FTP client services"; *)

использует Потоки, Kernel, Objects, IP, DNS, TCP, Строки8, ЛогЯдра;

конст
	ResOk = 0;
	ResFailed = 1;
	ResAlreadyOpen = 2;
	ResServerNotFound = 3;
	ResNoConnection = 4;
	ResUserPassError = 5;
	ResServerNotReady = 6;
	ResServerFailed = 7;

	FileActionOk = 250; CommandOk = 200; DataConnectionOpen = 125; FileStatusOk = 150;
	EnterPassword = 330; NeedPassword = 331; PathNameCreated = 257; UserLoggedIn = 230;

	ActvTimeout = 60 * 1000;
	Debug = ложь;

тип
	FTPEntry* = окласс
	перем
		full* : массив 331 из симв8;
		flags* : массив 11 из симв8;
		type* : массив 4 из симв8;
		user*, group*, size* : массив 9 из симв8;
		d0*, d1*, d2* : массив 13 из симв8;
		filename* : массив 256 из симв8;
		visible* : булево;
	кон FTPEntry;

	FTPListing* = укль на массив из FTPEntry;

	(** FTP client object must be used by a single process *)
	FTPClient* = окласс
	перем
		open : булево;
		busy : булево;
		connection : TCP.Connection; (* control connection to the server *)
		dataCon : TCP.Connection;
		dataIP : IP.Adr;
		dataPort : цел32;
		w : Потоки.Писарь; (* writer oo the control connection *)
		r : Потоки.Чтец; (* reader on the control connection *)
		msg- : массив 4096 из симв8;
		code : цел32;
		passiveTransfer : булево;
		actvListener : TCP.Connection;
		actvTimeout : Objects.Timer;

		listing- : FTPListing;
		nofEntries- : размерМЗ;

		проц &Init*;
		нач
			нов(actvTimeout)
		кон Init;


		проц Open*(конст host, user, password : массив из симв8; port : цел32; перем res : целМЗ);
		перем fadr : IP.Adr;
		нач {единолично}
			res := 0;
			busy := ложь; open := ложь;
			если open то res := ResAlreadyOpen; возврат всё;
			DNS.HostByName(host, fadr, res);
			если res = DNS.Ok то
				нов(connection);
				connection.Open(TCP.NilPort, fadr, port, res);
				если res = TCP.Ok то
					Потоки.НастройПисаря(w, connection.ЗапишиВПоток);
					Потоки.НастройЧтеца(r, connection.ПрочтиИзПотока);
					ReadResponse(code, msg);
					если (code >= 200) и (code < 300) то
						если Login(user, password) то open := истина;
							(* Set binary transfer mode - anything else seems useless *)
							w.пСтроку8("TYPE I"); w.пВК_ПС; w.ПротолкниБуферВПоток;
							ReadResponse(code, msg);
							если code # CommandOk то res := ResServerFailed всё
						иначе res := ResUserPassError
						всё
					иначе res := ResServerNotReady
					всё
				иначе res := ResNoConnection
				всё;
				если ~open то connection.Закрой(); w := НУЛЬ; r := НУЛЬ всё
			иначе res := ResServerNotFound
			всё
		кон Open;

		проц Login(конст user, password : массив из симв8) : булево;
		нач
			w.пСтроку8("USER "); w.пСтроку8(user); w.пВК_ПС; w.ПротолкниБуферВПоток;
			ReadResponse(code, msg);
			если (code = EnterPassword) или (code = NeedPassword) то
				w.пСтроку8("PASS "); w.пСтроку8(password); w.пВК_ПС; w.ПротолкниБуферВПоток;
				ReadResponse(code, msg);
				если (code = UserLoggedIn) или (code = EnterPassword) (* why ? *) то
					возврат истина
				иначе
					возврат ложь
				всё
			аесли code = UserLoggedIn то возврат истина
			иначе возврат ложь
			всё
		кон Login;

		проц ReadResponse(перем code : цел32; перем reply : массив из симв8);
		перем temp : массив 1024 из симв8; tcode: массив 4 из симв8; t : цел32;
			stop : булево;
		нач
			r.чЦел32(code, ложь); копируйСтрокуДо0("", reply);
			если r.ПодглядиСимв8() = "-" то (* multi line response *)
				stop := ложь;
				нцДо
					r.чСтроку8ДоКонцаСтрокиТекстаВключительно(temp); Строки8.ПодклейВСтрокуХвост(reply, temp); tcode[0] := симв8ИзКода(10); tcode[1] := 0X;
					Строки8.ПодклейВСтрокуХвост(reply, tcode);
					tcode[0] := temp[0]; tcode[1] := temp[1]; tcode[2] := temp[2]; tcode[3] := 0X;
					Строки8.ПрочтиЦел32_изСтроки(tcode, t);
					если (t = code) и (temp[3] # "-") то stop := истина всё;
				кцПри stop или (r.кодВозвратаПоследнейОперации # 0)
			иначе
				r.чСтроку8ДоКонцаСтрокиТекстаВключительно(temp); Строки8.ПодклейВСтрокуХвост(reply, temp);
			всё;
		кон ReadResponse;

		проц Close*(перем res : целМЗ);
		нач
			w.пСтроку8("QUIT"); w.пВК_ПС; w.ПротолкниБуферВПоток;
			ReadResponse(code, msg);
			если (code >= 200) и (code < 300) то res := 0 иначе res := code всё;
			connection.Закрой; w := НУЛЬ; r := НУЛЬ;
			open := ложь
		кон Close;

		проц IsAlive*() : булево;
		перем state: цел32;
		нач
			state := connection.state;
			если (state в TCP.ClosedStates) или (state = 5) то возврат ложь
			иначе возврат истина всё
		кон IsAlive;

		проц IsNum(ch : симв8) : булево;
		нач
			возврат (ch >= '0') и (ch <='9')
		кон IsNum;

		проц GetDataConnection( перем res : целМЗ);
		перем ch : симв8; i, j : размерМЗ; ipstr : массив 16 из симв8; p0, p1, port : цел32;
			str : массив 32 из симв8;

			проц Fail;
			нач
				res := -1; r.ПропустиДоКонцаСтрокиТекстаВключительно
			кон Fail;

		нач
			если passiveTransfer то
				w.пСтроку8("PASV"); w.пВК_ПС; w.ПротолкниБуферВПоток;
				r.чЦел32(code, ложь);
				если Debug то
					ЛогЯдра.пСтроку8("PASV");
					ЛогЯдра.пСтроку8("code = "); ЛогЯдра.пЦел64(code, 0); ЛогЯдра.пВК_ПС;
				всё;
			всё;

			если passiveTransfer и (code >= 200) и (code < 300) то
				(* search for a number *)
				нцДо ch := r.чИДайСимв8() кцПри IsNum(ch) или (r.кодВозвратаПоследнейОперации # 0);
				если r.кодВозвратаПоследнейОперации # 0 то Fail; возврат всё;
				(* read ip adr *)
				j := 0; i := 0;
				нцПока (r.кодВозвратаПоследнейОперации = 0) и (j < 4) делай
					если ch = "," то ch := "."; увел(j) всё;
					ЛогЯдра.пСимв8(ch);
					если j < 4 то ipstr[i] := ch; увел(i); ch := r.чИДайСимв8()  всё
				кц;
				ipstr[i] := 0X;
				если Debug то
					ЛогЯдра.пСтроку8("ipstr = "); ЛогЯдра.пСтроку8(ipstr); ЛогЯдра.пВК_ПС;
				всё;
				если r.кодВозвратаПоследнейОперации # 0 то Fail; возврат всё;

				(* read the port *)
				r.чЦел32(p0, ложь); ch := r.чИДайСимв8();
				если ch # "," то Fail; возврат всё;
				r.чЦел32(p1, ложь);
				r.ПропустиДоКонцаСтрокиТекстаВключительно;
				port := p0 * 256 + p1;
				если Debug то
					ЛогЯдра.пСтроку8(ipstr); ЛогЯдра.пВК_ПС;
					ЛогЯдра.пЦел64(port, 0); ЛогЯдра.пВК_ПС;
				всё;
				dataIP := IP.StrToAdr(ipstr);
				dataPort := port;
			иначе
				если passiveTransfer то r.ПропустиДоКонцаСтрокиТекстаВключительно всё; (* skip the negative reply message to PASV *)
				passiveTransfer := ложь;
				(* trying to find an unused local tcp port within the limits of FTP *)
				нов(actvListener);
				actvListener.Open(TCP.NilPort, IP.NilAdr, TCP.NilPort, res);
				IP.AdrToStr(connection.int.localAdr, str);
				i := 0; нцПока (str[i] # 0X) делай если (str[i] = ".") то str[i] := "," всё; увел(i) кц;
				str[i] := ","; str[i+1] := 0X;
				w.пСтроку8("PORT ");
				w.пСтроку8(str);
				w.пЦел64(actvListener.lport DIV 100H, 0);
				w.пСимв8(",");
				w.пЦел64(actvListener.lport остОтДеленияНа 100H, 0);
				w.пВК_ПС; w.ПротолкниБуферВПоток;
				ReadResponse(code, msg);
				если Debug то
					ЛогЯдра.пСтроку8("code = "); ЛогЯдра.пЦел64(code, 0); ЛогЯдра.пВК_ПС;
					ЛогЯдра.пСтроку8("msg = "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС;
				всё;
			всё
		кон GetDataConnection;

		проц ActvTimeoutHandler;
		нач
			actvListener.Закрой
		кон ActvTimeoutHandler;

		проц WaitEstablished(c: TCP.Connection);
		перем t: Kernel.MilliTimer;
		нач
			утв(c # НУЛЬ);
			если (c.state # TCP.Established) то
				Kernel.SetTimer(t, 500);
				нцПока (c.state # TCP.Established) и ~Kernel.Expired(t) делай
					Objects.Yield
				кц
			всё
		кон WaitEstablished;

		проц OpenDataConnection(перем connection : TCP.Connection; перем res : целМЗ);
		нач
			если passiveTransfer то
				нов(connection); connection.Open(TCP.NilPort, dataIP, dataPort, res)
			иначе
				Objects.SetTimeout(actvTimeout, ActvTimeoutHandler, ActvTimeout);
				actvListener.Accept(connection, res);
				если Debug то
					ЛогЯдра.пСтроку8("res = "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;
				всё;
				Objects.CancelTimeout(actvTimeout);
				actvListener.Закрой;
				если (res = TCP.Ok) то
					WaitEstablished(connection);
				всё;
				если Debug то
					ЛогЯдра.пСтроку8("Active connection established"); ЛогЯдра.пВК_ПС;
				всё
			всё
		кон OpenDataConnection;

		проц OpenPut*(конст remoteName : массив из симв8; перем outw : Потоки.Писарь; перем res : целМЗ);
		нач
			если ~open или busy то res := -2; возврат всё;
			GetDataConnection(res);
			если res # 0 то возврат всё;

			w.пСтроку8("STOR "); w.пСтроку8(remoteName); w.пВК_ПС; w.ПротолкниБуферВПоток;
			ReadResponse(code, msg);
			если Debug то
				ЛогЯдра.пСтроку8("code = "); ЛогЯдра.пЦел64(code, 0); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("msg = "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС;
			всё;
			если (code = FileStatusOk) или (code = FileActionOk) или (code = DataConnectionOpen)  то
				OpenDataConnection(dataCon, res);
				если Debug то
					ЛогЯдра.пСтроку8("ODC");  ЛогЯдра.пСтроку8("res = "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;
				всё;
				если res = 0 то
					busy := истина;
					Потоки.НастройПисаря(outw, dataCon.ЗапишиВПоток)
				всё
			иначе res := -1
			всё
		кон OpenPut;

		проц ClosePut*(перем res : целМЗ);
		нач
			busy := ложь;
			если dataCon # НУЛЬ то
				dataCon.Закрой;
				dataCon := НУЛЬ
			всё;
			ReadResponse(code, msg);
			если (code >= 200) и (code < 300) то res := 0 иначе res := code всё;
			если Debug то
				ЛогЯдра.пСтроку8("Result after close put"); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("code = "); ЛогЯдра.пЦел64(code, 0); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("msg = "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС
			всё
		кон ClosePut;

		проц OpenGet*(конст remoteName : массив из симв8; перем r : Потоки.Чтец; перем res : целМЗ);
		нач
			если ~open или busy то res := -2; возврат всё;
			busy := истина;
			GetDataConnection(res);
			если res # 0 то возврат всё;

			w.пСтроку8("RETR "); w.пСтроку8(remoteName); w.пВК_ПС; w.ПротолкниБуферВПоток;
			ReadResponse(code, msg);
			если Debug то
				ЛогЯдра.пСтроку8("code = "); ЛогЯдра.пЦел64(code, 0); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("msg = "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС;
			всё;
			если (code = FileStatusOk) или (code = FileActionOk) или (code = DataConnectionOpen)  то
				OpenDataConnection(dataCon, res);
				если Debug то
					ЛогЯдра.пСтроку8("ODC");  ЛогЯдра.пСтроку8("res = "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;
				всё;
				если res = 0 то
					Потоки.НастройЧтеца(r, dataCon.ПрочтиИзПотока)
				всё
			иначе res := -1
			всё
		кон OpenGet;

		проц CloseGet*(перем res : целМЗ);
		нач
			если dataCon # НУЛЬ то
				dataCon.Закрой;
				dataCon := НУЛЬ
			всё;
			busy := ложь;
			ReadResponse(code, msg);
			если (code >= 200) и (code < 300) то res := 0 иначе res := code всё;
			если Debug то
				ЛогЯдра.пСтроку8("Result after close get"); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("code = "); ЛогЯдра.пЦел64(code, 0); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("msg = "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС
			всё
		кон CloseGet;

		проц DeleteFile*(конст remoteName : массив из симв8; перем res : целМЗ);
		нач
			если ~open или busy то res := -2; возврат всё;
			w.пСтроку8("DELE "); w.пСтроку8(remoteName); w.пВК_ПС; w.ПротолкниБуферВПоток;
			ReadResponse(code, msg);
			если (code >= 200) и (code <300) то res := ResOk иначе res := ResFailed всё
		кон DeleteFile;

		проц ChangeDir*(конст dir : массив из симв8; перем res : целМЗ);
		нач
			если ~open или busy то res := -2; возврат всё;
			w.пСтроку8("CWD "); w.пСтроку8(dir); w.пВК_ПС; w.ПротолкниБуферВПоток;
			ReadResponse(code, msg);
			если (code >= 200) и (code <300) то res := ResOk иначе res := ResFailed всё
		кон ChangeDir;

		проц MakeDir*(конст dir : массив из симв8; перем res : целМЗ);
		нач
			если ~open или busy то res := -2; возврат всё;
			w.пСтроку8("MKD "); w.пСтроку8(dir); w.пВК_ПС; w.ПротолкниБуферВПоток;
			ReadResponse(code, msg);
			если (code >= 200) и (code <300) то res := ResOk иначе res := ResFailed всё
		кон MakeDir;

		проц RemoveDir*(конст dir : массив из симв8; перем res : целМЗ);
		нач
			если ~open или busy то res := -2; возврат всё;
			w.пСтроку8("RMD "); w.пСтроку8(dir); w.пВК_ПС; w.ПротолкниБуферВПоток;
			ReadResponse(code, msg);
			если (code >= 200) и (code <300) то res := ResOk иначе res := ResFailed всё
		кон RemoveDir;

		проц RenameFile*(конст currentName, newName : массив из симв8; перем res : целМЗ);
		нач
			если ~open или busy то res := -2; возврат всё;
			w.пСтроку8("RNFR "); w.пСтроку8(currentName); w.пВК_ПС; w.ПротолкниБуферВПоток;
			ReadResponse(code, msg);
			если (code = 350) то
				w.пСтроку8("RNTO "); w.пСтроку8(newName); w.пВК_ПС; w.ПротолкниБуферВПоток;
				ReadResponse(code, msg);
				если code = 250 то res := ResOk
				иначе res := ResFailed
				всё
			иначе res := ResFailed
			всё
		кон RenameFile;

		проц EnumerateNames*;
		перем
			res : целМЗ;
			r : Потоки.Чтец; s, filename : массив 256 из симв8;
			flags : массив 11 из симв8;
			type : массив 4 из симв8;
			user, group, size : массив 9 из симв8;
			d0, d1, d2: массив 13 из симв8;
			sr : Потоки.ЧтецИзСтроки;
			entry : FTPEntry;

		нач
			если ~open или busy то res := -2; возврат всё;
			если Debug то
				ЛогЯдра.пСтроку8("Enumerate Dir"); ЛогЯдра.пВК_ПС;
			всё;
			GetDataConnection(res);
			если res # 0 то возврат всё;
			w.пСтроку8("NLST"); w.пВК_ПС; w.ПротолкниБуферВПоток;
			ReadResponse(code, msg);
			если Debug то
				ЛогЯдра.пСтроку8("code = "); ЛогЯдра.пЦел64(code, 0); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("msg = "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС;
			всё;
			если (code = FileStatusOk) или (code = FileActionOk) или (code = DataConnectionOpen)  то
				если Debug то
					ЛогЯдра.пСтроку8("Open data connection"); ЛогЯдра.пВК_ПС;
				всё;
				OpenDataConnection(dataCon, res);
				если Debug то
					ЛогЯдра.пСтроку8("ODC");  ЛогЯдра.пСтроку8("res = "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;
				всё;
				если res = 0 то
					Потоки.НастройЧтеца(r, dataCon.ПрочтиИзПотока);
					нов(sr, 256); нов(listing, 16); nofEntries := 0;
					нцДо
						r.чСтроку8ДоКонцаСтрокиТекстаВключительно(s);
						если r.кодВозвратаПоследнейОперации = 0 то
							sr.ПримиСтроку8ДляЧтения(s); нов(entry);

							копируйСтрокуДо0("", flags);
							копируйСтрокуДо0("", type);
							копируйСтрокуДо0("", user);
							копируйСтрокуДо0("", group);
							копируйСтрокуДо0("", size);
							копируйСтрокуДо0("", d0);
							копируйСтрокуДо0("", d1);
							копируйСтрокуДо0("", d2);
							sr.чСтроку8ДоКонцаСтрокиТекстаВключительно(filename);

							копируйСтрокуДо0(flags, entry.flags);
							копируйСтрокуДо0(type, entry.type);
							копируйСтрокуДо0(user, entry.user);
							копируйСтрокуДо0(group, entry.group);
							копируйСтрокуДо0(size, entry.size);
							копируйСтрокуДо0(d0, entry.d0);
							копируйСтрокуДо0(d1, entry.d1);
							копируйСтрокуДо0(d2, entry.d2);
							копируйСтрокуДо0(filename, entry.filename);
							копируйСтрокуДо0(s, entry.full);
							AddFTPEntryToListing(entry);

(*							IF Debug THEN
								KernelLog.String("flags = "); KernelLog.String(flags); KernelLog.Ln;
								KernelLog.String("type = "); KernelLog.String(type); KernelLog.Ln;
								KernelLog.String("user = "); KernelLog.String(user); KernelLog.Ln;
								KernelLog.String("group = "); KernelLog.String(group); KernelLog.Ln;
								KernelLog.String("size = "); KernelLog.String(size); KernelLog.Ln;
								KernelLog.String("date = "); KernelLog.String(d0); KernelLog.String(d1);KernelLog.String(d2);KernelLog.Ln;
								KernelLog.String("filename = "); KernelLog.String(filename); KernelLog.Ln;
								KernelLog.Ln;
							END
*)						всё
					кцПри r.кодВозвратаПоследнейОперации # 0
				всё;
				если (dataCon # НУЛЬ) то dataCon.Закрой; всё;

				ReadResponse(code, msg);
				если Debug то
					ЛогЯдра.пСтроку8("Result after Dir"); ЛогЯдра.пВК_ПС;
					ЛогЯдра.пСтроку8("code = "); ЛогЯдра.пЦел64(code, 0); ЛогЯдра.пВК_ПС;
					ЛогЯдра.пСтроку8("msg = "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС;
				всё
			иначе res := ResFailed
			всё;
			dataCon := НУЛЬ
		кон EnumerateNames;

		проц EnumerateDir*(конст args : массив из симв8);
		перем  res : целМЗ;
			r : Потоки.Чтец; s, filename : массив 256 из симв8;
			flags : массив 11 из симв8;
			type : массив 4 из симв8;
			user, group, size : массив 9 из симв8;
			d0, d1, d2: массив 13 из симв8;
			sr : Потоки.ЧтецИзСтроки;
			entry : FTPEntry;
			ch : симв8;

(*
			PROCEDURE FixLengthStr(r : Streams.Reader; len : SIZE; VAR s : ARRAY OF CHAR);
			VAR i : SIZE;
			BEGIN
				WHILE (len > 0) & (r.res = 0) DO
					s[i] := r.Get();
					DEC(len); INC(i)
				END;
				s[i] := 0X
			END FixLengthStr;
*)

		нач
			если ~open или busy то res := -2; возврат всё;
			если Debug то
				ЛогЯдра.пСтроку8("Enumerate Dir"); ЛогЯдра.пВК_ПС;
			всё;
			GetDataConnection(res);
			если res # 0 то возврат всё;
			w.пСтроку8("LIST");
			если args # "" то w.пСтроку8(" "); w.пСтроку8(args) всё;
			w.пВК_ПС; w.ПротолкниБуферВПоток;
			ReadResponse(code, msg);
			если Debug то
				ЛогЯдра.пСтроку8("code = "); ЛогЯдра.пЦел64(code, 0); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("msg = "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС;
			всё;
			если (code = FileStatusOk) или (code = FileActionOk) или (code = DataConnectionOpen)  то
				если Debug то
					ЛогЯдра.пСтроку8("Open data connection"); ЛогЯдра.пВК_ПС;
				всё;
				OpenDataConnection(dataCon, res);
				если Debug то
					ЛогЯдра.пСтроку8("ODC");  ЛогЯдра.пСтроку8("res = "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;
				всё;
				если res = 0 то
					Потоки.НастройЧтеца(r, dataCon.ПрочтиИзПотока);
					нов(sr, 256); нов(listing, 16); nofEntries := 0;
					нцДо
						r.чСтроку8ДоКонцаСтрокиТекстаВключительно(s);
						если r.кодВозвратаПоследнейОперации = 0 то
							sr.ПримиСтроку8ДляЧтения(s); нов(entry);
(*							KernelLog.String("s = "); KernelLog.String(s); KernelLog.Ln;

							FixLengthStr(sr, 10, flags); sr.SkipBytes(1);
							FixLengthStr(sr, 3, type); sr.SkipBytes(1);
							FixLengthStr(sr, 8, user); sr.SkipBytes(1);
							FixLengthStr(sr, 8, group); sr.SkipBytes(1);
							FixLengthStr(sr, 8, size); sr.SkipBytes(1);
							FixLengthStr(sr, 12, date); sr.SkipBytes(1); *)

							ch := sr.ПодглядиСимв8();
							если  (ch = "-") или (ch = "d") или (ch = "l") то (* unix *)
								sr.чЦепочкуСимв8ДоБелогоПоля(flags); sr.ПропустиБелоеПоле;
								sr.чЦепочкуСимв8ДоБелогоПоля(type); sr.ПропустиБелоеПоле;
								sr.чЦепочкуСимв8ДоБелогоПоля(user); sr.ПропустиБелоеПоле;
								sr.чЦепочкуСимв8ДоБелогоПоля(group); sr.ПропустиБелоеПоле;
								sr.чЦепочкуСимв8ДоБелогоПоля(size); sr.ПропустиБелоеПоле;
								sr.чЦепочкуСимв8ДоБелогоПоля(d0); sr.ПропустиБелоеПоле;
								sr.чЦепочкуСимв8ДоБелогоПоля(d1); sr.ПропустиБелоеПоле;
								sr.чЦепочкуСимв8ДоБелогоПоля(d2); sr.ПропустиБелоеПоле;

								sr.чСтроку8ДоКонцаСтрокиТекстаВключительно(filename);
							иначе (* windows *)
								копируйСтрокуДо0("", type);
								копируйСтрокуДо0("", user);
								копируйСтрокуДо0("", group);
								копируйСтрокуДо0("", size);
								копируйСтрокуДо0("", d2);
								sr.чЦепочкуСимв8ДоБелогоПоля(d0); sr.ПропустиБелоеПоле;
								sr.чЦепочкуСимв8ДоБелогоПоля(d1); sr.ПропустиБелоеПоле;
								sr.чЦепочкуСимв8ДоБелогоПоля(flags); sr.ПропустиБелоеПоле;
								sr.чСтроку8ДоКонцаСтрокиТекстаВключительно(filename);
								если flags # "<DIR>" то копируйСтрокуДо0(flags, size); копируйСтрокуДо0("", flags)	всё
							всё;

							копируйСтрокуДо0(flags, entry.flags);
							копируйСтрокуДо0(type, entry.type);
							копируйСтрокуДо0(user, entry.user);
							копируйСтрокуДо0(group, entry.group);
							копируйСтрокуДо0(size, entry.size);
							копируйСтрокуДо0(d0, entry.d0);
							копируйСтрокуДо0(d1, entry.d1);
							копируйСтрокуДо0(d2, entry.d2);
							копируйСтрокуДо0(filename, entry.filename);
							копируйСтрокуДо0(s, entry.full);
							AddFTPEntryToListing(entry);

(*							IF Debug THEN
								KernelLog.String("flags = "); KernelLog.String(flags); KernelLog.Ln;
								KernelLog.String("type = "); KernelLog.String(type); KernelLog.Ln;
								KernelLog.String("user = "); KernelLog.String(user); KernelLog.Ln;
								KernelLog.String("group = "); KernelLog.String(group); KernelLog.Ln;
								KernelLog.String("size = "); KernelLog.String(size); KernelLog.Ln;
								KernelLog.String("date = "); KernelLog.String(d0); KernelLog.String(d1);KernelLog.String(d2);KernelLog.Ln;
								KernelLog.String("filename = "); KernelLog.String(filename); KernelLog.Ln;
								KernelLog.Ln;
							END
*)						всё
					кцПри r.кодВозвратаПоследнейОперации # 0
				всё;
				если (dataCon # НУЛЬ) то dataCon.Закрой; всё;

				ReadResponse(code, msg);
				если Debug то
					ЛогЯдра.пСтроку8("Result after Dir"); ЛогЯдра.пВК_ПС;
					ЛогЯдра.пСтроку8("code = "); ЛогЯдра.пЦел64(code, 0); ЛогЯдра.пВК_ПС;
					ЛогЯдра.пСтроку8("msg = "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС;
				всё
			иначе res := ResFailed
			всё;
			dataCon := НУЛЬ
		кон EnumerateDir;

		проц AddFTPEntryToListing(entry : FTPEntry);
		перем newList : FTPListing;
			i : размерМЗ;
		нач
			увел(nofEntries);
			если (nofEntries > длинаМассива(listing)) то
				нов(newList, длинаМассива(listing)*2);
				нцДля i := 0 до длинаМассива(listing)-1 делай newList[i] := listing[i] кц;
				listing := newList;
			всё;
			listing[nofEntries-1] := entry;
		кон AddFTPEntryToListing;

		проц GetCurrentDir*(перем dir : массив из симв8; перем res : целМЗ);
		перем p : размерМЗ;
		нач
			если ~open или busy то res := -2; возврат всё;
			w.пСтроку8("PWD"); w.пВК_ПС; w.ПротолкниБуферВПоток;
			ReadResponse(code, msg);
			ЛогЯдра.пСтроку8("msg = "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС;
			если code = PathNameCreated то
				копируйСтрокуДо0(msg, dir);
				p := Строки8.НайдиПодстроку('"', dir);
				если p >= 0 то
					Строки8.УдалиПодстроку˛неПроверяя0(dir, 0, p + 1);
					p := Строки8.НайдиПодстроку('"', dir); Строки8.УдалиПодстроку˛неПроверяя0(dir, p, Строки8.КвоБайтБезЗавершающего0(dir) - p)
				иначе
					p := Строки8.НайдиПодстроку(' ', dir); Строки8.УдалиПодстроку˛неПроверяя0(dir, p, Строки8.КвоБайтБезЗавершающего0(dir) - p)
				всё
			иначе копируйСтрокуДо0("", dir); res := ResFailed
			всё;
		кон GetCurrentDir;

		проц Raw*(конст cmd : массив из симв8; перем res : целМЗ);
		перем extMsg : массив 4096 из симв8;
			command : массив 32 из симв8; arguments : массив 512 из симв8;
		нач
			если ~open или busy то res := -2; возврат всё;
			SplitCommand(cmd, command, arguments);
			Строки8.СтрокуВНижнийРегистрASCII(command);
			если command = "list" то EnumerateDir(arguments)
			иначе
				w.пСтроку8(cmd); w.пВК_ПС; w.ПротолкниБуферВПоток;
				ReadResponse(code, extMsg);
				ЛогЯдра.пСтроку8("code = "); ЛогЯдра.пЦел64(code, 0);
				ЛогЯдра.пСтроку8(" , msg = "); ЛогЯдра.пСтроку8(extMsg); ЛогЯдра.пВК_ПС
			всё;
			res := 0
		кон Raw;

		проц SplitCommand(конст cmd : массив из симв8; перем command, args : массив из симв8);
		перем sr : Потоки.ЧтецИзСтроки;
		нач
			нов(sr, 512);
			sr.ПримиСтроку8ДляЧтения(cmd);
			sr.чЦепочкуСимв8ДоБелогоПоля(command); sr.ПропустиБелоеПоле;
			sr.чСтроку8ДоКонцаСтрокиТекстаВключительно(args);
		кон SplitCommand;

	кон FTPClient;

кон FTPClient.

System.Free FTPClient~

Color Codes
Highlight
Types and Procedures
Lock Acquire / Lock Release
Preferred notation (comment)
Unsafe / Temporary / Stupid / requires attention
Permanent Comment
Assertion
Debug


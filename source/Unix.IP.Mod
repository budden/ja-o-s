(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль IP;   (** AUTHOR "pjm, mvt, eb, G.F."; PURPOSE "IP (v4 and v6)"; *)

использует S := НИЗКОУР, ЛогЯдра, Строки8, Network;

конст
	(** Error codes *)
	Ok* = 0;

	(** IP address constants *)
	NilAdrIPv4 = 0;

	(* Comparators for Adr.usedProtocols *)
	IPv4* = 4;
	IPv6* = 6;
	NilAdrIdent = -1;   (* usedProtocol of NilAdrs *)

	MaxNofDNS = 4;

тип
	Adr* = запись
				ipv4Adr*: цел32;
				ipv6Adr*: массив 16 из симв8;
				usedProtocol*: цел32;
				data*: цел32;
			кон;
			(** An IP Address.
					usedProtocol = 0: No protocol yet used
					usedProtocol = IPv4: IPv4 address stored in field ipv4Adr
					usedProtocol = IPv6: IPv6 address stored in field ipv6Adr
					data can be used to store additional informations. I.e. in IPv6 the
					prefix length is stored in the data field *)

	Packet* = укль на массив из симв8;

	Name* = массив 128 из  симв8;   (** Name type for interface name *)

	ARPHandler* = проц {делегат} ( ip: Adr; complete: булево;
											link: Network.LinkAdr;
											size, sendTime, updateTime, updateDate, hash: цел32);

	Interface* = окласс
				(*! 	unused in UnixAos, included only for interface compatibility
					mostly a dummy, only 'localAdr' contains valid data in UnixAos !! *)

			перем
				(** IP addresses of this interface. *)
				localAdr-, maskAdr-, gatewayAdr-, subnetAdr-, broadAdr-: Adr;

				(** name of the interface *)
				name-: Name;

				(** Device that the interface belongs to *)
				dev-: Network.LinkDevice;

				(** DNS server list - can be used by DNS, not used in IP itself *)
				DNS-: массив MaxNofDNS из  Adr;   (* DNS server list *)
				DNScount-: цел32;   (* number of DNS servers in list *)

				(* interface *)
				next*: Interface;   (* next pointer for interface list *)
				closed-: булево;   (* is interface already closed? *)
				protocol-: цел32;
							 (* Interface for IPv4 or IPv6?. Only used by IP otherwise use dynamic type checking! *)



				проц & Init*( addr: Adr );
				нач
					localAdr := addr;
					name := "dummy";
				кон Init;

			кон Interface;

	InterfaceHandler* = проц {делегат} (int: Interface);

перем
	(* IP *)
	NilAdr*: Adr;   (* To check if an IP address is NIL use IsNilAdr instead *)
	preferredProtocol*: цел32;   (* Preferred IP protocol *)

	(** Is address not yet specified *)
	проц IsNilAdr*( adr: Adr ): булево;
	перем isNil: булево;  i: цел32;
	нач
		просей adr.usedProtocol из
		| IPv4:   возврат (adr.ipv4Adr = NilAdrIPv4)
		| IPv6:	isNil := истина;  i := 0;
				нцПока ((i < 16) и isNil) делай
					если adr.ipv6Adr[i] # 0X то  isNil := ложь  всё;
					увел( i );
				кц;
				возврат isNil;
		| NilAdrIdent:
				возврат истина;
		иначе
			возврат истина;
		всё;
	кон IsNilAdr;


	(* Checks if two addresses are equal *)
	проц AdrsEqual*( adr1, adr2: Adr ): булево;
	перем equal: булево;  i: цел32;

	нач
		если adr1.usedProtocol # adr2.usedProtocol то  возврат ложь  всё;
		просей adr1.usedProtocol из
		| IPv4:	если adr1.ipv4Adr = adr2.ipv4Adr то  возврат истина  всё;
		| IPv6:	equal := истина;  i := 0;
				нцПока ((i < 16) и equal) делай
					если adr1.ipv6Adr[i] # adr2.ipv6Adr[i] то  equal := ложь  всё;
					увел( i );
				кц;
				если adr1.data # adr2.data то  equal := ложь  всё;
				возврат equal;
		| NilAdrIdent:
				(* both addresses NIL therefore equal *)
				если adr2.usedProtocol = NilAdrIdent то  возврат истина  иначе  возврат ложь  всё
		иначе возврат ложь
		всё;
		возврат ложь
	кон AdrsEqual;


	(** Convert a dotted-decimal string to an ip address. Return NilAdr on failure. *)
	проц StrToAdr*( ipString: массив из симв8 ): Adr;
	перем retAdr: Adr;  i, j, x: цел32;
		adr: массив 4 из симв8;
		ok: булево;
		charCount: цел32;   (* ipv6: number of character between two : *)
		ipv6AdrPart: массив 6 из симв8;   (* two bytes of an IPv6 address *)
		ipv6AdrRight: массив 16 из симв8;   (* right part of an IPv6 address; after :: *)
		hexToChar: массив 3 из симв8;
		leftParts: цел32;   (* number of bytes before :: *)
		rightParts: цел32;   (* number of bytes after :: *)
		val: цел32; res: целМЗ;
		state: цел32;   (* state of the FSM look at the eof for more info *)
		dPointOcc: булево;   (* double point occured *)
		prefixVal: цел32;

		(* compute a subpart (two bytes) of a IPv6 address; subpart:=between two : *)
		проц ComputeIPv6Part( ): булево;
		нач
			просей charCount из
			| 0:		возврат истина;
			| 1, 2:	если dPointOcc то  ipv6AdrRight[rightParts] := 0X;  увел( rightParts );
					иначе  retAdr.ipv6Adr[leftParts] := 0X;  увел( leftParts );
					всё;
					Строки8.ПрочтиЦел32_16_ричноИзСтроки( ipv6AdrPart, val, res );
					если res = Строки8.Успех то
						если dPointOcc то  ipv6AdrRight[rightParts] := симв8ИзКода( val );  увел( rightParts );
						иначе  retAdr.ipv6Adr[leftParts] := симв8ИзКода( val );  увел( leftParts );
						всё;
					иначе  возврат ложь
					всё;
			| 3:		hexToChar[0] := ipv6AdrPart[0];  hexToChar[1] := 0X;
					Строки8.ПрочтиЦел32_16_ричноИзСтроки( hexToChar, val, res );
					если res = Строки8.Успех то
						если dPointOcc то  ipv6AdrRight[rightParts] := симв8ИзКода( val );  увел( rightParts );
						иначе  retAdr.ipv6Adr[leftParts] := симв8ИзКода( val );  увел( leftParts );
						всё;
					иначе  возврат ложь
					всё;
					ipv6AdrPart[0] := "0";  Строки8.ПрочтиЦел32_16_ричноИзСтроки( ipv6AdrPart, val, res );
					если res = Строки8.Успех то
						если dPointOcc то  ipv6AdrRight[rightParts] := симв8ИзКода( val );  увел( rightParts );
						иначе  retAdr.ipv6Adr[leftParts] := симв8ИзКода( val );  увел( leftParts );
						всё;
					иначе  возврат ложь
					всё;
			| 4:		hexToChar[0] := ipv6AdrPart[0];  hexToChar[1] := ipv6AdrPart[1];  hexToChar[2] := 0X;
					Строки8.ПрочтиЦел32_16_ричноИзСтроки( hexToChar, val, res );
					если res = Строки8.Успех то
						если dPointOcc то  ipv6AdrRight[rightParts] := симв8ИзКода( val );  увел( rightParts );
						иначе  retAdr.ipv6Adr[leftParts] := симв8ИзКода( val );  увел( leftParts );
						всё;
					иначе  возврат ложь
					всё;
					ipv6AdrPart[0] := "0";  ipv6AdrPart[1] := "0";  Строки8.ПрочтиЦел32_16_ричноИзСтроки( ipv6AdrPart, val, res );
					если res = Строки8.Успех то
						если dPointOcc то  ipv6AdrRight[rightParts] := симв8ИзКода( val );  увел( rightParts );
						иначе  retAdr.ipv6Adr[leftParts] := симв8ИзКода( val );  увел( leftParts );
						всё;
					иначе  возврат ложь
					всё;
			иначе  возврат ложь;
			всё;
			charCount := 0;  возврат истина;
		кон ComputeIPv6Part;

	нач
		retAdr := NilAdr;
		если IsValidIPv4Str( ipString ) то
			(* Return an ipv4 address *)
			i := 0;  j := 0;  x := -1;  ok := ложь;
			нц
				если (ipString[i] = ".") или (ipString[i] = 0X) то
					если (x < 0) или (x > 255) или (j = 4) то  прервиЦикл   всё;
					adr[j] := симв8ИзКода( x );
					если ipString[i] = 0X то  ok := (j = 3);  прервиЦикл   всё;
					x := -1;  увел( i );  увел( j )
				аесли (ipString[i] >= "0") и (ipString[i] <= "9") то
					если x = -1 то  x := 0  всё;
					x := x*10 + (кодСимв8( ipString[i] ) - кодСимв8( "0" ));  увел( i )
				иначе  прервиЦикл
				всё
			кц;

			если ok то  retAdr.ipv4Adr := S.подмениТипЗначения( цел32, adr );  retAdr.usedProtocol := IPv4;  возврат retAdr;
			иначе  возврат NilAdr;
			всё
		аесли IsValidIPv6Str( ipString ) то
			i := 0;  state := 1;  charCount := 0;  dPointOcc := ложь;
			retAdr.usedProtocol := 6;  retAdr.ipv4Adr := NilAdrIPv4;
			i := 0;  j := 0;  charCount := 0;  leftParts := 0;  rightParts := 0;  prefixVal := 0;
			Строки8.СтрокуВВерхнийРегистрASCII( ipString );

			нцПока (i < (длинаМассива( ipString ) - 1)) и (ipString[i] # 0X) делай
				просей state из  (* Using the same FSM as IsValidIPv6Str *)
				| -1:	(* Error state Should never happen, is checked by IsValidIPv6Str() *)
						возврат NilAdr;
				| 1:       (* reading two blocks of two bytes of 0-9\A-F *)
						если ipString[i] = ":" то
							ipv6AdrPart[charCount] := 0X;
							если ~ComputeIPv6Part() то  возврат NilAdr  всё;
							state := 2;
						аесли ipString[i] = "/" то
							ipv6AdrPart[charCount] := 0X;
							если ~ComputeIPv6Part() то  возврат NilAdr  всё;
							state := 3;
						иначе  (* 0-9, A-F *)
							 ipv6AdrPart[charCount] := ipString[i];  увел( charCount );
						всё;
				| 2:        (* a : occured *)
						если ipString[i] = ":" то  dPointOcc := истина;  state := 4
						иначе  (* 0-9, A-F *)
							state := 1;  charCount := 0;  ipv6AdrPart[charCount] := ipString[i];  увел( charCount );
						всё;
				| 3:		(* prefix will follow *)
						prefixVal := (prefixVal*10) + (кодСимв8( ipString[i] ) - кодСимв8( "0" ));
				| 4:        (* A :: occured *)
						если ipString[i] = "/" то  state := 3
						иначе
							если ~ComputeIPv6Part() то  возврат NilAdr  всё;
							(* 0-9, A-F *)
							state := 1;  charCount := 0;  ipv6AdrPart[charCount] := ipString[i];  увел( charCount )
						всё;
				иначе
				всё;
				увел( i );
			кц;

			ipv6AdrPart[charCount] := 0X;
			если charCount # 0 то
				если ~ComputeIPv6Part() то  возврат NilAdr  всё;
			всё;
			если dPointOcc то
				(* fill 0X for :: *)
				нцДля i := leftParts до ((длинаМассива( retAdr.ipv6Adr ) - 1) - rightParts) делай  retAdr.ipv6Adr[i] := 0X  кц;
				(* fill part behind :: *)
				нцДля i := 0 до (rightParts - 1) делай
					retAdr.ipv6Adr[(длинаМассива( retAdr.ipv6Adr ) - rightParts) + i] := ipv6AdrRight[i]
				кц;
			всё;
			если prefixVal > 64 то  возврат NilAdr  всё;
			retAdr.data := prefixVal;  возврат retAdr;
		всё;
		возврат NilAdr;
	кон StrToAdr;


(** Convert an IP address to a dotted-decimal string. *)
	проц AdrToStr*( adr: Adr;  перем string: массив из симв8 );
	перем i, j, x: цел32;
		a: массив 4 из симв8;
		val: цел32;
		hexToStr: массив 5 из симв8;
		prefixLenStr: массив 64 из симв8;
		maxZeroRow: цел32;  currentZeroRow: цел32;
		maxZeroStart: цел32;  currentZeroStart: цел32;
		lastZero: булево;  lastDPoint: булево;  countEnded: булево;
	нач
		просей adr.usedProtocol из
		| IPv4:
				Network.Put4( a, 0, adr.ipv4Adr );
				i := 0;
				нцДля j := 0 до 3 делай
					x := кодСимв8( a[j] );
					если x >= 100 то  string[i] := симв8ИзКода( кодСимв8( "0" ) + x DIV 100 );  увел( i )  всё;
					если x >= 10 то  string[i] := симв8ИзКода( кодСимв8( "0" ) + x DIV 10 остОтДеленияНа 10 );  увел( i )  всё;
					string[i] := симв8ИзКода( кодСимв8( "0" ) + x остОтДеленияНа 10 );  увел( i );
					если j = 3 то  string[i] := 0X  иначе  string[i] := "."  всё;
					увел( i )
				кц
		| IPv6:
				нцДля i := 0 до длинаМассива( adr.ipv6Adr ) - 1 шаг 2 делай
					(* simple version *)
					val := кодСимв8( adr.ipv6Adr[i] )*256;
					val := val + кодСимв8( adr.ipv6Adr[i + 1] );
					Строки8.ПишиЦел64_16_ричноВСтроку( val, 3, hexToStr );

					(* Delete leading zeros *)
					нцПока (hexToStr[0] = "0") и (hexToStr[1] # 0X) делай  Строки8.УдалиПодстроку˛неПроверяя0( hexToStr, 0, 1 )  кц;
					Строки8.ПодклейВСтрокуХвост( string, hexToStr );
					если i # (длинаМассива( adr.ipv6Adr ) - 2) то  Строки8.ПодклейВСтрокуХвост( string, ":" )  всё;
				кц;

				(* replace longest row of zeros with :: *)
				maxZeroRow := 0;  currentZeroRow := 0;
				maxZeroStart := 0;  currentZeroStart := 0;  i := 0;
				lastZero := ложь;  lastDPoint := истина;  countEnded := истина;

				нцПока string[i] # 0X делай
					если string[i] = "0" то
						если lastDPoint то
							увел( currentZeroRow );  lastZero := истина;  lastDPoint := ложь;
							если countEnded то  currentZeroStart := i;  countEnded := ложь  всё;
						всё;
					аесли string[i] = ":" то
						lastDPoint := истина;
						если lastZero то  lastZero := ложь  всё;
					иначе
						если lastDPoint то
							lastDPoint := ложь;  countEnded := истина;
							если currentZeroRow > maxZeroRow то
								maxZeroRow := currentZeroRow;  maxZeroStart := currentZeroStart;
							всё;
						всё;
					всё;
					увел( i );
				кц;

				если ~countEnded то
					если currentZeroRow > maxZeroRow то
						maxZeroRow := currentZeroRow;  maxZeroStart := currentZeroStart;
					всё;
				всё;
				если maxZeroRow # 0 то
					(* write a :: *)
					если maxZeroStart = 0 то
						string[0] := ":";  i := 1;
						нцПока ((string[i] # 0X) и ~((string[i] # "0") и (string[i] # ":"))) делай  увел( i )  кц;
						если string[i] = 0X то  копируйСтрокуДо0( "::", string )  иначе  Строки8.УдалиПодстроку˛неПроверяя0( string, 1, i - 2 )  всё;
					иначе
						i := maxZeroStart;
						нцПока ((string[i] = "0") или (string[i] = ":")) делай  увел( i )  кц;
						если string[i] = 0X то  string[maxZeroStart] := ":";  string[maxZeroStart + 1] := 0X;
						иначе  Строки8.УдалиПодстроку˛неПроверяя0( string, maxZeroStart, i - maxZeroStart - 1 );
						всё;
					всё;
				всё;
				если adr.data # 0 то  (* write prefix *)
					Строки8.ПишиЦел64_вСтроку( adr.data, prefixLenStr );  Строки8.ПодклейВСтрокуХвост( string, "/" );
					Строки8.ПодклейВСтрокуХвост( string, prefixLenStr );
				всё;
		иначе
			если IsNilAdr( adr ) то  string[0] := 0X  всё;
		всё;
	кон AdrToStr;


	(** Convert a IP address from an array [ofs..ofs+x] to an
		Adr-type variable.
		Example for IPv4:
		If the LSB (least significant byte) is stored the the beginning [ofs],
		LSBfirst must be set to TRUE.
			(address "a.b.c.d" is stored as [d,c,b,a])
		If the LSB is stored at the end [ofs+3], LSBfirst must be set to FALSE.
			(address "a.b.c.d" is stored as [a,b,c,d])
		*)
	проц ArrayToAdr*( конст arr: массив из симв8;  ofs, protocol: цел32;  LSBfirst: булево ): Adr;
	перем adr: Adr;  i, swapTemp: цел32;

	нач
		утв( (protocol = 4) или (protocol = 6) );
		если protocol = IPv4 то  (* index check *)
			если ~(ofs + 4 <= длинаМассива( arr )) то  возврат NilAdr  всё;
			S.копируйПамять( адресОт( arr[ofs] ), адресОт( adr.ipv4Adr ), 4 );
			если LSBfirst то  SwapEndian( adr.ipv4Adr )  всё;
			adr.usedProtocol := IPv4;
		аесли protocol = IPv6 то
			если ~(ofs + 16 <= длинаМассива( arr )) то  возврат NilAdr  всё;
			S.копируйПамять( адресОт( arr[ofs] ), адресОт( adr.ipv6Adr ), 16 );
			если LSBfirst то
				нцДля i := 0 до 3 делай
					S.копируйПамять( адресОт( adr.ipv6Adr[i*4] ), адресОт( swapTemp ), 4 );
					SwapEndian( swapTemp );
					S.копируйПамять( адресОт( swapTemp ), адресОт( adr.ipv6Adr[i*4] ), 4 );
				кц;
			всё;
			adr.usedProtocol := IPv6;
		иначе
			возврат NilAdr;
		всё;
		возврат adr;
	кон ArrayToAdr;


	(** Convert an Adr-type variable  into an array [ofs..ofs+x]
		Example in IPv4:
		If the LSB (least significant byte) should be stored the the
		beginning [ofs], LSBfirst must be set to TRUE.
			(address "a.b.c.d" is stored as [d,c,b,a])
		If the LSB should be stored at the end [ofs+3], LSBfirst must be set to FALSE.
			(address "a.b.c.d" is stored as [a,b,c,d])
		*)
	проц AdrToArray*( adr: Adr;  перем arr: массив из симв8;  ofs: цел32;  LSBfirst: булево );
	перем tempAdr: Adr;  i, swapTemp: цел32;

	нач
		tempAdr := adr;
		просей adr.usedProtocol из
		| IPv4:
				если ~(ofs + 4 <= длинаМассива( arr )) то  tempAdr := NilAdr  всё;
				если LSBfirst то  SwapEndian( tempAdr.ipv4Adr )  всё;
				S.копируйПамять( адресОт( tempAdr.ipv4Adr ), адресОт( arr[ofs] ), 4 );
		| IPv6:
				если ~(ofs + 16 <= длинаМассива( arr )) то  tempAdr := NilAdr  всё;
				если LSBfirst то
					нцДля i := 0 до 3 делай
						S.копируйПамять( адресОт( tempAdr.ipv6Adr[i*4] ), адресОт( swapTemp ), 4 );
						SwapEndian( swapTemp );
						S.копируйПамять( адресОт( swapTemp ), адресОт( tempAdr.ipv6Adr[i*4] ), 4 );
					кц;
				всё;
				S.копируйПамять( адресОт( adr.ipv6Adr ), адресОт( arr[ofs] ), 16 );
		иначе
		всё;
	кон AdrToArray;


	(** Aos command: Output statistics and configuration of all installed interfaces. *)
	проц IPConfig*( par: динамическиТипизированныйУкль ): динамическиТипизированныйУкль;
	нач
		ЛогЯдра.пСтроку8( "Interfaces:" );  ЛогЯдра.пВК_ПС;  возврат НУЛЬ;
	кон IPConfig;


	(* Return TRUE if adr matches the prefix *)
	проц MatchPrefix*( adr: Adr;  prefix: Adr ): булево;
	перем
		bytesToCheck: цел32;  bitsToCheck: цел32;  i: цел32;  matches: булево;  diffSet: мнвоНаБитахМЗ;
	нач
		matches := истина;
		bytesToCheck := prefix.data DIV 8;  bitsToCheck := prefix.data остОтДеленияНа 8;
		нцДля i := 0 до bytesToCheck - 1 делай
			если adr.ipv6Adr[i] # prefix.ipv6Adr[i] то  matches := ложь  всё;
		кц;
		если bitsToCheck # 0 то
			diffSet := мнвоНаБитахМЗ({0..bitsToCheck});
			если (S.подмениТипЗначения( мнвоНаБитах8, adr.ipv6Adr[bytesToCheck] ) - diffSet) # (S.подмениТипЗначения( мнвоНаБитах8, prefix.ipv6Adr[bytesToCheck] ) - diffSet) то  
				matches := ложь
			всё
		всё;
		возврат matches
	кон MatchPrefix;


	(** Checks if a string is a valid IPv4 address *)
	проц IsValidIPv4Str( конст ipString: массив из симв8 ): булево;
	перем i, j: цел32;  ipNr: цел32;
		digits: массив 4 из симв8;
		startClass: цел32;
	нач
		i := 0;

		(* Class A *)
		нцПока (i < Строки8.КвоБайтБезЗавершающего0( ipString )) и (ipString[i] # '.') и (i < 3) делай  digits[i] := ipString[i];  увел( i )  кц;
		digits[i] := 0X;

		если ipString[i] # '.' то  возврат ложь  всё;

		(* Check if in digits are only numbers *)
		j := 0;
		нцПока digits[j] # 0X делай
			если (кодСимв8( digits[j] ) - кодСимв8( "0" )) > 9 то  возврат ложь   всё;
			увел( j );
		кц;
		Строки8.ПрочтиЦел32_изСтроки( digits, ipNr );
		если ipNr > 255 то  возврат ложь   всё;

		(* Class B *)
		увел( i );  startClass := i;
		нцПока (i < Строки8.КвоБайтБезЗавершающего0( ipString )) и (ipString[i] # '.') и (i - startClass <= 3) делай
			digits[i - startClass] := ipString[i];  увел( i );
		кц;
		digits[i - startClass] := 0X;

		если ipString[i] # '.' то  возврат ложь  всё;

		(* Check if in digits are only number *)
		j := 0;
		нцПока digits[j] # 0X делай
			если (кодСимв8( digits[j] ) - кодСимв8( "0" )) > 9 то  возврат ложь   всё;
			увел( j );
		кц;
		Строки8.ПрочтиЦел32_изСтроки( digits, ipNr );
		если ipNr > 255 то  возврат ложь   всё;

		(* Class C *)
		увел( i );  startClass := i;
		нцПока (i < Строки8.КвоБайтБезЗавершающего0( ipString )) и (ipString[i] # '.') и (i - startClass <= 3) делай
			digits[i - startClass] := ipString[i];  увел( i );
		кц;
		digits[i - startClass] := 0X;

		если ipString[i] # '.' то  возврат ложь  всё;

		(* Check if in digits are only number *)
		j := 0;
		нцПока digits[j] # 0X делай
			если (кодСимв8( digits[j] ) - кодСимв8( "0" )) > 9 то  возврат ложь   всё;
			увел( j );
		кц;
		Строки8.ПрочтиЦел32_изСтроки( digits, ipNr );
		если ipNr > 255 то  возврат ложь   всё;

		(* Class D *)
		увел( i );  startClass := i;
		нцПока (i < Строки8.КвоБайтБезЗавершающего0( ipString )) и (i - startClass <= 3) делай  digits[i - startClass] := ipString[i];  увел( i )  кц;
		digits[i - startClass] := 0X;

		(* Check if in digits are only number *)
		j := 0;
		нцПока digits[j] # 0X делай
			если (кодСимв8( digits[j] ) - кодСимв8( "0" )) > 9 то  возврат ложь   всё;
			увел( j );
		кц;
		Строки8.ПрочтиЦел32_изСтроки( digits, ipNr );
		если ipNr > 255 то  возврат ложь   всё;

		возврат истина;
	кон IsValidIPv4Str;


	(** Checks if a string is a valid IPv6 address *)
	проц IsValidIPv6Str( ipString: массив из симв8 ): булево;
	перем i: цел32;
		state: цел32;   (* -1: error *)
		charCount: цел32;
		ascD: цел32;  ascH: цел32;
		dPointOcc: булево;
		prefixLenArr: массив 3 из цел32;
		prefixLen: цел32;
	нач
		i := 0;  state := 1;  dPointOcc := ложь;
		Строки8.СтрокуВВерхнийРегистрASCII( ipString );

		нцПока (i < (длинаМассива( ipString ) - 1)) и (ipString[i] # 0X) делай
			просей state из
			-1:       возврат ложь;
			| 1:
						(* 0-9 & A-F *)
						ascD := кодСимв8( ipString[i] ) - кодСимв8( "0" );
						ascH := кодСимв8( ipString[i] ) - кодСимв8( "A" );

						если ((ascD >= 0) и (ascD <= 9)) или ((ascH >= 0) и (ascH <= 5)) то
							увел( charCount );
							если charCount > 4 то  state := -1  всё;
						аесли ipString[i] = ":" то  charCount := 0;  state := 2;
						аесли ipString[i] = "/" то  charCount := 0;  state := 3;
						иначе  state := -1;
						всё;
			| 2:       ascD := кодСимв8( ipString[i] ) - кодСимв8( "0" );
						ascH := кодСимв8( ipString[i] ) - кодСимв8( "A" );
						если ipString[i] = ":" то
							если dPointOcc то  state := -1  иначе  dPointOcc := истина;  state := 4  всё
						аесли ((ascD >= 0) и (ascD <= 9)) или ((ascH >= 0) и (ascH <= 5)) то  увел( charCount );  state := 1;
						иначе  state := -1;
						всё;
			| 3:       ascD := кодСимв8( ipString[i] ) - кодСимв8( "0" );
						если ~((ascD >= 0) и (ascD <= 9)) то  state := -1;
						иначе
							если charCount > 3 то  state := -1  иначе  prefixLenArr[charCount] := ascD;  увел( charCount )  всё;
						всё;
			| 4:       ascD := кодСимв8( ipString[i] ) - кодСимв8( "0" );
						ascH := кодСимв8( ipString[i] ) - кодСимв8( "A" );
						если ipString[i] = "/" то  state := 3;
						аесли ((ascD >= 0) и (ascD <= 9)) или ((ascH >= 0) и (ascH <= 5)) то  увел( charCount );  state := 1;
						иначе  state := -1;
						всё;
			иначе
			всё;
			увел( i );
		кц;

		просей state из
		| 1:       возврат истина;
		| 3:       если charCount > 0 то  prefixLen := 0;
						нцДля i := 0 до charCount - 1 делай  prefixLen := prefixLen*10;  увел( prefixLen, prefixLenArr[i] )  кц;
						если prefixLen <= 64 то  возврат истина  иначе  возврат ложь  всё;
					иначе  возврат ложь;
					всё;
		| 4:      возврат истина;
		иначе
			возврат ложь;
		всё;
		возврат ложь;
	кон IsValidIPv6Str;


	(** Set IPv6 address to zero *)
	проц SetIPv6AdrNil( adr: Adr );
	перем i: цел32;
	нач
		нцДля i := 0 до 15 делай  adr.ipv6Adr[i] := 0X  кц;
	кон SetIPv6AdrNil;


	(* Swap internal representation of an IP address from big to little endian or vice versa. *)
	проц  -SwapEndian(  перем adr: цел32 );
	машКод
	#если I386 то
		POP EAX
		MOV ECX, [EAX]
		XCHG CL, CH
		ROL ECX, 16
		XCHG CL, CH
		MOV [EAX], ECX
	#аесли AMD64 то
		POP RAX
		MOV ECX, [RAX]
		XCHG CL, CH
		ROL ECX, 16
		XCHG CL, CH
		MOV [RAX], ECX
	#иначе
		unimplemented
	#кон
	кон SwapEndian;


нач
	(* NilAdr *)
	NilAdr.ipv4Adr := NilAdrIPv4;
	SetIPv6AdrNil( NilAdr );
	NilAdr.usedProtocol := NilAdrIdent;
кон IP.




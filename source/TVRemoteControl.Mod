модуль TVRemoteControl;	(** AUTHOR "oljeger@student.ethz.ch"; PURPOSE "Graphical channel switcher for TV"; *)

использует
	TVDriver, TV, TVChannels, Standard := WMStandardComponents, Base := WMComponents, Commands,
	Потоки, WM := WMWindowManager, Modules, ЛогЯдра, TeletextViewer, WMDialogs,
	Messages := WMMessages, WMRestorable, XML, Строки8, WMGrids, WMStringGrids;

конст
	Width = 200;
	ButtonWidth = 100;
	ButtonHeight = 30;
	ButtonsPerRow = 3;

	ButtonCol = 0FF00008FH;

тип
	(** Remote control window *)
	Window = окласс (Base.FormWindow)
	перем
		vcd: TVDriver.VideoCaptureDevice;
		devNr: цел32;
		tuner: TVDriver.TVTuner;
		audio: TVDriver.Audio;
		tvWnd: TV.TvWindow;
		nofChannels: размерМЗ;
		buttons: укль на массив из Standard.Button;
		isMute: булево;
		muteButton: Standard.Button;
		next: Window;

		проц &New *(vcd: TVDriver.VideoCaptureDevice; devNr: цел32; openTV: булево);
		перем
			i: размерМЗ;
			fix, panel: Standard.Panel;
			channel: TVChannels.TVChannel;
			openButton: Standard.Button;
			txtButton: Standard.Button;
			channelList : WMStringGrids.StringGrid;
		нач
			nofChannels := TVChannels.channels.GetCount();
			если vcd = НУЛЬ то
				ЛогЯдра.пСтроку8("{TVRemoteControl} ERROR: Fail to locate video capture device.");
				ЛогЯдра.пВК_ПС;
				возврат;
			всё;
			сам.vcd := vcd;
			tuner := vcd.GetTuner();
			audio := vcd.GetAudio();
			сам.devNr := devNr;
			нов(panel);
			panel.alignment.Set(Base.AlignClient);

			(* add a panel *)
			panel.fillColor.Set(0FFH);

			(* add 'fix' panel *)
			нов (fix);
			fix.alignment.Set (Base.AlignTop);
			fix.bounds.SetHeight (ButtonHeight);

			нов (channelList);

			channelList.model.Acquire;
			channelList.model.SetNofCols(1);
			channelList.model.SetNofRows(1);
			channelList.SetSelectionMode(WMGrids.GridSelectRows);
			channelList.model.Release;


			(* add 'Open TV Window' button *)
			нов (openButton);
			openButton.caption.SetAOC ("Open TV");
			openButton.bounds.SetWidth (Width DIV 3);
			openButton.onClick.Add (OnOpen);
			openButton.clDefault.Set (ButtonCol);
			openButton.alignment.Set (Base.AlignLeft);
			openButton.bounds.SetHeight (ButtonHeight);
			fix.AddContent (openButton);

			(* add 'Mute' button *)
			нов(muteButton);
			muteButton.caption.SetAOC ("Mute");
			muteButton.bounds.SetWidth (Width DIV 3);
			muteButton.onClick.Add (OnMuteToggle);
			muteButton.clDefault.Set (ButtonCol);
			muteButton.alignment.Set (Base.AlignLeft);
			muteButton.bounds.SetHeight (ButtonHeight);
			fix.AddContent (muteButton);
			isMute := ложь;

			(* add 'TXT' button *)
			нов (txtButton);
			txtButton.caption.SetAOC ("TXT");
			txtButton.bounds.SetWidth (Width DIV 3);
			txtButton.clDefault.Set (ButtonCol);
			txtButton.onClick.Add (OnTXT);
			txtButton.alignment.Set (Base.AlignLeft);
			txtButton.bounds.SetHeight (ButtonHeight);
			fix.AddContent (txtButton);

			panel.AddContent (fix);

			panel.AddContent (channelList);
			channelList.alignment.Set(Base.AlignClient);


	(*		(* add channel buttons *)
			IF nofChannels > 0 THEN
				NEW (buttons, nofChannels);
				FOR i := 0 TO nofChannels-1 DO
					NEW (buttons[i]);
					buttons[i].bounds.SetHeight (ButtonHeight);
					buttons[i].alignment.Set (Base.AlignTop);
					channel := TVChannels.channels.GetItem(i);
					buttons[i].caption.SetAOC (channel.name);
					buttons[i].onClick.Add (OnPush);
					panels[i MOD ButtonsPerRow].AddContent (buttons[i])
				END
			END; *)

			(* create the form window with panel size *)
			Init(Width, 300, истина);
			SetContent(panel);

			(* open the window *)
			manager := WM.GetDefaultManager();
			SetTitle(WM.NewString("TV Remote Control"));
			manager.Add(770, 100, сам, {WM.FlagFrame});
			next := window;
			window := сам;

			channelList.model.Acquire;
			channelList.model.SetNofRows(nofChannels(цел32));
			нцДля i := 0 до nofChannels-1 делай
				channel := TVChannels.channels.GetItem(i);
				channelList.model.SetCellText(0, цел32(i), Строки8.ЯвиУСтроку(channel.name));
				channelList.model.SetCellData(0, цел32(i), channel);
			кц;
			channelList.model.Release;
			channelList.Invalidate;
			channelList.onClick.Add(OnPush);


			(* open the TV window (there will be only one single instance) *)
			если openTV то
				OnOpen (НУЛЬ, НУЛЬ)
			всё
		кон New;

		(** Find a button for correct action *)
		проц FindButton (button: Standard.Button): цел32;
		перем i: цел32;
		нач
			i := 0;
			нцПока (i < длинаМассива(buttons)) и (buttons[i] # button) делай
				увел(i)
			кц;
			возврат i
		кон FindButton;

		(** Switch to the appropriate TV channel *)
		проц OnPush (sender, data: динамическиТипизированныйУкль);
		перем channel: TVChannels.TVChannel;
		нач
			если (data # НУЛЬ) и (data суть TVChannels.TVChannel) то
				channel := data(TVChannels.TVChannel);
				если tuner # НУЛЬ то
					tuner.SetTVFrequency (channel.freq)
				всё
			всё
		кон OnPush;

		(** Toggle audio mute state *)
		проц OnMuteToggle (sender, data: динамическиТипизированныйУкль);
		нач
			если isMute то
				muteButton.caption.SetAOC ("Mute");
				audio.SetAudioUnmute
			иначе
				muteButton.caption.SetAOC ("Unmute");
				audio.SetAudioMute
			всё;
			isMute := ~isMute
		кон OnMuteToggle;

		(** Open a TV window. Do nothing if already open *)
		проц OnOpen (sender, data: динамическиТипизированныйУкль);
		нач
			если ~ vcd.IsVideoOpen() то
				нов(tvWnd, vcd);
				(* Set the device number for non-default devices *)
				если devNr # -1 то
					tvWnd.vcdNr := devNr
				всё
			всё
		кон OnOpen;

		(** Open a teletext viewer window *)
		проц OnTXT (sender, data: динамическиТипизированныйУкль);
		перем
			viewer: TeletextViewer.TeletextViewer;
		нач
			нов(viewer);
			если (tvWnd # НУЛЬ) и tvWnd.alive то
				viewer.Switch(tvWnd.GetTVFreq());
				tvWnd.StartTeletextCapture
			иначе
				tvWnd := НУЛЬ
			всё
		кон OnTXT;

		(** Select another VideoCaptureDevice *)
		проц SetDevice* (dev: цел32);
		нач
			vcd := TVDriver.GetVideoDevice (dev);
			tuner := vcd.GetTuner();
			audio := vcd.GetAudio();
			devNr := dev
		кон SetDevice;

		(** Close the remote control window *)
		проц {перекрыта}Close;
		нач
			FreeWindow(сам);
			Close^
		кон Close;

		(** Handle window messages *)
		проц {перекрыта}Handle(перем m : Messages.Message);
		перем
			data: XML.Element;
			str: массив 10 из симв8;
		нач
			если (m.msgType = Messages.MsgExt) и (m.ext # НУЛЬ) то
				если (m.ext суть WMRestorable.Storage) то
					нов(data);  data.SetName("TVRemoteControlData");
					Строки8.ПишиЦел64_вСтроку(devNr, str);
					data.SetAttributeValue("device", str);
					m.ext(WMRestorable.Storage).Add("TVRemoteControl",
							"TVRemoteControl.Restore", сам, data)
				иначе Handle^(m)
				всё
			иначе Handle^(m)
			всё
		кон Handle;

	кон Window;

перем
	window: Window;

(** Open a remote control window *)
проц Open* (context : Commands.Context);
перем
	vcd : TVDriver.VideoCaptureDevice;
	devNr: цел32;
	wnd: Window;
нач {единолично}
	если context # НУЛЬ то
		(* Read VideoCaptureDevice number *)
		если context.arg.ПропустиБелоеПолеИЧитайЦел32(devNr, ложь) то
			vcd := TVDriver.GetVideoDevice(devNr)
		иначе
			devNr := -1;
			vcd := TVDriver.GetDefaultDevice()
		всё
	иначе
		devNr := -1;
		vcd := TVDriver.GetDefaultDevice()
	всё;
	(* Display error message if no VideoCaptureDevice has been found *)
	если vcd = НУЛЬ то
		если (context # НУЛЬ) и (context.arg.кодВозвратаПоследнейОперации = 0) то
			context.error.пСтроку8("{TV} Parameter is not a valid video device number."); context.error.пВК_ПС;
			WMDialogs.Error("TV - Error", "Parameter is not a valid video device number. Make sure that all TV card drivers are loaded.");
			возврат;
		иначе
			context.error.пСтроку8("{TV} Cannot open TV window: Fail to locate video capture device.");
			context.error.пВК_ПС;
			WMDialogs.Error("TVRemoteControl - Error",
				"Cannot open TV window: Fail to locate video capture device. Install device before opening the TV window. Example: BT848.Install");
			возврат
		всё
	всё;
	нов (wnd, vcd, devNr, истина);
кон Open;

(** Restore stored windows *)
проц Restore*(c : WMRestorable.Context);
перем
	manager: WM.WindowManager;
	xml: XML.Element;
	s: Строки8.уСтрока;
	devNr: цел32;
	vcd: TVDriver.VideoCaptureDevice;
	wnd: Window;
нач
	(* restore the desktop *)
	если c.appData # НУЛЬ то
		xml := c.appData(XML.Element);
		s := xml.GetAttributeValue("device");
		если s # НУЛЬ то
			(* Read device number *)
			Строки8.ПрочтиЦел32_изСтроки(s^, devNr);
			если devNr = -1 то
				vcd := TVDriver.GetDefaultDevice()
			иначе
				vcd := TVDriver.GetVideoDevice(devNr)
			всё;
			если vcd # НУЛЬ то
				нов(wnd, vcd, devNr, ложь);
				manager := WM.GetDefaultManager();
				manager.Remove(wnd);
				WMRestorable.AddByContext(wnd, c)
			иначе
				ЛогЯдра.пСтроку8("{TVRemoteControl} Could not restore the RemoteControl window."); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("{TVRemoteControl} Install the device driver first, e.g. BT848.Install"); ЛогЯдра.пВК_ПС
			всё
		всё
	всё
кон Restore;

(** Remove the window from the internal channelList *)
проц FreeWindow(wnd: Window);
перем
	w: Window;
нач
	если wnd = НУЛЬ то
		возврат
	аесли wnd = window то
		(* wnd is first channelList element *)
		window := window.next
	иначе
		w := window;
		нцПока (w # НУЛЬ) и (w.next # wnd) делай
			w := w.next
		кц;
		если w # НУЛЬ то
			(* wnd found: remove it from the channelList *)
			w.next := wnd.next
		всё
	всё;
кон FreeWindow;

(** Term hander *)
проц Cleanup;
перем
	w: Window;
нач
	w := window;
	нцПока w # НУЛЬ делай
		w.Close;
		w := w.next
	кц;
	window := НУЛЬ
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
кон TVRemoteControl.


System.Free TVRemoteControl ~
TVRemoteControl.Open ~

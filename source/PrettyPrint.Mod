модуль PrettyPrint; (** AUTOR "GF"; PURPOSE "pretty printing of Oberon programs"; *)

(* ----------------------------------------------------------
	Usage:
		PrettyPrint.Convert
			<filename>  <filename>  ...  ~


	The real printing on a postscript printer can be done in the Oberon subsystem:

		Desktops.PrintDoc  <printer name>	PPr/<filename> ~

  ----------------------------------------------------------- *)

использует Commands, Files, Strings, Потоки, Texts, TextUtilities, SyntaxHighlighter;

конст
	PPDir = "PPr";

перем
	highlighterU, highlighterL: SyntaxHighlighter.Highlighter;
	out, err: Потоки.Писарь;

	проц CheckSubDirectory( конст dname: массив из симв8 ): булево;
	перем
		enum: Files.Enumerator;
		fname, cname: массив 64 из симв8; time, date: цел32; size: Files.Size; flags: мнвоНаБитахМЗ;
	нач
		cname := "./";  Strings.Append( cname, dname );
		нов( enum );
		enum.Open( "./*", {} );
		нцПока enum.GetEntry( fname, flags, time, date, size ) делай
			если fname = cname то  возврат истина  всё
		кц;
		возврат ложь
	кон CheckSubDirectory;


	(* convert program sources into syntax highlighted Oberon Texts in subdir 'PPr' *)

	проц Convert*( context: Commands.Context );	(*  {filename} ~ *)
	перем
		filename, dest: массив 64 из симв8;  format: цел32; l: размерМЗ; res: целМЗ;
		f: Files.File;  text: Texts.Text;
		highlighter: SyntaxHighlighter.Highlighter;
	нач
		out := context.out;  err := context.error;

		out.пСтроку8( "PrettyPrint.Convert" ); out.пВК_ПС;
		если ~CheckSubDirectory( PPDir ) то  Files.CreateDirectory( PPDir, res )  всё;
		если highlighterU = НУЛЬ то  LoadHighlighter  всё;
		если highlighterU # НУЛЬ то
			нцПока context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( filename ) делай
				out.пСтроку8( "    " );  out.пСтроку8( filename );
				l := Strings.Length( filename );
				если filename[l-3] = 'm' то  highlighter := highlighterL  иначе  highlighter := highlighterU всё;
				f := Files.Old( filename );
				если f = НУЛЬ то
					err.пСтроку8( " : file not found" ); err.пВК_ПС;
				иначе
					нов( text );
					TextUtilities.LoadAuto( text, filename, format, res );
					SyntaxHighlighter.HighlightText( text, highlighter );
					dest := PPDir; Strings.Append( dest, "/" ); Strings.Append( dest, filename );
					TextUtilities.StoreOberonText( text, dest, res );
					out.пСтроку8( " => " ); out.пСтроку8( dest );  out.пВК_ПС;
				всё
			кц
		иначе
			err.пСтроку8( "Highlighter 'PrintOberon' not found" );  err.пВК_ПС
		всё;
		out.ПротолкниБуферВПоток;  err.ПротолкниБуферВПоток
	кон Convert;


	проц LoadHighlighter;
	перем res: целМЗ; msg: массив 128 из симв8;
	нач
		Commands.Call( "SyntaxHighlighter.Open PrettyPrintHighlighter.XML", {}, res, msg );
		highlighterU := SyntaxHighlighter.GetHighlighter( "PrintOberon" );
		highlighterL := SyntaxHighlighter.GetHighlighter( "printoberon" );
		Commands.Call( "SyntaxHighlighter.Open SyntaxHighlighter.XML", {}, res, msg );
	кон LoadHighlighter;

нач
	highlighterU := НУЛЬ;
	highlighterL := НУЛЬ
кон PrettyPrint.


	System.Free PrettyPrint ~

	PrettyPrint.Convert  PrettyPrint.Mod  Huffman.mod Xyz.Mod ~




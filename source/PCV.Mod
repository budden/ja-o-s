(* Paco, Copyright 2000 - 2002, Patrik Reali, ETH Zurich *)

модуль PCV; (** AUTHOR "prk / be"; PURPOSE "Parallel Compiler: symbol allocation"; *)

использует
	StringPool, ЛогЯдра,
	PCM, PCT, PCBT, PCLIR, PCC;

конст
	Trace = ложь;

(*
		(*procedure parameters*)
	RecVarParSize = 8;
	VarParSize = 4;

		(* Procedure Parameter Offsets *)
	ProcOff = 8;
	ProcOffSL = 12;
*)

		(* back-end types *)
	Signed = истина; Unsigned = ложь;

перем
	ptrsize, procsize, delegsize: PCBT.Size;	(*used by TypeSize*)

	AAllocPar, AAllocParSize,	(*allocated parameters, parameters that required TypeSize*)
	AfieldAllocated: цел32;	(*allocating an already allocated field*)

проц VarOffset(перем offset, varOffset: цел32;  var: PCT.Variable);
перем size: цел32;
нач
	size := var.type.size(PCBT.Size).size;
	увел(offset, size);
	если size >= 4 то  увел(offset, (-offset) остОтДеленияНа 4)
	аесли size = 2 то  увел(offset, (-offset) остОтДеленияНа 2)
	всё;
	varOffset := -offset
кон VarOffset;

проц ParOffset(перем offset, varOffset: цел32; type: PCT.Struct; isRef: булево; flags: мнвоНаБитахМЗ);  (* ug *)
перем size: цел32; open: булево;enhopen: булево; (* fof *)
нач
	если (type суть PCT.Array) и ({PCT.WinAPIParam,PCT.CParam}*flags #{} (* fof for linux *) ) то умень(type.size(PCBT.Size).size, PCLIR.CG.ParamAlign) всё; (* Notag *)
	size := type.size(PCBT.Size).size;
	open := (type суть PCT.Array) и (type(PCT.Array).mode = PCT.open);
	(** fof >> *)
	enhopen := (type суть PCT.EnhArray) и (type( PCT.EnhArray ).mode = PCT.open);
	(** << fof  *)
	если isRef и ~open то
       	если type суть PCT.Record то (* fof start*)
          		 если PCT.WinAPIParam в flags то
               		size := PCLIR.CG.ParamAlign
          		 аесли PCT.CParam в flags то (* fof for Linux *)
               				size := PCLIR.CG.ParamAlign
               			иначе
               	size := PCLIR.CG.ParamAlign * 2
           		всё
			(* fof end *)
       	иначе  size := PCLIR.CG.ParamAlign
       	всё
		(** fof >> *)
		аесли   isRef и enhopen то  (* VAR A: ARRAY [..] OF ... *)
			size := PCLIR.CG.ParamAlign
		(** << fof  *)
   	всё;
	увел(offset, size);
	увел(offset, (-offset) остОтДеленияНа PCLIR.CG.ParamAlign);
	varOffset := offset
кон ParOffset;

проц FieldOffset(перем offset: цел32;  size, align: цел32;  p: PCT.Symbol);
перем  adr: PCBT.Variable;
нач
	если p.adr # НУЛЬ то
		увел(AfieldAllocated)
	иначе
		нов(adr); p.adr := adr;
		если align = 4 то  увел(offset, (-offset) остОтДеленияНа 4)
		аесли align = 2 то  увел(offset, (-offset) остОтДеленияНа 2)
		аесли align # 1 то  СТОП(99)
		всё;
		adr.offset := offset
	всё;
(*
;PCM.LogWLn; PCM.LogWStr("FieldOffset "); PCM.LogWStr(p.name); PCM.LogWNum(offset);
PCM.LogWNum(size); PCM.LogWNum(align);
*)
	увел(offset, size)
кон FieldOffset;

проц TypeSizeShallow(type: PCT.Struct; перем redo: булево);
(* fof 070917
create a type size recursively without traversing pointers in data structures.
needed to prevent deadlock in the following situation

TYPE
	A= OBJECT
	VAR b: (any Data structure containing a variable of type B)
	(example:  b: ARRAY 10 OF B )
	END A;

	B=OBJECT
	VAR a: (any Data structure containing a variable of type A)
	(example:  a: RECORD c,d,e: SIGNED32; a: A; END;
	END B;

	The deadlock prevention is based on the allocation of size descriptors for a type X
	before traversing any pointers being contained (directly or indirectly) in X

	Pointers are identified by recursive traversal of types
*)

перем  size, fsize: PCBT.Size; recsize, brecsize: PCBT.RecSize; t: PCT.Struct;
		f: PCT.Variable;
нач
	если type.size= НУЛЬ то
		если type суть PCT.Basic то
			СТОП(99);
		аесли type суть PCT.Pointer то
			нов(size);
			size.size := ptrsize.size; size.align := 4; size.type := PCLIR.Address;
			size.containPtrs := истина;
			type.size := size;
			redo := истина; (* contains pointers, therefore recursion has to be restartet after allocation of size descriptors *)
		аесли type суть PCT.Record то
			просейТип type: PCT.Record делай
				если (PCM.GetProcessID() # type.scope.ownerID)  то
					type.scope.Await(PCT.structshallowallocated);
				иначе
					нов(recsize);  recsize.type := PCLIR.NoSize; recsize.level := 0;
					если type.brec # НУЛЬ то
						TypeSizeShallow(type.brec,redo); brecsize := type.brec.size(PCBT.RecSize);
						recsize.size := brecsize.size;
						recsize.level := brecsize.level+1;
						recsize.containPtrs := brecsize.containPtrs;
						если ~type.imported и type.brec.imported то PCBT.AllocateTD(brecsize) всё
					всё;
					f := type.scope.firstVar;
					нцПока f # НУЛЬ делай
						t := f.type;
						TypeSizeShallow(t,redo);
						fsize := t.size(PCBT.Size);
						recsize.containPtrs := recsize.containPtrs или fsize.containPtrs;
						FieldOffset(recsize.size, fsize.size, fsize.align, f);
						f := f.nextVar
					кц;
					увел(recsize.size, (-recsize.size) остОтДеленияНа 4);
					recsize.align := 4;
					утв(type.size = НУЛЬ);
					type.size := recsize;
					PCT.StateStructShallowAllocated(type.scope);
					если ~type.imported и ~(PCT.SystemType в type.flags) то PCBT.AllocateTD(recsize) всё;
				всё;
			всё;
		аесли type суть PCT.Array то
			просейТип type: PCT.Array делай
				нов(size); size.type := PCLIR.Address;
				если type.mode = PCT.open то
					size.size := type.opendim * PCLIR.CG.ParamAlign + PCLIR.CG.ParamAlign;
					size.align := PCLIR.CG.ParamAlign;
					size.containPtrs := ложь;
					type.size := size;
					redo := истина;
				аесли type.mode = PCT.static то
					TypeSizeShallow(type.base,redo);
					fsize := type.base.size(PCBT.Size);
					size.size := fsize.size * type.len;
					size.align := fsize.align;
					size.containPtrs := fsize.containPtrs;
					type.size := size;
				иначе
					СТОП(98)
				всё
			всё
			(** fof >> *)
		аесли type суть PCT.EnhArray то
			просейТип type: PCT.EnhArray делай
				нов( size );  size.type := PCLIR.Address;
				если type.mode = PCT.static то
					TypeSize( type.base );  fsize := type.base.size( PCBT.Size );
					size.size := fsize.size * type.len;   (* data, we do not write a header for static arrays. *)
					size.align := fsize.align;  size.containPtrs := fsize.containPtrs;  type.size := size;
					PCT.SetEnhArrayInc(type,fsize.size);
				аесли type.mode = PCT.open то
					(* this size corresponds to the size of the designator, not the array itself!! *)
					size.size := type.opendim * 2 * PCT.AddressSize + PCC.Descr_LenOffs * PCT.AddressSize (* has been shifted to front *) ;
					size.align := PCT.AddressSize;  size.containPtrs := истина; (* the designator contains a pointer to the array ! *) type.size := size;
					TypeSize( type.base );
					redo := истина;
				иначе СТОП( 98 )
				всё
			всё;
		аесли type суть PCT.Tensor то
			просейТип type: PCT.Tensor делай
				нов( size );  type.size := size;  size.size := 4;  size.align := 4;  size.type := PCLIR.Address;  size.containPtrs := истина;
				TypeSize( type.base );
			всё;
		(** << fof  *)
		аесли type суть PCT.Delegate то
			просейТип type: PCT.Delegate делай
				если PCT.StaticMethodsOnly в type.flags то
					size := procsize
				иначе
					size := delegsize
				всё;
				type.size := size;
				утв(size.size > 0, 999);
				redo := истина; (* may contain references to self when methods in record refer to record *)
			всё
		аесли type = PCT.String то	(*skip*)
		иначе
			PCM.LogWLn;
			PCM.LogWType(type);
			СТОП(97)
		всё;
		если redo то type.size(PCBT.Size).needsrecursion := истина иначе type.size(PCBT.Size).needsrecursion := ложь всё;
	всё;
кон TypeSizeShallow;


проц TypeSize(type: PCT.Struct);
перем  size: PCBT.Size; p: PCT.Parameter; redo: булево;
		f: PCT.Variable;
		name, namef: массив 256 из симв8;
нач
	если type.size = НУЛЬ то
		PCT.GetTypeName(type, name);
		если Trace то
			PCM.LogWLn; PCM.LogWStr("PCV.TypeSize "); PCM.LogWStr(name)
		всё;
		если type суть PCT.Basic то
			PCT.PrintString(type.owner.name); ЛогЯдра.пВК_ПС;
			СТОП(99)
		аесли type суть PCT.Pointer то
			просейТип type: PCT.Pointer делай
				нов(size);
				size.size := ptrsize.size; size.align := 4; size.type := PCLIR.Address;
				size.containPtrs := истина;
				type.size := size;
				TypeSize(type.base);
			всё
		аесли type суть PCT.Record то
			просейТип type: PCT.Record делай
				если (PCM.GetProcessID() # type.scope.ownerID)  то
					type.scope.Await(PCT.structallocated);
					утв(type.size # НУЛЬ, 500)
				иначе
					redo := ложь; TypeSizeShallow(type,redo);
					если redo то
						f := type.scope.firstVar;
						нцПока f # НУЛЬ делай
							StringPool.GetString(f.name, namef);
							TypeSize(f.type);
							f := f.nextVar
						кц;
					всё
				всё
			всё
		аесли type суть PCT.Array то
			redo := ложь; TypeSizeShallow(type,redo);
			если redo то TypeSize(type(PCT.Array).base); всё
		(** fof >> *)
		аесли type суть PCT.EnhArray то
			redo := ложь; TypeSizeShallow(type,redo);
			если redo то TypeSize(type(PCT.EnhArray).base); всё
		аесли type суть PCT.Tensor то
			просейТип type: PCT.Tensor делай
				нов( size );  type.size := size;  size.size := 4;  size.align := 4;  size.type := PCLIR.Address;  size.containPtrs := истина;
				TypeSize( type.base );
			всё;
		(** << fof  *)
		аесли type суть PCT.Delegate то
			просейТип type: PCT.Delegate делай
				redo := ложь; TypeSizeShallow(type,redo);
				если redo то
					p := type.scope.firstPar;
					нцПока p # НУЛЬ делай
						TypeSize(p.type);
						p := p.nextPar
					кц;
					TypeSize(type.return)
				всё;
			всё
		аесли type = PCT.String то (*skip*)
		иначе
			PCM.LogWLn;
			PCM.LogWType(type);
			СТОП(97)
		всё;
		type.size(PCBT.Size).needsrecursion := ложь;
	аесли type.size(PCBT.Size).needsrecursion то
		type.size(PCBT.Size).needsrecursion := ложь;
		если type  суть PCT.Pointer то
			TypeSize(type(PCT.Pointer).base);
		аесли type суть PCT.Record то
			просейТип type: PCT.Record делай
				f := type.scope.firstVar;
				нцПока f # НУЛЬ делай
					StringPool.GetString(f.name, namef);
					TypeSize(f.type);
					f := f.nextVar
				кц;
			всё;
		аесли type суть PCT.Array то
			TypeSize(type(PCT.Array).base);
		аесли type суть PCT.Delegate то
			просейТип type: PCT.Delegate делай
				p := type.scope.firstPar;
				нцПока p # НУЛЬ делай
					TypeSize(p.type);
					p := p.nextPar
				кц;
				TypeSize(type.return)
			всё
		всё;
	всё;
кон TypeSize;

проц AllocateParameters(p: PCT.Proc);
перем  adr: PCBT.Variable;  offset: цел32;  par: PCT.Parameter;rp: PCT.ReturnParameter;(* fof *)
нач
	par := p.scope.firstPar;
	offset := 0;
	нцПока (par # НУЛЬ) делай
		увел(AAllocPar);
		если par.type.size = НУЛЬ то  TypeSize(par.type); увел(AAllocParSize) всё;
		утв(par.type.size # НУЛЬ);	(* p.scope.parent >= allocated *)
		нов(adr); par.adr := adr;
		ParOffset(offset, adr.offset , par.type, par.ref, par.flags);
		par := par.nextPar
	кц;
(*	INC(offset, ProcOff);
	IF p.level # 0 THEN  INC(offset, ProcOffSL-ProcOff)  END; *)

	увел (offset, PCLIR.CG.ParamAlign * 2);
	если p.level # 0 то  увел (offset, PCLIR.CG.ParamAlign)  всё;

	par := p.scope.firstPar;
	нцПока (par # НУЛЬ) делай
		par.adr(PCBT.Variable).offset := offset - par.adr(PCBT.Variable).offset;
		par := par.nextPar
	кц;
	(** fof >> *)
	rp := p.scope.returnParameter;
	если rp #НУЛЬ то
		если rp.type.size = НУЛЬ то TypeSize(rp.type); всё;
		нов(adr); rp.adr := adr;
		adr.offset := offset ;
	всё;
	(** << fof  *)
	p.adr(PCBT.Procedure).parsize := offset;
кон AllocateParameters;

проц AllocateTypes(t: PCT.Type; v: PCT.Variable);
нач
	нцПока t # НУЛЬ делай
		TypeSize(t.type); t := t.nextType
	кц;
	нцПока v # НУЛЬ делай
		TypeSize(v.type); v := v.nextVar
	кц;
кон AllocateTypes;

проц AllocateLocals(var: PCT.Variable; перем size: цел32);
	перем offset: цел32; ladr: PCBT.Variable;
нач
	offset := size;
	нцПока var # НУЛЬ делай
		TypeSize(var.type);
		нов(ladr); var.adr := ladr;
		VarOffset(offset, ladr.offset , var);
		var := var.nextVar
	кц;
	увел(offset, (-offset) остОтДеленияНа PCLIR.CG.ParamAlign);
	size := offset;
кон AllocateLocals;

проц AllocateGlobals(var: PCT.Variable; mod: PCBT.Module; setOffset: булево; перем size: цел32);
	перем offset: цел32; gadr: PCBT.GlobalVariable;
нач
	offset := size;
	нцПока var # НУЛЬ делай
		TypeSize(var.type);
		нов(gadr, mod); var.adr := gadr;
		если setOffset то VarOffset(offset, gadr.offset , var) всё;
		var := var.nextVar
	кц;
	увел(offset, (-offset) остОтДеленияНа 4);
	size := offset;
кон AllocateGlobals;

(** PreAllocate - called on scope creation, creates PCBT.Address struct *)

проц PreAllocate*(context, scope: PCT.Scope);
перем proc: PCT.Proc; adr: PCBT.Procedure; madr: PCBT.Method;
	gadr: PCBT.GlobalVariable; zero: цел64; imported, visible: булево;
	mod: PCBT.Module;
нач
	если (scope суть PCT.ProcScope) то
		просейТип scope: PCT.ProcScope делай
			mod := scope.module.adr(PCBT.Module);
			proc := scope.ownerO;
			visible := (PCT.PublicR в proc.vis);
			утв(proc.adr = НУЛЬ);
			если proc суть PCT.Method то
				нов(madr, mod, visible);
				proc.adr := madr
			иначе
				нов(adr, mod, visible);
				proc.adr := adr
			всё
		всё
	аесли scope суть PCT.ModScope то
		просейТип scope: PCT.ModScope делай
			imported := scope # context;
			нов(mod); scope.owner.adr := mod;
			если ~imported то
				(*PCM.LogWLn; PCM.LogWStr("PCV.PreAllocate: New PCBT.context");*)
				PCBT.context := mod;
				если (scope.firstVar # НУЛЬ) и (scope.firstVar.name = PCT.SelfName) то
					нов(gadr, PCBT.context); scope.firstVar.adr := gadr;
					gadr.offset := PCBT.context.NewConst(zero, PCT.AddressSize);
				всё
			всё
		всё
	всё
кон PreAllocate;


(** Allocate - scope declarations (var/types) parsed *)

проц Allocate*(context, scope: PCT.Scope; hiddenVarsOnly: булево (* ug *));
	перем proc: PCT.Proc; madr: PCBT.Module; globals: PCT.Variable;
нач
	если scope суть PCT.RecScope то
		если ~hiddenVarsOnly то
			TypeSize(scope(PCT.RecScope).owner)
		всё
	аесли scope суть PCT.ProcScope то
		просейТип scope: PCT.ProcScope делай
			proc := scope.ownerO;
			если hiddenVarsOnly то
				AllocateLocals(scope.firstHiddenVar, proc.adr(PCBT.Procedure).locsize)
			иначе
				AllocateParameters(proc);
				AllocateTypes(scope.firstType, scope.firstVar);
				AllocateLocals(scope.firstVar, proc.adr(PCBT.Procedure).locsize)
			всё
		всё
	аесли scope суть PCT.ModScope то
		просейТип scope: PCT.ModScope делай
			madr := scope.owner.adr(PCBT.Module);
			если hiddenVarsOnly то
				AllocateGlobals(scope.firstHiddenVar, madr, context = scope, madr.locsize)
			иначе
				AllocateTypes(scope.firstType, scope.firstVar);
				globals := scope.firstVar;
(*
				ASSERT((scope.imported) OR (globals.name = PCT.SelfName));
*)
				если (globals # НУЛЬ) и (globals.name = PCT.SelfName) то globals := globals.nextVar всё;
				AllocateGlobals(globals, madr, context = scope, madr.locsize)
			всё
		всё
	всё
кон Allocate;


(** PostAllocate - scope procedures parsed *)

проц PostAllocate*(context, scope: PCT.Scope);
перем  p: PCT.Symbol; rec: PCT.Record; recsize: PCBT.RecSize;
нач
	если scope суть PCT.RecScope то
		просейТип scope: PCT.RecScope делай
			rec := scope.owner;
			recsize := rec.size(PCBT.RecSize);
			если rec.brec # НУЛЬ то
				rec.brec.scope.Await(PCT.procdeclared);
				recsize.nofMethods := rec.brec.size(PCBT.RecSize).nofMethods
			всё;
			p := scope.sorted;
			нцПока p # НУЛЬ делай
				если p суть PCT.Method то
					просейТип p: PCT.Method делай
						увел(recsize.nofLocalMethods);
						если p.super = НУЛЬ то
							p.adr(PCBT.Method).mthNo := recsize.nofMethods;
							увел(recsize.nofMethods)
						иначе
							p.adr(PCBT.Method).mthNo := p.super.adr(PCBT.Method).mthNo
						всё
					всё
				всё;(*if Method*)
				p := p.sorted
			кц
		всё(*WITH RecScope*)
	всё
кон PostAllocate;

проц BasicSize(type: PCT.Struct; size, align: цел32; BEsize: PCLIR.Size; signed: булево);
перем adr: PCBT.Size;
нач
	нов(adr); type.size := adr; adr.size := size; adr.align := align; adr.type := BEsize; adr.signed := signed;
	adr.containPtrs := type = PCT.Ptr
кон BasicSize;

проц Install*;
нач
	PCT.PreAllocate := PreAllocate;
	PCT.Allocate := Allocate;
	PCT.PostAllocate := PostAllocate
кон Install;

проц SetBasicSizes*;

	проц GetSize (size: PCLIR.Size): цел32;
	нач
		просей size из
		PCLIR.Int8: возврат 1;
		| PCLIR.Int16: возврат 2;
		| PCLIR.Int32: возврат 4;
		| PCLIR.Int64: возврат 8;
		всё;
	кон GetSize;

	проц GetAlign (size: PCLIR.Size): цел32;
	нач
		просей size из
		PCLIR.Int8: возврат 1;
		| PCLIR.Int16: возврат 2;
		| PCLIR.Int32: возврат 4;
		| PCLIR.Int64: возврат 4;
		всё;
	кон GetAlign;

	проц DeduceBasicSize (type: PCT.Struct; size: PCLIR.Size; signed: булево);
	нач
		BasicSize (type, GetSize (size), GetAlign (size), size, signed);
	кон DeduceBasicSize;

нач
	DeduceBasicSize (PCT.Set, PCLIR.Set, Unsigned);
	DeduceBasicSize (PCT.NilType, PCLIR.Address, Unsigned);
	DeduceBasicSize (PCT.Ptr, PCLIR.Address, Unsigned);

	(* wrapping the following assignments into a helper function does not work *)

	просей PCLIR.Address из
	PCLIR.Int32: PCT.Address := PCT.Int32;
	| PCLIR.Int64: PCT.Address := PCT.Int64;
	всё;

	PCT.SystemAddress.type := PCT.Address;

	просей PCLIR.Set из
	PCLIR.Int32: PCT.SetType := PCT.Int32;
	| PCLIR.Int64: PCT.SetType := PCT.Int64;
	всё;

	просей PCLIR.SizeType из
	PCLIR.Int32: PCT.Size := PCT.Int32;
	| PCLIR.Int64: PCT.Size := PCT.Int64;
	всё;

	PCT.SystemSize.type := PCT.Size;

	procsize.size := GetSize (PCLIR.Address);
	procsize.type := PCLIR.Address;
	delegsize.size := procsize.size * 2;
	delegsize.type := procsize.type;

	ptrsize := PCT.Ptr.size(PCBT.Size);

	PCT.AddressSize := GetSize (PCLIR.Address);
	PCT.SetSize := GetSize (PCLIR.Set);
кон SetBasicSizes;

нач
	если Trace то PCM.LogWLn; PCM.LogWStr("PCV.Trace on") всё;
	BasicSize(PCT.NoType, -1, 1, PCLIR.NoSize, Unsigned);
	BasicSize(PCT.UndefType, -1, 1, PCLIR.NoSize, Unsigned);
	BasicSize(PCT.Bool, 1, 1, PCLIR.Int8, Unsigned);
	BasicSize(PCT.Byte, 1, 1, PCLIR.Int8, Unsigned);
	BasicSize(PCT.Char8, 1, 1, PCLIR.Int8, Unsigned);
	если PCM.LocalUnicodeSupport то
		BasicSize(PCT.Char16, 2, 2, PCLIR.Int16, Unsigned);
		BasicSize(PCT.Char32, 4, 4, PCLIR.Int32, Unsigned);
	всё;
	BasicSize(PCT.Int8, 1, 1, PCLIR.Int8, Signed);
	BasicSize(PCT.Int16, 2, 2, PCLIR.Int16, Signed);
	BasicSize(PCT.Int32, 4, 4, PCLIR.Int32, Signed);
	BasicSize(PCT.Int64, 8, 4, PCLIR.Int64, Signed);
	BasicSize(PCT.Float32, 4, 4, PCLIR.Float32, Signed);
	BasicSize(PCT.Float64, 8, 4, PCLIR.Float64, Signed);
	BasicSize(PCT.Set, 4, 4, PCLIR.Int32, Unsigned);
	BasicSize(PCT.NilType, 4, 4, PCLIR.Address, Unsigned);
	BasicSize(PCT.Ptr, 4, 4, PCLIR.Address, Unsigned);
	ptrsize := PCT.Ptr.size(PCBT.Size);
	нов(procsize);
	procsize.size := 4; procsize.align := 4; procsize.type := PCLIR.Address; procsize.containPtrs := ложь;
	нов(delegsize);
	delegsize.size := 8; delegsize.align := 4; delegsize.type := PCLIR.Address; delegsize.containPtrs := истина;
кон PCV.

(*
	18.03.02	prk	PCBT code cleanup and redesign
	22.02.02	prk	unicode support
	11.12.01	prk	problem parsing invalid WITH syntax fixed
	28.11.01	prk	explicitly install PCV, avoid depending on the import list sequence
	05.09.01	prk	CanSkipAllocation flag for record scopes
	27.08.01	prk	scope.unsorted list removed; use var, proc, const and type lists instead
	17.08.01	prk	overloading
	13.08.01	prk	fixed bug in allocation size of delegates used in a record but decalred outside
	11.08.01	prk	Fixup and use lists for procedures in PCBT cleaned up
	10.08.01	prk	PCBT.Procedure: imported: BOOLEAN replaced by owner: Module
	09.08.01	prk	Symbol Table Loader Plugin
	02.07.01	prk	access flags, new design
	27.06.01	prk	StringPool cleaned up
	14.06.01	prk	type descs for dynamic arrays of ptrs generated by the compiler
	06.06.01	prk	use string pool for object names
	17.05.01	prk	Delegates
	07.05.01	be	register sign information added in the back-end
	25.03.01	prk	limited SIGNED64 implementation (as abstract type)
	22.02.01	prk	delegates
*)

модуль WebHTTP; (** AUTHOR "tf/be"; PURPOSE "HTTP parsing"; *)
(* 02.04.2003 es, additional result codes, WebDAV methods. *)
(* 12.04.2003 es, WebDAV result codes. *)


использует IP, TFLog, Потоки, Dates, Строки8;

конст
	HTTPPort* = 80;
	HTTPSPort*= 443;

	(** HTTP Result Codes *)
	(*     Informational      *)
	Continue* = 100;
	SwitchingProtocols* = 101;
	Processing* = 102; (* RFC 2518 *)
	(*         Successful        *)
	OK* = 200;
	Created* = 201;
	Accepted*= 202;
	NonAuthoritativeInformation*= 203;
	NoContent*= 204;
	ResetContent*= 205;
	PartialContent*= 206;
	MultiStatus* = 207; (* RFC 2518 *)
	(*	Redirection	*)
	MultipleChoices*= 300;
	ObjectMoved* = 301; (* moved permananently *)
	ObjectMovedTemporarily* = 302;  (* found *)
	SeeOther*= 303;
	NotModified* = 304;
	UseProxy*= 305;
	TemporaryRedirect*= 307;
	(*	Client Error	*)
	BadRequest* = 400;
	Unauthorized* = 401;
	PaymentRequired*= 402;
	Forbidden* = 403;
	NotFound* = 404;
	MethodNotAllowed*= 405;
	NotAcceptable*= 406;
	ProxyAuthenticationRequested*= 407;
	RequestTimeout*= 408;
	Conflict* = 409;
	Gone*= 410;
	LengthRequired* = 411;
	PreconditionFailed* = 412;
	RequestEntityTooLarge*= 413;
	RequestURITooLong* = 414;
	UnsupportedMediaType*= 415;
	RequestedRangeNotSatisfiable*= 416;
	ExpectationFailed*= 417;
	UnprocessableEntity* = 422; (* RFC 2518 *)
	Locked* = 423; (* RFC 2518 *)
	FailedDependency*= 424; (* RFC 2518 *)
	(*      Server Error     *)
	InternalServerError* = 500;
	NotImplemented* = 501;
	BadGateway*= 502;
	ServiceUnavailable*= 503;
	GatewayTimeout*= 504;
	VersionNotSupported* = 505;
	InsufficientStorage* = 507; (* RFC 2518 *)

	(** HTTP methods RFC 2616 Section 5.1.1*)
	UnknownM* = 0; GetM* = 1; HeadM* = 2; PutM* = 3; PostM* = 4; OptionsM* = 5;
	TraceM* = 6; DeleteM* = 7; ConnectM* = 8;

	(** new HTTP methods RFC 2518 Section 8: HTTP Extensions for Distributed Authoring -- WebDAV *)
	PropfindM* = 10; ProppatchM* = 11; MkcolM* = 12; CopyM* = 13; MoveM* = 14; LockM* = 15; UnlockM* = 16;

	(** new HTTP methods RFC 3253 Versioning Extensions to  WebDAV *)
	VersionControlM* = 17; ReportM* = 18; CheckoutM* = 19; CheckinM* = 20; UncheckoutM* = 21;
	MkworkspaceM* = 22; UpdateM* = 23; LabelM* = 24; MergeM* = 25; BaselineControlM* = 26; MkactivityM* = 27;

	(** HTTP date & time format *)
	DateTimeFormat* = "www, dd mmm yyyy hh:nn:ss GMT";

	(* Chunker stuff *)
	BufSize = 400H;
	TokenSize = 10H;

	MaxRequestHeaderFields* = 47+10; (* at most 47 standard headers of RFC 2616 plus a number of additional header fields*)

	DocType* = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';

тип
	AdditionalField* = укль на запись
		key* : массив 64 из симв8;
		value* : массив 1024 из симв8;
		next* : AdditionalField;
	кон;

	RequestHeader* = запись
		fadr* : IP.Adr;
		fport* : цел32;
		method* : цел32;
		maj*, min* : цел32;
		uri* : массив 4096 из симв8;
		host* : массив 256 из симв8;
		referer* : массив 256 из симв8;
		useragent* : массив 256 из симв8;
		accept* : массив 256 из симв8;
		transferencoding* : массив 64 из симв8;
		additionalFields* : AdditionalField;
	кон;

	ResponseHeader* = запись
		maj*, min* : цел32;
		statuscode* : цел32;
		reasonphrase* : массив 256 из симв8;
		server* : массив 256 из симв8;
		date* : массив 32 из симв8;
		location*: массив 1024 из симв8;
		contenttype* : массив 64 из симв8;
		contentlength* : размерМЗ;
		contentlocation*: массив 1024 из симв8;
		transferencoding* : массив 64 из симв8;
		lastmodified*: массив 32 из симв8;
		additionalFields* : AdditionalField;
	кон;

	ChunkedOutStream* = окласс
		перем (* General vars: *)
			outW: Потоки.Писарь;
			buf: массив BufSize из симв8;
			bufPos: цел32;
			chunked: булево;

			(* Chunked mode vars *)
			token: массив TokenSize из симв8;

		проц &Init*(перем inW: Потоки.Писарь; outW: Потоки.Писарь; перем request: RequestHeader; перем reply: ResponseHeader);
		нач
			сам.outW := outW;
			chunked := Version(request, 1,1);
			если chunked то
				Потоки.НастройПисаря(inW, Sender);
				копируйСтрокуДо0("chunked", reply.transferencoding);
				reply.contentlength := -1
			иначе
				inW := outW
			всё
		кон Init;

		проц Sender(конст inBuf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);
		перем i: размерМЗ;
		нач
			утв(chunked);
			i := ofs;
			нцПока (i < ofs+len) делай
				buf[bufPos] := inBuf[i];
				увел(i);
				увел(bufPos);
				если bufPos = BufSize то WriteChunked всё;
				если propagate то outW.ПротолкниБуферВПоток всё
			кц
		кон Sender;

		проц WriteChunked;
		нач (* inv: chunked=TRUE *)
			Строки8.ПишиЦел64_16_ричноВСтроку(bufPos, 8, token);
			outW.пСтроку8(token);
			outW.пВК_ПС;
			outW.пБайты(buf, 0, bufPos);
			outW.пВК_ПС;
			bufPos := 0
		кон WriteChunked;

		проц Update*;
		нач
			если chunked то WriteChunked всё;
			outW.ПротолкниБуферВПоток
		кон Update;

		проц Close*;
		нач
			если chunked то
				если bufPos > 0 то WriteChunked всё;
				outW.пСимв8("0");
				outW.пВК_ПС;
				outW.пВК_ПС
			всё;
			outW.ПротолкниБуферВПоток
		кон Close;
	кон ChunkedOutStream;

	ChunkedInStream* = окласс
		перем (* General vars: *)
			inR: Потоки.Чтец;
			remain: цел32;
			eof : булево;
			(* Chunked mode vars: *)
			chunkSize: цел32;
			first : булево;

		проц &Init*(перем inR, outR: Потоки.Чтец);
		нач
			сам.inR := inR;
			Потоки.НастройЧтеца(outR, Receiver);
			eof := ложь; first := истина;
		кон Init;

		проц Receiver(перем buf: массив из симв8; ofs, size, min: размерМЗ; перем len: размерМЗ; перем res: целМЗ);
		перем i: размерМЗ; token: массив 16 из симв8; ch: симв8;
		нач
			если ~eof то
				утв((size > 0) и (min <= size) и (min >= 0));
				len := 0; i := ofs; res := Потоки.Успех; chunkSize := -1;
				нцПока (chunkSize # 0) и (res = Потоки.Успех) и (len < size) делай
					(* Read the chunk size *)
					если remain = 0 то
						если ~first то inR.ПропустиДоКонцаСтрокиТекстаВключительно  всё; first := ложь;
						inR.чЦепочкуСимв8ДоБелогоПоля(token);
						inR.ПропустиДоКонцаСтрокиТекстаВключительно;
						Строки8.ПрочтиЦел32_16_ричноИзСтроки(token, chunkSize, res);
						remain := chunkSize
					всё;
					(* Fill data into out buffer *)
					нцПока (res = Потоки.Успех) и (len < size) и (remain > 0) делай
						inR.чСимв8(ch);
						res := inR.кодВозвратаПоследнейОперации;
						buf[i] := ch;
						увел(len); увел(i); умень(remain)
					кц;
				кц;
				если chunkSize = 0 то eof := истина всё
			иначе
				res := Потоки.КонецФайла
			всё
		кон Receiver;
	кон ChunkedInStream;

(* writing to stream 'inW' writes 'size' characters to 'outW' and then stops, returning Streams.EOF when attempting to write beyond stream end *)
(* implementation limination: 'remainder' bookkeeping and EOF detection occurs with Update(); but not after each Char() or Bytes(); a too large last data chunk may therefore be written only in part. when EOF is detected *)

	LimitedOutStream* = окласс
		перем outW: Потоки.Писарь;
			buf: массив BufSize из симв8;
			bufPos: цел32;
			remain-: цел32;

		проц &Init*(перем inW, outW: Потоки.Писарь; size : цел32);
		нач
			сам.outW := outW;
			remain := size;
			bufPos:=0;
			Потоки.НастройПисаря(inW, Sender);
		кон Init;

		проц Sender(конст outBuf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);
		перем i: размерМЗ;
		нач
			i := ofs;
			res:=outW.кодВозвратаПоследнейОперации ;
			нцПока (i < ofs+len) и (remain>0) делай
				buf[bufPos] := outBuf[i];
				увел(i);
				увел(bufPos);
				умень(remain);
				если (bufPos = BufSize) или (remain=0) то Write всё;
				если propagate то outW.ПротолкниБуферВПоток всё
			кц;
			если (remain=0) и (i < ofs+len) то res:= Потоки.КонецФайла всё;
		кон Sender;

		проц Write;
		нач
			outW.пБайты(buf, 0, bufPos);
			bufPos := 0
		кон Write;

		проц Update*;
		нач
			Write;
			outW.ПротолкниБуферВПоток;
		кон Update;

		проц Padding*(ch: симв8);
		перем i:цел32;
		нач
			Update; (*compute 'remain'*)
			нцПока remain>0 делай outW.пСимв8(ch); умень(remain) кц;
			outW.ПротолкниБуферВПоток;
		кон Padding;


	кон LimitedOutStream;

	LimitedInStream* = окласс
		перем inR: Потоки.Чтец;
			remain-: размерМЗ;

		проц &Init*(перем inR, outR: Потоки.Чтец; size : размерМЗ);
		нач
			сам.inR := inR;	remain := size;
			Потоки.НастройЧтеца(outR, Receiver);
		кон Init;

		проц Receiver(перем buf: массив из симв8; ofs, size, min: размерМЗ; перем len: размерМЗ; перем res: целМЗ);
		перем l: размерМЗ;
		нач
			если remain > 0 то
				утв((size > 0) и (min <= size) и (min >= 0));
				res := Потоки.Успех;
				l := size; если l > remain то l := remain всё;
				inR.чБайты(buf, ofs, l, len);
				умень(remain, len);
			иначе res := Потоки.КонецФайла
			всё
		кон Receiver;
	кон LimitedInStream;

проц EOL(перем in: Потоки.Чтец): булево;
нач
	in.ПропустиПробелыИТабуляции;
	возврат in.ВКонцеСтрокиТекстаИлиФайлаЛи¿();
кон EOL;

проц GetToken(перем in: Потоки.Чтец; перем token: массив из симв8);
нач
	in.ПропустиПробелыИТабуляции; in.чЦепочкуСимв8ДоБелогоПоля(token)
кон GetToken;

проц GetInt(перем i: цел32; конст buf: массив из симв8; перем x: цел32);
перем ch: симв8;
нач
	x := 0;
	нц
		ch := buf[i];
		если (ch < "0") или (ch > "9") то прервиЦикл всё;
		x := x * 10 + (кодСимв8(ch)-кодСимв8("0")); увел(i)
	кц
кон GetInt;

проц Match(конст  buf: массив из симв8; with: массив из симв8; перем i: цел32): булево;
перем j: цел32;
нач
	j := 0; нцПока (j<длинаМассива(with)) и (with[j] # 0X) и  (i<длинаМассива(buf)) и (buf[i] = with[j]) делай увел(i); увел(j) кц;
	возврат with[j] = 0X
кон Match;

проц EqualsI(конст buf: массив из симв8; with: массив из симв8): булево;
перем j: цел32;
нач
	j := 0; нцПока (with[j] # 0X) и (ASCII_вЗаглавную(buf[j]) = ASCII_вЗаглавную(with[j])) делай увел(j) кц;
	возврат ASCII_вЗаглавную(with[j]) = ASCII_вЗаглавную(buf[j])
кон EqualsI;

(** Currently only for additional fields *)
проц HasAdditionalField*(af : AdditionalField; fieldName: массив из симв8) : булево;
нач
	нцПока (af # НУЛЬ) и (~EqualsI(af.key, fieldName)) делай af := af.next кц;
	возврат af # НУЛЬ
кон HasAdditionalField;

(** Currently only for additional fields *)
проц GetAdditionalField*(af : AdditionalField; fieldName: массив из симв8) : AdditionalField;
нач
	нцПока (af # НУЛЬ) и (~EqualsI(af.key, fieldName)) делай af := af.next кц;
	возврат af
кон GetAdditionalField;

(** Currently only for additional fields *)
проц GetAdditionalFieldValue*(af: AdditionalField; fieldName: массив из симв8; перем value : массив из симв8) : булево;
нач
	нцПока (af # НУЛЬ) и (~EqualsI(af.key, fieldName)) делай af := af.next кц;
	если af # НУЛЬ то
		копируйСтрокуДо0(af.value, value);
		возврат истина
	иначе
		возврат ложь
	всё
кон GetAdditionalFieldValue;

(** return request property as a string *)
проц GetRequestPropertyValue*(перем header : RequestHeader; propertyName : массив из симв8; перем result : массив из симв8);
нач
	если propertyName = "#ip" то IP.AdrToStr(header.fadr, result)
	аесли propertyName = "#port" то Строки8.ПишиЦел64_вСтроку(header.fport, result)
	аесли propertyName = "#method" то
		просей header.method из
			|GetM : копируйСтрокуДо0("GET", result)
			|HeadM : копируйСтрокуДо0("HEAD", result)
			|PutM : копируйСтрокуДо0("PUT", result)
			|PostM : копируйСтрокуДо0("POST", result)
			|OptionsM : копируйСтрокуДо0("OPTIONS", result)
		иначе копируйСтрокуДо0("unknown", result)
		всё
	аесли propertyName = "host" то копируйСтрокуДо0(header.host, result)
	аесли propertyName = "referer" то копируйСтрокуДо0(header.referer, result)
	аесли propertyName = "useragent" то копируйСтрокуДо0(header.useragent, result)
	аесли propertyName = "accept" то копируйСтрокуДо0(header.accept, result)
	аесли propertyName = "transferencoding" то копируйСтрокуДо0(header.transferencoding, result)
	иначе
		если ~GetAdditionalFieldValue(header.additionalFields, propertyName, result) то копируйСтрокуДо0("", result) всё
	всё
кон GetRequestPropertyValue;

(** Currently only for additional fields *)
проц SetAdditionalFieldValue*(перем af: AdditionalField; fieldName, value: массив из симв8);
перем a: AdditionalField;
нач
	если (af = НУЛЬ) то нов(a); af := a
	иначе
		a := af; нцПока (a.next # НУЛЬ) и (a.key # fieldName) делай a := a.next кц;
		если (a.key # fieldName) то
			нов(a.next); a := a.next
		всё
	всё;
	копируйСтрокуДо0(fieldName, a.key); копируйСтрокуДо0(value, a.value)
кон SetAdditionalFieldValue;

проц GetVersion(перем ver: массив из симв8; перем maj, min: цел32):булево;
перем i: цел32;
нач
	i := 0; maj := 0; min := 0;
	если Match(ver, "HTTP/", i) то
		GetInt(i, ver, maj);
		если ver[i] = "." то увел(i) всё;
		GetInt(i, ver, min);
		возврат истина
	иначе возврат ложь
	всё
кон GetVersion;

(** Version - returns TRUE iff the HTTP version specified in h.maj/h.min is bigger or equal to Maj/Min *)
проц Version*(перем h: RequestHeader; Maj, Min: цел32): булево;
нач
	возврат (h.maj > Maj) или ((h.maj = Maj) и (h.min >= Min))
кон Version;

проц GetMethod*(перем s: массив из симв8; перем method: цел32);
нач
	если s = "GET" то method := GetM
	аесли s = "HEAD" то method := HeadM
	аесли s = "OPTIONS" то method := OptionsM
	аесли s = "POST" то method := PostM
	аесли s = "PUT" то method := PutM
	аесли s = "DELETE" то method := DeleteM
	аесли s = "TRACE" то method := TraceM
	аесли s = "CONNECT" то method := ConnectM
	(*	WebDAV	*)
	аесли s = "PROPFIND" то method := PropfindM
	аесли s = "PROPPATCH" то method := ProppatchM
	аесли s = "MKCOL" то method := MkcolM
	аесли s = "COPY" то method := CopyM
	аесли s = "MOVE" то method := MoveM
	аесли s = "LOCK" то method := LockM
	аесли s = "UNLOCK" то method := UnlockM
	(*	DeltaV	*)
	аесли s = "VERSION-CONTROL" то method := VersionControlM
	аесли s = "REPORT" то method := ReportM
	аесли s = "CHECKOUT" то method := CheckoutM
	аесли s = "CHECKIN" то method := CheckinM
	аесли s = "UNCHECKOUT" то method := UncheckoutM
	аесли s = "MKWORKSPACE" то method := MkworkspaceM
	аесли s = "UPDATE" то method := UpdateM
	аесли s = "LABEL" то method := LabelM
	аесли s = "MERGE" то method := MergeM
	аесли s = "BASELINE-CONTROL" то method := BaselineControlM
	аесли s = "MKACTIVITY" то method := MkactivityM
	иначе method := UnknownM
	всё
кон GetMethod;

проц GetMethodName*(code: цел32; перем name: массив из симв8);
нач
	просей code из
		GetM : копируйСтрокуДо0("GET", name)
		|HeadM : копируйСтрокуДо0("HEAD", name);
		|OptionsM : копируйСтрокуДо0("OPTIONS", name);
		|PostM : копируйСтрокуДо0("POST", name);
		|PutM : копируйСтрокуДо0("PUT", name);
		|DeleteM : копируйСтрокуДо0("DELETE", name);
		|TraceM : копируйСтрокуДо0("TRACE", name);
		|ConnectM : копируйСтрокуДо0("CONNECT", name);
		(*	WebDAV	*)
		|PropfindM: копируйСтрокуДо0("PROPFIND", name);
		|ProppatchM: копируйСтрокуДо0("PROPPATCH", name);
		|MkcolM: копируйСтрокуДо0("MKCOL", name);
		|CopyM: копируйСтрокуДо0("COPY", name);
		|MoveM: копируйСтрокуДо0("MOVE", name);
		|LockM: копируйСтрокуДо0("LOCK", name);
		|UnlockM: копируйСтрокуДо0("UNLOCK", name);
		(*	DeltaV	*)
		|VersionControlM: копируйСтрокуДо0("VERSION-CONTROL", name);
		|ReportM: копируйСтрокуДо0("REPORT", name);
		|CheckoutM: копируйСтрокуДо0("CHECKOUT", name);
		|CheckinM: копируйСтрокуДо0("CHECKIN", name);
		|UncheckoutM: копируйСтрокуДо0("UNCHECKOUT", name);
		|MkworkspaceM: копируйСтрокуДо0("MKWORKSPACE", name);
		|UpdateM: копируйСтрокуДо0("UPDATE", name);
		|LabelM: копируйСтрокуДо0("LABEL", name);
		|MergeM: копируйСтрокуДо0("MERGE", name);
		|BaselineControlM: копируйСтрокуДо0("BASELINE-CONTROL", name);
		|MkactivityM: копируйСтрокуДо0("MKACTIVITY", name);
		иначе копируйСтрокуДо0("UNKOWN", name)
	всё;
кон GetMethodName;

проц ParseRequest*(перем in: Потоки.Чтец; перем header: RequestHeader; перем res: целМЗ; log : TFLog.Log);
перем s: массив 32 из симв8; af: AdditionalField; ch :симв8; wellformed: булево;
нач
	header.host[0] := 0X;
	(*in.SkipWhitespace; *)(* optimization PH 2012: to avoid unnecessary work in malformed requests*)
	GetToken(in, s); GetMethod(s, header.method);
	GetToken(in, header.uri);
	GetToken(in, s);
	wellformed:=GetVersion(s, header.maj, header.min);
	header.host := ""; header.referer := ""; header.useragent := ""; header.accept := ""; header.transferencoding := "";
	header.additionalFields := НУЛЬ;
	если wellformed и EOL(in) и (header.method # UnknownM) и (header.uri # "") то
		in.ПропустиДоКонцаСтрокиТекстаВключительно();
		если header.maj >= 1 то
			ParseRequestHeaderFields(in,header,res); (* PH120209 disentangled ParseRequestHeaderFields*)
			in.ПропустиДоКонцаСтрокиТекстаВключительно;
		иначе
			если log # НУЛЬ то log.Enter; log.String("Unsupported HTTP version :"); log.Int(header.maj, 5); log.Exit всё;
			res := VersionNotSupported
		всё
	иначе
		если EOL(in) то in.ПропустиДоКонцаСтрокиТекстаВключительно(); всё; (*PH Jan 2012*)
		если log # НУЛЬ то log.Enter; log.String("Bad request :"); log.Int(header.method, 5); log.Exit всё;
		res := BadRequest
	всё
кон ParseRequest;

проц ParseRequestHeaderFields*(перем in: Потоки.Чтец; перем header: RequestHeader; перем res: целМЗ);
перем s: массив 32 из симв8; af: AdditionalField; ch :симв8; i:цел32;
нач
	i:=0;
	header.additionalFields:=НУЛЬ; (*PH 120210*)
	нцДо
		GetToken(in, s);
		Строки8.ОтрежьПолеСправа(s, ":");
		если s = "Host" то in.чСимв8(ch); in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.host)
		аесли s = "Referer" то in.чСимв8(ch); in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.referer)
		аесли s = "User-Agent" то in.чСимв8(ch); in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.useragent)
		аесли s = "Accept" то in.чСимв8(ch); in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.accept)
		аесли s = "Transfer-Encoding" то in.чСимв8(ch); in.чСтроку8ДоКонцаСтрокиТекстаВключительно( header.transferencoding)
		иначе
			нов(af); копируйСтрокуДо0(s, af.key); in.чСимв8(ch); in.чСтроку8ДоКонцаСтрокиТекстаВключительно(af.value);
			af.next := header.additionalFields; header.additionalFields := af;
			увел(i);
		всё;
		если i > MaxRequestHeaderFields-5 то res:=RequestEntityTooLarge; возврат всё; (* hardening against malignant requests*)
	кцПри (in.кодВозвратаПоследнейОперации # Потоки.Успех) или in.ВКонцеСтрокиТекстаИлиФайлаЛи¿();
	res := OK
кон ParseRequestHeaderFields;

проц ParseReply*(перем in: Потоки.Чтец; перем header: ResponseHeader; перем res: целМЗ; log : TFLog.Log);
перем s, sLow: массив 32 из симв8; af: AdditionalField;
	i :цел32; ch :симв8; wellformed: булево;
нач
	GetToken(in, s);
	wellformed:=GetVersion(s, header.maj, header.min);
	GetToken(in, s); i := 0; GetInt(i, s, header.statuscode); in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.reasonphrase);
	header.server := ""; header.date := ""; header.contenttype := "";
	header.contentlength := -1;
	header.transferencoding := ""; header.additionalFields := НУЛЬ;
	header.contentlocation := "";
	если header.maj >= 1 то
		нцДо
			GetToken(in, s);
			Строки8.ОтрежьПолеСправа(s, ":");
			(* to understand the Micros**t IIS replies *)
			Строки8.СкопируйПодстроку˛неПроверяя0(s, 0, 32, sLow);
			Строки8.СтрокуВНижнийРегистрASCII(sLow);
			если sLow = "server" то in.чСимв8(ch); in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.server)
			аесли sLow = "date" то in.чСимв8(ch);in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.date)
			аесли sLow = "location" то in.чСимв8(ch);in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.location)
			аесли sLow = "content-type" то in.чСимв8(ch); in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.contenttype)
			аесли sLow = "content-length" то in.чСимв8(ch); in.чСтроку8ДоКонцаСтрокиТекстаВключительно(s); Строки8.ПрочтиРазмерМЗ_изСтроки(s, header.contentlength)
			аесли sLow = "content-location" то in.чСимв8(ch);in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.contentlocation)
			аесли sLow = "transfer-encoding" то in.чСимв8(ch); in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.transferencoding)
			аесли sLow = "last-modified" то in.чСимв8(ch);in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.lastmodified)
			иначе
				нов(af); копируйСтрокуДо0(s, af.key); in.чСимв8(ch); in.чСтроку8ДоКонцаСтрокиТекстаВключительно(af.value);
				af.next := header.additionalFields; header.additionalFields := af
			всё;
		кцПри (in.кодВозвратаПоследнейОперации # Потоки.Успех) или in.ВКонцеСтрокиТекстаИлиФайлаЛи¿();
		in.ПропустиДоКонцаСтрокиТекстаВключительно();
		res := OK
	иначе
		если log # НУЛЬ то log.Enter; log.String("Unsupported HTTP version :"); log.Int(header.maj, 5); log.Exit всё;
		res := VersionNotSupported
	всё;
кон ParseReply;

проц ModifyReply*(перем in: Потоки.Чтец; перем header: ResponseHeader; перем res: целМЗ; log : TFLog.Log);
перем s, sLow: массив 32 из симв8; af: AdditionalField;
	i :цел32; ch :симв8;
нач
	нцДо
		GetToken(in, s);
		Строки8.ОтрежьПолеСправа(s, ":");
		(* to understand the Microsoft IIS replies *)
		Строки8.СкопируйПодстроку˛неПроверяя0(s, 0, 32, sLow);
		Строки8.СтрокуВНижнийРегистрASCII(sLow);
		если sLow = "server" то in.чСимв8(ch); in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.server)
		аесли sLow = "date" то in.чСимв8(ch);in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.date)
		аесли sLow = "location" то in.чСимв8(ch);in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.location)
		аесли sLow = "content-type" то in.чСимв8(ch); in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.contenttype)
		аесли sLow = "content-length" то in.чСимв8(ch); in.чСтроку8ДоКонцаСтрокиТекстаВключительно(s); Строки8.ПрочтиРазмерМЗ_изСтроки(s, header.contentlength)
		аесли sLow = "content-location" то in.чСимв8(ch);in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.contentlocation)
		аесли sLow = "transfer-encoding" то in.чСимв8(ch); in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.transferencoding)
		аесли sLow = "last-modified" то in.чСимв8(ch);in.чСтроку8ДоКонцаСтрокиТекстаВключительно(header.lastmodified)
		иначе
			нов(af); копируйСтрокуДо0(s, af.key); in.чСимв8(ch); in.чСтроку8ДоКонцаСтрокиТекстаВключительно(af.value); (*! to do: check if a field already exists -> replace instead of append *)
			af.next := header.additionalFields; header.additionalFields := af
		всё;
	кцПри (in.кодВозвратаПоследнейОперации # Потоки.Успех) или in.ВКонцеСтрокиТекстаИлиФайлаЛи¿();
	in.ПропустиДоКонцаСтрокиТекстаВключительно();
	res := OK
кон ModifyReply;

проц LogRequestHeader*(log : TFLog.Log; перем header : RequestHeader);
перем s : массив 32 из симв8; x: AdditionalField;
нач
	log.Enter;
	log.String("BEGIN HTTP-Request Header information ("); log.TimeStamp; log.String(")"); log.Ln;
	log.String(" HTTP request from "); IP.AdrToStr(header.fadr, s); log.String(s); log.String(" : "); log.Int(header.fport, 5); log.Ln;
	log.String("Request: ");
	GetMethodName(header.method, s); log.String(s);
	log.String(" "); log.String(header.uri); log.Ln;
	если header.host # "" то log.String("Host: "); log.String(header.host); log.Ln всё;
	если header.referer # "" то log.String("Referer: "); log.String(header.referer); log.Ln всё;
	если header.useragent # "" то log.String("User-Agent: "); log.String(header.useragent); log.Ln всё;
	если header.accept # "" то log.String("Accept: "); log.String(header.accept); log.Ln всё;
	x := header.additionalFields;
	нцПока x # НУЛЬ делай
		log.String(x.key); log.String(": "); log.String(x.value); log.Ln;
		x := x.next
	кц;
	log.String("END HTTP-Request Header information"); log.Ln; log.Ln;
	log.Exit;
кон LogRequestHeader;

проц LogResponseHeader*(log : TFLog.Log; перем header : ResponseHeader);
перем x: AdditionalField;
нач
	log.Enter;
	log.String("BEGIN HTTP-Reply Header information ("); log.TimeStamp; log.String(")"); log.Ln;
	log.String("Status Code: "); log.Int(header.statuscode, 5); log.String(" Reason: "); log.String(header.reasonphrase); log.Ln;
	если header.server # "" то log.String("Server: "); log.String(header.server); log.Ln всё;
	если header.date # "" то log.String("Date: "); log.String(header.date); log.Ln всё;
	если header.location # "" то log.String("Location: "); log.String(header.location); log.Ln всё;
	если header.contenttype # "" то log.String("Content-Type: "); log.String(header.contenttype); log.Ln всё;
	если header.contentlength # 0 то log.String("Content-Length: "); log.Int(header.contentlength, 0); log.Ln всё;
	если header.contentlocation # "" то log.String("Content-Location: "); log.String(header.contentlocation); log.Ln всё;
	если header.transferencoding # "" то log.String("Transfer-Encoding: "); log.String(header.transferencoding); log.Ln всё;
	если header.lastmodified # "" то log.String("Last-Modified: "); log.String(header.lastmodified); log.Ln всё;
	x := header.additionalFields;
	нцПока x # НУЛЬ делай
		log.String(x.key); log.String(": "); log.String(x.value); log.Ln;
		x := x.next
	кц;
	log.String("END HTTP-Reply Header information"); log.Ln; log.Ln;
	log.Exit;
кон LogResponseHeader;

проц WriteRequestLine*(s: Потоки.Писарь; maj, min : цел32; method : цел32; uri, host : массив из симв8);
перем name: массив 32 из симв8;
нач
	GetMethodName(method, name);
	если name = "UNKNOWN" то возврат иначе s.пСтроку8(name) всё;
	s.пСтроку8(" "); s.пСтроку8(uri); s.пСтроку8(" ");
	s.пСтроку8("HTTP/"); s.пЦел64(maj, 1); s.пСтроку8("."); s.пЦел64(min, 1);
	s.пВК_ПС();
	если host # "" то s.пСтроку8("Host: "); s.пСтроку8(host); s.пВК_ПС() всё
кон WriteRequestLine;


проц GetReasonPhrase*(code: цел32; перем phrase: массив из симв8);
нач
	(*	Informational	*)
	если (code =  Continue) то копируйСтрокуДо0("Continue", phrase)
	аесли (code = SwitchingProtocols) то копируйСтрокуДо0("Switching Protocols", phrase)
	аесли (code =  Processing) то копируйСтрокуДо0("Processing", phrase)
	(*	successful	*)
	аесли (code = OK) то копируйСтрокуДо0("OK", phrase);
	аесли (code = Created) то копируйСтрокуДо0("Created", phrase)
	аесли (code = Accepted) то копируйСтрокуДо0("Accepted", phrase)
	аесли (code = NonAuthoritativeInformation) то копируйСтрокуДо0("Non-Authoritative Information", phrase)
	аесли (code = NoContent) то копируйСтрокуДо0("No Content", phrase)
	аесли (code = ResetContent) то копируйСтрокуДо0("Reset Content", phrase)
	аесли (code = PartialContent) то копируйСтрокуДо0("Partial Content", phrase)
	аесли (code = MultiStatus) то копируйСтрокуДо0("Multi-Status", phrase)
	(*	Redirection	*)
	аесли (code = MultipleChoices) то копируйСтрокуДо0("Multiple Choices", phrase)
	аесли (code = ObjectMoved) то копируйСтрокуДо0("Object moved", phrase)
	аесли (code = ObjectMovedTemporarily) то копируйСтрокуДо0("Object Moved Temporarily", phrase)
	аесли (code = SeeOther) то копируйСтрокуДо0("See Other", phrase)
	аесли (code = NotModified) то копируйСтрокуДо0("Not modified", phrase)
	аесли (code = UseProxy) то копируйСтрокуДо0("Use Proxy", phrase)
	аесли (code = TemporaryRedirect) то копируйСтрокуДо0("Temporary Redirect", phrase)
	(*	Client Error	*)
	аесли (code = BadRequest) то копируйСтрокуДо0("Bad request", phrase)
	аесли (code = Unauthorized) то копируйСтрокуДо0("Unauthorized", phrase)
	аесли (code = PaymentRequired) то копируйСтрокуДо0("Payment Required", phrase)
	аесли (code = Forbidden) то копируйСтрокуДо0("Forbidden", phrase)
	аесли (code = NotFound) то копируйСтрокуДо0("Not found", phrase)
	аесли (code = MethodNotAllowed) то копируйСтрокуДо0("Method Not Allowed", phrase)
	аесли (code = NotAcceptable) то копируйСтрокуДо0("Not Acceptable", phrase)
	аесли (code = ProxyAuthenticationRequested) то копируйСтрокуДо0("Proxy Authentication Requested", phrase)
	аесли (code = RequestTimeout) то копируйСтрокуДо0("Request Timeout", phrase)
	аесли (code = Conflict) то копируйСтрокуДо0("Conflict", phrase)
	аесли (code = Gone) то копируйСтрокуДо0("Gone", phrase)
	аесли (code = LengthRequired) то копируйСтрокуДо0("Length required", phrase)
	аесли (code = PreconditionFailed) то копируйСтрокуДо0("Precondition failed", phrase)
	аесли (code = RequestEntityTooLarge) то копируйСтрокуДо0("Request Entity Too Large", phrase)
	аесли (code = RequestURITooLong) то копируйСтрокуДо0("Request URI too long", phrase)
	аесли (code = UnsupportedMediaType) то копируйСтрокуДо0("Unsupported Media Type", phrase)
	аесли (code = RequestedRangeNotSatisfiable) то копируйСтрокуДо0("Requested Range Not Satisfiable", phrase)
	аесли (code = ExpectationFailed) то копируйСтрокуДо0("Expectation Failed", phrase)
	аесли (code = UnprocessableEntity) то копируйСтрокуДо0("Unprocessable Entity", phrase)
	аесли (code = Locked) то копируйСтрокуДо0("Locked", phrase)
	аесли (code = FailedDependency) то копируйСтрокуДо0("Failed Dependency", phrase)
	(*	Server Error	*)
	аесли (code = InternalServerError) то копируйСтрокуДо0("Internal server error", phrase)
	аесли (code = NotImplemented) то копируйСтрокуДо0("Operation not implemented", phrase)
	аесли (code = BadGateway) то копируйСтрокуДо0("Bad Gateway", phrase)
	аесли (code = ServiceUnavailable) то копируйСтрокуДо0("Service Unavailable", phrase)
	аесли (code = GatewayTimeout) то копируйСтрокуДо0("Gateway Timeout", phrase)
	аесли (code = VersionNotSupported) то копируйСтрокуДо0("HTTP Version not supported", phrase)
	аесли (code = InsufficientStorage) то копируйСтрокуДо0("Insufficient Storage", phrase)
	иначе копируйСтрокуДо0("Unknown Status Code", phrase) (* Was "HTTP server error" *)
	всё;
кон GetReasonPhrase;

проц WriteStatus*(перем h: ResponseHeader; перем dst: Потоки.Писарь);
нач
	dst.пСтроку8("HTTP/"); dst.пЦел64(h.maj, 1); dst.пСтроку8("."); dst.пЦел64(h.min, 1);
	dst.пСтроку8(" ");dst.пЦел64(h.statuscode, 1); dst.пСтроку8(" ");
	GetReasonPhrase(h.statuscode, h.reasonphrase);
	dst.пСтроку8(h.reasonphrase); dst.пВК_ПС();
	dst.пСтроку8("Server: "); dst.пСтроку8(h.server); dst.пВК_ПС()
кон WriteStatus;

(* precondition: header statuscode and header reasonphrase are already filled in, e.g. by use of WriteStatus() *)
проц WriteHTMLStatus*(перем h: ResponseHeader; dst: Потоки.Писарь);
перем reasonphrase: массив 64 из симв8;
нач
	dst.пСтроку8(DocType); dst.пВК_ПС;
	dst.пСтроку8("<html><head><title>"); dst.пЦел64(h.statuscode,0); dst.пСтроку8(" - "); dst.пСтроку8(h.reasonphrase); dst.пСтроку8("</title></head>");
	dst.пСтроку8("<body>HTTP "); dst.пЦел64(h.statuscode,0); dst.пСтроку8(" - "); dst.пСтроку8(h.reasonphrase); dst.пСтроку8("<hr><address>");
	dst.пСтроку8(h.server); dst.пСтроку8( "</address></body></html>"); dst.пВК_ПС;
кон WriteHTMLStatus;

проц SendResponseHeader*(перем h: ResponseHeader; перем dst: Потоки.Писарь);
перем s: массив 32 из симв8; af: AdditionalField;
нач
	WriteStatus(h, dst);
	Строки8.ПишиДатуВремяВСтроку("www, dd mmm yyyy, hh:nn:ss GMT", Dates.Now(), s);
	dst.пСтроку8("Date: "); dst.пСтроку8(s); dst.пВК_ПС();
	если (h.statuscode # NotModified) то
		если (h.location # "") то
			dst.пСтроку8("Location: "); dst.пСтроку8(h.location); dst.пВК_ПС()
		всё;
		dst.пСтроку8("Content-Type: "); dst.пСтроку8(h.contenttype); dst.пВК_ПС();
		если (h.contentlength >= 0) то
			dst.пСтроку8("Content-Length: "); dst.пЦел64( h.contentlength, 1); dst.пВК_ПС()
		всё;
		если (h.contentlocation # "") то
			dst.пСтроку8("Content-Location: "); dst.пСтроку8(h.contentlocation); dst.пВК_ПС()
		всё;
		если (h.transferencoding # "") то
			dst.пСтроку8("Transfer-Encoding: "); dst.пСтроку8(h.transferencoding); dst.пВК_ПС()
		всё;
		если (h.lastmodified # "") то
			dst.пСтроку8("Last-Modified: ");dst.пСтроку8(h.lastmodified); dst.пВК_ПС()
		всё;
		af := h.additionalFields;
		нцПока (af # НУЛЬ) делай
			dst.пСтроку8(af.key); dst.пСтроку8(": "); dst.пСтроку8(af.value); dst.пВК_ПС();
			af := af.next
		кц
	всё;
	dst.пВК_ПС()
кон SendResponseHeader;

проц SendStatusReply*(code:цел32; перем request: RequestHeader; перем reply: ResponseHeader; перем out: Потоки.Писарь);
перем w : Потоки.Писарь;
	chunker: ChunkedOutStream;
нач
	reply.statuscode := code;
	GetReasonPhrase(code, reply.reasonphrase);
	reply.contenttype := "text/html; charset=UTF-8";
	нов(chunker, w, out, request, reply);
	SendResponseHeader(reply, out);
	WriteHTMLStatus(reply, w);
	w.ПротолкниБуферВПоток;
	chunker.Close
кон SendStatusReply;

проц GetPath*(перем url, path : массив из симв8);
перем i, j : цел32;
	protocol : массив 8 из симв8;
нач
	если Строки8.КвоБайтБезЗавершающего0(url) < 7 то копируйСтрокуДо0(url, path)
	иначе
		Строки8.СкопируйПодстроку˛неПроверяя0(url, 0, 7, protocol); Строки8.СтрокуВВерхнийРегистрASCII(protocol);
		i := 0;
		если protocol = "HTTP://" то i := 7
		аесли protocol = "HTTPS:/" то i := 8
		всё;
		если i > 0 то
			нцПока  (url[i] # "/") и (url[i] # 0X) делай увел(i) кц;
			если url[i] # 0X то j := 0; нцДо path[j] := url[i]; увел(i); увел(j) кцПри url[i] = 0X
			иначе path := "/"
			всё
		иначе копируйСтрокуДо0(url, path)
		всё
	всё
кон GetPath;

проц SplitHTTPAdr*(url : массив из симв8; перем host, path: массив из симв8; перем port: цел32): булево;
перем i, j : цел32;
нач
	(*assuming HTTP or HTTPS*)

	если (длинаМассива(url)>7) и  (url[4] = ":") и (url[5] = "/") и (url[6] = "/") то i:=7; port:=HTTPPort;
	аесли (длинаМассива(url)>8) и (url[5] = ":") и (url[6] = "/") и (url[7] = "/") то i:=8; port:=HTTPSPort;
	иначе возврат ложь
	всё;

	(* get host *)
	j := 0;
	нцПока (url[i] # ":") и (url[i] # "/") и (url[i] # 0X) делай
		если j < длинаМассива(host) - 1 то host[j] := url[i] иначе возврат ложь всё;
		 увел(i); увел(j);
		 если i = длинаМассива(url) то возврат ложь всё
	кц;
	host[j] := 0X;
	(* get port *)
	если url[i] = ":" то
		port := 0;
		увел(i);
		нцПока (i < длинаМассива(url)) и (кодСимв8(url[i]) >= кодСимв8("0")) и (кодСимв8(url[i]) <= кодСимв8("9"))  делай
			port := port * 10 + (кодСимв8(url[i]) - кодСимв8("0"));
			увел(i)
		кц
	всё;
	j := 0;
	нцПока (i < длинаМассива(url)) и (url[i] # 0X) делай
		если j < длинаМассива(host) - 1 то path[j] := url[i] иначе возврат ложь всё;
		 увел(i); увел(j);
		 если i = длинаМассива(url) то возврат ложь всё
	кц;
	path[j] := 0X;
	возврат истина
кон SplitHTTPAdr;

кон WebHTTP.



System.FreeDownTo WebHTTP~

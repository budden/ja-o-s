модуль Строки8;
(**
	This is a small library of common string manipulation command such as find, compare,
	append, etc.
	
	001 2006-06-16 tt: Added copy, append
	002 2006-06-14 fof : Equals -> Equal
	003 2006-06-29 tt: changed header format
	004 2006-11-16 tt: Added EqualIgnoreCase, changed Equal
	005 2006-12-07 fof: added terminator for non-null terminated strings in Copy	
	006 2006-12-08 fof: added IntToString, RealToString Append* methods
	007 2007-02-07 tt: Added Length
	008 2007-02-08 tt: Added AppendSet
	009 2007-07-03 tt: Formatted and updated documentation
*)
	использует НИЗКОУР, Utils, Трассировка;

	конст 
		(* The Ascii value of char "0" *)
		ToLowerCaseDiff = 30H; 
		(* MAX(SIGNED32)*)
		(*MAXLONGINT = 7FFFFFFFH; *)
		(* MIN(SIGNED32)*)
		(*MINLONGINT = 80000000H; *)
	
	(* Get the length of a string including terminating 0X *)
	проц КвоБайтБезЗавершающего0*(конст s: массив из симв8): цел32; 
		перем length: цел32; 
	нач 
		length := 0; 
		нцДо увел(length)
		кцПри (length >= длинаМассива(s)) или (s[length - 1] = 0X); 
		возврат length
	кон КвоБайтБезЗавершающего0; 

	(* Return the capital letter of character "ch" *)
	(*
	PROCEDURE CAP*(ch: CHAR): CHAR; 
	BEGIN 
		IF (ch >= 'a') & (ch <= 'z') THEN 
			ch := CHR(ORD(ch) - 32);  (* Convert small letter to capital letter *)
		END; 
		RETURN ch
	END CAP; 
	*)
	
	(* Return the minumum vaule of two given integers *)
	проц Min(a, b: цел32): цел32; 
	нач 
		если b < a то a := b всё; 
		возврат a
	кон Min; 

	(* Compares two strings.
		0: The two Strings are equal
		<0: The first unequal character in the first string of the two strings is smaller (ascii value)
		>0: The first unequal character in the first string of the two strings is larger (ascii value) *)
	проц Compare*(конст s1, s2: массив из симв8): цел32; 
		перем i, len: цел32; 
	нач 
		i := 0; 
		len := Min(длинаМассива(s1) - 1, длинаМассива(s2) - 1);
		нцПока (i < len) и (s1[i] = s2[i]) и (s1[i] # 0X) и (s2[i] # 0X) делай увел(i) кц; 
		возврат кодСимв8(s1[i]) - кодСимв8(s2[i])
	кон Compare; 

	(* Returns TRUE if s1 and s2 are equal. The case of all characters is ignored *)
	проц EqualIgnoreCase*(конст s1, s2: массив из симв8): булево; 
		перем i, len: цел32; 
	нач 
		i := 0; len := Min(длинаМассива(s1) - 1, длинаМассива(s2) - 1); 
		нцПока (i < len) и (ASCII_вЗаглавную(s1[i]) = ASCII_вЗаглавную(s2[i])) и (s1[i] # 0X) и (s2[i] # 0X) делай увел(i); кц; 
		возврат ASCII_вЗаглавную(s1[i]) = ASCII_вЗаглавную(s2[i])
	кон EqualIgnoreCase; 

	(* Convert a string (ascii reoresentation of a number) to an integer *)
	проц ПрочтиЦел32_изСтроки*(конст Стр: массив из симв8; перем Рез: цел32; res: булево); 
		перем i, d: цел32; neg: булево; ch: симв8; 
	нач 
		res := истина;
		i := 0; ch := Стр[0]; 
		нцПока (ch # 0X) и (ch <= ' ') делай увел(i); ch := Стр[i] кц; 
		neg := ложь; 
		если ch = '+' то увел(i); ch := Стр[i] всё; 
		если ch = '-' то neg := истина; увел(i); ch := Стр[i] всё; 
		нцПока (ch # 0X) и (ch <= ' ') делай увел(i); ch := Стр[i] кц; 
		Рез := 0; 
		нцПока (ch >= '0') и (ch <= '9') делай 
			d := кодСимв8(ch) - кодСимв8('0'); увел(i); ch := Стр[i]; 
			если Рез <= ((матМаксимум(цел32) - d) DIV 10) то 
				Рез := 10 * Рез + d
			аесли neg и (Рез = 214748364) и (d = 8) и ((ch < '0') или (ch > '9')) то 
				Рез := матМинимум(цел32); neg := ложь 
			иначе 
				(* Invalid number found -> set res to FALSE and abort loop *)
				res := ложь;
				ch := 0X;
			всё 
		кц; 
		если neg то Рез := -Рез всё 
	кон ПрочтиЦел32_изСтроки; 

	(* Finds the first occurrence of character ch in string s starting at pos start in s and returns the
		index. Returns -1 if ch cannot be found *)
	проц НайдиСимв8*(Искомое: симв8; конст s: массив из симв8; start: цел32): цел32; 
		перем found, i: цел32; 
	нач 
		found := -1; 
		нцПока (start < длинаМассива(s)) и (s[start] # 0X) и (s[start] # Искомое) делай увел(start); кц; 
		если (start < длинаМассива(s)) и (s[start] = Искомое) то found := start; всё; 
		возврат found
	кон НайдиСимв8; 

	(* Find in s the string stored in pat and start searching in s at location start.
		Returns -1 if not found, otherwise the index of the first found character in s *)
	проц FindString*(конст pat, s: массив из симв8; start: цел32): цел32; 
		перем found, i, patLen, sLen: цел32;
	нач 
		found := -1; patLen := длинаМассива(pat); sLen := длинаМассива(s); 
		нцПока (start < sLen) и (s[start] # 0X) и (found = -1) делай 
			i := 0; 
			нцПока (i < patLen) и (pat[i] = s[start + i]) и (pat[i] # 0X) и (s[i] # 0X) делай 
				увел(i)
			кц; 
			если i = patLen то found := start всё; 
			увел(start)
		кц;
		возврат found
	кон FindString; 

	(* Copy the whole string (0X terminated) from source to derst
		In contrast to the assignment dest := source, only the 0X terminated
		part of source is copied to dest. *)
	проц СкопируйПодстроку˛неПроверяя0*(конст source: массив из симв8; перем dest: массив из симв8); 
		перем i: цел32; 
	нач 
		i := 0; 
		нцДо dest[i] := source[i]; увел(i) 
		кцПри (dest[i - 1] = 0X) или (длинаМассива(source) = i) или (длинаМассива(dest) = i); 
		если i < длинаМассива(dest) то dest[i] := 0X иначе dest[i - 1] := 0X всё;  (*@4 fof: if source was not 0X terminated *)
	кон СкопируйПодстроку˛неПроверяя0; 

	(* Convert integer val to a 0X terminated string and store it in str. *)
	проц ПишиЦел64_вСтроку*(val: цел32; перем str: массив из симв8); 
	перем i, j: цел32; digits: массив 16 из цел32; 
	нач 
		если val = матМинимум(цел32) то 
			str := "-2147483648"; 
		иначе 
			i := 0; 
			если val < 0 то 
				val := -val; str[0] := '-'; j := 1 
			иначе 
				j := 0 
			всё; 

			нцДо 
				digits[i] := val остОтДеленияНа 10; 
				увел(i); 
				val := val DIV 10 
			кцПри val = 0; 

			умень(i);
			
			нцПока i >= 0 делай
				str[j] := симв8ИзКода(digits[i] + кодСимв8('0'));
				увел(j);
				умень(i)
			кц; 
			str[j] := 0X; 
		всё; 
	кон ПишиЦел64_вСтроку; 

	(* Convert boolean value into a string *)
	проц БулевоВСтрокуАнгл*(конст bool: булево; перем str: массив из симв8);  
	нач 
		если bool то
			str := "True";
		иначе
			str := "False";
		всё;
	кон БулевоВСтрокуАнгл; 

	(* Convert a string into a boolean *)
	проц БулевоИзСтрокиПоˉанглийски*(конст str: массив из симв8; перем bool: булево; перем res: булево); 
	нач 
		res := истина;
		если EqualIgnoreCase(str, "true") то
			bool := истина;
		аесли EqualIgnoreCase(str, "false") то
			bool := ложь;
		иначе
			res := ложь;
		всё;
	кон БулевоИзСтрокиПоˉанглийски;

	(* Returns the shifted binary exponent of a real (0 <= e < 256 *)
	проц Expo*(x: вещ32): цел32; 
		перем e: цел32;
	нач 
		(* Replaced the following code with safe variant *)
		(* RETURN ASR(SYSTEM.VAL(SIGNED32, x), 23) MOD 256 *)
		Utils.UNPK(x, e);
		возврат (e + 127) остОтДеленияНа 256
	кон Expo; 

	(* Returns 10^e (e <= 308, 308 < e delivers IEEE-code +INF). *)
	проц Ten(e: цел32): вещ32; 
		перем res: вещ32; 
	нач 
		(* hack! *)
		если e < -38 то 
			res := 0.0; 
		аесли e > 38 то 
			res := матМаксимум(вещ32); 
		иначе 
			res := 1.0; 
			нцПока (e > 0) делай res := res * 10.0; умень(e); кц; 
			нцПока (e < 0) делай res := res / 10.0; увел(e); кц; 
		всё; 
		возврат res
	кон Ten; 

	(* Returns the NaN code (0 <= c < 8399608) or -1 if not NaN/Infinite. *)
	проц NaNCode(x: вещ32): цел32; 
		перем l: цел32; 
	нач 
 		если Expo(x) = 255 то (* Infinite or NaN *)
			l := НИЗКОУР.подмениТипЗначения(цел32, x) остОтДеленияНа 800000H;
		иначе 
			l := -1; 
		всё; 
		возврат l
	кон NaNCode;

	(** truncates string to length *)
	проц СократиСтрокуДоДлины* (перем Стр: массив из симв8; Длина˛байт˛неСчитаяЗавершающего0: цел32);
	нач
		если длинаМассива(Стр) > Длина˛байт˛неСчитаяЗавершающего0 то Стр[Длина˛байт˛неСчитаяЗавершающего0] := 0X всё;
	кон СократиСтрокуДоДлины;

	(** copies src[soff ... soff + len - 1] to dst[doff ... doff + len - 1] *)
	проц СкопируйБайтыСоСдвигом* (конст Источник: массив из симв8; СмещениеВИсточнике˛байт, СкопироватьБайт: цел32; перем Приёмник: массив из симв8; СмещениеВПриёмнике˛байт: цел32);
	нач
		(* reverse copy direction in case src and dst denote the same string *)
		если СмещениеВИсточнике˛байт < СмещениеВПриёмнике˛байт то
			увел (СмещениеВИсточнике˛байт, СкопироватьБайт - 1); увел (СмещениеВПриёмнике˛байт, СкопироватьБайт - 1);
			нцПока СкопироватьБайт > 0 делай Приёмник[СмещениеВПриёмнике˛байт] := Источник[СмещениеВИсточнике˛байт]; умень (СмещениеВИсточнике˛байт); умень (СмещениеВПриёмнике˛байт); умень (СкопироватьБайт) кц
		иначе
			нцПока СкопироватьБайт > 0 делай Приёмник[СмещениеВПриёмнике˛байт] := Источник[СмещениеВИсточнике˛байт]; увел (СмещениеВИсточнике˛байт); увел (СмещениеВПриёмнике˛байт); умень (СкопироватьБайт) кц
		всё;
	кон СкопируйБайтыСоСдвигом;

	(** concatenates s1 and s2: s := s1 || s2 *)
	проц Склей* (конст s1, s2: массив из симв8; перем рез: массив из симв8);
	перем len1, len2 : цел32;
	нач
		len1 := КвоБайтБезЗавершающего0 (s1); len2 := КвоБайтБезЗавершающего0 (s2);
		СкопируйБайтыСоСдвигом(s2, 0, len2, рез, len1);
		СкопируйБайтыСоСдвигом (s1, 0, len1, рез, 0);
		СократиСтрокуДоДлины (рез, len1 + len2);
	кон Склей;

	(** concatenates s1 and s2: s := s1 || s2. The resulting string is truncated to the length of s if necessary *)
	проц Склей˛отбрасываяИзлишек*(конст s1, s2 : массив из симв8; перем s : массив из симв8);
	перем len1, len2 : цел32;
	нач
		len1 := КвоБайтБезЗавершающего0 (s1); len2 := КвоБайтБезЗавершающего0 (s2);
		если (len1 + 1 >= длинаМассива(s)) то
			копируйСтрокуДо0(s1, s);
		иначе
			если (len1 + len2 + 1 > длинаМассива(s)) то
				len2 := длинаМассива(s) - 1 - len1;
			всё;
			СкопируйБайтыСоСдвигом(s2, 0, len2, s, len1);
			СкопируйБайтыСоСдвигом (s1, 0, len1, s, 0);
			СократиСтрокуДоДлины (s, len1 + len2);
		всё;
	кон Склей˛отбрасываяИзлишек;

	(* Append "this" to "to". Copies as much as is possible to "to" (0X terminated) *)
	проц ПодклейВСтрокуХвост*(перем to: массив из симв8; конст this: массив из симв8); 
		перем i, j: цел32; 
	нач 
		i := 0; j := 0; 

		нцПока (i < длинаМассива(to)) и (to[i] # 0X) делай 
			увел(i) 
		кц; 

		нцПока (i < длинаМассива(to)) и (j < длинаМассива(this)) и (this[j] # 0X) делай
			 to[i] := this[j]; 
			 увел(i); увел(j)
		кц; 
		
		(* 0X terminate the string *)
		если j > 0 то (* tt: Appending of the empty string must not result in a trap *)
			если (this[j - 1] = 0X) или (i = длинаМассива(to)) то 
				to[i - 1] := 0X 
			иначе 
				to[i] := 0X 
			всё;  (*@4 fof: if source was not 0X terminated *)
		всё; 
	кон ПодклейВСтрокуХвост; 

	(* Append a character at the end of a string *)
	проц ПодклейВСтрокуСимв8*(перем to: массив из симв8; c: симв8); 
		перем str: массив 4 из симв8; 
	нач 
		str[0] := c; str[1] := 0X; ПодклейВСтрокуХвост(to, str); 
	кон ПодклейВСтрокуСимв8; 

	(** Write real x to buffer str as ascii text *)
	проц AppendReal*(перем str: массив из симв8; x: вещ32); 
		перем 
			e, h, i, n: цел32; 
			y, z, temp05: вещ32;
			d: массив 8 из симв8;  
	нач 
		n := 14;  (* larger number of n do not really make sense *)
		e := Expo(x); 
		если e = 255 то 
			нцПока n > 8 делай ПодклейВСтрокуСимв8(str, ' '); умень(n) кц; 
			h := NaNCode(x);
			если h # 0 то ПодклейВСтрокуХвост(str, "   NaN")
			аесли x < 0.0 то ПодклейВСтрокуХвост(str, "  -INF")
			иначе ПодклейВСтрокуХвост(str, "   INF")
			всё 
		иначе 
			если n <= 7 то n := 0 иначе умень(n, 7) всё; 
			нцПока (n > 7) делай ПодклейВСтрокуСимв8(str, ' '); умень(n) кц;  (* 0 <= n <= 7 fraction digits *)
			если (e # 0) и (x < 0.0) то ПодклейВСтрокуСимв8(str, '-'); x := -x 
			иначе ПодклейВСтрокуСимв8(str, ' ')
			всё; 
			если e = 0 то 
				h := 0 (* no denormals *)
			иначе 
				e := (e - 127) * 301 DIV 1000;  (* ln(2)/ln(10) = 0.301029996 *)
				если e < 38 то 
					z := Ten(e + 1); 
					если x >= z то y := x / z; увел(e) иначе y := x * Ten(-e) всё 
				иначе y := x * Ten(-38)
				всё; 
				если y >= 10.0 то y := y * Ten(-1); y := y + 0.5E0 / Ten(n); увел(e) 
				иначе 
					temp05 := 0.5E0; (* Otherwise not compilable *)
					y := y + temp05 / Ten(n); 
					если y >= 10.0 то y := y * Ten(-1); увел(e) всё 
				всё; 
				y := y * Ten(7); h := округлиВниз(y)
			всё; 
			i := 7; 
			нцПока i >= 0 делай d[i] := симв8ИзКода(h остОтДеленияНа 10 + кодСимв8('0')); h := h DIV 10; умень(i) кц; 
			ПодклейВСтрокуСимв8(str, d[0]); ПодклейВСтрокуСимв8(str, '.'); i := 1; 
			нцПока i <= n делай ПодклейВСтрокуСимв8(str, d[i]); увел(i) кц; 
			если e < 0 то ПодклейВСтрокуХвост(str, "E-"); e := -e иначе ПодклейВСтрокуХвост(str, "E+") всё; 
			i := e DIV 10; ПодклейВСтрокуСимв8(str, симв8ИзКода(i + кодСимв8('0'))); i := e остОтДеленияНа 10; 
			ПодклейВСтрокуСимв8(str, симв8ИзКода(i + кодСимв8('0')))
		всё
	кон AppendReal; 

	(* Append an integer to a string *)
	проц ПодклейВСтрокуРаспечаткуЦел64*(перем to: массив из симв8; i: цел32); 
		перем str: массив 64 из симв8; 
	нач 
		ПишиЦел64_вСтроку(i, str); ПодклейВСтрокуХвост(to, str); 
	кон ПодклейВСтрокуРаспечаткуЦел64; 

	(* Append a set to a string *)
	проц AppendSet*(перем to: массив из симв8; s: мнвоНаБитахМЗ); 
		перем first: булево; i: цел32; 
	нач 
		first := истина; ПодклейВСтрокуСимв8(to, '{'); 
		нцДля i := 0 до 31 делай 
			если i в s то 
				если ~first то ПодклейВСтрокуСимв8(to, ',') всё; 
				first := ложь; ПодклейВСтрокуРаспечаткуЦел64(to, i); 
			всё 
		кц; 
		ПодклейВСтрокуСимв8(to, '}'); 
	кон AppendSet; 

	(* Append a boolean to a string *)
	проц AppendBool*(перем to: массив из симв8; b: булево); 
	нач 
		если b то ПодклейВСтрокуХвост(to, "TRUE") иначе ПодклейВСтрокуХвост(to, "FALSE") всё; 
	кон AppendBool; 

	(* Convert a real to a string *)
	проц RealToStr*(r: вещ32; перем str: массив из симв8); 
	нач 
		str[0] := 0X; AppendReal(str, r); 
	кон RealToStr; 

нач 
	Трассировка.StringLn("Strings.");
кон Строки8.

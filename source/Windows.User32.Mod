(* Copyright (c) 1994 - 2000 Emil J. Zeller *)

модуль User32; (** non-portable / source: Windows.User32.Mod *)	(* ejz  *)
	использует НИЗКОУР, Kernel32;

	(** This module defines all the Win32 User32 APIs used by Oberon. *)

	тип
		DWORD = Kernel32.DWORD;
		Long = длинноеЦелМЗ;
	конст
		(** window messages *)
		WMCreate* = 01H; WMDestroy* = 02H; WMMove* = 03H; WMSize* = 05H; WMActivate* = 06H; WMSetFocus* = 07H;
		WMKillFocus* = 08H; WMPaint* = 0FH; WMClose* = 010H; WMQuit* = 012H; WMSetCursor* = 020H;
		WMMouseActivate* = 021H; WMGetMinMaxInfo* = 024H; WMWindowPosChanging* = 046H;
		WMGetIcon* = 07FH; WMSetIcon* = 080H; WMNCHitTest* = 084H;
		WMKeyFirst* = 0100H;
		WMKeyDown* = 0100H; WMKeyUp* = 0101H; WMChar* = 0102H; WMDeadChar* = 0103H;
		WMSysKeyDown* = 0104H; WMSysKeyUp* = 0105H; WMSysChar* = 0106H; WMSysDeadChar* = 107H;
		WMKeyLast* = 0108H;
		WMCommand* = 0111H;
		WMMouseFirst* = 0200H; WMMouseMove* = 0200H; WMMouseLast* = 020DH;
		WMLButtonDown* = 0201H; WMLButtonUp* = 0202H;
		WMRButtonDown* = 0204H; WMRButtonUp* = 0205H;
		WMMButtonDown* = 0207H; WMMButtonUp* = 0208H;
		WMMouseWheel* = 020AH;
		WMXButtonDown* = 020BH; WMXButtonUp* = 020CH;

		WMDropFiles* = 0233H;
		WMCut* = 0300H; WMCopy* = 0301H; WMPaste* = 0302;
		WMClear* = 0303H; WMUndo* = 0304H;
		WMUser* = 0400H;

		(** WndClass style values *)
		CSVRedraw* = 0; CSHRedraw* = 1; CSOwnDC* = 5; CSSaveBits* = 11; CSGlobalClass* = 14;

		(** CreateWindow dwStyle values *)
		WSMaximizeBox* = 16; WSMinimizeBox* = 17; WSThickFrame* = 18; WSSysMenu* = 19; WSBorder* = 23;
		WSMaximize* = 24; WSVisible* = 28; WSMinimize* = 29; WSChild* = 30;

		(** CreateWindow default value for x, y, nWidth, nHeight *)
		CWUseDefault* = цел32(080000000H);

		(** WMSize wParam values *)
		SizeRestored* = 0; SizeMinimized* = 1; SizeMaximized* = 2; SizeMaxshow* = 3; SizeMaxhide* = 4;

		(** WMActivate LoWord(wParam) values *)
		WAInactive* = 0; WAActive* = 1; WAClickActive* = 2;

		(** WMmouseActivate return codes *)
		MAActivate* = 1; MANoActivate* = 3;

		(** GetSystemMetrics nIndex values *)
		SMCXScreen* = 0; SMCYScreen* = 1; SMCYCaption* = 4;
		SMCXDlgFrame* = 7; SMCYDlgFrame* = 8;
		SMCXFixedFrame* = SMCXDlgFrame; SMCYFixedFrame* = SMCYDlgFrame;
		SMCYMenu* = 15; SMCXFrame* = 32; SMCYFrame* = 33;
		SMCMouseButtons* = 43;

		(** Predefined HWND values *)
		HWNDDesktop* = 0; HWNDBroadcast* = 0FFFFH;

		(** virtual key codes *)
		VKCancel* = 03H; VKBack* = 08H; VKTab * = 09H; VKClear* = 0CH; VKReturn* = 0DH;
		VKShift* = 010H; VKControl* = 011H; VKMenu* = 012H;
		VKPause* = 013H; VKCapital* = 014H; VKEscape* = 01BH;
		VKPrior* = 021H; VKNext* = 022H; VKEnd* = 023H; VKHome* = 024H;
		VKLeft* = 025H; VKUp* = 026H; VKRight* = 027H; VKDown* = 028H;
		VKSelect* = 029H; VKPrint* = 02AH; VKExecute* = 02BH; VKSnapshot* = 02CH;
		VKInsert* = 02DH; VKDelete* = 02EH; VKHelp* = 02FH;
		VKLWin* = 05BH; VKRWin* = 05CH; VKApps* = 05DH;
		VKF1* = 070H; VKF2* = 071H; VKF3* = 072H; VKF4* = 073H;
		VKF5* = 074H; VKF6* = 075H; VKF7* = 076H; VKF8* = 077H;
		VKF9* = 078H; VKF10* = 079H; VKF11* = 07AH; VKF12* = 07BH;

		(** WMMouse... wParam values *)
		MKLButton* = 0; MKRButton* = 1; MKShift* = 2; MKControl* = 3; MKMButton* = 4;

		(** predefined cursors/icons fo LoadCursor/LoadIcon *)
		IDCArrow* = 32512; IDCWait* = 32514;

		(** WMNCHitTest return values *)
		HTClient* = 1;

		(** ShowWindow nCmdShow values *)
		SWHide* = 0; SWShow* = 5;
		SWNormal* = 1; SWShowNormal* = 1;
		SWMinimize* = 6; SWShowMinimized* = 2;
		SWMaximize* = 3; SWShowMaximized* = 3;
		SWRestore* = 9;
		SWShowDefault* = 10;

		(** SetWindowPos uFlags values *)
		SWPNoSize* = 0; SWPNoMove* = 1; SWPNoZOrder* = 2; SWPNoRedraw* = 3; SWPNoActivate* = 4;
		SWPFrameChanged* = 5; SWPShowWindow* = 6; SWPHideWindow* = 7;

		(** clipboard formats *)
		CFText* = 1; CFBitmap* = 2; CFMetafilePict* = 3; CFDIB* = 8; CFUnicodeText* = 13; CFEnhMetafile* = 14; CFHDrop* = 15;

		(** InsertMenu uFlags values *)
		MFByCommand* = {}; MFByPosition* = 10;
		MFPopup* = 4; MFSeparator* = 11;
		MFEnabled* = {}; MFGrayed * = 0; MFDisabled* = 1;
		MFUnchecked* = {}; MFChecked* = 3;
		MFString* = {};

		(** MenuItemInfo fMask values *)
		MIIMID* = 1; MIIMType* = 4;

		(** WMSetIcon wPaam values *)
		IconSmall* = 0; IconBig* = 1;

		(** GetWindowLong/SetWindowLong nIndex values *)
		GWLWndProc* = -4; GWLStyle* = - 16; GWLExStyle* = -20;

		(** MessageBox uType values *)
		MBOk* = {}; MBOkCancel* = {0}; MBAbortRetryIgnore* = {1};
		MBYesNoCancel* = {0, 1}; MBYesNo* = {2}; MBRetryCancel* = {0, 2};
		MBIconHand* = {4}; MBIconQuestion* = {5}; MBIconExclamation* = {4, 5}; MBIconAsterisk* = {6};
		MBIconWarning* = MBIconExclamation; MBIconError* = MBIconHand;
		MBIconInformation* = MBIconAsterisk; MBIconStop* = MBIconHand;

		(** TrackPopupMenu uFlags values *)
		TPMLeftButton* = {}; TPMRightButton* = 1;
		TPMLeftAlign* = {}; TPMCenterAlign* = 2; TPMRightAlign* = 3;
		TPMTopAlign* = {}; TPMVCenterAlign* = 4; TPMBottomAlign* = 5;
		TPMHorizontal* = {}; TPMVertical* = 6;
		TPMNoNotify* = 7; TPMReturnCmd* = 8;

		(** SetWindowsHookEx idHook values *)
		WHKeyboard* = 2;

		(** GetSysColor *)
		ColorWindow* = 5;
		ColorWindowText* = 8;
		ColorBtnFace* = 15;
		ColorBtnShadow* = 16;
		Color3DLight* = 22;
		Color3DFace* = ColorBtnFace;
		Color3DShadow* = ColorBtnShadow;

	тип
		(** handle types for different win and gdi objects *)
		HWND* = Kernel32.HANDLE;
		HDC* = Kernel32.HANDLE;
		HGDIObj* = Kernel32.HANDLE;
		HFont* = HGDIObj;
		HBrush* = HGDIObj;
		HRgn* = HGDIObj;
		HBitmap* = HGDIObj;
		HIcon* = Kernel32.HANDLE;
		HCursor* = Kernel32.HANDLE;
		HMenu* = Kernel32.HANDLE;
		HPalette* = Kernel32.HANDLE;
		HAccel* = Kernel32.HANDLE;
		HHook* = Kernel32.HANDLE;

		(** The COLORREF value is used to specify an RGB color. *)
		ColorRef* = цел32;

		(** The POINT structure defines the x- and y- coordinates of a point. *)
		Point* = запись
			x*, y*: цел32
		кон;
		PointL* = Point;
		PointF* = запись
			x*, y*: вещ32
		кон;

		(** The RECT structure defines the coordinates of the upper-left and lower-right corners of a rectangle. *)
		Rect* = запись
			left*, top*, right*, bottom*: цел32
		кон;
		RectL* = Rect;

		(** The SIZE structure specifies the width and height of a rectangle. *)
		Size* = запись
			cx*, cy*: цел32
		кон;
		SizeL* = Size;

		(** The MINMAXINFO structure contains information about a window's maximized size and position and its minimum
			and maximum tracking size. *)
		MinMaxInfo* = запись
			ptReserved*, ptMaxSize*, ptMaxPosition*, ptMinTrackSize*, ptMaxTrackSize*: Point
		кон;

		(** The MSG structure contains message information from a thread's message queue. *)
		Msg* = запись
			hwnd*: HWND;
			message*: цел32;
			wParam*: WParam;
			lParam*: LParam;
			time*: цел32;
			point*: Point
		кон;

		(** A 32-bit value passed as a parameter to a window procedure or callback function. *)
		WParam* = размерМЗ; LParam* = размерМЗ;

		(** A 32-bit value returned from a window procedure or callback function. *)
			LResult* = адресВПамяти;

		(** A 32-bit pointer to a window procedure. *)
		WndProc* = проц {WINAPI} (hwnd: HWND; uMsg: цел32; wParam: WParam; lParam: LParam): LResult;

		(** The WNDCLASSEX structure contains window class information. It is used with the RegisterClassEx and
			GetClassInfoEx functions. *)
		WndClassEx* = запись
			cbSize*: цел32;
			style*: INTEGERSET;
			lpfnWndProc*: WndProc;
			cbClsExtra*, cbWndExtra*: цел32;
			hInstance*: Kernel32.HINSTANCE;
			hIcon*: HIcon;
			hCursor*: HCursor;
			hbrBackground*: HBrush;
			lpszMenuName*, lpszClassName*: Kernel32.LPSTR;
			hIconSm*: HIcon
		кон;

		(** Application-defined callback function used with the EnumWindows. *)
		WndEnumProc* = проц {WINAPI} (hwnd: HWND; lParam: LParam): Kernel32.BOOL;

		(** The WINDOWPOS structure contains information about the size and position of a window. *)
		WindowPos* = запись  			hwnd*, hwndInsertAfter*: HWND;
			x*, y*, cx*, cy*: цел32;
			flags*: INTEGERSET;
		кон;

		(** The WINDOWPLACEMENT structure contains information about the placement of a window on the screen. *)
		WindowPlacement* = запись
			length*: цел32;
			flags*: DWORD;
			showCmd*: цел32;
			ptMinPosition*, ptMaxPosition*: Point;
			rcNormalPosition*: Rect
		кон;

		(** The MENUITEMINFO structure contains information about a menu item. *)
		MenuItemInfo* = запись  			cbSize*: цел32;
			fMask*, fType*, fState*: DWORD;
			wID*: цел32;
			hSubMenu*: HMenu;
			hbmpChecked*, hbmpUnchecked*: HBitmap;
			dwItemData*: цел32;
			dwTypeData*: Kernel32.LPSTR;
			cch*: цел32
		кон;

		(** The ACCEL structure defines an accelerator key used in an accelerator table. *)
		Accel* = запись  			fVirt*: симв8;
			key*, cmd*: цел16
		кон;

		(** Proctype for SetWindowsHook(Ex) *)
		HookProc* = проц {WINAPI} (code: цел32; wParam: WParam; lParam: LParam): LResult;

		PaintStruct* = запись
			hdc*: HDC;
		    fErase*: Kernel32.BOOL;
		    rcPaint*: Rect;
		    fRestore, fIncUpdate: Kernel32.BOOL;
		    rgbReserved: массив 32 из симв8
		кон;

	перем
		(** The BeginPaint function prepares the specified window for painting and fills a PAINTSTRUCT
			structure with information about the painting. *)
		BeginPaint-: проц {WINAPI} (hWnd: HWND; перем lpPaint: PaintStruct): HDC;
		(** The BringWindowToTop function brings the specified window to the top of the Z order. *)
		BringWindowToTop-: проц {WINAPI} (hWnd: HWND): Kernel32.BOOL;
		(** The CallNextHookEx function passes the hook information to the next hook procedure in the current hook chain. *)
		CallNextHookEx-: проц {WINAPI} (hhk: HHook; nCode: цел32; wParam: WParam; lParam: LParam): LResult;
		(** The CloseClipboard function closes the clipboard. *)
		CloseClipboard-: проц {WINAPI} (): Kernel32.BOOL;
		(** The CreateAcceleratorTable function creates an accelerator table. *)
		CreateAcceleratorTable-: проц {WINAPI} (lpaccl: адресВПамяти; cEntries: цел32): HAccel;
		(** The CreateMenu function creates a menu. *)
		CreateMenu-: проц {WINAPI} (): HMenu;
		(** The CreateWindow function creates an overlapped, pop-up, or child window. *)
		CreateWindowEx-: проц {WINAPI} (dwExStyle: цел32; перем lpClassName, lpWindowName: массив   из симв8; dwStyle: INTEGERSET; x, y, nWidth, nHeight: цел32; hWndParent: HWND; hMenu: HMenu; hInstance: Kernel32.HINSTANCE; lpParam: LParam): HWND;
		(** The DefWindowProc function calls the default window procedure to provide default processing for any window
			messages that an application does not process. *)
		DefWindowProc-: WndProc;
		(** The DestroyAcceleratorTable function destroys an accelerator table. *)
		DestroyAcceleratorTable-: проц {WINAPI} (hAccel: HAccel): Kernel32.BOOL;
		(** The DestroyCursor function destroys a cursor and frees any memory the cursor occupied. *)
		DestroyCursor-: проц {WINAPI} (hCursor: HCursor): Kernel32.BOOL;
		(** The DestroyIcon function destroys an icon and frees any memory the icon occupied. *)
		DestroyIcon-: проц {WINAPI} (hIcon: HIcon): Kernel32.BOOL;
		(** The DestroyMenu function destroys the specified menu and frees any memory that the menu occupies. *)
		DestroyMenu-: проц {WINAPI} (hMenu: HMenu): Kernel32.BOOL;
		(** The DestroyWindow function destroys the specified window. *)
		DestroyWindow-: проц {WINAPI} (hWnd: HWND): Kernel32.BOOL;
		(** The DispatchMessage function dispatches a message to a window procedure. *)
		DispatchMessage-: проц {WINAPI} (перем lpMsg: Msg): цел32;
		(** The DrawMenuBar function redraws the menu bar of the specified window. *)
		DrawMenuBar-: проц {WINAPI} (hWnd: HWND): Kernel32.BOOL;
		(** The EmptyClipboard function empties the clipboard and frees handles to data in the clipboard. *)
		EmptyClipboard-: проц {WINAPI} (): Kernel32.BOOL;
		(** The EnableWindow function enables or disables mouse and keyboard input to the specified window or control. *)
		EnableWindow-: проц {WINAPI} (hWnd: HWND; bEnable: Kernel32.BOOL): Kernel32.BOOL;
		(** The EndPaint function marks the end of painting in the specified window. *)
		EndPaint-: проц {WINAPI} (hWnd: HWND; перем lpPaint: PaintStruct): Kernel32.BOOL;
		(** The EnumWindows function enumerates all top-level windows on the screen by passing the handle to each
			window, in turn, to an application-defined callback function. *)
		EnumWindows-: проц {WINAPI} (lpEnumProc: WndEnumProc; lParam: LParam): Kernel32.BOOL;
		(** The EqualRect function determines whether the two specified rectangles are equal by comparing the coordinates
			of their upper-left and lower-right corners. *)
		EqualRect-: проц {WINAPI} (lprc1, lprc2: Rect): Kernel32.BOOL;
		(** The GetAsyncKeyState function determines whether a key is up or down at the time the function is called, and
			whether the key was pressed after a previous call to GetAsyncKeyState. *)
		GetAsyncKeyState-: проц {WINAPI} (vKey: цел32): цел16;
		(** The GetClientRect function retrieves the coordinates of a window's client area. *)
		GetClientRect-: проц {WINAPI} (hWnd: HWND; перем lpRect: Rect): Kernel32.BOOL;
		(** The GetClipboardData function retrieves data from the clipboard in a specified format. *)
		GetClipboardData-: проц {WINAPI} (uFormat: цел32): Kernel32.HANDLE;
		(** The GetClipboardFormatName function retrieves from the clipboard the name of the specified registered format. *)
		GetClipboardFormatName-: проц {WINAPI} (format: цел32; перем lpszFormatName: массив   из симв8; cchMaxCount: цел32): цел32;
		(** The GetCursorPos function retrieves the cursor's position, in screen coordinates. *)
		GetCursorPos-: проц {WINAPI} (перем lpPoint: Point): Kernel32.BOOL;
		(** The GetDC function retrieves a handle to a display device context (DC) for the client area of a specified window
			or for the entire screen. *)
		GetDC-: проц {WINAPI} (hWnd: HWND): HDC;
		(** The GetFocus function retrieves the handle to the window that has the keyboard focus, if the window is attached
			to the calling thread's message queue. *)
		GetFocus-: проц {WINAPI} (): HWND;
		(** Retrieves a handle to the foreground window (the window with which the user is currently working). The system assigns a slightly higher priority to the thread that creates the foreground window than it does to other threads.  *)
		GetForegroundWindow-: проц {WINAPI} (): HWND;
		(** The GetKeyState function retrieves the status of the specified virtual key. *)
		GetKeyState-: проц {WINAPI} (vKey: цел32): цел16;
		(** The GetMenu function retrieves a handle to the menu assigned to the specified window. *)
		GetMenu-: проц {WINAPI} (hWnd: HWND): HMenu;
		(** The GetMenuItemInfo function retrieves information about a menu item. *)
		GetMenuItemInfo-: проц {WINAPI} (hMenu: HMenu; uItem: цел32; fyByPosition: Kernel32.BOOL; перем lpmii: MenuItemInfo): Kernel32.BOOL;
		(** The GetMenuString function copies the text string of the specified menu item into the specified buffer. *)
		GetMenuString-: проц {WINAPI} (hMenu: HMenu; uIDItem: цел32; перем lpString: массив   из симв8; nMaxCount: цел32; uFlag: DWORD): Kernel32.BOOL;
		(** The GetMessage function retrieves a message from the calling thread's message queue and places it in the
			specified structure. *)
		GetMessage-: проц {WINAPI} (перем lpMsg: Msg; hWnd: HWND; wMsgFilterMin, wMsgFilterMax: цел32): цел32;
		(** The GetParent function retrieves a handle to the specified child window's parent window. *)
		GetParent-: проц {WINAPI} (hWnd: HWND): HWND;
		(** The GetProp function retrieves a data handle from the property list of the given window. *)
		GetProp-: проц {WINAPI} (hWnd: HWND; lpString: цел32): цел32;
		(** The GetSystemMetrics function retrieves various system metrics (widths and heights of display elements) and
			system configuration settings. *)
		(** The GetSysColor function retrieves the current color of the specified display element. *)
		GetSysColor-: проц {WINAPI} (nIndex: цел32): ColorRef;
		(** The GetSystemMetrics function retrieves various system metrics (widths and heights of display elements)
			and system configuration settings. *)
		GetSystemMetrics-: проц {WINAPI} (nIndex: цел32): цел32;
		(** The GetWindowLong function retrieves information about the specified window. *)
		GetWindowLong-: проц {WINAPI} (hWnd: HWND; nIndex: цел32): цел32;
		(** The GetWindowPlacement function retrieves the show state and the restored, minimized, and maximized
			positions of the specified window. *)
		GetWindowLongPtr-: проц {WINAPI} (hWnd: HWND; nIndex: цел32): адресВПамяти;
		(** The GetWindowPlacement function retrieves the show state and the restored, minimized, and maximized
			positions of the specified window. *)
		GetWindowPlacement-: проц {WINAPI} (hWnd: HWND; перем lpwndpl: WindowPlacement): Kernel32.BOOL;
		(** The GetWindowRect function retrieves the dimensions of the bounding rectangle of the specified window. *)
		GetWindowRect-: проц {WINAPI} (hWnd: HWND; перем lpRect: Rect): Kernel32.BOOL;
		(** The GetWindowRgn function obtains a copy of the window region of a window. *)
		GetWindowRgn-: проц {WINAPI} (hWnd: HWND; перем hRgn: HRgn): цел32;
		(** The GetWindowText function copies the text of the specified window's title bar (if it has one) into a buffer. *)
		GetWindowText-: проц {WINAPI} (hWnd: HWND; перем lpString: массив   из симв8; nMaxCount: цел32): цел32;
		(** The GetUpdateRect function retrieves the coordinates of the smallest rectangle that completely encloses the
			update region of the specified window. *)
		GetUpdateRect-: проц {WINAPI} (hWnd: HWND; перем lpRect: Rect; bErase: Kernel32.BOOL): Kernel32.BOOL;
		(** The GetUpdateRgn function retrieves the update region of a window by copying it into the specified
			region. *)
		GetUpdateRgn-: проц {WINAPI} (hWnd: HWND; перем hRgn: HRgn; bErase: Kernel32.BOOL): цел32;
		(** The InsertMenu function inserts a new menu item into a menu, moving other items down the menu. *)
		InsertMenu-: проц {WINAPI} (hMenu: HMenu; uPosition: цел32; uFlags: DWORD; uIDNewItem: цел32; перем lpNewItem: массив   из симв8): Kernel32.BOOL;
		(** The IntersectRect function calculates the intersection of two source rectangles and places the coordinates of the
			intersection rectangle into the destination rectangle. *)
		IntersectRect-: проц {WINAPI} (перем lprcDst: Rect; lprcSrc1, lprcSrc2: Rect): Kernel32.BOOL;
		(** The InvalidateRect function adds a rectangle to the specified window's update region. *)
		InvalidateRect-: проц {WINAPI} (hWnd: HWND; lpRect: Rect; bErase: Kernel32.BOOL): Kernel32.BOOL;
		(** The IsChild function tests whether a window is a child window or descendant window of a specified parent window. *)
		IsChild-: проц {WINAPI} (hWndParent, hWnd: HWND): Kernel32.BOOL;
		(** The LoadCursor function loads the specified cursor resource from the executable (.EXE) file associated with
			an application instance. *)
		LoadCursor-: проц {WINAPI} (hInstance: Kernel32.HINSTANCE; перем lpCursorName: массив   из симв8): HCursor;
		(** The LoadCursorFromFile function creates a cursor based on data contained in a file. *)
		LoadCursorFromFile-: проц {WINAPI} (перем lpFileName: массив   из симв8): HCursor;
		(** The LoadIcon function loads the specified icon resource from the executable (.exe) file associated with an
			application instance. *)
		LoadIcon-: проц {WINAPI} (hInstance: Kernel32.HINSTANCE; перем lpIconName: массив   из симв8): HIcon;
		(** The MessageBeep function plays a waveform sound. *)
		MessageBeep-: проц {WINAPI} (uType: цел32): Kernel32.BOOL;
		(** The MessageBox function creates, displays, and operates a message box. *)
		MessageBox-: проц {WINAPI} (hWnd: HWND; перем lpText, lpCaption: массив   из симв8; uType: DWORD): цел32;
		(** The MoveWindow function changes the position and dimensions of the specified window. *)
		MoveWindow-: проц {WINAPI} (hWnd: HWND; X, Y, nWidth, nHeight: цел32; bRepaint: Kernel32.BOOL): Kernel32.BOOL;
		(** The OffsetRect function moves the specified rectangle by the specified offsets. *)
		OffsetRect-: проц {WINAPI} (перем lprc: Rect; dx, dy: цел32): Kernel32.BOOL;
		(** The OpenClipboard function opens the clipboard for examination and prevents other applications from modifying
			the clipboard content. *)
		OpenClipboard-: проц {WINAPI} (hWndNewOwner: HWND): Kernel32.BOOL;
		(** The PtInRect function determines whether the specified point lies within the specified rectangle. *)
		PtInRect-: проц {WINAPI} (lprc: Rect; ptx, pty: цел32): Kernel32.BOOL;
		(** The PostMessage function places (posts) a message in the message queue associated with the thread that created
			the specified window and then returns without waiting for the thread to process the message. *)
		PostMessage-: проц {WINAPI} (hWnd: HWND; Msg: цел32; wParam: WParam; lParam: LParam): Kernel32.BOOL;
		(** The PostQuitMessage function indicates to the system that a thread has made a request to terminate (quit). *)
		PostQuitMessage-: проц {WINAPI} (nExitCode: цел32);
		(** The RegisterClassEx function registers a window class for subsequent use in calls to the CreateWindow or
			CreateWindowEx function. *)
		RegisterClassEx-: проц {WINAPI} (перем lpwcx: WndClassEx): Kernel32.ATOM;
		(** The RegisterClipboardFormat function registers a new clipboard format. *)
		RegisterHotKey-: проц{WINAPI} (hWnd: HWND; id: цел32; fsmodifiers,vk: цел32): Kernel32.BOOL;
		(*The RegisterHotKey function defines a system-wide hot key *)
		RegisterClipboardFormat-: проц {WINAPI} (перем lpszFormat: массив   из симв8): цел16;
		(** The RegisterWindowMessage function defines a new window message that is guaranteed to be
			unique throughout the system. *)
		RegisterWindowMessage-: проц {WINAPI} (перем lpString: массив   из симв8): цел32;
		(** The ReleaseCapture function releases the mouse capture from a window in the current thread and restores normal
			mouse input processing. *)
		ReleaseCapture-: проц {WINAPI} (): Kernel32.BOOL;
		(** The ReleaseDC function releases a device context (DC), freeing it for use by other applications. *)
		ReleaseDC-: проц {WINAPI} (hWnd: HWND; hDC: HDC): цел32;
		(** The RemoveProp function removes an entry from the property list of the specified window. *)
		RemoveProp-: проц {WINAPI} (hWnd: HWND; lpString: цел32): цел32;
		(** The ScreenToClient function converts the screen coordinates of a specified point on the screen to client coordinates. *)
		ScreenToClient-: проц {WINAPI} (hWnd: HWND; перем lpPoint: Point): Kernel32.BOOL;
		(** The SendMessage function sends the specified message to a window or windows. *)
		SendMessage-: проц {WINAPI} (hWnd: HWND; Msg: цел32; wParam: WParam; lParam: LParam): LResult;
		(** The SetCapture function sets the mouse capture to the specified window belonging to the current thread. *)
		SetCapture-: проц {WINAPI} (hWnd: HWND): HWND;
		(** The SetClipboardData function places data on the clipboard in a specified clipboard format. *)
		SetClipboardData-: проц {WINAPI} (uFormat: цел32; hMem: Kernel32.HANDLE): Kernel32.HANDLE;
		(** The SetCursor function establishes the cursor shape. *)
		SetCursor-: проц {WINAPI} (hCursor: HCursor): HCursor;
		(** The SetCursorPos function moves the cursor to the specified screen coordinates. *)
		SetCursorPos-: проц {WINAPI} (X, Y: цел32): Kernel32.BOOL;
		(** The SetFocus function sets the keyboard focus to the specified window. *)
		SetFocus-: проц {WINAPI} (hWnd: HWND): HWND;
		(** The SetForegroundWindow function puts the thread that created the specified window into the foreground
			and activates the window. *)
		SetForegroundWindow-: проц {WINAPI} (hWnd: HWND): Kernel32.BOOL;
		(** The SetMenu function assigns a new menu to the specified window. *)
		SetMenu-: проц {WINAPI} (hWnd: HWND; hMenu: HMenu): Kernel32.BOOL;
		(** The SetMenuItemInfo function changes information about a menu item. *)
		SetMenuItemInfo-: проц {WINAPI} (hMenu: HMenu; uItem: цел32; fyByPosition: Kernel32.BOOL; перем lpmii: MenuItemInfo): Kernel32.BOOL;
		(** The SetProp function adds a new entry or changes an existing entry in the property list of the specified window. *)
		SetProp-: проц {WINAPI} (hWnd: HWND; lpString, hData: цел32): Kernel32.BOOL;
		(** The SetWindowsHookEx function installs an application-defined hook procedure into a hook chain. *)
		SetWindowsHookEx-: проц {WINAPI} (idHook: цел32; lpfn: HookProc; hMod: Kernel32.HINSTANCE; dwThreadId: цел32): HHook;
		(** The SetWindowLong function changes an attribute of the specified window. *)
		SetWindowLong-: проц {WINAPI} (hWnd: HWND; nIndex, dwNewLong: цел32): цел32;
		(** The SetWindowPos function changes the size, position, and Z order of a child, pop-up, or top-level window. *)
		SetWindowLongPtr-: проц {WINAPI} (hWnd: HWND; nIndex:цел32; dwNewLong: адресВПамяти): адресВПамяти;
		(** The SetWindowPos function changes the size, position, and Z order of a child, pop-up, or top-level window. *)
		SetWindowPos-: проц {WINAPI} (hWnd, hWndInsertAfter: HWND; X, Y, cx, cy: цел32; uFlags: DWORD): Kernel32.BOOL;
		(** The SetWindowRgn function sets the window region of a window. *)
		SetWindowRgn-: проц {WINAPI} (hWnd: HWND; hRgn: HRgn; bRedraw: Kernel32.BOOL): цел32;
		(** The SetWindowText function changes the text of the specified window's title bar (if it has one). *)
		SetWindowText-: проц {WINAPI} (hWnd: HWND; перем lpString: массив   из симв8): Kernel32.BOOL;
		(** The ShowCursor function displays or hides the cursor. *)
		ShowCursor-: проц {WINAPI} (bShow: Kernel32.BOOL): цел32;
		(** The ShowWindow function sets the specified window's show state. *)
		ShowWindow-: проц {WINAPI} (hWnd: HWND; nCmdShow: цел32): Kernel32.BOOL;
		(** The ShowWindowAsync function sets the show state of a window created by a different thread. *)
		ShowWindowAsync-: проц {WINAPI} (hWnd: HWND; nCmdShow: цел32): Kernel32.BOOL;
		(** The TrackPopupMenu function displays a shortcut menu at the specified location and tracks the selection of
			items on the menu. *)
		TrackPopupMenu-: проц {WINAPI} (hMenu: HMenu; uFlags: DWORD; x, y, nReserved: цел32; hWnd: HWND; перем prcRect: Rect): Kernel32.BOOL;
		(** The TranslateAccelerator function processes accelerator keys for menu commands. *)
		TranslateAccelerator-: проц {WINAPI} (hWnd: HWND; hAccTable: HAccel; перем lpMsg: Msg): цел32;
		(** The TranslateMessage function translates virtual-key messages into character messages. *)
		TranslateMessage-: проц {WINAPI} (перем lpMsg: Msg): Kernel32.BOOL;
		(** The UnhookWindowsHookEx function removes a hook procedure installed in a hook chain by the SetWindowsHookEx
			function. *)
		UnhookWindowsHookEx-: проц {WINAPI} (hhk: HHook): Kernel32.BOOL;
		(** The UnregisterClass function removes a window class, freeing the memory required for the class. *)
		UnregisterClass-: проц {WINAPI} (перем lpClassName: массив   из симв8; hInstance: Kernel32.HINSTANCE): Kernel32.BOOL;
		(** The UpdateWindow function updates the client area of the specified window by sending a WM_PAINT message
			to the window if the window's update region is not empty. *)
		UpdateWindow-: проц {WINAPI} (hWnd: HWND): Kernel32.BOOL;
		(** The ValidateRect function validates the client area within a rectangle by removing the rectangle from the
			update region of the specified window. *)
		ValidateRect-: проц {WINAPI} (hWnd: HWND; перем lpRect: Rect): Kernel32.BOOL;

		GetConsoleWindow-: проц{WINAPI} (): цел32 ;


	(** The EqualSize function determines whether the two specified sizes are equal. *)
	проц {WINAPI} EqualSize*(перем a, b: Size): булево;
	нач
		возврат (a.cx = b.cx) и (a.cy = b.cy)
	кон EqualSize;

проц Init;
перем mod: Kernel32.HMODULE; str: массив 32 из симв8;
нач
	str := "KERNEL32.DLL";
	mod := Kernel32.LoadLibrary(str);
	Kernel32.GetProcAddress(mod, "GetConsoleWindow", НИЗКОУР.подмениТипЗначения(адресВПамяти, GetConsoleWindow));
	(* the GetConsoleWindow function is not available for version minor Win2000, that's why it is set dynamically here *)
	str := "USER32.DLL";
	mod := Kernel32.LoadLibrary(str);
	Kernel32.GetProcAddress(mod, "BeginPaint", НИЗКОУР.подмениТипЗначения(адресВПамяти,BeginPaint ));
	Kernel32.GetProcAddress(mod, "BringWindowToTop", НИЗКОУР.подмениТипЗначения(адресВПамяти,BringWindowToTop ));
	Kernel32.GetProcAddress(mod, "CallNextHookEx", НИЗКОУР.подмениТипЗначения(адресВПамяти,CallNextHookEx ));
	Kernel32.GetProcAddress(mod, "CloseClipboard", НИЗКОУР.подмениТипЗначения(адресВПамяти,CloseClipboard ));
	Kernel32.GetProcAddress(mod, "CreateAcceleratorTableA", НИЗКОУР.подмениТипЗначения(адресВПамяти,CreateAcceleratorTable ));
	Kernel32.GetProcAddress(mod, "CreateMenu", НИЗКОУР.подмениТипЗначения(адресВПамяти,CreateMenu ));
	Kernel32.GetProcAddress(mod, "CreateWindowExA", НИЗКОУР.подмениТипЗначения(адресВПамяти,CreateWindowEx ));
	Kernel32.GetProcAddress(mod, "DefWindowProcA", НИЗКОУР.подмениТипЗначения(адресВПамяти,DefWindowProc ));
	Kernel32.GetProcAddress(mod, "DestroyAcceleratorTable", НИЗКОУР.подмениТипЗначения(адресВПамяти,DestroyAcceleratorTable ));
	Kernel32.GetProcAddress(mod, "DestroyMenu", НИЗКОУР.подмениТипЗначения(адресВПамяти,DestroyMenu ));
	Kernel32.GetProcAddress(mod, "DestroyWindow", НИЗКОУР.подмениТипЗначения(адресВПамяти,DestroyWindow ));
	Kernel32.GetProcAddress(mod, "DispatchMessageA", НИЗКОУР.подмениТипЗначения(адресВПамяти,DispatchMessage ));
	Kernel32.GetProcAddress(mod, "DrawMenuBar", НИЗКОУР.подмениТипЗначения(адресВПамяти,DrawMenuBar ));
	Kernel32.GetProcAddress(mod, "EmptyClipboard", НИЗКОУР.подмениТипЗначения(адресВПамяти,EmptyClipboard ));
	Kernel32.GetProcAddress(mod, "EnableWindow", НИЗКОУР.подмениТипЗначения(адресВПамяти,EnableWindow ));
	Kernel32.GetProcAddress(mod, "EndPaint", НИЗКОУР.подмениТипЗначения(адресВПамяти,EndPaint ));
	Kernel32.GetProcAddress(mod, "EnumWindows", НИЗКОУР.подмениТипЗначения(адресВПамяти,EnumWindows ));
	Kernel32.GetProcAddress(mod, "EqualRect", НИЗКОУР.подмениТипЗначения(адресВПамяти,EqualRect ));
	Kernel32.GetProcAddress(mod, "GetAsyncKeyState", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetAsyncKeyState ));
	Kernel32.GetProcAddress(mod, "GetClientRect", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetClientRect ));
	Kernel32.GetProcAddress(mod, "GetClipboardData", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetClipboardData ));
	Kernel32.GetProcAddress(mod, "GetClipboardFormatNameA", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetClipboardFormatName));
	Kernel32.GetProcAddress(mod, "GetCursorPos", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetCursorPos ));
	Kernel32.GetProcAddress(mod, "GetDC", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetDC ));
	Kernel32.GetProcAddress(mod, "GetFocus", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetFocus ));
	Kernel32.GetProcAddress(mod, "GetForegroundWindow", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetForegroundWindow ));
	Kernel32.GetProcAddress(mod, "GetKeyState", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetKeyState ));
	Kernel32.GetProcAddress(mod, "GetMenu", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetMenu ));
	Kernel32.GetProcAddress(mod, "GetMenuItemInfoA", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetMenuItemInfo ));
	Kernel32.GetProcAddress(mod, "GetMenuStringA", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetMenuString ));
	Kernel32.GetProcAddress(mod, "GetMessageA", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetMessage ));
	Kernel32.GetProcAddress(mod, "GetParent", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetParent ));
	Kernel32.GetProcAddress(mod, "GetPropA", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetProp ));
	Kernel32.GetProcAddress(mod, "GetSysColor", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetSysColor ));
	Kernel32.GetProcAddress(mod, "GetSystemMetrics", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetSystemMetrics ));
	Kernel32.GetProcAddress(mod, "GetWindowLongA", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetWindowLong ));
	Kernel32.GetProcAddress(mod, "GetWindowLongPtrA", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetWindowLongPtr ));
	Kernel32.GetProcAddress(mod, "GetWindowPlacement", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetWindowPlacement ));
	Kernel32.GetProcAddress(mod, "GetWindowRect", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetWindowRect ));
	Kernel32.GetProcAddress(mod, "GetWindowRgn", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetWindowRgn ));
	Kernel32.GetProcAddress(mod, "GetWindowTextA", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetWindowText ));
	Kernel32.GetProcAddress(mod, "GetUpdateRect", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetUpdateRect ));
	Kernel32.GetProcAddress(mod, "GetUpdateRgn", НИЗКОУР.подмениТипЗначения(адресВПамяти,GetUpdateRgn ));
	Kernel32.GetProcAddress(mod, "InvalidateRect", НИЗКОУР.подмениТипЗначения(адресВПамяти,InvalidateRect ));
	Kernel32.GetProcAddress(mod, "InsertMenuA", НИЗКОУР.подмениТипЗначения(адресВПамяти,InsertMenu ));
	Kernel32.GetProcAddress(mod, "IntersectRect", НИЗКОУР.подмениТипЗначения(адресВПамяти,IntersectRect ));
	Kernel32.GetProcAddress(mod, "IsChild", НИЗКОУР.подмениТипЗначения(адресВПамяти,IsChild ));
	Kernel32.GetProcAddress(mod, "LoadCursorA", НИЗКОУР.подмениТипЗначения(адресВПамяти,LoadCursor ));
	Kernel32.GetProcAddress(mod, "LoadCursorFromFileA", НИЗКОУР.подмениТипЗначения(адресВПамяти,LoadCursorFromFile ));
	Kernel32.GetProcAddress(mod, "LoadIconA", НИЗКОУР.подмениТипЗначения(адресВПамяти,LoadIcon ));
	Kernel32.GetProcAddress(mod, "MessageBeep", НИЗКОУР.подмениТипЗначения(адресВПамяти,MessageBeep ));
	Kernel32.GetProcAddress(mod, "MessageBoxA", НИЗКОУР.подмениТипЗначения(адресВПамяти,MessageBox ));
	Kernel32.GetProcAddress(mod, "MoveWindow", НИЗКОУР.подмениТипЗначения(адресВПамяти,MoveWindow ));
	Kernel32.GetProcAddress(mod, "OffsetRect", НИЗКОУР.подмениТипЗначения(адресВПамяти,OffsetRect ));
	Kernel32.GetProcAddress(mod, "OpenClipboard", НИЗКОУР.подмениТипЗначения(адресВПамяти,OpenClipboard ));
	Kernel32.GetProcAddress(mod, "PtInRect", НИЗКОУР.подмениТипЗначения(адресВПамяти,PtInRect ));
	Kernel32.GetProcAddress(mod, "PostMessageA", НИЗКОУР.подмениТипЗначения(адресВПамяти,PostMessage ));
	Kernel32.GetProcAddress(mod, "PostQuitMessage", НИЗКОУР.подмениТипЗначения(адресВПамяти,PostQuitMessage ));
	Kernel32.GetProcAddress(mod, "RegisterClassExA", НИЗКОУР.подмениТипЗначения(адресВПамяти,RegisterClassEx ));
	Kernel32.GetProcAddress(mod, "RegisterClipboardFormatA", НИЗКОУР.подмениТипЗначения(адресВПамяти,RegisterClipboardFormat ));
	Kernel32.GetProcAddress(mod, "RegisterHotKey", НИЗКОУР.подмениТипЗначения(адресВПамяти,RegisterHotKey ));
	Kernel32.GetProcAddress(mod, "RegisterWindowMessageA", НИЗКОУР.подмениТипЗначения(адресВПамяти,RegisterWindowMessage ));
	Kernel32.GetProcAddress(mod, "ReleaseCapture", НИЗКОУР.подмениТипЗначения(адресВПамяти,ReleaseCapture ));
	Kernel32.GetProcAddress(mod, "ReleaseDC", НИЗКОУР.подмениТипЗначения(адресВПамяти,ReleaseDC ));
	Kernel32.GetProcAddress(mod, "RemovePropA", НИЗКОУР.подмениТипЗначения(адресВПамяти,RemoveProp ));
	Kernel32.GetProcAddress(mod, "ScreenToClient", НИЗКОУР.подмениТипЗначения(адресВПамяти,ScreenToClient ));
	Kernel32.GetProcAddress(mod, "SendMessageA", НИЗКОУР.подмениТипЗначения(адресВПамяти,SendMessage ));
	Kernel32.GetProcAddress(mod, "SetCapture", НИЗКОУР.подмениТипЗначения(адресВПамяти,SetCapture ));
	Kernel32.GetProcAddress(mod, "SetClipboardData", НИЗКОУР.подмениТипЗначения(адресВПамяти,SetClipboardData ));
	Kernel32.GetProcAddress(mod, "SetCursor", НИЗКОУР.подмениТипЗначения(адресВПамяти,SetCursor ));
	Kernel32.GetProcAddress(mod, "SetCursorPos", НИЗКОУР.подмениТипЗначения(адресВПамяти,SetCursorPos ));
	Kernel32.GetProcAddress(mod, "SetFocus", НИЗКОУР.подмениТипЗначения(адресВПамяти,SetFocus ));
	Kernel32.GetProcAddress(mod, "SetForegroundWindow", НИЗКОУР.подмениТипЗначения(адресВПамяти,SetForegroundWindow ));
	Kernel32.GetProcAddress(mod, "SetMenu", НИЗКОУР.подмениТипЗначения(адресВПамяти,SetMenu ));
	Kernel32.GetProcAddress(mod, "SetMenuItemInfoA", НИЗКОУР.подмениТипЗначения(адресВПамяти,SetMenuItemInfo ));
	Kernel32.GetProcAddress(mod, "SetPropA", НИЗКОУР.подмениТипЗначения(адресВПамяти,SetProp ));
	Kernel32.GetProcAddress(mod, "SetWindowsHookExA", НИЗКОУР.подмениТипЗначения(адресВПамяти,SetWindowsHookEx ));
	Kernel32.GetProcAddress(mod, "SetWindowLongA", НИЗКОУР.подмениТипЗначения(адресВПамяти,SetWindowLong ));
	Kernel32.GetProcAddress(mod, "SetWindowLongPtrA", НИЗКОУР.подмениТипЗначения(адресВПамяти,SetWindowLongPtr ));
	Kernel32.GetProcAddress(mod, "SetWindowPos", НИЗКОУР.подмениТипЗначения(адресВПамяти,SetWindowPos ));
	Kernel32.GetProcAddress(mod, "SetWindowRgn", НИЗКОУР.подмениТипЗначения(адресВПамяти,SetWindowRgn ));
	Kernel32.GetProcAddress(mod, "SetWindowTextA", НИЗКОУР.подмениТипЗначения(адресВПамяти,SetWindowText ));
	Kernel32.GetProcAddress(mod, "ShowCursor", НИЗКОУР.подмениТипЗначения(адресВПамяти,ShowCursor ));
	Kernel32.GetProcAddress(mod, "ShowWindow", НИЗКОУР.подмениТипЗначения(адресВПамяти,ShowWindow ));
	Kernel32.GetProcAddress(mod, "ShowWindowAsync", НИЗКОУР.подмениТипЗначения(адресВПамяти,ShowWindowAsync ));
	Kernel32.GetProcAddress(mod, "TrackPopupMenu", НИЗКОУР.подмениТипЗначения(адресВПамяти,TrackPopupMenu ));
	Kernel32.GetProcAddress(mod, "TranslateAcceleratorA", НИЗКОУР.подмениТипЗначения(адресВПамяти,TranslateAccelerator ));
	Kernel32.GetProcAddress(mod, "TranslateMessage", НИЗКОУР.подмениТипЗначения(адресВПамяти,TranslateMessage ));
	Kernel32.GetProcAddress(mod, "UnhookWindowsHookEx", НИЗКОУР.подмениТипЗначения(адресВПамяти,UnhookWindowsHookEx ));
	Kernel32.GetProcAddress(mod, "UnregisterClassA", НИЗКОУР.подмениТипЗначения(адресВПамяти,UnregisterClass ));
	Kernel32.GetProcAddress(mod, "UpdateWindow", НИЗКОУР.подмениТипЗначения(адресВПамяти,UpdateWindow ));
	Kernel32.GetProcAddress(mod, "ValidateRect", НИЗКОУР.подмениТипЗначения(адресВПамяти,ValidateRect ));


кон Init;

нач
Init;
кон User32.

(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль DNS;   (** AUTHOR "pjm, mvt, gf"; PURPOSE "DNS client"; *)


использует S := НИЗКОУР, Out := ЛогЯдра, Unix, IP, Files;

конст
	(** Error codes *)
	Ok* = 0;  NotFound* = 3601;  BadName* = 3602;
	MaxNofServer* = 10;   (* max. number registered of DNS servers *)


тип
	Name* = массив 128 из симв8;   (* domain or host name type *)


	Hostent = укль {опасныйДоступКПамяти, неОтслСборщиком} на запись
			name		: укль {опасныйДоступКПамяти, неОтслСборщиком} на Name;
			aliases	: адресВПамяти;
			addrtype	: цел32;
			length		: цел32;
			addrlist	: адресВПамяти
		кон;

перем
	(** Local domain name *)
	domain*: Name;

	nlib: адресВПамяти;

	gethostbyaddr	: проц {C} ( adr: адресВПамяти; len, typ: цел32 ): Hostent;
	gethostbyname	: проц {C} ( name: адресВПамяти ): Hostent;
	gethostname		: проц {C} ( name: адресВПамяти; len: размерМЗ ): цел32;

	(* Statistic variables *)
	NDNSReceived-, NDNSSent-, NDNSMismatchID-, NDNSError-: цел32;

	(** Find the host responsible for mail exchange of the specified domain. *)
	проц MailHostByDomain*( конст domain: массив из симв8;  перем hostname: массив из симв8;  перем res: целМЗ );
	нач
		СТОП( 99 );	(* not implemented yet. needed ?? *)
		hostname[0] := 0X
	кон MailHostByDomain;

	(** Find the IP address of the specified host. *)

	проц HostByName*( конст hostname: массив из симв8;  перем addr: IP.Adr;  перем res: целМЗ );
	перем
		hostent: Hostent;
		firstaddrPtr: адресВПамяти;
	нач {единолично}
		hostent := gethostbyname( адресОт( hostname ) );
		если hostent # НУЛЬ то
			S.прочтиОбъектПоАдресу( hostent.addrlist, firstaddrPtr );
			если hostent.length = 4 то
				S.копируйПамять( firstaddrPtr, адресОт( addr.ipv4Adr ), 4 );
				addr.usedProtocol := IP.IPv4;
			иначе
				S.копируйПамять( firstaddrPtr, адресОт( addr.ipv6Adr ), 16 );
				addr.usedProtocol := IP.IPv6;
			всё;
			res := Ok
		иначе
			res := NotFound
		всё
	кон HostByName;

	(** Find the host name of the specified IP address. *)

	проц HostByNumber*( addr: IP.Adr;  перем hostname: массив из симв8;  перем res: целМЗ );
	перем
		hostent: Hostent;
	нач {единолично}
		если IP.IsNilAdr( addr ) то
			hostname[0] := 0X;  res := BadName;  возврат
		всё;
		если addr.usedProtocol = IP.IPv4 то
			hostent := gethostbyaddr( адресОт( addr.ipv4Adr ), 4, Unix.PFINET )
		иначе
			hostent := gethostbyaddr( адресОт( addr.ipv6Adr ), 16, Unix.PFINET6 )
		всё;
		если hostent # НУЛЬ то	(* err points to struct hostent *)
			копируйСтрокуДо0 (hostent.name^, hostname);
			res := Ok
		иначе
			res := NotFound
		всё
	кон HostByNumber;

	(* none portable, Unix ports only! *)
	проц GetHostName*( перем name: массив из симв8;  перем res: целМЗ );
	перем x: цел32;
	нач
		x := gethostname( адресОт( name ), длинаМассива( name ) );
		если x >= 0 то  res := Ok  иначе  res := NotFound  всё
	кон GetHostName;


	проц GetLocalDomain( перем dom: массив из симв8 );
	перем f: Files.File;  r: Files.Reader;  buf: массив 256 из симв8; ignore: булево;
	нач
		f := Files.Old( "/etc/resolv.conf" );
		если f # НУЛЬ то
			Files.OpenReader( r, f, 0 );
			нцПока r.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( buf ) и (buf # "domain") делай
				если buf[0] = "#" то  r.чСтроку8ДоКонцаСтрокиТекстаВключительно( buf )  всё	(* skip comment *)
			кц;
			если buf = "domain" то  ignore := r.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( buf );
			иначе  buf := "unknown.edu"
			всё
		иначе  buf := "unknown.edu"
		всё;
		копируйСтрокуДо0( buf, dom )
	кон GetLocalDomain;


нач
	GetLocalDomain( domain );
	NDNSReceived := 0;
	NDNSSent := 0;
	NDNSMismatchID := 0;
	NDNSError := 0;

	если Unix.Version = "Darwin" то
		nlib := Unix.libc
	иначе
		nlib := Unix.Dlopen( "libnsl.so.1", 2 );
		если nlib = 0 то  nlib := Unix.Dlopen( "libnsl.so", 2 )  всё;
		если nlib = 0 то
			Out.пСтроку8( "Unix.Dlopen( 'libnsl.so' ) failed" );  Out.пВК_ПС
		всё;
	всё;
	Unix.Dlsym( nlib, "gethostbyaddr", адресОт( gethostbyaddr ) );
	Unix.Dlsym( nlib, "gethostbyname", адресОт( gethostbyname ) );
	Unix.Dlsym( nlib, "gethostname", адресОт( gethostname ) );
кон DNS.



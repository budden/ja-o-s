(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль MathRe;   (** AUTHOR "adf"; PURPOSE "Real math functions"; *)

использует NbrInt, NbrRe, DataErrors, MathInt, MathRat, MathReSeries;

перем
	MaxFactorial-, maxIterations: NbrInt.Integer;
	delta, expInfinity, expNegligible, expZero, ln2, ln2Inv, ln10, ln10Inv, sqrtInfinity: NbrRe.Real;

тип
	ArcSinA = окласс (MathReSeries.Coefficient)

		проц {перекрыта}Evaluate*;
		перем i, k, index: NbrInt.Integer;  den, num: NbrRe.Real;
		нач {единолично}
			если NbrInt.Odd( n ) то
				num := 1;  den := 1;  k := n;
				нцДля i := k - 2 до 1 шаг -2 делай index := i;  num := num * index;  den := den * (index + 1) кц;
				coef := num / (n * den)
			иначе coef := 0
			всё;
			если n > maxIterations то eos := истина;  DataErrors.ReWarning( x, "Did not converge -  timed out." ) всё
		кон Evaluate;

	кон ArcSinA;

	ArcSinhA = окласс (MathReSeries.Coefficient)

		проц {перекрыта}Evaluate*;
		перем index: NbrInt.Integer;
		нач {единолично}
			если n = 0 то coef := 0
			аесли n = 1 то coef := 1 / x
			иначе
				если NbrInt.Odd( n ) то index := ((n - 2) * (n - 1)) иначе index := (n * (n - 1)) всё;
				coef := index * x
			всё;
			если n > maxIterations то eos := истина;  DataErrors.ReWarning( x, "Did not converge -  timed out." ) всё
		кон Evaluate;

	кон ArcSinhA;

	ArcSinhB = окласс (MathReSeries.Coefficient)

		проц {перекрыта}Evaluate*;
		нач {единолично}
			если n = 0 то coef := 0 иначе coef := (2 * n - 1) всё
		кон Evaluate;

	кон ArcSinhB;

	ArcTanhA = окласс (MathReSeries.Coefficient)

		проц {перекрыта}Evaluate*;
		перем index: NbrInt.Integer;
		нач {единолично}
			если n = 0 то coef := 0
			аесли n = 1 то coef := 1 / x
			иначе index := (-(n - 1) * (n - 1));  coef := index * x
			всё;
			если n > maxIterations то eos := истина;  DataErrors.ReWarning( x, "Did not converge -  timed out." ) всё
		кон Evaluate;

	кон ArcTanhA;

	ArcTanhB = окласс (MathReSeries.Coefficient)

		проц {перекрыта}Evaluate*;
		нач {единолично}
			если n = 0 то coef := 0 иначе coef := (2 * n - 1) всё
		кон Evaluate;

	кон ArcTanhB;

	TanA = окласс (MathReSeries.Coefficient)

		проц {перекрыта}Evaluate*;
		нач {единолично}
			если n = 0 то coef := 0
			аесли n = 1 то coef := 1
			иначе coef := -x
			всё;
			если n > maxIterations то eos := истина;  DataErrors.ReWarning( x, "Did not converge -  timed out." ) всё
		кон Evaluate;

	кон TanA;

	TanB = окласс (MathReSeries.Coefficient)

		проц {перекрыта}Evaluate*;
		нач {единолично}
			если n = 0 то coef := 0 иначе coef := (2 * n - 1) всё
		кон Evaluate;

	кон TanB;

	(**  h n i         G(n+1)
         |     | = ---------- ,  m 3 0
	 	j m k    m!G(n-m+1)
	*)
	проц Binomial*( top: NbrRe.Real;  bottom: NbrInt.Integer ): NbrRe.Real;
	(* Formula 6:3:1 of: J. Spanier and K. B. Oldham, An Atlas of Functions, Hemisphere Publishing Corp., Washington DC, 1987. *)
	перем i: NbrInt.Integer;  coef, prod: NbrRe.Real;
	нач
		если bottom < 0 то DataErrors.IntError( bottom, "Bottom parameter cannot be negative" );  prod := 0
		аесли bottom = 0 то prod := 1
		иначе
			i := bottom;  prod := 1;
			нцДо coef := (top - (bottom - i)) / i;  prod := coef * prod;  NbrInt.Dec( i ) кцПри i = 0
		всё;
		возврат prod
	кон Binomial;

(** Computes  n! = n * (n - 1) * (n - 2) * ... * 1,  MaxFactorial 3 n 3 0. *)
	проц Factorial*( n: NbrInt.Integer ): NbrRe.Real;
	перем x, n2, n4, n6, n8, nR: NbrRe.Real;
	нач
		если n < 0 то DataErrors.IntError( n, "Negative arguments are inadmissible." );  x := 0
		аесли n <= MathInt.MaxFactorial то x := MathInt.Factorial( n )
		аесли n <= MathRat.MaxFactorial то x := MathRat.Factorial( n )
		аесли n <= MaxFactorial то
			(* Use Stirling's approximation. *)
			nR := n;  n2 := nR * nR;  n4 := n2 * n2;  n6 := n2 * n4;  n8 := n4 * n4;
			x := (1 - 1 / (30 * n2) + 1 / (105 * n4) - 1 / (140 * n6) + 1 / (99 * n8)) / (12 * nR);
			x := Exp( x + nR * (Ln( nR ) - 1) );  x := x * Sqrt( 2 * NbrRe.Pi * nR )
		иначе DataErrors.IntError( n, "Argument is too large - overflow." );  x := 0
		всё;
		возврат x
	кон Factorial;

(** Returns a pseudo-random number  r  uniformly distributed over the unit interval, i.e.,  r N (0, 1). *)
	проц Random*( ): NbrRe.Real;
	перем x: NbrRe.Real;
	нач
		x := MathRat.Random();  возврат x
	кон Random;

(** Returns the Heaviside step function:   0 if x < x0,  1/2  if  x = x0,  and  1  if  x > x0. *)
	проц Step*( x, x0: NbrRe.Real ): NbrRe.Real;
	перем step: NbrRe.Real;
	нач
		если x < x0 то step := 0
		аесли x = x0 то step := 0.5
		иначе step := 1
		всё;
		возврат step
	кон Step;

(** Computes the square root. *)
	проц Sqrt*( x: NbrRe.Real ): NbrRe.Real;
	перем sqrt: NbrRe.Real;
	нач
		если x < 0 то DataErrors.ReError( x, "Argument cannot be negative." );  sqrt := 0
		аесли x = 0 то sqrt := 0
		иначе sqrt := NbrRe.Sqrt( x )
		всё;
		возврат sqrt
	кон Sqrt;

(** Computes the Pythagorean distance:  V(x2 + y2). *)
	проц Pythag*( x, y: NbrRe.Real ): NbrRe.Real;
	перем absx, absy, dist, ratio: NbrRe.Real;
	нач
		absx := NbrRe.Abs( x );  absy := NbrRe.Abs( y );
		если absx > absy то ratio := absy / absx;  dist := absx * Sqrt( 1 + ratio * ratio )
		аесли absy = 0 то dist := 0
		иначе ratio := absx / absy;  dist := absy * Sqrt( 1 + ratio * ratio )
		всё;
		возврат dist
	кон Pythag;

(** Computes  xn,  {x,n} 9 {0,0}. *)
	проц IntPower*( x: NbrRe.Real;  n: NbrInt.Integer ): NbrRe.Real;
	перем sign: NbrInt.Integer;  max, power: NbrRe.Real;
	нач
		sign := 1;
		если n = 0 то
			если x # 0 то power := 1 иначе DataErrors.Error( "Both argument and exponent cannot be zero." );  power := 1
			(* Sending an error message is ok, but if without stopping people in most cases do expect 0^0 =1 *)
			всё
		аесли x = 0 то
			если n > 0 то power := 0
			иначе DataErrors.IntError( n, "Exponent cannot be negative when argument is zero." );  power := 0;
			всё
		иначе
			если x < 0 то
				x := NbrRe.Abs( x );
				если NbrInt.Odd( n ) то sign := -1 всё
			всё;
			если n < 0 то x := 1 / x;  n := NbrInt.Abs( n ) всё;
			power := 1;
			нцПока n > 0 делай
				нцПока ~NbrInt.Odd( n ) и (n > 0) делай
					max := NbrRe.MaxNbr / x;
					если x > max то x := max;  n := 2;  DataErrors.Error( "Arithmatic overflow." ) всё;
					x := x * x;  n := n DIV 2
				кц;
				max := NbrRe.MaxNbr / power;
				если x > max то x := max;  n := 1;  DataErrors.Error( "Arithmatic overflow." ) всё;
				power := power * x;  NbrInt.Dec( n )
			кц
		всё;
		возврат sign * power
	кон IntPower;

(** Computes xy,  x 3 0,  {x,y} 9 {0,0} . *)
	проц Power*( x: NbrRe.Real;  y: NbrRe.Real ): NbrRe.Real;
	перем arg, power: NbrRe.Real;
	нач
		если x < 0 то DataErrors.ReError( x, "Argument cannot be negative." );  power := 0
		аесли x = 0 то
			power := 0;
			если y <= 0 то DataErrors.Error( "Exponent must be positive when argument is zero." ) всё
		аесли ( x = 1 ) или ( y = 0 ) то power := 1
		иначе arg := y * Ln( x );  power := Exp( arg )
		всё;
		возврат power
	кон Power;

(** Computes ex *)
	проц Exp*( x: NbrRe.Real ): NbrRe.Real;
	перем exp: NbrRe.Real;
	нач
		если x > expInfinity то DataErrors.ReError( x, "Argument is too large." );  exp := NbrRe.MaxNbr
		аесли x < expZero то exp := 0
		иначе exp := NbrRe.Exp( x )
		всё;
		возврат exp
	кон Exp;

(** Computes 2x *)
	проц Exp2*( x: NbrRe.Real ): NbrRe.Real;
	перем exp, xLn2: NbrRe.Real;
	нач
		xLn2 := x * ln2;
		если xLn2 > expInfinity то DataErrors.ReError( x, "Argument is too large." );  exp := NbrRe.MaxNbr
		аесли xLn2 < expZero то exp := 0
		иначе exp := NbrRe.Exp( xLn2 )
		всё;
		возврат exp
	кон Exp2;

(** Computes 10x *)
	проц Exp10*( x: NbrRe.Real ): NbrRe.Real;
	перем exp, xLn10: NbrRe.Real;
	нач
		xLn10 := x * ln10;
		если xLn10 > expInfinity то DataErrors.ReError( x, "Argument is too large." );  exp := NbrRe.MaxNbr
		аесли xLn10 < expZero то exp := 0
		иначе exp := NbrRe.Exp( xLn10 )
		всё;
		возврат exp
	кон Exp10;

(** Computes Loge(x) - the natural log. *)
	проц Ln*( x: NbrRe.Real ): NbrRe.Real;
	перем ln: NbrRe.Real;
	нач
		если x > 0 то ln := NbrRe.Ln( x ) иначе DataErrors.ReError( x, "Argument must be positive." );  ln := 0 всё;
		возврат ln
	кон Ln;

(** Computes Log2(x) - log base 2. *)
	проц Log2*( x: NbrRe.Real ): NbrRe.Real;
	перем ln, log: NbrRe.Real;
	нач
		если x > 0 то ln := NbrRe.Ln( x );  log := ln2Inv * ln
		иначе DataErrors.ReError( x, "Argument must be positive." );  log := 0
		всё;
		возврат log
	кон Log2;

(** Computes Log10(x) - log base 10. *)
	проц Log*( x: NbrRe.Real ): NbrRe.Real;
	перем ln, log: NbrRe.Real;
	нач
		если x > 0 то ln := NbrRe.Ln( x );  log := ln10Inv * ln
		иначе DataErrors.ReError( x, "Argument must be positive." );  log := 0
		всё;
		возврат log
	кон Log;

(** Triganometric Functions *)
	проц Sin*( x: NbrRe.Real ): NbrRe.Real;
	нач
		возврат NbrRe.Sin( x )
	кон Sin;

	проц Cos*( x: NbrRe.Real ): NbrRe.Real;
	нач
		возврат NbrRe.Cos( x )
	кон Cos;

	проц Tan*( x: NbrRe.Real ): NbrRe.Real;
	перем cos, sin, tan: NbrRe.Real;  a: TanA;  b: TanB;
	нач
		если NbrRe.Abs( x ) < delta то нов( a );  нов( b );  tan := MathReSeries.ContinuedFraction( a, b, x )
		иначе
			cos := Cos( x );  sin := Sin( x );
			если cos # 0 то tan := sin / cos
			иначе
				DataErrors.ReError( x, "Division by zero, i.e., the cos(x) = 0." );
				если sin > 0 то tan := NbrRe.MaxNbr иначе tan := NbrRe.MinNbr всё
			всё
		всё;
		возврат tan
	кон Tan;

	проц ArcSin*( x: NbrRe.Real ): NbrRe.Real;
	(* Returns the arcus sine of 'x' in the range [-p/2, p/2] where -1 <= x <= 1 *)
	перем a: ArcSinA;  n, d, abs, arcsin: NbrRe.Real;
	нач
		abs := NbrRe.Abs( x );
		если abs > 1 то DataErrors.ReError( x, "Argument is outside the admissible range:  -1 <= x <= 1." );  arcsin := 0
		аесли abs = 1 то n := x;  d := 0;  arcsin := ArcTan2( n, d )
		аесли abs > delta то n := x;  d := Sqrt( 1 - x * x );  arcsin := ArcTan2( n, d )
		иначе нов( a );  arcsin := MathReSeries.PowerSeries( a, x )
		всё;
		возврат arcsin
	кон ArcSin;

	проц ArcCos*( x: NbrRe.Real ): NbrRe.Real;
	(* Returns the arcus cosine of 'x' in the range [0, p] where -1 <= x <= 1 *)
	перем n, d, abs, arccos: NbrRe.Real;
	нач
		abs := NbrRe.Abs( x );
		если abs > 1 то DataErrors.ReError( x, "Argument is outside the admissible range:  -1 <= x <= 1." );  arccos := 0
		аесли abs = 1 то n := 0;  d := x;  arccos := ArcTan2( n, d )
		иначе n := Sqrt( 1 - x * x );  d := x;  arccos := ArcTan2( n, d )
		всё;
		возврат arccos
	кон ArcCos;

	проц ArcTan*( x: NbrRe.Real ): NbrRe.Real;
	нач
		возврат NbrRe.ArcTan( x )
	кон ArcTan;

	проц ArcTan2*( xn, xd: NbrRe.Real ): NbrRe.Real;   (** Quadrant-correct arcus tangent: atan(xn/xd). *)
	перем atan: NbrRe.Real;
	нач
		если xd = 0 то
			если xn # 0 то atan := NbrRe.Sign( xn ) * NbrRe.Pi / 2
			иначе DataErrors.Error( "Both arguments cannot be zero." );  atan := 0
			всё
		аесли xn = 0 то atan := (1 - NbrRe.Sign( xd )) * NbrRe.Pi / 2
		иначе atan := NbrRe.ArcTan( xn / xd ) + NbrRe.Sign( xn ) * (1 - NbrRe.Sign( xd )) * NbrRe.Pi / 2
		всё;
		возврат atan
	кон ArcTan2;

(** Hyperbolic Functions *)
	проц Sinh*( x: NbrRe.Real ): NbrRe.Real;
	перем abs, expM1, sinh: NbrRe.Real;
	нач
		abs := NbrRe.Abs( x );
		если abs < 1 то expM1 := Exp( abs ) - 1;  sinh := NbrRe.Sign( x ) * (expM1 + expM1 / (1 + expM1)) / 2
		аесли abs < expNegligible то sinh := (Exp( x ) - Exp( -x )) / 2
		иначе sinh := NbrRe.Sign( x ) * Exp( abs ) / 2
		всё;
		возврат sinh
	кон Sinh;

	проц Cosh*( x: NbrRe.Real ): NbrRe.Real;
	перем abs, cosh: NbrRe.Real;
	нач
		abs := NbrRe.Abs( x );
		если abs < expNegligible то cosh := (Exp( x ) + Exp( -x )) / 2 иначе cosh := Exp( abs ) / 2 всё;
		возврат cosh
	кон Cosh;

	проц Tanh*( x: NbrRe.Real ): NbrRe.Real;
	перем abs, exp, expM, exp2xM1, tanh: NbrRe.Real;
	нач
		abs := NbrRe.Abs( x );
		если abs < 1 то exp2xM1 := Exp( 2 * abs ) - 1;  tanh := NbrRe.Sign( x ) * exp2xM1 / (2 + exp2xM1)
		аесли abs < expNegligible то exp := Exp( x );  expM := Exp( -x );  tanh := (exp - expM) / (exp + expM)
		иначе tanh := NbrRe.Sign( x )
		всё;
		возврат tanh
	кон Tanh;

	проц ArcSinh*( x: NbrRe.Real ): NbrRe.Real;
	(* ArcSinh(x) is the arcus hyperbolic sine of 'x'. *)
	перем abs, asinh: NbrRe.Real;  a: ArcSinhA;  b: ArcSinhB;
	нач
		abs := NbrRe.Abs( x );
		если abs > sqrtInfinity то DataErrors.ReError( x, "Argument is too large." );  asinh := 0
		аесли x < -delta то asinh := -Ln( abs + Sqrt( abs ) * Sqrt( abs + 1 / abs ) )
		аесли x < delta то
			если x = 0 то asinh := 0
			иначе нов( a );  нов( b );  asinh := x * Sqrt( 1 + x * x ) * MathReSeries.ContinuedFraction( a, b, x )
			всё
		иначе asinh := Ln( x + Sqrt( x ) * Sqrt( x + 1 / x ) )
		всё;
		возврат asinh
	кон ArcSinh;

	проц ArcCosh*( x: NbrRe.Real ): NbrRe.Real;
	(* ArcCosh(x) is the arcus hyperbolic cosine of 'x'.
			All arguments greater than or equal to 1 are legal. *)
	перем acosh: NbrRe.Real;
	нач
		если x < 1 то DataErrors.ReError( x, "Argument is out of range, i.e., x >= 1." );  acosh := 0
		аесли x = 1 то acosh := 0
		аесли x < sqrtInfinity то acosh := Ln( x + Sqrt( x ) * Sqrt( x - 1 / x ) )
		иначе DataErrors.ReError( x, "Argument is too large." );  acosh := 0
		всё;
		возврат acosh
	кон ArcCosh;

	проц ArcTanh*( x: NbrRe.Real ): NbrRe.Real;
	(* ArcTanh(x) is the arcus hyperbolic tangent of 'x'. *)
	перем abs, atanh: NbrRe.Real;  a: ArcTanhA;  b: ArcTanhB;
	нач
		abs := NbrRe.Abs( x );
		если abs > 1 то DataErrors.ReError( x, "Argument is out of range, i.e., -1 <= x <= 1." );  atanh := 0
		аесли abs = 1 то atanh := NbrRe.Sign( x ) * expNegligible
		аесли abs > delta то atanh := Ln( (1 + x) / (1 - x) ) / 2
		аесли abs > 0 то нов( a );  нов( b );  atanh := x * MathReSeries.ContinuedFraction( a, b, x )
		иначе atanh := 0
		всё;
		возврат atanh
	кон ArcTanh;

нач
	если NbrRe.MaxNbr = матМаксимум( вещ32 ) то MaxFactorial := 34 иначе MaxFactorial := 171 всё;
	maxIterations := 1000;  expZero := Ln( 1/NbrRe.MaxNbr );  expInfinity := Ln( NbrRe.MaxNbr );
	expNegligible := -Ln( NbrRe.Epsilon ) / 2;  sqrtInfinity := Sqrt( NbrRe.MaxNbr ) / 2;  delta := 0.1;
	ln10 := NbrRe.Ln( 10 );  ln10Inv := 1 / ln10;  ln2 := NbrRe.Ln( 2 );  ln2Inv := 1 / ln2
кон MathRe.

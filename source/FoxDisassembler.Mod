модуль FoxDisassembler; (** AUTHOR ""; PURPOSE ""; *)

использует Потоки, ObjectFile, Scanner := FoxScanner, Basic := FoxBasic, BitSets, D := Debugging, Files, Commands;

конст Trace = ложь;
тип
	Unit* = ObjectFile.Unit;

	Block*= укль на запись (ObjectFile.Section)
		to-: Unit;
		next-: Block;
	кон;

	Disassembler* = окласс
	перем
		first, block: Block; w: Потоки.Писарь; stringWriter: Потоки.ПисарьВСтроку8ФиксированногоРазмера; code,data: BitSets.BitSet; codeUnit, dataUnit: ObjectFile.Bits;
		codeDisplacement-, dataDisplacement: Unit;

		проц & Init*(l0w: Потоки.Писарь);
		нач сам.w := l0w; нов(stringWriter, 256);
			codeDisplacement := 0; dataDisplacement := 0;

		кон Init;

		проц GetLogFile*(конст binaryFileName: массив из симв8): Files.File;
		перем fileName,extension: Files.FileName;
		нач
			Files.SplitExtension(binaryFileName, fileName, extension);
			Files.JoinExtension(fileName, "log", fileName);
			возврат Files.Old(fileName)
		кон GetLogFile;

		проц BlockHeader(l1block: Block);
		перем name: ObjectFile.SectionName;
		нач
			Basic.SegmentedNameToString(l1block.identifier.name, name);
			w.пСтроку8("-------- "); w.пСимв8(Scanner.TAB);
			если  ObjectFile.IsCode(l1block.type) то w.пСтроку8("code");
			иначе w.пСтроку8("data");
			всё;
			w.пСимв8(Scanner.TAB);
			w.пСтроку8(name);
			w.пСтроку8(" @");
			w.п16ричное(l1block.alignment,-8); (*w.String("-"); w.Hex(block.to,-8);*)
			w.пСтроку8(" ---");
			w.пВК_ПС;
		кон BlockHeader;

		проц WriteReference*(adr: Unit; isCode: булево; l2w: Потоки.Писарь);
		перем b: Block; name: ObjectFile.SectionName; offset: Unit;
		нач
			l2w.пСтроку8(" --> "); l2w.п16ричное(adr+codeDisplacement,-8);
			b := first; если b = НУЛЬ то возврат всё;
			если isCode то offset := codeDisplacement иначе offset := dataDisplacement всё;
			нцПока (b # НУЛЬ) и ((adr < b.alignment-offset) или (adr > b.to - offset) или (ObjectFile.IsCode(b.type)#isCode)) делай
				b := b.next;
			кц;
			если b = НУЛЬ то
				(* try to find any matching section *)
				b := first;
				нцПока (b # НУЛЬ) и ((adr < b.alignment-offset) или (adr > b.to - offset))  делай
					b := b.next;
				кц;
			всё;
			если (b # НУЛЬ) и (b # block) то
				Basic.SegmentedNameToString(b.identifier.name, name);
				l2w.пСтроку8(" [");
				l2w.пСтроку8(name);
				если adr # b.alignment то
					l2w.пСтроку8("+"); l2w.пЦел64(adr-b.alignment+offset,1)
				всё;
				l2w.пСтроку8("]");
			всё;
		кон WriteReference;

		проц DisassembleInstruction*(bitset: BitSets.BitSet; перем address: Unit; maxInstructionSize: Unit; w: Потоки.Писарь);
		нач
		кон DisassembleInstruction;

		проц DisassembleBlock(from, to: Unit);
		перем adr, prevadr, max: Unit; value: целМЗ; string: массив 256 из симв8;
		нач
			если code = НУЛЬ то возврат всё;
			adr := from;
			max := матМинимум(to+1, code.GetSize() DIV codeUnit);
			нцПока adr < max делай
				(* adr *)
				w.п16ричное(adr+codeDisplacement,-8); w.пСтроку8(": ");
				prevadr := adr;
				DisassembleInstruction(code, adr, max-adr, stringWriter);
				если prevadr = adr то w.пСтроку8("decoder error: address must increase"); w.пВК_ПС; возврат всё;
				stringWriter.ПротолкниБуферВПоток;
				stringWriter.ДайПрочитанное˛сколькоПоместитсяИСимвол0(string);
				(* value *)
				нцПока prevadr < adr делай
					value := code.GetBits(prevadr*codeUnit, codeUnit);
					w.п16ричное(value,-((codeUnit-1) DIV 4 +1)); w.пСтроку8(" ");
					увел(prevadr);
				кц;
				(* instruction string *)
				w.пСимв8(Scanner.TAB); w.пСтроку8(string); w.пВК_ПС;
			кц;
		кон DisassembleBlock;

		проц DataBlock(from, to: Unit);
		перем adr,width,max: Unit; value: целМЗ;
		нач
			если data = НУЛЬ то возврат всё;
			adr := from;
			max := матМинимум(to+1, data.GetSize() DIV dataUnit);
			нцПока adr < max делай
				w.п16ричное(adr+dataDisplacement,-8); w.пСтроку8(": ");
				width := 8;
				нцПока (adr < max) и (width > 0) делай
					value := data.GetBits(adr*dataUnit, dataUnit);
					w.п16ричное(value,-((dataUnit-1) DIV 4 +1)); w.пСтроку8(" ");
					увел(adr); умень(width);
				кц;
				w.пВК_ПС;
			кц;
		кон DataBlock;

		проц ParseLogFile*(file: Files.File): Block;
		перем reader: Files.Reader; newline: булево; sectionName: ObjectFile.SectionName; scanner: Scanner.AssemblerScanner;
			token: Scanner.Лексема; b: булево; l3block: Block; l4first: Block; last: Block;

			проц GetNextToken;
			перем l5b: булево;
			нач l5b := scanner.ДочитайЛексему(token)
			кон GetNextToken;

			проц ExpectToken(symbol: Scanner.ВидЛексемы): булево;
			нач если token.видЛексемы = symbol то GetNextToken; возврат истина иначе возврат ложь всё;
			кон ExpectToken;

			проц ExpectIdentifier(перем name: массив из симв8): булево;
			нач
				если (token.видЛексемы = Scanner.Идентификатор) то копируйСтрокуДо0(token.строкаИдентификатора,name); GetNextToken; возврат истина
				иначе возврат ложь
				всё;
			кон ExpectIdentifier;

			проц ExpectThisIdentifier(конст name: массив из симв8): булево;
			нач
				если (token.видЛексемы = Scanner.Идентификатор) и (token.строкаИдентификатора = name) то GetNextToken; возврат истина
				иначе возврат ложь
				всё;
			кон ExpectThisIdentifier;

			проц ExpectNumber(перем int: Unit):булево;
			нач
				если (token.видЛексемы = Scanner.Число) то
					int := token.integer(Unit); GetNextToken; возврат истина
				иначе возврат ложь
				всё;
			кон ExpectNumber;

			проц ParseLine(): Block;
			перем from,to: Unit; l6block: Block; displacement: Unit;
			нач
				l6block := НУЛЬ;
				если ExpectNumber(from) и ExpectToken(Scanner.Двоеточие) то
					если ExpectThisIdentifier("code") и ExpectIdentifier(sectionName) и  ExpectThisIdentifier("to") и ExpectNumber(to) то
						нов(l6block); l6block.type := ObjectFile.Code; Basic.ToSegmentedName(sectionName, l6block.identifier.name); l6block.alignment := from; l6block.fixed := истина; l6block.to := to;
					аесли ExpectThisIdentifier("data") и ExpectIdentifier(sectionName) и  ExpectThisIdentifier("to") и ExpectNumber(to) то
						нов(l6block); l6block.type := ObjectFile.Data; Basic.ToSegmentedName(sectionName, l6block.identifier.name); l6block.alignment := from; l6block.fixed := истина; l6block.to := to;
					всё;
				аесли ExpectThisIdentifier("code") и ExpectThisIdentifier("displacement") и ExpectNumber(displacement) то
					codeDisplacement := displacement; dataDisplacement := displacement
				аесли ExpectThisIdentifier("data") и ExpectThisIdentifier("displacement") и ExpectNumber(displacement) то
					dataDisplacement := displacement;
				иначе scanner.SkipToEndOfLine; GetNextToken;	scanner.ResetError;
				всё;
				если (l6block # НУЛЬ) и Trace то
					D.String("found section ");
					D.String(sectionName);
					если  ObjectFile.IsCode(l6block.type) то D.String(" (code) ") иначе D.String(" (data) ") всё;
					D.Int(l6block.alignment,1); D.String(" "); D.Int(l6block.to,1);
					D.Ln;
				всё;
				возврат l6block
			кон ParseLine;

		нач
			l4first := НУЛЬ; last := НУЛЬ;
			если file = НУЛЬ то возврат НУЛЬ всё;
			нов(reader, file, 0);
			scanner := Scanner.NewAssemblerScanner("",reader,0,НУЛЬ);
			b := scanner.ДочитайЛексему(token);
			нцДо
				l3block := ParseLine();
				если l3block # НУЛЬ то
					если l4first = НУЛЬ то l4first := l3block; last := l3block иначе last.next := l3block; last := l3block всё;
				всё;
				GetNextToken
			кцПри token.видЛексемы = Scanner.КонецТекста;
			возврат l4first;
		кон ParseLogFile;

		проц Disassemble*(l7code, l8data: BitSets.BitSet; l9codeUnit, l10dataUnit: ObjectFile.Bits; logFile: Files.File; address: адресВПамяти);
		нач
			сам.code := l7code;сам.data := l8data; сам.codeUnit := l9codeUnit; сам.dataUnit := l10dataUnit;
			first := ParseLogFile(logFile); block := first;
			если block = НУЛЬ то
				w.пСтроку8("------ code ------"); w.пВК_ПС;
				DisassembleBlock(0, матМаксимум(цел32)-1);
				если l7code # l8data то
					w.пСтроку8("------ data ------"); w.пВК_ПС;
					DataBlock(0, матМаксимум(цел32)-1);
				всё;
			иначе
				нцПока block # НУЛЬ делай
					если (address = 0) или (block.alignment <= address) и (block.to > address) то
						BlockHeader(block);
						если ObjectFile.IsCode(block.type) и (l7code # НУЛЬ) то
							DisassembleBlock(block.alignment-codeDisplacement, block.to-codeDisplacement);
						иначе
							DataBlock(block.alignment-dataDisplacement, block.to-dataDisplacement);
						всё;
					всё;
					block := block.next
				кц;
			всё;
		кон Disassemble;

		проц SetDisplacements*(l11code, l12data: Unit);
		нач
			codeDisplacement := l11code;
			dataDisplacement := l12data;
		кон SetDisplacements;

	кон Disassembler;

	проц FindPC*(context: Commands.Context);
	перем file: Files.File; logFile: Files.FileName; adr,maxadr: Unit; disassembler: Disassembler; block, found: Block; name: ObjectFile.SectionName;
	нач
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(logFile) и context.arg.ПропустиБелоеПолеИЧитайРазмерМЗ(adr, истина) то
			file := Files.Old(logFile);
			если file = НУЛЬ то Files.JoinExtension(logFile,".log",logFile); file := Files.Old(logFile) всё;
			если file = НУЛЬ то
				context.error.пСтроку8("file not found "); context.error.пСтроку8(logFile); context.error.пВК_ПС
			иначе
				нов(disassembler, context.out);
				maxadr := 0;
				block := disassembler.ParseLogFile(file);
				нцПока (block # НУЛЬ ) делай
					если (block.alignment < adr) и (block.alignment > maxadr) и ObjectFile.IsCode(block.type) то
						found := block;
						maxadr := block.alignment
					всё;
					block := block.next
				кц;
				Basic.SegmentedNameToString(found.identifier.name, name);
				context.out.пСтроку8(name); context.out.пСтроку8(":"); context.out.пЦел64(adr-found.alignment,1); context.out.пВК_ПС;
			всё;
		всё;
	кон FindPC;

	(*
	PROCEDURE Test*(context: Commands.Context);
	VAR filename: Files.FileName; name: ObjectFile.SectionName; block: Block;
	BEGIN
		IF context.arg.GetString(filename) THEN
			file := Files.Old(filename);
			IF file = NIL THEN (* error *) HALT(100) END;
			block := ParseLogFile(filename);
			WHILE block # NIL DO
				Basic.SegmentedNameToString(block.identifier.name, name);
				context.out.String(name); context.out.String(" at "); context.out.Int(block.alignment,1); context.out.Ln;
				block := block.next;
			END;
		END;
	END Test;
	*)

кон FoxDisassembler.

System.FreeDownTo FoxDisassembler ~
FoxDisassembler.Test ins.log ~

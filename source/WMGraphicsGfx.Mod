модуль WMGraphicsGfx; (** AUTHOR "Patrick Hunziker"; PURPOSE "use rich Gfx Business logic for WMWindowManager.Canvas"; *)

использует Gfx, GfxBuffer, GfxRaster, GfxMatrix, GfxRegions, Raster, Строки8, WMGraphics, WMWindowManager, ЛогЯдра;

тип Canvas*= окласс(WMGraphics.BufferCanvas)
	перем
		gfxContext-: GfxBuffer.Context;
		fillColor: цел32;
		gfxStrokeColor: Gfx.Color;
		gfxFillColor: Gfx.Color;
		gfxLineWidth: вещ32;
		dashLength: вещ32;

		проц &{перекрыта}New*(img : Raster.Image);
		нач
			New^(img);
			generator:=Строки8.ЯвиУСтроку("WMGraphicsGfx.GenCanvas");
			SetupDrawing;
		кон New;

		проц SetupDrawing;
		перем m: GfxMatrix.Matrix;
		нач
			нов(gfxContext);
			GfxBuffer.Init(gfxContext, img(*GetImage()*));
			(*
			GfxBuffer.SetCompOp(gfxContext, Raster.srcOverDst);
			GfxRaster.ResetClip(gfxContext);
			GfxRegions.SetToRect(gfxContext.clipReg, SHORT(clipRect.l), SHORT(clipRect.t), SHORT(clipRect.r), SHORT(clipRect.b));
			GfxMatrix.Init(m,1,0,0,1,dx,dy); (* one to one coordinate system, translated by the component's origin *)
			Gfx.SetCTM(gfxContext,m);
			*)
			SetDashLength(0);
			SetLineWidth(1.0);
			SetColor(WMGraphics.Blue);
			SetFillColor(WMGraphics.Red);
		кон SetupDrawing;

		проц {перекрыта}SetLineWidth*(w:вещ32);
		нач
			gfxLineWidth:=w;
			Gfx.SetLineWidth(gfxContext,w);
		кон SetLineWidth;

		(* set line color*)
		проц {перекрыта}SetColor*(color:WMGraphics.Color);
		нач
			если color#сам.color то
				SetColor^(color);
				gfxStrokeColor:=ColorToGfxColor(color);
				Gfx.SetStrokeColor(gfxContext, gfxStrokeColor);
			всё;
		кон SetColor;

		(* set line color*)
		проц SetDashLength*(dl:вещ32);
		перем on,off: массив 1 из вещ32;
		нач
			dashLength:=dl;
			если dashLength > 0 то
				on[0] := dashLength; off[0] := dashLength; Gfx.SetDashPattern(gfxContext, on, off, 1, 0);
			всё;
		кон SetDashLength;

		(* set fill/font color*)
		проц SetFillColor*(fillColor:цел32);
		нач
			если fillColor#сам.fillColor то
				сам.fillColor := fillColor;
				gfxFillColor:=ColorToGfxColor(fillColor);
				Gfx.SetFillColor(gfxContext, gfxFillColor);
			всё;
		кон SetFillColor;

		проц ColorToGfxColor(color:WMGraphics.Color):Gfx.Color;
			перем gfxcolor:Gfx.Color;
		нач
			gfxcolor.a := цел16(color остОтДеленияНа 100H);
			gfxcolor.b := цел16(color DIV 100H остОтДеленияНа 100H);
			gfxcolor.g := цел16(color DIV 10000H остОтДеленияНа 100H);
			gfxcolor.r := цел16(color DIV 1000000H остОтДеленияНа 100H);
			возврат gfxcolor
		кон ColorToGfxColor;

		проц GfxColorToColor(gfxColor:Gfx.Color):WMGraphics.Color;
		нач
			возврат цел32 (((цел64(gfxColor.r*100H)+gfxColor.g)*100H+gfxColor.b)*100H+gfxColor.a)
		кон GfxColorToColor;

		(* ????
		PROCEDURE SetFont*(f: WMGraphics.Font);
		BEGIN
			SetFont^(f);
			Gfx.SetFontName(gfxContext, f.name, SHORT(f.size));
		END SetFont;
		*)

		проц {перекрыта}DrawString*(x, y: размерМЗ; конст text : массив из симв8);
		нач
			утв(font#НУЛЬ);
			Gfx.DrawStringAt(gfxContext, x, y, text);
		кон DrawString;

		проц {перекрыта}Line*(x0, y0, x1, y1 : размерМЗ; lineColor : WMGraphics.Color; mode : целМЗ); (*this is a heavy duty procedure that can be called millions of times in time-varying or scrolled graphs - optimize*)
		нач
			если lineColor # color то
				SetColor(lineColor);
			всё;
			Gfx.DrawLine(gfxContext, dx+x0, dy+y0, dx+x1, dy+y1, {Gfx.Stroke}); (*may be refined by plotting to upper/lower boundary*)
			(* Performance issue: Gfx.DrawLine expects FLOAT32 coordinates;	inside of Gfx a  coordinate transformation and conversion from FLOAT32 to Integer pixel position will happen again *)
		кон Line;

		(** draw circle in requested mode (clockwise if r > 0, counterclockwise if r < 0) **)
		проц Circle(x,y,r:цел32); (*tentative -this method will be changed*)
		нач
			Gfx.DrawCircle(gfxContext, x,y,r, {Gfx.Stroke});
		кон Circle;

		(*to be extended by various Gfx specific drawing primitives*)

	кон Canvas;

проц GenCanvas*(img:Raster.Image):WMGraphics.BufferCanvas; (* generator procedure *)
перем c:Canvas;
нач
	нов(c,img); возврат c (* img is NIL, needs a call of c.New(img) later on *)
кон GenCanvas;

проц Test*; (* will be removed *)
перем w:WMWindowManager.BufferWindow; c: WMGraphics.BufferCanvas;
нач
	нов(w, 300,400, ложь);
	w.SetCanvasGenerator(GenCanvas);
	c:=w.canvas;

	c.Fill(w.bounds, WMGraphics.Red, WMGraphics.ModeCopy);

	c(Canvas).SetDashLength(10);
	c.SetLineWidth(1);
	c.Line(50,50,100,100, WMGraphics.Blue, WMGraphics.ModeCopy);
	c.SetLineWidth(3);
	c.Line(50,50,120,110, WMGraphics.Blue, WMGraphics.ModeCopy);

	c.SetPixel (50,30, WMGraphics.White, WMGraphics.ModeCopy);

	c(Canvas).SetFillColor(WMGraphics.Yellow);
	c.DrawString(60,40,"Hello World");

	c.SetColor(WMGraphics.Black);
	c.SetLineWidth(3);
	c(Canvas).Circle(150,200, 100);

	WMWindowManager.DefaultAddWindow(w);
	w.Invalidate(w.bounds);
кон Test;

кон WMGraphicsGfx.

WMGraphicsGfx.Test ~
System.FreeDownTo WMGraphicsGfx ~


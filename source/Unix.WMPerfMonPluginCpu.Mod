модуль WMPerfMonPluginCpu; (** AUTHOR "staubesv"; PURPOSE "Performance Monitor CPU load plugin"; *)

использует
	WMPerfMonPlugins, Modules;

конст

	ModuleName = "WMPerfMonPluginCpu";

тип

	(* Dummy parameter for compatiblity only *)
	CpuParameter* = укль на запись (WMPerfMonPlugins.Parameter)
		processorID* : размерМЗ;
	кон;

	CpuLoad* = окласс(WMPerfMonPlugins.Plugin);

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		нач
			p.name := "CPU Load (not implemented)"; p.description := "CPU Load (not yet implemented)";
			p.modulename := ModuleName;
			p.autoMin := истина; p.autoMax := истина; p.minDigits := 2;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		нач
			dataset[0] := 0;
		кон UpdateDataset;

	кон CpuLoad;

перем
	nbrOfCpus- : цел32;

проц InitPlugins;
перем cpuLoad : CpuLoad; par : WMPerfMonPlugins.Parameter;
нач
	нов(par); нов(cpuLoad, par);
кон InitPlugins;

проц Install*;
кон Install;

проц Cleanup;
нач
	WMPerfMonPlugins.updater.RemoveByModuleName(ModuleName);
кон Cleanup;

нач
	nbrOfCpus := 1;
	InitPlugins;
	Modules.InstallTermHandler(Cleanup);
кон WMPerfMonPluginCpu.

WMPerfMonPluginCpu.Install ~ 	System.Free WMPerfMonPluginCpu ~

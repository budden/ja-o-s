(* ported version of Minos to work with the ARM backend of the Fox Compiler Suite *)
модуль Кучи; (* originally called "MAU" *) (* Memory Allocation Unit; NW 15.12.2007*)
(* These procedures must remain in this order!*)

(*
	001 2007-07-03 tt: Added this header and formatted Module
	002 2007-09-04 tt: Added status info
*)

использует НИЗКОУР, Board, Memory, Трассировка;

конст
	Initialize = Board.InitializeHeap;

перем 
	heapStart	: адресВПамяти;	
	heap		: адресВПамяти;   	(*origin of free space*)
	heapEnd	: адресВПамяти;

	проц New*( перем p: цел32;  T: цел32 );   (*1*)
	(*allocate record, add tag field of 1 word with offset -4*)
	перем i, size: цел32;  
	нач
		p := heap + 4;
		НИЗКОУР.запишиОбъектПоАдресу( heap, T );   (*adr of type descriptor (tag) to tagfield of new record*)
		НИЗКОУР.прочтиОбъектПоАдресу( T, size );   (*obtain record size from type descriptor*)
		если size остОтДеленияНа 4 # 0 то увел(size, 4 - size остОтДеленияНа 4) всё;
		heap := p + size;
		(* Clear heap *)
		если Initialize то Memory.Fill8(p, size, 0X) всё;
		утв(heap < heapEnd);
		утв(heapStart <= heap);
	кон New; 
	
	проц AllocH*(перем a: цел32; len, elsize: цел32);  (*2*)
		(*allocate open array on heap, prefix with size field of 1 word with offset -4*)
		перем i, adr, size: цел32;
	нач
		size := len * elsize + 4; adr := адресОт(a);
		НИЗКОУР.запишиОбъектПоАдресу(adr, heap - 8);   (*address of array into descriptor*)
		НИЗКОУР.запишиОбъектПоАдресу(adr-4, len);   (*length of array into descriptor*)
		НИЗКОУР.запишиОбъектПоАдресу(heap, size);   (*size of block into header*)
		если size остОтДеленияНа 4 # 0 то увел(size, 4 - size остОтДеленияНа 4) всё;
		если Initialize то Memory.Fill8(heap + 4, len * elsize, 0X) всё;
		heap := heap + size;

		утв(heap < heapEnd);
		утв(heapStart <= heap);
	кон AllocH;

	проц AllocS*(перем a: цел32; len, elsize: цел32);  (*3*)
		(*allocate open array on stack*)
		перем adr: цел32;
	нач adr := адресОт(a); НИЗКОУР.SETSP(НИЗКОУР.SP() - len * elsize);
		НИЗКОУР.запишиОбъектПоАдресу(adr, НИЗКОУР.SP());   (*address of array into descriptor*)
		НИЗКОУР.запишиОбъектПоАдресу(adr-4, len)   (*length of array into descriptor*)
	кон AllocS;
	
	проц Alloc*( перем adr: цел32;  size: цел32 );
	(*allocate area from free space*)
	перем
		i: цел32;
	нач
		если size остОтДеленияНа 4 # 0 то увел(size, 4 - size остОтДеленияНа 4) всё;
		adr := heap;
		если Initialize то Memory.Fill8(adr, size, 0X) всё;
		(*INC (size, 4);*)
		(*SYSTEM.PUT (heap, size);*)
		heap := heap + size;
		утв(heap < heapEnd);
		утв(heapStart <= heap);
	кон Alloc;  
	
	проц HeapSize*(): цел32;
	нач
		возврат heapEnd - heapStart
	кон HeapSize;
	
	проц Free*(): цел32;
	нач
		возврат heapEnd - heap
	кон Free;
	
	(* PROCEDURES THAT ARE USED BY THE FOX COMPILER *)
	
	(** NewSys - Implementation of SYSTEM.NEW. **)
	проц НовОбъект˛неТребующийОметания*(перем pointer: динамическиТипизированныйУкль; Разм: размерМЗ; реальногоВремениЛи¿: булево);
	перем
		pointerAsInteger: цел32;
	нач	
		Alloc(pointerAsInteger, Разм);
		pointer := НИЗКОУР.подмениТипЗначения(динамическиТипизированныйУкль, pointerAsInteger)
	кон НовОбъект˛неТребующийОметания;

	(** NewRec - Implementation of NEW with a record.
	- this is essentially a wrapper that calls New(...) **)
	проц НовЗапись*(перем pointer: динамическиТипизированныйУкль; typeTag: адресВПамяти; реальногоВремениЛи¿: булево);
	перем
		pointerAsInteger: цел32;
	нач
		New(pointerAsInteger, НИЗКОУР.подмениТипЗначения(цел32, typeTag));
		pointer := НИЗКОУР.подмениТипЗначения(динамическиТипизированныйУкль, pointerAsInteger)
	кон НовЗапись;

	(** NewArr - Implementation of NEW with an array containing pointers. *)
	проц НовМассив˛требующийОметания*(перем Укль: динамическиТипизированныйУкль; метаданныеТипаЭлементаДляСборщикаМусора: адресВПамяти; квоЭлтов, квоРазмерностей: размерМЗ; реальногоВремениЛи¿: булево);
	перем
		openArray: массив 2 из цел32;
	нач
		AllocH(openArray[1], квоЭлтов * квоРазмерностей, НИЗКОУР.прочти32битаПоАдресу(метаданныеТипаЭлементаДляСборщикаМусора));
		Укль := НИЗКОУР.подмениТипЗначения(динамическиТипизированныйУкль, openArray[1]);
	кон НовМассив˛требующийОметания;

	
	(* replacement for overcomplicated code emission -- at the cost of a slightly increased runtime cost *)
	проц НовМассив*(конст вхРазмерности: массив из размерМЗ;  метаданныеТипаЭлементаДляСборщикаМусора: адресВПамяти; staticElements, размерЭлта: размерМЗ; перем Укль: динамическиТипизированныйУкль);
	тип
		UnsafeArray= укль {опасныйДоступКПамяти,неОтслСборщиком} на UnsafeArrayDesc;
		UnsafeArrayDesc = запись
			header : массив 2 из адресВПамяти;
			len: массив 8 из размерМЗ;
		кон;
	перем p: динамическиТипизированныйУкль; dim: размерМЗ;
			
			проц GetSize(): размерМЗ;
			перем i: размерМЗ; size: размерМЗ;
			нач
				size := 1;
				нцДля i := 0 до dim-1 делай
					size := size * вхРазмерности[i];
				кц;
				возврат size*staticElements;
			кон GetSize;
			
			проц SetSizes(dest: UnsafeArray);
			перем i: размерМЗ;
			нач
				нцДля i := 0 до dim-1 делай
					dest.len[i] := вхРазмерности[dim-1-i];
				кц;
			кон SetSizes;

		нач
			(* static elements is requred for this case : POINTER TO ARRAY OF ARRAY X OF RecordWithPointer *)
			dim := длинаМассива( вхРазмерности,0 );
			если метаданныеТипаЭлементаДляСборщикаМусора = НУЛЬ то
				НовОбъект˛неТребующийОметания(p, GetSize() * размерЭлта + dim * размер16_от(адресВПамяти) + 3 *размер16_от(адресВПамяти) + (dim DIV 2) * 2 * размер16_от(адресВПамяти), ложь);
			иначе
				НовМассив˛требующийОметания(p, метаданныеТипаЭлементаДляСборщикаМусора, GetSize(), dim, ложь);
			всё;
			SetSizes(p);
			Укль := p;
	кон НовМассив;
	

	проц Show*();
	нач
		Трассировка.пСтроку8("Heap base : "); Трассировка.п16ричное( heapStart, -8 ); Трассировка.пСтроку8("; heap "); Трассировка.п16ричное( heap, -8 ); Трассировка.пСтроку8("; end "); Трассировка.п16ричное( heapEnd, -8 ); Трассировка.пВК_ПС;
	кон Show;

нач 
	 (* Init heap, currently done manually *)
	heapStart := Board.HeapBase;
	heap := Board.HeapBase;
	heapEnd := Board.HeapEnd;
кон Кучи.


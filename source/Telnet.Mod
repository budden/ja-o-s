модуль Telnet; (* ejz,  *)
	использует Modules, Потоки, TCP, TCPServices, ShellCommands, ЛогЯдра;

	конст
		NUL = симв8ИзКода(0); BEL = симв8ИзКода(7); BS = симв8ИзКода(8); HT = симв8ИзКода(9); LF = симв8ИзКода(10); VT = симв8ИзКода(11); FF = симв8ИзКода(12); CR = симв8ИзКода(13);

		CmdEOF = симв8ИзКода(236); CmdSUSP = симв8ИзКода(237); CmdABORT = симв8ИзКода(238); CmdEOR = симв8ИзКода(239);
		CmdSE* = симв8ИзКода(240); CmdNOP = симв8ИзКода(241); CmdDM = симв8ИзКода(242); CmdBRK = симв8ИзКода(243); CmdIP = симв8ИзКода(244);
		CmdAO = симв8ИзКода(245); CmdAYT = симв8ИзКода(246); CmdEC = симв8ИзКода(247); CmdEL = симв8ИзКода(248); CmdGA = симв8ИзКода(249);
		CmdSB* = симв8ИзКода(250); CmdWILL* = симв8ИзКода(251); CmdWONT = симв8ИзКода(252); CmdDO* = симв8ИзКода(253); CmdDONT = симв8ИзКода(254);
		CmdIAC* = симв8ИзКода(255);

		OptEcho* = симв8ИзКода(1); OptSupGoAhead* = симв8ИзКода(3); OptStatus = симв8ИзКода(5); OptTimingMatk = симв8ИзКода(6);
		OptTerminalType* = симв8ИзКода(24); OptWindowSize* = симв8ИзКода(31); OptTerminalSpeed = симв8ИзКода(32);
		OptFlowControl = симв8ИзКода(33); OptLineMode = симв8ИзКода(34); OptEnvVars = симв8ИзКода(36); OptNewEnv = симв8ИзКода(39);

		MaxLine = 256;

		Telnet* = 0; VT100* = 1; Echo* = 2;Tcp*=3;  Closed* = 31;

	тип
		Connection* = окласс
			перем
				C: Потоки.ФункциональныйИнтерфейсКПотоку;
				R*: Потоки.Чтец; W*: Потоки.Писарь;
				flags*: мнвоНаБитахМЗ; (* Telnet, VT100, Echo, Closed *)

			проц WILL*(option: симв8);
			нач
				W.пСимв8(CmdIAC);
				если option = OptEcho то
					W.пСимв8(CmdDO); W.пСимв8(OptEcho)
				аесли option = OptSupGoAhead то
					W.пСимв8(CmdDO); W.пСимв8(OptSupGoAhead)
				иначе
					W.пСимв8(CmdDONT); W.пСимв8(option)
				всё
			кон WILL;

			проц WONT*(option: симв8);
			нач
				W.пСимв8(CmdIAC); W.пСимв8(CmdDONT); W.пСимв8(option)
			кон WONT;

			проц Do*(option: симв8);
			нач
				W.пСимв8(CmdIAC);
				если option = OptEcho то
					W.пСимв8(CmdWILL); W.пСимв8(OptEcho); включиВоМнвоНаБитах(flags, Echo)
				аесли option = OptSupGoAhead то
					W.пСимв8(CmdWILL); W.пСимв8(OptSupGoAhead)
				иначе
					W.пСимв8(CmdWONT); W.пСимв8(option)
				всё
			кон Do;

			проц DONT*(option: симв8);
			нач
				если option = OptEcho то исключиИзМнваНаБитах(flags, Echo) всё;
				W.пСимв8(CmdIAC); W.пСимв8(CmdWONT); W.пСимв8(option)
			кон DONT;

			проц SB*(option: симв8);
				перем ch0, ch: симв8;
			нач
ЛогЯдра.пСтроку8("SB "); ЛогЯдра.пЦел64(кодСимв8(option), 0); ЛогЯдра.пВК_ПС();
				R.чСимв8(ch0); R.чСимв8(ch);
				нцПока ~((ch0 = CmdIAC) и (ch = CmdSE)) делай
					ch0 := ch; R.чСимв8(ch)
				кц
			кон SB;

			проц Consume*(ch: симв8);
			кон Consume;

			проц Dispatch;
				перем ch: симв8;
			нач
				R.чСимв8(ch);
				нцПока R.кодВозвратаПоследнейОперации = Потоки.Успех делай
					если (ch = CmdIAC) и (Telnet в flags) то
						R.чСимв8(ch);
						просей ch из
							CmdWILL: R.чСимв8(ch); WILL(ch)
							|CmdWONT: R.чСимв8(ch); WONT(ch)
							|CmdDO: R.чСимв8(ch); Do(ch)
							|CmdDONT: R.чСимв8(ch); DONT(ch)
							|CmdSB: R.чСимв8(ch); SB(ch)
							|CmdIAC: Consume(ch)
						иначе
							СТОП(99)
						всё;
						W.ПротолкниБуферВПоток()
					иначе
						Consume(ch)
					всё;
					R.чСимв8(ch)
				кц
			кон Dispatch;

			проц Setup*;
			кон Setup;

			проц Close*;
			нач {единолично}
				если C # НУЛЬ то
					C.Закрой(); C := НУЛЬ
				всё
			кон Close;

			проц Wait;
			нач {единолично}
				дождись(Closed в flags)
			кон Wait;

			проц &Init*(C: Потоки.ФункциональныйИнтерфейсКПотоку);
			нач
				сам.C := C; flags := {};
				Потоки.НастройЧтеца(R, C.ПрочтиИзПотока);
				Потоки.НастройПисаря(W, C.ЗапишиВПоток)
			кон Init;

		нач {активное}
			Setup();
			Dispatch();
			нач {единолично}
				включиВоМнвоНаБитах(flags, Closed)
			кон
		кон Connection;

		ServerConnection = окласс (Connection)
			перем
				ctx: ShellCommands.Context;
				line: массив MaxLine+1 из симв8; pos, len: цел32;
				term: массив 32 из симв8;

			проц {перекрыта}WILL*(option: симв8);
			нач
				если option = OptTerminalType то
					W.пСимв8(CmdIAC); W.пСимв8(CmdSB);
					W.пСимв8(OptTerminalType);
					W.пСимв8(01X); (* SEND *)
					W.пСимв8(CmdIAC); W.пСимв8(CmdSE)
				иначе
					WILL^(option)
				всё
			кон WILL;

			проц IsVT100(str: массив из симв8): булево;
				перем i: цел32; old, ch: симв8;
			нач
				old := 0X; i := 0; ch := str[0];
				нцПока (ch # 0X) и ~((old = "V") и (ch = "T")) делай
					old := ch; увел(i); ch := str[i]
				кц;
				возврат (old = "V") и (ch = "T")
			кон IsVT100;

			проц {перекрыта}SB*(option: симв8);
				перем term: массив 32 из симв8; i: цел32; ch: симв8;
			нач
				если option = OptTerminalType то
					если R.ПодглядиСимв8() = 0X то (* IS *)
						R.чСимв8(ch); (* 0X *)
						R.чСимв8(ch); i := 0;
						нцПока (i < 31) и (ch # CmdIAC) делай
							term[i] := ch; увел(i); R.чСимв8(ch)
						кц;
						term[i] := 0X; утв(ch = CmdIAC);
						R.чСимв8(ch); утв(ch = CmdSE);
						если IsVT100(term) то
							копируйСтрокуДо0(term, сам.term); включиВоМнвоНаБитах(flags, VT100)
						аесли term # сам.term то
							копируйСтрокуДо0(term, сам.term); WILL(OptTerminalType)
						иначе
							сам.term := ""; исключиИзМнваНаБитах(flags, VT100)
						всё
					иначе
						SB^(option)
					всё
				иначе
					SB^(option)
				всё
			кон SB;

			проц Command;
				перем msg: массив MaxLine из симв8; res: целМЗ;
			нач
				ShellCommands.Execute(ctx, line, res, msg);
				если res # 0 то
					W.пЦел64(res, 0); W.пСтроку8(": "); W.пСтроку8(msg); W.пВК_ПС()
				всё
			кон Command;

			проц Prompt;
			нач
				W.пСтроку8("> ")
			кон Prompt;

			проц {перекрыта}Consume*(ch: симв8);
				перем i: цел32; update, echo: булево;
			нач
				echo := Echo в flags; update := echo;
				если ch = CR то
					если echo то W.пСимв8(CR) всё;
					если R.ПодглядиСимв8() = LF то
						R.чСимв8(ch); если echo то W.пСимв8(LF) всё
					всё;
					line[len] := 0X;
					Command();
					line := ""; pos := 0; len := 0;
					Prompt(); update := истина
				аесли ch = 08X то
					если pos > 0 то
						если echo то W.пСимв8(ch) всё;
						умень(pos)
					всё
				аесли ch = 07FX то
					если pos > 0 то
						если echo то W.пСимв8(ch) всё;
						умень(pos); умень(len); i := pos;
						нцПока i < MaxLine делай
							line[i] := line[i+1]; увел(i)
						кц
					всё
				аесли ch = 01BX то (* ESC *)
					если (VT100 в flags) и (R.ПодглядиСимв8() = "[") то
						R.чСимв8(ch); R.чСимв8(ch);
						просей ch из
							"C": если pos < len то
										если echo то W.пСимв8(01BX); W.пСтроку8("[C") всё;
										увел(pos)
									всё
							|"D": если pos > 0 то
										если echo то W.пСимв8(01BX); W.пСтроку8("[D") всё;
										умень(pos)
									всё
						иначе
ЛогЯдра.пСтроку8("ESC ["); ЛогЯдра.пСимв8(ch); ЛогЯдра.пВК_ПС()
						всё
					всё
				аесли (ch >= 020X) и (pos < MaxLine) то
					если echo то W.пСимв8(ch) всё;
					line[pos] := ch; увел(pos);
					если pos > len то len := pos всё
				всё;
				если update то W.ПротолкниБуферВПоток() всё
			кон Consume;

			проц {перекрыта}Setup*;
			нач
				если Telnet в flags то
					если ~(Echo в flags) то
						W.пСимв8(CmdIAC); W.пСимв8(CmdWILL); W.пСимв8(OptEcho)
					всё;
					если ~(VT100 в flags) то
						W.пСимв8(CmdIAC); W.пСимв8(CmdDO); W.пСимв8(OptTerminalType)
					всё
				всё;
				Prompt(); W.ПротолкниБуферВПоток()
			кон Setup;

			проц &{перекрыта}Init*(C: Потоки.ФункциональныйИнтерфейсКПотоку);
			нач
				Init^(C); включиВоМнвоНаБитах(flags, Telnet); term := "";
				line := ""; pos := 0; len := 0;
				нов(ctx, C, R, W, W);
			кон Init;

		кон ServerConnection;

		Agent = окласс (TCPServices.Agent)
			перем C: ServerConnection;

		нач {активное}
			нов(C, client);
			C.Wait(); Terminate()
		кон Agent;

	перем
		service: TCPServices.Service;

	проц NewAgent(c: TCP.Connection; s: TCPServices.Service): TCPServices.Agent;
		перем a: Agent;
	нач
		нов(a, c, s); возврат a
	кон NewAgent;

	проц OpenService*;
	перем res: целМЗ;
	нач
		если service = НУЛЬ то
			нов(service, 23, NewAgent,res)
		всё;
	кон OpenService;

	проц TermMod;
	нач
		если service # НУЛЬ то
			service.Stop; service := НУЛЬ;
		всё
	кон TermMod;

нач
	service := НУЛЬ; Modules.InstallTermHandler(TermMod)
кон Telnet.

(* 
System.Free WMVT100 Telnet ShellCommands ~	System.OpenKernelLog

ShellCommands.Mod WMVT100.Mod

Aos.Call Telnet.OpenService ~

Configuration.DoCommands
System.DeleteFiles Telnet.zip ~
ZipTool.Add \9 Telnet.zip Telnet.Mod ShellCommands.Mod WMVT100.Mod old.WMVT100.Mod ~
NetSystem.SetUser ftp:zeller@lillian.inf.ethz.ch ~
FTP.Open zeller@lillian.inf.ethz.ch ~
FTP.PutFiles Telnet.zip ~
FTP.Close ~
~

*)

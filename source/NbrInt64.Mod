(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль NbrInt64;   (** AUTHOR "prk & adf"; PURPOSE "MathH with name changes to avoid basic-type conflicts."; *)

использует NbrInt8, NbrInt32, Потоки;

тип
	(** A 64-bit integer type. *)
	Integer* =цел64;

перем
	MinNbr-, MaxNbr-, One, Two: Integer;

конст


	проц Abs*( a: Integer ): Integer;
	нач
		возврат матМодуль(a);
	кон Abs;

	проц Dec*( перем a: Integer );
	нач
		умень(a);
	кон Dec;

	проц Inc*( перем a: Integer );
	нач
		увел(a);
	кон Inc;

	проц Odd*( a: Integer ): булево;
	нач
		возврат Odd(a);
	кон Odd;

	проц Long*( i: NbrInt32.Integer ): Integer;
	нач
		возврат i;
	кон Long;

	проц IsInt32*( i: Integer ): булево;
	нач
			если (i >= NbrInt32.MinNbr) и (i <= NbrInt32.MaxNbr) то возврат истина иначе возврат ложь всё
	кон IsInt32;

	проц Short*( h: Integer ): NbrInt32.Integer;
	нач
		возврат устарПреобразуйКБолееУзкомуЦел(h);
	кон Short;

	проц Max*( x1, x2: Integer ): Integer;
	нач
		если x1 > x2 то возврат x1 иначе возврат x2 всё
	кон Max;

	проц Min*( x1, x2: Integer ): Integer;
	нач
		если x1 < x2 то возврат x1 иначе возврат x2 всё
	кон Min;

	проц Sign*( x: Integer ): NbrInt8.Integer;
	перем sign: NbrInt8.Integer;
	нач
		если x < 0 то sign := -1
		аесли x = 0 то sign := 0
		иначе sign := 1
		всё;
		возврат sign
	кон Sign;

	(** String conversions. *)
(** Admissible characters include: {" ", "-", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ","}. *)
	проц StringToInt*( string: массив из симв8;  перем x: Integer );
	перем negative: булево;  i: NbrInt8.Integer;
	нач
		i := 0;
		(* Pass over any leading white space. *)
		нцПока string[i] = симв8ИзКода( 20H ) делай NbrInt8.Inc( i ) кц;
		(* Determine the sign. *)
		если string[i] = симв8ИзКода( 2DH ) то negative := истина;  NbrInt8.Inc( i ) иначе negative := ложь всё;
		(* Read in the string and convert it into an integer. *)
		x := 0;
		нцПока string[i] # 0X делай
			если (симв8ИзКода( 30H ) <= string[i]) и (string[i] <= симв8ИзКода( 39H )) то x := 10 * x + устарПреобразуйКБолееШирокомуЦел( кодСимв8( string[i] ) - 30H )
			иначе
				(* Inadmissible character - it is skipped. *)
			всё;
			NbrInt8.Inc( i )
		кц;
		если negative то x := -x всё
	кон StringToInt;

(** LEN(string) >= 27 *)
	проц IntToString*( x: Integer;  перем string: массив из симв8 );
	перем positive: булево;  i, k: NbrInt8.Integer;
		a: массив 21 из симв8;
	нач
		если x > MinNbr то
			(* Determine the sign. *)
			если x < 0 то x := -x;  positive := ложь иначе positive := истина всё;
			(* Convert the integer into a string. *)
			нцПока x > 0 делай a[i] := симв8ИзКода( Short( x остОтДеленияНа 10 ) + 30H );  x := x DIV 10;  NbrInt8.Inc( i ) кц;
			(* Test for zero. *)
			если i = 0 то a[0] := симв8ИзКода( 30H );  NbrInt8.Inc( i ) всё;
			(* Terminate the string. *)
			a[i] := 0X;  k := 0;
			если ~positive то
				(* Write a minus sign. *)
				string[k] := симв8ИзКода( 2DH );  NbrInt8.Inc( k )
			всё;
			(* Rewrite the string in a formatted output, inverting the order stored in a[i]. *)
			нцДо
				NbrInt8.Dec( i );  string[k] := a[i];  NbrInt8.Inc( k );
				если (i > 0) и ((i остОтДеленияНа 3) = 0) то
					(* Write a comma. *)
					string[k] := симв8ИзКода( 2CH );  NbrInt8.Inc( k )
				всё
			кцПри i = 0;
			string[k] := 0X
		иначе копируйСтрокуДо0( "-9,223,372,036,854,775,808", string )
		всё
	кон IntToString;

(** Persistence: file IO *)
	проц Load*( R: Потоки.Чтец;  перем x: Integer );
	перем low, hi: NbrInt32.Integer
	нач
		NbrInt32.Load( R, low );  NbrInt32.Load( R, hi );
		x := low + 100000000H * hi;
	кон Load;

	проц Store*( W: Потоки.Писарь;  x: Integer );
	перем low, hi: NbrInt32.Integer;
	нач
		low := устарПреобразуйКБолееУзкомуЦел(x);
		hi := устарПреобразуйКБолееУзкомуЦел(x DIV 100000000H);
		NbrInt32.Store( W, low );  NbrInt32.Store( W, hi )
	кон Store;

нач
	MinNbr := матМинимум(Integer);
	MaxNbr := матМаксимум(Integer);
	One := 1;
	Two := 2;
кон NbrInt64.

модуль FoxTextCompiler; (** AUTHOR ""; PURPOSE ""; *)

(* См. также LisTextCompiler. Этот модуль прописан в настройках редактора в секции Applications/PET/Compilers инициализационного файла *)

использует Потоки, Modules, Compiler, TextUtilities, Diagnostics, Texts, 
	SyntaxTree := FoxSyntaxTree, 
	CompilerInterface, 
	Commands, 
	ПередатчикОпределенийПрепроцессора, Files;

конст

	Name = "Fox";
	Description = "Oberon Compiler";
	FileExtension = "MOD"; (*! temporary *)


	проц GetClipboardReader(): Потоки.Чтец;
	перем size : размерМЗ;
		selectionText: Texts.Text;
		pcStr: укль на массив из симв8;
		stringReader: Потоки.ЧтецИзСтроки;
	нач
		selectionText := Texts.clipboard;
		selectionText.AcquireRead;
		size := TextUtilities.UTF8Size(selectionText,0,selectionText.GetLength())+1;
		нов(pcStr,size);
		TextUtilities.SubTextToStr(selectionText,0,size,pcStr^);
		selectionText.ReleaseRead;
		нов(stringReader,size);
		stringReader.ПримиСтроку8ДляЧтения(pcStr^);
		возврат stringReader;
	кон GetClipboardReader;

	проц GetSelectionReader(): Потоки.Чтец;
	перем a, b, size : размерМЗ;
		selectionText: Texts.Text;
		from, to: Texts.TextPosition;
		pcStr: укль на массив из симв8;
		stringReader: Потоки.ЧтецИзСтроки;
	нач
		если Texts.GetLastSelection(selectionText, from, to) то
			selectionText.AcquireRead;
			a := матМинимум(from.GetPosition(), to.GetPosition());
			b := матМаксимум(from.GetPosition(), to.GetPosition());
			size := TextUtilities.UTF8Size(selectionText,a,b-a+1)+1;
			нов(pcStr,size);
			TextUtilities.SubTextToStr(selectionText, a, b - a+1, pcStr^);
			selectionText.ReleaseRead;
			нов(stringReader,b-a+1);
			stringReader.ПримиСтроку8ДляЧтения(pcStr^);
		иначе
			stringReader  := НУЛЬ;
		всё;
		возврат stringReader;
	кон GetSelectionReader;

	проц CompileText*(t: Texts.Text; конст source: массив из симв8; pos: цел32; конст pc,opt: массив из симв8; log: Потоки.Писарь;
	diagnostics : Diagnostics.Diagnostics; перем error: булево);
	перем stringReader: Потоки.ЧтецИзСтроки;
		reader: Потоки.Чтец;
		options: Compiler.CompilerOptions;
		importCache: SyntaxTree.ВнутренняяОбластьВидимостиМодуля;
		имяФайлаНенужно, расширениеФайла: Files.FileName;
	нач
		если t = НУЛЬ то
			log.пСтроку8 ("No text available"); log.пВК_ПС; log.ПротолкниБуферВПоток;
			error := истина; возврат
		всё;
		нов(stringReader,длинаМассива(opt));
		stringReader.ПримиСтроку8ДляЧтения(opt);
		если Compiler.GetOptions(stringReader,log,diagnostics,options) то
			просейТип diagnostics :
				| ПередатчикОпределенийПрепроцессора.ExtractCompilerOptionsDiagnostics делай
					diagnostics.definitions := options.definitions;
					возврат; 
				иначе всё;
			(* raskommentirujj ehto, chtoby poluchatq stek na kazhduju oshibku kompiljacii:
			diagnostics := Basic.GetTracingDiagnostics(diagnostics); *)
			reader := TextUtilities.ДайЧтецаИзТекста˛начинающегоСПозиции(t,pos);
			если pc # "" то 
				включиВоМнвоНаБитах(options.flags,Compiler.FindPC); копируйСтрокуДо0(pc, options.findPC);
				исключиИзМнваНаБитах(options.flags,Compiler.Warnings);
			аесли 
				options.findPC # "" то включиВоМнвоНаБитах(options.flags,Compiler.FindPC)
			всё;
			если Compiler.FindPC в options.flags то
				исключиИзМнваНаБитах(options.flags,Compiler.Warnings) всё;		
			Files.SplitExtension(source, имяФайлаНенужно, расширениеФайла);
			если расширениеФайла = "мсм" то
				включиВоМнвоНаБитах(options.flags, Compiler.расширяяМакросы)
			иначе
				исключиИзМнваНаБитах(options.flags, Compiler.расширяяМакросы) всё;
			если (Compiler.расширяяМакросы в options.flags) и
					(Compiler.английскиеИмена в options.flags) то
				log.пСтроку8("Нельзя расширять макросы с английскими именами"); log.пВК_ПС; log.ПротолкниБуферВПоток;
				error := истина; возврат всё;	
			error := ~Compiler.Modules(source,reader,0,diagnostics, log, options, importCache)
		всё;
	кон CompileText;



	проц CompileSelection*(context: Commands.Context);
	нач
		Compiler.CompileReader(context,GetSelectionReader())
	кон CompileSelection;

	проц CompileClipboard*(context: Commands.Context);
	нач
		Compiler.CompileReader(context,GetClipboardReader())
	кон CompileClipboard;

проц Cleanup;
нач
	CompilerInterface.Unregister(Name);
кон Cleanup;

нач
	CompilerInterface.Register(Name, Description, FileExtension, CompileText);
	Modules.InstallTermHandler(Cleanup);
кон FoxTextCompiler.

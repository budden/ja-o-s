модуль Codecs; (** AUTHOR "TF"; PURPOSE "CODEC repository"; *)

использует
	Потоки, Commands, Files, SoundDevices, Raster, Modules, Strings, Configuration, Unzip, Texts, Archives, UCS32;

конст
	ResFailed* = -1;
	ResOk* = 0;
	ResSeekInexact* = 1;

	ImgFmtBW* = 0;
	ImgFmtGrey* = 1;
	ImgFmtRGB* = 2;
	ImgFmtRGBA* = 3;

	STError* = -1;		(* e.g. when requested stream does not exist *)
	STUnknown* = 0;
	STAudio* = 1;
	STVideo* = 2;
	STImage* = 3;

	SeekByte* = 0;
	SeekSample* = 1;
	SeekKeySample* = 2;
	SeekFrame* = 3;
	SeekKeyFrame* = 4;

тип
	AVStreamInfo* = запись
		streamType* : цел32;
		seekability* : мнвоНаБитахМЗ;
		contentType* : массив 16 из симв8;

		length* : цел32;
		frames* : цел32;
		rate*: цел32;
	кон;

	FileInputStream* = окласс(Потоки.Чтец)
	перем
		r : Files.Rider;
		f* : Files.File;
		streamInfo*: AVStreamInfo;

		проц Receive(перем buf: массив из симв8; ofs, size, min: размерМЗ; перем len: размерМЗ; перем res: целМЗ);
		нач
			f.ReadBytes(r, buf, ofs, size);
			len := size - r.res;
			если len >= min то res := Потоки.Успех иначе res := Потоки.КонецФайла (* end of file *) всё
		кон Receive;

		проц &InitFileReader*(f : Files.File; pos: цел32);
		нач
			новЧтец(сам.Receive, 4096);
			сам.f := f;
			f.Set(r, pos);
			streamInfo.seekability := {SeekByte};
		кон InitFileReader;

		проц {перекрыта}можноЛиПерейтиКМестуВПотоке¿*(): булево;
		нач
			возврат истина;
		кон можноЛиПерейтиКМестуВПотоке¿;

		проц {перекрыта}ПерейдиКМестуВПотоке*(pos : Потоки.ТипМестоВПотоке);
		нач
			f.Set(r, pos);
			ОчистьСостояниеБуфера;
			прочитаноБайт := pos;
		кон ПерейдиКМестуВПотоке;
	кон FileInputStream;

	AVDemultiplexer* = окласс

		(** open the demultiplexer on an input stream *)
		проц Open*(in : Потоки.Чтец; перем res : целМЗ);
		кон Open;

		проц GetNumberOfStreams*() : цел32;
		нач
			возврат 0
		кон GetNumberOfStreams;

		проц GetStreamType*(streamNr : цел32): цел32;
		нач
			возврат -1;
		кон GetStreamType;

		проц GetStreamInfo*(streamNr : цел32): AVStreamInfo;
		кон GetStreamInfo;

		(* get stream streamNr *)
		проц GetStream*(streamNr: цел32): DemuxStream;
		кон GetStream;

		(* read data from streamNr, store it into buffer buf starting at offset ofs, store size bytes if possible, block if not read min bytes at least. Return number of read bytes in len and return code res *)
		(* this procedure should not be directly called - it is called by the DemuxStream object! *)
		проц GetData*(streamNr : цел32; перем buf: массив из симв8; ofs, size, min: размерМЗ; перем len: размерМЗ; перем res: целМЗ);
		кон GetData;

		(* seek the streamNr to position pos (defined bz seekType), res = 0 if Ok, otherwise an error number *)
		(* this procedure should not be directly called - it is called by the DemuxStream object! *)
		проц SetStreamPos*(streamNr : цел32; seekType : цел32; pos : цел32; перем itemSize : цел32; перем res : целМЗ);
		кон SetStreamPos;

	кон AVDemultiplexer;

	DemuxStream* = окласс(Потоки.Чтец)
	перем
		demultiplexer* : AVDemultiplexer;
		streamNr* : цел32;
		streamInfo* : AVStreamInfo;

		проц& Open*(demultiplexer : AVDemultiplexer; streamNr : цел32);
		нач
			сам.demultiplexer := demultiplexer;
			сам.streamNr := streamNr;
			новЧтец(Receive, 4096)
		кон Open;

		проц Receive(перем buf: массив из симв8; ofs, size, min: размерМЗ; перем len: размерМЗ; перем res: целМЗ);
		нач
			demultiplexer.GetData(streamNr, buf, ofs, size, min, len, res)
		кон Receive;

		проц {перекрыта}ПерейдиКМестуВПотоке*(pos : Потоки.ТипМестоВПотоке);
		перем  seekType, itemSize: цел32; res: целМЗ;
		нач
			seekType := SeekByte;
			demultiplexer.SetStreamPos(streamNr, seekType, pos(цел32), itemSize, res);
			ОчистьСостояниеБуфера
		кон ПерейдиКМестуВПотоке;

		(* seek the streamNr to position pos with seekType. itemSize contains the size of the element seeked to, if known and applicable; res = 0 if Ok, otherwise an error number *)
		проц SetPosX*(seekType : цел32; pos : цел32; перем itemSize : цел32; перем res : целМЗ);
		нач
			demultiplexer.SetStreamPos(streamNr, seekType, pos, itemSize, res);
			ОчистьСостояниеБуфера
		кон SetPosX;
	кон DemuxStream;

	AudioDecoder* = окласс
		(* open the decoder on a file *)
		проц Open*(in : Потоки.Чтец; перем res : целМЗ);
		кон Open;

		проц HasMoreData*():булево;
		кон HasMoreData;

		проц GetAudioInfo*(перем nofChannels, samplesPerSecond, bitsPerSample : цел32);
		кон GetAudioInfo;

		проц SetAudioInfo*(nofChannels, samplesPerSecond, bitsPerSample : цел32);
		кон SetAudioInfo;

		проц CanSeek*() : булево;
		нач возврат ложь
		кон CanSeek;

		проц GetCurrentSample*() : цел32;
		нач СТОП(301); возврат 0
		кон GetCurrentSample;

		проц GetTotalSamples*() : цел32;
		нач СТОП(301); возврат 0
		кон GetTotalSamples;

		проц GetCurrentTime*() : цел32;
		нач СТОП(301); возврат 0
		кон GetCurrentTime;

		проц SetStreamLength*(length : цел32);
		кон SetStreamLength;

		проц SeekSample*(sample: цел32; goKeySample : булево; перем res : целМЗ);
		кон SeekSample;

		проц SeekMillisecond*(millisecond : цел32; goKeySample : булево; перем res : целМЗ);
		кон SeekMillisecond;

		(** Prepare the next audio bytes not yet filled into a buffer *)
		проц Next*;
		кон Next;

		проц FillBuffer*(buffer : SoundDevices.Buffer);
		кон FillBuffer;

	кон AudioDecoder;

	AudioEncoder* = окласс
		(* open the encoder *)
		проц Open*(out : Потоки.Писарь; sRate, sRes, nofCh: цел32; перем res : целМЗ);
		кон Open;

		проц Write*(buffer : SoundDevices.Buffer; перем res : целМЗ);
		кон Write;

		проц Close*(перем res : целМЗ);
		кон Close;

	кон  AudioEncoder;

	VideoDecoder* = окласс
		(* open the decoder on a file *)
		проц Open*(in : Потоки.Чтец; перем res : целМЗ);
		кон Open;

		проц HasMoreData*():булево;
		кон HasMoreData;

		проц GetVideoInfo*(перем width, height, millisecondsPerFrame : цел32);
		кон GetVideoInfo;

		проц CanSeek*() : булево;
		нач возврат ложь
		кон CanSeek;

		проц GetCurrentFrame*() : цел32;
		кон GetCurrentFrame;

		проц GetCurrentTime*() : цел32;
		кон GetCurrentTime;

		проц SeekFrame*(frame : цел32; goKeyFrame : булево; перем res : целМЗ);
		кон SeekFrame;

		проц SeekMillisecond*(millisecond : цел32; goKeyFrame : булево; перем res : целМЗ);
		кон SeekMillisecond;

		(** Prepare the next frame *)
		проц Next*;
		кон Next;

		проц Render*(img : Raster.Image);
		кон Render;

	кон VideoDecoder;

	VideoEncoder* = окласс
		(* open the encoder *)
		проц Open*(out : Потоки.Писарь; перем res : целМЗ);
		кон Open;

		проц Write*(img : Raster.Image);
		кон Write;

		проц Close*(перем res : целМЗ);
		кон Close;

	кон  VideoEncoder;

	ImageDecoder* = окласс
		(* open the decoder on an InputStream *)
		проц Open*(in : Потоки.Чтец; перем res : целМЗ);
		кон Open;

		проц GetImageInfo*(перем width, height : размерМЗ; перем format, maxProgressionLevel : цел32);
		кон GetImageInfo;

		(** Render will read and decode the image data up to progrssionLevel.
			If the progressionLevel is lower than a previously rendered progressionLevel,
			the new level can be ignored by the decoder. If no progressionLevel is set with
			SetProgressionLevel, the level is assumed to be maxProgressionLevel of the image,
			which corresponds to best image quality.
		 *)
		проц SetProgressionLevel*(progressionLevel: цел32);
		кон SetProgressionLevel;

		(* return the image in Raster format that best matches the format *)
		проц GetNativeImage*(перем img : Raster.Image);
		кон GetNativeImage;

		(* renders the image into the given Raster.Image at the given progressionLevel *)
		проц Render*(img : Raster.Image);
		кон Render;

	кон ImageDecoder;


	ImageEncoder* = окласс
		(* open the encoder on a stream*)
		проц Open*(out : Потоки.Писарь);
		кон Open;

		проц SetQuality*(quality : цел32);
		кон SetQuality;

		проц WriteImage*(img : Raster.Image; перем res : целМЗ);
		кон WriteImage;

	кон ImageEncoder;


	TextDecoder* = окласс
		(* open the decoder on an InputStream *)
		проц Open*(in : Потоки.Чтец; перем res : целМЗ);
		кон Open;

		проц GetText*() : Texts.Text;
		нач
			СТОП(301); возврат НУЛЬ
		кон GetText;
	кон TextDecoder;

	TextEncoder* = окласс
		(* open the encoder on a stream*)
		проц Open*(out : Потоки.Писарь);
		кон Open;

		проц WriteText*(text : Texts.Text; перем res : целМЗ);
		кон WriteText;
	кон TextEncoder;

	CryptoDecoder* = окласс
		проц Open*(in: Потоки.Чтец; перем res: целМЗ);
		кон Open;

		проц GetReader*(): Потоки.Чтец;
		кон GetReader;
	кон CryptoDecoder;

	CryptoEncoder* = окласс
		проц Open*(out: Потоки.Писарь);
		кон Open;

		проц GetWriter*(): Потоки.Писарь;
		кон GetWriter;
	кон CryptoEncoder;

(****** Animations *)

конст
	(** ImageDescriptor.disposeMode *)
	Unspecified* = 0;
	DoNotDispose* = 1;
	RestoreToBackground* = 2;
	RestoreToPrevious* = 3;

	(** ImageDescriptor.flags *)
	WaitForUserInput* = 0;

тип

	ImageDescriptor* = окласс
	перем
		left*, top*, width*, height*  : размерМЗ;
		image* : Raster.Image;
		delayTime* : цел32; (* in milliseconds *)
		disposeMode* : цел32;
		flags* : мнвоНаБитахМЗ;
		previous*, next* : ImageDescriptor;

		проц &Init*;
		нач
			left := 0; top := 0; width := 0; height := 0;
			image := НУЛЬ;
			delayTime := 20; disposeMode := Unspecified;
			flags := {};
			previous := НУЛЬ; next := НУЛЬ;
		кон Init;

	кон ImageDescriptor;

	ImageSequence* = запись
		width*, height* : размерМЗ;
		bgColor* : цел32;
		images* : ImageDescriptor;
	кон;

	AnimationDecoder* = окласс

		(* open the decoder on an InputStream *)
		проц Open*(in : Потоки.Чтец; перем res : целМЗ);
		кон Open;

		проц GetImageSequence*(перем sequence : ImageSequence; перем res : целМЗ);
		кон GetImageSequence;

	кон AnimationDecoder;

тип

	DemuxFactory = проц () : AVDemultiplexer;

	AudioDecoderFactory = проц () : AudioDecoder;
	AudioEncoderFactory = проц () : AudioEncoder;

	VideoDecoderFactory = проц () : VideoDecoder;
	VideoEncoderFactory = проц () : VideoEncoder;

	ImageDecoderFactory = проц () : ImageDecoder;
	ImageEncoderFactory = проц () : ImageEncoder;

	TextDecoderFactory = проц () : TextDecoder;
	TextEncoderFactory = проц () : TextEncoder;

	CryptoDecoderFactory = проц () : CryptoDecoder;
	CryptoEncoderFactory = проц () : CryptoEncoder;

	AnimationDecoderFactory = проц () : AnimationDecoder;


проц GetDemuxFactoryName(конст name : массив из симв8; перем module , procedure : Modules.Name; перем res : целМЗ);
перем config, factoryName, msg : массив 128 из симв8;
нач
	res := ResFailed;
	config := "Codecs.Demultiplexer."; Strings.Append(config, name);
	Configuration.Get(config, factoryName, res);
	если (res = Configuration.Ok) то
		Commands.Split(factoryName, module, procedure, res, msg);
	всё
кон GetDemuxFactoryName;

проц GetDecoderFactoryName(конст type, name : массив из симв8; перем module, procedure : Modules.Name; перем res : целМЗ);
перем config, factoryName, msg : массив 128 из симв8;
нач
	res := ResFailed;
	config := "Codecs.Decoder."; Strings.Append(config, type); Strings.Append(config, ".");
	Strings.Append(config, name);
	Configuration.Get(config, factoryName, res);
	если (res = Configuration.Ok) то
		Commands.Split(factoryName, module, procedure, res, msg);
	всё
кон GetDecoderFactoryName;

проц GetEncoderFactoryName(конст type, name : массив из симв8; перем module, procedure : Modules.Name; перем res : целМЗ);
перем config, factoryName, msg : массив 128 из симв8;
нач
	res := ResFailed;
	config := "Codecs.Encoder."; Strings.Append(config, type); Strings.Append(config, ".");
	Strings.Append(config, name);
	Configuration.Get(config, factoryName, res);
	если (res = Configuration.Ok) то
		Commands.Split(factoryName, module, procedure, res, msg);
	всё
кон GetEncoderFactoryName;

(** Return a registered demultiplexer e.g. "AVI" *)
проц GetDemultiplexer*(конст name : массив из симв8) : AVDemultiplexer;
перем
	demux : AVDemultiplexer; factory : DemuxFactory;
	moduleName, procedureName : Modules.Name; res : целМЗ;
нач
	demux := НУЛЬ;
	GetDemuxFactoryName(name, moduleName, procedureName, res);
	если (res = ResOk) то
		дайПроцПоИмени(moduleName, procedureName, factory);
		если (factory # НУЛЬ) то
			demux := factory();
		всё;
	всё;
	возврат demux;
кон GetDemultiplexer;

(** Return a registered image decoder e.g. "JP2", "BMP", "PNG" *)
проц GetImageDecoder*(конст name : массив из симв8) : ImageDecoder;
перем
	decoder : ImageDecoder; factory : ImageDecoderFactory;
	moduleName, procedureName : Modules.Name; res : целМЗ;
нач
	decoder := НУЛЬ;
	GetDecoderFactoryName("Image", name, moduleName, procedureName, res);
	если (res = ResOk) то
		дайПроцПоИмени(moduleName, procedureName, factory);
		если (factory # НУЛЬ) то
			decoder := factory();
		всё;
	всё;
	возврат decoder;
кон GetImageDecoder;

(** Return a registered image decoder e.g. "BMP" *)
проц GetImageEncoder*(конст name : массив из симв8) : ImageEncoder;
перем
	encoder : ImageEncoder; factory : ImageEncoderFactory;
	moduleName, procedureName : Modules.Name; res : целМЗ;
нач
	encoder := НУЛЬ;
	GetEncoderFactoryName("Image", name, moduleName, procedureName, res);
	если (res = ResOk) то
		дайПроцПоИмени(moduleName, procedureName, factory);
		если (factory # НУЛЬ) то
			encoder := factory();
		всё;
	всё;
	возврат encoder;
кон GetImageEncoder;

(** Return a registered video decoder. The decoder name is typically a FourCC code  e.g. "DivX" *)
проц GetVideoDecoder*(конст name : массив из симв8) : VideoDecoder;
перем
	decoder : VideoDecoder; factory : VideoDecoderFactory;
	moduleName, procedureName : Modules.Name; res : целМЗ;
нач
	decoder := НУЛЬ;
	GetDecoderFactoryName("Video", name, moduleName, procedureName, res);
	если (res = ResOk) то
		дайПроцПоИмени(moduleName, procedureName, factory);
		если (factory # НУЛЬ) то
			decoder := factory();
		всё;
	всё;
	возврат decoder;
кон GetVideoDecoder;

(** Return a registered video encoder *)
проц GetVideoEncoder*(конст name : массив из симв8) : VideoEncoder;
перем
	encoder : VideoEncoder; factory : VideoEncoderFactory;
	moduleName, procedureName : Modules.Name; res : целМЗ;
нач
	encoder := НУЛЬ;
	GetEncoderFactoryName("Video", name, moduleName, procedureName, res);
	если (res = ResOk) то
		дайПроцПоИмени(moduleName, procedureName, factory);
		если (factory # НУЛЬ) то
			encoder := factory();
		всё;
	всё;
	возврат encoder;
кон GetVideoEncoder;

(** Return a registered audio decoder e.g. "MP3" *)
проц GetAudioDecoder*(конст name : массив из симв8) : AudioDecoder;
перем
	decoder : AudioDecoder; factory : AudioDecoderFactory;
	moduleName, procedureName : Modules.Name; res : целМЗ;
нач
	decoder := НУЛЬ;
	GetDecoderFactoryName("Audio", name, moduleName, procedureName, res);
	если (res = ResOk) то
		дайПроцПоИмени(moduleName, procedureName, factory);
		если (factory # НУЛЬ) то
			decoder := factory();
		всё;
	всё;
	возврат decoder;
кон GetAudioDecoder;

(** Return a registered audio encoder e.g. "WAV" *)
проц GetAudioEncoder*(конст name : массив из симв8) : AudioEncoder;
перем
	encoder : AudioEncoder; factory : AudioEncoderFactory;
	moduleName, procedureName : Modules.Name; res : целМЗ;
нач
	encoder := НУЛЬ;
	GetEncoderFactoryName("Audio", name, moduleName, procedureName, res);
	если (res = ResOk) то
		дайПроцПоИмени(moduleName, procedureName, factory);
		если (factory # НУЛЬ) то
			encoder := factory();
		всё;
	всё;
	возврат encoder;
кон GetAudioEncoder;

(** Return a registered text decoder e.g. "UTF-8" *)
проц GetTextDecoder*(конст name : массив из симв8) : TextDecoder;
перем
	decoder : TextDecoder; factory : TextDecoderFactory;
	moduleName, procedureName : Modules.Name; res : целМЗ;
нач
	decoder := НУЛЬ;
	GetDecoderFactoryName("Text", name, moduleName, procedureName, res);
	если (res = ResOk) то
		дайПроцПоИмени(moduleName, procedureName, factory);
		если (factory # НУЛЬ) то
			decoder := factory();
		всё;
	всё;
	возврат decoder;
кон GetTextDecoder;

(** Return a registered text encoder e.g. "Oberon" *)
проц GetTextEncoder*(конст name : массив из симв8) : TextEncoder;
перем
	encoder : TextEncoder; factory : TextEncoderFactory;
	moduleName, procedureName : Modules.Name; res : целМЗ;
нач
	encoder := НУЛЬ;
	GetEncoderFactoryName("Text", name, moduleName, procedureName, res);
	если (res = ResOk) то
		дайПроцПоИмени(moduleName, procedureName, factory);
		если (factory # НУЛЬ) то
			encoder := factory();
		всё;
	всё;
	возврат encoder;
кон GetTextEncoder;

(** Return a registered crypto decoder *)
проц GetCryptoDecoder*(конст name : массив из симв8) : CryptoDecoder;
перем
	decoder : CryptoDecoder; factory : CryptoDecoderFactory;
	moduleName, procedureName : Modules.Name; res : целМЗ;
нач
	decoder := НУЛЬ;
	GetDecoderFactoryName("Crypto", name, moduleName, procedureName, res);
	если (res = ResOk) то
		дайПроцПоИмени(moduleName, procedureName, factory);
		если (factory # НУЛЬ) то
			decoder := factory();
		всё;
	всё;
	возврат decoder;
кон GetCryptoDecoder;

(** Return a registered crypto encoder *)
проц GetCryptoEncoder*(конст name : массив из симв8) : CryptoEncoder;
перем
	encoder : CryptoEncoder; factory : CryptoEncoderFactory;
	moduleName, procedureName : Modules.Name; res : целМЗ;
нач
	encoder := НУЛЬ;
	GetEncoderFactoryName("Crypto", name, moduleName, procedureName, res);
	если (res = ResOk) то
		дайПроцПоИмени(moduleName, procedureName, factory);
		если (factory # НУЛЬ) то
			encoder := factory();
		всё;
	всё;
	возврат encoder;
кон GetCryptoEncoder;

(** Return a registered animation decoder e.g. "GIF", "ANI" *)
проц GetAnimationDecoder*(конст name : массив из симв8) : AnimationDecoder;
перем
	decoder : AnimationDecoder; factory : AnimationDecoderFactory;
	moduleName, procedureName : Modules.Name; res : целМЗ;
нач
	decoder := НУЛЬ;
	GetDecoderFactoryName("Animation", name, moduleName, procedureName, res);
	если (res = ResOk) то
		дайПроцПоИмени(moduleName, procedureName, factory);
		если (factory # НУЛЬ) то
			decoder := factory();
		всё;
	всё;
	возврат decoder;
кон GetAnimationDecoder;

проц SplitName*(конст  name : массив из симв8; перем protocol, filename : массив из симв8);
перем pos, i : размерМЗ;
нач
	pos := Strings.Pos("://", name);
	если pos >= 0 то
		нцДля i := 0 до pos - 1 делай protocol[i] := name[i] кц;
		protocol[pos] := 0X;
		увел(pos, 3);	i := 0; нцПока name[pos] # 0X делай filename[i] := name[pos]; увел(pos); увел(i) кц;
		filename[i] := 0X
	иначе
		копируйСтрокуДо0("", protocol);
		копируйСтрокуДо0(name, filename)
	всё
кон SplitName;

(* См. также JoinNameJQ *)
проц JoinName*(конст protocol, filename : массив из симв8; перем name : массив из симв8);
нач
	если (protocol # "") то
		Strings.Concat(protocol, "://", name); Strings.Concat(name, filename, name);
	иначе
		копируйСтрокуДо0(filename, name);
	всё;
кон JoinName;

проц JoinNameJQ*(конст prefix, name: UCS32.StringJQ; перем fullname: UCS32.StringJQ);
перем fullNameA : укль на массив из симв8;
нач
	нов(fullNameA,длинаМассива(fullname)*6);
	JoinName(UCS32.JQvNovU8(prefix)^, UCS32.JQvNovU8(name)^, fullNameA^);
	UCS32.COPYU8vJQ(fullNameA^, fullname) кон JoinNameJQ;


проц OpenInputStream*(конст name : массив из симв8) : Потоки.Чтец;
перем f : Files.File;
	is : FileInputStream;
	inpStream : Потоки.Чтец;
	r : Потоки.Делегат˛реализующийЧтениеИзПотока;
	tp, protocol, filename : массив 1024 из симв8;
	zf : Unzip.ZipFile;
	entry : Unzip.Entry;
	archive : Archives.Archive;
	res : целМЗ;
нач
	SplitName(name, protocol, filename);
	копируйСтрокуДо0(protocol, tp); Strings.LowerCase(tp);
	если protocol = "" то
		f := Files.Old(filename);
		если f = НУЛЬ то возврат НУЛЬ всё;
		нов(is, f, 0);
		возврат is
	аесли Strings.Match("*.zip", tp) то
		f := Files.Old(protocol);
		если f = НУЛЬ то возврат НУЛЬ всё;
		нов(zf, f, res);
		если res = 0 то
			entry := zf.FindEntry(filename);
			если entry # НУЛЬ то
				zf.OpenReceiver(r, entry, res);
				если res = 0 то
					нов(inpStream, r, 1024);
					возврат inpStream
				иначе возврат НУЛЬ
				всё
			иначе возврат НУЛЬ
			всё
		иначе возврат НУЛЬ
		всё
	аесли Strings.Match("*.skin", tp) то
		archive := Archives.Old(protocol, "skin");
		если archive = НУЛЬ то
			возврат НУЛЬ
		иначе
			archive.Acquire; r := archive.OpenReceiver(filename); archive.Release;
			если r = НУЛЬ то
				возврат НУЛЬ
			иначе
				нов(inpStream, r, 1024);
				возврат inpStream
			всё
		всё
	аесли Strings.Match("*.tar", tp) или Strings.Match("*.rep", tp) то
		archive := Archives.Old(protocol, "tar");
		если archive = НУЛЬ то
			возврат НУЛЬ
		иначе
			archive.Acquire; r := archive.OpenReceiver(filename); archive.Release;
			если r = НУЛЬ то
				возврат НУЛЬ
			иначе
				нов(inpStream, r, 1024);
				возврат inpStream
			всё
		всё
	всё;
	возврат НУЛЬ
кон OpenInputStream;

проц OpenOutputStream*(конст name : массив из симв8) : Потоки.Писарь;
перем
	file : Files.File; w : Files.Writer;
	writer : Потоки.Писарь;
	sender : Потоки.Делегат˛реализующийЗаписьВПоток;
	tp, protocol, filename : массив 1024 из симв8;
	archive : Archives.Archive;
нач
	writer := НУЛЬ;
	SplitName(name, protocol, filename);
	копируйСтрокуДо0(protocol, tp); Strings.LowerCase(tp);
	если protocol = "" то
		file := Files.New(filename);
		если file # НУЛЬ то
			Files.Register(file);
			нов(w, file, 0); writer := w;
		всё
	аесли Strings.Match("*.skin", tp) то
		archive := Archives.Old(protocol, "skin");
		если archive = НУЛЬ то archive := Archives.New(protocol, "skin"); всё;
		если archive # НУЛЬ то
			archive.Acquire; sender := archive.OpenSender(filename); archive.Release;
			если sender # НУЛЬ то
				нов(writer, sender, 1024);
			всё
		всё
	аесли Strings.Match("*.tar", tp) или Strings.Match("*.rep", tp)  то
		archive := Archives.Old(protocol, "tar");
		если archive = НУЛЬ то archive := Archives.New(protocol, "tar"); всё;
		если archive # НУЛЬ то
			archive.Acquire; sender := archive.OpenSender(filename); archive.Release;
			если sender # НУЛЬ то
				нов(writer, sender, 1024);
			всё
		всё
	всё;
	возврат writer;
кон OpenOutputStream;

кон Codecs.

--------------------------
System.Free Codecs~

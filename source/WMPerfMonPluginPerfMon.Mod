модуль WMPerfMonPluginPerfMon; (** AUTHOR "staubesv"; PURPOSE "Performance Monitor plugin for self-monitoring"; *)
(**
 * History:
 *
 *	27.02.2007	First release (staubesv)
 *)

использует
	WMPerfMonPlugins, Modules;

конст
	ModuleName = "WMPerfMonPluginPerfMon";

тип

	PerfMonStats= окласс(WMPerfMonPlugins.Plugin)

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		перем ds : WMPerfMonPlugins.DatasetDescriptor;
		нач
			p.name := "WMPerfMonPlugins"; p.description := "Performance Monitor monitoring";
			p.modulename := ModuleName;
			p.autoMin := ложь; p.autoMax := истина; p.minDigits := 7;

			нов(ds, 2);
			ds[0].name := "NnofPlugins";
			ds[1].name := "NnofValues";
			p.datasetDescriptor := ds;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		нач
			dataset[0] := WMPerfMonPlugins.NnofPlugins;
			dataset[1] := WMPerfMonPlugins.NnofValues;
		кон UpdateDataset;

	кон PerfMonStats;

проц Install*;
кон Install;

проц InitPlugin;
перем par : WMPerfMonPlugins.Parameter; plugin : PerfMonStats;
нач
	нов(par); нов(plugin, par);
кон InitPlugin;

проц Cleanup;
нач
	WMPerfMonPlugins.updater.RemoveByModuleName(ModuleName);
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
	InitPlugin;
кон WMPerfMonPluginPerfMon.

WMPerfMonPluginPerfMon.Install ~   System.Free WMPerfMonPluginPerfMon ~

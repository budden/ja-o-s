модуль CryptoTestRSA;

использует
	RSA := CryptoRSA, B := CryptoBigNumbers, P := CryptoPrimes,
	Files, Log := ЛогЯдра, WMDialogs;


конст
	Size = 1024;
	PrivateKeyFile = "SSH.RSAKey.priv";
	PublicKeyFile = "SSH.RSAKey.pub";

	проц Test1*;
	перем
		pub, priv: RSA.Key;
		p, q, e, signature: B.BigNumber;
		digest: массив 64 из симв8;
	нач
		Log.пСтроку8( "generating RSA Keys" ); Log.пВК_ПС;
		p := P.NewPrime( Size DIV 2, ложь );
		Log.пСтроку8("p = "); B.Print( p ); Log.пВК_ПС;

		q := P.NewPrime( Size DIV 2, ложь );
		Log.пСтроку8("q = "); B.Print( q ); Log.пВК_ПС;
		B.AssignInt( e, 3 );

		RSA.MakeKeys( p, q, e, "Test Key", pub, priv );
		digest := "Everyone gets Friday off her panties.";
		signature :=  priv.Sign( digest, 32 );
		если signature # НУЛЬ то
			если pub.Verify( digest, 32, signature ) то
				Log.пСтроку8( "sign : verify  ok" ); Log.пВК_ПС;
			иначе
				Log.пСтроку8( "sign : verify failed" ); Log.пВК_ПС;
			всё
		иначе
			Log.пСтроку8( "error: wrong passphrase for private key" ); Log.пВК_ПС
		всё;
	кон Test1;

	проц TestRSAKeyFiles*;
	конст
		headline = "enter passphrase for opening your private key";
	перем
		pub, priv: RSA.Key;
		signature: B.BigNumber;
		pw, digest: массив 64 из симв8;
		f: Files.File; r: Files.Reader;
		i, ignore: цел32;
	нач
		f := Files.Old( PrivateKeyFile ); Files.OpenReader( r, f, 0 );
		i := 0;
		нцДо
			ignore := WMDialogs.QueryPassword( headline, pw );
			r.ПерейдиКМестуВПотоке( 0 );
			priv := RSA.LoadPrivateKey( r, pw );
			увел( i )
		кцПри (priv # НУЛЬ) или (i = 3);
		если priv = НУЛЬ то
			Log.пСтроку8( "wrong passphrase" ); Log.пВК_ПС;  возврат
		всё;

		f := Files.Old( PublicKeyFile ); Files.OpenReader( r, f, 0 );
		pub := RSA.LoadPublicKey( r );

		digest := "Everyone gets Friday off her panties.";
		signature :=  priv.Sign( digest, 32 );
		если pub.Verify( digest, 32, signature ) то
			Log.пСтроку8( "sign : verify  ok" ); Log.пВК_ПС;
		иначе
			Log.пСтроку8( "sign : verify failed" ); Log.пВК_ПС;
		всё
	кон TestRSAKeyFiles;


кон CryptoTestRSA.

Compiler.Compile CryptoRSA.Mod CryptoTestRSA.Mod ~

System.Free CryptoTestRSA CryptoRSA CryptoPrimes CryptoBigNumbers~

CryptoTestRSA.Test1 ~

CryptoTestRSA.TestRSAKeyFiles ~

модуль  srVolShader;
использует srBase;

тип SREAL=srBase.SREAL;
тип Color=srBase.COLOR;

тип Shader* = окласс

проц Shade*(перем x,y,z,r,g,b,a: SREAL);
кон Shade;

кон Shader;

тип checkerboard3d*= окласс(Shader)
перем
	isquish*, jsquish*, ksquish*: цел16;
	black, white: Color;

проц squish*(i,j,k: цел16);
нач
	isquish:=i; jsquish:=j; ksquish:=k;
кон squish;

проц set*(b,w: Color);
нач
	black:=b; white:=w;
кон set;

проц {перекрыта}Shade*(перем x,y,z,r,g,b,a: SREAL);
перем
	i,j,k:цел16;
нач
	i:=устарПреобразуйКБолееУзкомуЦел(округлиВниз(x*isquish));
	j:=устарПреобразуйКБолееУзкомуЦел(округлиВниз(y*jsquish));
	k:=устарПреобразуйКБолееУзкомуЦел(округлиВниз(z*ksquish));
	если нечётноеЛи¿(i+j+k) то
		r:=black.red; g:=black.green; b:=black.blue; a:=0
	иначе
		r:=white.red; g:=white.green; b:=black.blue; a:=0
	всё
кон Shade;

кон checkerboard3d;


кон srVolShader.


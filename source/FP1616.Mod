модуль FP1616;	(** AUTHOR "PL"; PURPOSE "FixPoint 16.16 Module"; *)

конст
	FIXPOINT = 65536;						(* 2^8 *)

тип
(* converts a 16.16 fixpoint integer to float *)
проц FixpToFloat*(x: размерМЗ): вещ32;
нач
	возврат (x / FIXPOINT);
кон FixpToFloat;

(* converts a float into a 16.16 fixpoint integer *)
проц FloatToFixp*(x: вещ32): цел32;
нач
	если x > FIXPOINT то возврат FIXPOINT;
	аесли x < -FIXPOINT то возврат -FIXPOINT
	иначе возврат округлиВниз(x * (FIXPOINT));
	всё;
кон FloatToFixp;

проц Int*(fp1616: цел32) : цел32;
нач
	возврат fp1616 DIV FIXPOINT
кон Int;

(* addition / subtraction just do it *)

(* mulitplicates 2 Fixpoint Numbers *)
проц Mult*(x, y: цел32): цел32;
нач
	возврат (округлиВниз(FixpToFloat(x)*y));
кон Mult;

(* division of 2 fixpoint NUmbers *)
проц Div*(x, y: цел32): цел32;
нач
	возврат (округлиВниз(x/FixpToFloat(y)));
кон Div;

кон FP1616.

модуль SVGDecoder;

использует Codecs, Потоки, WMGraphics, Raster, SVGLoader, SVG, XML, XMLScanner, XMLParser;

тип
	SVGDecoder = окласс(Codecs.ImageDecoder)
		перем
			in: Потоки.Чтец;
			img: Raster.Image;
			width, height: размерМЗ;

		(* open the decoder on a file *)
		проц {перекрыта}Open*(in : Потоки.Чтец; перем res : целМЗ);
		перем
			xmlScanner: XMLScanner.Scanner;
			xmlParser: XMLParser.Parser;
			xmlDoc: XML.Document;
			xmlRoot: XML.Element;
		нач
			если in = НУЛЬ то SVG.Error("SVGCodec: Input Stream is NIL"); возврат всё;
			сам.in := in;

			нов(xmlScanner, in);
			нов(xmlParser, xmlScanner);

			xmlDoc := xmlParser.Parse();
			если xmlDoc = НУЛЬ то SVG.Error("SVGCodec: XML Doc is NIL"); возврат всё;

			xmlRoot := xmlDoc.GetRoot();
			если xmlRoot = НУЛЬ то SVG.Error("SVGCodec: XML root element is NIL"); возврат всё;

			img := SVGLoader.LoadSVGEmbedded(xmlRoot);
			если img = НУЛЬ то SVG.Error("SVGCodec: Image is NIL"); возврат всё;

			width := img.width;
			height := img.height;
		кон Open;

		проц {перекрыта}GetImageInfo*(перем width, height : размерМЗ; перем format, maxProgressionLevel : цел32);
		нач
			width := сам.width;
			height := сам.height;
		кон GetImageInfo;

		(** Render will read and decode the image data up to progrssionLevel.
			If the progressionLevel is lower than a previously rendered progressionLevel,
			the new level can be ignored by the decoder. If no progressionLevel is set with
			SetProgressionLevel, the level is assumed to be maxProgressionLevel of the image,
			which corresponds to best image quality.
		 *)
		проц {перекрыта}SetProgressionLevel*(progressionLevel: цел32);
		кон SetProgressionLevel;

		(* return the image in Raster format that best matches the format *)
		проц {перекрыта}GetNativeImage*(перем img : Raster.Image);
		нач
			img := сам.img;
		кон GetNativeImage;

		(* renders the image into the given Raster.Image at the given progressionLevel *)
		проц {перекрыта}Render*(img : Raster.Image);
		перем canvas : WMGraphics.BufferCanvas;
		нач
			если сам.img#НУЛЬ то
				нов(canvas, img);
				canvas.DrawImage(0, 0, сам.img, WMGraphics.ModeCopy);
			всё
		кон Render;
	кон SVGDecoder;

проц Factory*() : Codecs.ImageDecoder;
перем p : SVGDecoder;
нач
	нов(p);
	возврат p
кон Factory;

кон SVGDecoder.

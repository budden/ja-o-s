модуль VfW; (** AUTHOR "thomas.frey@alumni.ethz.ch"; PURPOSE "Video for Windows bindings for WinA2"; *)
(* see TestVideo.Mod for usage example *)

использует
	ЛогЯдра, GDI32, Kernel32, User32, НИЗКОУР;

конст
	WS_POPUP* = цел32(80000000H);
	WM_CAP_START* = 0400H;

	WM_CAP_GET_CAPSTREAMPTR* = WM_CAP_START + 1;

	WM_CAP_SET_CALLBACK_ERROR* = WM_CAP_START + 2;
	WM_CAP_SET_CALLBACK_STATUS* = WM_CAP_START + 3;
	WM_CAP_SET_CALLBACK_YIELD* = WM_CAP_START + 4;
	WM_CAP_SET_CALLBACK_FRAME* = WM_CAP_START + 5;
	WM_CAP_SET_CALLBACK_VIDEOSTREAM* = WM_CAP_START + 6;
	WM_CAP_SET_CALLBACK_WAVESTREAM* = WM_CAP_START + 7;
	WM_CAP_GET_USER_DATA* = WM_CAP_START + 8;
	WM_CAP_SET_USER_DATA* = WM_CAP_START + 9;

	WM_CAP_DRIVER_CONNECT* = WM_CAP_START + 10;
	WM_CAP_DRIVER_DISCONNECT* = WM_CAP_START + 11;
	WM_CAP_DRIVER_GET_NAME* = WM_CAP_START + 12;
	WM_CAP_DRIVER_GET_VERSION* = WM_CAP_START + 13;
	WM_CAP_DRIVER_GET_CAPS* = WM_CAP_START + 14;

	WM_CAP_FILE_SET_CAPTURE_FILE* = WM_CAP_START + 20;
	WM_CAP_FILE_GET_CAPTURE_FILE* = WM_CAP_START + 21;
	WM_CAP_FILE_ALLOCATE* = WM_CAP_START + 22;
	WM_CAP_FILE_SAVEAS* = WM_CAP_START + 23;
	WM_CAP_FILE_SET_INFOCHUNK* = WM_CAP_START + 24;
	WM_CAP_FILE_SAVEDIB* = WM_CAP_START + 25;

	WM_CAP_EDIT_COPY* = WM_CAP_START + 30;

	WM_CAP_SET_AUDIOFORMAT* = WM_CAP_START + 35;
	WM_CAP_GET_AUDIOFORMAT* = WM_CAP_START + 36;

	WM_CAP_DLG_VIDEOFORMAT* = WM_CAP_START + 41;
	WM_CAP_DLG_VIDEOSOURCE* = WM_CAP_START + 42;
	WM_CAP_DLG_VIDEODISPLAY* = WM_CAP_START + 43;
	WM_CAP_GET_VIDEOFORMAT* = WM_CAP_START + 44;
	WM_CAP_SET_VIDEOFORMAT* = WM_CAP_START + 45;
	WM_CAP_DLG_VIDEOCOMPRESSION* = WM_CAP_START + 46;

	WM_CAP_SET_PREVIEW* = WM_CAP_START + 50;
	WM_CAP_SET_OVERLAY* = WM_CAP_START + 51;
	WM_CAP_SET_PREVIEWRATE* = WM_CAP_START + 52;
	WM_CAP_SET_SCALE* = WM_CAP_START + 53;
	WM_CAP_GET_STATUS* = WM_CAP_START + 54;
	WM_CAP_SET_SCROLL* = WM_CAP_START + 55;

	WM_CAP_GRAB_FRAME* = WM_CAP_START + 60;
	WM_CAP_GRAB_FRAME_NOSTOP* = WM_CAP_START + 61;

	WM_CAP_SEQUENCE* = WM_CAP_START + 62;
	WM_CAP_SEQUENCE_NOFILE* = WM_CAP_START + 63;
	WM_CAP_SET_SEQUENCE_SETUP* = WM_CAP_START + 64;
	WM_CAP_GET_SEQUENCE_SETUP* = WM_CAP_START + 65;
	WM_CAP_SET_MCI_DEVICE* = WM_CAP_START + 66;
	WM_CAP_GET_MCI_DEVICE* = WM_CAP_START + 67;
	WM_CAP_STOP* = WM_CAP_START + 68;
	WM_CAP_ABORT* = WM_CAP_START + 69;

	WM_CAP_SINGLE_FRAME_OPEN* = WM_CAP_START + 70;
	WM_CAP_SINGLE_FRAME_CLOSE* = WM_CAP_START + 71;

	WM_CAP_SINGLE_FRAME* = WM_CAP_START + 72;


	WM_CAP_PAL_OPEN* = WM_CAP_START + 80;
	WM_CAP_PAL_SAVE* = WM_CAP_START + 81;
	WM_CAP_PAL_PASTE* = WM_CAP_START + 82;
	WM_CAP_PAL_AUTOCREATE* = WM_CAP_START + 83;
	WM_CAP_PAL_MANUALCREATE* = WM_CAP_START + 84;
	WM_CAP_SET_CALLBACK_CAPCONTROL* = WM_CAP_START + 85;

	WM_CAP_END* = WM_CAP_SET_CALLBACK_CAPCONTROL;

тип
	HWND* = Kernel32.HANDLE;

	VHdr* = запись
		lpData* : адресВПамяти;
		dwBufferLength*,
		dwBytesUsed*,
		dwTimeCaptured*,
		dwUser*,
		dwFlags* : цел32;
		dwReserved* : массив 4 из адресВПамяти;
	кон;

	Capturerec* = запись
		dwRequestMicroSecPerFrame*: цел32;
		fMakeUserHitOKToCapture*: цел32;
		wPercentDropForError*: цел32;
		fYield*: цел32;
		dwIndexSize*: цел32;
		wChunkGranularity*: цел32;
		fUsingDOSMemory*: цел32;
		wNumVideoRequested*: цел32;
		fCaptureAudio*: цел32;
		wNumAudioRequested*: цел32;
		vKeyAbort*: цел32;
		fAbortLeftMouse*: цел32;
		fAbortRightMouse*: цел32;
		fLimitEnabled*: цел32;
		wTimeLimit*: цел32;
		fMCIControl*: цел32;
		fStepMCIDevice*: цел32;
		dwMCIStartTime*: цел32;
		dwMCIStopTime*: цел32;
		fStepCaptureAt2x*: цел32;
		wStepCaptureAverageFrames*: цел32;
		dwAudioBufferSize*: цел32;
		fDisableWriteCache*: цел32;
		AVStreamMaster*: цел32;
	кон;

	CapVideoStreamCallback* = проц {WINAPI} (hWnd : HWND; перем lpVHdr : VHdr) : User32.LResult;
	CapYieldCallback* = проц {WINAPI} (hWnd : HWND) : User32.LResult;

перем
	CapCreateCaptureWindow-: проц {WINAPI} (конст lpszWindowName: массив из симв8;
		dwStyle, x, y, nWidth, nHeight : цел32;
		ParentWin : HWND;
		nId : цел32): HWND;
	CapGetDriverDescription- : проц {WINAPI} (wDriverIndex : цел32;
		перем lpszName : массив из симв8; cbName : цел32;
		перем lpszVer : массив из симв8; cbVar : цел32) : булево;
	(* PeekMessage is missing in Win32.User32 *)
	PeekMessage-: проц {WINAPI} (перем lpMsg: User32.Msg; hWnd: HWND; wMsgFilterMin, wMsgFilterMax, wRemoveMsg: цел32): цел32;


проц GetProcAddress(hModule: адресВПамяти; перем adr: адресВПамяти; конст procName: массив из симв8);
нач
	Kernel32.GetProcAddress(hModule, procName, adr);
кон GetProcAddress;

проц Init;
перем
	mod: Kernel32.HMODULE; str: массив 32 из симв8;
нач
	str := "AVICAP32.DLL";
	mod := Kernel32.LoadLibrary(str);
	если mod = 0 то
		ЛогЯдра.пСтроку8("Failed to load AVICAP32.DLL"); ЛогЯдра.пВК_ПС;
	всё;
	GetProcAddress(mod, НИЗКОУР.подмениТипЗначения(адресВПамяти, CapCreateCaptureWindow ), "capCreateCaptureWindowA");
	GetProcAddress(mod, НИЗКОУР.подмениТипЗначения(адресВПамяти, CapGetDriverDescription ), "capGetDriverDescriptionA");


	str := "USER32.DLL";
	mod := Kernel32.LoadLibrary(str);
	GetProcAddress(mod, НИЗКОУР.подмениТипЗначения(адресВПамяти, PeekMessage ), "PeekMessageA")
кон Init;

проц CapDriverConnect*(hwnd : HWND; index : цел32) : булево;
перем result : User32.LResult;
нач
	result := User32.SendMessage(hwnd, WM_CAP_DRIVER_CONNECT, index, 0);
	возврат result # 0
кон CapDriverConnect;

проц CapDriverDisconnect*(hwnd : HWND) : булево;
перем result : User32.LResult;
нач
	result := User32.SendMessage(hwnd, WM_CAP_DRIVER_DISCONNECT, 0, 0);
	возврат result # 0
кон CapDriverDisconnect;

проц CapSetCallbackFrame*(hwnd : HWND; proc : CapVideoStreamCallback) : булево;
перем result : User32.LResult;
нач
	result := User32.SendMessage(hwnd, WM_CAP_SET_CALLBACK_FRAME, 0, адресВПамяти(proc));
	возврат result # 0
кон CapSetCallbackFrame;

проц CapSetCallbackStream*(hwnd : HWND; proc : CapVideoStreamCallback) : булево;
перем result : User32.LResult;
нач
	result := User32.SendMessage(hwnd, WM_CAP_SET_CALLBACK_VIDEOSTREAM, 0, адресВПамяти(proc));
	возврат result # 0
кон CapSetCallbackStream;

проц CapSetCallbackYield*(hwnd : HWND; proc : CapYieldCallback) : булево;
перем result : User32.LResult;
нач
	result := User32.SendMessage(hwnd, WM_CAP_SET_CALLBACK_YIELD, 0, адресВПамяти(proc));
	возврат result # 0
кон CapSetCallbackYield;

проц CapGetVideoFormat*(hwnd : HWND; перем format : GDI32.BitmapInfoHeader) : булево;
перем result : User32.LResult;
нач
	result := User32.SendMessage(hwnd, WM_CAP_GET_VIDEOFORMAT, 40, адресОт(format));
	возврат result # 0
кон CapGetVideoFormat;

проц CapSetVideoFormat*(hwnd : HWND; перем format : GDI32.BitmapInfoHeader) : булево;
перем result : User32.LResult;
нач
	result := User32.SendMessage(hwnd, WM_CAP_SET_VIDEOFORMAT, 40, адресОт(format));
	возврат result # 0
кон CapSetVideoFormat;

проц CapGetSequenceSetup*(hwnd : HWND; перем format : Capturerec) : булево;
перем result : User32.LResult;
нач
	result := User32.SendMessage(hwnd, WM_CAP_GET_SEQUENCE_SETUP, 96, адресОт(format));
	возврат result # 0
кон CapGetSequenceSetup;

проц CapSetSequenceSetup*(hwnd : HWND; перем format : Capturerec) : булево;
перем result : User32.LResult;
нач
	result := User32.SendMessage(hwnd, WM_CAP_SET_SEQUENCE_SETUP, 96, адресОт(format));
	возврат result # 0
кон CapSetSequenceSetup;

проц CapCaptureStop*(hwnd : HWND) : булево;
перем result : User32.LResult;
нач
	result := User32.SendMessage(hwnd, WM_CAP_STOP, 0, 0);
	возврат result # 0
кон CapCaptureStop;

проц CapGrabFrame*(hwnd : HWND) : булево;
перем result : User32.LResult;
нач
	result := User32.SendMessage(hwnd, WM_CAP_GRAB_FRAME, 0, 0);
	возврат result # 0
кон CapGrabFrame;

проц CapSequenceNoFile*(hwnd : HWND) : булево;
перем result : User32.LResult;
нач
	result := User32.SendMessage(hwnd, WM_CAP_SEQUENCE_NOFILE, 0, 0);
	возврат result # 0
кон CapSequenceNoFile;

проц DumpCapRec*(конст capRec : Capturerec);
нач
	ЛогЯдра.пСтроку8("capRec.dwRequestMicroSecPerFrame= "); ЛогЯдра.пЦел64(capRec.dwRequestMicroSecPerFrame, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.fMakeUserHitOKToCapture= "); ЛогЯдра.пЦел64(capRec.fMakeUserHitOKToCapture, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.wPercentDropForError= "); ЛогЯдра.пЦел64(capRec.wPercentDropForError, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.fYield= "); ЛогЯдра.пЦел64(capRec.fYield, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.dwIndexSize= "); ЛогЯдра.пЦел64(capRec.dwIndexSize, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.wChunkGranularity= "); ЛогЯдра.пЦел64(capRec.wChunkGranularity, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.fUsingDOSMemory= "); ЛогЯдра.пЦел64(capRec.fUsingDOSMemory, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.wNumVideoRequested= "); ЛогЯдра.пЦел64(capRec.wNumVideoRequested, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.fCaptureAudio= "); ЛогЯдра.пЦел64(capRec.fCaptureAudio, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.wNumAudioRequested= "); ЛогЯдра.пЦел64(capRec.wNumAudioRequested, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.vKeyAbort= "); ЛогЯдра.пЦел64(capRec.vKeyAbort, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.fAbortLeftMouse= "); ЛогЯдра.пЦел64(capRec.fAbortLeftMouse, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.fAbortRightMouse= "); ЛогЯдра.пЦел64(capRec.fAbortRightMouse, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.fLimitEnabled= "); ЛогЯдра.пЦел64(capRec.fLimitEnabled, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.wTimeLimit= "); ЛогЯдра.пЦел64(capRec.wTimeLimit, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.fMCIControl= "); ЛогЯдра.пЦел64(capRec.fMCIControl, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.fStepMCIDevice= "); ЛогЯдра.пЦел64(capRec.fStepMCIDevice, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.dwMCIStartTime= "); ЛогЯдра.пЦел64(capRec.dwMCIStartTime, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.dwMCIStopTime= "); ЛогЯдра.пЦел64(capRec.dwMCIStopTime, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.fStepCaptureAt2x= "); ЛогЯдра.пЦел64(capRec.fStepCaptureAt2x, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.wStepCaptureAverageFrames= "); ЛогЯдра.пЦел64(capRec.wStepCaptureAverageFrames, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.dwAudioBufferSize= "); ЛогЯдра.пЦел64(capRec.dwAudioBufferSize, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.fDisableWriteCache= "); ЛогЯдра.пЦел64(capRec.fDisableWriteCache, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("capRec.AVStreamMaster= "); ЛогЯдра.пЦел64(capRec.AVStreamMaster, 0); ЛогЯдра.пВК_ПС;
кон DumpCapRec;

проц DumpBitmapInfoHeader*(перем info : GDI32.BitmapInfoHeader);
нач
	ЛогЯдра.пСтроку8("info.biSize= "); ЛогЯдра.пЦел64(info.biSize, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("info.biWidth= "); ЛогЯдра.пЦел64(info.biWidth, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("info.biHeight= "); ЛогЯдра.пЦел64(info.biHeight, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("info.biBitCount= "); ЛогЯдра.пЦел64(info.biBitCount, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("info.biCompression= "); ЛогЯдра.пЦел64(info.biCompression, 0); ЛогЯдра.пВК_ПС;
кон DumpBitmapInfoHeader;


нач
	Init()
кон VfW.

System.Free VfW ~


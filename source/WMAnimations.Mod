модуль WMAnimations; (** AUTHOR "staubesv"; PURPOSE "Visual components for animations"; *)

использует
	Потоки, Kernel, ЛогЯдра, Строки8, Files, Codecs, Raster, WMRectangles, WMGraphics, XML, WMProperties, WMComponents;

конст
	Ok = 0;
	NoDecoderFound = 1;
	FileNotFound = 2;
	Error = 3;

	State_Waiting = 0;
	State_Rendering = 1;
	State_Terminating = 99;
	State_Terminated = 100;

тип

	Animation* = окласс(WMComponents.VisualComponent)
	перем
		imageName- : WMProperties.StringProperty;
		isRepeating- : WMProperties.BooleanProperty;
		scaleImage- : WMProperties.BooleanProperty;
		forceNoBg- : WMProperties.BooleanProperty;

		(* the field below are protected by the hierarchy lock *)
		sequence : Codecs.ImageSequence;
		first, current : Codecs.ImageDescriptor;
		image : Raster.Image;
		aux_canvas : WMGraphics.BufferCanvas;

		state : цел32;
		timer : Kernel.Timer;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrAnimation);
			нов(imageName, PrototypeImageName, НУЛЬ, НУЛЬ); properties.Add(imageName);
			нов(isRepeating, PrototypeIsRepeating, НУЛЬ, НУЛЬ); properties.Add(isRepeating);
			нов(scaleImage, PrototypeScaleImage, НУЛЬ, НУЛЬ); properties.Add(scaleImage);
			нов(forceNoBg, PrototypeForceNoBg, НУЛЬ, НУЛЬ); properties.Add(forceNoBg);
			first := НУЛЬ; current := НУЛЬ;
			image := НУЛЬ; aux_canvas := НУЛЬ;
			state := State_Waiting;
			нов(timer);
		кон Init;

		проц {перекрыта}Initialize*;
		нач
			Initialize^;
			Acquire; LoadImages; Release; Invalidate;
		кон Initialize;

		проц {перекрыта}PropertyChanged*(sender, property : динамическиТипизированныйУкль);
		нач
			если (property = imageName) то
				LoadImages; Invalidate;
			аесли (property = isRepeating) и isRepeating.Get() то
				если (current # НУЛЬ) и ((current.previous # НУЛЬ) или (current.next # НУЛЬ)) то SetState(State_Rendering); всё;
			аесли (property = scaleImage) или (property = forceNoBg) то
				Invalidate;
			иначе
				PropertyChanged^(sender, property)
			всё
		кон PropertyChanged;

		проц LoadImages;
		перем name : Строки8.уСтрока; res : целМЗ;
		нач
			first := НУЛЬ; current := НУЛЬ; image := НУЛЬ;
			name := imageName.Get();
			если (name # НУЛЬ) то
				OpenAnimation(name^, sequence, res); current := first;
				если (res = Ok) то
					first := sequence.images; current := first;
					если (image = НУЛЬ) или (image.width # sequence.width) или (image.height # sequence.height) то
						нов(image);
						Raster.Create(image, sequence.width, sequence.height, Raster.BGRA8888);
						нов(aux_canvas, image);
					всё;
					aux_canvas.Fill(WMRectangles.MakeRect(0, 0, image.width, image.height), 0, WMGraphics.ModeCopy);
				иначе
					current := НУЛЬ; image := НУЛЬ; aux_canvas := НУЛЬ;
				всё;
			всё;
			если (current # НУЛЬ) то
				SetState(State_Rendering);
			иначе
				SetState(State_Waiting);
			всё;
			timer.Wakeup;
		кон LoadImages;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		перем  name : Строки8.уСтрока;
		нач
			DrawBackground^(canvas);
			если (image # НУЛЬ) то
				если ~scaleImage.Get() или (image.width = bounds.GetWidth()) и (image.height = bounds.GetHeight()) то
					canvas.DrawImage(0, 0, image, WMGraphics.ModeSrcOverDst);
				иначе
					canvas.ScaleImage(image, WMRectangles.MakeRect(0, 0, image.width, image.height), GetClientRect(), WMGraphics.ModeSrcOverDst, WMGraphics.ScaleBilinear);
				всё;
			иначе
				name := imageName.Get();
				если (name # НУЛЬ) то
					canvas.SetColor(WMGraphics.Red);
					WMGraphics.DrawStringInRect(canvas, GetClientRect(), ложь, WMGraphics.AlignCenter, WMGraphics.AlignCenter, name^);
				всё;
			всё
		кон DrawBackground;

		проц SetState(state : цел32);
		нач {единолично}
			если (сам.state < State_Terminating) или (state = State_Terminated) то
				сам.state := state;
			всё;
		кон SetState;

		проц {перекрыта}Finalize*;
		нач
			Finalize^;
			SetState(State_Terminated);
			timer.Wakeup;
			нач {единолично} дождись(state = State_Terminated); кон;
		кон Finalize;

		проц Update;
		перем imageDesc, p : Codecs.ImageDescriptor; delayTime : цел32;
		нач
			Acquire;
			если (image = НУЛЬ) то Release; возврат; всё;
			imageDesc := current;
			если (imageDesc # НУЛЬ) то
				если (imageDesc.previous # НУЛЬ) то
					p := imageDesc.previous;
					если (p.disposeMode = Codecs.RestoreToBackground) то
						если forceNoBg.Get() то
							aux_canvas.Fill(WMRectangles.MakeRect(p.left, p.top, p.left + p.width, p.top + p.height), 0, WMGraphics.ModeCopy);
						иначе
							aux_canvas.Fill(WMRectangles.MakeRect(p.left, p.top, p.left + p.width, p.top + p.height), sequence.bgColor, WMGraphics.ModeCopy);
						всё;
					аесли (p.disposeMode = Codecs.RestoreToPrevious) то

					всё;
				иначе
					aux_canvas.Fill(WMRectangles.MakeRect(0, 0, sequence.width, sequence.height), 0, WMGraphics.ModeCopy);
				всё;
				aux_canvas.DrawImage(imageDesc.left, imageDesc.top, imageDesc.image, WMGraphics.ModeSrcOverDst);
				если (imageDesc.next # НУЛЬ) то
					current := imageDesc.next;
				аесли isRepeating.Get() и (first.next # НУЛЬ) то
					current := first;
				иначе
					SetState(State_Waiting);
					Release;
					Invalidate;
					возврат;
				всё;
			всё;
			если (current # НУЛЬ) то
			(*	KernelLog.String(" delay : ");
				KernelLog.Int(current.delayTime, 0);
				KernelLog.String(", dispose: "); KernelLog.Int(current.disposeMode, 0);
				KernelLog.Ln; *)
				если (current.delayTime > 0) то
					delayTime := current.delayTime;
				иначе
					delayTime := 20; (* make sure that activity does not busy loop, assume max. 50 fps *)
				всё;
			иначе
				delayTime := 0;
			всё;
			Release;
			Invalidate;
			если (delayTime > 0) то timer.Sleep(delayTime); всё;
		кон Update;

	нач {активное}
		нцПока (state < State_Terminating) делай
			нач {единолично} дождись(state # State_Waiting); кон;
			Update;
		кц;
		SetState(State_Terminated);
	кон Animation;

перем
	PrototypeImageName : WMProperties.StringProperty;
	PrototypeIsRepeating, PrototypeScaleImage, PrototypeForceNoBg: WMProperties.BooleanProperty;

	StrAnimation : Строки8.уСтрока;

проц OpenAnimation(filename : массив из симв8; перем sequence : Codecs.ImageSequence; перем res : целМЗ);
перем
	name, extension : Files.FileName; reader : Потоки.Чтец;
	animationDecoder : Codecs.AnimationDecoder; imageDecoder : Codecs.ImageDecoder;
	imageDescriptor : Codecs.ImageDescriptor;
нач
	Строки8.ОтрежьПолеСлеваИСправа(filename, '"');
	Files.SplitExtension(filename, name, extension);
	Строки8.СтрокуВВерхнийРегистрASCII(extension);
	animationDecoder := Codecs.GetAnimationDecoder(extension);
	если (animationDecoder # НУЛЬ) то
		reader := Codecs.OpenInputStream(filename);
		если (reader # НУЛЬ) то
			animationDecoder.Open(reader, res);
			если (res = Codecs.ResOk) то
				animationDecoder.GetImageSequence(sequence, res);
			всё;
		иначе
			res := FileNotFound;
		всё;
	иначе
		imageDecoder := Codecs.GetImageDecoder(extension);
		если (imageDecoder # НУЛЬ) то
			reader := Codecs.OpenInputStream(filename);
			если (reader # НУЛЬ) то
				imageDecoder.Open(reader, res);
				если (res = Codecs.ResOk) то
					(* fake single frame image sequence *)
					нов(imageDescriptor);
					imageDecoder.GetNativeImage(imageDescriptor.image);
					если (imageDescriptor.image # НУЛЬ) то
						imageDescriptor.width := imageDescriptor.image.width;
						imageDescriptor.height := imageDescriptor.image.height;
						sequence.width := imageDescriptor.width;
						sequence.height := imageDescriptor.height;
						sequence.bgColor := 0;
						sequence.images := imageDescriptor;
						res := Ok;
					иначе
						sequence.images := НУЛЬ;
						res := Error;
					всё;
				всё;
			иначе
				res := FileNotFound;
			всё;
		иначе
			res := NoDecoderFound;
		всё;
	всё;
кон OpenAnimation;

проц GenAnimation*() : XML.Element;
перем a : Animation;
нач
	нов(a); возврат a;
кон GenAnimation;

проц FindAnimation*(конст uid : массив из симв8; component : WMComponents.Component) : Animation;
перем c : WMComponents.Component;
нач
	утв(component # НУЛЬ);
	c := component.FindByUID(uid);
	если (c # НУЛЬ) и (c суть Animation) то
		возврат c (Animation);
	иначе
		возврат НУЛЬ;
	всё;
кон FindAnimation;

проц InitPrototypes;
нач
	нов(PrototypeImageName, НУЛЬ, Строки8.ЯвиУСтроку("ImageName"), Строки8.ЯвиУСтроку("Filename of GIF image"));
	PrototypeImageName.Set(НУЛЬ);
	нов(PrototypeIsRepeating, НУЛЬ, Строки8.ЯвиУСтроку("IsRepeating"), Строки8.ЯвиУСтроку("Restart animation when finished?"));
	PrototypeIsRepeating.Set(истина);
	нов(PrototypeScaleImage, НУЛЬ, Строки8.ЯвиУСтроку("ScaleImage"), Строки8.ЯвиУСтроку("Scale images to component bounds?"));
	PrototypeScaleImage.Set(истина);
	нов(PrototypeForceNoBg, НУЛЬ, Строки8.ЯвиУСтроку("ForceNoBg"), Строки8.ЯвиУСтроку("Force background color to be transparent"));
	PrototypeForceNoBg.Set(ложь);
кон InitPrototypes;

проц InitStrings;
нач
	StrAnimation := Строки8.ЯвиУСтроку("Animation");
кон InitStrings;

нач
	InitStrings;
	InitPrototypes;
кон WMAnimations.

модуль srVoxel;
использует srBase, Math, srMath, srE,  srHex,Out := ЛогЯдра;

тип SREAL=srBase.SREAL;
тип PT=srBase.PT;
тип Ray = srBase.Ray;
тип Voxel = srBase.Voxel;

тип ColoredVox* = окласс(Voxel);
перем
	r, g, b: SREAL;
проц SetColor* (red, green, blue : SREAL);
нач
	r := srBase.clamp(red );
	g := srBase.clamp(green );
	b := srBase.clamp(blue );
кон SetColor;
проц {перекрыта}Shade*(перем ray: Ray);
нач
	ray.r := ray.r + r*ray.ra;
	ray.g := ray.g + g*ray.ga;
	ray.b := ray.b + b*ray.ba;
	ray.ra := 0;
	ray.ga := 0;
	ray.ba := 0;
кон Shade;
кон ColoredVox;

тип GoorowVox* = окласс(Voxel);
перем
	r, g, b: SREAL;
проц {перекрыта}Shade*(перем ray: Ray);
нач
	ray.r := ray.r + ray.lxyz.x*ray.ra;
	ray.g := ray.g + ray.lxyz.y*ray.ga;
	ray.b := ray.b + ray.lxyz.z*ray.ba;
	ray.ra := 0;
	ray.ga := 0;
	ray.ba := 0;
	ray.a :=0;
кон Shade;
кон GoorowVox;

тип LitVox* = окласс(Voxel);
перем
	r, g, b, nx, ny, nz: SREAL;
проц SetColor* (red, green, blue : SREAL);
нач
	r := srBase.clamp(red);
	g := srBase.clamp(green);
	b := srBase.clamp(blue);
кон SetColor;
проц {перекрыта}Shade*(перем ray: Ray);
перем
	x,y,z,dotrl: SREAL;
нач
	x := 1/2 - ray.lxyz.x; y := 1/2 - ray.lxyz.y; z := 1/2 - ray.lxyz.z;
	srBase.normalize(x,y,z);
	dotrl :=x*srBase.light.x + y*srBase.light.y + z*srBase.light.z;
	если dotrl > 0 то
		ray.r := ray.r +(r*dotrl)*ray.ra ;
		ray.g := ray.g + (g*dotrl)*ray.ga;
		ray.b := ray.b + (b*dotrl)*ray.ba;
	всё;
	ray.ra := 0;
	ray.ga := 0;
	ray.ba := 0;
	ray.a := 0;
кон Shade;
кон LitVox;

тип ColoredMVox* = окласс(Voxel); (*NOT CORRECT YET *)
перем
	r, g, b, mf, a: SREAL;
проц SetColor*(red, green, blue, mfraction: SREAL);
нач
	mf := srBase.clamp(mfraction);
	a := mf;
	r := srBase.clamp(red)*a;
	g := srBase.clamp(green)*a;
	b := srBase.clamp(blue)*a;
кон SetColor;
проц {перекрыта}Shade*(перем ray: Ray);
перем
	dr, dg, db: SREAL;
нач
	dr :=  r*ray.ra;
	dg :=  g*ray.ga;
	db :=  b*ray.ba;
	ray.r := ray.r + dr;
	ray.g := ray.g + dg;
	ray.b := ray.b + db;
	ray.ra := ray.ra - a*(dg+db);
	ray.ga := ray.ga - a*(dr+db);
	ray.ba := ray.ba - a*(dr+dg);
	ray.a := (ray.ra+ray.ga+ray.ba)/3;
	mirror(ray);
кон Shade;
кон ColoredMVox;

тип DiffuseMVox* = окласс(Voxel);
перем
	r, g, b, mf, a: SREAL;
проц {перекрыта}Shade*(перем ray: Ray);
перем
	nx, ny, nz: цел16;
	dot: SREAL;
	inside: булево;
нач
	просей ray.face из
		0: inside := истина
		|1: nx := -1
		|2: ny := -1
		|3: nz := -1
		|4: nx := 1
		|5: ny := 1
		|6: nz := 1
	иначе
	всё;
	если inside то dot := 0 иначе dot := матМодуль(nx*ray.dxyz.x + ny*ray.dxyz.y+ nz*ray.dxyz.z) всё;
	ray.ra := dot*ray.ra- 0.3;
	ray.ga := dot*ray.ga- 0.3;
	ray.ba := dot*ray.ba- 0.3;
	ray.a := (ray.ra+ray.ga+ray.ba)/3;
	mirror(ray);
кон Shade;
кон DiffuseMVox;

тип DiffuseSphMVox* = окласс(ColoredVox);
перем
	 mf, a: SREAL;
проц {перекрыта}Shade*(перем ray: Ray);
перем
	nx, ny, nz: SREAL;
	dot: SREAL;
	inside: булево;
нач
	nx := 1/2 - ray.lxyz.x; ny := 1/2-ray.lxyz.y; nz := 1/2-ray.lxyz.z;
	srBase.normalize(nx,ny, nz);
	если inside то dot := 0 иначе dot := матМодуль(nx*ray.dxyz.x + ny*ray.dxyz.y+ nz*ray.dxyz.z) всё;
	ray.a := dot*ray.a/2;
	ray.ra := dot*ray.ra/2;
	ray.ga := dot*ray.ga/2;
	ray.ba := dot*ray.ba/2;
	mirror(ray);
кон Shade;
кон DiffuseSphMVox;

тип DiffuseSphVox* = окласс(ColoredVox);
перем
	 mf, a: SREAL;
проц {перекрыта}Shade*(перем ray: Ray);
перем
	dot: SREAL;
	p: srBase.PT;
нач
	p.x:= 1/2 - ray.lxyz.x; p.y:= 1/2 - ray.lxyz.y; p.z:= 1/2 - ray.lxyz.z;
	srBase.normalizePT(p);
	dot := матМодуль(p.x*ray.dxyz.x + p.y*ray.dxyz.y+ p.z*ray.dxyz.z);
	ray.r := ray.r + r * ray.ra*dot;
	ray.g := ray.g + g * ray.ga*dot;
	ray.b := ray.b + b * ray.ba*dot;
	ray.ra := 0;
	ray.ga := 0;
	ray.ba := 0;
	ray.a := 0;
кон Shade;
кон DiffuseSphVox;

тип AlphaVox* = окласс(Voxel);
перем
	r, g, b, ra, ga, ba: SREAL;
проц SetColor* (red, green, blue, alpha  : SREAL);
нач
	r := srBase.clamp(red * alpha);
	g := srBase.clamp(green * alpha);
	b := srBase.clamp(blue * alpha);
кон SetColor;
проц {перекрыта}Shade*(перем ray: Ray);
перем
	dr, dg, db: SREAL;
нач
	dr :=  r*ray.ra;
	dg :=  g*ray.ga;
	db :=  b*ray.ba;
	ray.r := ray.r + dr;
	ray.g := ray.g + dg;
	ray.b := ray.b + db;
	ray.ra := ray.ra - (dg+db)/2;
	ray.ga := ray.ga - (dr+db)/2;
	ray.ba := ray.ba - (dr+dg)/2;
	ray.a := ray.a -(dr+dg+db)/3;
	ray.length := ray.length + ray.scale;
кон Shade;
кон AlphaVox;

(*TYPE ColoredDetailVox*=OBJECT(ColoredVox);

PROCEDURE Shade (VAR ray: Ray);
VAR
	l, x, y, z: SREAL;
	ecount: SIGNED16;
BEGIN
	ray.r := ray.r + r * ray.a;
	ray.g := ray.g + g * ray.a;
	ray.b := ray.b + b * ray.a;
	ray.a := ray.a - a
END Shade;

END ColoredDetailVox; *)

тип TransparaVox*=окласс(Voxel);
перем
	r, g, b, black: SREAL;
проц SetColor* (red, green, blue,bl  : SREAL);
нач
	r := red;
	g := green;
	b := blue;
	black:=bl;
	passable := истина;
кон SetColor;

проц {перекрыта}Shade*(перем ray: Ray);
перем
	depth: SREAL;
	exit:PT;
	dr,dg,db,dblack: SREAL;
нач
	exit:=srBase.Exit(ray);
	depth:=srBase.distsquared(ray.lxyz,exit);
	dr := r*depth;
	dg := g*depth;
	db := b*depth;
	dblack:=black*depth;
	ray.r := ray.r + dr;
	ray.g := ray.g + dg;
	ray.b := ray.b + db;
	ray.ra := ray.ra - dr-dblack;
	ray.ga := ray.ga - dg-dblack;
	ray.ba := ray.ba - db-dblack;
	srBase.clamp3(ray.ra,ray.ga,ray.ba);
	ray.a := (ray.ra+ray.ga+ray.ba)/3;
кон Shade;
кон TransparaVox;

тип RainbowVox*=окласс(Voxel);
перем
	r, g, b, black: SREAL;
проц SetColor* (red, green, blue,bl  : SREAL);
нач
	r := red;
	g := green;
	b := blue;
	black:=bl;
	passable := истина;
кон SetColor;

проц {перекрыта}Shade*(перем ray: Ray);
перем
	depth: SREAL;
	exit:PT;
	dr,dg,db,dblack: SREAL;
нач
	exit:=srBase.Exit(ray);
	depth:=srBase.distsquared(ray.lxyz,exit);
	dr := матМодуль(r*depth*ray.dxyz.x);
	dg := матМодуль(g*depth*ray.dxyz.y);
	db := матМодуль(b*depth*ray.dxyz.z);
	dblack:=black*depth;
	ray.r := ray.r + dr;
	ray.g := ray.g + dg;
	ray.b := ray.b + db;
	ray.ra := ray.ra - dr-dblack;
	ray.ga := ray.ga - dg-dblack;
	ray.ba := ray.ba - db-dblack;
	srBase.clamp3(ray.ra,ray.ga,ray.ba);
	ray.a := (ray.ra+ray.ga+ray.ba)/3;
кон Shade;
кон RainbowVox;


тип JelloVox*=окласс(AlphaVox);

проц {перекрыта}Shade*(перем ray: Ray);
перем
	x,y,z: SREAL;
	depth: SREAL;
	dr, dg, db: SREAL;
нач
(*	x := ray.lxyz.x-ray.xlx;
	y := ray.lxyz.y-ray.xly;
	z := ray.lxyz.z-ray.xlz;	*)
	depth := Math.sqrt(x*x+y*y+z*z);
	dr :=  r*ray.ra*depth;
	dg :=  g*ray.ga*depth;
	db :=  b*ray.ba*depth;
	ray.ra := ray.ra - dr;
	ray.ga := ray.ga - dg;
	ray.ba := ray.ba - db;
	ray.a := (ray.ra+ray.ga+ray.ba)/3;
	ray.length := ray.length + ray.scale;
кон Shade;
кон JelloVox;

тип AirVox*=окласс(ColoredVox);

проц {перекрыта}Shade*(перем ray: Ray);
перем
	x,y,z: SREAL;
	depth: SREAL;
	dr, dg, db: SREAL;

нач
(*	x := ray.lxyz.x-ray.xlx;
	y := ray.lxyz.y-ray.xly;
	z := ray.lxyz.z-ray.xlz;	*)
	depth := Math.sqrt(x*x+y*y+z*z)*srBase.fog;
	dr :=  r*ray.ra*depth;
	dg :=  g*ray.ga*depth;
	db :=  b*ray.ba*depth;
	ray.ra := ray.ra - dr;
	ray.ga := ray.ga - dg;
	ray.ba := ray.ba - db;
	ray.a := (ray.ra+ray.ga+ray.ba)/3;
	ray.length := ray.length + ray.scale;
кон Shade;
кон AirVox;

тип InkVox*=окласс(ColoredVox);

проц {перекрыта}Shade*(перем ray: Ray);
перем
	ink: SREAL;
нач
	ink := 0.05*ray.a;
	ray.ra := ray.ra - ink;
	ray.ga := ray.ga - ink;
	ray.ba := ray.ba - ink;
	ray.a := (ray.ra+ray.ga+ray.ba)/3;
	ray.length := ray.length + ray.scale;
кон Shade;
кон InkVox;

тип OutlineVox*=окласс(ColoredVox);
перем
	or, og, ob: SREAL;
проц SetOutline* (red, green, blue: SREAL);
нач
	or := red ;
	og := green ;
	ob := blue;
кон SetOutline;
проц {перекрыта}Shade*(перем ray: Ray);
перем
	ecount: цел16;
нач
	если (ray.lxyz.x< 0.01) или (ray.lxyz.x > 0.99) то увел(ecount) всё;
	если (ray.lxyz.y <0.01) или (ray.lxyz.y > 0.99) то увел(ecount) всё;
	если (ray.lxyz.z < 0.01) или (ray.lxyz.z > 0.99) то увел(ecount) всё;
	если ecount > 1 то
		ray.r := ray.r + or * ray.ra;
		ray.g := ray.g + og * ray.ga;
		ray.b := ray.b + ob * ray.ba;
		ray.ra := 0;
		ray.ga := 0;
		ray.ba := 0;
		ray.a := 0;
	иначе
		ray.r := ray.r + r * ray.ra;
		ray.g := ray.g + g * ray.ga;
		ray.b := ray.b + b * ray.ba;
		ray.ra := 0;
		ray.ga := 0;
		ray.ba := 0;
		ray.a := 0;
	всё
кон Shade;
кон OutlineVox;

тип GoutlineVox*=окласс(ColoredVox)
перем
	tx, ty, tz: SREAL;		(* thickness of outline *)
	or, og, ob: SREAL; 	(* outline color *)
проц {перекрыта}Shade*(перем ray: Ray);
перем
	ecount: цел16;
	l, le, xe, ye, ze: SREAL;
нач
	ecount := 0;
	если (ray.lxyz.x < 1/100)  то
		xe := 100*(1/100-ray.lxyz.x)
	аесли (ray.lxyz.x > 99/100) то
		xe := 00*(1-ray.lxyz.x)
	всё;
	если (ray.lxyz.y < 1/100)  то
		ye := 100*(1/100-ray.lxyz.y)
	аесли (ray.lxyz.y > 99/100) то
		ye := 100*(1-ray.lxyz.y)
	всё;
	если (ray.lxyz.z < 1/100)  то
		ze := 100*(1/100-ray.lxyz.z)
	аесли (ray.lxyz.z > 99/100) то
		ze := 100*(1-ray.lxyz.z)
	всё;
	le := (xe+ye+ze)/3;
	l := (ray.lxyz.x+ray.lxyz.y+ray.lxyz.z)/3;
	ray.r := ray.r + r * ray.ra*l;
	ray.g := ray.g + g * ray.ga*l;
	ray.b := ray.b + b * ray.ba*l;
	ray.ra := 0;
	ray.ga := 0;
	ray.ba := 0;
	ray.a := 0;
кон Shade;
кон GoutlineVox;

тип GouraudVox* = окласс(ColoredVox);

перем
	brightness: цел16;

проц & init*;
нач
	brightness := 16;
кон init;

проц {перекрыта}tick*;
нач
	если srBase.rand.Uniform()>1/2 то
		brightness := (brightness + 1) остОтДеленияНа 20;
	иначе
		brightness := (brightness - 1) остОтДеленияНа 20;
	всё
кон tick;
проц {перекрыта}Shade*(перем ray: Ray);
перем
	l: SREAL;
нач
	l := (ray.lxyz.x+ray.lxyz.y+ray.lxyz.z)/3;
	ray.r := ray.r + r * ray.ra*l;
	ray.g := ray.g + g * ray.ga*l;
	ray.b := ray.b + b * ray.ba*l;
	ray.a := (ray.ra+ray.ga+ray.ba)/3;
кон Shade;
кон GouraudVox;

тип VGouraudVox* = окласс(GouraudVox);

проц {перекрыта}Shade*(перем ray: Ray);
перем
	l: SREAL;
нач
	l := (ray.lxyz.x+ray.lxyz.y)/2;
	ray.r := ray.r + r * ray.ra*l;
	ray.g := ray.g + g * ray.ga*l;
	ray.b := ray.b + b * ray.ba*l;
	ray.a := (ray.ra+ray.ga+ray.ba)/3;
кон Shade;
кон VGouraudVox;

тип HGouraudVox* = окласс(GouraudVox);

проц {перекрыта}Shade*(перем ray: Ray);
перем
	l: SREAL;
нач
	l := (ray.lxyz.x+ray.lxyz.z)/2;
	ray.r := ray.r + r * ray.ra*l;
	ray.g := ray.g + g * ray.ga*l;
	ray.b := ray.b + b * ray.ba*l;
	ray.ra := 0;
	ray.ga := 0;
	ray.ba := 0;
	ray.a := 0;
кон Shade;
кон HGouraudVox;

тип NouraudVox* = окласс(ColoredVox);

проц {перекрыта}Shade*(перем ray: Ray);
перем
	l: SREAL;
нач
	l := 2*(матМодуль(1/2-ray.lxyz.x) + матМодуль(1/2-ray.lxyz.y) + матМодуль(1/2-ray.lxyz.z))/3;
	ray.r := ray.r + r * ray.ra*l;
	ray.g := ray.g + g * ray.ga*l;
	ray.b := ray.b + b * ray.ba*l;
	ray.ra := 0;
	ray.ga := 0;
	ray.ba := 0;
	ray.a := 0;
кон Shade;
кон NouraudVox;

тип DiffuseVox* = окласс(ColoredVox);

проц {перекрыта}Shade*(перем ray: Ray);
перем
	nx, ny, nz: цел16;
	dot: SREAL;
	inside: булево;
нач
	просей ray.face из
		0: inside := истина
		|1: nx := -1
		|2: ny := -1
		|3: nz := -1
		|4: nx := 1
		|5: ny := 1
		|6: nz := 1
	иначе
	всё;
	если inside то dot := 1 иначе dot := матМодуль(nx*ray.dxyz.x + ny*ray.dxyz.y+ nz*ray.dxyz.z) всё;
	если dot<1/2 то dot:=1/2 всё;
	ray.r := ray.r + r * ray.ra*dot ;
	ray.g := ray.g + g * ray.ga*dot;
	ray.b := ray.b + b * ray.ba*dot;
	ray.ra := 0;
	ray.ga := 0;
	ray.ba := 0;
	ray.a := 0;
кон Shade;

кон DiffuseVox;

тип DiffuseNouraudVox* = окласс(ColoredVox);

проц {перекрыта}Shade*(перем ray: Ray);
перем
	nx, ny, nz: цел16;
	dot: SREAL;
	inside: булево;
	l: SREAL;
нач
	l := 2*(матМодуль(1/2-ray.lxyz.x) + матМодуль(1/2-ray.lxyz.y) + матМодуль(1/2-ray.lxyz.z))/3;
	просей ray.face из
		0: inside := истина
		|1: nx := -1
		|2: ny := -1
		|3: nz := -1
		|4: nx := 1
		|5: ny := 1
		|6: nz := 1
	иначе
	всё;
	если inside то dot := l иначе dot := l*(1/3+2*матМодуль(nx*ray.dxyz.x + ny*ray.dxyz.y+ nz*ray.dxyz.z)/2) всё;
	ray.r := ray.r + r * ray.ra*dot;
	ray.g := ray.g + g * ray.ga*dot;
	ray.b := ray.b + b * ray.ba*dot;
	ray.ra := 0;
	ray.ga := 0;
	ray.ba := 0;
	ray.a := 0;
кон Shade;

кон DiffuseNouraudVox;

тип GridVox* = окласс(Voxel);
перем
	r, g, b, a, gr, gg, gb, ga, Z: SREAL;

проц SetColor* (red, green, blue, alpha: SREAL);
нач
	r := red * alpha;
	g := green * alpha;
	b := blue * alpha;
	a := alpha;
кон SetColor;

проц SetGridColor* (red, green, blue, alpha: SREAL);
нач
	gr := red * alpha;
	gg := green * alpha;
	gb := blue * alpha;
	ga := alpha;
кон SetGridColor;

проц SetGrid*(z: SREAL);
нач
	Z := z;
кон SetGrid;

проц {перекрыта}Shade*(перем ray: Ray);
перем
	lx, ly, x, y: SREAL;
	i, j: цел32;
нач
	просей ray.face из
		 1: lx := ray.lxyz.y; ly := ray.lxyz.z;
		|2: lx := ray.lxyz.x; ly := ray.lxyz.z;
		|3: lx := ray.lxyz.x; ly := ray.lxyz.y;
		|4: lx := ray.lxyz.y; ly := ray.lxyz.z;
		|5: lx := ray.lxyz.x; ly := ray.lxyz.z;
		|6: lx := ray.lxyz.x; ly := ray.lxyz.y;
	иначе
	всё;
	x := lx*Z; y := ly*Z;
	i := округлиВниз(x); j := округлиВниз(y);
	x := x - i; y := y - j;
	если ((x<0.1) или (y<0.1)) то
		ray.r := ray.r + gr;
		ray.g := ray.g + gg;
		ray.b := ray.b + gb;
		ray.ra := ray.ra - (gg+gb);
		ray.ga := ray.ga - (gr+gb);
		ray.ba := ray.ba - (gr+gg);
	иначе
		ray.r := ray.r + r;
		ray.g := ray.g + g;
		ray.b := ray.b + b;
		ray.ra := ray.ra - (g+b);
		ray.ga := ray.ga - (r+b);
		ray.ba := ray.ba - (r+g);
	всё;
	ray.a := (ray.ra+ray.ga+ray.ba)/3;
кон Shade;
кон GridVox;

тип GridChirkleVox* = окласс(Voxel);
перем
	r, g, b, a, Z: SREAL;

проц SetColor* (red, green, blue, alpha: SREAL);
нач
	r := red * alpha;
	g := green * alpha;
	b := blue * alpha;
	a := alpha;
	register;
кон SetColor;

проц {перекрыта}Shade*(перем ray: Ray);
перем
	dx, dy, dz, d2: SREAL;
нач
	dx := (1/2-ray.lxyz.x);
	dy := (1/2-ray.lxyz.y);
	dz := (1/2-ray.lxyz.z);
	d2 := dx*dx+dy+dy+dz+dz;
	если d2>1 то
		ray.r := ray.r + r;
		ray.g := ray.g + g;
		ray.b := ray.b + b;
		ray.ra := ray.ra - (g+b);
		ray.ga := ray.ga - (r+b);
		ray.ba := ray.ba - (r+g);
		ray.a := (ray.ra+ray.ga+ray.ba)/3;
	всё
кон Shade;

кон GridChirkleVox;

тип CheckerVox* = окласс(Voxel);
перем
	r, g, b, a, Z: SREAL;
проц SetColor* (red, green, blue, alpha: SREAL);
нач
	r := red * alpha;
	g := green * alpha;
	b := blue * alpha;
	a := alpha;
	register;
кон SetColor;
проц {перекрыта}Shade*(перем ray: Ray);
перем
	d, x,y,z, dr, dg, db: SREAL;
	ijk: srBase.IPT;
нач
	srE.E(ray.lxyz, ijk);
	x := ray.lxyz.x*2- ijk.i*2;
	y := ray.lxyz.y*2- ijk.j*2;
	z := ray.lxyz.z*2- ijk.k*2;
	d := матМодуль((1/2-x)*(1/2-x)*(1/2-z)*(Z));
	dr :=  (1- ray.lxyz.x*d)*ray.ra;
	dg :=  (1- ray.lxyz.y*d)*ray.ga;
	db :=  (1 - ray.lxyz.z*d)*ray.ba;
	ray.r := ray.r + dr;
	ray.g := ray.g + dg;
	ray.b := ray.b + db;
	ray.ra := ray.ra - (dg+db);
	ray.ga := ray.ga - (dr+db);
	ray.ba := ray.ba - (dr+dg);
	ray.a := (ray.ra+ray.ga+ray.ba)/3;
кон Shade;

проц {перекрыта}tick*;
нач
	Z := 10+(srBase.frame остОтДеленияНа 13);
кон tick;

кон CheckerVox;

тип HexaVox* = окласс(Voxel);
перем
	V: Voxel;
	hhx: SREAL;

проц&init*;
нач
	hhx := 6;
кон init;

проц setVox*(v: Voxel);
нач
	V := v;
кон setVox;

проц {перекрыта}connectmessage*;
нач
	Out.пСтроку8("HexaVox"); Out.пВК_ПС;
кон connectmessage;

проц {перекрыта}talk*(c: симв8; перем connection: булево);
нач
	просей c из
		'+': hhx := hhx + 0.05; Out.пСтроку8("hhx +."); Out.пВК_ПС;
		| 'G': hhx := hhx - 0.05; Out.пСтроку8("hhx - "); Out.пВК_ПС;
	иначе
		Out.пСтроку8(".");
	всё;
кон talk;

проц {перекрыта}Shade*(перем ray: Ray);
перем
	lx,ly: SREAL;
	Q, gray: SREAL;
нач
	просей ray.face из
		 1: lx := ray.lxyz.y; ly := ray.lxyz.z;
		|2: lx := ray.lxyz.x; ly := ray.lxyz.z;
		|3: lx := ray.lxyz.x; ly := ray.lxyz.y;
		|4: lx := ray.lxyz.y; ly := ray.lxyz.z;
		|5: lx := ray.lxyz.x; ly := ray.lxyz.z;
		|6: lx := ray.lxyz.x; ly := ray.lxyz.y;
	иначе
	всё;
	Q := srHex.hexize2(50*0.866*lx, 50*0.866*ly);
	если Q < 1/10 то
		gray := (1-Q*10);
		ray.r := ray.r - gray*ray.ra;
		ray.g := ray.g - gray*ray.ga;
		ray.b := ray.b - gray*ray.ba;
		ray.ra := ray.ra-gray;
		ray.ga := ray.ga-gray;
		ray.ba := ray.ba-gray;
		ray.a := (ray.ra+ray.ga+ray.ba)/3;
	всё;
	если V # НУЛЬ то V.Shade(ray) всё;
кон Shade;
кон HexaVox;

тип SPHexaVox*=окласс(HexaVox);

проц ctop(x,y,z: SREAL; перем th,ph: SREAL);
нач
	srBase.normalize(x,y,z);
	th := 6.28*srMath.sin(x);
	ph :=  6.28*srMath.cos(y);
кон ctop;

проц {перекрыта}Shade*(перем ray: Ray);
перем
	Q, gray: SREAL;
	th,ph: SREAL;
нач
	ctop(ray.lxyz.x,ray.lxyz.y,ray.lxyz.z,th,ph);
	Q := srHex.hexize2(3*0.866*th, 3*0.866*ph);
	если Q < 1/10 то
		gray := (1-Q*10);
		ray.ra := ray.ra - gray;
		ray.ga := ray.ga - gray;
		ray.ba := ray.ba - gray;
		ray.a := (ray.ra+ray.ga+ray.ba)/3;
	всё;
	если V # НУЛЬ то V.Shade(ray) всё;
кон Shade;

кон SPHexaVox;

тип PolkaVox* = окласс(Voxel);
перем
	brightness: цел16;
	r, g, b, rr, gg, bb: SREAL;

проц & init*;
нач
	brightness := 16;
кон init;

проц SetColor* (red, green, blue, r2, g2, b2 : SREAL);
нач
	r := srBase.clamp(red );
	g := srBase.clamp(green );
	b := srBase.clamp(blue );
	rr := srBase.clamp(r2);
	gg := srBase.clamp(g2);
	bb := srBase.clamp(b2);
кон SetColor;

проц {перекрыта}tick*;
нач
	если srBase.rand.Uniform()>1/2 то
		brightness := (brightness + 1) остОтДеленияНа 20;
	иначе
		brightness := (brightness - 1) остОтДеленияНа 20;
	всё
кон tick;

проц {перекрыта}Shade*(перем ray: Ray);
перем
	l, x, y, z: SREAL;
	nx, ny, nz: цел16;
		dot: SREAL;
	inside: булево;
нач
	просей ray.face из
		0: inside := истина
		|1: nx := -1
		|2: ny := -1
		|3: nz := -1
		|4: nx := 1
		|5: ny := 1
		|6: nz := 1
	иначе
	всё;
	если inside то dot := 1 иначе dot := матМодуль(nx*ray.dxyz.x + ny*ray.dxyz.y+ nz*ray.dxyz.z) всё; 	x := 2*матМодуль(1/2 - ray.lxyz.x);
	y := 2*матМодуль(1/2 - ray.lxyz.y);
	z := 2*матМодуль(1/2 - ray.lxyz.z);
	l := (x+y+z)/3;
	dot := dot*brightness;
	ray.r := ray.r + (r * ray.ra*l)*dot + (rr * ray.ra*(1-l))*dot ;
	ray.g := ray.g + g * ray.ga*l *dot+ (gg * ray.ga*(1-l))*dot;
	ray.b := ray.b + b * ray.ba*l*dot + (bb * ray.ba*(1-l)*dot);
	ray.ra := 0;
	ray.ga := 0;
	ray.ba := 0;
	ray.a := 0;
кон Shade;
кон PolkaVox;

тип GeckoVox* = окласс(Voxel);
перем
	r, g, b, a: SREAL;
	ecount: цел16;
проц SetColor* (red, green, blue, alpha: SREAL);
нач
	r := red * alpha;
	g := green * alpha;
	b := blue * alpha;
	a := alpha
кон SetColor;
проц {перекрыта}Shade*(перем ray: Ray);
перем
	d, dr, dg, db: SREAL;
нач
	d := матМодуль((1/2-ray.lxyz.x)*(1/2-ray.lxyz.y)*(1/2-ray.lxyz.z)*70);
	dr :=  r*ray.ra*d;
	dg :=  g*ray.ga*d;
	db :=  b*ray.ba*d;
	ray.r := ray.r + dr;
	ray.g := ray.g + dg;
	ray.b := ray.b + db;
	ray.ra := ray.ra - (dg+db);
	ray.ga := ray.ga - (dr+db);
	ray.ba := ray.ba - (dr+dg);
	ray.a := (ray.ra+ray.ga+ray.ba)/3;
кон Shade;
кон GeckoVox;

(*TYPE SerpVox* = OBJECT(Voxel);
VAR
	r1, g1, b1, r2, g2, b2: SREAL;

PROCEDURE SetColor1* (r, g, b: SREAL);
BEGIN
	r1 := r;
	g1 := g;
	b := b
END SetColor1;

PROCEDURE SetColor2* (r, g, b: SREAL);
BEGIN
	r2 := r;
	g2 := g;
	b2 := b
END SetColor2;

PROCEDURE Shade (VAR ray: Ray);
VAR
	i, j, k: SIGNED32;
	sc, d: SIGNED16;
BEGIN
	ray.splitme := TRUE;
	sc := 0;
	d := 3;
	WHILE d > 0 DO
		IF (1 / 3 < ray.lx) & (ray.lx < 2 / 3) THEN INC(sc) END;
		IF (1 / 3 < ray.ly) & (ray.ly < 2 / 3) THEN INC(sc) END;
		IF (1 / 3 < ray.lz) & (ray.lz < 2 / 3) THEN INC(sc) END;
		IF sc < 2 THEN
			sc := 0;
			IF ray.lx >= 2 / 3  THEN
				ray.lx := ray.lx - 2 / 3
			ELSIF ray.lx >= 1 / 3 THEN
				ray.lx := ray.lx - 1 / 3
			END;
			ray.lx := ray.lx * 3;
			IF ray.ly >= 2 / 3  THEN
				ray.ly := ray.ly - 2 / 3
			ELSIF ray.ly >= 1 / 3 THEN
				ray.ly := ray.ly - 1 / 3
			END;
			ray.ly := ray.ly * 3;
			IF ray.lz >= 2 / 3  THEN
				ray.lz := ray.lz - 2 / 3
			ELSIF ray.lz >= 1 / 3 THEN
				ray.lz := ray.lz - 1 / 3
			END;
			ray.lz := ray.lz * 3
		END;
		DEC(d)
	END;
	IF sc > 1 THEN
		ray.r := ray.r + r1 * ray.ra * ray.lx;
		ray.g := ray.g + g1 * ray.ga * ray.ly;
		ray.b := ray.b + b1 * ray.ba * ray.lz;
		ray.ra := 0;
		ray.ga := 0;
		ray.ba := 0;
	ELSE
		ray.r := ray.r + r2 * ray.ra;
		ray.g := ray.g + g2 * ray.ga;
		ray.b := ray.b + b2 * ray.ba;
	END
END Shade;

END SerpVox;
*)

тип BiVox* = окласс(Voxel);
перем
	v1, v2: Voxel;

проц set*(x,y: Voxel);
нач
	v1 := x;
	v2 := y;
кон set;

проц {перекрыта}probe*(x,y,z: SREAL):Voxel;
перем
	v: Voxel;
нач
	v := v1.probe(x,y,z);
	v := v2.probe(x,y,z);
	возврат(сам);
кон probe;

проц {перекрыта}Shade*(перем ray: Ray);
нач
 v1.Shade(ray);
 v2.Shade(ray);
кон Shade;

кон BiVox;

(*
TYPE SphBiVox* = OBJECT(BiVox);
VAR
	cx, cy, cz, R2: SREAL;

PROCEDURE&init;
BEGIN
	cx := 1/2; cy := 1/2; cz :=1/2;
	R2 := 0.3;
END init;

PROCEDURE tick*;
BEGIN
	R2 := 1/3 + ((srBase.frame MOD 10)-4)/450;
END tick;

PROCEDURE Shade (VAR ray: Ray);
VAR
	r2: SREAL;
	x,y,z,ax, ay, az, bx, by, bz : SREAL;
	i: SIGNED16;
BEGIN
	r2 := (cx-ray.lx)*(cx-ray.lx) + (cy-ray.ly)*(cy-ray.ly) + (cz-ray.lz)*(cz-ray.lz);
	IF r2 < R2 THEN (* ray is within sphere *)
		IF v2 # NIL THEN v2.Shade(ray) END;
		IF ray.a > 1/10 THEN
			ax := ray.lx; ay := ray.ly; az := ray.lz;
			bx := ray.lx + ray.dx; by := ray.ly+ ray.dy; bz := ray.lz+ ray.dz;
			x := (ax+bx)/2; y := (ay+by)/2; z := (az + bz)/2;
			FOR i := 0 TO 12 DO
				r2 := (cx-x)*(cx-x) + (cy-y)*(cy-y) + (cz-z)*(cz-z);
				IF r2 > R2 THEN
					bx := x; by := y; bz := z
				ELSE
					ax := x; ay := y; az := z
				END;
				x := (ax+bx)/2; y := (ay+by)/2; z := (az + bz)/2;
			END;
			IF ray.a > 1/10 THEN
				ray.lx := x; ray.ly := y; ray.lz := z;
				IF v1 # NIL THEN v1.Shade(ray) END
			END
		END
	ELSE
		IF v1 # NIL THEN v1.Shade(ray) END
	END;
END Shade;

END SphBiVox;
*)
тип FuzzyTVox*=окласс(AlphaVox);
перем
	fuzzdivisor, fuzzsubtract: SREAL;
проц & init*;
нач
	passable := истина;
	fuzzdivisor := 100;
	fuzzsubtract := 0.005
кон init;

проц setFuzz*(f: SREAL);
нач
	fuzzdivisor := f;
	fuzzsubtract := 1/(2*fuzzdivisor)
кон setFuzz;

проц {перекрыта}Shade*(перем ray: Ray);
перем
	dr, dg, db: SREAL;
нач
	ray.xyz.x := ray.xyz.x + srBase.rand.Uniform()/fuzzdivisor-fuzzsubtract;
	ray.xyz.y := ray.xyz.y + srBase.rand.Uniform()/fuzzdivisor-fuzzsubtract;
	ray.xyz.z:= ray.xyz.z + srBase.rand.Uniform()/fuzzdivisor-fuzzsubtract;
	dr :=  r*ray.ra;
	dg :=  g*ray.ga;
	db :=  b*ray.ba;
	ray.r := ray.r + dr;
	ray.g := ray.g + dg;
	ray.b := ray.b + db;
	ray.ra := ray.ra - (dg+db);
	ray.ga := ray.ga - (dr+db);
	ray.ba := ray.ba - (dr+dg);
	ray.a := (ray.ra+ray.ga+ray.ba)/3;
кон Shade;

кон FuzzyTVox;

проц mirror(перем ray: Ray);
нач
	просей ray.face из
		1: 	 ray.dxyz.x:= -ray.dxyz.x;
		|2:	ray.dxyz.y:= -ray.dxyz.y;
		|3:	ray.dxyz.z:= -ray.dxyz.z;
		|4: 	ray.dxyz.x:= -ray.dxyz.x;
		|5:	ray.dxyz.y:= -ray.dxyz.y;
		|6:	ray.dxyz.z:= -ray.dxyz.z;
	иначе
	всё;
	ray.changed := истина;
кон mirror;

кон srVoxel.

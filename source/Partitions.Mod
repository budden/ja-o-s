модуль Partitions; (** AUTHOR "staubesv"; PURPOSE "Commandline front-end for PartitionsLib"; *)
(**
 * This is the commandline front-end for PartitionsLib.
 *
 * Command overview:
 *
 * Uncritical operations:
 *
 *	Partitions.Show ~  								Show all disk devices and their partition layout
 * 	Partitions.Show detail ~							Show detailed information about all disk devices and their partition layout
 *	Partitions.ShowAosFSLimits ~ 					Show limitations of the Aos File System
 *
 * 	Partitions.ShowOps~   							Show all pending disk operations
 * 	Partitions.ShowOps detail ~						Show detailed information about all pending disk operations
 *
 *	Partitions.ShowOp uid ~							Show detailed status of the specified operation
 *	Partitions.Abort <opID> ~ 						Abort the disk operation with the specified operation ID
 * 	Partitions.Remove <opID> ~						Abort the disk operation with the specified operation ID and remove it from the operation manager
 *
 *	Partitions.Check dev#part ~						Perform a read test on the specified partition
 *	Partitions.Eject dev ~								Eject medium of the specified device
 *	Partitions.Sync dev ~								Synchronize device caches to medium
 *
 * 	Partitions.Safe~ 									Disallow extremely critical operations
 * 	Partitions.Unsafe~								Allow extermely critical operations
 *
 *	Partitions.ShowBlocks dev#part first nbr			Show <nbr> blocks starting at block <first> of the specified partition
 *
 * Critical operations:
 *
 *	Partitions.Create dev#part type sizeMB ~			Create a partition
 *	Partitions.Delete dev#part type ~					Delete the specified partition
 *	Partitions.Activate dev#part ~					Set the active bit (boot) of the specified partition
 *	Partitions.Deactivate dev#part ~					Clear the active bit (boot) of the specified partition
 *	Partitions.ChangeType dev#part from to ~			Change the type of the specified partition from <from> to <to>
 * 	Partitions.Format dev#part fs ~					Format the specified partition with the specified file system (AosFS, FatFS)
 *	Partitions.WriteMBR dev#part ~					Write MBR boot code to specified partition (partition table will be untouched)
 *
 *	Partitions.InstallBootManager dev#0 ~			Install boot manager
 *	Partitions.PartitionToFile dev#part file f nbr~ 		Write <nbr> blocks starting at block <f> to the specified file.
 *	Partitions.FileToPartition dev#part file f nbr~		Write the content of <file> to the specified partition starting at block <f>, <nbr> blocks
 *
 * Bluebottle-specific opertaions
 *
 *	Partitions.UpdateBootFile dev#part bootfile		Update the boot file (e.g. IDE.Bin) for the specified partition
 *	Partitions.UpdateBootLoader dev#part bl			Update the boot loader (e.g. OBL.Bin) for the specified partition
 *
 *	Partitions.GetConfig dev#part ~					Get the config string of the specified partition
 * 	Partitions.SetConfig dev#part config				Set the config string for the specified partition
 *
 *
 * History:
 *
 * 	05.08.2005	Cleanup (staubesv)
 *	25.11.2005	Added ShowOp procedure (staubesv)
 *	06.01.2006	Small cleanup (staubesv)
 *	17.01.2006	WriteMBR: Caller can specify "DESTROY" parameter, fixed SetConfig (staubesv)
 *)

использует ЛогЯдра, Texts, TextUtilities, Disks, Files, Lib := PartitionsLib, Plugins, Commands, Потоки, Строки8, FATScavenger;

конст

	Trace = ложь;

	Invalid = матМинимум(цел32);

	(* InstallBootManager default arguments *)
	BootManagerMBRFile = "BootManagerMBR.Bin";
	BootManagerTailFile = "BootManagerTail.Bin";

(** Show all currently pending disk operations *)
проц ShowOps*(context : Commands.Context); (** [detail] ~ *)
перем par : массив 10 из симв8; details : булево;
нач
	par := ""; context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(par);
	details := (par = "detail");
	Lib.operations.Show(context.out, details);
кон ShowOps;

(** Show the detailed state of the specified operation *)
проц ShowOp*(context : Commands.Context); (** uid ~ *)
перем operation : Lib.Operation; uid : цел32;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦел32(uid, ложь)  то
		operation := Lib.operations.GetByUid(uid);
		если operation # НУЛЬ то
			operation.Show(context.out, истина);
		иначе
			context.error.пСтроку8("Error: Operation UID "); context.error.пЦел64(uid, 0); context.error.пСтроку8(" not found"); context.error.пВК_ПС;
			context.result := Commands.CommandError;
		всё;
	иначе
		context.error.пСтроку8("Expected parameters: uid"); context.error.пВК_ПС;
		context.result := Commands.CommandParseError;
	всё;
кон ShowOp;

(** Abort a running operation *)
проц Abort*(context : Commands.Context); (** uid  ~ *)
перем operation : Lib.Operation; uid : цел32;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦел32(uid, ложь) то
		operation := Lib.operations.GetByUid(uid);
		если operation # НУЛЬ то
			operation.Abort;
			context.out.пСтроку8("Operation UID "); context.out.пЦел64(uid, 0); context.out.пСтроку8(" aborted"); context.out.пВК_ПС;
		иначе
			context.error.пСтроку8("Error: Operation UID "); context.error.пЦел64(uid, 0); context.error.пСтроку8(" not found"); context.error.пВК_ПС;
			context.result := Commands.CommandParseError;

		всё;
	иначе
		context.error.пСтроку8("Expected parameters: uid"); context.error.пВК_ПС;
		context.result := Commands.CommandError;
	всё;
кон Abort;

(** Remove (and if necessary abort) operations from the operations registry *)
проц Remove*(context : Commands.Context); (** uid | "all" | "finished" ~*)
перем par : массив 128 из симв8; uid, num : цел32;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦел32(uid, ложь) то
		если Lib.operations.RemoveByUid(uid) то
			context.out.пСтроку8("Operation UID "); context.out.пЦел64(uid, 0); context.out.пСтроку8(" has been removed"); context.out.пВК_ПС;
		иначе
			context.error.пСтроку8("Error: Could not remove operation UID "); context.error.пЦел64(uid, 0); context.error.пВК_ПС;
			context.result := Commands.CommandError;
		всё;
	аесли context.arg.кодВозвратаПоследнейОперации = Потоки.НеверныйФормат то
		par := "";
		context.arg.ПерейдиКМестуВПотоке(0);
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(par)  то
			Строки8.СтрокуВВерхнийРегистрASCII(par);
			если par = "ALL" то
				num := Lib.operations.RemoveAll(истина);
				context.out.пЦел64(num, 0); context.out.пСтроку8(" operations have been removed"); context.out.пВК_ПС;
			аесли par = "FINISHED" то
				context.out.пСтроку8("All finished operation have been removed"); context.out.пВК_ПС;
			иначе
				context.error.пСтроку8("Expected parameters: uid | all | finished"); context.error.пВК_ПС;
				context.result := Commands.CommandParseError;
			всё;
		иначе
			context.error.пСтроку8("Expected parameters: uid | all | finished"); context.error.пВК_ПС;
			context.result := Commands.CommandParseError;
		всё;
	иначе
		context.error.пСтроку8("Expected parameters: uid | all | finished"); context.error.пВК_ПС;
		context.result := Commands.CommandParseError;
	всё;
кон Remove;

проц Mount*(context : Commands.Context);
перем
	mount :Lib.Mount;
	prefix, alias, volumePars, fsPars : массив 64 из симв8;
	selection : Lib.Selection;
нач
	если GetSelection(context, истина, selection) то
		volumePars := ""; fsPars := "";
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(alias) и context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(prefix) то
			нов(mount, selection.disk, selection.partition, context.out);
			mount.SetParameters(prefix, alias, volumePars, fsPars);
			mount.SetBlockingStart;
			если Lib.StatusError в mount.state.status то context.result := Commands.CommandError всё;
		иначе
			context.error.пСтроку8("Expected parameters: dev#part alias prefix"); context.error.пВК_ПС;
			context.result := Commands.CommandError;
		всё;
	всё;
кон Mount;

(* Format a partition with an N2KFS, AosFS or FatFS. *)
проц Format*(context : Commands.Context); (** dev#part [ "AosFS" | "NatFS" | "NatFS2" [ FSRes [ BootFile [ Flag ] ] ] ] | ["FatFS" ["Quick"]] ~ *)
перем
	formatAos : Lib.FormatPartition; formatFat : FATScavenger.FormatPartition;
	fsname, bootfile, quick : массив 128 из симв8;
	fsRes, flags : цел32;
	quickFormat : булево;
	selection : Lib.Selection;
нач
	если GetSelection(context, истина, selection) то
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fsname) то
			если fsname = "FatFS" то
				quickFormat := context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(quick) и (quick = "Quick");
				нов(formatFat, selection.disk, selection.partition, context.out);
				formatFat.SetParameters(Строки8.ЯвиУСтроку("no name"), quickFormat);
				formatFat.SetBlockingStart;
				если Lib.StatusError в formatFat.state.status то context.result := Commands.CommandError всё;
				context.out.пСтроку8("Partitions UID "); context.out.пЦел64(formatFat.uid, 0);
				context.out.пСтроку8(": Started FAT format on "); context.out.пСтроку8(formatFat.diskpartString); context.out.пВК_ПС;
			аесли (fsname = "AosFS") или (fsname = "NatFS") или (fsname = "NatFS1") или (fsname = "NatFS2") то
				bootfile := ""; fsRes := -2; flags := 0;
				если context.arg.ПропустиБелоеПолеИЧитайЦел32(fsRes, ложь) то
					если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(bootfile) то
						context.arg.ПропустиБелоеПоле; context.arg.чЦел32(flags, ложь);
					всё;
				всё;
				нов(formatAos, selection.disk, selection.partition, context.out);
				formatAos.SetParameters(fsname, bootfile, fsRes, flags);
				formatAos.SetBlockingStart;
				если Lib.StatusError в formatAos.state.status то context.result := Commands.CommandError всё;
			иначе
				context.error.пСтроку8("File system "); context.error.пСтроку8(fsname); context.error.пСтроку8(" is not supported"); context.error.пВК_ПС;
				context.result := Commands.CommandError;
			всё;
		иначе (* optional parameters not specified *)
			нов(formatAos, selection.disk, selection.partition, context.out);
			formatAos.SetParameters("AosFS", "", -2, 0);
			formatAos.SetBlockingStart;
			если Lib.StatusError в formatAos.state.status то context.result := Commands.CommandError всё;
			context.out.пСтроку8("Partitions UID "); context.out.пЦел64(formatAos.uid, 0);
			context.out.пСтроку8(": Started format on "); context.out.пСтроку8(formatAos.diskpartString); context.out.пВК_ПС;
		всё;
	всё;
кон Format;

(* Update the boot file in an existing Oberon partition. *)
проц UpdateBootFile*(context : Commands.Context); (** dev#part BootFile ~ *)
перем
	updateBootFile : Lib.UpdateBootFile;
	selection : Lib.Selection;
	filename : Files.FileName;
нач
	если GetSelection(context, ложь, selection) то
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename) то
			нов(updateBootFile, selection.disk, selection.partition, context.out);
			updateBootFile.SetParameters(filename);
			updateBootFile.SetBlockingStart;
			если Lib.StatusError в updateBootFile.state.status то context.result := Commands.CommandError всё;
		иначе
			context.error.пСтроку8("Expected parameters: dev#part bootfilename"); context.error.пВК_ПС;
			context.result := Commands.CommandParseError;
		всё;
	всё;
кон UpdateBootFile;

проц GetConfig*(context : Commands.Context);	(** dev#part  *)
конст MaxSize = 2048;
перем
	getConfig : Lib.GetConfig;
	selection : Lib.Selection;
	configuration : Lib.Configuration;
	table : Потоки.ПисарьВСтроку8ФиксированногоРазмера;
	string : массив MaxSize из симв8;
	i : цел32;
нач
	если GetSelection(context, истина, selection) то
		нов(getConfig, selection.disk, selection.partition, context.out);
		getConfig.SetBlockingStart;
		если Lib.StatusError в getConfig.state.status то
			context.result := Commands.CommandError;
		иначе
			нов(configuration);
			configuration.table := getConfig.GetTable();
			table := configuration.GetTableAsString();
			table.ДайПрочитанное˛сколькоПоместитсяИСимвол0(string);
			(* Commands uses the quote character to separate commands *)
			нцДля i := 0 до длинаМассива(string)-1 делай если string[i] = ";" то string[i] := ","; всё; кц;
			context.out.пСтроку8("Partitions.SetConfig "); context.out.пСтроку8(getConfig.diskpartString); context.out.пВК_ПС;
			context.out.пСтроку8(string); context.out.пВК_ПС;
		всё;
	всё;
кон GetConfig;

(* Write the specified configuration string to the specified partition.							*)
(* Notes:																					*)
(*	- Use the "," character to separate commands 											*)
(*	- After an "#" character, the rest of the line is skipped/ignored							*)
проц SetConfig*(context : Commands.Context); (** dev#part { str = "val" } ~ *)
перем
	setConfig : Lib.SetConfig;
	selection : Lib.Selection;
	configString : Строки8.уСтрока;
	ch : симв8;
	i : цел32;
нач
	если GetSelection(context, истина, selection) то
		если context.arg.можноЛиПерейтиКМестуВПотоке¿() то
			(* append character "~" to config string *)
			нов(configString, context.arg.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() + 1);
			i := 0;
			нцПока (context.arg.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() > 0) делай
				context.arg.чСимв8(ch);
				если (ch = ",") то
					configString[i] := ";"; (* Commands uses comma character to separate commands *)
				иначе
					configString[i] := ch;
				всё;
				увел(i);
			кц;
			configString[i-1] := "~";
			configString[i] := 0X;
			нов(setConfig, selection.disk, selection.partition, context.out);
			setConfig.SetParameters(configString, 0);
			setConfig.SetBlockingStart;
			если Lib.StatusError в setConfig.state.status то context.result := Commands.CommandError всё;
		иначе
			context.error.пСтроку8("Expected argument stream that supports SetPos"); context.error.пВК_ПС;
			context.result := Commands.CommandError;
		всё;
	всё;
кон SetConfig;

(** Perform a read check on partition *)
проц Check*(context : Commands.Context); (** dev#part *)
перем
	selection : Lib.Selection;
	checkPartition : Lib.CheckPartition;
нач
	если GetSelection(context, ложь, selection) то
		нов(checkPartition, selection.disk, selection.partition, context.out);
		checkPartition.SetBlockingStart;
		если Lib.StatusError в checkPartition.state.status то context.result := Commands.CommandError всё;
	иначе (* skip; error written to <w> by ScanOpenPart *)
	всё;
кон Check;

(** Change the type of dev#part from oldtype to newtype *)
проц ChangeType*(context : Commands.Context); (** dev#part oldtype newtype ~ *)
перем
	change : Lib.ChangePartType;
	oldtype, newtype : цел32;
	selection : Lib.Selection;
нач
	если GetSelection(context, истина, selection) то
		если ~selection.disk.isDiskette то
			если context.arg.ПропустиБелоеПолеИЧитайЦел32(oldtype, ложь) и (oldtype > 0) и (oldtype < 256) то
				если context.arg.ПропустиБелоеПолеИЧитайЦел32(newtype, ложь) и (newtype > 0) и (newtype < 256) то
					нов(change, selection.disk, selection.partition, context.out);
					change.SetParameters(oldtype, newtype);
					change.SetBlockingStart;
					если Lib.StatusError в change.state.status то context.result := Commands.CommandError всё;
				иначе
					context.error.пСтроку8("Expected parameters: dev#part oldtype newtype, failed to parse newtype"); context.error.пВК_ПС;
					context.result := Commands.CommandParseError;
				всё;
			иначе
				context.error.пСтроку8("Expected parameters: dev#part oldtype newtype, failed to parse oldtype"); context.error.пВК_ПС;
				context.result := Commands.CommandParseError;
			всё;
		иначе
			context.error.пСтроку8("Operation not support for floppy disk drives."); context.error.пВК_ПС;
			context.result := Commands.CommandError;
		всё;
	всё;
кон ChangeType;

(** Delete a partition *)
проц Delete*(context : Commands.Context); (** dev#part type ~ *)
перем
	delete : Lib.DeletePartition;
	selection : Lib.Selection;
	type : цел32;
нач
	если GetSelection(context, ложь, selection) то
		если ~selection.disk.isDiskette то
			если context.arg.ПропустиБелоеПолеИЧитайЦел32(type, ложь) и (type > 0) и (type < 256) то
				нов(delete, selection.disk, selection.partition, context.out);
				delete.SetParameters(type);
				delete.SetBlockingStart;
				если Lib.StatusError в delete.state.status то context.result := Commands.CommandError всё;
			иначе
				context.error.пСтроку8("Expected parameters: dev#part type sizeMB, error while parsing type"); context.error.пВК_ПС;
				context.result := Commands.CommandParseError;
			всё;
		иначе
			context.error.пСтроку8("Operation not supported for floppy disks"); context.error.пВК_ПС;
			context.result := Commands.CommandError;
		всё;
	всё;
кон Delete;

проц Create*(context : Commands.Context); (** dev#part type sizeMB ~ *)
перем
	create : Lib.CreatePartition;
	selection : Lib.Selection;
	type, size : цел32;
нач
	если GetSelection(context, ложь, selection) то
		если ~selection.disk.isDiskette то
			если context.arg.ПропустиБелоеПолеИЧитайЦел32(type, ложь) и (type > 0) и (type < 256) то
				если context.arg.ПропустиБелоеПолеИЧитайЦел32(size, ложь) и (size > 0) то
					нов(create, selection.disk, selection.partition, context.out);
					create.SetParameters(size, type, ложь);
					create.SetBlockingStart;
					если Lib.StatusError в create.state.status то context.result := Commands.CommandError всё;
				иначе
					context.error.пСтроку8("Expected parameters: dev#part type sizeMB, error while parsing size"); context.error.пВК_ПС;
					context.result := Commands.CommandParseError;
				всё;
			иначе
				context.error.пСтроку8("Expected parameters: dev#part type sizeMB, error while parsing type"); context.error.пВК_ПС;
				context.result := Commands.CommandParseError;
			всё;
		иначе
			context.error.пСтроку8("Operation not supported on floppy disks"); context.error.пВК_ПС;
			context.result := Commands.CommandError;
		всё;
	всё;
кон Create;

(** Mark partition as active *)
проц Activate*(context : Commands.Context); (** dev#part ~ *)
нач
	ChangeActiveBit(истина, context);
кон Activate;

(** Mark partition as inactive *)
проц Deactivate*(context : Commands.Context); (** dev#part ~ *)
нач
	ChangeActiveBit(ложь, context);
кон Deactivate;

проц ChangeActiveBit(active : булево; context : Commands.Context);
перем
	setFlags : Lib.SetFlags;
	selection : Lib.Selection;
нач
	если GetSelection(context, истина, selection) то
		если ~selection.disk.isDiskette то
			нов(setFlags, selection.disk, selection.partition, context.out);
			setFlags.SetParameters(active);
			setFlags.SetBlockingStart;
			если Lib.StatusError в setFlags.state.status то context.result := Commands.CommandError всё;
		иначе
			context.error.пСтроку8("Operation not supported for floppy disks"); context.error.пВК_ПС;
			context.result := Commands.CommandError;
		всё;
	всё;
кон ChangeActiveBit;

(** Write <numblock> sectors from a file to a partition, starting at block <block> *)
проц FileToPartition*(context : Commands.Context); (** dev#part filename [block numblocks] ~ *)
перем
	fileToPartition : Lib.FileToPartition;
	filename : массив 128 из симв8;
	block, numblocks : цел32;
	selection : Lib.Selection;
нач
	если GetSelection(context, истина, selection) то
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename) то
			если context.arg.ПропустиБелоеПолеИЧитайЦел32(block, ложь) то
				если ~context.arg.ПропустиБелоеПолеИЧитайЦел32(numblocks, ложь) то
					context.error.пСтроку8("Expected parameters: dev#part filename [block numblocks], failed to parse numblocks"); context.error.пВК_ПС;
					context.result := Commands.CommandParseError;
					возврат;
				всё;
			иначе (* optional parameters not specified *)
				block := -1; numblocks := -1;
			всё;
			нов(fileToPartition, selection.disk, selection.partition, context.out);
			fileToPartition.SetParameters(filename, block, numblocks);
			fileToPartition.SetBlockingStart;
			если Lib.StatusError в fileToPartition.state.status то context.result := Commands.CommandError всё;
		иначе
			context.error.пСтроку8("Expected parameters: dev#part name [block numblocks], failed to parse filename"); context.error.пВК_ПС;
			context.result := Commands.CommandParseError;
		всё;
	всё;
кон FileToPartition;

(** Write <numblock> sectors from a partition to a file, starting at block <block>.
	If the optional parameters are not specified, store whole partition into file *)
проц PartitionToFile*(context : Commands.Context); (** dev#part filename [block numblocks] ~ *)
перем
	partitionToFile : Lib.PartitionToFile;
	filename : массив 128 из симв8;
	block, numblocks : цел32;
	selection : Lib.Selection;
нач
	если GetSelection(context, истина, selection) то
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename) то
			если context.arg.ПропустиБелоеПолеИЧитайЦел32(block, ложь) то
				если ~context.arg.ПропустиБелоеПолеИЧитайЦел32(numblocks, ложь) то
					context.error.пСтроку8("Expected parameters: dev#part filename [block numblocks], failed to parse numblocks"); context.error.пВК_ПС;
					context.result := Commands.CommandParseError;
					возврат;
				всё;
			иначе (* optional parameters not specified *)
				block := -1; numblocks := -1;
			всё;
			нов(partitionToFile, selection.disk, selection.partition, context.out);
			partitionToFile.SetParameters(filename, block, numblocks);
			partitionToFile.SetBlockingStart;
			если Lib.StatusError в partitionToFile.state.status то context.result := Commands.CommandError всё;
			context.out.пСтроку8("Partitions UID "); context.out.пЦел64(partitionToFile.uid, 0); context.out.пСтроку8(": Started PartitionToFile on ");
			context.out.пСтроку8(partitionToFile.diskpartString); context.out.пВК_ПС;
		иначе
			context.error.пСтроку8("Expected parameters: dev#part name [block numblocks], failed to parse filename"); context.error.пВК_ПС;
			context.result := Commands.CommandParseError;
		всё;
	всё;
кон PartitionToFile;

(** Write the specified Master Boot Record (MBR) to the specified partition. The partition table will be preserved 	*)
(*	unless the optional parameter "DESTROY" is specified.														*)
(*	WARNING: Using the DESTROY parameter will render any disk content unusable.								*)
проц WriteMBR*(context : Commands.Context); (** dev#0 filename ["DESTROY"] ~ *)
перем
	writeMBR : Lib.WriteMBR;
	filename, destroy : массив 128 из симв8;
	selection : Lib.Selection;
нач
	если GetSelection(context, ложь, selection) то
		если ~selection.disk.isDiskette то
			если selection.partition = 0 то
				если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename) то
					нов(writeMBR, selection.disk, selection.partition, context.out);
					если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(destroy) и (destroy = "DESTROY") то
						writeMBR.SetParameters(filename, ложь, ложь);
					иначе
						writeMBR.SetParameters(filename, истина, ложь);
					всё;
					writeMBR.SetBlockingStart;
					если Lib.StatusError в writeMBR.state.status то context.result := Commands.CommandError всё;
				иначе
					context.error.пСтроку8("Expected parameters: dev#0 filename, failed to parse filename"); context.error.пВК_ПС;
					context.result := Commands.CommandParseError;
				всё;
			иначе
				context.error.пСтроку8("Expected parameters: dev#0 filename, partition is not 0"); context.error.пВК_ПС;
				context.result := Commands.CommandParseError;
			всё;
		иначе
			context.error.пСтроку8("Operation not supported for floppy disks"); context.error.пВК_ПС;
			context.result := Commands.CommandError;
		всё;
	всё;
кон WriteMBR;

(** Update the boot loader OBL in an existing AosFS partition, replacing it by the new BBL handling the Init string differently.
The BBL must imperatively have the same size, 4 blocks, as the OBL. The same BBL is applicable to all AosFS partitions. *)
проц UpdateBootLoader*(context : Commands.Context); (** dev#part BootLoader ~ *)
перем
	updateLoader : Lib.UpdateBootLoader;
	selection : Lib.Selection;
	filename : Files.FileName;
нач
	если GetSelection(context, ложь, selection) то
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename) то
			нов(updateLoader, selection.disk, selection.partition, context.out);
			updateLoader.SetParameters(filename);
			updateLoader.SetBlockingStart;
			если Lib.StatusError в updateLoader.state.status то context.result := Commands.CommandError всё;
		иначе
			context.error.пСтроку8("Expected parameters: dev#part bootloader"); context.error.пВК_ПС;
			context.result := Commands.CommandError;
		всё;
	всё;
кон UpdateBootLoader;

(** Install boot manager on the specified device *)
проц InstallBootManager*(context : Commands.Context); (** dev#0 [BootManagerMBR [BootManagerTail]] ~ *)
перем installBootManager : Lib.InstallBootManager; selection : Lib.Selection; mbrFile, tailFile : Files.FileName;
нач
	если GetSelection(context, ложь, selection) то
		если ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(mbrFile) то mbrFile := BootManagerMBRFile; всё;
		если ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(tailFile) то tailFile := BootManagerTailFile; всё;
		нов(installBootManager, selection.disk, selection.partition, context.out);
		installBootManager.SetParameters(mbrFile, tailFile);
		installBootManager.SetBlockingStart;
		если Lib.StatusError в installBootManager.state.status то context.result := Commands.CommandError всё;
	всё;
кон InstallBootManager;

проц ShowBlockCallback(text : Texts.Text);
перем string : Строки8.уСтрока;
нач
	text.AcquireRead;
	нов(string, text.GetLength()); TextUtilities.TextToStr(text, string^);
	text.ReleaseRead;
	ЛогЯдра.пСтроку8(string^); ЛогЯдра.пВК_ПС;
кон ShowBlockCallback;

проц ShowBlocks*(context : Commands.Context); (** dev#part block [numblocks] ~ *)
перем
	showBlocks : Lib.ShowBlocks;
	block, numblocks : цел32;
	selection : Lib.Selection;
нач
	если GetSelection(context, ложь, selection) то
		если context.arg.ПропустиБелоеПолеИЧитайЦел32(block, ложь) то
			если ~context.arg.ПропустиБелоеПолеИЧитайЦел32(numblocks, ложь) то
				(* optional parameter not specified *) numblocks := -1;
			всё;
			нов(showBlocks, selection.disk, selection.partition, context.out);
			showBlocks.SetParameters(block, numblocks);
			showBlocks.SetCallback(ShowBlockCallback);
			showBlocks.SetBlockingStart;
			если Lib.StatusError в showBlocks.state.status то context.result := Commands.CommandError всё;
		иначе
			context.error.пСтроку8("Expected parameters: dev#part block [numblocks], failed to parse block"); context.error.пВК_ПС;
			context.result := Commands.CommandParseError;
		всё;
	всё;
кон ShowBlocks;

(** Eject medium of device dev *)
проц Eject*(context : Commands.Context); (** dev ~ *)
перем
	plugin : Plugins.Plugin;
	dev : Disks.Device;
	name : массив 32 из симв8;
	temp: массив 256 из симв8;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name)  то
		plugin := Disks.registry.Get(name);
		если plugin # НУЛЬ то
			dev := plugin (Disks.Device);
			Lib.Eject(dev, temp); context.out.пСтроку8(temp); context.out.пВК_ПС;
		иначе
			context.error.пСтроку8("Device "); context.error.пСтроку8(name); context.error.пСтроку8(" not found"); context.error.пВК_ПС;
			context.result := Commands.CommandError;
		всё;
	иначе
		context.error.пСтроку8("Expected parameters: dev"); context.error.пВК_ПС;
		context.result := Commands.CommandParseError;
	всё;
кон Eject;

(** Sync device to medium *)
проц Sync*(context: Commands.Context); (** dev ~ *)
перем
	plugin: Plugins.Plugin;
	dev: Disks.Device;
	name: массив 32 из симв8;
	temp: массив 256 из симв8;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name) то
		plugin := Disks.registry.Get(name);
		если plugin # НУЛЬ то
			dev := plugin (Disks.Device);
			Lib.Sync(dev, temp); context.out.пСтроку8(temp); context.out.пВК_ПС;
		иначе
			context.error.пСтроку8("Device "); context.error.пСтроку8(name); context.error.пСтроку8(" not found"); context.error.пВК_ПС;
			context.result := Commands.CommandError;
		всё;
	иначе
		context.error.пСтроку8("Expected parameters: dev"); context.error.пВК_ПС;
		context.result := Commands.CommandParseError;
	всё;
кон Sync;

проц Unsafe*(context : Commands.Context); (** ~ *)
нач
	Lib.safe := ложь;
	context.out.пСтроку8("NOW in UNSAFE mode!"); context.out.пВК_ПС;
кон Unsafe;

проц Safe*(context : Commands.Context); (** ~ *)
нач
	Lib.safe := истина;
	context.out.пСтроку8("Now in safe mode"); context.out.пВК_ПС;
кон Safe;

(** Show all disk devices and their partition layout. *)
проц Show*(context : Commands.Context); (** ["detail"] ~ *)
перем
	diskTable : Lib.Disks; disk : Lib.Disk;
	par : массив 10 из симв8;
	verbose : булево;
	i : размерМЗ;
	temp : массив 256 из симв8;
нач
	par := ""; context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(par);
	verbose := (par = "detail");
	Lib.diskModel.Update;
	Lib.diskModel.Acquire;
	diskTable := Lib.diskModel.disks;
	если diskTable # НУЛЬ то
		нцДля i := 0 до длинаМассива(diskTable)-1 делай
			disk := diskTable[i];
			ShowDevice(context, disk, verbose);
			если disk.res # Disks.MediaMissing то
				если (disk.table # НУЛЬ) то
					ShowTable(context, disk, disk.table, verbose)
				иначе
					Lib.GetErrorMsg("Error", disk.res, temp); context.error.пСтроку8(temp); context.error.пВК_ПС;
				всё
			всё;
			context.error.пВК_ПС;
		кц;
	иначе
		context.error.пСтроку8("No Devices found"); context.error.пВК_ПС;
		context.result := Commands.CommandError;
	всё;
	Lib.diskModel.Release;
кон Show;

проц ShowDevice(context : Commands.Context; disk: Lib.Disk; verbose: булево);
перем temp: массив 256 из симв8;
нач
	context.out.пСтроку8("Disk: "); context.out.пСтроку8(disk.device.name); context.out.пСтроку8(", ");
	если disk.res = Disks.Ok то
		Lib.WriteK(context.out, округлиВниз(disk.size * 1.0 * disk.device.blockSize / 1024));
		если verbose то
			context.out.пСтроку8(" = "); context.out.пЦел64(disk.size, 1);
			context.out.пСтроку8(" * "); context.out.пЦел64(disk.device.blockSize,1);
		всё
	иначе
		Lib.GetErrorMsg("GetSize failed", disk.res, temp); context.error.пСтроку8(temp);
	всё;
	если Disks.Removable в disk.device.flags то context.out.пСтроку8(", removable") всё;
	если Disks.ReadOnly в disk.device.flags то context.out.пСтроку8(", read-only") всё;
	если verbose то
		если disk.res # Disks.MediaMissing то
			context.out.пСтроку8(", ");
			если disk.gres = Disks.Ok то
				context.out.пСтроку8("CHS: "); context.out.пЦел64(disk.geo.cyls, 1); context.out.пСтроку8("*");
				context.out.пЦел64(disk.geo.hds, 1); context.out. пСтроку8("*"); context.out.пЦел64(disk.geo.spt, 1);
			иначе
				Lib.GetErrorMsg("GetCHS: ", disk.gres, temp); context.error.пСтроку8(temp);
			всё
		всё
	всё;
	если disk.device.desc # "" то context.out.пСтроку8(", "); context.out.пСтроку8(disk.device.desc) всё;
	если verbose то	context.out.пСтроку8(", mntcnt="); context.out.пЦел64(disk.device.openCount, 1) всё;
	context.out.пВК_ПС;
кон ShowDevice;

проц ShowTable( context : Commands.Context; disk: Lib.Disk; table: Disks.PartitionTable; verbose: булево);
перем j: размерМЗ; r: вещ64; ugly : массив 10 из симв8; temp : массив 128 из симв8; ignore : цел32;
нач
	нцДля j := 0 до длинаМассива(table)-1 делай
		r := (table[j].size * 1.0D0 * disk.device.blockSize) / (1024*1024); (* M *)
		Lib.WritePart(context.out, disk.device, j);
		если verbose то
			context.out.пЦел64(table[j].start, 12);
			context.out.пЦел64(table[j].size, 12)
		всё;
		Строки8.ПишиВещ64_вСтроку(r, 6, 1, 0, ugly);
		если r < 10 то context.out.пСтроку8(ugly);
		иначе context.out.пЦел64(округлиВниз(r), 6)
		всё;
		context.out.пСтроку8(" MB ");
		если (table[j].type >= 1) и (table[j].type <= 255) то
			context.out.пЦел64(table[j].type, 3)
		иначе
			context.out.пСтроку8("---")
		всё;
		context.out.пСимв8(" ");
		если (j # 0) и ~(Disks.Primary в table[j].flags) то context.out.пСтроку8( " | ") всё; (* logical drive *)
		если Disks.Boot в table[j].flags то context.out.пСтроку8(" * ") всё; (* bootable *)
		Lib.WriteType(table[j].type, temp, ignore); context.out.пСтроку8(temp);
		если verbose то
			если Disks.Mounted в table[j].flags то context.out.пСтроку8(" [mounted]") всё
		всё;
		context.out.пВК_ПС;
	кц
кон ShowTable;

(** Display limitations of AosFS *)
проц ShowAosFSLimits*(context : Commands.Context); (** ~ *)
нач
	Lib.ShowAosFSLimits;
кон ShowAosFSLimits;

проц UpdateDiskModel*(context : Commands.Context);(** ~ *)
нач
	Lib.diskModel.Update;
	context.out.пСтроку8("Disk model updated."); context.out.пВК_ПС;
кон UpdateDiskModel;

(* Scan the command line parameters for a device#partition specification. *)
(* The Writer <w> is used to return error messages, <r> contains p.str (dev#part skipped) *)
(* check : IF TRUE, only Disks.Valid partitions are returns *)
проц GetSelection*(context : Commands.Context; check : булево;  перем selection : Lib.Selection) : булево;
перем devpart : массив 32 из симв8;
нач
	selection.disk.device := НУЛЬ; selection.partition := -1; (* invalid *)
	если ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(devpart) то
		context.error.пСтроку8("Expected parameters: dev#part"); context.error.пВК_ПС; context.error.ПротолкниБуферВПоток;
		context.result := Commands.CommandParseError;
		возврат ложь;
	всё;
	context.arg.ПропустиБелоеПоле;

	(* special case: diskette *)
	если Строки8.ПодходитЛиПодМаскуИмениФайла¿("Diskette*", devpart) то
		check := ложь;
	всё;
	Lib.diskModel.Update;
	если Lib.diskModel.GetDisk(devpart, selection, check) то
		если Trace то
			ЛогЯдра.пСтроку8("Partitions: Command line selection: "); ЛогЯдра.пСтроку8(selection.disk.device.name);
			ЛогЯдра.пСимв8("#"); ЛогЯдра.пЦел64(selection.partition, 0); ЛогЯдра.пВК_ПС;
		всё;
		возврат истина;
	иначе
		context.error.пСтроку8("Partition "); context.error.пСтроку8(devpart); context.out.пСтроку8(" not found."); context.error.пВК_ПС;
		context.error.ПротолкниБуферВПоток; context.result := Commands.CommandError;
	всё;
	возврат ложь;
кон GetSelection;

кон Partitions.

System.Free DiskBenchmark Partitions ~

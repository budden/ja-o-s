модуль FoxBasic;   (**  AUTHOR "fof"; PURPOSE "Oberon Compiler: basic helpers: strings, lists, hash tables, graphs, indented writer";  **)
(* (c) fof ETH Zürich, 2009 *)

использует ЛогЯдра, StringPool, Strings, Потоки, Diagnostics, Files, НИЗКОУР, ObjectFile, Modules, D:= Debugging,
	ReaderSchitUTF8LF, СписокДинТипУклейНаМассиве;

конст
	(* error numbers *)
	(* first 255 tokens reserved for expected symbol error message *)
	UndeclaredIdentifier* = 256;
	MultiplyDefinedIdentifier* = 257;
	NumberIllegalCharacter* = 258;
	StringIllegalCharacter* = 259;
	NoMatchProcedureName* = 260;
	CommentNotClosed* = 261;
	IllegalCharacterValue* = 262;
	ValueStartIncorrectSymbol* = 263;
	IllegalyMarkedIdentifier* = 264;
	IdentifierNoType* = 265;
	IdentifierNoRecordType* = 266;
	IdentifierNoObjectType* = 267;
	ImportNotAvailable* = 268;
	RecursiveTypeDeclaration* = 269;
	NumberTooLarge* = 270;
	IdentifierTooLong* = 271;
	StringTooLong* = 272;

	InitErrMsgSize = 300;	(* initial size of array of error messages *)

	invalidString* = -1;
	InvalidCode* = -1;
тип
	(*
	String* = POINTER TO ARRAY OF CHAR;
	*)
	String* = StringPool.Index;
	SegmentedName*= ObjectFile.SegmentedName;
	FileName*= Files.FileName;
	(* Возможно, что тут не то же число, к-рое в ObjectFile.SegmentedNameLen, 
	 но они наверняка близки. См, например, WriteSegmentedName *)
	SectionName*= массив ObjectFile.SegmentedNameLen из симв8;
	MessageString*= массив 1024 из симв8;

	Integer* = цел64;
	Set* = мнвоНаБитах64;
	Real*= вещ64;


	ErrorMsgs = укль на массив из StringPool.Index;


	Position*= запись
		start*, end*, line*, linepos*: Потоки.ТипМестоВПотоке;
		reader*: Потоки.Чтец;
		имяФайла*: укль на Files.FileNameUCS32;
		лексема*: динамическиТипизированныйУкль; (* ссылка на лексему, из которой это место. Тип лексемы здесь ещё не определён, и кроме того,
			он разный для Лиса и Fox *)
	кон;

	ErrorCode* = целМЗ;

	Fingerprint* = ObjectFile.Fingerprint;

	List* = СписокДинТипУклейНаМассиве.List;

	HashEntryAny = запись
		key, value: динамическиТипизированныйУкль;
		valueInt: размерМЗ;
	кон;

	HashEntryInt = запись
		key, valueInt: размерМЗ;
		value: динамическиТипизированныйУкль;
	кон;

	HashAnyArray = укль на массив из HashEntryAny;
	HashIntArray = укль на массив из HashEntryInt;

	HashTable* = окласс
	перем
		table: HashAnyArray;
		size: размерМЗ;
		used-: размерМЗ;
		maxLoadFactor: вещ32;

		(* Interface *)

		проц & Init* (initialSize: размерМЗ);
		нач
			утв(initialSize > 2);
			нов(table, initialSize);
			size := initialSize;
			used := 0;
			maxLoadFactor := 0.75;
		кон Init;

		проц Put*(key, value: динамическиТипизированныйУкль);
		перем hash: размерМЗ;
		нач
			утв(used < size);
			утв(key # НУЛЬ);
			hash := HashValue(key);
			если table[hash].key = НУЛЬ то
				увел(used, 1);
			иначе
				утв(table[hash].key = key);
			всё;
			table[hash].key := key;
			table[hash].value := value;

			если (used / size) > maxLoadFactor то Grow всё;
		кон Put;

		проц Get*(key: динамическиТипизированныйУкль):динамическиТипизированныйУкль;
		нач
			возврат table[HashValue(key)].value;
		кон Get;

		проц Has*(key: динамическиТипизированныйУкль):булево;
		нач
			возврат table[HashValue(key)].key = key;
		кон Has;

		проц Length*(): размерМЗ;
		нач
			возврат used;
		кон Length;

		проц Clear*;
		перем i: размерМЗ;
		нач
			нцДля i := 0 до size - 1 делай table[i].key := НУЛЬ; table[i].value := НУЛЬ; table[i].valueInt := 0 кц;
		кон Clear;

		(* Interface for integer values *)

		проц PutInt*(key: динамическиТипизированныйУкль; value: размерМЗ);
		перем hash: размерМЗ;
		нач
			утв(used < size);
			hash := HashValue(key);
			если table[hash].key = НУЛЬ то
				увел(used, 1);
			всё;
			table[hash].key := key;
			table[hash].valueInt := value;
			если (used / size) > maxLoadFactor то Grow всё;
		кон PutInt;

		проц GetInt*(key: динамическиТипизированныйУкль):размерМЗ;
		нач
			возврат table[HashValue(key)].valueInt;
		кон GetInt;

		(* Internals *)

		(* only correctly working, if NIL key cannot be entered *)
		проц HashValue(key: динамическиТипизированныйУкль): размерМЗ;
		перем value, h1, h2, i: размерМЗ;
		нач
			value := НИЗКОУР.подмениТипЗначения(размерМЗ, key) DIV размер16_от(адресВПамяти);
			i := 0;
			h1 := value остОтДеленияНа size;
			h2 := 1; (* Linear probing *)
			нцДо
				value := (h1 + i*h2) остОтДеленияНа size;
				увел(i);
			кцПри((table[value].key = НУЛЬ) или (table[value].key = key) или (i > size));
			утв((table[value].key = НУЛЬ) и (table[value].value = НУЛЬ) или (table[value].key = key));
			возврат value;
		кон HashValue;

		проц Grow;
		перем oldTable: HashAnyArray; oldSize, i: размерМЗ; key: динамическиТипизированныйУкль;
		нач
			oldSize := size;
			oldTable := table;
			Init(size*2);
			нцДля i := 0 до oldSize-1 делай
				key := oldTable[i].key;
				если key # НУЛЬ то
					если oldTable[i].value # НУЛЬ то
						Put(key, oldTable[i].value);
					иначе
						PutInt(key, oldTable[i].valueInt);
					всё;
				всё;
			кц;
		кон Grow;

	кон HashTable;

	IntIterator*= окласс
	перем
		table: HashIntArray;
		count : размерМЗ;

		проц & Init(t: HashIntArray);
		нач
			table := t;
			count := -1;
		кон Init;

		проц GetNext*(перем key: размерМЗ; перем value: динамическиТипизированныйУкль): булево;
		нач
			нцДо
				увел(count);
			кцПри (count = длинаМассива(table)) или (table[count].value # НУЛЬ);
			если count = длинаМассива(table) то
				возврат ложь
			всё;
			key := table[count].key;
			value := table[count].value;
			возврат истина;
		кон GetNext;

	кон IntIterator;

	HashTableInt* = окласс
	перем
		table: HashIntArray;
		size: размерМЗ;
		used-: размерМЗ;
		maxLoadFactor: вещ32;

		(* Interface *)

		проц & Init* (initialSize: размерМЗ);
		нач
			утв(initialSize > 2);
			нов(table, initialSize);
			size := initialSize;
			used := 0;
			maxLoadFactor := 0.75;
		кон Init;

		проц Put*(key: размерМЗ; value: динамическиТипизированныйУкль);
		перем hash: размерМЗ;
		нач
			утв(key # 0);
			утв(used < size);
			hash := HashValue(key);
			если table[hash].key = 0 то
				увел(used, 1);
			всё;
			table[hash].key := key;
			table[hash].value := value;
			если (used / size) > maxLoadFactor то Grow всё;
		кон Put;

		проц Get*(key: размерМЗ):динамическиТипизированныйУкль;
		нач
			возврат table[HashValue(key)].value;
		кон Get;

		проц Has*(key: размерМЗ):булево;
		нач
			возврат table[HashValue(key)].key = key;
		кон Has;

		проц Length*(): размерМЗ;
		нач
			возврат used;
		кон Length;

		проц Clear*;
		перем i: размерМЗ;
		нач
			нцДля i := 0 до size - 1 делай table[i].key := 0; кц;
		кон Clear;

		(* Interface for integer values *)

		проц PutInt*(key, value: размерМЗ);
		перем hash: размерМЗ;
		нач
			(*ASSERT(key # 0);*)
			утв(used < size);
			hash := HashValue(key);
			если table[hash].key = 0 то
				увел(used, 1);
			всё;
			table[hash].key := key;
			table[hash].valueInt := value;
			если (used / size) > maxLoadFactor то Grow всё;
		кон PutInt;

		проц GetInt*(key: размерМЗ): размерМЗ;
		нач
			возврат table[HashValue(key)].valueInt;
		кон GetInt;

		(* Internals *)

		проц HashValue(key: размерМЗ): размерМЗ;
		перем value, h1, h2, i: размерМЗ;
		нач
			i := 0;
			value := key;
			h1 := key остОтДеленияНа size;
			h2 := 1; (* Linear probing *)
			нцДо
				value := (h1 + i*h2) остОтДеленияНа size;
				увел(i);
			кцПри((table[value].key = 0) или (table[value].key = key) или (i > size));
			утв((table[value].key = 0) или (table[value].key = key));
			возврат value;
		кон HashValue;

		проц Grow;
		перем oldTable: HashIntArray; oldSize, i, key: размерМЗ;
		нач
			oldSize := size;
			oldTable := table;
			Init(size*2);
			нцДля i := 0 до oldSize-1 делай
				key := oldTable[i].key;
				если key # 0 то
					если oldTable[i].value # НУЛЬ то
						Put(key, oldTable[i].value);
					иначе
						PutInt(key, oldTable[i].valueInt);
					всё;
				всё;
			кц;
		кон Grow;

		проц GetIterator*(): IntIterator;
		перем iterator: IntIterator;
		нач
			нов(iterator, table);
			возврат iterator;
		кон GetIterator;

	кон HashTableInt;

	HashEntrySegmentedName = запись
		key: ObjectFile.SegmentedName; (* key[0]= MIN(SIGNED32) <=> empty *)
		value: динамическиТипизированныйУкль;
	кон;
	HashSegmentedNameArray = укль на массив из HashEntrySegmentedName;

	HashTableSegmentedName* = окласс
	перем
		table: HashSegmentedNameArray;
		size: размерМЗ;
		used-: размерМЗ;
		maxLoadFactor: вещ32;

		(* Interface *)

		проц & Init* (initialSize: размерМЗ);
		нач
			утв(initialSize > 2);
			нов(table, initialSize);
			size := initialSize;
			used := 0;
			maxLoadFactor := 0.75;
			Clear;
		кон Init;

		проц Put*(конст key: SegmentedName; value: динамическиТипизированныйУкль);
		перем hash: размерМЗ;
		нач
			утв(used < size);
			hash := HashValue(key);
			если table[hash].key[0] < 0 то
				увел(used, 1);
			всё;
			table[hash].key := key;
			table[hash].value := value;
			если (used / size) > maxLoadFactor то Grow всё;
		кон Put;

		проц Get*(конст key: SegmentedName):динамическиТипизированныйУкль;
		нач
			возврат table[HashValue(key)].value;
		кон Get;

		проц Has*(конст key: SegmentedName):булево;
		нач
			возврат table[HashValue(key)].key = key;
		кон Has;

		проц Length*(): размерМЗ;
		нач
			возврат used;
		кон Length;

		проц Clear*;
		перем i: размерМЗ;
		нач
			нцДля i := 0 до size - 1 делай table[i].key[0] := -1; кц;
		кон Clear;

		(* Internals *)
		проц Hash*(конст name: SegmentedName): размерМЗ;
		перем fp, i: размерМЗ;
		нач
			fp := name[0]; i := 1;
			нцПока (i<длинаМассива(name)) и (name[i] >= 0) делай
				fp:=НИЗКОУР.подмениТипЗначения(размерМЗ, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, вращБит(fp, 7)) / НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, name[i]));
				увел(i);
			кц;
			возврат fp
		кон Hash;

		проц HashValue(конст key: SegmentedName): размерМЗ;
		перем value, h, i: размерМЗ;
		нач
			утв(key[0] >= 0);
			h := Hash(key);
			i := 0;
			нцДо
				value := (h + i) остОтДеленияНа size;
				увел(i);
			кцПри((table[value].key[0] < 0) или (table[value].key = key) или (i > size));
			утв((table[value].key[0] <0 ) или (table[value].key = key));
			возврат value;
		кон HashValue;

		проц Grow;
		перем oldTable: HashSegmentedNameArray; oldSize, i: размерМЗ; key: SegmentedName;
		нач
			oldSize := size;
			oldTable := table;
			Init(size*2);
			нцДля i := 0 до oldSize-1 делай
				key := oldTable[i].key;
				если key[0] # матМинимум(цел32) то
					если oldTable[i].value # НУЛЬ то
						Put(key, oldTable[i].value);
					всё;
				всё;
			кц;
		кон Grow;

	кон HashTableSegmentedName;

	IntegerObject = окласс
	кон IntegerObject;

	Writer* = окласс (Потоки.Писарь)
	перем
		indent-: цел32;
		doindent: булево;
		w-: Потоки.Писарь;

		проц InitBasicWriter*( п: Потоки.Писарь );
		перем пю: ReaderSchitUTF8LF.ПисарьЮникодаОбёртка;
		нач
			просейТип п:
			| ReaderSchitUTF8LF.ПисарьЮникодаОбёртка делай
				пю := п;
			иначе
				нов(пю,п) всё;
			сам.w := пю; indent := 0; doindent := истина;
		кон InitBasicWriter;

		проц & InitW(l1w: Потоки.Писарь); (* protect against use of NEW *)
		нач InitBasicWriter(l1w);
		кон InitW;

		проц {перекрыта}Сбрось*;
		нач w.Сбрось;
		кон Сбрось;

		проц {перекрыта}можноЛиПерейтиКМестуВПотоке¿*( ): булево;
		нач возврат w.можноЛиПерейтиКМестуВПотоке¿();
		кон можноЛиПерейтиКМестуВПотоке¿;

		проц {перекрыта}ПерейдиКМестуВПотоке*( pos: Потоки.ТипМестоВПотоке );
		нач w.ПерейдиКМестуВПотоке(pos);
		кон ПерейдиКМестуВПотоке;

		проц {перекрыта}ПротолкниБуферВПоток*;
		нач w.ПротолкниБуферВПоток;
		кон ПротолкниБуферВПоток;

		проц {перекрыта}МестоВПотоке*( ): Потоки.ТипМестоВПотоке;
		нач возврат w.МестоВПотоке()
		кон МестоВПотоке;

		проц Indent;
		перем i: цел32;
		нач
			если doindent то
				нцДля i := 0 до indent-1 делай
					w.пСимв8(9X);
				кц;
				doindent := ложь
			всё;
		кон Indent;

		проц {перекрыта}пСимв8*( x: симв8 );
		нач Indent; w.пСимв8(x);
		кон пСимв8;

		проц {перекрыта}пБайты*(конст x: массив из симв8;  ofs, len: размерМЗ );
		нач  w.пБайты(x,ofs,len);
		кон пБайты;

		проц {перекрыта}пЦел8_мз*( x: цел8 );
		нач  w.пЦел8_мз(x)
		кон пЦел8_мз;

		проц {перекрыта}пЦел16_мз*( x: цел16 );
		нач  w.пЦел16_мз(x)
		кон пЦел16_мз;

		проц {перекрыта}пЦел32_мз*( x: цел32 );
		нач  w.пЦел32_мз(x)
		кон пЦел32_мз;

		проц {перекрыта}пЦел64_мз*( x: цел64 );
		нач  w.пЦел64_мз(x)
		кон пЦел64_мз;

		проц {перекрыта}пЦел32_сп*( x: цел32 );
		нач  w.пЦел32_сп(x)
		кон пЦел32_сп;

		проц {перекрыта}п2МладшихБайта_сп*( x: цел32 );
		нач  w.п2МладшихБайта_сп(x)
		кон п2МладшихБайта_сп;

		проц {перекрыта}пМладшийБайт_сп*( x: цел32 );
		нач  w.пМладшийБайт_сп(x)
		кон пМладшийБайт_сп;

		проц {перекрыта}пНеБолее32БитИзМнваНаБитах_мз*( x: мнвоНаБитахМЗ );
		нач  w.пНеБолее32БитИзМнваНаБитах_мз(x)
		кон пНеБолее32БитИзМнваНаБитах_мз;

		проц {перекрыта}пБулево_мз*( x: булево );
		нач  w.пБулево_мз(x)
		кон пБулево_мз;

		проц {перекрыта}пВещ32_мз*( x: вещ32 );
		нач  w.пВещ32_мз(x)
		кон пВещ32_мз;

		проц {перекрыта}пВещ64_мз*( x: Real );
		нач  w.пВещ64_мз(x)
		кон пВещ64_мз;

		проц {перекрыта}пСтроку8˛включаяСимв0*(конст x: массив из симв8 );
		нач  w.пСтроку8˛включаяСимв0(x)
		кон пСтроку8˛включаяСимв0;

		проц {перекрыта}пЦел64_7б*( x: цел64 );
		нач  w.пЦел64_7б(x)
		кон пЦел64_7б;

		проц {перекрыта}пВК_ПС*;
		нач w.пВК_ПС; doindent := истина;
		кон пВК_ПС;

		проц {перекрыта}пСтроку8*(конст x: массив из симв8 );
		нач Indent; w.пСтроку8(x)
		кон пСтроку8;

		проц {перекрыта}пЦел64*( x: цел64; wd: размерМЗ );
		нач Indent; w.пЦел64(x,wd)
		кон пЦел64;

		проц {перекрыта}пМнвоНаБитахМЗ*( s: мнвоНаБитахМЗ );   (* from P. Saladin *)
		нач Indent; w.пМнвоНаБитахМЗ(s)
		кон пМнвоНаБитахМЗ;

		проц {перекрыта}п16ричное*(x: цел64; wd: размерМЗ );
		нач Indent; w.п16ричное(x,wd)
		кон п16ричное;

		проц {перекрыта}пАдресВПамяти* (x: адресВПамяти);
		нач Indent; w.пАдресВПамяти(x)
		кон пАдресВПамяти;

		проц {перекрыта}пДатуОберонаISO*( t, d: цел32 );
		нач Indent; w.пДатуОберонаISO(t,d)
		кон пДатуОберонаISO;

		проц {перекрыта}пДатуОберона822*( t, d, tz: цел32 );
		нач Indent; w.пДатуОберона822(t,d,tz)
		кон пДатуОберона822;

		проц {перекрыта}пВещ64*( x: Real;  n: цел32 );
		нач Indent; w.пВещ64(x,n)
		кон пВещ64;

		проц {перекрыта}пВещ64_ФФТ*( x: Real;  n, f, l2D: цел32 );
		нач Indent; w.пВещ64_ФФТ(x,n,f,l2D)
		кон пВещ64_ФФТ;

		проц SetIndent*(i: цел32);
		нач
			indent := i
		кон SetIndent;

		проц IncIndent*;
		нач увел(indent)
		кон IncIndent;

		проц DecIndent*;
		нач умень(indent)
		кон DecIndent;

		проц BeginAlert*;
		кон BeginAlert;

		проц EndAlert*;
		кон EndAlert;

		проц BeginKeyword*;
		нач
		кон BeginKeyword;

		проц EndKeyword*;
		нач
		кон EndKeyword;

		проц BeginComment*;
		кон BeginComment;

		проц EndComment*;
		кон EndComment;

		проц AlertString*(конст s: массив из симв8);
		нач
			BeginAlert; w.пСтроку8(s); EndAlert;
		кон AlertString;

	кон Writer;

	TracingDiagnostics = окласс (Diagnostics.Diagnostics)
	перем diagnostics: Diagnostics.Diagnostics;

		проц &InitDiagnostics(l3diagnostics: Diagnostics.Diagnostics);
		нач
			сам.diagnostics := l3diagnostics
		кон InitDiagnostics;

		проц {перекрыта}Error*(конст source: массив из симв8; position: Потоки.ТипМестоВПотоке; конст message : массив из симв8);
		нач
			если diagnostics # НУЛЬ то
				diagnostics.Error(source,position,message);
			всё;
			D.Ln;
			D.String(" ---------------------- TRACE for COMPILER ERROR  < ");
			D.String(source);
			если position # Потоки.НевернаяПозицияВПотоке то D.String("@"); D.Int(position,1) всё;
			D.String(" "); D.String(message);
			D.String(" > ---------------------- ");
			D.TraceBack
		кон Error;

		проц {перекрыта}Warning*(конст source : массив из симв8; position: Потоки.ТипМестоВПотоке; конст message : массив из симв8);
		нач
			если diagnostics # НУЛЬ то
				diagnostics.Warning(source,position,message);
			всё;
		кон Warning;

		проц {перекрыта}Information*(конст source : массив из симв8; position: Потоки.ТипМестоВПотоке; конст message : массив из симв8);
		нач
			если diagnostics # НУЛЬ то
				diagnostics.Information(source,position,message);
			всё;
		кон Information;

	кон TracingDiagnostics;

	DebugWriterFactory*= проц{делегат} (конст title: массив из симв8): Потоки.Писарь;
	WriterFactory*=проц{делегат} (w: Потоки.Писарь): Writer;
	DiagnosticsFactory*=проц{делегат} (w: Потоки.Писарь): Diagnostics.Diagnostics;

перем
	integerObjects: HashTableInt;
	errMsg: ErrorMsgs;	(*error messages*)
	emptyString-: String;
	debug: булево;
	getDebugWriter: DebugWriterFactory;
	getWriter: WriterFactory;
	getDiagnostics: DiagnosticsFactory;
	invalidPosition-: Position;

	(* Make a string out of a series of characters. *)
	проц MakeString*( конст s: массив из симв8 ): String;
	(* VAR str: String;  *)
	нач
		(* INC( strings ); (* на самом деле количество разных строк может и не увеличиться, 
			и вообще неясно, зачем этот счётчик *) *)
		(*
		(* allocation based *)
		NEW( str, Strings.Length( s ) +1);  COPY( s, str^ );  RETURN str;
		*)
		возврат StringPool.GetIndex1( s )
	кон MakeString;

	проц GetString*(s: String; перем str: массив из симв8);
	нач
		StringPool.GetString(s,str);
	кон GetString;

	проц StringEqual*( s, t: String ): булево;
	нач
		возврат s = t;
		(*
		(* allocation based *)
		RETURN s^ = t^
		*)
	кон StringEqual;
	
	проц ПечString*(w : Потоки.Писарь; стр: String);
	перем стрр: массив 512 из симв8;
	нач
		если стр = invalidString то
			w.пСтроку8("«invalidString»")
		иначе
			GetString(стр, стрр);
			w.пСтроку8(стрр) всё кон ПечString;
			
	проц ДайСтрокуКакСвежуюКопию*(стр: String) : Strings.String;
	перем стрр: массив 512 из симв8;
	нач
		если стр = invalidString то
			возврат НУЛЬ
		иначе
			GetString(стр, стрр);
			возврат Strings.NewString(стрр) всё кон ДайСтрокуКакСвежуюКопию;
	


	проц GetErrorMessage*(err: ErrorCode; конст msg: массив из симв8; перем res: массив из симв8);
	перем str: массив 128 из симв8;
	нач
		res := "";
		если (errMsg # НУЛЬ) и (0 <= err) и (err < длинаМассива(errMsg)) то
			StringPool.GetString(errMsg[err], str);
			Strings.Append(res,str);
			Strings.Append(res, " ");
		всё;
		Strings.Append(res, msg);
        Strings.Append(res, ". ");
	кон GetErrorMessage;

	проц AppendDetailedErrorMessage*(перем message: массив из симв8; pos: Position; reader: Потоки.Чтец);
    перем err: массив 1024 из симв8; ch: симв8; oldpos: Потоки.ТипМестоВПотоке;
    нач
        если (reader # НУЛЬ) и (reader.можноЛиПерейтиКМестуВПотоке¿()) то
            oldpos := reader.МестоВПотоке();
            reader.ПерейдиКМестуВПотоке(pos.linepos);
            reader.чСимв8(ch);
            (* read until end of source line *)
            нцПока (ch # 0X) и (ch # 0AX) и (ch # 0DX) делай
                Strings.AppendChar(err, ch);
                если reader.МестоВПотоке() = pos.start то
                    Strings.Append(err,"(*!*)");
                всё;
                reader.чСимв8(ch);
            кц;
            reader.ПерейдиКМестуВПотоке(oldpos);
        всё;
        Strings.TrimWS(err);
        Strings.Append(message, err);
    кон AppendDetailedErrorMessage;

	проц AppendPosition*(перем msg: массив из симв8; pos: Position);
	нач
		если pos.line >= 0 то
			Strings.Append(msg, " in line ");
			Strings.AppendInt(msg, pos.line);
			Strings.Append(msg, ", col ");
			Strings.AppendInt(msg, pos.start- pos.linepos);
			Strings.Append(msg, ", pos ");
			Strings.AppendInt(msg, pos.start);
		всё;
	кон AppendPosition;

	проц MakeMessage(pos: Position; code: ErrorCode; конст msg: массив из симв8; перем message: массив из симв8);
	нач
		MakeDetailedMessage(pos, code, msg, НУЛЬ, message);
		Strings.AppendChar(message, 0X); (* terminate message *)
	кон MakeMessage;

	проц MakeDetailedMessage(pos: Position; code: ErrorCode; конст msg: массив из симв8; reader: Потоки.Чтец; перем message: массив из симв8);
	нач
		GetErrorMessage(code, msg, message);
		AppendDetailedErrorMessage(message, pos, reader);
		AppendPosition(message, pos);
	кон MakeDetailedMessage;

	(* error message with code. Позиция по соглашению меряется в буквах Юникода, при том последовательность ВК-ПС считается за одну букву *)
	проц ErrorC*(diagnostics: Diagnostics.Diagnostics; конст source: массив из симв8; pos: Position; code: ErrorCode; конст msg: массив из симв8);
	перем message: массив 1024 из симв8; file: Files.File;

		проц GetReader(): Потоки.Чтец;
		перем reader := НУЛЬ: Потоки.Чтец; fileReader : Files.Reader; ro : ReaderSchitUTF8LF.ЧтецЮникодаОбёртка;
		нач
			если (pos.linepos >= 0) и ((source # "") или (pos.reader # НУЛЬ)) то
				reader := pos.reader;
				если reader = НУЛЬ то
					file := Files.Old(source);
					если file # НУЛЬ то
						нов (fileReader, file, 0);
						нов (ro, fileReader); 						
						ro.ПерейдиКМестуВПотоке(pos.linepos);
						reader := ro;
					всё;
				всё;
			всё;
			возврат reader;
		кон GetReader;

	нач
		если diagnostics # НУЛЬ то
			MakeDetailedMessage(pos, code, msg, GetReader(), message);
			diagnostics.Error(source, pos.start, message);
		всё;
	кон ErrorC;

	(* Сообщение об ошибке без числового кода. pos по соглашению измеряется в литерах юникода, при этом конец строки считается за один символ *)
	проц Error*(diagnostics: Diagnostics.Diagnostics; конст source: массив из симв8; pos: Position; конст msg: массив из симв8);
	нач
		ErrorC(diagnostics, source, pos, InvalidCode, msg);
	кон Error;

	проц Warning*(diagnostics: Diagnostics.Diagnostics; конст source: массив из симв8; pos: Position; конст msg: массив из симв8);
	перем message: массив 1024 из симв8;
	нач
		если diagnostics # НУЛЬ то
			MakeMessage(pos, InvalidCode, msg,message);
			diagnostics.Warning(source, pos.start, message);
		всё;
	кон Warning;

	проц Information*(diagnostics: Diagnostics.Diagnostics; конст source: массив из симв8; pos: Position;конст msg: массив из симв8);
	перем message: массив 256 из симв8;
	нач
		если diagnostics # НУЛЬ то
			MakeMessage(pos, InvalidCode, msg,message);
			diagnostics.Information(source, pos.start, message);
		всё;
	кон Information;

	(** SetErrorMsg - Set message for error n *)

	проц SetErrorMessage*(n: ErrorCode; конст  msg: массив из симв8);
	нач
		если errMsg = НУЛЬ то нов(errMsg, InitErrMsgSize) всё;
		нцПока длинаМассива(errMsg^) < n делай Expand(errMsg) кц;
		StringPool.GetIndex(msg, errMsg[n])
	кон SetErrorMessage;

	проц SetErrorExpected*(n: ErrorCode; конст msg: массив из симв8);
	перем err: массив 256 из симв8;
	нач
		err := "ожидалось '";
		Strings.Append(err,msg);
		Strings.Append(err, "'");
		SetErrorMessage(n,err);
	кон SetErrorExpected;

	проц AppendNumber*(перем s: массив из симв8; num: цел64);
	перем nums: массив 32 из симв8;
	нач
		Strings.IntToStr(num,nums);
		Strings.Append(s,nums);
	кон AppendNumber;

	проц InitSegmentedName*(перем name: SegmentedName);
	перем i: размерМЗ;
	нач нцДля i := 0 до длинаМассива(name)-1 делай name[i] := -1 кц;
	кон InitSegmentedName;

	проц ToSegmentedName*(конст name: массив из симв8; перем pooledName: SegmentedName);
	нач
		ObjectFile.StringToSegmentedName(name,pooledName);
	кон ToSegmentedName;

	проц SegmentedNameToString*(конст pooledName: SegmentedName; перем name: массив из симв8);
	нач
		ObjectFile.SegmentedNameToString(pooledName, name);
	кон SegmentedNameToString;

	проц WriteSegmentedName*(w: Потоки.Писарь; name: SegmentedName);
	перем sectionName: ObjectFile.SectionName;
	нач
		SegmentedNameToString(name, sectionName);
		w.пСтроку8(sectionName);
	кон WriteSegmentedName;

	проц AppendToSegmentedName*(перем name: SegmentedName; конст this: массив из симв8);
	перем i,j: размерМЗ; string: ObjectFile.SectionName;
	нач
		i := 0;
		нцПока (i<длинаМассива(name)) и (name[i] >= 0)  делай
			увел(i)
		кц;
		если (this[0] = ".") и (i < длинаМассива(name)) то (* suffix *)
			j := 0;
			нцПока this[j+1] # 0X делай
				string[j] := this[j+1];
				увел(j);
			кц;
			string[j] := 0X;
			name[i] := StringPool.GetIndex1(string);
			если i<длинаМассива(name)-1 то name[i+1] := -1 всё;
		иначе
			StringPool.GetString(name[i-1], string);
			Strings.Append(string, this);
			name[i-1] := StringPool.GetIndex1(string);
		всё;
	кон AppendToSegmentedName;

	(* suffix using separation character "." *)
	проц SuffixSegmentedName*(перем name: SegmentedName; this: StringPool.Index);
	перем string, suffix: ObjectFile.SectionName; i: размерМЗ;
	нач
		i := 0;
		нцПока (i < длинаМассива(name)) и (name[i] >= 0) делай
			увел(i);
		кц;
		если i < длинаМассива(name) то (* suffix *)
			name[i] := this;
			если i<длинаМассива(name)-1 то name[i+1] := -1 всё;
		иначе
			StringPool.GetString(name[i-1], string);
			StringPool.GetString(this, suffix);
			Strings.Append(string,".");
			Strings.Append(string, suffix);
			name[i-1] := StringPool.GetIndex1(string);
		всё;
	кон SuffixSegmentedName;

	проц SegmentedNameEndsWith*(конст name: SegmentedName; конст this: массив из симв8): булево;
	перем string: ObjectFile.SectionName; i: размерМЗ;
	нач
		i := 0;
		нцПока (i< длинаМассива(name)) и (name[i] >= 0) делай
			увел(i);
		кц;
		умень(i);
		если i < 0 то
			возврат ложь
		иначе
			StringPool.GetString(name[i],string);
			возврат Strings.EndsWith(this, string);
		всё
	кон SegmentedNameEndsWith;

	проц RemoveSuffix*(перем name: SegmentedName);
	перем i,pos,pos0: размерМЗ; string: ObjectFile.SectionName;
	нач
		i := 0;
		нцПока  (i< длинаМассива(name)) и (name[i] >= 0) делай
			увел(i);
		кц;
		утв(i>0);
		если i < длинаМассива(name) то (* name[i] = empty *) name[i-1] := -1
		иначе (* i = LEN(name), name[i] = nonempty *)
			умень(i);
			StringPool.GetString(name[i],string);
		pos0 := 0; pos := 0;
		нцПока (pos0 < длинаМассива(string)) и (string[pos0] # 0X) делай
			если string[pos0] = "." то pos := pos0 всё;
			увел(pos0);
		кц;
		если pos = 0 то (* no dot in name or name starts with dot *)
				name[i] := -1
		иначе (* remove last part in name *)
			string[pos] := 0X;
				name[i] := StringPool.GetIndex1(string);
		всё;
		всё;
	кон RemoveSuffix;

	проц GetSuffix*(конст name: SegmentedName; перем string: массив из симв8);
	перем i,pos,pos0: размерМЗ;
	нач
		i := 0;
		нцПока  (i< длинаМассива(name)) и (name[i] >= 0) делай
			увел(i);
		кц;
		утв(i>0);
		StringPool.GetString(name[i-1],string);
		если i = длинаМассива(name) то
			pos0 := 0; pos := 0;
			нцПока (pos0 < длинаМассива(string)) и (string[pos0] # 0X) делай
				если string[pos0] = "." то pos := pos0 всё;
				увел(pos0);
			кц;
			если pos # 0 то (* no dot in name or name starts with dot *)
				pos0 := 0;
				нцДо
					увел(pos); (* start with character after "." *)
					string[pos0] := string[pos];
					увел(pos0);
				кцПри string[pos]  = 0X;
			всё;
		всё;
	кон GetSuffix;

	проц IsPrefix*(конст prefix, of: SegmentedName): булево;
	перем prefixS, ofS: SectionName; i: размерМЗ;
	нач
		i := 0;
		нцПока (i< длинаМассива(prefix)) и (prefix[i] = of[i]) делай увел(i) кц;

		если i = длинаМассива(prefix) то возврат истина (* identical *)
		иначе (* prefix[i] # of[i] *)
			если prefix[i] < 0 то возврат истина
			аесли of[i] < 0 то возврат ложь
			аесли (i<длинаМассива(prefix)-1) то возврат ложь
			иначе
				StringPool.GetString(prefix[i], prefixS);
				StringPool.GetString(of[i], ofS);
			возврат Strings.StartsWith(prefixS, 0, ofS)
			всё
		всё;
	кон IsPrefix;

	проц Expand(перем oldAry: ErrorMsgs);
	перем
		len, i: размерМЗ;
		newAry: ErrorMsgs;
	нач
		если oldAry = НУЛЬ то возврат всё;
		len := длинаМассива(oldAry^);
		нов(newAry, len * 2);
		нцДля i := 0 до len-1 делай
			newAry[i] := oldAry[i];
		кц;
		oldAry := newAry;
	кон Expand;

	проц Concat*(перем result: массив из симв8; конст prefix, name, suffix: массив из симв8);
	перем i, j: размерМЗ;
	нач
		i := 0; нцПока prefix[i] # 0X делай  result[i] := prefix[i];  увел(i)  кц;
		j := 0; нцПока name[j] # 0X делай  result[i+j] := name[j];  увел(j)  кц;
		увел(i, j);
		j := 0; нцПока suffix[j] # 0X делай result[i+j] := suffix[j]; увел(j)  кц;
		result[i+j] := 0X;
	кон Concat;

	проц Lowercase*(конст name: массив из симв8; перем result: массив из симв8);
	перем ch: симв8; i: размерМЗ;
	нач
		i := 0;
		нцДо
			ch := name[i];
			если (ch >= 'A') и (ch <= 'Z') то
				ch := симв8ИзКода(кодСимв8(ch)-кодСимв8('A')+кодСимв8('a'));
			всё;
			result[i] := ch; увел(i);
		кцПри ch = 0X;
	кон Lowercase;

	проц Uppercase*(конст name: массив из симв8; перем result: массив из симв8);
	перем ch: симв8; i: размерМЗ;
	нач
		i := 0;
		нцДо
			ch := name[i];
			если (ch >= 'a') и (ch <= 'z') то
				ch := симв8ИзКода(кодСимв8(ch)-кодСимв8('a')+кодСимв8('A'));
			всё;
			result[i] := ch; увел(i);
		кцПри ch = 0X;
	кон Uppercase;

	проц GetIntegerObj*(value: цел32):динамическиТипизированныйУкль;
	перем obj: IntegerObject;
	нач
		если integerObjects.Has(value) то
			возврат integerObjects.Get(value);
		всё;
		нов(obj);
		integerObjects.Put(value, obj);
		возврат obj;
	кон GetIntegerObj;

	проц Align*(перем offset: размерМЗ; alignment: размерМЗ);
	нач
		если alignment >0  то
			увел(offset,(-offset) остОтДеленияНа alignment);
		аесли alignment < 0 то
			умень(offset,offset остОтДеленияНа (-alignment));
		всё;
	кон Align;

	проц InitErrorMessages;
	нач
		SetErrorMessage(UndeclaredIdentifier, "undeclared identifier");
		SetErrorMessage(MultiplyDefinedIdentifier, "multiply defined identifier");
		SetErrorMessage(NumberIllegalCharacter, "illegal character in number");
		SetErrorMessage(StringIllegalCharacter, "illegal character in string");
		SetErrorMessage(NoMatchProcedureName, "procedure name does not match");
		SetErrorMessage(CommentNotClosed, "comment not closed");
		SetErrorMessage(IllegalCharacterValue, "illegal character value");
		SetErrorMessage(ValueStartIncorrectSymbol, "value starts with incorrect symbol");
		SetErrorMessage(IllegalyMarkedIdentifier, "illegaly marked identifier");
		SetErrorMessage(IdentifierNoType, "identifier is not a type");
		SetErrorMessage(IdentifierNoRecordType, "identifier is not a record type");
		SetErrorMessage(IdentifierNoObjectType, "identifier is not an object type");
		SetErrorMessage(ImportNotAvailable, "import is not available");
		SetErrorMessage(RecursiveTypeDeclaration, "recursive type declaration");
		SetErrorMessage(NumberTooLarge, "number too large");
		SetErrorMessage(IdentifierTooLong, "identifier too long");
		SetErrorMessage(StringTooLong, "string too long");
	кон InitErrorMessages;

	проц ActivateDebug*;
	нач
		debug := истина;
	кон ActivateDebug;

	проц GetFileReader*(конст filename: массив из симв8): Потоки.Чтец;
	перем
		file: Files.File; fileReader: Files.Reader; offset: цел32;
	нач
		(* Optimisation: skip header of oberon files and return a file reader instead of default text reader*)
		file := Files.Old (filename);
		если file = НУЛЬ то возврат НУЛЬ всё;
		нов (fileReader, file, 0);
		если (fileReader.чИДайСимв8 () = 0F0X) и (fileReader.чИДайСимв8 () = 001X) то
			offset := кодСимв8 (fileReader.чИДайСимв8 ());
			увел (offset, устарПреобразуйКБолееШирокомуЦел (кодСимв8 (fileReader.чИДайСимв8 ())) * 0100H);
			fileReader.ПерейдиКМестуВПотоке(offset);
		иначе fileReader.ПерейдиКМестуВПотоке(0)
		всё;
		возврат fileReader
	кон GetFileReader;

	проц GetWriter*(w: Потоки.Писарь): Writer;
	перем writer: Writer; пю : ReaderSchitUTF8LF.ПисарьЮникодаОбёртка;
	нач
		утв(w # НУЛЬ);
		просейТип w:
		| Writer делай возврат w
		| ReaderSchitUTF8LF.ПисарьЮникодаОбёртка делай
			если getWriter = НУЛЬ то
				нов(writer,w); возврат writer
			иначе возврат getWriter(w) всё
		иначе
			нов(пю,w);
			возврат GetWriter(пю) всё кон GetWriter;

	проц GetDebugWriter*(конст title: массив из симв8): Потоки.Писарь;
	перем w: Потоки.Писарь;
	нач
		если getDebugWriter # НУЛЬ то  w:= getDebugWriter(title)
		иначе нов(w, ЛогЯдра.ЗапишиВПоток,1024)
		всё;
		возврат w;
	кон GetDebugWriter;

	проц GetDiagnostics*(w: Потоки.Писарь): Diagnostics.Diagnostics;
	перем diagnostics: Diagnostics.StreamDiagnostics;
	нач
		если getDiagnostics # НУЛЬ то возврат getDiagnostics(w)
		иначе нов(diagnostics,w); возврат diagnostics
		всё;
	кон GetDiagnostics;

	проц GetDefaultDiagnostics*(): Diagnostics.Diagnostics;
	перем w: Потоки.Писарь;
	нач
		нов(w, ЛогЯдра.ЗапишиВПоток,128);
		возврат GetDiagnostics(w);
	кон GetDefaultDiagnostics;

	проц InitWindowWriter;
	перем install: проц;
	нач
		getDebugWriter := НУЛЬ; getWriter := НУЛЬ;
		если Modules.ModuleByName("WindowManager") # НУЛЬ то
			дайПроцПоИмени("FoxA2Interface","Install",install);
		всё;
		если install # НУЛЬ то install всё;
	кон InitWindowWriter;

	проц InstallWriterFactory*(writer: WriterFactory; l4debug: DebugWriterFactory; diagnostics: DiagnosticsFactory);
	нач
		getWriter := writer;
		getDebugWriter := l4debug;
		getDiagnostics := diagnostics;
	кон InstallWriterFactory;

	проц Replace(перем in: массив из симв8; конст this, by: массив из симв8);
	перем pos: размерМЗ;
	нач
		pos := Strings.Pos(this,in);
		если pos >= 0 то
			Strings.Delete(in,pos,Strings.Length(this));
			Strings.Insert(by, in, pos);
		всё;
	кон Replace;

	операция "="*(конст left: массив из симв8; right: String): булево;
	нач
		возврат right = StringPool.GetIndex1(left);
	кон "=";

	операция "="*(left: String; конст right: массив из симв8): булево;
	нач
		возврат right = left;
	кон "=";
	
	проц MessageS*(конст format, s0: массив из симв8): MessageString;
	перем message: MessageString;
	нач
		копируйСтрокуДо0(format, message);
		Replace(message,"%0",s0);
		возврат message
	кон MessageS;

	проц MessageSS*(конст format, s0, s1: массив из симв8): MessageString;
	перем message: MessageString;
	нач
		копируйСтрокуДо0(format, message);
		Replace(message,"%0",s0);
		Replace(message,"%1",s1);
		возврат message
	кон MessageSS;

	проц MessageI*(конст format: массив из симв8; i0: цел32): MessageString;
	перем message: MessageString; number: массив 32 из симв8;
	нач
		копируйСтрокуДо0(format, message);
		Strings.IntToStr(i0,number);
		Replace(message,"%0",number);
	кон MessageI;

	проц MessageSI*(конст format: массив из симв8; конст s0: массив из симв8; i1: цел32): MessageString;
	перем message: MessageString; number: массив 32 из симв8;
	нач
		копируйСтрокуДо0(format, message);
		Replace(message,"%0",s0);
		Strings.IntToStr(i1,number);
		Replace(message,"%1",number);
	кон MessageSI;

		(*
		Get next available name from stream ignoring comments and end of comment brackets
		Returns TRUE on success, returns FALSE on end of stream, on error or if "~" or ";" encountered.
		Scanner based on Peek() feature of stream. Necessary to make it restartable.
	*)
	проц GetStringParameter*(r: Потоки.Чтец; перем string: массив из симв8): булево;
	перем ch: симв8; i: размерМЗ; done,error: булево;

		проц Next;
		нач r.чСимв8(ch); ch := r.ПодглядиСимв8();
		кон Next;

		проц Append(l5ch: симв8);
		нач string[i] := l5ch; увел(i)
		кон Append;

		проц SkipWhitespace;
		нач нцПока (ch <= " ") и (ch # 0X) делай Next кц;
		кон SkipWhitespace;

		проц Comment;
		перем l6done: булево;
		нач
			l6done := ложь;
			Next;
			нцДо
				просей ch из
				|"(": Next; если ch = "*" то Comment; SkipWhitespace всё;
				|"*": Next; если ch =")" то Next; l6done:= истина всё;
				| 0X: l6done := истина;
				иначе Next;
				всё;
			кцПри l6done;
		кон Comment;

		проц l7String(delimiter: симв8);
		перем l0done: булево;
		нач
			l0done := ложь; Next;
			нцДо
				если ch = delimiter то l0done := истина; Next;
				аесли ch = 0X то l0done := истина; error := истина;
				иначе Append(ch); Next;
				всё;
			кцПри l0done или (i=длинаМассива(string)-1);
		кон l7String;

	нач
		i := 0; done := ложь;
		ch := r.ПодглядиСимв8(); (* restart scanning *)
		SkipWhitespace;
		нцДо
			просей ch из
				"(": Next; если ch = "*" то Comment ; SkipWhitespace иначе Append(ch) всё;
				| "*": Next; если ch = ")" то Next; SkipWhitespace иначе Append(ch) всё;
				| '"', "'": done := истина; если i = 0 то l7String(ch) всё;
				| 0X .. ' ', '~', ';': done := истина;
			иначе
				Append(ch);
				Next;
			всё;
		кцПри done или (i = длинаМассива(string)-1);
		string[i] := 0X;
		возврат (i > 0) и done и ~error;
	кон GetStringParameter;

	проц GetTracingDiagnostics*(diagnostics: Diagnostics.Diagnostics): Diagnostics.Diagnostics;
	перем tracing: TracingDiagnostics;
	нач
		нов(tracing, diagnostics); возврат tracing
	кон GetTracingDiagnostics;

нач
	InitErrorMessages;
	InitWindowWriter;
	emptyString := MakeString("");
	debug := ложь;
	invalidPosition.start := -1;
	invalidPosition.end := -1;
	invalidPosition.line := -1;
	invalidPosition.linepos := -1;
	нов(integerObjects, 128);
кон FoxBasic.

FoxBasic.ActivateDebug ~

FoxBasic.Test ~

модуль POP3Client; (** AUTHOR "TF"; PURPOSE "Simple POP3 Client"; *)

использует
	Потоки, Files, IP, DNS, TCP, Strings, ЛогЯдра;

конст
	StateIdle = 0;
	StateConnected = 1;
	StateAuthenticate = 2;
	StateTransaction = 3;

	ResOk* = 0;
	ResFailed* = 1;
	ResAlreadyOpen* = 2;
	ResServerNotFound* = 3;
	ResNoConnection* = 4;
	ResUserPassError* = 5;
	ResServerNotReady* = 6;
	ResServerFailed* = 7;

тип POP3Client* = окласс
	перем connection : TCP.Connection;
		w : Потоки.Писарь; (* writer on the control connection *)
		r : Потоки.Чтец; (* reader on the control connection *)
		state : цел32;
		message : массив 513 из симв8;

		проц Connect*(конст host: массив из симв8; port : цел32; конст user, password: массив из симв8;  перем res : целМЗ);
		перем fadr : IP.Adr;
		нач {единолично}
			res := 0;
			если state # StateIdle то res := ResAlreadyOpen; возврат всё;
			DNS.HostByName(host, fadr, res);
			если res = DNS.Ok то
				нов(connection);
				connection.Open(TCP.NilPort, fadr, port, res);
				если res = TCP.Ok то
					Потоки.НастройПисаря(w, connection.ЗапишиВПоток);
					Потоки.НастройЧтеца(r, connection.ПрочтиИзПотока);
					state := StateConnected;
					если ReadResponse(message) то state := StateAuthenticate;
						если Login(user, password) то state := StateTransaction
						иначе res := ResUserPassError
						всё
					всё
				иначе res := ResNoConnection
				всё;
				если state = StateIdle то connection.Закрой(); w := НУЛЬ; r := НУЛЬ всё
			иначе res := ResServerNotFound
			всё
		кон Connect;

		проц Login*(конст user, password : массив из симв8) : булево;
		нач
			w.пСтроку8("USER "); w.пСтроку8(user); w.пВК_ПС; w.ПротолкниБуферВПоток;
			если ReadResponse(message) то
				w.пСтроку8("PASS "); w.пСтроку8(password); w.пВК_ПС; w.ПротолкниБуферВПоток;
				если ReadResponse(message) то
					возврат истина
				иначе возврат ложь
				всё
			иначе возврат ложь
			всё;
		кон Login;

		проц Quit*;
		нач {единолично}
			w.пСтроку8("QUIT"); w.пВК_ПС; w.ПротолкниБуферВПоток;
			если ReadResponse(message) то всё;
			state := StateIdle;
			connection.Закрой;
			w := НУЛЬ; r := НУЛЬ
		кон Quit;

		проц List*;
		перем nr, len : цел32;
		нач {единолично}
			w.пСтроку8("LIST"); w.пВК_ПС; w.ПротолкниБуферВПоток;
			если ReadResponse(message) то
				нцПока r.ПодглядиСимв8() # "." делай
					r.чЦел32(nr, ложь); r.ПропустиБелоеПоле; r.чЦел32(len, ложь); r.ПропустиДоКонцаСтрокиТекстаВключительно;
					ЛогЯдра.пСтроку8("Message"); ЛогЯдра.пЦел64(nr, 2); ЛогЯдра.пСтроку8(" "); ЛогЯдра.пЦел64(len, 0);  ЛогЯдра.пВК_ПС;
				кц;
				r.ПропустиДоКонцаСтрокиТекстаВключительно
			всё;
		кон List;

		проц GetMessage*(nr : цел32; конст filename : массив из симв8) : булево;
		перем str : массив 1024 из симв8; f : Files.File; fw : Files.Writer;
		нач {единолично}
			f := Files.New(filename);
			если f # НУЛЬ то Files.OpenWriter(fw, f, 0)
			иначе возврат ложь
			всё;

			w.пСтроку8("RETR "); w.пЦел64(nr, 0); w.пВК_ПС; w.ПротолкниБуферВПоток;
			если ReadResponse(message) то
				нцДо
					r.чСтроку8ДоКонцаСтрокиТекстаВключительно(str);
					если str # "." то
						если str[0] = "." то Strings.Delete(str, 0, 1) всё;
						fw.пСтроку8(str); fw.пВК_ПС;
						ЛогЯдра.пСтроку8(str)
					всё
				кцПри (str = ".") или (r.кодВозвратаПоследнейОперации # 0);
				fw.ПротолкниБуферВПоток;
				Files.Register(f);
				возврат r.кодВозвратаПоследнейОперации = 0
			иначе возврат ложь
			всё;
		кон GetMessage;

		проц NOOP*;
		нач {единолично}
			w.пСтроку8("LIST"); w.пВК_ПС; w.ПротолкниБуферВПоток;
			если ReadResponse(message) то
			всё
		кон NOOP;

		проц ReadResponse(перем message : массив из симв8) : булево;
		перем ch : симв8; tok : массив 4 из симв8;
		нач
			ch := r.чИДайСимв8(); r.чЦепочкуСимв8ДоБелогоПоля(tok); r.ПропустиБелоеПоле;  r.чСтроку8ДоКонцаСтрокиТекстаВключительно(message);
			ЛогЯдра.пСтроку8("message = "); ЛогЯдра.пСтроку8(message); ЛогЯдра.пВК_ПС;
			возврат ch = "+"
		кон ReadResponse;

	кон POP3Client;

проц Test*;
перем client : POP3Client; res : целМЗ;
нач
	нов(client);
	client.Connect("lillian.ethz.ch", 110, "user", "password", res);
	если res = 0 то
		 client.List;
		если client.GetMessage(2, "test.txt") то ЛогЯдра.пСтроку8(" download ok ") иначе ЛогЯдра.пСтроку8("download failed.");   всё;
	иначе ЛогЯдра.пСтроку8("res = "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;
	всё;
	client.Quit;
кон Test;

кон POP3Client.

POP3Client.Test
System.Free POP3Client



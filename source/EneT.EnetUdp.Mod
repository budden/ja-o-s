модуль EnetUdp;
(**
	AUTHOR: Alexey Morozov, HighDim GmbH, 2015
	PURPOSE: Ethernet networking stack, UDP protocol
*)

использует
	НИЗКОУР, EnetBase, Interfaces := EnetInterfaces, EnetUtils, EnetTiming, Trace := EnetTrace;

конст
	MaxNumSockets* = 32; (** maximal number of UDP sockets *)

тип
	Int32 = EnetBase.Int32;
	Int16 = EnetBase.Int16;
	Int = EnetBase.Int;
	ResultCode = EnetBase.ResultCode;

	(* UDP received datagram handler *)
	RecvDatagramHandler* = проц{делегат}(
														socket: Socket;
														конст srcAddr: EnetBase.IpAddr;
														srcPort: Int;
														перем data: массив из симв8;
														dataOffs, dataLen: Int;
														packet: EnetBase.Packet
														);

	(**
		UDP socket (must not be shared among multiple threads)
	*)
	Socket* = укль на запись
		intf*: EnetBase.Interface; (** the used interface; selection of the interface is done based on the IP address of the destination *)

		localPort*: Int; (** local (source) port *)
		sendToIpAddr*: EnetBase.IpAddr; (** IP address to send to *)
		sendToPort*: Int; (** port to send to *)
		resolvedSendToIpAddr: булево;

		recvHandler*: RecvDatagramHandler; (** installed handler of received datagrams *)
		recvHandlerParam *: динамическиТипизированныйУкль; (** user-defined parameter for the receiver handler *)

		sendToMacAddr*: EnetBase.MacAddr;

		sendToIpAddrLast: EnetBase.IpAddr;
		sendToMacAddrLast: EnetBase.MacAddr;

		localPortNet: Int16; (* local port in the network byte order *)
	кон;

перем
	sockets: массив MaxNumSockets из Socket;
	numSockets: Int;

	(**
		Create a UDP socket given a local port number

		localPort: local (source) port
		recvHandler: handler of received datagrams
		socket: reference to the created socket (NIL in case of an error)
		res: result code

		returns NIL in case of an error
	*)
	проц NewSocket*(
										перем socket: Socket;
										localPort: Int;
										перем res: ResultCode
									): булево;
	нач
		если numSockets >= MaxNumSockets то
			res := EnetBase.ErrOutOfResources;
			возврат ложь;
		всё;
		если (localPort < 0) или (localPort > 65536) то
			res := EnetBase.ErrInvalidValue;
			возврат ложь;
		всё;
		нов(socket);
		socket.localPort := localPort;
		socket.sendToPort := 0;
		socket.recvHandler := НУЛЬ;

		если EnetBase.LittleEndianSystem то
			socket.localPortNet := EnetUtils.SwitchEndianness16(Int16(localPort));
		иначе
			socket.localPortNet := Int16(localPort);
		всё;

		socket.sendToIpAddrLast := EnetBase.NilIpAddr;

		sockets[numSockets] := socket;
		увел(numSockets);

		res := 0;
		возврат истина;
	кон NewSocket;

	(**
		Setup socket's received datagram handler
	*)
	проц SetRecvHandler*(socket: Socket; recvHandler: RecvDatagramHandler; перем res: ResultCode): булево;
	нач
		socket.recvHandler := recvHandler;
		res := 0;
		возврат истина;
	кон SetRecvHandler;

	(**
		Setup default destination of datagrams sent on a given socket using Send procedure
	*)
	проц SetDestination*(socket: Socket; конст sendToIpAddr: EnetBase.IpAddr; sendToPort: Int; completionHandler: EnetBase.TaskHandler; перем res: ResultCode): булево;
	нач
		если (sendToPort <= 0) или (sendToPort >= 65535) то
			res := EnetBase.ErrInvalidValue;
			возврат ложь;
		всё;

		если ~Interfaces.ResolveIpAddr(sendToIpAddr,socket.sendToMacAddr,socket.intf,completionHandler,res) то возврат ложь; всё;
		если res = 0 то socket.resolvedSendToIpAddr := истина;
		иначе socket.resolvedSendToIpAddr := ложь;
		всё;
		socket.sendToIpAddr := sendToIpAddr;
		socket.sendToPort := sendToPort;
		возврат истина;
	кон SetDestination;

	(**
		Send a datagram on a given socket
	*)
	проц Send*(socket: Socket; конст data: массив из симв8; offset, length: Int; flags: мнвоНаБитахМЗ; completionHandler: EnetBase.TaskHandler; перем res: ResultCode): булево;
	перем
		packet: EnetBase.Packet;
		intf: EnetBase.Interface;
		dev: EnetBase.LinkDevice;
		payloadOffs: Int;
	нач

		если socket.sendToPort = 0 то res := EnetBase.ErrInvalidValue; возврат ложь; всё;
		если socket.intf = НУЛЬ то res := EnetBase.ErrNoIntfFound; возврат ложь; всё;

		intf := socket.intf;
		если ~socket.resolvedSendToIpAddr то
			если ~Interfaces.ResolveIpAddr(socket.sendToIpAddr,socket.sendToMacAddr,socket.intf,НУЛЬ,res) то возврат ложь; всё;
		всё;

		dev := intf.dev;
		если ~Interfaces.GetTxPacket(intf,packet) то res := EnetBase.ErrTxPacketPoolEmpty; возврат ложь; всё;

		если socket.sendToIpAddr.ver = 4 то
			EnetBase.SetUdpPacket(packet,
									intf.dev.macAddr,
									socket.sendToMacAddr,
									intf.ipv4Addr,
									socket.sendToIpAddr,
									socket.localPort,
									socket.sendToPort,
									length
									);
		иначе
			EnetBase.SetUdpPacket(packet,
									intf.dev.macAddr,
									socket.sendToMacAddr,
									intf.ipv6Addr,
									socket.sendToIpAddr,
									socket.localPort,
									socket.sendToPort,
									length
									);
		всё;

		если ~dev.setPacketPayload(dev,packet,data,offset,flags,res) то
			возврат ложь;
		всё;

		возврат Interfaces.SendPacket(intf,packet,flags,completionHandler,res);
	кон Send;

	(**
		Send at datagram on a given socket to a specified destination
	*)
	проц SendTo*(socket: Socket; конст sendToIpAddr: EnetBase.IpAddr; sendToPort: цел32; конст data: массив из симв8; offset, length: Int; flags: мнвоНаБитахМЗ; completionHandler: EnetBase.TaskHandler; перем res: ResultCode): булево;
	перем
		packet: EnetBase.Packet;
		intf: EnetBase.Interface;
		dev: EnetBase.LinkDevice;
		payloadOffs: Int;
	нач
		если sendToPort = 0 то
			res := EnetBase.ErrInvalidValue;
			возврат ложь;
		всё;

		если ~(sendToIpAddr = socket.sendToIpAddrLast) то
			если Interfaces.ResolveIpAddr(sendToIpAddr,socket.sendToMacAddrLast,socket.intf,НУЛЬ,res) то
				socket.sendToIpAddrLast := sendToIpAddr;
			иначе возврат ложь;
			всё;
		всё;

		intf := socket.intf;
		dev := intf.dev;
		если ~Interfaces.GetTxPacket(intf,packet) то
			res := EnetBase.ErrTxPacketPoolEmpty;
			возврат ложь;
		всё;

		если socket.sendToIpAddrLast.ver = 4 то
			EnetBase.SetUdpPacket(packet,
									intf.dev.macAddr,
									socket.sendToMacAddrLast,
									intf.ipv4Addr,
									socket.sendToIpAddrLast,
									socket.localPort,
									sendToPort,
									length
									);
		иначе
			EnetBase.SetUdpPacket(packet,
									intf.dev.macAddr,
									socket.sendToMacAddrLast,
									intf.ipv6Addr,
									socket.sendToIpAddrLast,
									socket.localPort,
									sendToPort,
									length
									);
		всё;

		если ~dev.setPacketPayload(dev,packet,data,offset,flags,res) то
			возврат ложь;
		всё;

		возврат Interfaces.SendPacket(intf,packet,flags,completionHandler,res);
	кон SendTo;

	(**
		Handling of an UDP packet
	*)
	проц HandlePacket*(intf: EnetBase.Interface; packet: EnetBase.Packet; flags: мнвоНаБитахМЗ);
	перем
		i: Int;
		socket: Socket;
		srcPort: Int;
		dstPort: Int16;
		srcIpAddr: EnetBase.IpAddr;
		payloadOffs, payloadLen: Int;
	нач
		dstPort := packet.udpHdr.dstPort;

		(*
			search a socket bound to the packet's destination port
		*)
		i := 0;
		нцПока (i < numSockets) и (dstPort # sockets[i].localPortNet) делай (*! optimize socket search (e.g. search only among the sockets with installed recv handlers etc.) *)
			увел(i);
		кц;
		если i < numSockets то
			если EnetBase.EnableTrace то Trace.StringLn("EnetUdp: Received UDP datagram for open port " и dstPort); всё;
			socket := sockets[i];
			если socket.recvHandler # НУЛЬ то
				payloadOffs := размер16_от(EnetBase.EthFrameHdr)+размер16_от(EnetBase.UdpHdr);
				если ~(EnetBase.FlagIpv6 в flags) то (* IPv4 *)
					srcIpAddr.addr[0] := packet.ipv4Hdr.srcIpAddr;
					srcIpAddr.ver := 4;
					увел(payloadOffs,размер16_от(EnetBase.Ipv4Hdr));
				иначе (* IPv6 *)
					srcIpAddr.addr[0] := packet.ipv6Hdr.srcIpAddr[0];
					srcIpAddr.addr[1] := packet.ipv6Hdr.srcIpAddr[1];
					srcIpAddr.addr[2] := packet.ipv6Hdr.srcIpAddr[2];
					srcIpAddr.addr[3] := packet.ipv6Hdr.srcIpAddr[3];
					srcIpAddr.ver := 6;
					увел(payloadOffs,размер16_от(EnetBase.Ipv6Hdr));
				всё;
				srcPort := Int(EnetUtils.SwitchEndianness16(packet.udpHdr.srcPort)) остОтДеленияНа 10000H;
				packet.payloadOffs := payloadOffs;
				payloadLen := EnetUtils.SwitchEndianness16(packet.udpHdr.length) - размер16_от(EnetBase.UdpHdr);
				socket.recvHandler(socket,srcIpAddr,srcPort,packet.data^,packet.dataOffs+payloadOffs,payloadLen,packet);
			всё;
		иначе
			если EnetBase.EnableTrace то Trace.StringLn("EnetUdp: Received UDP datagram on closed port " и dstPort); всё;
		всё;

	кон HandlePacket;

	проц Install*(intf: EnetBase.Interface);
	нач
		EnetBase.SetIpPacketHandler(intf,EnetBase.ProtoUdp,HandlePacket);
	кон Install;
кон EnetUdp.

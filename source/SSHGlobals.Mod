модуль SSHGlobals; (* GF ??.??.2002 / 10.12.2020 *)

использует
		Texts, TextUtilities, UTF8Strings, Strings,
		Ciphers := CryptoCiphers, Log := ЛогЯдра;

конст
	ConfigFile* = "SSH.Configuration.Text";
	HostkeysFile* = "SSH.KnownHosts";		(* entries in openssh format! *)
	PrivateKeyFile* = "SSH.RSAKey.priv";
	PublicKeyFile* = "SSH.RSAKey.pub";			(* openssh format *)

	HT= 09X;  CR = 0DX; NL = 0AX;

тип
	Buffer = укль на массив из симв8;

перем
	buf: Buffer;
	bp: цел32; (* current position in buf *)

	debug-: булево;		

	hexd: массив 17 из симв8;	(* constant *)

	(*-------------- Debugging -----------------------*)

	проц DebugOn*;
	нач
		Log.пВК_ПС;
		debug := истина
	кон DebugOn;
	
	проц DebugOff*;
	нач
		debug := ложь
	кон DebugOff;

	(*--------------- SSH configuration  -----------------*)

	проц NextLine;
	нач
		нцДо
			нцПока (buf[bp] = HT) или (buf[bp] >= ' ') делай  увел( bp )  кц;
			нцПока (buf[bp] = CR) или (buf[bp] = NL) делай  увел( bp )  кц
		кцПри (buf[bp] # '#') или (buf[bp] = 0X);  (* skip comments *)
	кон NextLine;

	проц GetConfigString( перем str: массив из симв8 );
	перем i: цел32;
	нач
		нцПока(buf[bp] = HT) или (buf[bp] = ' ') делай  увел( bp )  кц;
		i := 0;
		нцПока buf[bp] > ' ' делай
			str[i] := buf[bp];  увел( i ); увел( bp )
		кц;
		str[i] := 0X
	кон GetConfigString;

	проц GetConfigInt( перем i: цел32 );
	нач
		нцПока(buf[bp] = HT) или (buf[bp] = ' ') делай  увел( bp )  кц;
		i := 0;
		нцПока (buf[bp] >= '0') и (buf[bp] <= '9') делай
			i := 10*i + кодСимв8( buf[bp] ) - кодСимв8( '0' );
			увел( bp )
		кц
	кон GetConfigInt;


	проц GetCipherList*( перем list: массив из симв8 );
	перем n: цел16;
		x: массив 64 из симв8;
	нач
		если buf = НУЛЬ то  buf := GetConfigBuffer()  всё;
		bp := 0;  n := 0;  копируйСтрокуДо0( "", list );
		если buf[bp] = '#' то  NextLine  всё;
		нцПока buf[bp] # 0X делай
			GetConfigString( x );
			если x = "cipher" то
				GetConfigString( x );
				если n > 0 то  Strings.Append( list, "," )  всё;
				Strings.Append( list, x );  увел( n );
			всё;
			NextLine
		кц
	кон GetCipherList;

	проц GetHMacList*( перем list: массив из симв8 );
	перем n: цел16;
		x: массив 64 из симв8;
	нач
		если buf = НУЛЬ то  buf := GetConfigBuffer()  всё;
		bp := 0;  n := 0;  копируйСтрокуДо0( "", list );
		если buf[bp] = '#' то  NextLine  всё;
		нцПока buf[bp] # 0X делай
			GetConfigString( x );
			если x = "hmac" то
				GetConfigString( x );
				если n > 0 то  Strings.Append( list, "," )  всё;
				Strings.Append( list, x );  увел( n );
			всё;
			NextLine
		кц
	кон GetHMacList;


	проц GetCipherParams*(	конст name: массив из симв8;
											перем modname: массив из симв8;
											перем bits: цел32;
											перем mode: цел8 );
	перем x: массив 128 из симв8;
	нач
		если buf = НУЛЬ то  buf := GetConfigBuffer()  всё;
		bp := 0;
		копируйСтрокуДо0( "unknown", modname );  bits := 0;
		если buf[bp] = '#' то  NextLine  всё;
		нцДо
			GetConfigString( x );
			если x = "cipher" то  GetConfigString( x );
				если x = name то
					GetConfigString( modname );  GetConfigInt( bits );
					GetConfigString( x );
					если x = "CBC" то  mode := Ciphers.CBC
					аесли x = "CTR" то  mode := Ciphers.CTR
					иначе  mode := Ciphers.ECB
					всё;
				всё
			всё;
			NextLine
		кцПри (buf[bp] = 0X) или (modname # "unknown")
	кон GetCipherParams;


	проц GetKexMethods*( перем kex: массив из симв8 );
	перем x: массив 128 из симв8; n: целМЗ;
	нач
		если buf = НУЛЬ то  buf := GetConfigBuffer()  всё;
		bp := 0;
		если buf[bp] = '#' то  NextLine  всё;
		kex := "";  n := 0;
		нцДо
			GetConfigString( x );
			если x = "kex" то  
				GetConfigString( x );
				если n # 0 то  Strings.Append( kex, "," )  всё;
				Strings.Append( kex, x );
				увел( n )
			всё;
			NextLine
		кцПри buf[bp] = 0X
	кон GetKexMethods;


	проц GetHMacParams*(	конст name: массив из симв8;
									перем modname: массив из симв8;  перем bytes: цел32 );
	перем x: массив 128 из симв8;
	нач
		если buf = НУЛЬ то  buf := GetConfigBuffer()  всё;
		bp := 0;
		копируйСтрокуДо0( "unknown", modname );  bytes := 0;
		если buf[bp] = '#' то  NextLine  всё;
		нцДо
			GetConfigString( x );
			если x = "hmac" то  GetConfigString( x );
				если x = name то
					GetConfigString( modname );  GetConfigInt( bytes )
				всё
			всё;
			NextLine
		кцПри (buf[bp] = 0X) или (modname # "unknown")
	кон GetHMacParams;
	проц ExpandBuf( перем buf: Buffer; newSize: размерМЗ );
	перем newBuf: Buffer; i: размерМЗ;
	нач
		если длинаМассива( buf^ ) >= newSize то возврат всё;
		нов( newBuf, newSize );
		нцДля i := 0 до длинаМассива( buf^ ) - 1 делай  newBuf[i] := buf[i]  кц;
		buf := newBuf;
	кон ExpandBuf;


	проц GetConfigBuffer(): Buffer;
	перем
		text: Texts.Text; r: Texts.TextReader;
		ch: симв32; format: цел32; len, i, j, bytesPerChar: размерМЗ; res: целМЗ;
		buffer: Buffer;
	нач
		нов( text );
		TextUtilities.LoadAuto( text, ConfigFile, format, res );
		если res # 0 то
			Log.пСтроку8( "could not open file " ); Log.пСтроку8( ConfigFile ); Log.пВК_ПС
		иначе
			text.AcquireRead;
			нов( r, text ); r.SetPosition( 0 );
			len := text.GetLength();
			bytesPerChar := 2;
			нов( buffer, bytesPerChar*len );
			j := 0;
			нцДля i := 0 до len - 1 делай  r.ReadCh( ch );
				нцПока ~UTF8Strings.EncodeChar( ch, buffer^, j ) делай
					(* buffer too small *)
					увел( bytesPerChar );
					ExpandBuf( buffer, bytesPerChar*len );
				кц
			кц;
			buffer[j] := 0X;
			text.ReleaseRead;
			возврат buffer
		всё
	кон GetConfigBuffer;
	
	
	проц ShowByte*( c: симв8 );
	нач
		просей c из
		| CR:  Log.пСтроку8(" \r" )
		| NL:  Log.пСтроку8( " \n" )
		| HT:  Log.пСтроку8( " \t" )
		| ' ' .. '~':
			Log.пСимв8( ' ' );  Log.пСимв8( c )
		иначе
			Log.пСимв8( '\' );  Log.п16ричное( кодСимв8( c ), -2 )
		всё
	кон ShowByte;


нач
	buf := НУЛЬ;
	hexd := "0123456789ABCDEF";
кон SSHGlobals.



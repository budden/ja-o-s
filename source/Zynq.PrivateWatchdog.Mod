модуль PrivateWatchdog; (** AUTHOR "Timothée Martiel, 11/2017"; PURPOSE "Zynq private watchdog driver"; *)
использует НИЗКОУР, Platform;

конст
	(** Modes *)
	Reset * = истина; (** Resets the system *)
	Interrupt * = ложь; (** Triggers an interrupt *)

	ControlWatchdogEnable = 0;
	ControlAutoReload = 1;
	ControlItEnable = 2;
	ControlWdMode = 3;
	ControlPrescalerOfs = 8;
	ControlPrescalerMask = {8 .. 15};

перем
	frequency: цел64;

	(** Start the private watchdog with the given mode and delay *)
	проц Start * (mode: булево; delay: цел32);
	перем
		val: мнвоНаБитахМЗ;
	нач
		утв(frequency > 0);
		Platform.mpcore.Watchdog_Reset_Status_Register := 1; (* Clear the reset status *)
		Feed(delay);
		val := {ControlWatchdogEnable};
		если mode то
			(* Reset *)
			включиВоМнвоНаБитах(val, ControlWdMode)
		иначе
			включиВоМнвоНаБитах(val, ControlItEnable)
		всё;
		Platform.mpcore.Watchdog_Control_Register := НИЗКОУР.подмениТипЗначения(цел32, val)
	кон Start;

	(** Stop private watchdog *)
	проц Stop *;
	нач
		Platform.mpcore.Watchdog_Disable_Register := Platform.PrivateWatchdogDisableKey0;
		Platform.mpcore.Watchdog_Disable_Register := цел32(Platform.PrivateWatchdogDisableKey1);
		Platform.mpcore.Watchdog_Control_Register := 0
	кон Stop;

	(** Feed the watchdog: overwrites its count with the given delay *)
	проц Feed * (delay: цел32);
	нач
		Platform.mpcore.Watchdog_Load_Register := цел32(цел64(delay) * frequency);
	кон Feed;

	(** Check if the watchdog has been triggered *)
	проц Triggered * (): булево;
	нач
		возврат Platform.mpcore.Watchdog_Reset_Status_Register = 1
	кон Triggered;

	(** Initialise watchdog reference frequency (CPU freq / 2) *)
	проц Init * (timerFrequency: цел64);
	нач
		frequency := timerFrequency
	кон Init;
кон PrivateWatchdog.

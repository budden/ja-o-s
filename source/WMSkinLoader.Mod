модуль WMSkinLoader;	(** AUTHOR "ug"; December 1, 2005 *)

использует
	Commands, Modules, WMComponents, WMStandardComponents,
	Strings, UTF8Strings, Files,
	Looks, ЛогЯдра,
	WM := WMWindowManager;

конст
	Toleft = 300;
	Fromtop = 400;
	PanelWidth = 100;
	ButtonHeight = 20;

тип
	Window=окласс (WMComponents.FormWindow);
	перем
		nofLooks : цел32;
		nextINchain : Window;
		buttonArr : укль на массив из WMStandardComponents.Button;
		lookList : Looks.LookList;

		проц &New *(lookList : Looks.LookList);
		перем
			panel : WMStandardComponents.Panel;
			i : цел32;
			look : Looks.Look;
		нач
			утв((lookList # НУЛЬ) и (lookList.GetCount() > 0));
			сам.lookList := lookList;
			nofLooks := lookList.GetCount ();

			нов (panel);
			panel.bounds.SetWidth (PanelWidth);
			panel.bounds.SetHeight (nofLooks * ButtonHeight);

			нов(buttonArr, nofLooks);
			нцДля i := 0 до nofLooks-1 делай
				нов(buttonArr[i]);
				look := lookList.GetItem (i);
				buttonArr[i].caption.SetAOC(look.name); buttonArr[i].alignment.Set(WMComponents.AlignTop);
				buttonArr[i].bounds.SetWidth(PanelWidth); buttonArr[i].bounds.SetHeight(ButtonHeight);
				buttonArr[i].onClick.Add(LoadSkin);
				panel.AddContent(buttonArr[i]);
			кц;

			Init (panel.bounds.GetWidth (), panel.bounds.GetHeight (), истина);
			SetContent (panel);

			WM.AddWindow (сам, Toleft, Fromtop);
			manager := WM.GetDefaultManager();
			manager.SetFocus(сам);
			SetTitle (WM.NewString ("SkinLoader"));

			nextINchain := windows;
			windows := сам
		кон New;

		проц FindSender(sender : WMStandardComponents.Button; перем index : цел32);
		перем i : цел32;
		нач
			i := 0;
			нцПока (i < nofLooks) и (buttonArr[i] # sender) делай увел(i) кц;
			если i < nofLooks то index := i иначе index := -1 всё
		кон FindSender;

		проц LoadSkin(sender, data : динамическиТипизированныйУкль);
		перем look : Looks.Look;
			cmd, msg : массив 128 из симв8;
			index: цел32; res: целМЗ;
		нач
			если sender суть WMStandardComponents.Button то
				FindSender(sender(WMStandardComponents.Button), index);
				если index >= 0 то
					look := lookList.GetItem(index);
					если (UTF8Strings.Compare(look.name, "ZeroSkin") = UTF8Strings.CmpEqual) то
						cmd := "SkinEngine.Unload";
					иначе
						Strings.Concat("SkinEngine.Load ", look.file, cmd);
					всё;
					Commands.Call(cmd, {}, res, msg);
					если res # 0 то ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8(msg); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования всё
				всё
			всё
		кон LoadSkin;

		проц {перекрыта}Close*;
		нач
			Close^;
			FreeWindow(сам)
		кон Close;

	кон Window;

перем
	windows : Window;

проц FreeWindow(free : Window);
перем
	win : Window;
нач
	если free = windows то
		windows := windows.nextINchain
	иначе
		win := windows;
		нцПока (win # НУЛЬ) и (win.nextINchain # free) делай
			win := win.nextINchain
		кц;
		если win # НУЛЬ то
			win.nextINchain := free.nextINchain
		всё
	всё
кон FreeWindow;

проц Open*(context : Commands.Context);
перем win: Window; filename : Files.FileName; lookList : Looks.LookList;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename);
	Looks.LoadLooks(filename, lookList);
	если (lookList.GetCount() > 0) то
		нов(win, lookList);
	иначе
		context.out.пСтроку8("WMSkinLoader: No looks found in file '");
		context.out.пСтроку8(filename); context.out.пСтроку8("'");
		context.out.пВК_ПС;
	всё;
кон Open;

проц Cleanup;
нач
	нцПока windows # НУЛЬ делай
		windows.Close();
	кц
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон WMSkinLoader.

System.Free WMSkinLoader Looks ~
WMSkinLoader.Open SkinList.XML ~



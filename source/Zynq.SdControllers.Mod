модуль SdControllers;
(**
	AUTHOR Timothée Martiel, 12/2015
	PURPOSE SD Host Controller Initialization for Zynq SoC.
*)

использует
	Platform, Modules, Objects, Commands,
	Sd, SdDisks, SdEnvironment, Log := SdEnvironment;

конст
	Ready = 0;
	Running = 1;
	Stopped = 2;
	Error = 3;

тип
	HostController * = окласс
	перем
		hc: Sd.HostController;
		handler: InterruptHandler;
		state, event: цел32;
		card: Sd.Card;
		halted: булево;

		проц & Init (base: адресВПамяти; int: цел32; clock: цел64);
		перем
			result: целМЗ;
		нач
			нов(hc);
			Sd.InitHostController(hc, base);
			если ~Sd.SetExternalClock(hc, clock, clock, result) то
				Log.String("[SD] Failed to initialize host controller: error code ");
				Log.Int(result, 0);
				Log.Ln;
				state := Error;
				возврат
			всё;
			state := Ready; (* Do not put this later, as events might be triggered in the constructor *)
			нов(handler, сам);
			SdEnvironment.InstallHandler(handler.Handle, int);
		кон Init;

		проц HandleEvent (card: Sd.Card; event: цел32; param: динамическиТипизированныйУкль);
		нач {единолично}
			дождись(state # Running);
			сам.event := event;
			сам.card := card;
			если state = Ready то state := Running всё
		кон HandleEvent;

		проц Stop;
		нач {единолично}
			если state < Stopped то state := Stopped всё;
			дождись(halted)
		кон Stop;

		проц WaitForEventCompletion;
		нач {единолично}
			дождись(state # Running)
		кон WaitForEventCompletion;

	нач {активное}
		нц
			нач {единолично}
				дождись(state # Ready);
				если state >= Stopped то прервиЦикл всё;
			кон;
			SdDisks.HandleSdEvent(card, event);
			нач {единолично}
				state := Ready
			кон
		кц
	выходя
		нач {единолично} halted := истина кон
	кон HostController;

	InterruptHandler * = окласс
	перем
		hc: Sd.HostController;
		timer: Objects.Timer;
		mask: мнвоНаБитахМЗ;
		blocked: булево;

		проц & Init (hc: HostController);
		нач
			сам.hc := hc.hc;
			нов(timer);
			blocked := ложь
		кон Init;

		проц Block (hc: Sd.HostController; mask: мнвоНаБитахМЗ; timeout: цел32): булево;
		перем
			irqs: мнвоНаБитахМЗ;
		нач {единолично}
			утв(hc = сам.hc);
			blocked := истина;
			irqs := hc.regs.InterruptSignalEnable;
			hc.regs.InterruptSignalEnable := irqs + mask;
			сам.mask := mask;
			Objects.SetTimeout(timer, Unblock, timeout);
			дождись(~blocked);
			hc.regs.InterruptSignalEnable := irqs;
			возврат mask * hc.regs.InterruptStatus # {}
		кон Block;

		проц Unblock;
		нач {единолично}
			blocked := ложь
		кон Unblock;

		проц Handle;
		нач
			если hc.regs.InterruptStatus * mask # {} то
				Unblock;
				Objects.CancelTimeout(timer);
			всё;
			Sd.HandleInterrupt(hc);
		кон Handle;
	кон InterruptHandler;

перем
	hc: массив 2 из HostController;

	проц Init;
	перем
		i: цел32;
	нач
		Modules.InstallTermHandler(Cleanup);
		нцДля i := 0 до Platform.SdNb - 1 делай
			если SdEnvironment.Enable(i) то
				если Sd.EnableTrace то
					Log.String("[SD] Enabling controller "); Log.Int(i, 0); Log.Ln;
					Log.String("[SD]	register base = "); Log.Address(Platform.SdBase[i]); Log.Ln;
					Log.String("[SD]	base clock = "); Log.Int(SdEnvironment.HcClock(i), 0); Log.String(" Hz"); Log.Ln;
					Log.String("[SD]	irq = "); Log.Int(Platform.SdIrq[i], 0); Log.Ln;
				всё;
				нов(hc[i], Platform.SdBase[i], Platform.SdIrq[i], SdEnvironment.HcClock(i));
				Sd.SetEventHandler(hc[i].hc, hc[i].HandleEvent, НУЛЬ);
				hc[i].WaitForEventCompletion;
(*				Sd.SetBlocker(hc[i].hc, hc[i].handler.Block)*)
			иначе
				если Sd.EnableTrace то
					Log.String("[SD] Not Enabling controller "); Log.Int(i, 0); Log.Ln
				всё;
			всё
		кц
	кон Init;

	проц Cleanup;
	перем
		i: цел32;
	нач
		нцДля i := 0 до длинаМассива(hc) - 1 делай
			если hc[i] # НУЛЬ то hc[i].Stop всё
		кц
	кон Cleanup;

	проц Statistics * (c: Commands.Context);
	перем
		accesses: цел32;
		byteRead, byteWritten, read, write: цел64;
		tread, twrite: цел64;
		speedR, speedW: вещ64;
	нач
		byteRead := Sd.NbyteRead;
		byteWritten := Sd.NbyteWritten;
		read := Sd.Nread;
		write := Sd.Nwrite;
		tread := SdEnvironment.ToMicro(Sd.Tread);
		twrite := SdEnvironment.ToMicro(Sd.Twrite);

		если read > 0 то
			speedR := вещ64(byteRead) / вещ64(tread);
			c.out.пСтроку8("SD Statistics:"); c.out.пВК_ПС;
			c.out.пСтроку8("  Bytes read: "); c.out.пЦел64(byteRead, 0); c.out.пВК_ПС;
			c.out.пСтроку8("  Number of reads: "); c.out.пЦел64(read, 0); c.out.пВК_ПС;
			c.out.пСтроку8("  Read time: "); c.out.пЦел64(tread, 0); c.out.пСтроку8(" us"); c.out.пВК_ПС;
			c.out.пСтроку8("  Average read size: "); c.out.пЦел64(byteRead DIV read, 0); c.out.пСтроку8(" bytes"); c.out.пВК_ПС;
			c.out.пСтроку8("  Read speed: "); c.out.пВещ64_ФФТ(speedR, 0, 3, 0); c.out.пСтроку8(" Mb/s"); c.out.пВК_ПС;
			c.out.пСтроку8("  Average read time: "); c.out.пВещ64_ФФТ(вещ64(tread) / вещ64(read), 0, 3, 0); c.out.пСтроку8(" us"); c.out.пВК_ПС;
		иначе
			c.out.пСтроку8("No read statistics");
			c.out.пВК_ПС
		всё;

		если write > 0 то
			speedW := вещ64(byteWritten) / вещ64(twrite);
			c.out.пСтроку8("  Bytes written: "); c.out.пЦел64(byteWritten, 0); c.out.пВК_ПС;
			c.out.пСтроку8("  Number of writes: "); c.out.пЦел64(write, 0); c.out.пВК_ПС;
			c.out.пСтроку8("  Write time: "); c.out.пЦел64(twrite, 0); c.out.пСтроку8(" us"); c.out.пВК_ПС;
			c.out.пСтроку8("  Average write size: "); c.out.пЦел64(byteWritten DIV write, 0); c.out.пСтроку8(" bytes"); c.out.пВК_ПС;
			c.out.пСтроку8("  Write speed: "); c.out.пВещ64_ФФТ(speedW, 0, 3, 0); c.out.пСтроку8(" Mb/s"); c.out.пВК_ПС;
			c.out.пСтроку8("  Average write time: "); c.out.пВещ64_ФФТ(вещ64(twrite) / вещ64(write), 0, 3, 0); c.out.пСтроку8(" us"); c.out.пВК_ПС;
		иначе
			c.out.пСтроку8("No write statistics");
			c.out.пВК_ПС
		всё;

		accesses := SdDisks.NcacheHits + SdDisks.NcacheMiss;
		c.out.пСтроку8("SD Disks Cache Statistics"); c.out.пВК_ПС;
		c.out.пСтроку8("  Number of accesses: "); c.out.пЦел64(accesses, 0); c.out.пВК_ПС;
		c.out.пСтроку8("  Number of hits: "); c.out.пЦел64(SdDisks.NcacheHits, 0); c.out.пСтроку8(" ("); c.out.пВещ64_ФФТ(SdDisks.NcacheHits / accesses * 100.0, 0, 2, 0); c.out.пСтроку8(" %)"); c.out.пВК_ПС;
		c.out.пСтроку8("  Number of misses: "); c.out.пЦел64(SdDisks.NcacheMiss, 0); c.out.пСтроку8(" ("); c.out.пВещ64_ФФТ(SdDisks.NcacheMiss / accesses * 100.0, 0, 2, 0); c.out.пСтроку8(" %)"); c.out.пВК_ПС;
		c.out.пСтроку8("  Number of evictions: "); c.out.пЦел64(SdDisks.NcacheEvict, 0); c.out.пСтроку8(" ("); c.out.пВещ64_ФФТ(SdDisks.NcacheEvict / SdDisks.NcacheMiss * 100.0, 0, 2, 0); c.out.пСтроку8(" %)"); c.out.пВК_ПС;
		c.out.пСтроку8("SD Disks Write Buffer Statistics"); c.out.пВК_ПС;
		c.out.пСтроку8("  Average write size: "); c.out.пВещ64_ФФТ(SdDisks.NbufferSize / SdDisks.NbufferWrites, 0, 2, 0); c.out.пСтроку8(" bytes"); c.out.пВК_ПС;
		c.out.пСтроку8("  Average queue length: "); c.out.пВещ64_ФФТ(SdDisks.NbufferQueueSize / SdDisks.NbufferQueueSamples, 0, 2, 0); c.out.пВК_ПС;
		c.out.ПротолкниБуферВПоток;
	кон Statistics;

нач
	Init
кон SdControllers.

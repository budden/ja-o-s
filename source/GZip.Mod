модуль GZip;	(** DK **)

использует Потоки, Files, Строки8, ZlibInflate, ZlibDeflate, Zlib, ZlibBuffers, Commands;

конст
	WriteError = 2907;
	DefaultWriterSize = 4096;
	DefaultReaderSize = 4096;


	BufSize = 4000H;
	FileError  = -1;


	(** compression levels **)
	DefaultCompression* = ZlibDeflate.DefaultCompression; NoCompression* = ZlibDeflate.NoCompression;
	BestSpeed* = ZlibDeflate.BestSpeed; BestCompression* = ZlibDeflate.BestCompression;


		(** compression strategies **)
	DefaultStrategy* = ZlibDeflate.DefaultStrategy; Filtered* = ZlibDeflate.Filtered; HuffmanOnly* = ZlibDeflate.HuffmanOnly;



	DeflateMethod = 8;

	(** flush values **)
	NoFlush* = ZlibDeflate.NoFlush;
	SyncFlush* = ZlibDeflate.SyncFlush;
	FullFlush* = ZlibDeflate.FullFlush;



тип

	(** Reader for buffered reading of a file via Streams.Read* procedures.  See OpenReader. *)
	Deflator* = окласс	(** not sharable between multiple processes *)
		перем
			writer: Потоки.Писарь;
			s : ZlibDeflate.Stream;
			res : целМЗ;
			crc32-: цел32; (*crc32 of uncompressed data*)
			out : укль на массив BufSize из симв8;
			flush: цел8;
			inputsize : размерМЗ;

		проц WriteHeader(w: Потоки.Писарь);
		перем
			i: цел16;
		нач
			w.пСимв8(1FX);
			w.пСимв8(8BX);
			w.пСимв8(симв8ИзКода(DeflateMethod));
			нцДля i := 0 до 6 делай w.пСимв8(0X); кц;
		кон WriteHeader;


		проц &Init*(writer: Потоки.Писарь; level, strategy, flush: цел8);
		нач
			если writer = НУЛЬ то
				res := Zlib.StreamError; возврат;
			иначе
				сам.writer := writer;
				сам.flush := flush;
				сам.WriteHeader(writer);
				res := writer.кодВозвратаПоследнейОперации;
				если res = Потоки.Успех то
					ZlibDeflate.Open(s, level, strategy, ложь);
					если s.res = ZlibDeflate.Ok то
						нов(out); ZlibBuffers.Init(s.out, out^, 0, BufSize, BufSize);
						crc32 := Zlib.CRC32(0, out^, -1, -1);
						inputsize := 0;
					иначе
						res := s.res;
					всё;
				всё;
			всё;
		кон Init;

		проц Send* (конст buf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);
		перем
			done : булево;
		нач
			утв((0 <= ofs) и (0 <= len) и (len <= длинаМассива(buf)), 110);
			если ~сам.s.open то
				сам.res := Zlib.StreamError;
			аесли (сам.res < ZlibDeflate.Ok) или (len <= 0) то
				res := сам.res;
			иначе
				ZlibBuffers.Init(сам.s.in, buf, ofs, len, len);
				увел(inputsize, len);
				нцПока (сам.res = ZlibDeflate.Ok) и (сам.s.in.avail # 0) делай
					если (сам.s.out.avail = 0) то
						writer.пБайты(сам.out^, 0, BufSize);
						ZlibBuffers.Rewrite(сам.s.out)
					всё;
					если сам.res = Потоки.Успех то
						ZlibDeflate.Deflate(сам.s, сам.flush);
						сам.res := сам.s.res
					всё
				кц;
				сам.crc32 := Zlib.CRC32(сам.crc32, buf, ofs, len - сам.s.in.avail);
			всё;
			res := сам.res;
			если propagate то
				утв(сам.s.in.avail = 0, 110);
				done := ложь;
				нц
					len := BufSize - сам.s.out.avail;
					если len # 0 то
						writer.пБайты(сам.out^, 0, len);
						ZlibBuffers.Rewrite(сам.s.out)
					всё;
					если done то прервиЦикл всё;
					ZlibDeflate.Deflate(сам.s, ZlibDeflate.Finish);
					если (len = 0) и (сам.s.res = ZlibDeflate.BufError) то
						сам.res := Потоки.Успех
					иначе
						сам.res := сам.s.res
					всё;
					done := (сам.s.out.avail # 0) или (сам.res = ZlibDeflate.StreamEnd);
					если (сам.res # ZlibDeflate.Ok) и (сам.res # ZlibDeflate.StreamEnd) то прервиЦикл всё
				кц;
				ZlibDeflate.Close(сам.s);
				сам.res := сам.s.res;
				writer.пЦел32_мз(crc32);
				writer.пЦел32_мз(inputsize(цел32));
				writer.ПротолкниБуферВПоток();
			всё;
		кон Send;

	кон Deflator;


	(** Reader for buffered reading of a file via Streams.Read* procedures.  See OpenReader. *)
	Inflator* = окласс	(** not sharable between multiple processes *)
		перем
			reader: Потоки.Чтец;
			res: целМЗ;
			transparent : булево;
			crc32-: цел32; (*crc32 of uncompressed data*)
			in : укль на массив BufSize из симв8;
			s: ZlibInflate.Stream;

		проц &Init*(reader: Потоки.Чтец);
		нач
			если reader = НУЛЬ то
				res := Zlib.StreamError; возврат;
			иначе
				сам.reader := reader;
				CheckHeader();
				если (res = Потоки.Успех) то
					ZlibInflate.Open(s, ложь);
					если s.res.code = ZlibInflate.Ok то
						нов(in); ZlibBuffers.Init(s.in, in^,0, BufSize,0);
						crc32 := Zlib.CRC32(9, in^, -1 , -1);
					всё;
				всё;
			всё;
		кон Init;


		проц Receive*(перем buf: массив из симв8; ofs, size, min: размерМЗ; перем len: размерМЗ; перем res: целМЗ);
		перем
			intlen : цел32;
		нач
			утв((0 <= ofs) и (0 <= len) и (ofs + size <= длинаМассива(buf)), 100);
			если transparent то
				reader.чБайты(buf, ofs, size, len);
				если len >= min то res := Потоки.Успех иначе res := Потоки.КонецФайла (* end of file *) всё;
			иначе
				если ~s.open то
					res := Zlib.StreamError; len := 0
				иначе
					ZlibBuffers.Init(s.out, buf, ofs, size, size);
					нцПока (s.out.avail # 0) и (s.res.code # Zlib.StreamEnd) делай
						если s.in.avail = 0 то
							reader.чБайты(in^, 0, BufSize, len);
							ZlibBuffers.Rewind(s.in, len);
							если s.in.avail = 0 то
								если reader.кодВозвратаПоследнейОперации < 0 то
									res := FileError
								всё
							всё
						всё;
						если res = Zlib.Ok то
							ZlibInflate.Inflate(s, ZlibInflate.NoFlush);
						всё
					кц;
					crc32 := Zlib.CRC32(crc32, buf, ofs, size - s.out.avail);
					len := size - s.out.avail;
				всё;

			всё;
			если len >= min то res := Потоки.Успех иначе res := Потоки.КонецФайла (* end of file *)всё;
		кон Receive;

		проц CheckHeader;
		конст
			headCRC = 2; extraField = 4; origName = 8; comment = 10H; reserved = 20H;
		перем
			ch, method, flags: симв8; len: цел16;
		нач
			ch := reader.чИДайСимв8();
			если reader.кодВозвратаПоследнейОперации = Потоки.КонецФайла то
				res := Потоки.КонецФайла;
			аесли ch # 1FX то
				transparent := истина; res := Потоки.Успех
			иначе	(* first byte of magic id ok *)
				ch := reader.чИДайСимв8();
				если (reader.кодВозвратаПоследнейОперации = Потоки.КонецФайла) или (ch # 8BX)то
					transparent := истина;  res := Потоки.Успех
				иначе	(* second byte of magic id ok *)
					method := reader.чИДайСимв8(); flags := reader.чИДайСимв8();
					если (reader.кодВозвратаПоследнейОперации = Потоки.КонецФайла) или (кодСимв8(method) # DeflateMethod) или (кодСимв8(flags) >= reserved) то
						res := Zlib.DataError
					иначе
						нцДля len := 1 до 6 делай ch := reader.чИДайСимв8(); кц;	(* skip time, xflags and OS code *)
						если нечётноеЛи¿(кодСимв8(flags) DIV extraField) то	(* skip extra field *)
							ch := reader.чИДайСимв8(); len := кодСимв8(ch);
							ch := reader.чИДайСимв8(); len := len + 100H*кодСимв8(ch);
							нцПока (reader.кодВозвратаПоследнейОперации = Потоки.КонецФайла) и (len # 0) делай
								ch := reader.чИДайСимв8(); умень(len)
							кц
						всё;
						если нечётноеЛи¿(кодСимв8(flags) DIV origName) то	(* skip original file name *)
							нцДо ch := reader.чИДайСимв8(); кцПри (reader.кодВозвратаПоследнейОперации = Потоки.КонецФайла) или (ch = 0X)
						всё;
						если нечётноеЛи¿(кодСимв8(flags) DIV comment) то	(* skip the .gz file comment *)
							нцДо ch := reader.чИДайСимв8(); кцПри (reader.кодВозвратаПоследнейОперации = Потоки.КонецФайла) или (ch = 0X)
						всё;
						если нечётноеЛи¿(кодСимв8(flags) DIV headCRC) то	(* skip header crc *)
							ch := reader.чИДайСимв8(); ch := reader.чИДайСимв8();
						всё;
						если (reader.кодВозвратаПоследнейОперации = Потоки.КонецФайла) то res := Zlib.DataError
						иначе res := Потоки.Успех
						всё
					всё
				всё
			всё
		кон CheckHeader;

	кон Inflator;


проц Deflate*(in,out :Files.File; level, strategy, flush: цел8);
перем
	d : Deflator;
	R: Files.Reader;
	W2 : Потоки.Писарь;
	W1 : Files.Writer;
	buf : массив  16384 из симв8;
	read : размерМЗ;
нач
	утв((in # НУЛЬ) и (out # НУЛЬ));
	Files.OpenReader(R, in, 0);

	Files.OpenWriter(W1,out,0);

	нов(d, W1 , level, strategy, flush);
	Потоки.НастройПисаря(W2, d.Send);

	R.чБайты(buf, 0, длинаМассива(buf), read);
	нцПока (read > 0) и (W2.кодВозвратаПоследнейОперации = Потоки.Успех) делай
		W2.пБайты(buf,0, read);
		R.чБайты(buf, 0, длинаМассива(buf), read);
	кц;
	W2.ПротолкниБуферВПоток();
кон Deflate;


проц Inflate*(in,out :Files.File);
перем
	d : Inflator;
	R1 : Files.Reader;
	R2 : Потоки.Чтец;
	W : Files.Writer;
	buf : массив  16384 из симв8;
	read : размерМЗ;
нач
	утв((in # НУЛЬ) и (out # НУЛЬ));
	Files.OpenReader(R1, in, 0);

	нов(d,R1);
	Потоки.НастройЧтеца(R2, d.Receive);

	Files.OpenWriter(W,out,0);
	R2.чБайты(buf, 0, длинаМассива(buf), read);
	нцПока (read > 0) и (R2.кодВозвратаПоследнейОперации = Потоки.Успех) делай
		W.пБайты(buf,0, read);
		R2.чБайты(buf, 0, длинаМассива(buf), read);
	кц;
	W.ПротолкниБуферВПоток();

кон Inflate;

проц GZip*(context:Commands.Context);
перем filename: Files.FileName; from,to: Files.File; compression, strategy: цел32;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename) то
		from:=Files.Old(filename);
		Строки8.ПодклейВСтрокуХвост(filename, ".gz");
		to:=Files.New(filename);
		если (from#НУЛЬ) и  (to#НУЛЬ) то
			если ~context.arg.ПропустиБелоеПолеИЧитайЦел32(compression,ложь) то
				compression:=DefaultCompression;
				strategy:=DefaultStrategy;
			аесли ~context.arg.ПропустиБелоеПолеИЧитайЦел32(strategy,ложь) то
				strategy:=DefaultStrategy;
			всё;
			Deflate(from,to,цел8(compression), цел8(strategy), FullFlush(*?*));
			Files.Register(to);
			context.out.пСтроку8("gzipped "); context.out.пСтроку8(filename);context.out.пВК_ПС; context.out.ПротолкниБуферВПоток;
		иначе
			context.out.пСтроку8("gzip failed for "); context.out.пСтроку8(filename);context.out.пВК_ПС; context.out.ПротолкниБуферВПоток;
		всё;
	иначе
		context.error.пСтроку8("file not found"); context.error.пВК_ПС; context.error.ПротолкниБуферВПоток;
	всё;
кон GZip;

проц UnGZip*(context:Commands.Context);
перем filename: Files.FileName; from,to: Files.File; pos: размерМЗ;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename) то
		pos:=Строки8.НайдиПодстроку(".gz", filename);
		если pos<0 то
			context.error.пСтроку8("no .gz file found"); context.error.пВК_ПС; context.error.ПротолкниБуферВПоток;
		иначе
			from:=Files.Old(filename);
			filename[pos]:=0X;
			to:=Files.New(filename);
			Inflate(from,to);
			Files.Register(to);
			context.out.пСтроку8("un-gzipped "); context.out.пСтроку8(filename);context.out.пВК_ПС; context.out.ПротолкниБуферВПоток;
		всё;
	иначе
		context.error.пСтроку8("no file to UnGZip"); context.error.пВК_ПС; context.error.ПротолкниБуферВПоток;
	всё;
кон UnGZip;


кон GZip.

GZip.GZip "../httproot/raphael-min.js" ~
GZip.UnGZip "../httproot/raphael-min2.js.gz" ~


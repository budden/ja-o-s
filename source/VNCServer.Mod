модуль VNCServer;	(** AUTHOR "TF"; PURPOSE "New VNC Server"; *)

использует
	НИЗКОУР, Потоки, TCP, IP, WMRectangles,
	ЛогЯдра, DES, Random, ЭВМ, Kernel, Inputs, Raster,
	Strings;

конст
	Version = "RFB 003.003";
	TraceVersion = 0;
	TraceAuthentication = 1;
	TraceMsg = 2;
	TraceKeyEvent = 3;
	Trace = {  } ;

	(* encodings *)
	EncRaw = 0; EncCopyRect = 1; EncRRE = 2; EncCoRRE= 4; EncHextile =5; EncZRLE = 16;

	(* Authentication constants *)
	AuthNone = 1; AuthVNC = 2; AuthOk = 0; AuthFailed = 1;

	(* hextile flags *)
	HexRaw = 1; HexBGSpecified = 2; HexFGSpecified = 4; HexAnySubrects = 8; HexSubrectsColoured = 16;

	MaxRect = 40;
	MaxWidth = 4096;
	MaxCutSize = 64 * 1024;
	BundleRectangles = истина;
	BigPackets = истина;
	SendFBUpdatePacketEarly = истина; (* the value of this is questionable *)

тип
	Rectangle = WMRectangles.Rectangle;
	RectBuf = укль на массив из Rectangle;
	WorkBuf = укль на массив из симв8;
	String = Strings.String;
	VNCMouseListener* = проц {делегат} (x, y, dz : цел32; keys : мнвоНаБитахМЗ);
	VNCKeyboardListener* = проц {делегат} (ucs : цел32; flags : мнвоНаБитахМЗ; keysym : цел32);
	VNCClipboardListener* = проц {делегат} (text : String);
	VNCNofClientsActiveListener* = проц {делегат} (nofClients : цел32);

	PFHextile = массив 16 * 16 из цел32;

	VNCInfo* = окласс
	перем
		name*, password* : массив 64 из симв8;
		img* : Raster.Image;
		ml* : VNCMouseListener;
		kl* : VNCKeyboardListener;
		cutl* : VNCClipboardListener;
		ncal* : VNCNofClientsActiveListener;
		width*, height* : размерМЗ;
		(** only used in service case *)
		connection* : TCP.Connection;
		agent* : VNCAgent; (** not valid in service init *)
	кон VNCInfo;

	(* the service must fill in the VNCInfo structure. img must not be NIL *)
	VNCService* = проц {делегат} (vncInfo : VNCInfo);

	Agent = окласс
	перем
		client: TCP.Connection;
		next: Agent; s: Server;
	кон Agent;

	PixelFormat = запись
		sr, sg, sb : цел32;
		bpp, depth, rmax, gmax, bmax, rshift, gshift, bshift : цел32;
		bigendian, truecolor, native16 : булево;
	кон;

	UpdateQ = окласс
	перем buffer : RectBuf;
		nofRect : цел32;
		clip : Rectangle;
		agent : VNCAgent;
		alive, allowed : булево;

		проц &Init*(agent : VNCAgent; w, h : размерМЗ);
		нач
			сам.agent := agent; alive := истина;
			нов(buffer, MaxRect);
			nofRect := 0; clip := WMRectangles.MakeRect(0, 0, w, h)
		кон Init;

		проц Add(перем r : Rectangle);
		перем i, a : размерМЗ; e : Rectangle; done : булево;
		нач {единолично}
			WMRectangles.ClipRect(r, clip);
			если WMRectangles.RectEmpty(r) то возврат всё;
			если nofRect = 0 то buffer[0] := r; nofRect := 1
			иначе
				a := WMRectangles.Area(r);
				i := 0; done := ложь;
				нцПока ~done и (i < nofRect) делай
					e := r; WMRectangles.ExtendRect(e, buffer[i]);
					если WMRectangles.Area(e) <= WMRectangles.Area(buffer[i]) + a то buffer[i] := e; done := истина всё;
					увел(i)
				кц;
				если ~done то
					если nofRect < MaxRect то buffer[nofRect] := r; увел(nofRect)
					иначе WMRectangles.ExtendRect(buffer[0], r);
					всё
				всё
			всё
		кон Add;

		проц GetBuffer(перем nof : цел32; drawBuf : RectBuf);
		перем r, e : Rectangle; i, j, a : размерМЗ; done : булево;
		нач {единолично}
			drawBuf[0] := buffer[0]; nof := 1;
			нцДля j := 1 до nofRect - 1 делай
				r := buffer[j]; a := WMRectangles.Area(r);
				i := 0; done := ложь;
				нцПока ~done и (i < nof) делай
					e := r; WMRectangles.ExtendRect(e, drawBuf[i]);
					если WMRectangles.Area(e) <= WMRectangles.Area(drawBuf[i]) + a то drawBuf[i] := e; done := истина всё;
					увел(i)
				кц;
				если ~done то
					утв(nof < MaxRect);
					drawBuf[nof] := r; увел(nof)
				всё
			кц;
			nofRect := 0
		кон GetBuffer;

		проц Close;
		нач {единолично}
			alive := ложь
		кон Close;

		проц SetAllowed;
		нач {единолично}
			allowed := истина
		кон SetAllowed;

	нач {активное}
		нц
			нач {единолично}
				дождись(~alive или allowed и (nofRect > 0));
				allowed := ложь
			кон;
			если ~alive то прервиЦикл всё;
			agent.DoUpdates
		кц
	кон UpdateQ;

	VNCAgent* = окласс(Agent)
	перем vncInfo : VNCInfo;
		in : Потоки.Чтец; out : Потоки.Писарь;
		pf : PixelFormat;
		traceStr : массив 64 из симв8;
		encodings : мнвоНаБитахМЗ; (* sencodings the client supports *)
		keyState : мнвоНаБитахМЗ; (* set of currently pressed grey keys *)
		updateQ : UpdateQ;
		drawRectBuffer : RectBuf; (* declared as field to avoid stack clearing *)
		workBuffer : WorkBuf;
		allowUpdate : булево;
		mode : Raster.Mode; (* chached mode to avoid bind *)
		pfHextile : PFHextile;

		проц &Init*(server : Server; client : TCP.Connection; vncInfo : VNCInfo);
		нач
			s := server; сам.client := client; сам.vncInfo := vncInfo;
			client.DelaySend(ложь);
			нов(in, client.ПрочтиИзПотока, 1024);
			нов(out, client.ЗапишиВПоток,  4096);
			(* defaults *)
			pf.bigendian := ложь; pf.truecolor := истина;
			pf.rmax := 31; pf.gmax := 63; pf.bmax := 31;
			pf.rshift := 11; pf.gshift := 5; pf.bshift := 0;
			pf.bpp := 16; pf.depth := 16;
			InitPixelFormat(pf);
			allowUpdate := ложь;
			Raster.InitMode(mode, Raster.srcCopy);
			нов(drawRectBuffer, MaxRect);
			нов(workBuffer, MaxWidth * 4);
			нов(updateQ, сам, vncInfo.width, vncInfo.height)
		кон Init;

		проц SendVersion() : булево;
		перем clientVersion : массив 12 из симв8; len : размерМЗ;
		нач
			out.пСтроку8(Version); out.пСимв8(0AX); out.ПротолкниБуферВПоток();
			если out.кодВозвратаПоследнейОперации # Потоки.Успех то возврат ложь всё;
			in.чБайты(clientVersion, 0, 12, len); clientVersion[11] := 0X;
			если TraceVersion в Trace то ЛогЯдра.пСтроку8("Client Version : "); ЛогЯдра.пСтроку8(clientVersion); ЛогЯдра.пВК_ПС всё;
			возврат (clientVersion = Version)
		кон SendVersion;

		проц Authenticate() : булево;
		перем challenge, response, clear : массив 16 из симв8;
			des : DES.DES; seq : Random.Sequence;
			i : цел32; len: размерМЗ; ok : булево;
		нач
			ЭВМ.атомарноУвел(NnofAuthenticate);
			если vncInfo.password = "" то
				ЭВМ.атомарноУвел(NnofAuthNone);
				out.пЦел32_сп(AuthNone); out.ПротолкниБуферВПоток; возврат out.кодВозвратаПоследнейОперации = Потоки.Успех
			иначе
				ЭВМ.атомарноУвел(NnofAuthVNC);
				out.пЦел32_сп(AuthVNC);
				(* initialize random number generator for challenge *)
				нов(seq); seq.InitSeed(Kernel.GetTicks());

				(* generate and send challenge *)
				нцДля i:=0 до 15 делай challenge[i] := симв8ИзКода(seq.Dice(256)); out.пСимв8(challenge[i]) кц; out.ПротолкниБуферВПоток();
				если out.кодВозвратаПоследнейОперации # Потоки.Успех то возврат ложь всё;

				нов(des); des.SetKey(vncInfo.password);
				in.чБайты(response, 0, 16, len);
				des.Decrypt(response, 0, clear, 0); des.Decrypt(response, 8, clear, 8);
				(* check decrypted response against challenge *)
				ok := истина; нцДля i := 0 до 15 делай если clear[i] # challenge[i] то ok := ложь всё кц;
				(* inform client *)
				если ~ok то
					если TraceAuthentication в Trace то ЛогЯдра.пСтроку8("Authentication error."); ЛогЯдра.пВК_ПС всё;
					ЭВМ.атомарноУвел(NnofAuthFailed);
					out.пЦел32_сп(AuthFailed)
				иначе
					если TraceAuthentication в Trace то ЛогЯдра.пСтроку8("Authenticated."); ЛогЯдра.пВК_ПС всё;
					ЭВМ.атомарноУвел(NnofAuthOk);
					out.пЦел32_сп(AuthOk)
				всё;
				out.ПротолкниБуферВПоток;
				возврат ok и (out.кодВозвратаПоследнейОперации = Потоки.Успех)
			всё
		кон Authenticate;

		проц CloseAllOtherClients;
		нач
			s.CloseAllOthers(сам)
		кон CloseAllOtherClients;

		проц Setup() : булево;
		перем len : цел32;
		нач
			(* read the client initialization 5.1.3 *)
			если in.чИДайСимв8() # 01X то CloseAllOtherClients всё; (* Service *)

			(* send server initialization 5.1.4 *)
			out.п2МладшихБайта_сп(vncInfo.width(цел32)); out.п2МладшихБайта_сп(vncInfo.height(цел32));

			(* pixelformat *)
			out.пСимв8(симв8ИзКода(pf.bpp)); out.пСимв8(симв8ИзКода(pf.depth));
			если pf.bigendian то out.пСимв8(1X) иначе out.пСимв8(0X) всё;
			если pf.truecolor то out.пСимв8(1X) иначе out.пСимв8(0X) всё;
			out.п2МладшихБайта_сп(pf.rmax); out.п2МладшихБайта_сп(pf.gmax); out.п2МладшихБайта_сп(pf.bmax);
			out.пСимв8(симв8ИзКода(pf.rshift)); out.пСимв8(симв8ИзКода(pf.gshift)); out.пСимв8(симв8ИзКода(pf.bshift));
			out.пСимв8(0X); out.пСимв8(0X); out.пСимв8(0X); (* padding *)

			(* name *)
			len := 0; нцПока vncInfo.name[len] # 0X делай увел(len) кц;
			out.пЦел32_сп(len); out.пСтроку8(vncInfo.name);
			out.ПротолкниБуферВПоток;

			возврат out.кодВозвратаПоследнейОперации = Потоки.Успех
		кон Setup;

		проц SetPixelFormat; (* 5.2.1 *)
		нач
			(* skip padding *)
			in.ПропустиБайты(3);
			(* Pixel format *)
			pf.bpp := кодСимв8(in.чИДайСимв8());
			pf.depth := кодСимв8(in.чИДайСимв8());
			pf.bigendian := in.чИДайСимв8() = 1X;
			pf.truecolor := in.чИДайСимв8() = 1X;
			pf.rmax := in.чЦел16_сп();
			pf.gmax := in.чЦел16_сп();
			pf.bmax := in.чЦел16_сп();
			pf.rshift := кодСимв8(in.чИДайСимв8());
			pf.gshift := кодСимв8(in.чИДайСимв8());
			pf.bshift := кодСимв8(in.чИДайСимв8());
			(* skip padding *)
			in.ПропустиБайты(3);
			InitPixelFormat(pf)
		кон SetPixelFormat;

		проц InitPixelFormat(перем pf : PixelFormat);
		перем t : цел32;
		нач
			t := pf.rmax; pf.sr := 0; нцПока t > 0 делай t := t DIV 2; увел(pf.sr) кц; pf.sr := 8 - pf.sr;
			t := pf.gmax; pf.sg := 0; нцПока t > 0 делай t := t DIV 2; увел(pf.sg) кц; pf.sg := 8 - pf.sg;
			t := pf.bmax; pf.sb := 0; нцПока t > 0 делай t := t DIV 2; увел(pf.sb) кц; pf.sb := 8 - pf.sb;
			pf.native16 := (pf.rmax = 31) и (pf.gmax = 63) и (pf.bmax = 31) и (pf.rshift = 11) и (pf.gshift = 5) и (pf.bshift = 0)
		кон InitPixelFormat;

(*		PROCEDURE TracePixelFormat(VAR pf : PixelFormat);
		BEGIN
			KernelLog.String("bpp: "); KernelLog.Int(pf.bpp, 4); KernelLog.Ln;
			KernelLog.String("depth: "); KernelLog.Int(pf.depth, 4); KernelLog.Ln;
			KernelLog.String("bigendian:");
			IF pf.bigendian THEN KernelLog.String("TRUE") ELSE KernelLog.String("FALSE") END; KernelLog.Ln;
			KernelLog.String("truecolor:");
			IF pf.truecolor THEN KernelLog.String("TRUE") ELSE KernelLog.String("FALSE") END; KernelLog.Ln;
			KernelLog.String("rmax: "); KernelLog.Int(pf.rmax, 4); KernelLog.Ln;
			KernelLog.String("gmax: "); KernelLog.Int(pf.gmax, 4); KernelLog.Ln;
			KernelLog.String("bmax: "); KernelLog.Int(pf.bmax, 4); KernelLog.Ln;
			KernelLog.String("rshift: "); KernelLog.Int(pf.rshift, 4); KernelLog.Ln;
			KernelLog.String("gshift: "); KernelLog.Int(pf.gshift, 4); KernelLog.Ln;
			KernelLog.String("bshift: "); KernelLog.Int(pf.bshift, 4); KernelLog.Ln;
		END TracePixelFormat;
*)
		(** is no longer specified in the 2002 revision of the protocol *)
		проц FixupColorMapEntries;
		перем nof, first  : цел32;
		нач
			ЛогЯдра.пСтроку8("FixupColorMapEntries no longer supported... "); ЛогЯдра.пВК_ПС;
			in.ПропустиБайты(1); first := in.чЦел16_сп(); nof := in.чЦел16_сп(); нцПока nof > 0 делай in.ПропустиБайты(6); умень(nof) кц
		кон FixupColorMapEntries;

		(* supported encodings 5.2.3 *)
		проц SetEncodings;
		перем nof, e : цел32;
		нач
			(* skip padding *)
			in.ПропустиБайты(1);
			encodings := {};
			nof := in.чЦел16_сп();
			нцПока (nof > 0) делай e := in.чЦел32_сп(); если (e>=0) и (e < 32) то включиВоМнвоНаБитах(encodings, e) всё; умень(nof) кц
		кон SetEncodings;

		проц SendRect(перем r : Rectangle);
		нач
			out.п2МладшихБайта_сп(r.l(цел32)); out.п2МладшихБайта_сп(r.t(цел32)); out.п2МладшихБайта_сп((r.r - r.l)(цел32)); out.п2МладшихБайта_сп((r.b - r.t)(цел32));
			если EncHextile в encodings то
				out.пЦел32_сп(5);
				SendHextile(out, vncInfo.img, mode, pf, workBuffer, pfHextile, r)
			иначе
				out.пЦел32_сп(0);
				SendRawRect(out, vncInfo.img, mode, pf, workBuffer, r)
			всё
		кон SendRect;

		проц DoUpdates;
		перем nof, i : цел32;
		нач {единолично} (* must be exclusive to avoid sending collisions *)
			updateQ.GetBuffer(nof, drawRectBuffer);
			если BundleRectangles то
				out.пСимв8(0X); (* message type *) out.пСимв8(0X); (* padding *)
				out.п2МладшихБайта_сп(nof); (* number of rectangles *)
				если SendFBUpdatePacketEarly то out.ПротолкниБуферВПоток всё;
				нцДля i := 0 до nof - 1 делай SendRect(drawRectBuffer[i]); если ~BigPackets то out.ПротолкниБуферВПоток всё кц;
				если BigPackets то out.ПротолкниБуферВПоток всё
			иначе
				нцДля i := 0 до nof - 1 делай
					out.пСимв8(0X); (* message type *) out.пСимв8(0X); (* padding *)
					out.п2МладшихБайта_сп(1); (* number of rectangles *)
					SendRect(drawRectBuffer[i]);
					если ~BigPackets то out.ПротолкниБуферВПоток всё
				кц;
				если BigPackets то out.ПротолкниБуферВПоток всё
			всё;
		кон DoUpdates;

		проц AddDirty*(r : Rectangle);
		нач
			updateQ.Add(r)
		кон AddDirty;

		(* 5.2.4 *)
		проц FBUpdateRequest;
		перем rect, r : Rectangle;
		нач
			если in.чИДайСимв8() # 1X то r := WMRectangles.MakeRect(0, 0, vncInfo.width, vncInfo.height); updateQ.Add(r) всё;
			rect.l := in.чЦел16_сп(); rect.t := in.чЦел16_сп();
			rect.r := rect.l + in.чЦел16_сп(); rect.b := rect.t + in.чЦел16_сп();
			(* ignoring the rect for now *)
			updateQ.SetAllowed
		кон FBUpdateRequest;

		(* 5.2.5 *)
		проц KeyEvent;
		перем down : булево;
			flags, greyKey : мнвоНаБитахМЗ;
			ucs, keysym : цел32;
		нач
			down := in.чИДайСимв8() = 1X;
			если down то flags := {} иначе flags := {Inputs.Release} всё;
			in.ПропустиБайты(2); (* skip padding *)
			keysym := in.чЦел32_сп();
			если down и (keysym < 80H) то ucs := keysym иначе ucs := 0 всё;

			(* flags *)
			greyKey := {};
			просей keysym из
				| Inputs.KsShiftL : greyKey := {Inputs.LeftShift}
				| Inputs.KsShiftR : greyKey := {Inputs.RightShift}
				| Inputs.KsControlL : greyKey := {Inputs.LeftCtrl}
				| Inputs.KsControlR : greyKey := {Inputs.RightCtrl}
				| Inputs.KsMetaL : greyKey := {Inputs.LeftMeta}
				| Inputs.KsMetaR : greyKey := {Inputs.RightMeta}
				| Inputs.KsAltL : greyKey := {Inputs.LeftAlt}
				| Inputs.KsAltR : greyKey := {Inputs.RightAlt}
			иначе
			всё;
			если down то
				просей keysym из
					| Inputs.KsBackSpace : ucs := 7FH (* backspace *)
					| Inputs.KsTab : ucs := 09H (* tab *)
					| Inputs.KsReturn : ucs := 0DH (* return/enter *)
					| Inputs.KsEscape : ucs := 01BH (* escape *)
					| Inputs.KsInsert : ucs := 0A0H (* insert *)
					| Inputs.KsDelete : ucs := 0A1H (* delete *)
					| Inputs.KsHome : ucs := 0A8H (* home *)
					| Inputs.KsEnd : ucs := 0A9H (* end *)
					| Inputs.KsPageUp : ucs := 0A2H (* pgup *)
					| Inputs.KsPageDown : ucs := 0A3H (* pgdn *)
					| Inputs.KsLeft : ucs := 0C4H (* left *)
					| Inputs.KsUp : ucs := 0C1H (* up *)
					| Inputs.KsRight : ucs := 0C3H (* right *)
					| Inputs.KsDown : ucs := 0C2H (* down *)
					| Inputs.KsF1: ucs := 0A4H (* f1 *)
					| Inputs.KsF2: ucs := 0A5H (* f2 *)
					| Inputs.KsF3: ucs := 01BH (* f3 *)
					| Inputs.KsF4: ucs := 0A7H (* f4 *)
					| Inputs.KsF5: ucs := 0F5H (* f5 *)
					| Inputs.KsF6: ucs := 0F6H (* f6 *)
					| Inputs.KsF7: ucs := 0F7H (* f7 *)
					| Inputs.KsF8: ucs := 0F8H (* f8 *)
					| Inputs.KsF9: ucs := 0F9H (* f9 *)
					| Inputs.KsF10: ucs := 0FAH (* f10 *)
					| Inputs.KsF11: ucs := 0FBH (* f11 *)
					| Inputs.KsF12: ucs := 0FCH (* f12 *)
				иначе
				всё
			всё;
			если down то keyState := keyState + greyKey иначе keyState := keyState - greyKey всё;
			если keyState * Inputs.Ctrl # {} то
				если (ucs > кодСимв8('A')) и (ucs < кодСимв8('Z')) то keysym := ucs - кодСимв8('A') + 1 всё; (* Ctrl-A - Ctrl-Z *)
				если (ucs > кодСимв8('a')) и (ucs < кодСимв8('z')) то keysym := ucs - кодСимв8('a') + 1 всё; (* Ctrl-a - Ctrl-z *)
				ucs := 0;
			всё;
			flags := flags + keyState;
			если TraceKeyEvent в Trace то ЛогЯдра.пСтроку8("Keysym = "); ЛогЯдра.п16ричное(keysym, -4); ЛогЯдра.пВК_ПС всё;
			если vncInfo.kl # НУЛЬ то vncInfo.kl(ucs, flags, keysym) всё
		кон KeyEvent;

		(* 5.2.6 *)
		проц PointerEvent;
		перем buttons : цел32; keys : мнвоНаБитахМЗ; x, y, dz : цел32;
		нач
			buttons := кодСимв8(in.чИДайСимв8()); x := in.чЦел16_сп(); y := in.чЦел16_сп();
			keys := {};
			если buttons остОтДеленияНа 2 = 1 то включиВоМнвоНаБитах(keys, 0) всё;
			если buttons DIV 2 остОтДеленияНа 2 = 1 то включиВоМнвоНаБитах(keys, 1) всё;
			если buttons DIV 4 остОтДеленияНа 2 = 1 то включиВоМнвоНаБитах(keys, 2) всё;
			если buttons DIV 8 остОтДеленияНа 2 = 1 то dz := -1 всё;
			если buttons DIV 16 остОтДеленияНа 2 = 1 то dz := +1 всё;
			если vncInfo.ml # НУЛЬ то vncInfo.ml(x, y, dz, keys) всё
		кон PointerEvent;

		(* 5.2.7 *)
		проц ClientCutText;
		перем i, len : цел32;
			text : String;
			skip : симв8;
		нач
			in.ПропустиБайты(3); (* padding *)
			len := in.чЦел32_сп();
			нов(text, матМинимум(MaxCutSize, len + 1));
			нцДля i := 0 до len - 1 делай
				если len < MaxCutSize - 1 то text[i] := in.чИДайСимв8() иначе skip := in.чИДайСимв8() всё;
			кц;
			text[матМинимум(MaxCutSize - 1, len)] := 0X;
			если vncInfo.cutl # НУЛЬ то vncInfo.cutl(text) всё;
		кон ClientCutText;

		(** send a text as clipboard content to the client *)
		проц SendClipboard*(text : String);
		перем len : цел32;
		нач {единолично}
			out.пСимв8(3X); (* message type *)
			out.пСимв8(0X); out.пСимв8(0X); out.пСимв8(0X); (* padding *)
			len := Strings.Length(text^)(цел32);
			out.пЦел32_сп(len);
			out.пСтроку8(text^);
		кон SendClipboard;


		(** 5.4.2 CopyRect Returns FALSE, if client does not support CopyRect
			this does not wait for fbupdatereq, does not flush *)
		проц CopyRect*(srcx, srcy : цел32; dst : Rectangle) : булево;
		нач {единолично}
			если ~(EncCopyRect в encodings) то возврат ложь всё;
			out.пСимв8(0X); (* message type *) out.пСимв8(0X); (* padding *)
			out.п2МладшихБайта_сп(1); (* number of rectangles *)
			out.п2МладшихБайта_сп(dst.l(цел32)); out.п2МладшихБайта_сп(dst.t(цел32)); out.п2МладшихБайта_сп((dst.r - dst.l)(цел32)); out.п2МладшихБайта_сп((dst.b - dst.t)(цел32));
			out.пЦел32_сп(1);
			out.п2МладшихБайта_сп(srcx); out.п2МладшихБайта_сп(srcy);
			возврат истина
		кон CopyRect;

		проц Serve;
		перем msgType : симв8;
		нач
			нцДо
				msgType := in.чИДайСимв8();
				если in.кодВозвратаПоследнейОперации = Потоки.Успех то
					просей msgType из
						0X : SetPixelFormat; если TraceMsg в Trace то traceStr := "SetPixelFormat" всё
						|1X: FixupColorMapEntries; если TraceMsg в Trace то traceStr := "FixupColorMapEntries" всё
						|2X: SetEncodings; если TraceMsg в Trace то traceStr := "Encoding" всё
						|3X: FBUpdateRequest; если TraceMsg в Trace то traceStr := "FBUpdate" всё
						|4X: KeyEvent; если TraceMsg в Trace то traceStr := "KeyEvent" всё
						|5X: PointerEvent; если TraceMsg в Trace то traceStr := "PointerEvent" всё
						|6X: ClientCutText; если TraceMsg в Trace то traceStr := "ClientCutText" всё
					иначе если TraceMsg в Trace то traceStr := "unknown" всё
					всё;
					если TraceMsg в Trace то ЛогЯдра.пСтроку8("VNC request: "); ЛогЯдра.пСтроку8(traceStr); ЛогЯдра.пВК_ПС всё
				всё
			кцПри in.кодВозвратаПоследнейОперации # Потоки.Успех
		кон Serve;

	нач {активное}
		если SendVersion() и Authenticate() и Setup() то
			ЭВМ.атомарноУвел(NnofEnteredServe);
			Serve;
			ЭВМ.атомарноУвел(NnofLeftServer);
		всё;
		client.Закрой;
		updateQ.Close;
		s.Remove(сам);
	кон VNCAgent;

	(** Wait for new TCP connections, start a VNC agent as soon as needed *)
	Server* = окласс
	перем res: целМЗ; service, client: TCP.Connection; root : Agent; agent : VNCAgent;
		vncInfo : VNCInfo;
		nofAgents : цел32;
		stopped : булево;
		init : VNCService;

		проц &Open*(port: цел32; vncInfo : VNCInfo; init : VNCService; перем res: целМЗ);
		нач
			stopped := ложь; сам.vncInfo := vncInfo; сам.init := init;
			нов(service); service.Open(port, IP.NilAdr, TCP.NilPort, res);
			если res = Потоки.Успех то нов(root); root.next := НУЛЬ
			иначе service := НУЛЬ (* stop active body *)
			всё;
			nofAgents := 0
		кон Open;

		проц CloseAllOthers(this : Agent);
		перем p : Agent;
		нач {единолично}
			p := root.next;
			нцПока p # НУЛЬ делай если p # this то p.client.Закрой() всё; p := p.next кц;
		кон CloseAllOthers;

		проц Remove(a: Agent);
		перем p: Agent;
		нач
			нач {единолично}
				p := root;
				нцПока (p.next # НУЛЬ) и (p.next # a) делай p := p.next кц;
				если p.next = a то p.next := a.next всё;
				умень(nofAgents)
			кон;
			если vncInfo.ncal # НУЛЬ то vncInfo.ncal(nofAgents) всё
		кон Remove;

		проц AddDirty*(r : Rectangle);
		перем p: Agent;
		нач {единолично}
			p := root.next;
			нцПока (p # НУЛЬ) делай p(VNCAgent).AddDirty(r); p := p.next кц;
		кон AddDirty;

		проц SendClipboard*(t : String);
		перем p: Agent;
		нач {единолично}
			p := root.next;
			нцПока (p # НУЛЬ) делай p(VNCAgent).SendClipboard(t); p := p.next кц;
		кон SendClipboard;

		проц Close*;
		перем p : Agent;
		нач {единолично}
			service.Закрой();
			p := root.next;
			нцПока p # НУЛЬ делай p.client.Закрой(); p := p.next кц;
			дождись(root.next = НУЛЬ);	(* wait for all agents to remove themselves *)
			дождись(stopped)	(* wait for service to terminate *)
		кон Close;

	нач {активное}
		если service # НУЛЬ то
			нц
				service.Accept(client, res);
				если res # Потоки.Успех то прервиЦикл всё;
				если init # НУЛЬ то
					нов(vncInfo);
					vncInfo.connection := client;
					init(vncInfo);
					vncInfo.width := vncInfo.img.width;
					vncInfo.height := vncInfo.img.height
				всё;
				нов(agent, сам, client, vncInfo);
				vncInfo.agent := agent;
				нач {единолично}
					увел(nofAgents);
					agent.next := root.next; root.next := agent;
				кон;
				если vncInfo.ncal # НУЛЬ то vncInfo.ncal(nofAgents) всё
			кц
		всё;
		нач {единолично} stopped := истина кон
	кон Server;

перем
	NnofAuthenticate-,
	NnofAuthNone-,
	NnofAuthVNC-, NnofAuthOk-, NnofAuthFailed-,
	NnofEnteredServe-, NnofLeftServer- : цел32;

проц SendPixel(out : Потоки.Писарь; pix : цел32; перем pf : PixelFormat);
нач
	если pf.depth = 8 то out.пСимв8(симв8ИзКода(pix))
	аесли pf.depth = 16 то
		если pf.bigendian то out.п2МладшихБайта_сп(pix) иначе out.пСимв8(симв8ИзКода(pix остОтДеленияНа 100H)); out.пСимв8(симв8ИзКода(pix DIV 100H)) всё
	иначе
		если pf.bigendian то out.пЦел32_сп(pix) иначе
			out.пСимв8(симв8ИзКода(pix остОтДеленияНа 100H)); out.пСимв8(симв8ИзКода(pix DIV 100H остОтДеленияНа 100H));
			out.пСимв8(симв8ИзКода(pix DIV 10000H остОтДеленияНа 100H)); out.пСимв8(симв8ИзКода(pix DIV 1000000H остОтДеленияНа 100H))
		всё
	всё
кон SendPixel;

проц SendRawRect(out : Потоки.Писарь; img : Raster.Image; перем mode : Raster.Mode; перем pf : PixelFormat; buf : WorkBuf; r : Rectangle);
перем i, j, rh, rw : размерМЗ; cb, cg, cr : цел32;
нач
	rh := r.b - r.t; rw := r.r - r.l;
(*	KernelLog.String("w/h"); KernelLog.Int(rw, 5); KernelLog.Int(rh, 5);
	KernelLog.String("rect :"); KernelLog.Int(r.l, 5); KernelLog.Int(r.t, 5);KernelLog.Int(r.r, 5); KernelLog.Int(r.b, 5); KernelLog.Ln; *)
	если pf.native16 то (* optimized 16 bit case *)
		нцДля i := 0 до rh - 1 делай
			Raster.GetPixels(img, r.l, r.t + i, rw, Raster.BGR565, buf^, 0, mode);
			out.пБайты(buf^, 0, 2*rw)
		кц
	иначе (* not so optimized generic case *)
		нцДля i := 0 до rh - 1 делай
			Raster.GetPixels(img, r.l, r.t + i, rw, Raster.BGR888, buf^, 0, mode);
			нцДля j := 0 до rw - 1 делай
				cb := логСдвиг(кодСимв8(buf[j * 3]), -pf.sb);
				cg := логСдвиг(кодСимв8(buf[j * 3 + 1]), -pf.sg);
				cr := логСдвиг(кодСимв8(buf[j * 3 + 2]), -pf.sr);
				SendPixel(out, логСдвиг(cg, pf.gshift) + логСдвиг(cb, pf.bshift) + логСдвиг(cr, pf.rshift), pf)
			кц
		кц
	всё
кон SendRawRect;

проц AnalyzeColors(перем hextile : PFHextile; nofPixels : размерМЗ; перем bg, fg : цел32; перем solid, mono : булево);
перем i : размерМЗ; n0, n1, c0, c1, c : цел32;
нач
	n0 := 0; n1 := 0; solid := истина; mono := истина;
	нцДля i := 0 до nofPixels - 1 делай
		c := hextile[i];
		если n0 = 0 то c0 := c всё;
		если c = c0 то увел(n0)
		иначе
			если n1 = 0 то c1 := c; solid := ложь всё;
			если c = c1 то увел(n1)
			иначе mono := ложь
			всё
		всё
	кц;
	если n0 > n1 то bg := c0; fg := c1 иначе bg := c1; fg := c0 всё
кон AnalyzeColors;

проц EncodeHextile(hextile : PFHextile; buf : WorkBuf; перем pf : PixelFormat; w, h : размерМЗ; bg, fg : цел32; mono : булево;
	перем nofRects : цел32) : размерМЗ;
перем pos, x, y, tx, ty, bypp, i, j : размерМЗ; c : цел32; eq : булево;
нач
	pos := 0; nofRects := 0;
	если pf.depth = 8 то bypp := 1
	аесли pf.depth = 16 то bypp := 2
	иначе bypp := 4
	всё;
	нцДля y := 0 до h - 1 делай
		нцДля x := 0 до w - 1 делай
			c := hextile[y * w + x];
			если c # bg то
				tx := x + 1;
				(* in x direction *)
				нцПока (tx < w) и (hextile[y * w + tx] = c) делай увел(tx) кц;
				ty := y + 1; eq := истина;
				нцПока (ty < h) и eq делай
					(* check a line *)
					j := x; нцПока (j < tx) и (hextile[ty * w + j] = c) делай увел(j) кц;
					если j < tx то eq := ложь иначе увел(ty) всё;
				кц;
				если ~mono то
					(* send the pixel (move into procedure ?) *)
					если pf.depth = 8 то buf[pos] := симв8ИзКода(c); увел(pos)
					аесли pf.depth = 16 то
						если pf.bigendian то buf[pos] := симв8ИзКода(c DIV 256); увел(pos); buf[pos] := симв8ИзКода(c остОтДеленияНа 256); увел(pos)
						иначе buf[pos] := симв8ИзКода(c остОтДеленияНа 256); увел(pos); buf[pos] := симв8ИзКода(c DIV 256); увел(pos)
						всё
					иначе
						если pf.bigendian то
							buf[pos] := симв8ИзКода(c DIV 1000000H остОтДеленияНа 100H); увел(pos);
							buf[pos] := симв8ИзКода(c DIV 10000H остОтДеленияНа 100H); увел(pos);
							buf[pos] := симв8ИзКода(c DIV 100H остОтДеленияНа 100H); увел(pos);
							buf[pos] := симв8ИзКода(c остОтДеленияНа 100H); увел(pos)
						иначе
							buf[pos] := симв8ИзКода(c остОтДеленияНа 100H); увел(pos);
							buf[pos] := симв8ИзКода(c DIV 100H остОтДеленияНа 100H); увел(pos);
							buf[pos] := симв8ИзКода(c DIV 10000H остОтДеленияНа 100H); увел(pos);
							buf[pos] := симв8ИзКода(c DIV 1000000H остОтДеленияНа 100H); увел(pos)
						всё
					всё
				всё;
				(* send rectangle coordinates *)
				buf[pos] := симв8ИзКода(x * 16 + y); увел(pos);
				(* w, h *)
				buf[pos] := симв8ИзКода((tx- x - 1) * 16 + (ty - y) - 1); увел(pos);
				увел(nofRects);
				(* clear the rectangle with bg col *)
				нцДля j := y до ty - 1 делай нцДля i := x до tx - 1 делай hextile[j * w + i] := bg кц кц;
				(* check if hextile is shorter than raw encoding *)
				если pos >= w * h * bypp то возврат 0 всё
			всё
		кц
	кц;
	возврат pos
кон EncodeHextile;

проц SendHextile(out : Потоки.Писарь; img : Raster.Image; перем mode : Raster.Mode; перем pf : PixelFormat;
	buf : WorkBuf; перем hextile : PFHextile; r : Rectangle);
перем x, y, w, h, ofs, i : размерМЗ; cb, cg, cr, bg, fg : цел32;
			validbg, validfg, mono, solid : булево;
			newbg, newfg, nofRects : цел32;
			flags, encBytes: размерМЗ;
			hextileFlags : мнвоНаБитахМЗ;
нач
	validbg := ложь;
	y := r.t;
	нцПока y < r.b делай
		x := r.l;
		h := 16; если r.b - y < 16 то h := r.b - y всё; (* current hextile height *)
		нцПока x < r.r делай
			w := 16; если r.r - x < 16 то w := r.r - x всё; (* current hextile width *)
			(* copy the hexile pixels into workbuffer *)
			ofs := 0; нцДля i := 0 до h - 1  делай Raster.GetPixels(img, x, y + i, w, Raster.BGR888, buf^, ofs, mode); увел(ofs, w * 3) кц;
			(* to pixelformat *)
			нцДля i := 0 до w * h - 1 делай
				cb := логСдвиг(кодСимв8(buf[i * 3]), -pf.sb);
				cg := логСдвиг(кодСимв8(buf[i * 3 + 1]), -pf.sg);
				cr := логСдвиг(кодСимв8(buf[i * 3 + 2]), -pf.sr);
				hextile[i] := логСдвиг(cg, pf.gshift) + логСдвиг(cb, pf.bshift) + логСдвиг(cr, pf.rshift);
			кц;
			AnalyzeColors(hextile, w * h, newbg, newfg, solid, mono);
			hextileFlags := {};
			если ~validbg или (newbg # bg) то validbg := истина; bg := newbg; включиВоМнвоНаБитах(hextileFlags, HexBGSpecified) всё;
			если ~solid то
				включиВоМнвоНаБитах(hextileFlags, HexAnySubrects);
				если mono то если ~validfg или (newfg # fg) то validfg := истина; fg := newfg; включиВоМнвоНаБитах(hextileFlags, HexFGSpecified) всё
				иначе validfg := ложь; включиВоМнвоНаБитах(hextileFlags, HexSubrectsColoured)
				всё;
				encBytes := EncodeHextile(hextile, buf, pf, w, h, bg, fg, mono, nofRects);
				если encBytes = 0 то (* hextile would need more bytes than raw *)
					validbg := ложь; validfg := ложь;
					hextileFlags := { HexRaw }
				всё
			всё;

			flags := 0; нцДля i := 0 до 31 делай если i в hextileFlags то увел(flags, i) всё кц;
			out.пСимв8(симв8ИзКода(flags));

			если HexBGSpecified в hextileFlags то SendPixel(out, bg, pf) всё;
			если HexFGSpecified в hextileFlags то SendPixel(out, fg, pf) всё;
			если HexRaw в hextileFlags то нцДля i := 0 до w * h - 1 делай SendPixel(out, hextile[i], pf); кц
			аесли HexAnySubrects в hextileFlags то
				out.пСимв8(симв8ИзКода(nofRects));
				out.пБайты(buf^, 0, encBytes)
			всё;
			увел(x, 16)
		кц;
		увел(y, 16)
	кц
кон SendHextile;

проц OpenServer*(port : цел32; img : Raster.Image; name, password : массив из симв8; ml : VNCMouseListener;
										kl : VNCKeyboardListener; cl : VNCClipboardListener; ncal : VNCNofClientsActiveListener) : Server;
перем server : Server;
	 vncInfo : VNCInfo; res : целМЗ;
нач
	нов(vncInfo);
	копируйСтрокуДо0(password, vncInfo.password);
	копируйСтрокуДо0(name, vncInfo.name);
	vncInfo.width := img.width;
	vncInfo.height := img.height;
	vncInfo.img := img;
	vncInfo.ml := ml; vncInfo.kl := kl; vncInfo.cutl := cl; vncInfo.ncal := ncal;
	нов(server, port, vncInfo, НУЛЬ, res);
	если res # 0 то server := НУЛЬ всё;
	возврат server;
кон OpenServer;

проц OpenService*(port : цел32; init : VNCService) : Server;
перем server : Server; res : целМЗ;
нач
	нов(server, port, НУЛЬ, init, res);
	если res # 0 то server := НУЛЬ всё;
	возврат server;
кон OpenService;

кон VNCServer.


модуль OpenTypeFonts; (** AUTHOR "eos, PL"; PURPOSE "Bluebottle port of OpenType"; *)

	(**
		Make OpenType fonts available to Oberon System
	**)

	(*
		18.11.1999 - portability fix for big_endian architectures in FillRect (pjm)
		10.12.1999 - assert valid memory access in FillRect (eos)
	*)

	использует
		НИЗКОУР, Строки8, OTInt := OpenTypeInt, OType := OpenType, OpenTypeScan, Files, ЛогЯдра, Commands;

	конст
		ScreenDPI = 71;
		FontId = 0DBX;
		FontFont = 0;
		FontMetric = 1;


	тип
		RasterData* = запись (OType.RasterData)
			adr*: адресВПамяти;							(* base address of pattern *)
			bpr*: цел32;									(* number of bytes per row *)
			len*: цел32;									(* pattern length *)
		кон;

		Char* = укль на CharDesc;
		CharDesc* = запись								(** The objects in a font library. *)	(* Note: offset used in GetCharObj *)
			dx*, x*, y*, w*, h*: цел16;					(** Character width, pattern offset (x, y), pattern size (w, h). *)
			pat*: цел32									(** Character raster data. *)
		кон;

	перем
		Pattern: массив 360*360 DIV 8 из симв8;				(* enough for 36 point at 720 dpi *)
		Glyph: OType.Glyph;
		Char2: Char;


	(* fill rectangle in pattern *)
	проц FillRect*(llx, lly, urx, ury, opacity: цел16; перем data: OType.RasterData0);
		перем x0, x1, h, n: цел16; adr, a: адресВПамяти; mask: мнвоНаБитахМЗ; byte: симв8;
	нач
		просейТип data: RasterData делай
			x0 := llx DIV 8; x1 := urx DIV 8;
			adr := data.adr + data.bpr * lly + x0;
			h := ury - lly;
			если x0 = x1 то
				mask := мнвоНаБитахМЗ({(llx остОтДеленияНа 8) .. ((urx-1) остОтДеленияНа 8)})
			иначе
				mask := мнвоНаБитахМЗ({(llx остОтДеленияНа 8) .. 7})
			всё;
			n := h; a := adr;
			нцПока n > 0 делай
				утв((data.adr <= a) и (a < data.adr + data.len), 110);
				НИЗКОУР.прочтиОбъектПоАдресу(a, byte);
				НИЗКОУР.запишиОбъектПоАдресу(a, симв8ИзКода(цел32(мнвоНаБитахМЗ(кодСимв8(byte)) + mask)));
				умень(n); увел(a, data.bpr)
			кц;
			если x0 < x1 то
				увел(x0); увел(adr);
				нцПока x0 < x1 делай
					n := h; a := adr;
					нцПока n > 0 делай
						утв((data.adr <= a) и (a < data.adr + data.len), 111);
						НИЗКОУР.запишиОбъектПоАдресу(a, 0FFX);
						умень(n); увел(a, data.bpr)
					кц;
					увел(x0); увел(adr)
				кц;
				если 8*x1 # urx то
					mask := мнвоНаБитахМЗ({0 .. (urx-1) остОтДеленияНа 8});
					n := h; a := adr;
					нцПока n > 0 делай
						утв((data.adr <= a) и (a < data.adr + data.len), 112);
						НИЗКОУР.прочтиОбъектПоАдресу(a, byte);
						НИЗКОУР.запишиОбъектПоАдресу(a, симв8ИзКода(цел32(мнвоНаБитахМЗ(кодСимв8(byte)) + mask)));
						умень(n); увел(a, data.bpr)
					кц
				всё
			всё
		всё
	кон FillRect;

	проц MakeFont (inst: OType.Instance; name: массив из симв8);
		конст
			mode = {OType.Hinted, OType.Width, OType.Raster};
		перем
			file: Files.File; r, m: Files.Rider; font: OType.Font; i, chars, ranges, xmin, ymin, xmax, ymax, j: цел16;
			beg, end: массив 64 из цел16; data: RasterData; no, bytes, k: цел32;
			ras: OpenTypeScan.Rasterizer;
	нач
		file := Files.New(name);
		утв(file # НУЛЬ);
		file.Set(r, 0);
		file.Write(r, FontId);																	(* Id *)
		file.Write(r, 0X);																		(* type (metric/font) *)
		file.Write(r, 0X);																		(* family *)
		file.Write(r, 0X);																		(* variant *)
		i := inst.font.hhea.ascender + inst.font.hhea.descender + inst.font.hhea.lineGap;
		Files.WriteInt(r, устарПреобразуйКБолееУзкомуЦел(OTInt.MulDiv(i, inst.yppm, 40H*устарПреобразуйКБолееШирокомуЦел(inst.font.head.unitsPerEm))));	(* height *)
		Files.WriteInt(r, 0); Files.WriteInt(r, 0); Files.WriteInt(r, 0); Files.WriteInt(r, 0);	(* fix later *) (* min/max X/Y *)

		font := inst.font;
		i := 0; chars := 0; ranges := 0;
		если OType.UnicodeToGlyph(font, OType.CharToUnicode[1]) = 0 то
			i := 2; chars := 1; beg[0] := 0; end[0] := 1; ranges := 1								(* make range for 0X *)
		всё;
		нцДо
			нцПока (i < 256) и (i # 9) и (OType.UnicodeToGlyph(font, OType.CharToUnicode[i]) = 0) делай увел(i) кц;
			если i < 256 то
				beg[ranges] := i; увел(i); увел(chars);
				нцПока (i < 256) и (OType.UnicodeToGlyph(font, OType.CharToUnicode[i]) # 0) делай увел(i); увел(chars) кц;
				end[ranges] := i; увел(ranges)
			всё
		кцПри i = 256;
		Files.WriteInt(r, ranges);															(* number of runs *)
		i := 0;
		нцПока i < ranges делай
			Files.WriteInt(r, beg[i]); Files.WriteInt(r, end[i]);								(* start/end of run *)
			увел(i)
		кц;

		file.Set(m, file.Pos(r));																(* open rider for later writing metrics *)
		i := 0;
		нцПока i < chars делай
			Files.WriteInt(r, 0); Files.WriteInt(r, 0); Files.WriteInt(r, 0); Files.WriteInt(r, 0); Files.WriteInt(r, 0);
			увел(i)
		кц;

		xmin := матМаксимум(цел16); ymin := матМаксимум(цел16); xmax := матМинимум(цел16); ymax := матМинимум(цел16);
		i := 0;
		нцПока i < ranges делай
			j := beg[i];
			нцПока j < end[i] делай
				no := OType.UnicodeToGlyph(font, OType.CharToUnicode[j]);
				если (j = 9) и (no = 0) то
					no := OType.UnicodeToGlyph(font, OType.CharToUnicode[кодСимв8("I")]);
					OType.LoadGlyph(inst, Glyph, ras, устарПреобразуйКБолееУзкомуЦел(no), {OType.Hinted, OType.Width});
					Glyph.awx := 8*Glyph.awx;
					Glyph.hbx := 0; Glyph.hby := 0; Glyph.rw := 0; Glyph.rh := 0
				иначе
					OType.LoadGlyph(inst, Glyph, ras, устарПреобразуйКБолееУзкомуЦел(no), mode)
				всё;
				Files.WriteInt(m, Glyph.awx);												(* advance *)
				Files.WriteInt(m, Glyph.hbx);												(* horizontal bearing x *)
				Files.WriteInt(m, Glyph.hby);												(* horizontal bearing y *)
				Files.WriteInt(m, Glyph.rw);												(* image width *)
				Files.WriteInt(m, Glyph.rh);													(* image height *)
				если Glyph.rw * Glyph.rh # 0 то
					если Glyph.hbx < xmin то xmin := Glyph.hbx всё;
					если Glyph.hby < ymin то ymin := Glyph.hby всё;
					если Glyph.hbx + Glyph.rw > xmax то xmax := Glyph.hbx + Glyph.rw всё;
					если Glyph.hby + Glyph.rh > ymax то ymax := Glyph.hby + Glyph.rh всё;
					data.rect := FillRect; data.adr := адресОт(Pattern); data.bpr := (Glyph.rw+7) DIV 8; data.len := длинаМассива(Pattern);
					bytes := Glyph.rh * data.bpr;
					утв(bytes < длинаМассива(Pattern));
					k := 0; нцДо Pattern[k] := 0X; увел(k) кцПри k = bytes;
					OType.EnumRaster(ras, data);
					k := 0; нцДо r.file.Write(r, Pattern[k]); увел(k) кцПри k = bytes			(* pattern *)
				всё;
				увел(j)
			кц;
			увел(i)
		кц;

		file.Set(r, 6);
		Files.WriteInt(r, xmin); Files.WriteInt(r, xmax);										(* minX/maxX *)
		Files.WriteInt(r, ymin); Files.WriteInt(r, ymax);										(* minY/maxY *)
		Files.Register(file)
	кон MakeFont;

	(**
		command for creating Oberon raster font files from an OpenType font file
		syntax:
			file - name of OpenType font file (e.g. "Arialb.TTF")
			font - Oberon name (e.g. "Arial")
			[style] - optional style character for Oberon name (e.g. "b")
			{size} - list of point sizes (e.g. "8 10 12 14 16 20 24")
			{dev} - list of device specifiers (e.g. "Scn Pr2 Pr3 Pr6")
	**)
	проц Make*(context : Commands.Context);
	перем
		temp : массив 256 из симв8; tempInt : цел32;
		font: OType.Font; name, fname, str: массив 32 из симв8; style: массив 3 из симв8; sizes, i: цел32;
		size: массив 16 из цел32; res: цел16; inst: OType.Instance;
	нач
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name);
		context.out.пСтроку8(name); context.out.пВК_ПС;

		font := OType.Open(name);
		если font # НУЛЬ то
			OType.InitGlyph(Glyph, font);

				context.arg.ПропустиБелоеПоле(); context.arg.чЦепочкуСимв8ДоБелогоПоля(name);
				context.arg.ПропустиБелоеПоле(); context.arg.чЦепочкуСимв8ДоБелогоПоля(temp);
				если ((Строки8.КвоБайтБезЗавершающего0(temp) = 1)
				   или (Строки8.КвоБайтБезЗавершающего0(temp) = 2)) и
				   ~IsNumber(temp) то
					копируйСтрокуДо0(temp, style);
					context.arg.ПропустиБелоеПоле(); context.arg.чЦепочкуСимв8ДоБелогоПоля(temp);
				иначе
					style[0] := 0X;
				всё;

				sizes := 0;
				нцПока IsNumber(temp) делай
					утв(sizes < длинаМассива(size));
					Строки8.ПрочтиЦел32_изСтроки(temp, tempInt);
					size[sizes] := tempInt; увел(sizes);
					context.arg.ПропустиБелоеПоле(); context.arg.чЦепочкуСимв8ДоБелогоПоля(temp);
				кц;

				если temp = "Scn" то res := ScreenDPI
				аесли temp = "Pr2" то res := 200
				аесли temp = "Pr3" то res := 300
				аесли temp = "Pr6" то res := 600
				иначе res := 0
				всё;
				если res # 0 то
					нцДля i := 0 до sizes-1 делай
						копируйСтрокуДо0(name, fname);
						Строки8.ПишиЦел64_вСтроку(size[i], str);
						Строки8.ПодклейВСтрокуХвост(fname, str);
						если style # "" то Строки8.ПодклейВСтрокуХвост(fname, style) всё;
						Строки8.ПодклейВСтрокуХвост(fname, "."); Строки8.ПодклейВСтрокуХвост(fname, temp); Строки8.ПодклейВСтрокуХвост(fname, ".Fnt");
						OType.GetInstance(font, 40H*size[i], res, res, OType.Identity, inst);
						ЛогЯдра.пСтроку8(fname); ЛогЯдра.пВК_ПС;
						MakeFont(inst, fname);
					кц
				всё;

		всё;
	кон Make;

	проц IsNumber(str : массив из симв8): булево;
	перем i : цел32;
	нач
		Строки8.ПрочтиЦел32_изСтроки(str, i);
		если i # 0 то возврат истина иначе возврат ложь всё
	кон IsNumber;

нач
	нов(Glyph);
	нов(Char2);
кон OpenTypeFonts.

----------------------------------------------------------

System.Free OpenTypeFonts OpenType~
OpenTypeFonts.Make schweif.ttf Schweif 12 14 16 18 Scn ~

OpenTypeFonts.Install~

(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль CubeCplx;   (** AUTHOR "fof"; PURPOSE "3D matrix object of type Real."; *)

использует НИЗКОУР, NbrInt, ArrayXdBytes, ArrayXd := ArrayXdCplx, NbrCplx,DataErrors, NbrRat, NbrRe,
	CubeInt, CubeRat, CubeRe, DataIO;

конст
	(** The version number used when reading/writing a cube to file. *)
	VERSION* = 1;

тип
	Value* = ArrayXd.Value;  Index* = цел32;  Array* = ArrayXd.Array;  IntValue = ArrayXd.IntValue;
	RatValue = NbrRat.Rational;  ReValue = NbrRe.Real;  ArrayC* = ArrayXd.Array3;  Map* = ArrayXd.Map;

	(** Type Cube is DataIO registered, instances of it can therefore be made persistent. *)

	Cube* = окласс (ArrayXd.Array)
	перем lenx-, leny-, lenz-: цел32;   (* lenx = nr.Columns, leny = nr.Rows *)
		ox-, oy-, oz-: цел32;
		Get-: проц {делегат} ( x, y, z: Index ): Value;

		(* override *)
		проц {перекрыта}AlikeX*( ): ArrayXdBytes.Array;
		перем copy: Cube;
		нач
			нов( copy, origin[0], len[0], origin[1], len[1], origin[2], len[2] );  возврат copy;
		кон AlikeX;

		проц {перекрыта}NewRangeX*( neworigin, newlen:ArrayXdBytes.IndexArray;  copydata: булево );
		нач
			если длинаМассива( newlen ) # 3 то СТОП( 1001 ) всё;
			NewRangeX^( neworigin, newlen, copydata );
		кон NewRangeX;

		проц {перекрыта}ValidateCache*;
		нач
			ValidateCache^;
			если dim # 3 то СТОП( 100 ) всё;
			lenx := len[0];  leny := len[1];  lenz := len[2];  ox := origin[0];  oy := origin[1];  oz := origin[2];
		кон ValidateCache;

		проц {перекрыта}SetBoundaryCondition*( c: цел8 );   (* called by new, load and directly *)
		нач
			SetBoundaryCondition^( c );
			просей c из
			ArrayXd.StrictBoundaryC:
					Get := Get3;
			| ArrayXd.AbsorbingBoundaryC:
					Get := Get3BAbsorbing;
			| ArrayXd.PeriodicBoundaryC:
					Get := Get3BPeriodic;
			| ArrayXd.SymmetricOnBoundaryC:
					Get := Get3BSymmetricOnB
			| ArrayXd.SymmetricOffBoundaryC:
					Get := Get3BSymmetricOffB
			| ArrayXd.AntisymmetricOnBoundaryC:
					Get := Get3BAntisymmetricOnB
			| ArrayXd.AntisymmetricOffBoundaryC:
					Get := Get3BAntisymmetricOffB
			всё;
		кон SetBoundaryCondition;

	(** new *)
		проц & New*( ox, w, oy, h, oz, d: цел32 );
		нач
			NewXdB( ArrayXdBytes.Array3( ox, oy, oz ), ArrayXdBytes.Array3( w, h, d ) );
		кон New;

		проц Alike*( ): Cube;
		перем copy: ArrayXdBytes.Array;
		нач
			copy := AlikeX();  возврат copy( Cube );
		кон Alike;

		проц NewRange*( ox, w, oy, h, oz, d: цел32;  copydata: булево );
		нач
			если (w # len[0]) или (h # len[1]) или (d # len[2]) или (ox # origin[0]) или (oy # origin[1]) или (oz # origin[2]) то
				NewRangeX^( ArrayXdBytes.Array3( ox, oy, oz ), ArrayXdBytes.Array3( w, h, d ), copydata )
			всё;
		кон NewRange;

		проц Copy*( ): Cube;
		перем res: ArrayXdBytes.Array;
		нач
			res := CopyX();  возврат res( Cube );
		кон Copy;

		проц Set*( x, y, z: Index;  v: Value );
		нач
			ArrayXdBytes.Set3( сам, x, y, z, v );
		кон Set;

	(** copy methods using the current boundary condition SELF.bc*)
		проц CopyToVec*( dest: Array;  dim: Index;  srcx, srcy, srcz, destx, len: Index );
		перем slen: ArrayXdBytes.IndexArray;
		нач
			если (dest.dim # 1) то СТОП( 1003 ) всё;
			slen := ArrayXdBytes.Index3( 1, 1, 1 );  slen[dim] := len;
			CopyToArray( dest, ArrayXdBytes.Index3( srcx, srcy, srcz ), slen, ArrayXdBytes.Index1( destx ),
								   ArrayXdBytes.Index1( len ) );
		кон CopyToVec;

		проц CopyToMtx*( dest: Array;  dimx, dimy: Index;  srcx, srcy, srcz, destx, desty, lenx, leny: Index );
		перем slen: ArrayXdBytes.IndexArray;
		нач
			если (dest.dim # 2) или (dimx >= dimy) то СТОП( 1005 ) всё;
			slen := ArrayXdBytes.Index3( 1, 1, 1 );  slen[dimx] := lenx;  slen[dimy] := leny;
			CopyToArray( dest, ArrayXdBytes.Index3( srcx, srcy, srcz ), slen, ArrayXdBytes.Index2( destx, desty ),
								   ArrayXdBytes.Index2( lenx, leny ) );
		кон CopyToMtx;

		проц CopyToCube*( dest: Array;  srcx, srcy, srcz, destx, desty, destz, lenx, leny, lenz: Index );
		перем slen: ArrayXdBytes.IndexArray;
		нач
			если (dest.dim # 3) то СТОП( 1005 ) всё;
			slen := ArrayXdBytes.Index3( lenx, leny, lenz );
			CopyToArray( dest, ArrayXdBytes.Index3( srcx, srcy, srcz ), slen, ArrayXdBytes.Index3( destx, desty, destz ), slen );
		кон CopyToCube;

		проц CopyToHCube*( dest: Array;  dimx, dimy, dimz: Index;
													  srcx, srcy, srcz, destx, desty, destz, destt, lenx, leny, lenz: Index );
		перем slen: ArrayXdBytes.IndexArray;
		нач
			если (dest.dim # 4) или (dimx >= dimy) или (dimy >= dimz) то СТОП( 1005 ) всё;
			slen := ArrayXdBytes.Index4( 1, 1, 1, 1 );  slen[dimx] := lenx;  slen[dimy] := leny;  slen[dimz] := lenz;
			CopyToArray( dest, ArrayXdBytes.Index3( srcx, srcy, srcz ), ArrayXdBytes.Index3( lenx, leny, lenz ),
								   ArrayXdBytes.Index4( destx, desty, destz, destt ), slen );
		кон CopyToHCube;

		проц CopyTo1dArray*( перем dest: массив из Value;  sx, sy, sz, slenx, sleny, slenz: Index;  dpos, dlen: цел32 );
		перем destm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			destm :=
				ArrayXdBytes.MakeMemoryStructure( 1, ArrayXdBytes.Index1( 0 ), ArrayXdBytes.Index1( длинаМассива( dest ) ), размер16_от( Value ),
																			  адресОт( dest[0] ) );
			ArrayXd.CopyArrayToArrayPartB( сам, destm, bc, ArrayXdBytes.Index3( sx, sy, sz ),
																  ArrayXdBytes.Index3( slenx, sleny, slenz ), ArrayXdBytes.Index1( dpos ),
																  ArrayXdBytes.Index1( dlen ) );
		кон CopyTo1dArray;

		проц CopyTo2dArray*( перем dest: массив из массив из Value;  sx, sy, sz, slenx, sleny, slenz: Index;
													   dposx, dposy, dlenx, dleny: цел32 );
		перем destm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			destm :=
				ArrayXdBytes.MakeMemoryStructure( 2, ArrayXdBytes.Index2( 0, 0 ), ArrayXdBytes.Index2( длинаМассива( dest, 1 ), длинаМассива( dest, 0 ) ),
																			  размер16_от( Value ), адресОт( dest[0, 0] ) );
			ArrayXd.CopyArrayToArrayPartB( сам, destm, bc, ArrayXdBytes.Index3( sx, sy, sz ),
																  ArrayXdBytes.Index3( slenx, sleny, slenz ), ArrayXdBytes.Index2( dposx, dposy ),
																  ArrayXdBytes.Index2( dlenx, dleny ) );
		кон CopyTo2dArray;

		проц CopyTo3dArray*( перем dest: массив из массив из массив из Value;  sx, sy, sz, slenx, sleny, slenz: Index;
													   dposx, dposy, dposz, dlenx, dleny, dlenz: цел32 );
		перем destm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			destm :=
				ArrayXdBytes.MakeMemoryStructure( 3, ArrayXdBytes.Index3( 0, 0, 0 ),
																			  ArrayXdBytes.Index3( длинаМассива( dest, 2 ), длинаМассива( dest, 1 ), длинаМассива( dest, 0 ) ), размер16_от( Value ),
																			  адресОт( dest[0, 0, 0] ) );
			ArrayXd.CopyArrayToArrayPartB( сам, destm, bc, ArrayXdBytes.Index3( sx, sy, sz ),
																  ArrayXdBytes.Index3( slenx, sleny, slenz ),
																  ArrayXdBytes.Index3( dposx, dposy, dposz ),
																  ArrayXdBytes.Index3( dlenx, dleny, dlenz ) );
		кон CopyTo3dArray;

		проц CopyTo4dArray*( перем dest: массив из массив из массив из массив из Value;  sx, sy, sz, slenx, sleny, slenz: Index;
													   dposx, dposy, dposz, dpost, dlenx, dleny, dlenz, dlent: цел32 );
		перем destm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			destm :=
				ArrayXdBytes.MakeMemoryStructure( 4, ArrayXdBytes.Index4( 0, 0, 0, 0 ),
																			  ArrayXdBytes.Index4( длинаМассива( dest, 3 ), длинаМассива( dest, 2 ), длинаМассива( dest, 1 ), длинаМассива( dest, 0 ) ), размер16_от( Value ),
																			  адресОт( dest[0, 0, 0, 0] ) );
			ArrayXd.CopyArrayToArrayPartB( сам, destm, bc, ArrayXdBytes.Index3( sx, sy, sz ),
																  ArrayXdBytes.Index3( slenx, sleny, slenz ),
																  ArrayXdBytes.Index4( dposx, dposy, dposz, dpost ),
																  ArrayXdBytes.Index4( dlenx, dleny, dlenz, dlent ) );
		кон CopyTo4dArray;

	(** copy from without boundary conditions *)
		проц CopyFrom1dArray*( перем src: массив из Value;  spos, slen: Index;  dx, dy, dz, dlenx, dleny, dlenz: Index );
		перем srcm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			srcm :=
				ArrayXdBytes.MakeMemoryStructure( 1, ArrayXdBytes.Index1( 0 ), ArrayXdBytes.Index1( длинаМассива( src ) ), размер16_от( Value ),
																			  адресОт( src[0] ) );
			ArrayXdBytes.CopyArrayPartToArrayPart( srcm, сам, ArrayXdBytes.Index1( spos ), ArrayXdBytes.Index1( slen ),
																			   ArrayXdBytes.Index3( dx, dy, dz ),
																			   ArrayXdBytes.Index3( dlenx, dleny, dlenz ) );
		кон CopyFrom1dArray;

		проц CopyFrom2dArray*( перем src: массив из массив из Value;  sposx, spoxy, slenx, sleny: Index;
														    dx, dy, dz, dlenx, dleny, dlenz: Index );
		перем srcm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			srcm :=
				ArrayXdBytes.MakeMemoryStructure( 2, ArrayXdBytes.Index2( 0, 0 ), ArrayXdBytes.Index2( длинаМассива( src, 1 ), длинаМассива( src, 0 ) ),
																			  размер16_от( Value ), адресОт( src[0, 0] ) );
			ArrayXdBytes.CopyArrayPartToArrayPart( srcm, сам, ArrayXdBytes.Index2( sposx, spoxy ),
																			   ArrayXdBytes.Index2( slenx, sleny ), ArrayXdBytes.Index3( dx, dy, dz ),
																			   ArrayXdBytes.Index3( dlenx, dleny, dlenz ) );
		кон CopyFrom2dArray;

		проц CopyFrom3dArray*( перем src: массив из массив из массив из Value;  sposx, spoxy, sposz, slenx, sleny, slenz: Index;
														    dx, dy, dz, dlenx, dleny, dlenz: Index );
		перем srcm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			srcm :=
				ArrayXdBytes.MakeMemoryStructure( 3, ArrayXdBytes.Index3( 0, 0, 0 ),
																			  ArrayXdBytes.Index3( длинаМассива( src, 2 ), длинаМассива( src, 1 ), длинаМассива( src, 0 ) ), размер16_от( Value ),
																			  адресОт( src[0, 0, 0] ) );
			ArrayXdBytes.CopyArrayPartToArrayPart( srcm, сам, ArrayXdBytes.Index3( sposx, spoxy, sposz ),
																			   ArrayXdBytes.Index3( slenx, sleny, slenz ),
																			   ArrayXdBytes.Index3( dx, dy, dz ),
																			   ArrayXdBytes.Index3( dlenx, dleny, dlenz ) );
		кон CopyFrom3dArray;

		проц CopyFrom4dArray*( перем src: массив из массив из массив из массив из Value;
														    sposx, spoxy, sposz, spost, slenx, sleny, slenz, slent: Index;
														    dx, dy, dz, dlenx, dleny, dlenz: Index );
		перем srcm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			srcm :=
				ArrayXdBytes.MakeMemoryStructure( 4, ArrayXdBytes.Index4( 0, 0, 0, 0 ),
																			  ArrayXdBytes.Index4( длинаМассива( src, 3 ), длинаМассива( src, 2 ), длинаМассива( src, 1 ), длинаМассива( src, 0 ) ), размер16_от( Value ),
																			  адресОт( src[0, 0, 0, 0] ) );
			ArrayXdBytes.CopyArrayPartToArrayPart( srcm, сам, ArrayXdBytes.Index4( sposx, spoxy, sposz, spost ),
																			   ArrayXdBytes.Index4( slenx, sleny, slenz, slent ),
																			   ArrayXdBytes.Index3( dx, dy, dz ),
																			   ArrayXdBytes.Index3( dlenx, dleny, dlenz ) );
		кон CopyFrom4dArray;

	кон Cube;

	операция ":="*( перем l: Cube;  перем r: массив из массив из массив из Value );
	нач
		(*	IF r = NIL THEN l := NIL;  RETURN END;  *)
		если l = НУЛЬ то нов( l, 0, длинаМассива( r, 2 ), 0, длинаМассива( r, 1 ), 0, длинаМассива( r, 0 ) );  иначе l.NewRange( 0, длинаМассива( r, 2 ), 0, длинаМассива( r, 1 ), 0, длинаМассива( r, 0 ), ложь );  всё;
		ArrayXdBytes.CopyMemoryToArrayPart( адресОт( r[0, 0, 0] ), l, длинаМассива( r, 0 ) * длинаМассива( r, 1 ) * длинаМассива( r, 2 ), НУЛЬ , НУЛЬ );
	кон ":=";

	операция ":="*( перем l: Cube;  r: CubeInt.Cube );
	перем i, last: цел32;
	нач
		если r = НУЛЬ то l := НУЛЬ иначе
			если l = НУЛЬ то нов( l, r.origin[0], r.len[0], r.origin[1], r.len[1], r.origin[2], r.len[2] );  всё;
			last := длинаМассива( r.data ) - 1;
			нцДля i := 0 до last делай l.data[i] := r.data[i];  кц;
		всё;
	кон ":=";

	операция ":="*( перем l: Cube;  r: CubeRat.Cube );
	перем i, last: цел32;
	нач
		если r = НУЛЬ то l := НУЛЬ иначе
			если l = НУЛЬ то нов( l, r.origin[0], r.len[0], r.origin[1], r.len[1], r.origin[2], r.len[2] );  всё;
			last := длинаМассива( r.data ) - 1;
			нцДля i := 0 до last делай l.data[i] := r.data[i];  кц;
		всё;
	кон ":=";

	операция ":="*( перем l: Cube;  r: CubeRe.Cube );
	перем i, last: цел32;
	нач
		если r = НУЛЬ то l := НУЛЬ иначе
			если l = НУЛЬ то нов( l, r.origin[0], r.len[0], r.origin[1], r.len[1], r.origin[2], r.len[2] );  всё;
			last := длинаМассива( r.data ) - 1;
			нцДля i := 0 до last делай l.data[i] := r.data[i];  кц;
		всё;
	кон ":=";

	операция ":="*( перем l: Cube;  r: Value );
	нач
		если l # НУЛЬ то ArrayXd.Fill( l, r );  всё;
	кон ":=";

	операция ":="*( перем l: Cube;  r: ReValue );
	перем r1: Value;
	нач
		r1 := r;  l := r1;
	кон ":=";

	операция ":="*( перем l: Cube;  r: RatValue );
	перем r1: Value;
	нач
		r1 := r;  l := r1;
	кон ":=";

	операция ":="*( перем l: Cube;  r: IntValue );
	перем r1: Value;
	нач
		r1 := r;  l := r1;
	кон ":=";

	операция "+"*( l, r: Cube ): Cube;
	перем res: Cube;
	нач
		res := l.Alike();  ArrayXd.Add( l, r, res );  возврат res;
	кон "+";

	операция "-"*( l, r: Cube ): Cube;
	перем res: Cube;
	нач
		res := l.Alike();  ArrayXd.Sub( l, r, res );  возврат res;
	кон "-";

	операция "+"*( l: Cube;  r: Value ): Cube;
	перем res: Cube;
	нач
		res := l.Alike();  ArrayXd.AddAV( l, r, res );  возврат res;
	кон "+";

	операция "+"*( l: Cube;  r: IntValue ): Cube;
	перем res: Cube;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.AddAV( l, r1, res );  возврат res;
	кон "+";

	операция "+"*( l: Cube;  r: RatValue ): Cube;
	перем res: Cube;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.AddAV( l, r1, res );  возврат res;
	кон "+";

	операция "+"*( l: Cube;  r: ReValue ): Cube;
	перем res: Cube;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.AddAV( l, r1, res );  возврат res;
	кон "+";

	операция "+"*( l: Value;  r: Cube ): Cube;
	нач
		возврат r + l
	кон "+";

	операция "+"*( l: ReValue;  r: Cube ): Cube;
	нач
		возврат r + l
	кон "+";

	операция "+"*( l: RatValue;  r: Cube ): Cube;
	нач
		возврат r + l
	кон "+";

	операция "+"*( l: IntValue;  r: Cube ): Cube;
	нач
		возврат r + l
	кон "+";

	операция "-"*( l: Cube;  r: Value ): Cube;
	перем res: Cube;
	нач
		res := l.Alike();  ArrayXd.SubAV( l, r, res );  возврат res;
	кон "-";

	операция "-"*( l: Cube;  r: ReValue ): Cube;
	перем res: Cube;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.SubAV( l, r1, res );  возврат res;
	кон "-";

	операция "-"*( l: Cube;  r: RatValue ): Cube;
	перем res: Cube;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.SubAV( l, r1, res );  возврат res;
	кон "-";

	операция "-"*( l: Cube;  r: IntValue ): Cube;
	перем res: Cube;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.SubAV( l, r1, res );  возврат res;
	кон "-";

	операция "-"*( l: Value;  r: Cube ): Cube;
	перем res: Cube;
	нач
		res := r.Alike();  ArrayXd.SubVA( l, r, res );  возврат res;
	кон "-";

	операция "-"*( l: ReValue;  r: Cube ): Cube;
	перем res: Cube;  l1: Value;
	нач
		res := r.Alike();  l1 := l;  ArrayXd.SubVA( l1, r, res );  возврат res;
	кон "-";

	операция "-"*( l: RatValue;  r: Cube ): Cube;
	перем res: Cube;  l1: Value;
	нач
		res := r.Alike();  l1 := l;  ArrayXd.SubVA( l1, r, res );  возврат res;
	кон "-";

	операция "-"*( l: IntValue;  r: Cube ): Cube;
	перем res: Cube;  l1: Value;
	нач
		res := r.Alike();  l1 := l;  ArrayXd.SubVA( l1, r, res );  возврат res;
	кон "-";

	операция "-"*( l: Cube ): Cube;
	нач
		возврат 0 - l;
	кон "-";

	операция "*"*( l: Cube;  r: Value ): Cube;
	перем res: Cube;
	нач
		res := l.Alike();  ArrayXd.MulAV( l, r, res );  возврат res;
	кон "*";

	операция "*"*( l: Cube;  r: ReValue ): Cube;
	перем res: Cube;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.MulAV( l, r1, res );  возврат res;
	кон "*";

	операция "*"*( l: Cube;  r: RatValue ): Cube;
	перем res: Cube;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.MulAV( l, r1, res );  возврат res;
	кон "*";

	операция "*"*( l: Cube;  r: IntValue ): Cube;
	перем res: Cube;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.MulAV( l, r1, res );  возврат res;
	кон "*";

	операция "*"*( l: Value;  r: Cube ): Cube;
	нач
		возврат r * l;
	кон "*";

	операция "*"*( l: ReValue;  r: Cube ): Cube;
	нач
		возврат r * l;
	кон "*";

	операция "*"*( l: RatValue;  r: Cube ): Cube;
	нач
		возврат r * l;
	кон "*";

	операция "*"*( l: IntValue;  r: Cube ): Cube;
	нач
		возврат r * l;
	кон "*";

	операция "/"*( l: Cube;  r: Value ): Cube;
	перем res: Cube;
	нач
		res := l.Alike();  ArrayXd.DivAV( l, r, res );  возврат res;
	кон "/";

	операция "/"*( l: Cube;  r: ReValue ): Cube;
	перем res: Cube;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.DivAV( l, r1, res );  возврат res;
	кон "/";

	операция "/"*( l: Cube;  r: RatValue ): Cube;
	перем res: Cube;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.DivAV( l, r1, res );  возврат res;
	кон "/";

	операция "/"*( l: Cube;  r: IntValue ): Cube;
	перем res: Cube;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.DivAV( l, r1, res );  возврат res;
	кон "/";

	операция "/"*( l: Value;  r: Cube ): Cube;
	перем res: Cube;
	нач
		res := r.Alike();  ArrayXd.DivVA( l, r, res );  возврат res;
	кон "/";

	операция "/"*( l: ReValue;  r: Cube ): Cube;
	перем res: Cube;  l1: Value;
	нач
		res := r.Alike();  l1 := l;  ArrayXd.DivVA( l1, r, res );  возврат res;
	кон "/";

	операция "/"*( l: RatValue;  r: Cube ): Cube;
	перем res: Cube;  l1: Value;
	нач
		res := r.Alike();  l1 := l;  ArrayXd.DivVA( l1, r, res );  возврат res;
	кон "/";

	операция "/"*( l: IntValue;  r: Cube ): Cube;
	перем res: Cube;  l1: Value;
	нач
		res := r.Alike();  l1 := l;  ArrayXd.DivVA( l1, r, res );  возврат res;
	кон "/";

(* The procedures needed to register type Cube so that its instances can be made persistent. *)
	проц LoadCube( R: DataIO.Reader;  перем obj: окласс );
	перем a: Cube;  version: цел8;  ver: NbrInt.Integer;
	нач
		R.чЦел8_мз( version );
		если version = -1 то
			obj := НУЛЬ  (* Version tag is -1 for NIL. *)
		аесли version = VERSION то нов( a, 0, 0, 0, 0, 0, 0 );  a.Read( R );  obj := a
		иначе ver := version;  DataErrors.IntError( ver, "Alien version number encountered." );  СТОП( 1000 )
		всё
	кон LoadCube;

	проц StoreCube( W: DataIO.Writer;  obj: окласс );
	перем a: Cube;
	нач
		если obj = НУЛЬ то W.пЦел8_мз( -1 ) иначе W.пЦел8_мз( VERSION );  a := obj( Cube );  a.Write( W ) всё
	кон StoreCube;

	проц Register;
	перем a: Cube;
	нач
		нов( a, 0, 0, 0, 0, 0, 0 );  DataIO.PlugIn( a, LoadCube, StoreCube )
	кон Register;

(** Load and Store are procedures for external use that read/write an instance of Cube from/to a file. *)
	проц Load*( R: DataIO.Reader;  перем obj: Cube );
	перем ptr: окласс;
	нач
		R.Object( ptr );  obj := ptr( Cube )
	кон Load;

	проц Store*( W: DataIO.Writer;  obj: Cube );
	нач
		W.Object( obj )
	кон Store;

нач
	Register
кон CubeCplx.

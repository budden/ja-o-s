(* ETH Oberon, Copyright 2001 ETH Zuerich Institut fuer Computersysteme, ETH Zentrum, CH-8092 Zuerich.
Refer to the "General ETH Oberon System Source License" contract available at: http://www.oberon.ethz.ch/ *)

модуль PictImages; (** non-portable *)	(* eos  **)
(** AUTHOR "eos"; PURPOSE "Pict image format"; *)

	(**
		Support for images in Oberon Picture format
	**)

	(*
		9.12.1998 - first release (together with GfxMaps)
		23.8.1999 - migrated from GfxPictures
		17.11.1999 - replaced F8 format by D8
		17.11.1999 - always use white/black for pictures with depth=1
		18.11.1999 - bugfix in run_length encoding (buf[0]+1 overflowed for buf[0]=127)
		19.11.1999 - don't dither when storing as picture
		19.06.2000 - Aos version
	*)

	использует
		НИЗКОУР, Files, Raster;


	конст
		R = Raster.r; G = Raster.g; B = Raster.b; A = Raster.a;


(*
	VAR
		PrntPat*: ARRAY 9 OF Raster.Image;	(** printer patterns (same as in Printer3) **)
*)

	(**--- Oberon Pictures ---**)

	(* pack/unpack procedures for 1 and 4 bit formats *)

	проц PackP1 (перем fmt: Raster.Format0; adr: адресВПамяти; bit: размерМЗ; перем pix: Raster.Pixel);
		перем b: симв8;
	нач
		НИЗКОУР.прочтиОбъектПоАдресу(adr, b);
		если кодСимв8(pix[R]) + кодСимв8(pix[G]) + кодСимв8(pix[B]) >= 3*128 то
			если ~нечётноеЛи¿(арифмСдвиг(кодСимв8(b), -bit)) то
				НИЗКОУР.запишиОбъектПоАдресу(adr, симв8ИзКода(кодСимв8(b) + арифмСдвиг(1, bit)))
			всё
		иначе
			если нечётноеЛи¿(арифмСдвиг(кодСимв8(b), -bit)) то
				НИЗКОУР.запишиОбъектПоАдресу(adr, симв8ИзКода(кодСимв8(b) - арифмСдвиг(1, bit)))
			всё
		всё
	кон PackP1;

	проц UnpackP1 (перем fmt: Raster.Format0; adr: адресВПамяти; bit: размерМЗ; перем pix: Raster.Pixel);
		перем b: симв8;
	нач
		НИЗКОУР.прочтиОбъектПоАдресу(adr, b);
		если нечётноеЛи¿(арифмСдвиг(кодСимв8(b), -bit)) то pix[R] := 0X; pix[G] := 0X; pix[B] := 0X; pix[A] := 0FFX
		иначе pix[R] := 0FFX; pix[G] := 0FFX; pix[B] := 0FFX; pix[A] := 0FFX
		всё
	кон UnpackP1;

	проц PackP4 (перем fmt: Raster.Format0; adr: адресВПамяти; bit: размерМЗ; перем pix: Raster.Pixel);
		перем b: симв8; i: цел32;
	нач
		НИЗКОУР.прочтиОбъектПоАдресу(adr, b);
		i := Raster.PaletteIndex(fmt.pal, кодСимв8(pix[R]), кодСимв8(pix[G]), кодСимв8(pix[B])) остОтДеленияНа 10H;
		если bit = 0 то НИЗКОУР.запишиОбъектПоАдресу(adr, симв8ИзКода(кодСимв8(b) - кодСимв8(b) остОтДеленияНа 10H + i))
		иначе НИЗКОУР.запишиОбъектПоАдресу(adr, симв8ИзКода(кодСимв8(b) остОтДеленияНа 10H + 10H*i))
		всё
	кон PackP4;

	проц UnpackP4 (перем fmt: Raster.Format0; adr: адресВПамяти; bit: размерМЗ; перем pix: Raster.Pixel);
		перем b: симв8;
	нач
		НИЗКОУР.прочтиОбъектПоАдресу(adr, b); pix := fmt.pal.col[арифмСдвиг(кодСимв8(b), -bit) остОтДеленияНа 10H]
	кон UnpackP4;

	(* load Oberon picture from file *)
	проц LoadPict (img: Raster.Image; перем fname: массив из симв8; перем done: булево);
		перем
			file: Files.File; r: Files.Reader; id, w, h, depth: цел16; gen: массив 64 из симв8; i, n: цел32;
			adr, a: адресВПамяти; pal: Raster.Palette; red, green, blue, byte: симв8; fmt: Raster.Format;
	нач
		file := Files.Old(fname);
		если file # НУЛЬ то
			Files.OpenReader(r, file, 0); r.чЦел16_мз(id);
			если id = 07F7H то	(* document *)
				r.чСтроку8˛включаяСимв0(gen); r.ПропустиБайты(4*2);
				r.чЦел16_мз(id)
			всё;

			если id = -4093 то	(* Pictures.PictFileId *)
				r.чЦел16_мз(w); r.чЦел16_мз(h); r.чЦел16_мз(depth);

				(* load picture palette *)
				i := 0; n := арифмСдвиг(1, depth); нов(pal);
				нцПока i < n делай
					r.чСимв8(red); r.чСимв8(green); r.чСимв8(blue);
					Raster.SetRGB(pal.col[i], кодСимв8(red), кодСимв8(green), кодСимв8(blue));
					увел(i)
				кц;
				если depth = 1 то
					Raster.SetRGB(pal.col[0], 255, 255, 255);
					Raster.SetRGB(pal.col[1], 0, 0, 0)
				всё;
				Raster.InitPalette(pal, устарПреобразуйКБолееУзкомуЦел(n), 2 + depth DIV 4);

				(* initialize image *)
				если depth = 1 то
					Raster.InitFormat(fmt, Raster.custom, 1, 1, {Raster.index}, pal, PackP1, UnpackP1)
				аесли depth = 4 то
					Raster.InitFormat(fmt, Raster.custom, 4, 1, {Raster.index}, pal, PackP4, UnpackP4)
				иначе
					Raster.InitPaletteFormat(fmt, pal)
				всё;
				Raster.Create(img, w, h, fmt);

				(* load run-length encoded pixels *)
				adr := img.adr + h * img.bpr;
				нцПока h > 0 делай
					a := adr - img.bpr;
					нцПока a < adr делай
						r.чСимв8(byte); n := кодСимв8(byte);
						если n < 128 то
							нцДо
								r.чСимв8(byte);
								НИЗКОУР.запишиОбъектПоАдресу(a, byte); увел(a);
								умень(n)
							кцПри n < 0
						иначе
							n := 100H - n;
							r.чСимв8(byte);
							нцДо
								НИЗКОУР.запишиОбъектПоАдресу(a, byte); увел(a);
								умень(n)
							кцПри n < 0
						всё
					кц;
					умень(h); умень(adr, img.bpr)
				кц;

				done := истина
			всё
		всё
	кон LoadPict;

	(* store Oberon picture *)
	проц StorePict (img: Raster.Image; перем fname: массив из симв8; перем done: булево);
		тип Bytes129 = массив 129 из симв8;
		перем
			file: Files.File; w: Files.Writer; pal: Raster.Palette; i, y, inc, x: размерМЗ; fmt: Raster.Format; depth: цел16;
			pix: укль на массив из симв8; buf: массив 129 из цел8;
			SrcCopy: Raster.Mode;
	нач
		file := Files.New(fname);
		если file # НУЛЬ то
			Raster.InitMode(SrcCopy, Raster.srcCopy);
			Files.OpenWriter(w, file, 0);
			w.пЦел16_мз(-4093);	(* Pictures.PictFileId *)
			w.пЦел16_мз(цел16(img.width)); w.пЦел16_мз(цел16(img.height));

			(* find out which format to use *)
			pal := img.fmt.pal;
			если (img.fmt.components = {Raster.index}) и (pal # НУЛЬ) то	(* contains index information *)
				если img.fmt.bpp > 4 то
					Raster.InitPaletteFormat(fmt, pal);
					depth := 8
				аесли img.fmt.bpp > 1 то
					Raster.InitFormat(fmt, Raster.custom, 4, 1, {Raster.index}, pal, PackP4, UnpackP4);
					depth := 4
				иначе
					Raster.InitFormat(fmt, Raster.custom, 1, 1, {Raster.index}, pal, PackP1, UnpackP1);
					depth := 1
				всё
			иначе	(* calculate palette *)
				нов(pal);
				Raster.ComputePalette(img, pal, 0, 255, 4);	(* no reserved colors *)
				Raster.InitPaletteFormat(fmt, pal);
				depth := 8
			всё;

			(* write palette *)
			w.пЦел16_мз(depth);
			i := 0;
			нцПока i < арифмСдвиг(1, depth) делай
				w.пСимв8(pal.col[i, R]); w.пСимв8(pal.col[i, G]); w.пСимв8(pal.col[i, B]);
				увел(i)
			кц;

			(* write run-length encoded pixels *)
			нов(pix, img.width); y := img.height; inc := 8 DIV depth;
			нцПока y > 0 делай
				умень(y);
				Raster.GetPixels(img, 0, y, img.width, fmt, pix^, 0, SrcCopy);
				buf[0] := 0; buf[1] := устарПреобразуйКБолееУзкомуЦел(кодСимв8(pix[0])); x := inc; i := 1;
				нцПока x < img.width делай
					если buf[0] < 0 то	(* accumulating equal bytes *)
						если (buf[0] > -128) и (pix[i] = симв8ИзКода(buf[1])) то
							умень(buf[0])
						иначе
							w.пБайты(НИЗКОУР.подмениТипЗначения(Bytes129, buf), 0, 2);
							buf[0] := 0; buf[1] := устарПреобразуйКБолееУзкомуЦел(кодСимв8(pix[i]))
						всё
					аесли buf[0] > 0 то	(* accumulating different bytes *)
						если buf[0] = 127 то	(* buffer full *)
							w.пБайты(НИЗКОУР.подмениТипЗначения(Bytes129, buf), 0, buf[0]+2);
							buf[0] := 0; buf[1] := устарПреобразуйКБолееУзкомуЦел(кодСимв8(pix[i]))
						аесли pix[i] # pix[i-1] то
							увел(buf[0]); buf[устарПреобразуйКБолееШирокомуЦел(buf[0])+1] := устарПреобразуйКБолееУзкомуЦел(кодСимв8(pix[i]))
						иначе
							умень(buf[0]);
							w.пБайты(НИЗКОУР.подмениТипЗначения(Bytes129, buf), 0, buf[0]+2);
							buf[0] := -1; buf[1] := устарПреобразуйКБолееУзкомуЦел(кодСимв8(pix[i]))
						всё
					аесли pix[i] = симв8ИзКода(buf[1]) то	(* starting to accumulate equal bytes *)
						buf[0] := -1
					иначе	(* starting to accumulate different bytes *)
						buf[0] := 1; buf[2] := устарПреобразуйКБолееУзкомуЦел(кодСимв8(pix[i]))
					всё;
					увел(x, inc); увел(i)
				кц;
				если buf[0] >= 0 то w.пБайты(НИЗКОУР.подмениТипЗначения(Bytes129, buf), 0, buf[0]+2)
				иначе w.пБайты(НИЗКОУР.подмениТипЗначения(Bytes129, buf), 0, 2)
				всё
			кц;
			w.ПротолкниБуферВПоток;
			Files.Register(file);
			done := истина
		всё
	кон StorePict;

	(* * install load/store procedures for handling Oberon pictures **)
(*	PROCEDURE Install*;
	BEGIN
		Raster.LoadProc := LoadPict; Raster.StoreProc := StorePict
	END Install;*)

	проц AosLoad*(x: динамическиТипизированныйУкль) : динамическиТипизированныйУкль;
	нач
		если x суть Raster.PictureTransferParameters то просейТип x:Raster.PictureTransferParameters делай
			LoadPict(x.img, x.name, x.done);
			Raster.Init(x.img, x.img.width, x.img.height, x.img.fmt, -x.img.bpr, адресОт(x.img.mem[0]) + (x.img.height-1)*x.img.bpr);
		всё всё;
		возврат НУЛЬ
	кон AosLoad;

	проц AosStore*(x: динамическиТипизированныйУкль) : динамическиТипизированныйУкль;
	нач
		если x суть Raster.PictureTransferParameters то просейТип x:Raster.PictureTransferParameters делай
			StorePict(x.img, x.name, x.done)
		всё всё;
		возврат НУЛЬ
	кон AosStore;


(*
	(**--- Oberon Patterns ---**)

	(** create image from Oberon pattern (format=A1) **)
	PROCEDURE PatternToImage* (pat: ADDRESS): Raster.Image;
		VAR w, h: SIGNED16; byte: CHAR; img: Raster.Image;
	BEGIN
		ASSERT(pat # 0, 100);
		IF (1 <= pat) & (pat <= 8) THEN RETURN PrntPat[pat] END;
		SYSTEM.GET(pat, byte); w := ORD(byte);
		SYSTEM.GET(pat+1, byte); h := ORD(byte);
		NEW(img); Raster.Init(img, w, h, Raster.A1, (w+7) DIV 8, pat+2);
		RETURN img
	END PatternToImage;

	(** initialize pattern from sets **)
	PROCEDURE NewPattern* (w, h: SIGNED32; VAR image: ARRAY OF SET): Raster.Image;
		VAR bpr, y: SIGNED32; sa, da: ADDRESS; img: Raster.Image;
	BEGIN
		bpr := (w+7) DIV 8;
		NEW(img); Raster.Create(img, w, h, Raster.A1);
		y := 0; sa := ADDRESSOF(image[0]); da := img.adr;
		WHILE y < h DO
			SYSTEM.MOVE(sa, da, bpr); INC(sa, 4); INC(da, bpr); INC(y)
		END;
		RETURN img
	END NewPattern;

	PROCEDURE InitPatterns;
		VAR pat: ARRAY 8 OF SET; i: SIGNED32;
	BEGIN
		(* adapted from Printer3 *)
		pat[0] := {}; pat[1] := {}; pat[2] := {}; pat[3] := {0,4,8,12,16,20,24,28};
		FOR i := 0 TO 3 DO pat[4 + i] := pat[i] END;
		PrntPat[1] := NewPattern(32, 8, pat);
		pat[0] := {0,4,8,12,16,20,24,28}; pat[1] := {}; pat[2] := {2,6,10,14,18,22,26,30}; pat[3] := {};
		FOR i := 0 TO 3 DO pat[4 + i] := pat[i] END;
		PrntPat[2] := NewPattern(32, 8, pat);
		pat[0] := {}; pat[1] := {0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30};
		FOR i := 0 TO 5 DO pat[2 + i] := pat[i] END;
		PrntPat[3] := NewPattern(32, 8, pat);
		pat[0] := {0,4,8,12,16,20,24,28}; pat[1] := {2,6,10,14,18,22,26,30};
		FOR i := 0 TO 5 DO pat[2 + i] := pat[i] END;
		PrntPat[4] := NewPattern(32, 8, pat);
		FOR i := 0 TO 7 DO pat[i] := {0..31} END;
		PrntPat[5] := NewPattern(32, 8, pat);
		pat[0] := {0,4,8,12,16,20,24,28}; pat[1] := {1,5,9,13,17,21,25,29};
		pat[2] := {2,6,10,14,18,22,26,30}; pat[3] := {3,7,11,15,19,23,27,31};
		FOR i := 0 TO 3 DO pat[4 + i] := pat[i] END;
		PrntPat[6] := NewPattern(32, 8, pat);
		pat[0] := {3,7,11,15,19,23,27,31}; pat[1] := {2,6,10,14,18,22,26,30};
		pat[2] := {1,5,9,13,17,21,25,29}; pat[3] := {0,4,8,12,16,20,24,28};
		FOR i := 0 TO 3 DO pat[4 + i] := pat[i] END;
		PrntPat[7] := NewPattern(32, 8, pat);
		FOR i := 0 TO 7 DO pat[i] := {0,4,8,12,16,20,24,28} END;
		PrntPat[8] := NewPattern(32, 8, pat)
	END InitPatterns;

BEGIN
	InitPatterns
*)
кон PictImages.

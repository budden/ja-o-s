модуль WMPerfMonPluginQuoteServer; (** AUTHOR "staubesv"; PURPOSE "Performance Monitor plugin for quote server statistics"; *)
(**
 * History:
 *
 *	27.02.2007	First release (staubesv)
 *)

использует
	WMPerfMonPlugins, QuoteServer, Modules;

конст
	ModuleName = "WMPerfMonPluginQuoteServer";

тип

	QuoteStats= окласс(WMPerfMonPlugins.Plugin)

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		перем ds : WMPerfMonPlugins.DatasetDescriptor;
		нач
			p.name := "Quote Server";
			p.description := "Quote server statistics";
			p.modulename := ModuleName;
			p.autoMin := ложь; p.autoMax := истина; p.minDigits := 7;

			нов(ds, 2);
			ds[0].name := "NnofQuotes";
			ds[1].name := "Nrequests";
			p.datasetDescriptor := ds;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		нач
			dataset[0] := QuoteServer.NnofQuotes;
			dataset[1] := QuoteServer.Nrequests;
		кон UpdateDataset;

	кон QuoteStats;

проц Install*;
кон Install;

проц InitPlugin;
перем par : WMPerfMonPlugins.Parameter; stats : QuoteStats;
нач
	нов(par); нов(stats, par);
кон InitPlugin;

проц Cleanup;
нач
	WMPerfMonPlugins.updater.RemoveByModuleName(ModuleName);
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
	InitPlugin;
кон WMPerfMonPluginQuoteServer.

WMPerfMonPluginQuoteServer.Install ~   System.Free WMPerfMonPluginQuoteServer ~

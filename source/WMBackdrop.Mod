модуль WMBackdrop;	(** AUTHOR "TF"; PURPOSE "Backdrop images"; *)

(* 03-04-03	RG: Context menu support added *)

использует
	Kernel, Files, ЛогЯдра, Потоки, Modules, Commands, Options, Строки8, WMRectangles, Raster, WMMessages,
	WMWindowManager, WMGraphics, TFClasses, WMPopups, WMComponents, WMRestorable, WMDialogs, XML;

конст
	ImagesFile = "Wallpapers.txt";

тип

	ImageInfo = запись
		filename: Files.FileName;
		img: WMGraphics.Image;
	кон;

	Window = окласс(WMWindowManager.Window)
	перем
		img : WMGraphics.Image;
		picname : массив 256 из симв8;
		changeable, stop: булево;
		interval: цел32; (* in seconds *)
		timer : Kernel.Timer;

		currentImg: цел32;

		fullscreen : булево;
		fullscreenX, fullscreenY, fullscreenW, fullscreenH : цел32;

		проц & New*;
		нач
			isVisible := истина;
			picname := "";
			changeable := ложь; stop := ложь;
			interval := 0;
			нов(timer);
			currentImg := -1;
			fullscreen := ложь;
			SetTitle(Строки8.ЯвиУСтроку("Backdrop"));
		кон New;

		проц {перекрыта}Draw*(canvas : WMGraphics.Canvas; w, h : размерМЗ; q : цел32);
		нач
			если img # НУЛЬ то
				canvas.ScaleImage(img,
					WMRectangles.MakeRect(0, 0, img.width, img.height),
					WMRectangles.MakeRect(0, 0, w, h), WMGraphics.ModeCopy, q);
			всё
		кон Draw;

		проц {перекрыта}PointerDown*(x, y : размерМЗ; keys : мнвоНаБитахМЗ);
		перем view : WMWindowManager.ViewPort;
			w, h : вещ32; originator : динамическиТипизированныйУкль;
			contextMenu : WMPopups.Popup;
		нач
			originator := sequencer.GetOriginator();
			если (originator # НУЛЬ) и (originator суть WMWindowManager.ViewPort) то
				если keys={0} то
					view := originator(WMWindowManager.ViewPort);
					w := bounds.r - bounds.l;
					h := bounds.b - bounds.t;
					view.SetRange(bounds.l, bounds.t, w, h, истина);
				аесли keys={2} то
					нов(contextMenu);
					если ~fullscreen то contextMenu.Add("Full screen", HandleFullScreen); всё;
					если (imgList # НУЛЬ) и (длинаМассива(imgList) > 0) то
						contextMenu.Add("Next", Change);
						если changeable то
							contextMenu.Add("Stop Change", HandleChangeable)
						иначе
							contextMenu.Add("Change", HandleChangeable)
						всё;
					всё;
					contextMenu.Add("Remove", HandleRemove);
					contextMenu.Popup(bounds.l+x, bounds.t+y)
				всё
			всё
		кон PointerDown;

		проц SetChangeable(c: булево);
		нач{единолично}
			changeable := c
		кон SetChangeable;

		проц HandleChangeable(sender, data: динамическиТипизированныйУкль);
		перем str: массив 32 из симв8; value, res: цел32;
		нач
			если changeable то
				SetChangeable(ложь);
			иначе
				str := "300";
				res := WMDialogs.QueryString("Interval (in sec.)", str);
				если res= WMDialogs.ResOk то
					Строки8.ПрочтиЦел32_изСтроки(str, value);
					если value > 0 то
						interval := value * 1000;
					иначе
						interval := 30 * 1000;
					всё;
					SetChangeable(истина);
				иначе
					interval := 0;
					SetChangeable(ложь);
				всё;
			всё;
			timer.Wakeup;
		кон HandleChangeable;

		проц HandleFullScreen(sender, par: динамическиТипизированныйУкль);
		перем view : WMWindowManager.ViewPort; w, h : вещ32; originator : динамическиТипизированныйУкль;
		нач
			manager.SetFocus(сам);
			originator := sender(WMComponents.Component).sequencer.GetOriginator();
			manager.SetFocus(сам);
			view := originator(WMWindowManager.ViewPort);
			w := bounds.r - bounds.l;
			h := bounds.b - bounds.t;
			view.SetRange(bounds.l, bounds.t, w, h, истина);
		кон HandleFullScreen;

		проц HandleRemove(sender, par: динамическиТипизированныйУкль);
		нач
			Stop();
			manager.SetFocus(сам);
			manager.Remove(сам);
			windowList.Remove(сам)
		кон HandleRemove;

		проц {перекрыта}Handle*(перем x: WMMessages.Message);
		перем configuration : XML.Element; value : цел32;
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) то
				если (x.ext суть WMRestorable.Storage) то
					нов(configuration); configuration.SetName("Configuration");
					WMRestorable.StoreString(configuration, "Image", picname);
					если changeable то value := interval; иначе value := 0; всё;
					WMRestorable.StoreLongint(configuration, "ChangeInterval", value);
					WMRestorable.StoreBoolean(configuration, "Fullscreen", fullscreen);
					WMRestorable.StoreLongint(configuration, "FullscreenX", fullscreenX);
					WMRestorable.StoreLongint(configuration, "FullscreenY", fullscreenY);
					WMRestorable.StoreLongint(configuration, "FullscreenW", fullscreenW);
					WMRestorable.StoreLongint(configuration, "FullscreenH", fullscreenH);
					x.ext(WMRestorable.Storage).Add("Backdrop", "WMBackdrop.Restore", сам, configuration)
				иначе Handle^(x)
				всё
			иначе Handle^(x)
			всё
		кон Handle;

		проц Change(sender, data: динамическиТипизированныйУкль);
		перем index: размерМЗ; img : WMGraphics.Image;
		нач
			если imgList # НУЛЬ то

				если (currentImg < 0) то
					(* First time we change the image. If the current image is part of the image list, set the index to it *)
					currentImg := FindIndex(picname);
				всё;

				нцДо
					увел(currentImg);
					index := currentImg  остОтДеленияНа длинаМассива(imgList);
				кцПри (imgList[index].filename # "");

				ЛогЯдра.пСтроку8("WMBackdrop: Changing to  "); ЛогЯдра.пСтроку8(imgList[index].filename); ЛогЯдра.пВК_ПС;

				если imgList[index].img = НУЛЬ то
					img := GetImage(imgList[index].filename, GetWidth(), GetHeight());
					если img = НУЛЬ то
						ЛогЯдра.пСтроку8("WBackdrop: Image could not be loaded."); ЛогЯдра.пВК_ПС;
						возврат
					иначе
						imgList[index].img := img;
					всё
				всё;
				нач{единолично}
					копируйСтрокуДо0(imgList[index].filename, picname);
					сам.img := imgList[index].img;
					Invalidate(WMRectangles.MakeRect(0, 0, GetWidth(), GetHeight()))
				кон
			всё
		кон Change;

		проц Stop;
		нач
			нач{единолично}
				stop := истина;
			кон;
			timer.Wakeup();
			нач {единолично}
				дождись(~stop)
			кон
		кон Stop;


	нач {активное}
		нц
			нач {единолично}
				дождись(changeable или stop);
			кон;
			если stop то прервиЦикл всё;
			Change(НУЛЬ, НУЛЬ);
			timer.Sleep(interval);
			если stop то прервиЦикл всё
		кц;

		нач{единолично}
			stop := ложь;
		кон
	кон Window;

перем
	windowList : TFClasses.List;
	imgList: укль на массив из ImageInfo;

проц GetImage(конст name : массив из симв8; w, h : размерМЗ) : WMGraphics.Image;
перем img, t : WMGraphics.Image;
	i, count : размерМЗ; ptr : динамическиТипизированныйУкль;

нач
	img := НУЛЬ;
	windowList.Lock;
	i := 0; count := windowList.GetCount();
	нцПока (img = НУЛЬ) и (i < count) делай
		ptr := windowList.GetItem(i);
		если (ptr(Window).picname = name) и (
			(ptr(Window).img.width = w) и (ptr(Window).img.height = h)
			или ( (w = 0) или (h = 0)))
		 то
			img := ptr(Window).img
		всё;
		увел(i)
	кц;
	windowList.Unlock;
	если img = НУЛЬ то
		t := WMGraphics.LoadImage(name, истина);
		если t # НУЛЬ то
			если w = 0 то w := t.width всё;
			если h = 0 то h := t.height всё;
			img := GetResizedImage(t, w, h);
		всё
	всё;
	ЛогЯдра.пВК_ПС;
	возврат img
кон GetImage;

проц GetResizedImage(image : WMGraphics.Image; width, height : размерМЗ) : WMGraphics.Image;
перем canvas : WMGraphics.BufferCanvas; resizedImage : WMGraphics.Image;
нач
	утв(image # НУЛЬ);
	нов(resizedImage); Raster.Create(resizedImage, width, height, WMWindowManager.format);
	нов(canvas, resizedImage);
	canvas.ScaleImage(image,
		WMRectangles.MakeRect(0, 0, image.width-1, image.height-1),
		WMRectangles.MakeRect(0, 0, width, height),
		WMGraphics.ModeCopy, WMGraphics.ScaleBilinear);
	утв(resizedImage # НУЛЬ);
	возврат resizedImage;
кон GetResizedImage;

(* Return index of img *)
проц FindIndex(конст imageName : массив из симв8) : цел32;
перем index : цел32;
нач
	index := -1;
	если (imageName # "") и (imgList # НУЛЬ) то
		index := 0;
		нцПока (index < длинаМассива(imgList)) и (imgList[index].filename # imageName) делай увел(index); кц;
		если (index >= длинаМассива(imgList)) то (* not found *) index := -1; всё;
	всё;
	возврат index;
кон FindIndex;

проц DefaultPos(перем x, y, w, h : размерМЗ);
перем manager : WMWindowManager.WindowManager;
	view : WMWindowManager.ViewPort;
	s : WMWindowManager.WindowStyle;
нач
	manager := WMWindowManager.GetDefaultManager();
	view := WMWindowManager.GetDefaultView();
	s := manager.GetStyle();
	x := округлиВниз(view.range.l); y := округлиВниз(view.range.t);
	w := округлиВниз(view.range.r - view.range.l);
	h := округлиВниз(view.range.b - view.range.t);
кон DefaultPos;

проц Rearrange;
перем ptr : динамическиТипизированныйУкль; i : размерМЗ; manager : WMWindowManager.WindowManager;
нач
	(* rearrange backrops so the latest added is on top to avoid confusion in the user *)
	manager := WMWindowManager.GetDefaultManager();
	windowList.Lock;
	нцДля i := windowList.GetCount() - 1 до 0 шаг -1 делай
		ptr := windowList.GetItem(i);
		manager.ToBack(ptr(WMWindowManager.Window))
	кц;
	windowList.Unlock;
кон Rearrange;

(**
	parameters x y w h
	defaults : x = 0 y = 0 w = img.width h = img.height
	any parameter can be replaced by ? to use the respective position or size of the current viewport *)
проц AddBackdropImage*(context : Commands.Context); (** [Options] imagename ~ *)
перем
	options : Options.Options;
	manager : WMWindowManager.WindowManager;
	view : WMWindowManager.ViewPort;
	bw : Window;
	img : WMGraphics.Image;
	x, y, tx, ty, tw, th : размерМЗ;
	fx, fy, fw, fh : цел32;
	width, height : размерМЗ;
	name : Files.FileName;
нач {единолично}
	нов(options);
	options.Add("f", "fullscreen", Options.Flag);
	если options.Parse(context.arg, context.error) то
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name);

		если options.GetFlag("fullscreen") то
			context.arg.ПропустиБелоеПоле;context.arg.чЦел32(fx, ложь);
			context.arg.ПропустиБелоеПоле;context.arg.чЦел32(fy, ложь);
			context.arg.ПропустиБелоеПоле;context.arg.чЦел32(fw, ложь);
			context.arg.ПропустиБелоеПоле; context.arg.чЦел32(fh, ложь);
			если (fw = 0) то fw := 1; всё;
			если (fh = 0) то fh := 1; всё;
			view := WMWindowManager.GetDefaultView();
			x := fx * view.width0;
			y := fy * view.height0;
			width := fw* view.width0;
			height := fh * view.height0;
		иначе
			DefaultPos(tx, ty, tw, th);
			если context.arg.ПодглядиСимв8() = '?' то
				x := tx; context.arg.ПропустиБайты(1); context.arg.ПропустиБелоеПоле();
			иначе
				x := 0;
				если (context.arg.ПодглядиСимв8() >= '0') и (context.arg.ПодглядиСимв8() <= '9') или (context.arg.ПодглядиСимв8() = '-') то context.arg.чРазмерМЗ(x, истина) всё;
				context.arg.ПропустиБелоеПоле();
			всё;

			если context.arg.ПодглядиСимв8() = '?' то
				y := ty; context.arg.ПропустиБайты(1); context.arg.ПропустиБелоеПоле();
			иначе
				y := 0;
				если (context.arg.ПодглядиСимв8() >= '0') и (context.arg.ПодглядиСимв8() <= '9') или (context.arg.ПодглядиСимв8() = '-') то context.arg.чРазмерМЗ(y, истина) всё;
				context.arg.ПропустиБелоеПоле();
			всё;
			если context.arg.ПодглядиСимв8() = '?' то
				width := tw; context.arg.ПропустиБайты(1); context.arg.ПропустиБелоеПоле();
			иначе
				width := 0;
				если (context.arg.ПодглядиСимв8() >= '0') и (context.arg.ПодглядиСимв8() <= '9') или (context.arg.ПодглядиСимв8() = '-') то context.arg.чРазмерМЗ(width, истина) всё;
				context.arg.ПропустиБелоеПоле();
			всё;
			если context.arg.ПодглядиСимв8() = '?' то
				height := th; context.arg.ПропустиБайты(1); context.arg.ПропустиБелоеПоле();
			иначе
				height := 0;
				если (context.arg.ПодглядиСимв8() >= '0') и (context.arg.ПодглядиСимв8() <= '9') или (context.arg.ПодглядиСимв8() = '-') то context.arg.чРазмерМЗ(height, истина) всё;
				context.arg.ПропустиБелоеПоле();
			всё;
		всё;

		img := GetImage(name, width, height);
		если img = НУЛЬ то
			context.error.пСтроку8("WMBackdrop: Image '"); context.error.пСтроку8(name);
			context.error.пСтроку8("' could not be loaded."); context.error.пВК_ПС;
			возврат
		всё;

		(* use image size *)
		если (width = 0) или (height = 0) то
			width := img.width;
			height := img.height;
		всё;

		нов(bw);
		копируйСтрокуДо0(name, bw.picname);
		bw.bounds := WMRectangles.MakeRect(0, 0, width, height);
		bw.img := img;
		если options.GetFlag("fullscreen") то
			bw.fullscreen := истина;
			bw.fullscreenX := fx; bw.fullscreenY := fy; bw.fullscreenW := fw; bw.fullscreenH := fh;
		всё;
		windowList.Add(bw);
		manager := WMWindowManager.GetDefaultManager();
		manager.Add(x, y, bw, {WMWindowManager.FlagStayOnBottom, WMWindowManager.FlagHidden});
		Rearrange;
	всё;
кон AddBackdropImage;

(* restore the desktop *)
проц Restore*(context : WMRestorable.Context);
перем w : Window;
	 xml : XML.Element;
	 s : Строки8.уСтрока; img : WMGraphics.Image;
	 view : WMWindowManager.ViewPort;
нач
	если context.appData # НУЛЬ то
		xml := context.appData(XML.Element);
		s := xml.GetAttributeValue("Image");
		если s # НУЛЬ то
			img := GetImage(s^, 0, 0);
			если img # НУЛЬ то

				нов(w);
				WMRestorable.LoadBoolean(xml, "Fullscreen", w.fullscreen);

				если w.fullscreen то
					view := WMWindowManager.GetDefaultView();
					если (view # НУЛЬ) то
						WMRestorable.LoadLongint(xml, "FullscreenX", w.fullscreenX);
						WMRestorable.LoadLongint(xml, "FullscreenY", w.fullscreenY);
						WMRestorable.LoadLongint(xml, "FullscreenW", w.fullscreenW);
						WMRestorable.LoadLongint(xml, "FullscreenH", w.fullscreenH);
						context.l := w.fullscreenX * view.width0;
						context.r := context.l + (w.fullscreenW * view.width0);
						context.t := w.fullscreenY * view.height0;
						context.b := context.t + (w.fullscreenH * view.height0);
					всё;
				всё;

				копируйСтрокуДо0(s^, w.picname);
				w.img :=  GetResizedImage(img, context.r - context.l, context.b - context.t);
				windowList.Add(w);
				WMRestorable.AddByContext(w, context);
				Rearrange
			всё
		всё;
		s := xml.GetAttributeValue("ChangeInterval");
		если (s # НУЛЬ) и (w # НУЛЬ) то
			если s^ # "0" то
				Строки8.ПрочтиЦел32_изСтроки(s^, w.interval);
				если w.interval > 500 то
					w.SetChangeable(истина);
				всё;
			всё;
		всё;
	всё
кон Restore;

проц Cleanup;
перем manager : WMWindowManager.WindowManager;
	ptr : динамическиТипизированныйУкль;
	w: Window;
	i : размерМЗ;
нач
	manager := WMWindowManager.GetDefaultManager();
	windowList.Lock;
	нцДля i := 0 до windowList.GetCount() - 1 делай
		ptr := windowList.GetItem(i);
		w := ptr(Window);
		w.Stop;
		manager.Remove(ptr(WMWindowManager.Window))
	кц;
	windowList.Unlock;
кон Cleanup;

проц RemoveAll*;
нач
	Cleanup;
кон RemoveAll;

проц ChangeList;
перем
	f: Files.File;
	r: Files.Reader;
	i, nr: цел32;

	проц NumberImg(): цел32;
	перем line: Files.FileName; r: Files.Reader; nr: цел32;
	нач
		Files.OpenReader(r, f, 0);
		нцПока (r.кодВозвратаПоследнейОперации = Потоки.Успех) делай
			r.чСтроку8ДоКонцаСтрокиТекстаВключительно(line);
			увел(nr);
		кц;
		возврат nr;
	кон NumberImg;

нач
	f := Files.Old(ImagesFile);
	если (f # НУЛЬ) то
		nr := NumberImg();
		нов(imgList, nr);
		Files.OpenReader(r, f, 0);
		i := 0;
		нцПока (r.кодВозвратаПоследнейОперации = Потоки.Успех) делай
			r.чСтроку8ДоКонцаСтрокиТекстаВключительно(imgList[i].filename);
			увел(i);
		кц;
		ЛогЯдра.пСтроку8("WMBackdrop: Image list "); ЛогЯдра.пСтроку8(ImagesFile); ЛогЯдра.пСтроку8(" loaded."); ЛогЯдра.пВК_ПС;
	иначе
		ЛогЯдра.пСтроку8("WMBackdrop: No image list found");ЛогЯдра.пВК_ПС;
	всё
кон ChangeList;

нач
	нов(windowList);
	ChangeList;
	Modules.InstallTermHandler(Cleanup)
кон WMBackdrop.

System.Free WMBackdrop ~

(* install backdrop at current view position and size *)
WMBackdrop.AddBackdropImage "Desktop1_1024x768.png" ? ? ? ?
WMBackdrop.AddBackdropImage BluebottlePic0.png ? ? ? ?~
WMBackdrop.AddBackdropImage AosBackdrop.png ? ? ? ?

(* install backdrop at specified position with original size of the image *)
WMBackdrop.AddBackdropImage BluebottlePic0.png ~
WMBackdrop.AddBackdropImage BluebottlePic0.png 0 0 ~
WMBackdrop.AddBackdropImage AosBackdrop.png 1280 0 ~

(* install backdrop whose size is specified relative to the view port size *)
WMBackdrop.AddBackdropImage --fullscreen BluebottlePic0.png ~
WMBackdrop.AddBackdropImage --fullscreen BluebottlePic0.png -1 0 ~


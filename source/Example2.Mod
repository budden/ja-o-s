(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Example2;	(* pjm *)

(*
Bounded buffer.
Ref: C.A.R. Hoare, "Monitors: An Operating System Structuring Concept", CACM 17(10), 1974
*)

тип
	Item* = окласс кон Item;

	Buffer* = окласс
		перем head, num: размерМЗ; buffer: укль на массив из Item;

		проц Append*(x: Item);
		нач {единолично}
			дождись(num # длинаМассива(buffer));
			buffer[(head+num) остОтДеленияНа длинаМассива(buffer)] := x;
			увел(num)
		кон Append;

		проц Remove*(): Item;
		перем x: Item;
		нач {единолично}
			дождись(num # 0);
			x := buffer[head];
			head := (head+1) остОтДеленияНа длинаМассива(buffer);
			умень(num);
			возврат x
		кон Remove;

		проц &Init*(n: размерМЗ);
		нач
			head := 0; num := 0; нов(buffer, n)
		кон Init;

	кон Buffer;

кон Example2.

модуль EFILib; (** AUTHOR "Matthias Frei"; PURPOSE "EFI API"; *)

использует НИЗКОУР, EFI, EFIFileProtocol, EFISimpleFS, EFILoadedImage, Трассировка;

тип Allocation = запись
	baseAddress: EFI.PhysicalAddress;
	numPages: EFI.Int;
кон;
тип PtrToLongString = укль на массив 1024 из EFI.Char16; (* to be able to use system.val etc.*)

конст
	maxAllocations = 128;
перем
	(* memory allocation *)
	allocations : массив maxAllocations из Allocation;
	numAllocations : цел32;

	(* argument parsing *)
	args : PtrToLongString;
	argLen : цел32;
	argPos : цел32;

(* allocate memory pages. If addr = -1 (=FFFFFFFFFH) then a page at an arbitrary position will be allocated. *)
проц AllocateMemory*(перем addr : EFI.PhysicalAddress; numPages : EFI.Int) : EFI.Status;
перем
	status : EFI.Status;
	allocationType : EFI.Int;
нач
	если numAllocations < maxAllocations-1 то
		если addr = -1 то
			allocationType := EFI.AllocateAnyPage;
		иначе
			allocationType := EFI.AllocateAddress;
		всё;
		status := EFI.table.BS.AllocatePages(allocationType, EFI.MTLoaderData, numPages, addr);
		если status = EFI.Success то
			allocations[numAllocations].baseAddress := addr;
			allocations[numAllocations].numPages := numPages;
			увел(numAllocations);
		иначе
			addr := 0;
		всё;
		возврат status;
	иначе
		возврат EFI.Error;
	всё;
кон AllocateMemory;

(* free all pages allocated by AllocateMemory *)
проц FreeMemory*;
перем
	status : EFI.Status;
	i : цел32;
нач
	нцДля i := 0 до numAllocations - 1 делай
		status := EFI.table.BS.FreePages(allocations[i].baseAddress, allocations[i].numPages);
	кц;
кон FreeMemory;


проц GetMemoryMapping*(перем adr: EFI.PhysicalAddress) : EFI.Status;
конст
	approxMemMapSize = 2048; (* we actually can not resize the buffer. just hope its enough *)
перем
	MemMapSize, MapKey, DescriptorSize: EFI.Int;  DescriptorVersion : EFI.Int32;
	status : EFI.Status;
	MemMap : массив approxMemMapSize из EFI.MemoryDescriptor;
	MemMapElement : EFI.MemoryDescriptorPointer;
	i, numEntries : EFI.Int;
	hi, numcritEntries : цел64;
	memsize : цел64;
	tmp : EFI.PhysicalAddress;
нач
	MemMapSize := длинаМассива(MemMap)*размер16_от(EFI.MemoryDescriptor);
	status := EFI.table.BS.GetMemoryMap(MemMapSize, MemMap, MapKey, DescriptorSize, DescriptorVersion);
	если status = EFI.ErrBufferTooSmall то
		(* we cant resize. We are doomed!*)
		возврат status;
	всё;

	adr := -1;
	трассируй(adr);
	status :=AllocateMemory(adr, 10); (* a few additional pages *)
	(* now bootTablePhAddr contains the base address of the allocated pages *)
	если status # EFI.Success то
		возврат status;
	всё;
	numcritEntries := 0;
	MemMapSize := длинаМассива(MemMap)*размер16_от(EFI.MemoryDescriptor);
	status := EFI.table.BS.GetMemoryMap(MemMapSize, MemMap, MapKey, DescriptorSize, DescriptorVersion);
	если status = EFI.ErrBufferTooSmall то
		(* we cant resize. We are doomed!*)
		возврат status;
	всё;
	numEntries := MemMapSize DIV DescriptorSize; (*DIV SIZEOF(EFI.MemoryDescriptor); *)


	tmp := 0H;
	memsize := 0;
	нцДля i := 0 до numEntries-1 делай
		MemMapElement := НИЗКОУР.подмениТипЗначения(EFI.MemoryDescriptorPointer, адресОт(MemMap[0])+i*DescriptorSize);

(*		IF (MemMapElement.Type #  EFI.MTConventionalMemory *)
		если ( (MemMapElement.Type = EFI.MTRuntimeServicesCode) или ( MemMapElement.Type = EFI.MTRuntimeServicesData)
			или (MemMapElement.Type = EFI.MTUnusableMemory) или (MemMapElement.Type = EFI.MTACPIReclaimMemory)
			или (MemMapElement.Type = EFI.MTACPIMemoryNVSM) или (MemMapElement.Type = EFI.MTMemoryMappedIO)
			или (MemMapElement.Type = EFI.MTMemoryMappedIOPortSpace) или (MemMapElement.Type = EFI.MTPalCode)
			или (MemMapElement.Type = EFI.MTLoaderCode) или (MemMapElement.Type = EFI.MTLoaderData)
			или (MemMapElement.Type = EFI.MTReserved)
			или (MemMapElement.Type = EFI.MTBootServicesCode) или (MemMapElement.Type = EFI.MTBootServicesData) (* this line shouldnt be - but it freezes if i touch that memory *)
			)то
			Трассировка.пАдресВПамяти(адресВПамяти(MemMapElement.PhysicalStart));
			Трассировка.пСтроку8(" - " );
			Трассировка.пАдресВПамяти(адресВПамяти(MemMapElement.PhysicalStart+MemMapElement.NumberOfPages*устарПреобразуйКБолееШирокомуЦел(EFI.PageSize)));
			Трассировка.пСтроку8(" X ");
			Трассировка.пАдресВПамяти(адресВПамяти(MemMapElement.VirtualStart));
			Трассировка.пСтроку8(" - " );
			Трассировка.пАдресВПамяти(адресВПамяти(MemMapElement.VirtualStart+MemMapElement.NumberOfPages*устарПреобразуйКБолееШирокомуЦел(EFI.PageSize)));
			Трассировка.пВК_ПС;



			если (tmp # MemMapElement.PhysicalStart) то
				НИЗКОУР.запишиОбъектПоАдресу(адресВПамяти(adr+(numcritEntries*2+1+1)*8), MemMapElement.PhysicalStart);
				tmp := MemMapElement.PhysicalStart+MemMapElement.NumberOfPages*устарПреобразуйКБолееШирокомуЦел(EFI.PageSize);
				НИЗКОУР.запишиОбъектПоАдресу(адресВПамяти(adr+(numcritEntries*2+1+1+1)*8), tmp);
				numcritEntries := 	numcritEntries +1;
			иначе
				numcritEntries := 	numcritEntries -1;
				tmp := MemMapElement.PhysicalStart+MemMapElement.NumberOfPages*устарПреобразуйКБолееШирокомуЦел(EFI.PageSize);
				НИЗКОУР.запишиОбъектПоАдресу(адресВПамяти(adr+(numcritEntries*2+1+1+1)*8), tmp);
				numcritEntries := 	numcritEntries +1;
			всё;
		всё;
		memsize := memsize + MemMapElement.NumberOfPages;
	кц;

	нцДля hi:= 0 до numcritEntries-1 делай
		НИЗКОУР.прочтиОбъектПоАдресу(адресВПамяти(adr+(hi*2+1+1)*8), tmp);
		НИЗКОУР.прочтиОбъектПоАдресу(адресВПамяти(adr+(hi*2+1+1+1)*8), tmp);
	кц;

	НИЗКОУР.запишиОбъектПоАдресу(адресВПамяти(adr),numcritEntries);
	НИЗКОУР.запишиОбъектПоАдресу(адресВПамяти(adr+8),memsize);
	трассируй(adr);
	возврат EFI.Success;
кон GetMemoryMapping;

(* find the size of the RAM by investigating the entries of the EFI Memory Map.  *)
проц GetMemorySize*(перем memsize : EFI.Int64 ) : EFI.Status;
конст
	approxMemMapSize = 2048; (* we actually can not resize the buffer. just hope its enough *)
перем
	MemMapSize, MapKey, DescriptorSize: EFI.Int;  DescriptorVersion : EFI.Int32;
	status : EFI.Status;
	MemMap : массив approxMemMapSize из EFI.MemoryDescriptor;
	MemMapElement : EFI.MemoryDescriptorPointer;
	i, numEntries : EFI.Int;
нач
	MemMapSize := длинаМассива(MemMap)*размер16_от(EFI.MemoryDescriptor);
	status := EFI.table.BS.GetMemoryMap(MemMapSize, MemMap, MapKey, DescriptorSize, DescriptorVersion);
	если status = EFI.ErrBufferTooSmall то
		(* we cant resize. We are doomed!*)
		возврат status;
	всё;

	numEntries := MemMapSize DIV DescriptorSize; (*DIV SIZEOF(EFI.MemoryDescriptor); *)
	memsize := 0;

	Трассировка.пВК_ПС();
	Трассировка.пАдресВПамяти(MemMapSize);
	Трассировка.пВК_ПС();
	Трассировка.пАдресВПамяти(размер16_от(EFI.MemoryDescriptor));
	Трассировка.пВК_ПС();
	Трассировка.пАдресВПамяти(36);
	Трассировка.пВК_ПС();
	Трассировка.пАдресВПамяти(DescriptorSize);
	Трассировка.пВК_ПС();
	Трассировка.пАдресВПамяти(DescriptorVersion);
	Трассировка.пВК_ПС();
	Трассировка.пВК_ПС();
	нцДля i := 0 до numEntries-1 делай
		MemMapElement := НИЗКОУР.подмениТипЗначения(EFI.MemoryDescriptorPointer, адресОт(MemMap[0])+i*DescriptorSize);
	(*
			MTLoaderCode* = 1;
	MTLoaderData* = 2;
	MTBootServicesCode* = 3;
	MTBootServicesData* = 4;
	MTRuntimeServicesCode* = 5;
	MTRuntimeServicesData* = 6;
	MTConventionalMemory*= 7;
	MTUnusableMemory* = 8;
	MTACPIReclaimMemorz* = 9;
	MTACPIMemoryNVSM* = 10;
	MTMemoryMappedIO* = 11;
	MTMemoryMappedIOPortSpace* = 12;
	MTPalCode* = 13;
	MTMaxMemoryType* = 14;
		*)
(*	IF (TRUE) THEN*)

		memsize := memsize + MemMapElement.NumberOfPages;
		(*IF (memsize < MemMap[i].PhysicalStart + MemMap[i].NumberOfPages*LONG(EFI.PageSize)) THEN
			memsize := MemMap[i].PhysicalStart + MemMap[i].NumberOfPages*LONG(EFI.PageSize);
		END;*)
	кц;
	memsize := memsize * устарПреобразуйКБолееШирокомуЦел(EFI.PageSize);
	Трассировка.пАдресВПамяти(устарПреобразуйКБолееШирокомуЦел(EFI.PageSize));
	Трассировка.пВК_ПС();
	Трассировка.п16ричное(memsize,8);
	Трассировка.пВК_ПС();
(*	memsize := SYSTEM.GET32(ADDRESSOF(memsize)+4); (* BUG! where ? *)*)

	возврат EFI.Success;
кон GetMemorySize;

(* stop bootservices. The idea is that we get the current memory map, s.t. the OS is aware of EFI allocated memory. We dont
care so we just throw it away *)
проц ExitBootServices*(): EFI.Status;
конст
	approxMemMapSize = 2048; (* we actually can not resize the buffer. just hope its enough *)
перем
	MemMapSize, MapKey, DescriptorSize: EFI.Int; DescriptorVersion : EFI.Int32;
	status : EFI.Status;
	MemMap : массив approxMemMapSize из EFI.MemoryDescriptor;
нач
	MemMapSize := длинаМассива(MemMap)*размер16_от(EFI.MemoryDescriptor);
	нцДо
		status := EFI.table.BS.GetMemoryMap(MemMapSize, MemMap, MapKey, DescriptorSize, DescriptorVersion);
		если status = EFI.ErrBufferTooSmall то
			(* we cant resize. We are doomed! *)
			возврат status;
		всё;
		status := EFI.table.BS.ExitBootServices(EFI.imageHandle, MapKey);
	кцПри status = EFI.Success;

	(*
     EFI.table.ConsoleInHandle := 0H;
     EFI.table.ConIn:= NIL;
	EFI.table.ConsoleOutHandle:= 0H;
	EFI.table.ConOut:= NIL;
	EFI.table.StandardErrorHandle:= 0H;
	EFI.table.StdErr:= NIL;
	EFI.table.BS:= NIL;*)

	возврат EFI.Success;
кон ExitBootServices;

(* Get the arguments given to the image *)
проц GetArgs*(перем loadOptionsSize : цел32; перем loadOptions : EFILoadedImage.PtrToArrayOfByte) : EFI.Status;
перем
	prot : EFI.Protocol; loadedImage : EFILoadedImage.Protocol;
	status : EFI.Status;
нач
	status := EFI.table.BS.HandleProtocol(EFI.imageHandle, EFILoadedImage.GUID, prot);
	если status = EFI.Success то
		loadedImage := НИЗКОУР.подмениТипЗначения(EFILoadedImage.Protocol, prot);
		loadOptionsSize := loadedImage.LoadOptionsSize;
		loadOptions := loadedImage.LoadOptions;
	всё;
	возврат status;
кон GetArgs;

(* (re-) initialize variables for GetNextArg. Automatically called by at the first call to GetNextArg. Can be used to reset GetNextArg.
Assumes that EFI-Shell passes commandline as a string in the form "imagename (arguments)*" like e.g. unix shells do. *)
проц InitArgs*() : EFI.Status;
перем
	prot : EFI.Protocol; loadedImage : EFILoadedImage.Protocol;
	status : EFI.Status;
	loadOptionsSize : цел32;
	loadOptions : EFILoadedImage.PtrToArrayOfByte;
нач
	status := EFI.table.BS.HandleProtocol(EFI.imageHandle, EFILoadedImage.GUID, prot); (* not using GetArgs since we want access to other fields of the loadedImage-protocol *)
	если status = EFI.Success то
		loadedImage := НИЗКОУР.подмениТипЗначения(EFILoadedImage.Protocol, prot);
		loadOptionsSize := loadedImage.LoadOptionsSize;
		loadOptions := loadedImage.LoadOptions;

		(* setup GetNextArg variables *)
		args:= НИЗКОУР.подмениТипЗначения(PtrToLongString, loadOptions);
		argLen := loadOptionsSize DIV 2; (* 16-bit characters *)
		argPos := 0;

		(* skip the 'imagename' by skipping the first word of the load options *)
		(* Do not skip if the image was not loaded directly by the firmware bootmanager.
		This should be useful to not have to type the name of the image as an argument when starting directly
		from the bootmanager while still being compatible to the EFI-Shell.
		ParentHandle should be NULL if the image was loaded by the firmware bootmanager. *)
		(* Apparently this does not work (at least it does not in qemu-EFI) although it should - therefore commented out
		IF (loadedImage.ParentHandle # 0) THEN *)

		нцПока (argPos < argLen) и (args[argPos] # 0) и (args[argPos] # кодСимв8(' ')) делай
			увел(argPos);
		кц;

		(*END; *)
	иначе
		ReportError(status);
	всё;
	возврат status;
кон InitArgs;

(* read one argument after another. Returns true iff successfully parsed.
Treats ' ' (blank) as seperator between arguments. (text between '"' is NOT merged into one argument.
 *)
проц GetNextArg*(перем arg : массив из EFI.Char16) : булево;
перем
	status : EFI.Status;
	i : цел32;
нач
	если args = НУЛЬ то
		status := InitArgs();
		если (status # EFI.Success) то
			возврат ложь;
		всё;
	всё;

	(* skip whitspace *)
	нцПока (argPos < argLen) и (args[argPos] = кодСимв8(' ')) делай увел(argPos); кц;

	i := 0;
	нцПока (i < длинаМассива(arg)) и (argPos < argLen) и (args[argPos] # 0) и (args[argPos] # кодСимв8(' ')) делай
		arg[i] := args[argPos];
		увел(i); увел(argPos);
	кц;

	возврат (i > 0);
кон GetNextArg;

(* read a string from simpleInputInterface *)
проц ReadString*(перем buf : массив из EFI.Char16);
конст ScanCodeNull = 0; (* if ScanCode = 0 then UnicodeChar is valid. Else a special key was hit (e.g ESC, Fx,..) *)
	CharLF = 0AH;
	CharCR = 0DH;
перем key : EFI.InputKey; i : цел32;
	status : EFI.Status;
	ConIn : укль на EFI.SimpleInputInterface;
	ConOut : укль на EFI.SimpleTextOutputInterface;
	newline, cursorEnabled : булево;
	lastChar : массив 2 из EFI.Char16;
нач
	ConIn := EFI.table.ConIn;
	ConOut := EFI.table.ConOut;
	lastChar[1] := 0;
	cursorEnabled := ConOut.Mode.CursorVisible;
	если (~cursorEnabled) то
		status := ConOut.EnableCursor(ConOut, истина);
	всё;
	status := ConIn.Reset(ConIn, ложь);
	newline := ложь; i := 0;
	нцПока (i < длинаМассива(buf)-1) и (~newline) делай
		status := ConIn.ReadKey(ConIn, key);
		если (status = EFI.Success) и (key.ScanCode = ScanCodeNull) то
			если (key.UnicodeChar # CharLF) и (key.UnicodeChar # CharCR) то
				buf[i] := key.UnicodeChar;
				lastChar[0] := key.UnicodeChar;
			иначе
				buf[i] := 0H;
				newline := истина;
				lastChar[0] := key.UnicodeChar;
			всё;
			status := ConOut.OutputString(ConOut, lastChar);
			увел(i);
		всё;
	кц;
	buf[длинаМассива(buf)-1] := 0H;
	если (~cursorEnabled) то
		status := ConOut.EnableCursor(ConOut, ложь);
	всё;
кон ReadString;

(* convert signed integer to a string *)
проц IntToString* (конст x : цел32; перем s : массив из симв8);
перем i, slen, start : размерМЗ; x0 : цел32;
нач
	если x = матМинимум (цел32) то
		s := "-2147483648";
		возврат;
	всё;

	slen := длинаМассива(s)-1;
	если (slen > 11) то
		slen := 11;
	всё;
	s[slen] := 0X;

	start := 0;
	если (x < 0) и ( slen > 0 ) то
		x0 := -x;
		s[0] := '-'; start := 1;
	всё;
	i := slen;
	нцПока ( i > start ) делай
		умень(i);
		s[i] := симв8ИзКода (x0 остОтДеленияНа 10 + 30H);
		x0 := x0 DIV 10;
	кц;
кон IntToString;

(* Copied from BIOS.I386.Machine.Mod - StrToInt *)
(** Convert a string to an integer. Parameter i specifies where in the string scanning should begin (usually 0 in the first call). Scanning stops at the first non-valid character, and i returns the updated position. Parameter s is the string to be scanned. The value is returned as result, or 0 if not valid. Syntax: number = ["-"] digit {digit} ["H" | "h"] . digit = "0" | ... "9" | "A" .. "F" | "a" .. "f" . If the number contains any hexdecimal letter, or if it ends in "H" or "h", it is interpreted as hexadecimal. *)
проц StringToInt* (перем i: размерМЗ; конст s: массив из симв8): цел32;
перем vd, vh, sgn, d: цел32; hex: булево;
нач
	vd := 0; vh := 0; hex := ложь;
	если s[i] = "-" то sgn := -1; увел (i) иначе sgn := 1 всё;
	нц
		если (s[i] >= "0") и (s[i] <= "9") то d := кодСимв8 (s[i])-кодСимв8 ("0")
		аесли (ASCII_вЗаглавную (s[i]) >= "A") и (ASCII_вЗаглавную (s[i]) <= "F") то d := кодСимв8 (ASCII_вЗаглавную (s[i]))-кодСимв8 ("A") + 10; hex := истина
		иначе прервиЦикл
		всё;
		vd := 10*vd + d; vh := 16*vh + d;
		увел (i)
	кц;
	если ASCII_вЗаглавную (s[i]) = "H" то hex := истина; увел (i) всё;	(* optional H *)
	если hex то vd := vh всё;
	возврат sgn * vd
кон StringToInt;

(* copy the String 'str' to the char16-String 'lstr'. If 'lstr' is shorter than 'str' the prefix fitting into 'lstr' is copied. *)
проц StringToLongString*(конст str : массив из симв8; перем lstr : массив из EFI.Char16);
перем strlen, lstrlen,i : размерМЗ;
нач
	strlen := длинаМассива(str);
	lstrlen := длинаМассива(lstr);
	i := 0;
	нцПока (i < strlen) и (i < lstrlen) и (кодСимв8(str[i]) # 0) делай
		lstr[i] := кодСимв8(str[i]);
		увел(i);
	кц;
	если lstrlen > 0 то
		lstr[i] := 0;
	всё;
кон StringToLongString;

(* returns the protocol identified by 'guid'. If there are multiple handles that handle this protocol, one is chosen arbitrarily *)
проц GetProtocol*(конст guid : EFI.GUID; перем prot : EFI.Protocol) : EFI.Status;
перем
	handle : EFI.Handle;
	handleBuf : массив 512 из EFI.Handle; (* make it large enough, we cant resize easily *)
	handleBufSize, i : EFI.Int;
	status : EFI.Status;
нач
	handleBufSize := длинаМассива(handleBuf) * размер16_от(EFI.Handle);
	status := EFI.table.BS.LocateHandle(EFI.ByProtocol, guid, 0, handleBufSize, handleBuf);
	если (status = EFI.Success) и (handleBufSize > 0) то
		i := handleBufSize DIV размер16_от(EFI.Handle);
		нцПока(i>0) делай
			умень(i);
			handle := handleBuf[i];
			status := EFI.table.BS.HandleProtocol(handle, guid, prot);
			если status = EFI.Success то возврат EFI.Success; всё;
		кц;
		возврат EFI.Error;
	всё;
	возврат status;
кон GetProtocol;

проц GetFileSize*(file : EFIFileProtocol.Protocol) : EFI.Int64;
перем
	status : EFI.Status;
	infoBuf : EFIFileProtocol.FileInfo;
	infoBufSize : EFI.Int;
	fileSize : EFI.Int64;
нач
	infoBufSize := размер16_от(EFIFileProtocol.FileInfo);
	status := file.GetInfo(file, EFIFileProtocol.FileInfoGUID, infoBufSize, infoBuf);
	если (status = EFI.Success) то
		fileSize := infoBuf.FileSize;
	всё;
	возврат fileSize;
кон GetFileSize;

(* search 'fn' on all devices that handle the SimpleFS protocol (= FAT partitions) and open it read-write *)
проц OpenFile*(конст fn : массив из EFI.Char16) : EFIFileProtocol.Protocol;
перем
	status : EFI.Status;
	handleBuf : массив 128 из EFI.Handle; (* make it large enough, we cant resize easily *)
	handleBufSize : EFI.Int;
	file : EFIFileProtocol.Protocol;
	done : булево;
	iter : цел32;
нач
	(* get all devices (or device drivers, handlers or what ever) that support the EFISimpleFS *)
	handleBufSize := длинаМассива(handleBuf) * размер16_от(EFI.Handle);
	status := EFI.table.BS.LocateHandle(EFI.ByProtocol, EFISimpleFS.GUID, 0, handleBufSize, handleBuf);
	если (status = EFI.Success) и (handleBufSize > 0) то
		done := ложь;
		iter := 0;
		(* look for the file on each device *)
		нцПока (iter < handleBufSize DIV размер16_от(EFI.Handle)) и (~done) делай
			file := OpenFileOnDevice(fn, handleBuf[iter]);
			если (file # НУЛЬ) то
				done := истина;
			всё;
			увел(iter);
		кц;
	иначе
		file := НУЛЬ;
	всё;
	возврат file;
кон OpenFile;

(* look for file 'fn' on device 'deviceHandle' and try to open it read-write. Returns NIL if not found *)
проц OpenFileOnDevice(конст fn : массив из EFI.Char16; deviceHandle : EFI.Handle)  : EFIFileProtocol.Protocol;
перем
	root : EFIFileProtocol.Protocol;
	file : EFIFileProtocol.Protocol;
	protSimpleFS : EFISimpleFS.Protocol;
	prot : EFI.Protocol;
	status : EFI.Status;
нач
	file := НУЛЬ;
	(* first get a SimpleFS-Protocol for handle *)
	status := EFI.table.BS.HandleProtocol(deviceHandle, EFISimpleFS.GUID, prot);
	protSimpleFS := НИЗКОУР.подмениТипЗначения(EFISimpleFS.Protocol,prot);
	если (status = EFI.Success) и (protSimpleFS # НУЛЬ) то
		(* get a File-descriptor thingy for the root directory *)
		status := protSimpleFS.OpenVolume(protSimpleFS, root);
		если (status = EFI.Success) то
			status := root.Open(root, file, fn, EFIFileProtocol.ModeRead, 0);
			если (status # EFI.Success) то
				file := НУЛЬ;
			всё;
		всё;
	всё;
	возврат file;
кон OpenFileOnDevice;

(* allocate memory and copy the content of the file to memory. If loadAddr = -1 an arbitrary memory page will be allocated *)
проц LoadFile*(file : EFIFileProtocol.Protocol; перем loadAddr : адресВПамяти) : EFI.Status;
перем
	status : EFI.Status;
	fileSize : EFI.Int64;
	numPages : EFI.Int;
	addr : EFI.PhysicalAddress;
	memSize : EFI.Int;
нач
	fileSize :=  GetFileSize(file);
	numPages := устарПреобразуйКБолееУзкомуЦел(fileSize DIV EFI.PageSize) + 1;

	addr := loadAddr;
	status := AllocateMemory(addr, numPages);
	(* now addr contains the base address of the allocated pages *)
	если status = EFI.Success то
		loadAddr := адресВПамяти(addr);
		memSize := EFI.Int(fileSize);
		status := file.Read(file, memSize, loadAddr);
	всё;
	возврат status;
кон LoadFile;

(* Reports a description of the error code *)
проц ReportError*(status : EFI.Status);
нач
(*
	CASE status OF
		|EFI.Success : Trace.String("Success");
		(* Warnings *)
		| EFI.WarnUnknownGlyph : Trace.String("Warning : Unknown Glyph");
		| EFI.WarnDeleteFailure : Trace.String("Warning : Delete Failure");
		| EFI.WarnWriteFailure : Trace.String("Warning : Write Failure");
		| EFI.WarnBufferTooSmall : Trace.String("Warning : Buffer Too Small");
		(* Errors*)
		| EFI.ErrLoadError : Trace.String("Error : Load Error");
		| EFI.ErrInvalidParameter : Trace.String("Error : Invalid Parameter");
		| EFI.ErrUnsupported : Trace.String("Error : Unsupported");
		| EFI.ErrBadBufferSize : Trace.String("Error : Bad Buffer Size");
		| EFI.ErrBufferTooSmall : Trace.String("Error : Buffer Too Small");
		(* ... TODO ... *)
		| EFI.ErrNotFound :  Trace.String("Error : Not Found");
		(* ... TODO ... *)
		| EFI.ErrEndOfFile : Trace.String("Error : End Of File");
		| EFI.ErrInvalidLanguage : Trace.String("Error : Invalid Language");
	END;
	*)
	Трассировка.пСтроку8("Error Code: "); Трассировка.пЦел64(цел32(status - EFI.Error), 0);
	Трассировка.пВК_ПС;
кон ReportError;

нач
	numAllocations := 0;
	args := НУЛЬ;
кон EFILib.

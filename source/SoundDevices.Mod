модуль SoundDevices;	(** AUTHOR "TF"; PURPOSE "Abstract sound device driver / Generic sound support"; *)

использует
		Plugins, Modules;

перем
	devices* : Plugins.Registry;

конст
	ResOK* = 0;
	ResQualityReduced* = 1; (** ResReducedQuality can be the res value of OpenPlayChannel if the playing quality
													of the resulting channel may be reduced due to insufficient ressources *)
	ResNoMoreChannels* = 10;
	ResUnsupportedFrequency* = 20;
	ResUnsupportedSubChannels* = 30;
	ResUnsupportedSamplingRes* = 40;
	ResUnsupportedFormat* = 50;

	(* Channel types *)
	ChannelPlay* = 0;
	ChannelRecord* = 1;

	(** Currently only PCM defined *)
	FormatPCM* = 0;

тип
	(** a generic buffer. data^ contains the data to be played or space for the data recorded.
		len contains the length of the buffer.
		len MOD 4 must be 0
		Buffers may be extended by the client application for buffer management. A buffer may only be reused if
		it is returned to the BufferListener.
	*)
	Buffer* = окласс
	перем
		len* : размерМЗ;
		data* : укль на массив из симв8;
	кон Buffer;

	(* a blocking buffer pool that can be used by a sound playing process to synchronize sound generation *)
	BufferPool* = окласс
		перем head, num: размерМЗ; buffer: укль на массив из Buffer;

		проц &Init*(n: цел32);
		нач
			head := 0; num := 0; нов(buffer, n)
		кон Init;

		проц Add*(x: Buffer);
		нач {единолично}
			дождись(num # длинаМассива(buffer));
			buffer[(head+num) остОтДеленияНа длинаМассива(buffer)] := x;
			увел(num)
		кон Add;

		проц Remove*(): Buffer;
		перем x: Buffer;
		нач {единолично}
			дождись(num # 0);
			x := buffer[head];
			head := (head+1) остОтДеленияНа длинаМассива(buffer);
			умень(num);
			возврат x
		кон Remove;

		проц NofBuffers*(): размерМЗ;
		нач {единолично}
			возврат num
		кон NofBuffers;

	кон BufferPool;

	(** A buffer listener is called when a play / or record buffer is completed,
			the application should reuse eg. process the buffer *)
	BufferListener* = проц { делегат } (buffer : Buffer);

	(** Generic MixerChannel object. Allows to set and get volume information *)
	MixerChannel* = окласс
		(** Return the name (as UTF-8 Unicode) of this channel *)
		проц GetName*(перем name : массив из симв8);
		нач СТОП(99) (* abstract *)
		кон GetName;

		(** Return the description string (as UTF-8 Unicode) of this channel *)
		проц GetDesc*(перем desc : массив из симв8);
		нач СТОП(99) (* abstract *)
		кон GetDesc;

		(** Set the volume of the channel *)
		(** 0 is silent, 255 is max *)
		проц SetVolume*(volume : целМЗ);
		нач СТОП(99) (* abstract *)
		кон SetVolume;

		(** Get the volume of the channel *)
		(** 0 is silent, 255 is max *)
		проц GetVolume*() : целМЗ;
		нач СТОП(99) (* abstract *)
		кон GetVolume;

		(** mute or unmute the channel *)
		проц SetMute*(mute : булево);
		нач СТОП(99) (* abstract *)
		кон SetMute;

		(** get the "mute - state" of the channel *)
		проц GetIsMute*() : булево;
		нач СТОП(99) (* abstract *)
		кон GetIsMute;
	кон MixerChannel;

	(** a MixerChangedProc delegate is called whenever a channel (volume / mute) is changed *)
	MixerChangedProc* = проц { делегат } (channel : MixerChannel);

	(** Generic channel *)
	Channel* = окласс
		(** Return if the channel is ChannelPlay or ChannelRecord *)
		проц GetChannelKind*() : цел32;
		кон GetChannelKind;

		(** Set the current volume of the channel *)
		(** Volume is a 8.8 bit fix-point value, 0 is silent *)
		проц SetVolume*(volume : цел32);
		нач СТОП(99) (* abstract *)
		кон SetVolume;

		(** Get the current volume of the channel *)
		проц GetVolume*() : цел32;
		нач СТОП(99) (* abstract *)
		кон GetVolume;

		(** GetPosition return the current position in samples. MAY CHANGE TO SIGNED64*)
		проц GetPosition*() : цел32;
		нач СТОП(99) (* abstract *)
		кон GetPosition;

		(** Register a delegate that handles reuse / processing of buffers. Only one Buffer listener can be registered
		per channel *)
		проц RegisterBufferListener*(bufferListener : BufferListener);
		нач СТОП(99) (* abstract *)
		кон RegisterBufferListener;

		(** Start playing / recording *)
		проц Start*;
		нач СТОП(99) (* abstract *)
		кон Start;

		(** Queue another buffer for playing / recording *)
		проц QueueBuffer*(x : Buffer);
		нач СТОП(99) (* abstract *)
		кон QueueBuffer;

		(** Pause playing / recording, no buffers are returned *)
		проц Pause*;
		нач СТОП(99) (* abstract *)
		кон Pause;

		(** Stop the playing / recording and return all buffers *)
		проц Stop*;
		нач СТОП(99) (* abstract *)
		кон Stop;

		(** The channel is closed, the driver may release any ressources reserved for it. The object is still there
			but can never be opened again*)
		проц Close*;
		нач СТОП(99) (* abstract *)
		кон Close;
	кон Channel;

	Driver* = окласс (Plugins.Plugin)
	перем
		masterIn*, masterOut* : MixerChannel;

(** Generic functions *)
		проц Init*;
		нач
			нов(masterIn); нов(masterOut)
		кон Init;

		проц Enable*;
		кон Enable;

		проц Disable*;
		кон Disable;

(** Capabilities *)
		проц NofNativeFrequencies*():цел32;
		нач СТОП(99) (* abstract *)
		кон NofNativeFrequencies;

		проц GetNativeFrequency*(nr : цел32):цел32;
		нач СТОП(99) (* abstract *)
		кон GetNativeFrequency;

		проц NofSamplingResolutions*():цел32;
		нач СТОП(99) (* abstract *)
		кон NofSamplingResolutions;

		проц GetSamplingResolution*(nr : цел32):цел32;
		нач СТОП(99) (* abstract *)
		кон GetSamplingResolution;

		(** How many different sub channel settings are possible. Default implementation returns 2 for mono and stereo *)
		проц NofSubChannelSettings*():цел32;
		нач
			возврат 2
		кон NofSubChannelSettings;

		(** Get sub channel setting nr. Default implementation returns mono and stereo *)
		проц GetSubChannelSetting*(nr : цел32):цел32;
		нач
			если nr = 0 то возврат 1
			аесли nr = 1 то возврат 2
			иначе возврат 1
			всё
		кон GetSubChannelSetting;

		(** How many different wave formats are possible. Default implementation returns 1 *)
		проц NofWaveFormats*():цел32;
		нач
			возврат 1
		кон NofWaveFormats;

		(** Get wave format nr. Default implementation returns FormatPCM *)
		проц GetWaveFormat*(nr : цел32):цел32;
		нач
			возврат FormatPCM
		кон GetWaveFormat;

(** Playing *)
		(** Open a new channel for playing. If more than one channel is opened, the sound driver needs to mix the
			channels in software or hardware, using the respective volumes. Sampling rate conversion must be done if needed.
			The driver may respond with res = ResNoMoreChannels, if it can not open more channels. (The driver
			SHOULD support more play channels (eg. 8 / 16 or more channels))
			The driver can also respond with res = ResReducedQuality if the playback quality is reduced due to insufficient
			ressources.
			channel is the resulting Play channel, NIL if an error that prevents playing has occured.
			(Applications only interested in the ability of playing and not in playback quality should only check for
			  channel # NIL and not for res = ResOk)
			samplingRate is the desired samplingRate
			samplingResolution = 8 / 16 / 24 / 32 (All drivers should support at least 8 and 16 bit)
			nofSubChannes = 1 for Mono, 2 for Stereo, 4 for Quadro etc.
			format is the wave format
		*)
		проц OpenPlayChannel*(перем channel : Channel; samplingRate, samplingResolution, nofSubChannels, format : цел32; перем res : целМЗ);
		кон OpenPlayChannel;

(** Recording *)
		(** Open a new channel for recording.
			If more than one channel is opened, the sound driver copies the recorded data to all the recording
			channels, using the respective volumes. Sampling rate conversion must be done if needed. Support for
			multichannel recording is possible but NOT required. The driver may respond with res := ResNoMoreChannels, if
			more than one recording channel is opened.
			channel is the resulting Recorder channel, NIL if an error occured.
			samplingRate is the desired samplingRate
			samplingResolution = 8 / 16 / 24 / 32 (All drivers should support at least 8 and 16 bit)
			nofSubChannes = 1 for Mono, 2 for Stereo, 4 for Quadro etc.
			format is the wave format
		*)
		проц OpenRecordChannel*(перем channel : Channel; samplingRate, samplingResolution, nofSubChannels, format : цел32; перем res : целМЗ);
		кон OpenRecordChannel;

(** Mixer *)
		(** Register a listener for channel changes,
			The number of listeners is not limited
		  *)
		проц RegisterMixerChangeListener*(mixChangedProc : MixerChangedProc);
		кон RegisterMixerChangeListener;

		(** Unregister a previously registered channel change listener  *)
		проц UnregisterMixerChangeListener*(mixChangedProc : MixerChangedProc);
		кон UnregisterMixerChangeListener;

		(** Return channel object
			channel 0 is always present and is specified as the master output volume
			channel 1 is always present and is specified as the master input volume
				Drivers may ignore channel 0 or 1 but need to return a generic "Channel" object for these channel numbers
			GetMixerChannel returns NIL if the channelNr is invalid
		*)
		проц GetMixerChannel*(channelNr : цел32; перем channel : MixerChannel);
		нач
			если channelNr = 0 то channel := masterOut
			аесли channelNr = 1 то channel := masterIn
			иначе channel := НУЛЬ
			всё
		кон GetMixerChannel;

		(** Returns the number of mixer channels available, at least 2 *)
		проц GetNofMixerChannels*() : цел32;
		нач
			возврат 2
		кон GetNofMixerChannels;
	кон Driver;

(** Returns the default sound device. (Blocks until at least one sound device is installed *)
проц GetDefaultDevice*() : Driver;
перем p : Plugins.Plugin;
нач
	p := devices.Await("");
	утв(p суть Driver);
	возврат p(Driver)
кон GetDefaultDevice;

проц Cleanup;
нач
	Plugins.main.Remove(devices)
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
	нов(devices, "SoundDevices", "Sound drivers")
кон SoundDevices.

(* Copyright (c) 2004 - 2005 DP Sisteme  2000 srl*)

модуль Shell32; (** non-portable / source: Windows.Shell32.Mod *)
	использует НИЗКОУР, Kernel32, Modules;
	конст
	(*
	#define SW_HIDE             0
	#define SW_SHOWNORMAL       1
	#define SW_NORMAL           1
	#define SW_SHOWMINIMIZED    2
	#define SW_SHOWMAXIMIZED    3
	#define SW_MAXIMIZE         3
	#define SW_SHOWNOACTIVATE   4
	#define SW_SHOW             5
	#define SW_MINIMIZE         6
	#define SW_SHOWMINNOACTIVE  7
	#define SW_SHOWNA           8
	#define SW_RESTORE          9
	#define SW_SHOWDEFAULT      10
	#define SW_FORCEMINIMIZE    11
	#define SW_MAX              11
	*)
	SWNORMAL* = 1;

    тип
    	LPCSTR* = адресВПамяти;
    	HWND* = Kernel32.HANDLE;
		HINSTANCE* = Kernel32.HINSTANCE;
		INT* =  цел32;
	перем
	shell32: Kernel32.HMODULE;

	ShellExecute-: проц {WINAPI}
	(hwnd: HWND; lpOperation, lpFile: массив из симв8; lpParameters, lpDirectory: LPCSTR;  nShowCmd: INT): HINSTANCE;


	проц TermMod;
	нач
		если shell32 # Kernel32.NULL то
			неважно Kernel32.FreeLibrary(shell32); shell32 := Kernel32.NULL
		всё
	кон TermMod;

	проц Init;
	перем str: массив 32 из симв8;
	нач
		str := "shell32.dll";
		shell32 := Kernel32.LoadLibrary(str);
		Kernel32.GetProcAddress(shell32, "ShellExecuteA", НИЗКОУР.подмениТипЗначения(адресВПамяти, ShellExecute));

		Modules.InstallTermHandler(TermMod)
	кон Init;

нач
	Init()
кон Shell32.

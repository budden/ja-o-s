модуль WMPlayer; (** AUTHOR "PL"; PURPOSE "MediaPlayer GUI"; *)

использует
	Строки8, Modules, Commands, Files, Texts, TextUtilities,
	WMGraphics, WMMessages, WMComponents, WMStandardComponents, WMWindowManager, WMTextView, WMDialogs,
	MediaPlayer;

конст
	WindowTitle = "Media Player";
	DefaultWidth = 800;
	DefaultHeight = 60;

	StyleRegular = {};
	StyleBold = {0};

	Tab = Texts.TabChar;
тип

	KillerMsg = окласс
	кон KillerMsg;

тип

	(* Small window showing information about the currently loaded content (framerate, etc. ) *)
	InfoWindow = окласс(WMComponents.FormWindow)
	перем
		data : MediaPlayer.Setup;

		tv : WMTextView.TextView;
		text : Texts.Text;
		tw : TextUtilities.TextWriter;

		next : InfoWindow;

		проц CreateForm() : WMComponents.VisualComponent;
		перем
			panel : WMStandardComponents.Panel;
			tabPositions : WMTextView.TabPositions; tabStops : WMTextView.CustomTabStops;
		нач
			нов(panel); panel.bounds.SetExtents(300, 100); panel.fillColor.Set(цел32(0FFFFFFFFH));

			нов(tv);tv.alignment.Set(WMComponents.AlignClient);
			tv.isMultiLine.Set(истина); tv.showBorder.Set(истина); tv.alwaysShowCursor.Set(ложь);
			panel.AddContent(tv);

			нов(tabPositions, 2); tabPositions[0] := 70; tabPositions[1] := 150;
			нов(tabStops, tabPositions);
			tv.SetTabStops(tabStops);
			возврат panel;
		кон CreateForm;

		проц CreateContent;
		перем framerate : вещ32; hours, minutes, seconds : цел32;
		нач
			нов(text); нов(tw, text);
			tw.SetFontStyle(StyleBold); tw.пСтроку8("Content"); tw.пСимв32_вЮ8(Tab); tw.SetFontStyle(StyleRegular);
			tw.пСтроку8(data.uri);
			tw.пВК_ПС;
			tw.пСтроку8("________________________________________________");
			tw.пВК_ПС;
			tw.SetFontStyle(StyleBold); tw.пСтроку8("Duration"); tw.пСимв32_вЮ8(Tab); tw.SetFontStyle(StyleRegular);
			ConvertTime(data.maxTime*100, hours, minutes, seconds);
			если hours # 0 то tw.пЦел64(hours, 0); tw.пСтроку8("h "); всё;
			если (hours # 0) или (minutes # 0) то tw.пЦел64(minutes, 0); tw.пСтроку8("min "); всё;
			tw.пЦел64(seconds, 0); tw.пСтроку8("s");
			tw.пВК_ПС;
			tw.SetFontStyle(StyleBold); tw.пСтроку8("Video"); tw.пСимв32_вЮ8(Tab); tw.SetFontStyle(StyleRegular);
			если data.hasVideo то
				tw.пЦел64(data.width, 0); tw.пСтроку8("x"); tw.пЦел64(data.height, 0); tw.пСтроку8(", ");
				если data.mspf # 0 то
					framerate := 1000 / data.mspf;
					tw.пЦел64(округлиВниз(framerate), 0);
					если framerate - округлиВниз(framerate) # 0 то
						tw.пСимв8("."); tw.пЦел64(округлиВниз((framerate - округлиВниз(framerate))*1000), 0);
					всё;
					tw.пСтроку8(" frames per second");
				иначе
					tw.пСтроку8("Unknown framerate");
				всё;
			иначе
				tw.пСтроку8("n/a");
			всё;
			tw.пВК_ПС;
			tw.SetFontStyle(StyleBold); tw.пСтроку8("Audio"); tw.пСимв8(9X);tw.SetFontStyle(StyleRegular);
			если data.hasAudio то
				tw.пЦел64(data.channels, 0); tw.пСтроку8(" Channel(s), "); tw.пЦел64(data.rate, 0); tw.пСтроку8("Hz, ");
				tw.пЦел64(data.bits, 0); tw.пСтроку8(" bits");
			иначе
				tw.пСтроку8("n/a");
			всё;
			tw.пВК_ПС;
			tw.ПротолкниБуферВПоток;
			tv.SetText(text);
		кон CreateContent;

		проц &New*(setupData : MediaPlayer.Setup);
		перем vc : WMComponents.VisualComponent;
		нач
			утв(setupData # НУЛЬ);
			data := setupData;
			vc := CreateForm(); CreateContent;
			Init(vc.bounds.GetWidth(), vc.bounds.GetHeight(), ложь);
			SetContent(vc);
			SetTitle(Строки8.ЯвиУСтроку("Content Info"));
			WMWindowManager.AddWindow (сам, 100, 100);
			manager := WMWindowManager.GetDefaultManager ();
			manager.SetFocus(сам);
		кон New;

	кон InfoWindow;

тип

	Control = окласс(WMComponents.VisualComponent)
	перем
		playBtn, stopBtn, pauseBtn : WMStandardComponents.Button;
		owner : Window;

		проц ButtonHandler(sender, data : динамическиТипизированныйУкль);
		нач
			если (sender = playBtn) то
				owner.ButtonHandler(owner.playBtn, НУЛЬ);
			аесли (sender = pauseBtn) то
				owner.ButtonHandler(owner.pauseBtn, НУЛЬ);
			аесли (sender = stopBtn) то
				owner.ButtonHandler(owner.stopBtn, НУЛЬ);
			всё;
		кон ButtonHandler;

		проц &New(owner : Window);
		перем panel : WMStandardComponents.Panel;
		нач
			утв(owner # НУЛЬ);
			сам.owner := owner;
			Init;

			нов(panel); panel.alignment.Set(WMComponents.AlignTop);
			panel.bounds.SetHeight(20);
			AddContent(panel);

			нов(playBtn); playBtn.alignment.Set(WMComponents.AlignLeft);
			playBtn.caption.SetAOC("Play");
			playBtn.onClick.Add(ButtonHandler);
			panel.AddContent(playBtn);

			нов(pauseBtn); pauseBtn.alignment.Set(WMComponents.AlignLeft);
			pauseBtn.caption.SetAOC("Pause");
			pauseBtn.onClick.Add(ButtonHandler);
			panel.AddContent(pauseBtn);

			нов(stopBtn); stopBtn.alignment.Set(WMComponents.AlignLeft);
			stopBtn.caption.SetAOC("Stop");
			stopBtn.onClick.Add(ButtonHandler);
			panel.AddContent(stopBtn);
		кон New;

	кон Control;

тип

	Window* = окласс (WMComponents.FormWindow)
	перем
		playBtn, stopBtn, pauseBtn, ffBtn, rewBtn, infoBtn : WMStandardComponents.Button;
		search: WMStandardComponents.Scrollbar;

		timeLbl, totTimeLbl: WMStandardComponents.Label;

		player : MediaPlayer.Player;
		stepSize : цел32;

		(* Info about currently loaded content *)
		filename : Files.FileName;
		setupData : MediaPlayer.Setup;
		infos : InfoWindow;

		windowInfo : WMWindowManager.WindowInfo;

		проц CreateForm(): WMComponents.VisualComponent;
		перем
			panel, toolbar : WMStandardComponents.Panel;
			label: WMStandardComponents.Label;
		нач
			нов(panel); panel.bounds.SetExtents(DefaultWidth, DefaultHeight); panel.fillColor.Set(цел32(0FFFFFFFFH)); panel.takesFocus.Set(истина);

			(* Search / position scrollbar *)
			нов(toolbar); toolbar.fillColor.Set(цел32(0FFFFFFFFH)); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);

			нов(search); search.alignment.Set(WMComponents.AlignClient);
			search.vertical.Set(ложь); search.max.Set(1000); search.pos.Set(0);
			toolbar.AddContent(search);

			(* Buttons *)
			нов(toolbar); toolbar.fillColor.Set(цел32(0A0A0A0FFH)); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);

			нов(playBtn); playBtn.alignment.Set(WMComponents.AlignLeft); playBtn.caption.SetAOC("Play");
			toolbar.AddContent(playBtn);

			нов(pauseBtn); pauseBtn.alignment.Set(WMComponents.AlignLeft); pauseBtn.caption.SetAOC("Pause");
			toolbar.AddContent(pauseBtn);

			нов(stopBtn); stopBtn.alignment.Set(WMComponents.AlignLeft); stopBtn.caption.SetAOC("Stop");
			toolbar.AddContent(stopBtn);

			нов(rewBtn); rewBtn.alignment.Set(WMComponents.AlignLeft); rewBtn.caption.SetAOC("<<");
			toolbar.AddContent(rewBtn); rewBtn.isRepeating.Set(истина);

			нов(ffBtn); ffBtn.alignment.Set(WMComponents.AlignLeft); ffBtn.caption.SetAOC(">>");
			toolbar.AddContent(ffBtn); ffBtn.isRepeating.Set(истина);

			нов(infoBtn); infoBtn.alignment.Set(WMComponents.AlignRight); infoBtn.caption.SetAOC("Info");
			toolbar.AddContent(infoBtn);

			(* Info Labels *)
			нов(label); label.bounds.SetWidth(100); label.alignment.Set(WMComponents.AlignLeft);
			panel.AddContent(label); label.caption.SetAOC(" Elapsed Time: ");

			нов(timeLbl); timeLbl.bounds.SetWidth(100); timeLbl.alignment.Set(WMComponents.AlignLeft);
			panel.AddContent(timeLbl); timeLbl.caption.SetAOC("00:00:00");

			нов(label); label.bounds.SetWidth(100); label.alignment.Set(WMComponents.AlignLeft);
			panel.AddContent(label); label.caption.SetAOC("Total Time: ");

			нов(totTimeLbl); totTimeLbl.bounds.SetWidth(100); totTimeLbl.alignment.Set(WMComponents.AlignLeft);
			panel.AddContent(totTimeLbl); timeLbl.caption.SetAOC("00:00:00");

			возврат panel;
		кон CreateForm;

		проц &New*;
		перем vc : WMComponents.VisualComponent;
		нач
			нов(player);
			player.setup := SetUpController;
			player.update := Update;
			player.console := ложь;

			IncCount();
			vc := CreateForm();

			playBtn.onClick.Add(ButtonHandler);
			stopBtn.onClick.Add(ButtonHandler);
			pauseBtn.onClick.Add(ButtonHandler);
			ffBtn.onClick.Add(ButtonHandler);
			rewBtn.onClick.Add(ButtonHandler);
			infoBtn.onClick.Add(ButtonHandler);
			search.onPositionChanged.Add(SliderSearch);

			Init(vc.bounds.GetWidth(), vc.bounds.GetHeight(), ложь);
			SetContent(vc);
			SetTitle(Строки8.ЯвиУСтроку(WindowTitle));
			SetIcon(WMGraphics.LoadImage("WMIcons.tar://WMPlayer.png", истина));

			WMWindowManager.ClearInfo(windowInfo);
			windowInfo.vc.width := 100;
			windowInfo.vc.height := 20;
			windowInfo.vc.generator := сам.GenerateControl;

			WMWindowManager.ExtAddWindow(сам, 100, 30, {WMWindowManager.FlagFrame, WMWindowManager.FlagStayOnTop, WMWindowManager.FlagClose, WMWindowManager.FlagMinimize});
			manager := WMWindowManager.GetDefaultManager();
			manager.SetFocus(сам);
		кон New;

		проц GenerateControl*() : динамическиТипизированныйУкль;
		перем control : Control;
		нач
			нов(control, сам);
			возврат control;
		кон GenerateControl;

		проц Open*(конст fileName : массив из симв8);
		перем res : целМЗ; msg : массив 256 из симв8;
		нач
			копируйСтрокуДо0(fileName, filename);
			player.Open(filename, msg, res);
			если res # MediaPlayer.Ok то
				WMDialogs.Error(WindowTitle, msg);
				SetTitle(Строки8.ЯвиУСтроку("Media Player"));
				копируйСтрокуДо0("", windowInfo.openDocuments[0].name);
			иначе
				msg := "Media Player - "; Строки8.ПодклейВСтрокуХвост(msg, fileName);
				SetTitle(Строки8.ЯвиУСтроку(msg));
				копируйСтрокуДо0(filename, windowInfo.openDocuments[0].name);
			всё;
			SetInfo(windowInfo);
		кон Open;

		проц ButtonHandler(sender, data : динамическиТипизированныйУкль);
		перем b : WMStandardComponents.Button; info : InfoWindow;
		нач
			если ~IsCallFromSequencer() то
				sequencer.ScheduleEvent(сам.ButtonHandler, sender, data);
			иначе
				если (sender # НУЛЬ) и (sender суть WMStandardComponents.Button) то
					b := sender (WMStandardComponents.Button);
					если b = playBtn то player.Play;
					аесли b = stopBtn то player.Stop;
					аесли b = pauseBtn то player.Pause;
					аесли b = ffBtn то player.SetPos(player.GetPos() + stepSize);
					аесли b = rewBtn то player.SetPos(player.GetPos() - 2*stepSize);
					аесли b = infoBtn то
						если setupData # НУЛЬ то
							нов(info, setupData);
							нач {единолично} info.next := infos; infos := info; кон;
						иначе
							WMDialogs.Error(WindowTitle, "No content loaded.");
						всё;
					всё;
				всё;
			всё;
		кон ButtonHandler;

		проц {перекрыта}Close*;
		нач
			player.Close;
			(* Close all open InfoWindow if any *)
			нцПока infos # НУЛЬ делай infos.Close; infos := infos.next; кц;
			Close^;
			DecCount;
		кон Close;

		проц {перекрыта}Handle*(перем x: WMMessages.Message);
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) и (x.ext суть KillerMsg) то Close
			иначе Handle^(x)
			всё;
		кон Handle;

		проц SliderSearch(sender, data: динамическиТипизированныйУкль);
		перем pos: размерМЗ;
		нач
			pos := search.pos.Get();
			player.SetPos(pos(цел32))
		кон SliderSearch;

		(* Update the Info Labels *)
		проц Update*(status, pos, maxpos, displayTime: цел32);
		перем temp : массив 16 из симв8;
		нач
			ConvertTimeToStr(displayTime * 100, temp);
			timeLbl.caption.SetAOC(temp);
			если (setupData # НУЛЬ) и (setupData.hasVideo) то
				search.pos.Set(pos);
			иначе
				search.pos.Set(displayTime);
			всё;
			если (pos # 0) и (displayTime # 0) то stepSize := 10* pos DIV displayTime всё
		кон Update;

		(* Init the GUI with infos from the Decoder *)
		проц SetUpController*(setup : MediaPlayer.Setup);
		перем temp : массив 16 из симв8;
		нач
			setupData := setup;
			если setup.canSeek то
				search.visible.Set(истина);
				если setup.hasVideo то
					search.max.Set(setup.maxFrames);
					search.pageSize.Set(setup.maxFrames DIV 25);
				иначе
					search.max.Set(setup.maxTime);
					search.pageSize.Set(setup.maxTime DIV 100);
				всё;
			иначе
				search.visible.Set(ложь)
			всё;
			ConvertTimeToStr(setup.maxTime * 100, temp);
			totTimeLbl.caption.SetAOC(temp)
		кон SetUpController;

	кон Window;

перем
	nofWindows : цел32;

(* Convert milliseconds to hours:minutes:seconds *)
проц ConvertTime(ms : цел32; перем hours, minutes, seconds : цел32);
конст Hour = 60*60*1000; Minute = 60*1000;
перем rest : цел32;
нач
	hours := ms DIV Hour; rest := ms остОтДеленияНа Hour;
	minutes := rest DIV Minute; rest := rest остОтДеленияНа Minute;
	seconds := rest DIV 1000;
кон ConvertTime;

(* Convert time in millisecond into string of the form "hh:mm:ss" *)
проц ConvertTimeToStr(ms : цел32; перем timeStr : массив из симв8);
перем hour, minute, second : цел32; nbr : массив 4 из симв8;

	(* Append 'number' to 'string' using 2 or more digits *)
	проц Append(перем string : массив из симв8; number : цел32);
	нач
		Строки8.ПишиЦел64_вСтроку(number, nbr);
		если number >= 10 то Строки8.ПодклейВСтрокуХвост(string, nbr);
		иначе Строки8.ПодклейВСтрокуХвост(string, "0"); Строки8.ПодклейВСтрокуХвост(string, nbr);
		всё;
	кон Append;

нач
	ConvertTime(ms, hour, minute, second);
	timeStr := "";
	Append(timeStr, hour); Строки8.ПодклейВСтрокуХвост(timeStr, ":");
	Append(timeStr, minute); Строки8.ПодклейВСтрокуХвост(timeStr, ":");
	Append(timeStr, second);
кон ConvertTimeToStr;

проц Open*(context : Commands.Context); (** filename ~ *)
перем inst : Window; fileName: Files.FileName;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fileName) то
		нов(inst);
		inst.Open(fileName);
	всё
кон Open;

проц IncCount;
нач {единолично}
	увел(nofWindows);
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows);
кон DecCount;

проц Cleanup;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WMWindowManager.WindowManager;
нач {единолично}
	нов(die); msg.ext := die; msg.msgType := WMMessages.MsgExt;
	m := WMWindowManager.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0)
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
кон WMPlayer.

------------------------------------------------------------------------------
System.Free WMPlayer ~
WMPlayer.Open track.mp3~
WMPlayer.Open test.wav~
WMPlayer.Open FAT:tndx.avi~
WMPlayer.Open flags.avi~

WMPlayer.Open Filme:/test.wav ~
WMPlayer.Open Filme:/test2.wav ~
WMPlayer.Open Filme:/test.mp3 ~

MediaPlayer.Open Filme:/test.mp3 ~

MediaPlayer.Close ~


(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль MathMitLef;   (** AUTHOR "adf"; PURPOSE "Mittag-Leffler function"; *)

(** The Mittag-Leffler function is the characteristic solution to fractional-order differential equations,
	like the exponential function is the characteristic solution to ordinary differential equations. *)

использует NbrInt, NbrRe, NbrCplx, DataErrors, MathRe, MathGamma, MathCplx, MathCplxSeries, CalcGauss;

перем
	maxIterations: NbrInt.Integer;  storeAlpha, storeBeta, tolerance: NbrRe.Real;
	yk: массив 2 из NbrRe.Real;
	yp: массив 3 из NbrRe.Real;

тип
	MitLef = окласс (MathCplxSeries.Coefficient)
		(* Series solution for the Mittag-Leffler function.  Only apply this as a solution around 0. *)

		проц {перекрыта}Evaluate*;
		перем x: NbrRe.Real;
		нач
			x := storeBeta + n * storeAlpha;
			если GammaIsSingularAt( x ) то coef := 0 иначе coef := 1 / MathGamma.Fn( x ) всё;
			если n = maxIterations то eos := истина;  DataErrors.ReWarning( x, "Did not converge -  timed out." ) всё
		кон Evaluate;

	кон MitLef;

	AsympMitLef = окласс (MathCplxSeries.Coefficient)
		(* Series solution for the Mittag-Leffler function.  Only apply this as a solution for large x. *)

		проц {перекрыта}Evaluate*;
		перем x: NbrRe.Real;
		нач
			x := storeBeta - n * storeAlpha;
			если GammaIsSingularAt( x ) то coef := 0 иначе coef := 1 / MathGamma.Fn( x ) всё;
			если n = maxIterations то eos := истина;  DataErrors.ReWarning( x, "Did not converge -  timed out." ) всё
		кон Evaluate;

	кон AsympMitLef;

	DMitLef = окласс (MathCplxSeries.Coefficient)
		(* Series solution for the derivative of the Mittag-Leffler function.  Only apply this as a solution around 0. *)

		проц {перекрыта}Evaluate*;
		перем x: NbrRe.Real;
		нач
			x := (1 + n) * storeAlpha + storeBeta;
			если GammaIsSingularAt( x ) то coef := 0 иначе coef := (1 + n) / MathGamma.Fn( x ) всё;
			если n = maxIterations то eos := истина;  DataErrors.ReWarning( x, "Did not converge -  timed out." ) всё
		кон Evaluate;

	кон DMitLef;

	проц K( x: NbrRe.Real;  z: NbrCplx.Complex ): NbrCplx.Complex;
	(* Use the mappings:  x , c,  yk[0] , a,  yk[1] , b,  z , z.  *)
	перем coef1, coef2, r1: NbrRe.Real;  answer, denom, numer: NbrCplx.Complex;
	нач
		coef1 := MathRe.Power( x, (1 - yk[1]) / yk[0] ) / (NbrRe.Pi * yk[0]);
		coef2 := MathRe.Exp( -MathRe.Power( x, 1 / yk[0] ) );  r1 := NbrRe.Pi * (1 - yk[1]);
		numer := x * MathRe.Sin( r1 ) - z * MathRe.Sin( r1 + NbrRe.Pi * yk[0] );
		denom := x * x - 2 * x * z * MathRe.Cos( NbrRe.Pi * yk[0] ) + z * z;  answer := coef1 * coef2 * numer / denom;
		возврат answer
	кон K;

	проц P( x: NbrRe.Real;  z: NbrCplx.Complex ): NbrCplx.Complex;
	(* Use the mappings:  x , f,  yp[0] , a,  yp[1] , b,  yp[2] , e,  z , z. *)
	перем coef1, coef2, r1: NbrRe.Real;  answer, denom, numer: NbrCplx.Complex;
	нач
		coef1 := MathRe.Power( yp[2], 1 + (1 - yp[1]) / yp[0] ) / (2 * NbrRe.Pi * yp[0]);
		coef2 := MathRe.Exp( MathRe.Power( yp[2], 1 / yp[0] ) * MathRe.Cos( x / yp[0] ) );
		r1 := x * (1 + (1 - yp[1]) / yp[0]) + MathRe.Power( yp[2], 1 / yp[0] ) * MathRe.Sin( x / yp[0] );
		numer := MathRe.Cos( r1 ) + NbrCplx.I * MathRe.Sin( r1 );  denom := yp[2] * MathCplx.Exp( NbrCplx.I * x ) - z;
		answer := coef1 * coef2 * numer / denom;  возврат answer
	кон P;

	проц GammaIsSingularAt( x: NbrRe.Real ): булево;
	нач
		если x > 0 то возврат ложь
		аесли x < 0 то
			если x = NbrRe.Int( x ) + 1 то возврат истина иначе возврат ложь всё
		иначе возврат истина
		всё
	кон GammaIsSingularAt;

(** The Mittag-Leffler function,  Ea,b(z) = ek=0% zk/G(ak+b), a, b N R, a > 0,  z N C.  *)
	проц Fn*( alpha, beta, x: NbrRe.Real ): NbrRe.Real;
	перем abs, answer, arg: NbrRe.Real;  ml, z: NbrCplx.Complex;
	нач
		z := x;  ml := CplxFn( alpha, beta, z );  abs := NbrCplx.Abs( ml );  arg := NbrCplx.Arg( ml );
		если (arg < tolerance) и (-tolerance < arg) то answer := abs
		аесли (arg > NbrRe.Pi - tolerance) или (arg < -NbrRe.Pi + tolerance) то answer := -abs
		иначе DataErrors.Error( "The result is complex.  Use CplxFn instead." )
		всё;
		возврат answer
	кон Fn;

	проц CplxFn*( alpha, beta: NbrRe.Real;  z: NbrCplx.Complex ): NbrCplx.Complex;
	перем i1, i2, k, k0, res: NbrInt.Integer;  a, absZ, argZ, b, r1, r2, x0: NbrRe.Real;
		answer, c1, c2: NbrCplx.Complex;  ml: MitLef;  aml: AsympMitLef;
	нач
		(* Implements the algorithm of R. Gorenflo, I. Loutchko and Yu. Luchko, "Computation of the Mittag-Leffler
			function  Ea,b(z)  and its derivatives," Fractional Calculus and Applied Analysis, 5 (2002), 491-518.. *)
		alpha := NbrRe.Abs( alpha );  absZ := NbrCplx.Abs( z );  argZ := NbrCplx.Arg( z );
		если absZ < 1 / NbrRe.MaxNbr то
			если GammaIsSingularAt( beta ) то answer := 0 иначе answer := 1 / MathGamma.Fn( beta ) всё
		аесли (alpha = 1) и (beta = 1) то
			answer := MathCplx.Exp( z )
		аесли 1 < alpha то
			k0 := NbrRe.Floor( alpha ) + 1;  c1 := (2 * NbrRe.Pi / k0) * NbrCplx.I;  r1 := 1 / k0;  r2 := alpha / k0;  answer := 0;
			нцДля k := 0 до k0 - 1 делай
				c2 := MathCplx.RealPower( z, r1 ) * MathCplx.Exp( c1 * k );  answer := answer + CplxFn( r2, beta, c2 )
			кц;
			answer := answer / k0
		иначе
			если absZ < 0.9 то нач {единолично}
					storeAlpha := alpha;  storeBeta := beta;  i1 := NbrRe.Ceiling( (1 - beta) / alpha );
					i2 := NbrRe.Ceiling( MathRe.Ln( NbrRe.Epsilon * (1 - absZ) ) / MathRe.Ln( absZ ) );
					maxIterations := NbrInt.Max( i1, i2 );  нов( ml );  answer := MathCplxSeries.PowerSeries( ml, z ) кон
			аесли absZ < NbrRe.Floor( 10 + 5 * alpha ) то
				если beta >= 0 то
					r1 := 1;  x0 := NbrRe.Max( r1, 2 * absZ );
					x0 := NbrRe.Max( x0, MathRe.Power( -MathRe.Ln( NbrRe.Epsilon * NbrRe.Pi / 6 ), alpha ) )
				иначе
					r1 := NbrRe.Abs( beta );  r2 := MathRe.Power( 1 + r1, alpha );  x0 := NbrRe.Max( r2, 2 * absZ );
					r2 := 6 * (2 + r1) * MathRe.Power( 2 * r1, r1 );
					x0 := NbrRe.Max( x0, MathRe.Power( -2 * MathRe.Ln( NbrRe.Epsilon * NbrRe.Pi / r2 ), alpha ) )
				всё;
				если (NbrRe.Abs( argZ ) > alpha * NbrRe.Pi) и
					(NbrRe.Abs(NbrRe.Abs( argZ ) - alpha * NbrRe.Pi) > NbrRe.Epsilon) то
					если beta < 1 + alpha то
						нач {единолично}
							a := 0;  b := x0;  yk[0] := alpha;  yk[1] := beta;
							answer := CalcGauss.SolveCplx( K, a, b, z, CalcGauss.Medium, tolerance, res ) кон;
						если res # CalcGauss.OKay то
							если res = CalcGauss.MaxSubDivReached то
								DataErrors.Warning( "Maximum subdivisions reached when integrating K." )
							аесли res = CalcGauss.RoundoffError то
								DataErrors.Warning( "Excessive roundoff error occurred when integrating K." )
							аесли res = CalcGauss.RoughIntegrand то
								DataErrors.Warning( "A rough integrand encountered when integrating K." )
							иначе DataErrors.Error( "Unknown error originating from CalcGauss.SolveCplx." )
							всё
						всё
					иначе
						нач {единолично}
							a := 1;  b := x0;  yk[0] := alpha;  yk[1] := beta;
							answer := CalcGauss.SolveCplx( K, a, b, z, CalcGauss.Medium, tolerance, res ) кон;
						если res # CalcGauss.OKay то
							если res = CalcGauss.MaxSubDivReached то
								DataErrors.Warning( "Maximum subdivisions reached when integrating K." )
							аесли res = CalcGauss.RoundoffError то
								DataErrors.Warning( "Excessive roundoff error occurred when integrating K." )
							аесли res = CalcGauss.RoughIntegrand то
								DataErrors.Warning( "A rough integrand encountered when integrating K." )
							иначе DataErrors.Error( "Unknown error originating from CalcGauss.SolveCplx." )
							всё
						всё;  нач {единолично}
							a := -alpha * NbrRe.Pi;  b := -a;  yp[0] := alpha;  yp[1] := beta;  yp[2] := 1;
							answer := answer + CalcGauss.SolveCplx( P, a, b, z, CalcGauss.Fine, tolerance, res ) кон;
						если res # CalcGauss.OKay то
							если res = CalcGauss.MaxSubDivReached то
								DataErrors.Warning( "Maximum subdivisions reached when integrating P." )
							аесли res = CalcGauss.RoundoffError то
								DataErrors.Warning( "Excessive roundoff error occurred when integrating P." )
							аесли res = CalcGauss.RoughIntegrand то
								DataErrors.Warning( "A rough integrand encountered when integrating P." )
							иначе DataErrors.Error( "Unknown error originating from CalcGauss.SolveCplx." )
							всё
						всё
					всё
				аесли (NbrRe.Abs( argZ ) < alpha * NbrRe.Pi) и
					(NbrRe.Abs(NbrRe.Abs( argZ ) - alpha * NbrRe.Pi) > NbrRe.Epsilon) то
					если beta < 1 + alpha то
						нач {единолично}
							a := 0;  b := x0;  yk[0] := alpha;  yk[1] := beta;
							answer := CalcGauss.SolveCplx( K, a, b, z, CalcGauss.Medium, tolerance, res ) кон;
						если res # CalcGauss.OKay то
							если res = CalcGauss.MaxSubDivReached то
								DataErrors.Warning( "Maximum subdivisions reached when integrating K." )
							аесли res = CalcGauss.RoundoffError то
								DataErrors.Warning( "Excessive roundoff error occurred when integrating K." )
							аесли res = CalcGauss.RoughIntegrand то
								DataErrors.Warning( "A rough integrand encountered when integrating K." )
							иначе DataErrors.Error( "Unknown error originating from CalcGauss.SolveCplx." )
							всё
						всё
					иначе
						нач {единолично}
							a := absZ / 2;  b := x0;  yk[0] := alpha;  yk[1] := beta;
							answer := CalcGauss.SolveCplx( K, a, b, z, CalcGauss.Medium, tolerance, res ) кон;
						если res # CalcGauss.OKay то
							если res = CalcGauss.MaxSubDivReached то
								DataErrors.Warning( "Maximum subdivisions reached when integrating K." )
							аесли res = CalcGauss.RoundoffError то
								DataErrors.Warning( "Excessive roundoff error occurred when integrating K." )
							аесли res = CalcGauss.RoughIntegrand то
								DataErrors.Warning( "A rough integrand encountered when integrating K." )
							иначе DataErrors.Error( "Unknown error originating from CalcGauss.SolveCplx." )
							всё
						всё;  нач {единолично}
							a := -alpha * NbrRe.Pi;  b := -a;  yp[0] := alpha;  yp[1] := beta;  yp[2] := absZ / 2;
							answer := answer + CalcGauss.SolveCplx( P, a, b, z, CalcGauss.Fine, tolerance, res ) кон;
						если res # CalcGauss.OKay то
							если res = CalcGauss.MaxSubDivReached то
								DataErrors.Warning( "Maximum subdivisions reached when integrating P." )
							аесли res = CalcGauss.RoundoffError то
								DataErrors.Warning( "Excessive roundoff error occurred when integrating P." )
							аесли res = CalcGauss.RoughIntegrand то
								DataErrors.Warning( "A rough integrand encountered when integrating P." )
							иначе DataErrors.Error( "Unknown error originating from CalcGauss.SolveCplx." )
							всё
						всё
					всё;
					answer :=
						answer +
							MathCplx.RealPower( z, (1 - beta) / alpha ) *
								MathCplx.Exp( MathCplx.RealPower( z, 1 / alpha ) ) / alpha
				иначе
					нач {единолично}
						a := (absZ + 1) / 2;  b := x0;  yk[0] := alpha;  yk[1] := beta;
						answer := CalcGauss.SolveCplx( K, a, b, z, CalcGauss.Medium, tolerance, res ) кон;
					если res # CalcGauss.OKay то
						если res = CalcGauss.MaxSubDivReached то
							DataErrors.Warning( "Maximum subdivisions reached when integrating K." )
						аесли res = CalcGauss.RoundoffError то
							DataErrors.Warning( "Excessive roundoff error occurred when integrating K." )
						аесли res = CalcGauss.RoughIntegrand то
							DataErrors.Warning( "A rough integrand encountered when integrating K." )
						иначе DataErrors.Error( "Unknown error originating from CalcGauss.SolveCplx." )
						всё
					всё;  нач {единолично}
						a := -alpha * NbrRe.Pi;  b := -a;  yp[0] := alpha;  yp[1] := beta;  yp[2] := (absZ + 1) / 2;
						answer := answer + CalcGauss.SolveCplx( P, a, b, z, CalcGauss.Fine, tolerance, res ) кон;
					если res # CalcGauss.OKay то
						если res = CalcGauss.MaxSubDivReached то
							DataErrors.Warning( "Maximum subdivisions reached when integrating P." )
						аесли res = CalcGauss.RoundoffError то
							DataErrors.Warning( "Excessive roundoff error occurred when integrating P." )
						аесли res = CalcGauss.RoughIntegrand то
							DataErrors.Warning( "A rough integrand encountered when integrating P." )
						иначе DataErrors.Error( "Unknown error originating from CalcGauss.SolveCplx." )
						всё
					всё
				всё
			иначе  (* absZ > NbrRe.Floor( 10 + 5*alpha ) *)
				если argZ < 3 * alpha * NbrRe.Pi / 4 то
					answer :=
						MathCplx.RealPower( z, (1 - beta) / alpha ) *
							MathCplx.Exp( MathCplx.RealPower( z, 1 / alpha ) ) / alpha
				иначе answer := 0
				всё;  нач {единолично}
					storeAlpha := alpha;  storeBeta := beta;
					(* Factor of 2 put in for 32-bit precision - needed to assure convergence. *)
					maxIterations := 2 * NbrRe.Floor( -MathRe.Ln( NbrRe.Epsilon ) / MathRe.Ln( absZ ) );  нов( aml );
					answer := answer - MathCplxSeries.PowerSeries( aml, 1 / z );
					(* Remove the zeroth term from the sum, because the actual starts at one. *)
					если ~GammaIsSingularAt( beta ) то answer := answer + 1 / MathGamma.Fn( beta ) всё кон
			всё
		всё;
		возврат answer
	кон CplxFn;

(** The derivative of the Mittag-Leffler function,  dEa,b(z)/dz = ek=0% (1+k)zk/G(a(1+k)+b).  *)
	проц DFn*( alpha, beta, x: NbrRe.Real ): NbrRe.Real;
	перем abs, answer, arg: NbrRe.Real;  dml, z: NbrCplx.Complex;
	нач
		z := x;  dml := DCplxFn( alpha, beta, z );  abs := NbrCplx.Abs( dml );  arg := NbrCplx.Arg( dml );
		если (arg < tolerance) и (-tolerance < arg) то answer := abs
		аесли (arg > NbrRe.Pi - tolerance) или (arg < -NbrRe.Pi + tolerance) то answer := -abs
		иначе DataErrors.Error( "The result is complex.  Use DCplxFn instead." )
		всё;
		возврат answer
	кон DFn;

	проц DCplxFn*( alpha, beta: NbrRe.Real;  z: NbrCplx.Complex ): NbrCplx.Complex;
	перем answer: NbrCplx.Complex;  absZ, d, k, k1, omega: NbrRe.Real;  k0: NbrInt.Integer;  dml: DMitLef;
	нач
		alpha := NbrRe.Abs( alpha );  absZ := NbrCplx.Abs( z );
		если absZ < 1 / NbrRe.MaxNbr то
			если GammaIsSingularAt( alpha + beta ) то answer := 0 иначе answer := 1 / MathGamma.Fn( alpha + beta ) всё
		аесли absZ < 0.9 то
			если alpha > 1 то k1 := 1 + (2 - alpha - beta) / (alpha - 1)
			иначе
				d := 1 + alpha * (alpha - 4 * beta + 6);  k := 1 + (3 - alpha - beta) / alpha;
				если d <= 1 то k1 := k
				иначе
					omega := alpha + beta - 1.5;
					k1 := NbrRe.Max( k, 1 + (1 - 2 * omega * alpha + MathRe.Sqrt( d )) / (2 * alpha * alpha) )
				всё
			всё;  нач {единолично}
				k0 := NbrRe.Ceiling( NbrRe.Max( k1, MathRe.Ln( NbrRe.Epsilon * (1 - absZ) ) ) / MathRe.Ln( absZ ) );
				maxIterations := k0;  storeAlpha := alpha;  storeBeta := beta;  нов( dml );
				answer := MathCplxSeries.PowerSeries( dml, z ) кон
		иначе
			answer := CplxFn( alpha, beta - 1, z );
			если beta # 1 то answer := answer - (beta - 1) * CplxFn( alpha, beta, z ) всё;
			answer := answer / (alpha * z)
		всё;
		возврат answer
	кон DCplxFn;

нач
	tolerance := MathRe.Sqrt( NbrRe.Epsilon )
кон MathMitLef.

модуль CSS2Scanner;	(** Stefan Walthert  *)
(** AUTHOR "swalthert"; PURPOSE ""; *)

использует
	ЛогЯдра, Строки8, Потоки, Files, DynamicStrings;

конст
	(** Scanner: Tokens *)
	Null = -2;
	Invalid* = -1;
	Ident* = 0;
	AtKeyword* = 1;	(** '@'ident *)
	String* = 2;	(** '"'chars'"' | "'"chars"'" *)
	Hash* = 3;	(** '#'name *)
	Important* = 4;	(** '!important' *)
	Number* = 5;	(** number (cf. Scanner.numType) *)
	Percentage* = 6;	(** num'%' *)
	Dimension* = 7;	(** num ident *)
	URI* = 8;	(** 'url('string')' | 'url('chars')' *)
	Function* = 9;	(** ident'(' *)
	UnicodeRange* = 10;	(**  *)
	Cdo* = 11;	(** '<!--' *)
	Cdc* = 12;	(** '-->' *)
	Slash* = 13;	(** '/' *)
	Comma* = 14;	(** ',' *)
	Greater* = 15;	(** '>' *)
	Plus* = 16;	(** '+' *)
	Minus* = 17;	(** '-' *)
	Asterisk* = 18;	(** '*' *)
	Semicolon* = 19;	(** ';' *)
	Colon* = 20;	(** ':' *)
	Dot* = 21;	(** '.' *)
	BracketOpen* = 22;	(** '[' *)
	BracketClose* = 23;	(** ']' *)
	ParenOpen* = 24;	(** '(' *)
	ParenClose* = 25;	(** ')' *)
	BraceOpen* = 26;	(** '{' *)
	BraceClose* = 27;	(** '}' *)
	Equal* = 28;	(** '=' *)
	Includes* = 29;	(** '~=' *)
	Dashmatch* = 30;	(** '|=' *)
	Eof* = 31;	(**  *)

	(** real or integer number *)
	Undefined* = 0;
	Integer* = 1;	(** integer number *)
	Real* = 2;	(** real number *)

тип
		Scanner* = окласс
		перем
			sym-: цел32;
			numberType-: цел8;
			intVal-: цел32;
			realVal-: вещ64;
			line-, row-, pos: цел32;
			reportError*: проц (pos, line, row: цел32; msg: массив из симв8);
			nextCh: симв8;
			dynstr: DynamicStrings.DynamicString;
			f: Files.File;
			r: Files.Reader;

		проц & Init*(f: Files.File);
		нач
			если f = НУЛЬ то
				sym := Invalid
			иначе
				reportError := DefaultReportError;
				sym := Null; numberType := Undefined; intVal := 0; realVal := 0.0;
				line := 1; row := 1;
				нов(dynstr);
				сам.f := f;
				Files.OpenReader(r, f, 0); pos := 0;
				NextCh()
			всё
		кон Init;

		проц Error(msg: массив из симв8);
		нач
			reportError(GetPos(), line, row, msg)
		кон Error;

		проц NextCh;
		нач
			если (nextCh = DynamicStrings.CR) (* OR (nextCh = Strings.LF) *) то увел(line); row := 1
			иначе увел(row)
			всё;
			если r.кодВозвратаПоследнейОперации # Потоки.Успех то
				nextCh := 0X; sym := Eof
			иначе
				r.чСимв8(nextCh); увел(pos)
			всё
		кон NextCh;

		проц SkipWhiteSpace;
		нач
			нцПока IsWhiteSpace(nextCh) делай
				NextCh()
			кц
		кон SkipWhiteSpace;

		проц ScanComment;
		нач
			нц
				NextCh();
				нцПока (nextCh # '*') и (sym # Eof) делай
					NextCh()
				кц;
				если nextCh = '*' то
					NextCh();
					если nextCh = '/' то
						NextCh(); прервиЦикл
					всё
				аесли sym = Eof то
					Error("unclosed comment")
				всё
			кц
		кон ScanComment;

		проц ScanEscape(isString: булево; перем i: цел32);
		перем val: цел32; n: цел8; hexstr: массив 7 из симв8; newline: булево;
		нач
			newline := ложь;
			NextCh();
			если IsDigit(nextCh) или (('a' <= nextCh) и (nextCh <= 'f')) или (('A' <= nextCh) и (nextCh <= 'F')) то (* hexadecimal digit *)
				n := 0;
(*				WHILE ~IsWhiteSpace(nextCh) & (n < 6) DO	*)
				нцПока (IsDigit(nextCh) или (('a' <= nextCh) и (nextCh <= 'f')) или (('A' <= nextCh) и (nextCh <= 'F'))) и (n < 6) делай
					hexstr[n] := nextCh; NextCh(); увел(n)
				кц;
				hexstr[n] := 0X;
				HexStrToInt(hexstr, val);
				если IsWhiteSpace(nextCh) и (n # 6) то NextCh() всё;	(* skip space after escape digits (if less than 6 digits) *)
			иначе
				val := кодСимв8(nextCh);
				если (nextCh = 0AX) или (nextCh = 0DX) то newline := истина всё;
				NextCh()
			всё;
			(* INC(i, number of bytes needed to write unicode value val as a UTF8 character); *)
			если ~isString или ~newline то
				(* compute UTF8 characters out of 'val', put them to dynstr *)
			всё
		кон ScanEscape;

		проц ScanIdent;
		перем i: цел32;
		нач
			если IsNmChar(nextCh) то
				i := 0;
				если IsEscape(nextCh) то
					ScanEscape(ложь, i)
				иначе
					dynstr.Put(nextCh, 0); увел(i);
					NextCh()
				всё;
				нцПока IsNmChar(nextCh) делай
					если IsEscape(nextCh) то
						ScanEscape(ложь, i)
					иначе
						dynstr.Put(nextCh, i); увел(i);
						NextCh()
					всё
				кц;
				dynstr.Put(0X, i); sym := Ident
			иначе
				Error("{nmstart} expected")
			всё
		кон ScanIdent;

		проц ScanName;
		перем i: цел32;
		нач
			i := 0;
			нцПока IsNmChar(nextCh) делай
				если IsEscape(nextCh) то
					ScanEscape(ложь, i)
				иначе
					dynstr.Put(nextCh, i); увел(i);
					NextCh()
				всё
			кц;
			dynstr.Put(0X, i); sym := Ident
		кон ScanName;

		проц ScanString;
		перем i: цел32; ch, allowedQuote: симв8;
		нач
			ch := nextCh;
			если ch = '"' то allowedQuote := "'"
			аесли ch = "'" то allowedQuote := '"'
			иначе Error("quote expected")
			всё;
			NextCh();
			i := 0;
			нцПока ((nextCh = 9X) или (nextCh = ' ') или (nextCh = '!') или (('#' <= nextCh) и (nextCh <= '&'))
					или (('(' <= nextCh) и (nextCh <= '~')) или (nextCh = allowedQuote)
					или IsNonAscii(nextCh) или IsEscape(nextCh)) и (sym # Eof) делай
				если IsEscape(nextCh) то
					ScanEscape(истина, i)
				иначе
					dynstr.Put(nextCh, i); NextCh(); увел(i)
				всё;
			кц;
			если nextCh # ch то Error("quote expected") всё;
			dynstr.Put(0X, i);
			NextCh()
		кон ScanString;

		проц ScanURL;
		перем i : цел32;
		нач
			i := 0;
			нцПока ((nextCh = '!') или (('#' <= nextCh) и (nextCh <= '&')) или (('*' <= nextCh) и (nextCh <= '~'))
					или IsNonAscii(nextCh) или IsEscape(nextCh)) и (sym # Eof) делай
				если IsEscape(nextCh) то
					ScanEscape(ложь, i)
				иначе
					dynstr.Put(nextCh, i); увел(i);
					NextCh()
				всё
			кц;
			dynstr.Put(0X, i)
		кон ScanURL;

		проц ScanNumber;
		перем a, b, div: цел32;
		нач
			a := 0;
			нцПока IsDigit(nextCh) и (sym # Eof) делай
				a := 10 * a + кодСимв8(nextCh) - кодСимв8('0');
				NextCh()
			кц;
			если nextCh = '.' то
				b := 0; div := 1;
				NextCh();
				если ~IsDigit(nextCh) то sym := Dot; возврат всё;
				нцПока IsDigit(nextCh) и (sym # Eof) делай
					b := 10 * b + кодСимв8(nextCh) - кодСимв8('0'); div := 10 * div;
					NextCh()
				кц;
				realVal := a + b / div;
				sym := Number; numberType := Real
			иначе
				intVal := a;
				sym := Number; numberType := Integer
			всё;
			если IsNmStart(nextCh) то
				ScanIdent(); sym := Dimension
			аесли nextCh = '%' то
				NextCh(); sym := Percentage
			всё
		кон ScanNumber;

		проц Scan*;
		перем s: Строки8.уСтрока; msg: массив 22 из симв8;
		нач
			dynstr.Put(0X, 0); sym := Null;
			numberType := Undefined; intVal := 0; realVal := 0.0;	(* reset all fields *)
			нцДо
				SkipWhiteSpace();
				просей nextCh из
				| 0X: sym := Eof
				| 'a' .. 'z', 'A' .. 'Z', '\': ScanIdent();
						если nextCh = '(' то
							NextCh();
							s := GetStr();
							если s^ = 'url' то
								SkipWhiteSpace();
								если (nextCh = '"') или (nextCh = "'") то
									ScanString()
								иначе
									ScanURL()
								всё;
								SkipWhiteSpace();
								если nextCh = ')' то
									NextCh(); sym := URI
								иначе
									Error("')' expected")
								всё
							иначе
								sym := Function
							всё
						всё
				| '!': NextCh(); SkipWhiteSpace();
						ScanIdent(); s := GetStr();
						если s^ = 'important' то
							sym := Important
						иначе
							Error("'!important' expected")
						всё
				| '+': NextCh();
						если IsDigit(nextCh) или (nextCh = '.') то
							ScanNumber()
						иначе
							sym := Plus
						всё
				| '-': NextCh();
						если nextCh = '-' то
							NextCh();
							если nextCh = '>' то
								NextCh(); sym := Cdc
							иначе
								Error("'-->' expected")
							всё
						иначе
							sym := Minus
						всё;
				| '0' .. '9', '.' : ScanNumber()
				| '@': NextCh(); ScanIdent(); sym := AtKeyword
				| '#': NextCh(); ScanName(); sym := Hash
				| '*': NextCh(); sym := Asterisk
				| '<': NextCh();
						если nextCh = '!' то
							NextCh();
							если nextCh = '-' то
								NextCh();
								если nextCh = '-' то
									NextCh(); sym := Cdo
								иначе
									Error("'<!--' expected")
								всё
							иначе
								Error("'<!--' expected")
							всё
						иначе
							Error("'<!--' expected")
						всё
				| '/': NextCh();
						если nextCh = '*' то
							ScanComment(); sym := Null
						иначе
							sym := Slash
						всё
				| '>': NextCh(); sym := Greater
				| '~': NextCh();
						если nextCh = '=' то
							NextCh(); sym := Includes
						иначе
							Error("'~= expected")
						всё
				| '|': NextCh();
						если nextCh = '=' то
							NextCh(); sym := Dashmatch
						иначе
							Error("'|=' expected")
						всё
				| '=': NextCh(); sym := Equal
				| '"', "'": ScanString(); sym := String
				| '[': NextCh(); sym := BracketOpen
				| ']': NextCh(); sym := BracketClose
				| '(': NextCh(); sym := ParenOpen
				| ')': NextCh(); sym := ParenClose
				| '{': NextCh(); sym := BraceOpen
				| '}': NextCh(); sym := BraceClose
				| ',': NextCh(); sym := Comma
				| ';': NextCh(); sym := Semicolon
				| ':': NextCh(); sym := Colon
				иначе
					msg := "unknown character"; msg[17] := " "; msg[18] := "'"; msg[19] := nextCh; msg[20] := "'"; msg[21] := 0X;
					Error(msg)
				всё
			кцПри sym # Null
		кон Scan;

		проц GetStr*(): Строки8.уСтрока;
		нач
			возврат dynstr.ToArrOfChar();
		кон GetStr;

		проц GetPos*(): цел32;
		нач
			возврат pos
		кон GetPos;

	кон Scanner;

	проц IsWhiteSpace(ch: симв8): булево;
	нач
		возврат (ch = 020X) или (ch = 9X) или (ch = 0DX) или (ch = 0AX)
	кон IsWhiteSpace;

	проц IsNonAscii(ch: симв8): булево;
	нач
		возврат ложь
	кон IsNonAscii;

	проц IsEscape(ch: симв8): булево;
	нач
		возврат ch = '\'
	кон IsEscape;

	проц IsNmStart(ch: симв8): булево;
	нач
		возврат (('a' <= ch) и (ch <= 'z')) или (('A' <= ch) и (ch <= 'Z')) или (ch = '-') или IsNonAscii(ch) или IsEscape(ch)
	кон IsNmStart;

	проц IsNmChar(ch: симв8): булево;
	нач
		возврат (('a' <= ch) и (ch <= 'z')) или (('A' <= ch) и (ch <= 'Z')) или (ch = '-')
				или IsDigit(ch) или IsNonAscii(ch) или IsEscape(ch)
	кон IsNmChar;

	проц IsDigit(ch: симв8): булево;
	нач
		возврат ('0' <= ch) и (ch <= '9')
	кон IsDigit;

	проц HexStrToInt(перем str: массив из симв8; перем val: цел32);
	перем i, d: цел32; ch: симв8;
	нач
		i := 0; ch := str[0];
		нцПока (ch # 0X) и (ch <= " ") делай
			увел(i); ch := str[i]
		кц;
		val := 0;
		нцПока (("0" <= ch) и (ch <= "9")) или (("A" <= ch) и (ch <= "F")) делай
			если (("0" <= ch) и (ch <= "9")) то d := кодСимв8(ch)-кодСимв8("0")
			иначе d := кодСимв8(ch) - кодСимв8("A") + 10
			всё;
			увел(i); ch := str[i];
			если val <= ((матМаксимум(цел32)-d) DIV 10H) то
				val := 10H*val+d
			иначе
				СТОП(99)
			всё
		кц
	кон HexStrToInt;

	проц DefaultReportError(pos, line, row: цел32; msg: массив из симв8);
	нач
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСимв8(симв8ИзКода(9H)); ЛогЯдра.пСимв8(симв8ИзКода(9H)); ЛогЯдра.пСтроку8("pos "); ЛогЯдра.пЦел64(pos, 6);
		ЛогЯдра.пСтроку8(", line "); ЛогЯдра.пЦел64(line, 0); ЛогЯдра.пСтроку8(", row "); ЛогЯдра.пЦел64(row, 0);
		ЛогЯдра.пСтроку8("    "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
	кон DefaultReportError;

кон CSS2Scanner.

модуль RAWPrinter; (** AUTHOR "dk"; PURPOSE "Printing Raw to Port 9100"; *)
	использует Files, Потоки,  IP, TCP, DNS;

	конст
		DefConPort = 9100;

		(*Errors*)
		FILENOTFOUND = -1;
		HOSTNOTFOUND = -2;
		READERNIL = -3;
		OK = 0;


	проц PrintFile*(конст printer, fn: массив из симв8; перем res : целМЗ);
	перем
		fileReader : Files.Reader;
		file : Files.File;
	нач
		file := Files.Old(fn);
		если (file # НУЛЬ) то
		   Files.OpenReader(fileReader, file, 0);
		   PrintStream(printer, fileReader, res);
		иначе
			res := FILENOTFOUND;
		всё;
	кон PrintFile;


	проц PrintStream*(конст printer : массив из симв8; reader : Потоки.Чтец; перем res : целМЗ);
		перем
			writer: Потоки.Писарь;
			buf : массив 10000 из симв8;
			conn : TCP.Connection;
			fadr: IP.Adr;
			connres: целМЗ; len : размерМЗ;
	нач
		 DNS.HostByName(printer, fadr, connres);
		если res = DNS.Ok то
			нов(conn); conn.Open(TCP.NilPort, fadr, DefConPort, connres);
			Потоки.НастройПисаря(writer, conn.ЗапишиВПоток);
			если connres = TCP.Ok то
				если reader # НУЛЬ то
					нцПока reader.кодВозвратаПоследнейОперации = Потоки.Успех делай
						reader.чБайты(buf, 0, длинаМассива(buf), len);
						writer.пБайты(buf, 0, len);
					кц;
					writer.ПротолкниБуферВПоток();
					conn.Закрой();
					res := OK;
				иначе
					res := READERNIL;
				всё;
			иначе
				res := HOSTNOTFOUND;
			всё;
		всё;

	кон PrintStream;



кон RAWPrinter.


Usage:


PROCEDURE PrintTest1*();
VAR
	res : INTEGER;
BEGIN
	RAWPrinter.PrintFile("129.132.134.122", "test.ps", res);
	KernelLog.String("Printing res : "); KernelLog.Int(res, 5); KernelLog.Ln;
END PrintTest1;


PROCEDURE PrintTest2*();
VAR
	file : Files.File;
	res : INTEGER;
	reader : Files.Reader;
BEGIN
	file := Files.Old("test.ps");
	Files.OpenReader(reader, file, 0);
	RAWPrinter.PrintStream("129.132.134.122", reader,res);
	KernelLog.String("Printing res : "); KernelLog.Int(res, 5); KernelLog.Ln;
END PrintTest2;

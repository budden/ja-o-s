(* Patrick Stuedi, 30.08.01 *)

модуль RfsClientProxy; (** AUTHOR "pstuedi"; PURPOSE "Remote File System proxy"; *)

использует  НИЗКОУР, RfsConnection, Files, Network, TCP, ЛогЯдра, Потоки;

конст
	(*Server Procedures*)
	ERROR = 0;
	GETATTR = 1;
	SETATTR = 2;
	LOOKUP = 3;
	READ = 4;
	WRITE = 5;
	CREATE = 6;
	REMOVE = 8;
	RENAME = 10;
	READDIR = 11;
	CREATETMP = 12;
	CHDIR = 13;
	KILL = 14;
	AUTHENT = 15;

	(*Errorcodes*)
	REPLYOK* = 0;
	RECEIVERROR* = 1;
	PARAMERROR* = 2;
	CACHEMISS* = 3;
	GETATTRERROR* =  4;
	SETATTRERROR* = 5;
	NOFILE* = 6;
	READERROR* = 7;
	WRITEERROR* = 8;
	REMOVEERROR* = 9;
	RENAMEERROR* = 10;
	NODIR* = 11;
	AUTHENTICATIONERROR* = 12;

	HeaderLength = 100;
	Payload* = 16280;
	BufSize = Payload + HeaderLength;
	MaxNameLen = 64;
	DataOff = 8;
	Ok = TCP.Ok;
	DefaultPort = 9107;

(* Dummy Types for Files.Volume interface *)
тип
	Address* = цел32;



тип
	(** Type for storing Directory Information, used by RfsFS.Filesystem.Enumerate() **)
	Dir *= окласс
		перем
			first*: Dirent;
			last*: Dirent;
			nbrOfEntrys*: цел32;

		проц &Init*;
		нач
			first := НУЛЬ;
			last := НУЛЬ;
			nbrOfEntrys := 0;
		кон Init;

		(** insert tuple name, time, date and size. name is at offset off in Array **)
		проц Insert*(перем name: массив из симв8; off, len, time, date, size: цел32);
			перем entry: Dirent;
		нач
			нов(entry);
			CopyBuffer(name, off, entry.name, 0, len);
			entry.name[len] := 0X;
			entry.time := time;
			entry.date := date;
			entry.size := size;
			entry.next := НУЛЬ;

			если last # НУЛЬ то
				last.next := entry;
				last := last.next;
			иначе
				last := entry;
				first := last;
			всё;

			увел(nbrOfEntrys);
		кон Insert;

		(** Get next tuple from Object **)
		проц Get*(перем name: массив из симв8; перем time, date, size: цел32);
			перем len: цел32;
		нач
			если first # НУЛЬ то
				len := Len(first.name);
				CopyBuffer(first.name, 0, name, 0, len);
				name[len] := 0X;
				time := first.time;
				date := first.date;
				size := first.size;
				умень(nbrOfEntrys);
				first := first.next;
			всё;
		кон Get;

	кон Dir;

тип
	(** Type of Dir Entry **)
	Dirent* = окласс
		перем
			name: массив MaxNameLen из симв8;
			time, date, size: цел32;
			next: Dirent;
	кон Dirent;


тип
	(** Virtual Filesystem Object (Rfs File Protocoll), communicates via RfsRPC with rfsServerProxy on the serverside **)
	Proxy* = окласс (Files.Volume)
		перем
			connection: RfsConnection.Connection;
			user, passwd, host, path: массив MaxNameLen из симв8;
			port : цел16;
			buf, backupBuf: массив BufSize из симв8;


		проц &InitProxy*(перем user, passwd, host, path: массив из симв8; port: цел16);
			перем lenHost, lenUser, lenPasswd, lenPath: цел32;
		нач
			lenUser := Len(user);
			lenPasswd := Len(passwd);
			lenHost := Len(host);
			lenPath := Len(path);
			CopyBuffer(user, 0, сам.user, 0, lenUser);
			CopyBuffer(passwd, 0, сам.passwd, 0, lenPasswd);
			CopyBuffer(host, 0, сам.host, 0, lenHost);
			CopyBuffer(path, 0, сам.path, 0, lenPath);
			сам.user[lenUser] := 0X;
			сам.passwd[lenPasswd] := 0X;
			сам.host[lenHost] := 0X;
			сам.path[lenPath] := 0X;
			сам.port := port;
			нов(connection, host, port);
		кон InitProxy;

		(** does nothing, just for benchmarking **)
		проц Error*(перем errorcode: целМЗ);
			перем procID, testID: цел32; msgBytes, dataBytes, received: размерМЗ; res: целМЗ;
		нач
			(*prepare params and send*)
			procID := ERROR;
			dataBytes := 0;
			msgBytes := dataBytes + 8;
			Int2Char(procID, buf, 0);
			Int2Char(dataBytes, buf, 4);
			connection.Send(buf, 0, msgBytes, res);

			(*receiving result*)
			GetResult(connection, errorcode, dataBytes, received, buf);

			если errorcode = RECEIVERROR то
				CopyBuffer(buf, 0, backupBuf, 0, msgBytes);
				connection.Reset();
				Mount(errorcode);
				если errorcode = REPLYOK то
					Char2Int(backupBuf, 0, testID);
					если testID = procID то
						connection.Send(backupBuf, 0, msgBytes, res);
						GetResult(connection, errorcode, dataBytes, received, buf);
					иначе
						errorcode := RECEIVERROR;
					всё;
				всё;
			всё;
			connection.Close();
		кон Error;

		(** retrieves fileLength, time and date of a file identified by fileID **)
		проц GetAttr*(fileID: цел32; перем fileLen, time, date: цел32; перем errorcode: целМЗ);
			перем procID, testID: цел32; msgBytes, dataBytes, received: размерМЗ; res: целМЗ;
		нач
			(*prepare params and send*)
			procID := GETATTR;
			dataBytes := 8;
			msgBytes := dataBytes + 8;
			Int2Char(procID, buf, 0);
			Int2Char(dataBytes, buf, 4);
			Int2Char(4, buf, DataOff);
			Int2Char(fileID, buf, DataOff + 4);
			connection.Send(buf, 0, msgBytes, res);

			(*getting result*)
			GetResult(connection, errorcode, dataBytes, received, buf);
			если errorcode = RECEIVERROR то
				CopyBuffer(buf, 0, backupBuf, 0, msgBytes);
				connection.Reset();
				Mount(errorcode);
				если errorcode = REPLYOK то
					Char2Int(backupBuf, 0, testID);
					если testID = procID то
						connection.Send(backupBuf, 0, msgBytes, res);
						GetResult(connection, errorcode, dataBytes, received, buf);
					иначе
						errorcode := RECEIVERROR;
					всё;
				всё;
			всё;

			если errorcode # REPLYOK то
				fileLen := 0;
				time := 0;
				date := 0;
			иначе
				Char2Int(buf, 0, fileLen);
				Char2Int(buf, 4, time);
				Char2Int(buf, 8, date);
			всё;
		кон GetAttr;

		(** Sets the attributes time and date of a file identified by filename **)
		проц SetAttr*(перем filename: массив из симв8; time, date: цел32; перем errorcode: цел32);
			перем procID, testID, filenameLen: цел32; msgBytes, dataBytes, received: размерМЗ; res: целМЗ;
		нач
			(*prepare params and send*)
			procID := SETATTR;
			filenameLen := Len(filename);
			dataBytes := 12 + filenameLen;
			msgBytes := dataBytes + 8;
			Int2Char(procID, buf, 0);
			Int2Char(dataBytes, buf, 4);
			Int2Char(time, buf, DataOff);
			Int2Char(date, buf, DataOff + 4);
			Int2Char(filenameLen, buf, DataOff + 8);
			CopyBuffer(filename, 0, buf, DataOff + 12, filenameLen);
			connection.Send(buf, 0, msgBytes, res);

			(*getting result*)
			GetResult(connection, errorcode, dataBytes, received, buf);
			если errorcode = RECEIVERROR то
				CopyBuffer(buf, 0, backupBuf, 0, msgBytes);
				connection.Reset();
				Mount(errorcode);
				если errorcode = REPLYOK то
					Char2Int(backupBuf, 0, testID);
					если testID = procID то
						connection.Send(backupBuf, 0, msgBytes, res);
						GetResult(connection, errorcode, dataBytes, received, buf);
					иначе
						errorcode := RECEIVERROR;
					всё;
				всё;
			всё;
		кон SetAttr;

		(** transforms a filename into a fileID **)
		проц Lookup*(перем filename : массив из симв8; перем fileID: цел32; перем errorcode: целМЗ);
			перем filenameLen, procID, testID, fileIDLen: цел32; msgBytes, received, dataBytes: размерМЗ; res: целМЗ;
		нач
			(*prepare params and send*)
			procID := LOOKUP;
			filenameLen := Len(filename);
			dataBytes := filenameLen + 4;
			msgBytes := dataBytes + 8;

			Int2Char(procID, buf, 0);
			Int2Char(dataBytes, buf, 4);
			Int2Char(filenameLen, buf, DataOff);
			CopyBuffer(filename, 0, buf, DataOff + 4, filenameLen);
			connection.Send(buf, 0, msgBytes, res);

			(*getting result*)
			GetResult(connection, errorcode, dataBytes, received, buf);
			если errorcode = RECEIVERROR то
				CopyBuffer(buf, 0, backupBuf, 0, msgBytes);
				connection.Reset();
				Mount(errorcode);
				если errorcode = REPLYOK то
					Char2Int(backupBuf, 0, testID);
					если testID = procID то
						connection.Send(backupBuf, 0, msgBytes, res);
						GetResult(connection, errorcode, dataBytes, received, buf);
					иначе
						errorcode := RECEIVERROR;
					всё;
				всё;
			всё;

			если errorcode # 0 то
				fileIDLen := 0;
				fileID := 0;
			иначе
				Char2Int(buf, 0, fileIDLen);
				Char2Int(buf, 4, fileID);
			всё;
		кон Lookup;


		(** Reads len Bytes of data from a given offset off in a file identified by fileID and returns the data in buf at offset dstOff **)
		проц Read*(fileID, off, len: цел32; перем buffer: массив из симв8; dstOff: цел32; перем received: размерМЗ; перем errorcode: целМЗ);
			перем procID, testID, fileLen: цел32; msgBytes, dataBytes: размерМЗ; res: целМЗ;
		нач
			(*prepare params and send*)
			procID := READ;
			dataBytes := 16;
			msgBytes := dataBytes + 8;
			Int2Char(procID, buf, 0);
			Int2Char(dataBytes, buf, 4);
			Int2Char(4, buf, DataOff);
			Int2Char(fileID, buf, DataOff + 4);
			Int2Char(off, buf, DataOff + 8);
			Int2Char(len, buf, DataOff + 12);
			connection.Send(buf, 0, msgBytes, res);

			(*getting result*)
			GetResult(connection, errorcode, dataBytes, received, buf);
			если errorcode = RECEIVERROR то
				CopyBuffer(buf, 0, backupBuf, 0, msgBytes);
				connection.Reset();
				Mount(errorcode);
				если errorcode = REPLYOK то
					Char2Int(backupBuf, 0, testID);
					если testID = procID то
						connection.Send(backupBuf, 0, msgBytes, res);
						GetResult(connection, errorcode, dataBytes, received, buf);
					иначе
						errorcode := RECEIVERROR;
					всё;
				всё;
			всё;

			если errorcode # 0 то
				fileLen := 0;
				received := 0;
			иначе
				Char2Int(buf, 0, fileLen);
				CopyBuffer(buf, 4, buffer, dstOff, fileLen);
				received := received -4;
			всё;
		кон Read;

		(** Writes len Bytes of data beginning off bytes from the beginning of file into a file identified by fileID **)
		проц Write*(fileID, off, len: цел32; перем buffer: массив из симв8; перем written: цел32; перем errorcode: целМЗ);
			перем procID, testID: цел32; msgBytes, dataBytes, received: размерМЗ; res: целМЗ;
		нач
			(*prepare params and send*)
			procID := WRITE;
			dataBytes := 16 + len;
			msgBytes := dataBytes + 8;
			Int2Char(procID, buf, 0);
			Int2Char(dataBytes, buf, 4);
			Int2Char(4, buf, DataOff);
			Int2Char(fileID, buf, DataOff + 4);
			Int2Char(off, buf, DataOff + 8);
			Int2Char(len, buf, DataOff + 12);
			CopyBuffer(buffer, 0, buf, DataOff + 16, len);
			connection.Send(buf, 0, msgBytes, res);

			(*getting result*)
			GetResult(connection, errorcode, dataBytes, received, buf);
			если errorcode = RECEIVERROR то
				CopyBuffer(buf, 0, backupBuf, 0, msgBytes);
				connection.Reset();
				Mount(errorcode);
				если errorcode = REPLYOK то
					Char2Int(backupBuf, 0, testID);
					если testID = procID то
						connection.Send(backupBuf, 0, msgBytes, res);
						GetResult(connection, errorcode, dataBytes, received, buf);
					иначе
						errorcode := RECEIVERROR;
					всё;
				всё;
			всё;

			если errorcode # 0 то
				written := 0;
			иначе
				Char2Int(buf, 0, written);
			всё;
		кон Write;

		(** Creates a file with name filename and return a fileID for it **)
		проц Create*(перем filename : массив из симв8; перем fileID: цел32; перем errorcode: целМЗ);
			перем filenameLen, procID, testID, fileIDLen: цел32; msgBytes, received, dataBytes: размерМЗ; res: целМЗ;
		нач
			(*prepare params and send*)
			procID := CREATE;
			filenameLen := Len(filename);
			dataBytes := filenameLen + 4;
			msgBytes := dataBytes + 8;
			Int2Char(procID, buf, 0);
			Int2Char(dataBytes, buf, 4);
			Int2Char(filenameLen, buf, DataOff);
			CopyBuffer(filename, 0, buf, DataOff + 4, filenameLen);
			connection.Send(buf, 0, msgBytes, res);

			(*getting result*)
			GetResult(connection, errorcode, dataBytes, received, buf);
			если errorcode = RECEIVERROR то
				CopyBuffer(buf, 0, backupBuf, 0, msgBytes);
				connection.Reset();
				Mount(errorcode);
				если errorcode = REPLYOK то
					Char2Int(backupBuf, 0, testID);
					если testID = procID то
						connection.Send(backupBuf, 0, msgBytes, res);
						GetResult(connection, errorcode, dataBytes, received, buf);
					иначе
						errorcode := RECEIVERROR;
					всё;
				всё;
			всё;

			если errorcode # 0 то
				fileIDLen := 0;
				fileID := 0;
			иначе
				Char2Int(buf, 0, fileIDLen);
				Char2Int(buf, 4, fileID);
			всё;
		кон Create;


		(** Deletes a File with name filename **)
		проц Remove*(перем filename : массив из симв8; перем errorcode: целМЗ);
			перем filenameLen, procID, testID: цел32; msgBytes, received, dataBytes: размерМЗ; res: целМЗ;
		нач
			(*prepare params and send*)
			procID := REMOVE;
			filenameLen := Len(filename);
			dataBytes := filenameLen + 4;
			msgBytes := dataBytes + 8;

			Int2Char(procID, buf, 0);
			Int2Char(dataBytes, buf, 4);
			Int2Char(filenameLen, buf, DataOff);
			CopyBuffer(filename, 0, buf, DataOff + 4, filenameLen);
			connection.Send(buf, 0, msgBytes, res);

			(*getting result*)
			GetResult(connection, errorcode, dataBytes, received, buf);
			если errorcode = RECEIVERROR то
				CopyBuffer(buf, 0, backupBuf, 0, msgBytes);
				connection.Reset();
				Mount(errorcode);
				если errorcode = REPLYOK то
					Char2Int(backupBuf, 0, testID);
					если testID = procID то
						connection.Send(backupBuf, 0, msgBytes, res);
						GetResult(connection, errorcode, dataBytes, received, buf);
					иначе
						errorcode := RECEIVERROR;
					всё;
				всё;
			всё;
		кон Remove;

		(** Renames a file with name filenemFrom into a new name filenameTo **)
		проц Rename*(перем filenameFrom, filenameTo: массив из симв8; перем errorcode: целМЗ);
			перем filenameLenFrom, filenameLenTo, procID, testID: цел32; msgBytes, received, dataBytes: размерМЗ; res: целМЗ;
		нач
			(*prepare params and send*)
			procID := RENAME;
			filenameLenFrom := Len(filenameFrom);
			filenameLenTo := Len(filenameTo);
			dataBytes := filenameLenFrom + filenameLenTo + 8;
			msgBytes := dataBytes + 8;
			Int2Char(procID, buf, 0);
			Int2Char(dataBytes, buf, 4);
			Int2Char(filenameLenFrom, buf, DataOff);
			CopyBuffer(filenameFrom, 0, buf, DataOff + 4, filenameLenFrom);
			Int2Char(filenameLenTo, buf, DataOff + 4 + filenameLenFrom);
			CopyBuffer(filenameTo, 0, buf, DataOff + 4 + filenameLenFrom + 4, filenameLenTo);
			connection.Send(buf, 0, msgBytes, res);

			(*getting result*)
			GetResult(connection, errorcode, dataBytes, received, buf);
			если errorcode = RECEIVERROR то
				CopyBuffer(buf, 0, backupBuf, 0, msgBytes);
				connection.Reset();
				Mount(errorcode);
				если errorcode = REPLYOK то
					Char2Int(backupBuf, 0, testID);
					если testID = procID то
						connection.Send(backupBuf, 0, msgBytes, res);
						GetResult(connection, errorcode, dataBytes, received, buf);
					иначе
						errorcode := RECEIVERROR;
					всё;
				всё;
			всё;
		кон Rename;


		(** Retrieves a variable number of entries with names matching the mask string from a directory identified by filename.
			  the detail is set to 1 if time, data and size information is also to be retrieved. the offset value tells how many entries
			  should be skip first **)
		проц ReadDir*(конст filename, mask : массив из симв8; detail, cookie: цел32; dir: Dir; перем endOfDir: цел32; перем errorcode: целМЗ);
			перем filenameLen, procID, testID, currentIndex, maskLen, time, date, size: цел32; msgBytes, received, dataBytes: размерМЗ; res: целМЗ;
		нач
			(*prepare params and send*)
			procID := READDIR;
			filenameLen := Len(filename);
			maskLen := Len(mask);
			dataBytes := 4 + 4 + 4 + 4 + filenameLen + maskLen;
			msgBytes := dataBytes + 8;
			Int2Char(procID, buf, 0);
			Int2Char(dataBytes, buf, 4);
			Int2Char(filenameLen, buf, DataOff);
			CopyBuffer(filename, 0, buf, DataOff + 4, filenameLen);
			Int2Char(maskLen, buf, DataOff + 4 + filenameLen);
			CopyBuffer(mask, 0, buf, DataOff + 8 + filenameLen, maskLen);
			Int2Char(detail, buf, DataOff + 8 + filenameLen + maskLen);
			Int2Char(cookie, buf, DataOff + 12 + filenameLen + maskLen);
			connection.Send(buf, 0, msgBytes, res);

			(*getting result*)
			GetResult(connection, errorcode, dataBytes, received, buf);
			если errorcode = RECEIVERROR то
				CopyBuffer(buf, 0, backupBuf, 0, msgBytes);
				connection.Reset();
				Mount(errorcode);
				если errorcode = REPLYOK то
					Char2Int(backupBuf, 0, testID);
					если testID = procID то
						connection.Send(backupBuf, 0, msgBytes, res);
						GetResult(connection, errorcode, dataBytes, received, buf);
					иначе
						errorcode := RECEIVERROR;
					всё;
				всё;
			всё;

			если errorcode # 0 то
				endOfDir := 0;
				dir := НУЛЬ;
			иначе
				Char2Int(buf, 0, endOfDir);
				currentIndex := 4;
				filenameLen := 0;
				если detail > 0 то
					нцПока (currentIndex + 16) <= received делай
						Char2Int(buf, currentIndex, filenameLen);
						Char2Int(buf, currentIndex + 4, time);
						Char2Int(buf, currentIndex + 8, date);
						Char2Int(buf, currentIndex + 12, size);
						если (currentIndex + 16 + filenameLen) <= received то
							dir.Insert(buf, currentIndex + 16, filenameLen, time, date, size);
						всё;
						currentIndex := currentIndex + 16 + filenameLen;
					кц;
				иначе
					нцПока (currentIndex + 4) <= received делай
						Char2Int(buf, currentIndex, filenameLen);
						если (currentIndex + 4 + filenameLen) <= received то
							dir.Insert(buf, currentIndex + 4, filenameLen, 0, 0, 0);
						всё;
						currentIndex := currentIndex + 4 + filenameLen;
					кц;
				всё;
			всё;
		кон ReadDir;

		(** Creates a temporary File on the server and returns the name for it **)
		проц CreateTmp*(перем filename : массив из симв8; перем hashval: цел32; перем errorcode: целМЗ);
			перем filenameLen, procID, testID, hashvalLen: цел32; msgBytes, received, dataBytes: размерМЗ; res: целМЗ;
		нач
			(*prepare params and send*)
			procID := CREATETMP;
			filenameLen := Len(filename);
			dataBytes := filenameLen + 4;
			msgBytes := dataBytes + 8;
			Int2Char(procID, buf, 0);
			Int2Char(dataBytes, buf, 4);
			Int2Char(filenameLen, buf, DataOff);
			CopyBuffer(filename, 0, buf, DataOff + 4, filenameLen);
			connection.Send(buf, 0, msgBytes, res);

			(*getting result*)
			GetResult(connection, errorcode, dataBytes, received, buf);
			если errorcode = RECEIVERROR то
				CopyBuffer(buf, 0, backupBuf, 0, msgBytes);
				connection.Reset();
				Mount(errorcode);
				если errorcode = REPLYOK то
					Char2Int(backupBuf, 0, testID);
					если testID = procID то
						connection.Send(backupBuf, 0, msgBytes, res);
						GetResult(connection, errorcode, dataBytes, received, buf);
					иначе
						errorcode := RECEIVERROR;
					всё;
				всё;
			всё;

			если errorcode # 0 то
				hashvalLen := 0;
				hashval := 0;
				filenameLen := 0;
				filename[0] := 0X;
			иначе
				Char2Int(buf, 0, hashvalLen);

				Char2Int(buf, 4, hashval);

				Char2Int(buf, 8, filenameLen);

				CopyBuffer(buf, 12, filename, 0, filenameLen);
				filename[filenameLen] := 0X;
			всё;
		кон CreateTmp;

		(** Changes the Directory of the corresponding rfsServerProxy Process **)
		проц ChDir*(перем dir : массив из симв8; перем errorcode: целМЗ);
			перем dirLen, procID, testID: цел32; msgBytes, received, dataBytes: размерМЗ; res: целМЗ;
		нач
			(*prepare params and send*)
			procID := CHDIR;
			dirLen := Len(dir);
			dataBytes := dirLen + 4;
			msgBytes := dataBytes + 8;

			Int2Char(procID, buf, 0);
			Int2Char(dataBytes, buf, 4);
			Int2Char(dirLen, buf, DataOff);
			CopyBuffer(dir, 0, buf, DataOff + 4, dirLen);
			connection.Send(buf, 0, msgBytes, res);

			(*getting result*)
			GetResult(connection, errorcode, dataBytes, received, buf);
			если errorcode = RECEIVERROR то
				CopyBuffer(buf, 0, backupBuf, 0, msgBytes);
				connection.Reset();
				Mount(errorcode);
				если errorcode = REPLYOK то
					Char2Int(backupBuf, 0, testID);
					если testID = procID то
						connection.Send(backupBuf, 0, msgBytes, res);
						GetResult(connection, errorcode, dataBytes, received, buf);
					иначе
						errorcode := RECEIVERROR;
					всё;
				всё;
			всё;
		кон ChDir;


		(** cleans the open file structure on the server an kills the process **)
		проц Kill*(перем errorcode: целМЗ);
			перем procID, testID: цел32; msgBytes, dataBytes, received: размерМЗ; res: целМЗ;
		нач
			(*prepare params and send*)
			procID := KILL;
			dataBytes := 0;
			msgBytes := dataBytes + 8;
			Int2Char(procID, buf, 0);
			Int2Char(dataBytes, buf, 4);
			connection.Send(buf, 0, msgBytes, res);

			(*getting result*)
			GetResult(connection, errorcode, dataBytes, received, buf);
			если errorcode = RECEIVERROR то
				CopyBuffer(buf, 0, backupBuf, 0, msgBytes);
				connection.Reset();
				Mount(errorcode);
				если errorcode = REPLYOK то
					Char2Int(backupBuf, 0, testID);
					если testID = procID то
						connection.Send(backupBuf, 0, msgBytes, res);
						GetResult(connection, errorcode, dataBytes, received, buf);
					иначе
						errorcode := RECEIVERROR;
					всё;
				всё;
			всё;
			connection.Close();
		кон Kill;

		(** Authenticates the session. User and Passwd are sending in plainttext **)
		проц Authent*(перем user, passwd, path: массив из симв8; перем errorcode: целМЗ);
			перем userLen, passwdLen, pathLen, procID: цел32; msgBytes, received, dataBytes: размерМЗ; res: целМЗ;
		нач
			(*prepare params and send*)
			procID := AUTHENT;
			userLen := Len(user);
			passwdLen := Len(passwd);
			pathLen := Len(path);
			dataBytes := userLen + passwdLen + pathLen + 12;
			msgBytes := dataBytes + 8;
			Int2Char(procID, buf, 0);
			Int2Char(dataBytes, buf, 4);
			Int2Char(userLen, buf, DataOff);
			CopyBuffer(user, 0, buf, DataOff + 4, userLen);
			Int2Char(passwdLen, buf, DataOff + 4 + userLen);
			CopyBuffer(passwd, 0, buf, DataOff + 4 + userLen + 4, passwdLen);
			Int2Char(pathLen, buf, DataOff + 4 + userLen + 4 + passwdLen);
			CopyBuffer(path, 0, buf, DataOff + 4 + userLen + 4 + passwdLen + 4, pathLen);
			connection.Send(buf, 0, msgBytes, res);

			(*getting result*)
			GetResult(connection, errorcode, dataBytes, received, buf);
			если errorcode # REPLYOK то
				connection.Close();
			всё;
		кон Authent;


		(** this procedure is called from RfsClientProxy.New, it dows the Authentication and changes **)
		(** the directory on the server **)
		проц Mount*(перем errorcode: целМЗ);
			перем res: целМЗ;
		нач
			errorcode := RECEIVERROR;
			connection.Open(res);
			если res = Ok то
				Authent(user, passwd, path, errorcode);
				если errorcode = REPLYOK то
					ChDir(path, errorcode);
					если errorcode # REPLYOK то
						ЛогЯдра.пСтроку8("Mount->cant change Directory");
						ЛогЯдра.пВК_ПС;
					всё;
				иначе
					ЛогЯдра.пСтроку8("Mount->Authentication Error");
					ЛогЯдра.пВК_ПС;
				всё;
			иначе
				ЛогЯдра.пСтроку8("Mount->can`t open the connection");
				ЛогЯдра.пВК_ПС;
			всё;
		кон Mount;

		(** Kill the rfsServerProxy Process and closes the connection **)
		проц Unmount*(перем errorcode: целМЗ);
		нач
			Kill(errorcode);
		кон Unmount;

		проц {перекрыта}AllocBlock*(hint: Address; перем adr: Address);
		кон AllocBlock;

		проц {перекрыта}FreeBlock*(adr: Address);
		кон FreeBlock;

		проц {перекрыта}MarkBlock*(adr: Address);
		кон MarkBlock;

		проц {перекрыта}Marked*(adr: Address): булево;
		кон Marked;

		проц {перекрыта}Available*(): цел32;
		нач
			возврат 0;
		кон Available;

		проц {перекрыта}GetBlock*(adr: цел32; перем blk: массив из симв8);
		кон GetBlock;

		проц {перекрыта}PutBlock*(adr: цел32; перем blk: массив из симв8);
		кон PutBlock;

	кон Proxy;

(** a new global Proxy for a specific host, port pair is created. in addition a dummy Volume is set to par.vol.
	  this is needed so that the mounting works correctly **)
проц New*(context : Files.Parameters);
перем
	server, user, passwd, path: массив MaxNameLen из симв8;
	i: размерМЗ; errorcode: целМЗ; ch: симв8; port: цел32; newVol: Proxy;

нач
	context.arg.ПропустиБелоеПоле;

	ch := context.arg.ПодглядиСимв8();
	i := 0;
	нцПока (i < длинаМассива(user)-1) и (ch > " ") и (ch # ":") и (context.arg.кодВозвратаПоследнейОперации = Потоки.Успех) делай
		context.arg.чСимв8(ch); (* consume ch *)
		user[i] := ch; увел(i);
		ch := context.arg.ПодглядиСимв8();
	кц;
	user[i] := 0X;

	ch := context.arg.ПодглядиСимв8();
	i := 0;
	нцПока (i < длинаМассива(passwd)-1) и (ch > " ") и (ch # "@") и (context.arg.кодВозвратаПоследнейОперации = Потоки.Успех) делай
		context.arg.чСимв8(ch); (* consume ch *)
		passwd[i] := ch; увел(i);
		ch := context.arg.ПодглядиСимв8();
	кц;
	passwd[i] := 0X;

	ch := context.arg.ПодглядиСимв8();
	i := 0;
	нцПока (i < длинаМассива(server)-1) и (ch > " ") и (ch # ":") и (context.arg.кодВозвратаПоследнейОперации = Потоки.Успех) делай
		context.arg.чСимв8(ch); (* consume ch *)
		server[i] := ch; увел(i);
		ch := context.arg.ПодглядиСимв8();
	кц;
	server[i] := 0X;

	port := 0;
	если (ch = ":") то
		context.arg.чСимв8(ch); (* consume ":" *)
		context.arg.чЦел32(port, ложь);
	иначе
		port := DefaultPort;
	всё;

	ch := context.arg.ПодглядиСимв8();
	i := 0;
	если ch = "/" то
		context.arg.чСимв8(ch); (* consume "/" *)
		ch := context.arg.ПодглядиСимв8();
		нцПока (i < длинаМассива(path)-1) и (ch > " ") и (ch # ":") и (context.arg.кодВозвратаПоследнейОперации = Потоки.Успех) делай
			context.arg.чСимв8(ch); (* consume ch *)
			path[i] := ch; увел(i);
			ch := context.arg.ПодглядиСимв8();
		кц;
		path[i] := 0X;
	иначе
		CopyBuffer(user, 0, path, 0, Len(user));
	всё;

	context.out.пСтроку8("Proxy->user: "); context.out.пСтроку8(user);
	context.out.пСтроку8(", server: "); context.out.пСтроку8(server);
	context.out.пСтроку8(", port "); context.out.пЦел64(port, 4);
	context.out.пСтроку8(", path: "); context.out.пСтроку8(path);
	context.out.пСтроку8(", password: "); context.out.пСтроку8(passwd);
	context.out.пВК_ПС;

	нов(newVol, user, passwd, server, path, устарПреобразуйКБолееУзкомуЦел(port));
	newVol.Mount(errorcode);
	если errorcode = REPLYOK то
		context.vol := newVol;
		context.out.пСтроку8("Proxy->done");
		context.out.пВК_ПС;
	иначе
		context.error.пСтроку8("Proxy->Failure");
		context.error.пВК_ПС;
	всё;
кон New;

проц GetResult(connection: RfsConnection.Connection; перем errorcode: целМЗ; перем dataBytes, received: размерМЗ; перем buf: массив из симв8);
	перем res: целМЗ;
нач
	errorcode := ReadInteger(connection, res);
	если res = Ok то
		dataBytes := ReadInteger(connection, res);
		если res = Ok то
			connection.Receive(buf, 0, dataBytes, received, res);
			если res = Ok то
				возврат;
			всё;
		всё;
	всё;
	errorcode := RECEIVERROR;
кон GetResult;

(** An integer is filled into an array of character **)
проц Int2Char*(int: размерМЗ; перем buf: массив из симв8; off: размерМЗ);
нач
	(*
	buf[off + 0] := CHR(int MOD Block);
	int := int DIV Block;
	buf[off + 1] := CHR(int MOD Block);
	int := int DIV Block;
	buf[off + 2] := CHR(int MOD Block);
	int := int DIV Block;
	buf[off + 3] := CHR(int MOD Block);
	*)

	Network.PutNet4(buf, off, целМЗ(int));
кон Int2Char;

(** four bytes of an array of characters are casted into an integer **)
проц Char2Int*(buf: массив из симв8; off: цел32; перем int: цел32);
нач
	int := Network.GetNet4(buf, off);
кон Char2Int;


проц ReadInteger*(connection: RfsConnection.Connection; перем res: целМЗ): цел32;
	перем val: цел32; received: размерМЗ; buf: массив 4 из симв8;
нач
	connection.Receive(buf, 0, 4, received, res);
	если received # 4 то
		val := -1;
		res := PARAMERROR;
	иначе
		Char2Int(buf, 0, val);
	всё;
	возврат val;
кон ReadInteger;


проц Len(x: массив из симв8): цел32;
	перем j: цел32;
нач
	j := 0;
	нцПока x[j] # 0X делай
		увел(j);
	кц;
	возврат j;
кон Len;

(** Fast Buffer Copying. copy from offset offFrom len Bytes of Buffer bufFrom into bufTo at offset offTo **)
проц CopyBuffer*(конст bufFrom: массив из симв8; offFrom: размерМЗ; перем bufTo: массив из симв8; offTo, len: размерМЗ);
нач
	утв(offTo + len <= длинаМассива(bufTo));
	НИЗКОУР.копируйПамять(адресОт(bufFrom[offFrom]), адресОт(bufTo[offTo]), len);
кон CopyBuffer;

кон RfsClientProxy.

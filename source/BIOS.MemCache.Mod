(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль MemCache; (** AUTHOR "pjm"; PURPOSE "Memory cache control"; *)

использует НИЗКОУР, ЭВМ;

конст
		(** cache properties *)
	UC* = 0; WC* = 1; WT* = 4; WP* = 5; WB* = 6;

	PS = 4096;	(* page size in bytes *)
	M = 100000H;	(* 1K, 1M, 1G *)

	Ok = 0;

тип
	SetCacheMessage = укль на запись (ЭВМ.Message)
		physAdr: адресВПамяти; size, type: цел32;
		res: массив ЭВМ.МаксКвоПроцессоров из целМЗ
	кон;

(* Return the value of the MTTRcap register. *)

проц -GetMTTRcapLow(): мнвоНаБитахМЗ;
машКод
#если I386 то
	MOV ECX, 0FEH	; MTTRcap
	RDMSR
#аесли AMD64 то
	XOR RAX, RAX
	MOV ECX, 0FEH	; MTTRcap
	RDMSR
#иначе
	unimplemented
#кон
кон GetMTTRcapLow;

(*
(* Return the value of the MTTRdefType register. *)

PROCEDURE -GetMTTRdefTypeLow(): SET;
CODE
#IF I386 THEN
	MOV ECX, 2FFH	; MTTRdefType
	RDMSR
#ELSIF AMD64 THEN
	XOR RAX, RAX
	MOV ECX, 2FFH	; MTTRdefType
	RDMSR
#ELSE
	unimplemented
#END
END GetMTTRdefTypeLow;
*)

(* Return the value of the specified MTTRphysBase register. *)

проц -GetMTTRphysBaseLow(n: адресВПамяти): мнвоНаБитахМЗ;
машКод
#если I386 то
	POP ECX
	SHL ECX, 1
	ADD ECX, 200H	; MTTRphysBase0
	RDMSR
#аесли AMD64 то
	XOR RAX, RAX
	POP RCX
	SHL RCX, 1
	ADD RCX, 200H	; MTTRphysBase0
	RDMSR
#иначе
	unimplemented
#кон
кон GetMTTRphysBaseLow;

(* Return the value of the specified MTTRphysMask register. *)

проц -GetMTTRphysMaskLow(n: адресВПамяти): мнвоНаБитахМЗ;
машКод
#если I386 то
	POP ECX
	SHL ECX, 1
	ADD ECX, 201H	; MTTRphysMask0
	RDMSR
#аесли AMD64 то
	XOR RAX, RAX
	POP RCX
	SHL RCX, 1
	ADD RCX, 201H	; MTTRphysMask0
	RDMSR
#иначе
	unimplemented
#кон
кон GetMTTRphysMaskLow;

(* Set the specified MTTRphysBase register. *)

проц -SetMTTRphysBase(n: адресВПамяти; high, low: мнвоНаБитахМЗ);
машКод
#если I386 то
	POP EAX
	POP EDX
	POP ECX
	SHL ECX, 1
	ADD ECX, 200H	; MTTRphysBase0
	WRMSR
#аесли AMD64 то
	POP RAX
	POP RDX
	POP RCX
	SHL RCX, 1
	ADD RCX, 200H	; MTTRphysBase0
	WRMSR
#иначе
	unimplemented
#кон
кон SetMTTRphysBase;

(* Set the specified MTTRphysMask register. *)

проц -SetMTTRphysMask(n: адресВПамяти; high, low: мнвоНаБитахМЗ);
машКод
#если I386 то
	POP EAX
	POP EDX
	POP ECX
	SHL ECX, 1
	ADD ECX, 201H	; MTTRphysMask0
	WRMSR
#аесли AMD64 то
	POP RAX
	POP RDX
	POP RCX
	SHL RCX, 1
	ADD RCX, 201H	; MTTRphysMask0
	WRMSR
#иначе
	unimplemented
#кон
кон SetMTTRphysMask;

(** Set the cache properties of the specified physical memory area on the current processor. {physAdr, size MOD PS = 0}  Must be called from supervisor mode. *)

проц LocalSetCacheProperties*(physAdr: адресВПамяти; size, type: цел32; перем res: целМЗ);
перем i, n, f: цел32; mask, base: мнвоНаБитахМЗ; j, k: адресВПамяти;
нач
	утв((physAdr остОтДеленияНа PS = 0) и (size остОтДеленияНа PS = 0) и (size # 0));
	если (physAdr >= M) или (physAdr < 0) то
		k := size; нцПока k > 0 делай k := арифмСдвиг(k, 1) кц;	(* shift highest set bit into bit 31 *)
		если k = 80000000H то	(* only one bit was set => size is power of 2 *)
			если physAdr остОтДеленияНа size = 0 то
				ЭВМ.ЗапросиБлокировку(ЭВМ.ЯдернаяБлокировкаДляРаботыСПамятью);	(* hack *)
				если ЭВМ.MTTR в ЭВМ.свойстваПроцессора_1 то	(* MTTRs supported *)
					mask := GetMTTRcapLow();
					если (type # WC) или (10 в mask) то
						n := НИЗКОУР.подмениТипЗначения(цел32, mask * {0..7});
						i := 0; f := -1; res := Ok;
						нцПока (i # n) и (res = Ok) делай
							mask := GetMTTRphysMaskLow(i);
							если 11 в mask то	(* entry is valid *)
								mask := mask * {12..матМаксимум(мнвоНаБитахМЗ)};
								base := GetMTTRphysBaseLow(i) * mask;
								j := physAdr; k := physAdr+size;
								нцПока (j # k) и (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, j) * mask # base) делай увел(j, PS) кц;	(* performance! *)
								если j # k то res := 1508 всё	(* cache type of region already set *)
							иначе
								если f = -1 то f := i всё	(* first free entry *)
							всё;
							увел(i)
						кц;
						если res = Ok то
							если f # -1 то
								SetMTTRphysBase(f, {}, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, physAdr) * {12..31} + НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, type) * {0..7});
								SetMTTRphysMask(f, {0..3}, (-НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, size-1)) * {12..31} + {11})
							иначе
								res := 1506	(* out of cache control entries *)
							всё
						иначе
							(* skip *)
						всё
					иначе
						res := 1511	(* region type not supported *)
					всё
				иначе
					res := 1505	(* MTTRs not supported *)
				всё;
				ЭВМ.ОтпустиБлокировку(ЭВМ.ЯдернаяБлокировкаДляРаботыСПамятью)
			иначе
				res := 1510	(* region base must be aligned on size *)
			всё
		иначе
			res := 1509	(* region size must be power of 2 *)
		всё
	иначе
		res := 1507	(* implementation restriction - fixed entries not supported *)
	всё
кон LocalSetCacheProperties;

проц HandleSetCacheProperties(id: цел32; конст state: ЭВМ.СостояниеПроцессора; msg: ЭВМ.Message);
нач
	просейТип msg: SetCacheMessage делай
		(* to do: page 11-25 *)
		LocalSetCacheProperties(msg.physAdr, msg.size, msg.type, msg.res[id])
	всё
кон HandleSetCacheProperties;

(** Broadcast a LocalSetCacheProperties operation to all processors. *)

проц GlobalSetCacheProperties*(physAdr: адресВПамяти; size, type: цел32; перем res: целМЗ);
перем i: цел32; msg: SetCacheMessage;
нач
	нов(msg); msg.physAdr := physAdr; msg.size := size; msg.type := type;
	нцДля i := 0 до ЭВМ.МаксКвоПроцессоров-1 делай msg.res[i] := 2304 кц;	(* default result *)
	ЭВМ.Broadcast(HandleSetCacheProperties, msg, {ЭВМ.Self, ЭВМ.FrontBarrier, ЭВМ.BackBarrier});
	res := 0;
	нцДля i := 0 до ЭВМ.МаксКвоПроцессоров-1 делай
		если (res = 0) и (msg.res[i] # 0) то res := msg.res[i] всё	(* return first non-ok result found *)
	кц
кон GlobalSetCacheProperties;

(** Disable all caching on the current processor. *)

проц LocalDisableCaching*;
машКод {SYSTEM.Pentium, SYSTEM.Privileged}
	PUSHFD
	CLI

	MOV EAX, CR0
	OR EAX, 40000000H
	AND EAX, 0DFFFFFFFH
	MOV CR0, EAX

	WBINVD

	MOV EAX, CR4
	AND EAX, 0FFFFFF7FH
	MOV CR4, EAX

	MOV EAX, CR3
	MOV CR3, EAX

	MOV ECX, 2FFH	; MTTRdefType
	MOV EAX, 0
	MOV EDX, 0
	WRMSR

	WBINVD

	MOV EAX, CR3
	MOV CR3, EAX

	MOV EAX, CR0
	OR EAX, 60000000H
	MOV CR0, EAX

	POPFD
кон LocalDisableCaching;

проц HandleDisableCaching(id: цел32; конст state: ЭВМ.СостояниеПроцессора; msg: ЭВМ.Message);
нач
	LocalDisableCaching
кон HandleDisableCaching;

(** Broadcast a LocalDisableCaching operation to all processors. *)

проц GlobalDisableCaching*;
нач
	ЭВМ.Broadcast(HandleDisableCaching, НУЛЬ, {ЭВМ.Self, ЭВМ.FrontBarrier, ЭВМ.BackBarrier})
кон GlobalDisableCaching;

кон MemCache.

(*
to do:
o change error codes
*)

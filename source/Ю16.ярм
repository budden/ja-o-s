(* UCS2 ~= UTF16 *)
модуль Ю16; 

использует Ю32;

(* Название УНС2 =  UCS2 подчеркивает, что полная поддержка суррогатных пар может отсутствовать. 
   http://www.unicode.org/faq/utf_bom.html#utf16-11

			См. также Ю16_2 - там есть процедуры.
   *) 
   
(* Этот модуль должен грузиться рано по течению сборки и не быть связан с подобными модулями для других типов *)
	
конст СмещениеДляСуррогатнойПары* = логСдвиг( бцел32( 0xD800 ), 10 ) + 0xDC00 - 0x10000;
	КодКП* = 0;
	(** при преобразовании из УНС2 (UCS2) в ФПЮ-8 (UTF-8) строки длины N нужно запасти
	буфер длины КоэфтРаздуванияУНС2вЮ8*N. Коэффициент наверняка завышен, т.к. он взят из 32-разрядного 
	UCS32, но сейчас некогда этим заниматься *)
	КоэфтРаздуванияУНС2вФПЮ8* = Ю32.КоэфтРаздуванияИзСтрокиВ_Ю8;
	
тип 
	Симв* = запись кодСимв- : бцел16 кон;
	Строка* = массив из Симв;
	уСтрока* = укль на Строка;
	LPWSTR* = адресВПамяти;

проц СимвИзКода*(iCode : бцел16) : Симв;
 	перем rez : Симв;
	нач	rez.кодСимв := iCode; возврат rez кон СимвИзКода;
	
перем ЗаменяющийСимвол*, Симв0* : Симв;

проц - СамостоятельныйСимволЛи¿( c: Симв ): булево;
нач
	возврат мнвоНаБитах32( c.кодСимв ) * мнвоНаБитах32( 0xFFFFF800 ) # мнвоНаБитах32( 0xD800 );
кон СамостоятельныйСимволЛи¿;

проц - СуррогатВедущийЛи¿( c: Симв ): булево;
нач
	возврат мнвоНаБитах32( c.кодСимв ) * мнвоНаБитах32( 0x400 ) = { };
кон СуррогатВедущийЛи¿;

проц - ВерхнийСуррогатЛи¿( c: Симв ): булево; (* D800–DBFF *)
нач
	возврат мнвоНаБитах32( c.кодСимв ) * мнвоНаБитах32( 0xFFFFFC00 ) = мнвоНаБитах32( 0xD800 );
кон ВерхнийСуррогатЛи¿;

проц - НижнийСуррогатЛи¿( c: Симв ): булево; (* DC00–DFFF *)
нач
	возврат мнвоНаБитах32( c.кодСимв ) * мнвоНаБитах32( 0xFFFFFC00 ) = мнвоНаБитах32( 0xDC00 );
кон НижнийСуррогатЛи¿;

проц - ГоловаСуррогатнойПарыПоКодуСимв32*( supplementary: бцел32 ): бцел16;
нач
	возврат бцел16( логСдвиг( supplementary, -10 ) + 0xD7C0 );
кон ГоловаСуррогатнойПарыПоКодуСимв32;

проц - ХвостСуррогатнойПарыПоКодуСимв32*( supplementary: бцел32 ): бцел16;
нач
	возврат бцел16( мнвоНаБитах32( supplementary ) * мнвоНаБитах32( 0x3FF ) + мнвоНаБитах32( 0xDC00 ));
кон ХвостСуррогатнойПарыПоКодуСимв32; 
	
(* JQvNovU2 заменяет слишком большие коды на символ-заместитель *)
проц Ю32_вСвежуюЮ16*( конст vkh : Ю32.Строка) : уСтрока;
перем b : Строкопостроитель; i : размерМЗ; kod : бцел32;
нач
	нов(b, матМаксимум(1,длинаМассива(vkh)));
	i := 0;
	нц
		если i = длинаМассива(vkh) - 1 то
			b.пСимв(0); прервиЦикл всё;
		kod := vkh[i].UCS32CharCode;
		если kod >= матМаксимум(бцел16) то
			b.пСимв(кодСимв8('?'));
		аесли kod = Ю32.КодКП то
			b.пСимв(Ю32.КодКП); прервиЦикл
		иначе
			b.пСимв(бцел16(kod)) всё;
		увел(i) кц;
	возврат b.дайСвежуюКопиюСтроки() кон Ю32_вСвежуюЮ16;
	
	
(* Обязательно завершаем нулём. Если места не хватит, придётся обрезать! *)
проц U8вU2ОбрезаяИДобиваяНулём*(u8: массив из симв8; перем u2: Строка); 
перем промеж: Ю32.уСтрока; i : размерМЗ; kod : бцел32;
нач
	промеж := Ю32.Ю8_вСвежуюСтроку(u8);
	i := 0;
	утв(длинаМассива(u2)>0);
	нц
		если (i = длинаМассива(u2) - 1) или (i = длинаМассива(u8) - 1) то
			u2[i].кодСимв := Ю32.КодКП; прервиЦикл всё;
		kod := промеж[i].UCS32CharCode;
		если kod >= матМаксимум(бцел16) то
			u2[i].кодСимв := кодСимв8('?');
		аесли kod = Ю32.КодКП то
			u2[i].кодСимв := Ю32.КодКП; прервиЦикл
		иначе
			u2[i].кодСимв := бцел16(kod) всё;
		увел(i) кц;
	возврат кон U8вU2ОбрезаяИДобиваяНулём;
	
проц U2вU8ОбрезаяИДобиваяНулём*(u2: Строка; перем u8: массив из симв8);
нач копируйСтрокуДо0(Ю16_вСвежуюЮ8(u2)^,u8) кон U2вU8ОбрезаяИДобиваяНулём;

(* см. примечание у U8vNovU2 *)	
проц Ю16_вСвежуюЮ8*(конст u2 : Строка) : укль на массив из симв8;
перем промеж: Ю32.уСтрока;
нач
	промеж := Ю16_вСвежуюЮ32(u2);
	возврат Ю32.СтрокуВСвежуюЮ8(промеж^) кон Ю16_вСвежуюЮ8;

(* U8vNovU2 копирует строку в UTF8 на кучу в UCS2. Безобразие в том, что
  это делается через промежуточную StringJQ *)
проц Ю8_вСвежуюЮ16*(u8 : массив из симв8) : уСтрока;
перем промеж: Ю32.уСтрока;
нач
	промеж := Ю32.Ю8_вСвежуюСтроку(u8);
	возврат Ю32_вСвежуюЮ16(промеж^) кон Ю8_вСвежуюЮ16;

	
проц Ю16_вСвежуюЮ32*( конст clipA2UCS2String: Строка ): Ю32.уСтрока;
перем
	rez: Ю32.уСтрока;
	clipboardPos := 0, textPos := 0, clipboardLen := 0: размерМЗ;
	codePoint : бцел32; lead, trail: Симв;
нач
	clipboardLen := длинаМассива(clipA2UCS2String);
	нов(rez, clipboardLen + 1);
	нцПока clipboardPos < clipboardLen делай
		codePoint := ЗаменяющийСимвол.кодСимв;
		lead := clipA2UCS2String[ clipboardPos ];

		(* make unicode *)
		если СамостоятельныйСимволЛи¿( lead ) то
			codePoint := lead.кодСимв;
			увел( clipboardPos );
		иначе
			если СуррогатВедущийЛи¿( lead ) то
				увел( clipboardPos );
				если ( clipboardPos < clipboardLen ) то
					trail := clipA2UCS2String[ clipboardPos ];
					если НижнийСуррогатЛи¿( trail ) то
						codePoint := логСдвиг( lead.кодСимв, 10 ) + trail.кодСимв - СмещениеДляСуррогатнойПары;
						увел( clipboardPos );
					всё;
				всё;
			аесли clipboardPos > 0 то
				trail := clipA2UCS2String[ clipboardPos - 1 ];
				если ВерхнийСуррогатЛи¿( trail ) то
					codePoint := логСдвиг( trail.кодСимв, 10 ) + lead.кодСимв - СмещениеДляСуррогатнойПары;
				всё;
				увел( clipboardPos );
			всё;
		всё;

		(* CRLF -> LF *)
		если ( codePoint = Ю32.КодВК ) то
			codePoint := Ю32.КодПС;
			если ( clipboardPos < clipboardLen ) и ( clipA2UCS2String[ clipboardPos ].кодСимв = Ю32.КодПС ) то
				увел( clipboardPos );
			всё;
		всё;

		(* accept codePoint *)
		rez[ textPos ] := Ю32.СимвИзКода( codePoint );
		увел( textPos );
	кц;

	если textPos # 0 то
		rez[ textPos ] := Ю32.СимвИзКода(Ю32.КодКП);
	всё;
	возврат rez
кон Ю16_вСвежуюЮ32;


тип
	Строкопостроитель* = окласс
	перем
		length: размерМЗ;
		data: уСтрока;

		проц &новСтрокопостроитель*( начальныйРазмер: размерМЗ );
		нач
			если начальныйРазмер < 260 то начальныйРазмер := 260 всё;
			нов( data, начальныйРазмер );
			data[ 0 ] := Симв0; 
			length := 0;
		кон новСтрокопостроитель;

		проц СотриВсё*;
		нач
			data[ 0 ] := Симв0;
			length := 0;
		кон СотриВсё;

		проц пСимв*( codePoint: бцел16 );
		перем i, newLength: размерМЗ; newData: уСтрока;
		нач
			если length + 2 >= длинаМассива( data ) то
				newLength := матМинимум(матМаксимум(размерМЗ) - 2, length + матМаксимум( 16, length DIV 2 ));
				нов( newData, newLength );
				нцДля i := 0 до length - 1 делай newData[ i ] := data[ i ]; кц;
				data := newData;
			всё;
			data[ length ] := СимвИзКода(codePoint);
			увел( length );
			data[ length ] := Симв0;
		кон пСимв;

		проц дайСвежуюКопиюСтроки*( ): уСтрока;
		перем rez : уСтрока; i : размерМЗ;
		нач
			нов(rez, length);
			нцДля i := 0 до length - 1 делай rez[i] := data[i] кц;
			возврат data
		кон дайСвежуюКопиюСтроки;

	кон Строкопостроитель;
	
нач
ЗаменяющийСимвол := СимвИзКода( 0xFFFD );
кон Ю16.


модуль WebBrowserPanel; (** AUTHOR "Simon L. Keel"; PURPOSE "components for loading and displaying web-pages"; *)

использует
	HTMLScanner, HTMLParser, HTMLTransformer, WebBrowserComponents,
	WMComponents, WMProperties, WMTextView, WMStandardComponents, Texts, WMEvents, TextUtilities,
	Codecs, XML, XMLObjects, Strings, ЛогЯдра, Messages := WMMessages;

конст
	verbose = истина;
	modeSourceCode = 0;
	modeParsedHtml = 1;
	modeBbtXml = 2;
	modeBbtText = 3;
	outputMode = modeBbtText;
	typePercent = 0;
	typeParts = 1;
	typeFix = 2;

тип
	String = Strings.String;
	VisualComponent = WMComponents.VisualComponent;

	NotifyMsg* = укль на запись
		url* : String;
		title* : String;
		loadID* : цел32;
	кон;

	LoadedMsg* = укль на запись
		vc : VisualComponent;
		url : String;
		title : String;
	кон;

	FrameNode = укль на запись
		next : FrameNode;
		name : String;
		scrolling : булево;
		size : цел32;
		relative : булево;
		panel : VisualComponent;
		isLast : булево;
	кон;

	FrameslotNode = укль на запись
		next : FrameslotNode;
		size : цел32;
		type : цел32;
	кон;

	FramesetNode = укль на запись
		next : FramesetNode;
		frameset : XML.Element;
		src : String;
		name : String;
		scrolling : булево;
	кон;

	WebPanel* = окласс (VisualComponent)
	перем
		url- : WMProperties.StringProperty;
		notify* : WMEvents.EventListener;
		openNewWindow* : проц {делегат} (url : String);
		loadLink* : WMEvents.EventListener;
		vc : VisualComponent;
		loadingText : WebBrowserComponents.ShortText;
		pending : булево;
		loadID : цел32;

		проц &{перекрыта}Init*;
		перем
			s : String;
		нач
			Init^;
			takesFocus.Set(ложь);
			нов(url, НУЛЬ, Strings.NewString("WebPanel URL"), Strings.NewString("Stores the URL of a WebPanel"));
			s := Strings.NewString("   Loading...");
			нов(loadingText, s^);
			pending := ложь;
		кон Init;

		проц Load*(loadID : цел32);
		перем
			cl : ContentLoader;
		нач  (*{EXCLUSIVE}  ??*)
			если ~pending то
				pending := истина;
				сам.loadID := loadID;
				если vc # НУЛЬ то
					RemoveContent(vc);
				всё;
				AddContent(loadingText);
				Reset(сам, НУЛЬ);
				AlignSubComponents();
				Invalidate();
				нов(cl, сам);
			всё;
		кон Load;

		проц Loaded(sender, data : динамическиТипизированныйУкль);
		перем
			msg : LoadedMsg;
			notifyMsg : NotifyMsg;
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.Loaded, sender, data)
			иначе
				RemoveContent(loadingText);
				msg := data(LoadedMsg);
				vc := msg.vc;
				AddContent(vc);
				нов(notifyMsg);
				notifyMsg.url := msg.url;
				notifyMsg.title := msg.title;
				notifyMsg.loadID := loadID;
				notify(сам, notifyMsg);
				Reset(сам, НУЛЬ);
				AlignSubComponents();
				Invalidate();
				pending := ложь;
			всё;
		кон Loaded;

		проц LoadLink(sender, data : динамическиТипизированныйУкль);
		перем
			link, target : String;
			targetLow : String;
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.LoadLink, sender, data)
			иначе
				DecodeLinkData(data, link, target);
				если Strings.StartsWith2("#", link^) то
					(* TODO: notify WebBrowser.Window about new URL !! *)
				иначе
					targetLow := Strings.LowerCaseInNew(target^);
					если (target^ = "") или (targetLow^ = "_self") или (targetLow^ = "_top") или (targetLow^ = "_parent") то
						если loadLink # НУЛЬ то
							loadLink(сам, data);
						иначе
							url.Set(link);
							Load(-1);
						всё;
					иначе
						если openNewWindow # НУЛЬ то
							openNewWindow(link);
						всё;
					всё;
				всё;
			всё;
		кон LoadLink;

	кон WebPanel;

	ContentLoader = окласс
	перем
		webPanel : WebPanel;
		msg : LoadedMsg;
		vc : VisualComponent;
		url : String;
		title : String;
		encodedUrl: массив 1024 из симв8;

		проц &New*(webPanel : WebPanel);
		нач
			сам.webPanel := webPanel;
		кон New;

	нач {активное}
		нов(msg);
		msg.url := webPanel.url.Get();
		если verbose то ЛогЯдра.пСтроку8("ContentLoader: Loading: "); ЛогЯдра.пСтроку8(msg.url^); ЛогЯдра.пВК_ПС; всё;
		msg.vc := GetContent(msg.url, msg.title, webPanel.bounds.GetWidth(), webPanel.bounds.GetHeight(), истина, webPanel.LoadLink, НУЛЬ);
		webPanel.Loaded(сам, msg);
		если verbose то ЛогЯдра.пСтроку8("ContentLoader: Loading done."); ЛогЯдра.пВК_ПС; всё;
	кон ContentLoader;

	HTMLPanel = окласс (VisualComponent)
	перем
		rc : WebBrowserComponents.ResourceConnection;
		width : размерМЗ;
		height : размерМЗ;
		scrollbars : булево;
		loadLink : WMEvents.EventListener;
		charset : String;
		frameName : String;
		firstResize : булево;
		tv : WMTextView.TextView;
		text : Texts.Text;
		blankText : Texts.Text;
		vScrollbar : WMStandardComponents.Scrollbar;
		hScrollbar : WMStandardComponents.Scrollbar;
		scanner : HTMLScanner.Scanner;
		parser : HTMLParser.Parser;
		transformer : HTMLTransformer.Transformer;
		xmlDoc : XML.Document;
		textWriter : TextUtilities.TextWriter;
		bgImage : WebBrowserComponents.TileImagePanel;
		decoder : Codecs.TextDecoder;
		bbtDecoder : TextUtilities.BluebottleDecoder;
		encoder : Codecs.TextEncoder;
		res : целМЗ;
		contents: XMLObjects.Enumerator;
		content: динамическиТипизированныйУкль;
		framesetElem : XML.Element;
		frameset : FramesetPanel;
		titleElem : XML.Element;
		item : HTMLTransformer.EmbeddedObject;

		проц &New*(перем title : String; rc :WebBrowserComponents.ResourceConnection; width : размерМЗ; height : размерМЗ; scrollbars : булево; loadLink : WMEvents.EventListener; charset : String; frameName : String);
		перем
			sharpPos : размерМЗ;
			wrp : WMTextView.LinkWrapper;
			(*sequencer : Messages.MsgSequencer;*)
		нач
			Init;
(*
			NEW(sequencer, Handle);
			SetSequencer(sequencer);
*)
			takesFocus.Set(ложь);
			сам.rc := rc;
			сам.width := width;
			сам.height := height;
			сам.scrollbars := scrollbars;
			сам.loadLink := loadLink;
			сам.charset := charset;
			сам.frameName := frameName;
			alignment.Set(WMComponents.AlignClient);
			firstResize := истина;
			Load(title);
			(* doesn't work... *)
			sharpPos := Strings.LastIndexOfByte2("#", rc.url^);
			если sharpPos # -1 то
				нов(wrp);
				wrp.link := Strings.Substring2(sharpPos, rc.url^);
				tv.LinkClicked(сам, wrp);
			всё;
		кон New;

		проц Load(перем title : String);
		нач
			если verbose то ЛогЯдра.пСтроку8("---Loading HTMLPanel. Url: "); ЛогЯдра.пСтроку8(rc.url^); ЛогЯдра.пВК_ПС(); всё;
			если outputMode = modeSourceCode то
				decoder := Codecs.GetTextDecoder("ISO8859-1");
				decoder.Open(rc.reader, res);
				нов(text);
				нов(textWriter, text);
				encoder := Codecs.GetTextEncoder("UTF-8");
				encoder.Open(textWriter);
				encoder.WriteText(decoder.GetText(), res);
				textWriter.ПротолкниБуферВПоток;
				(* Show *)
				нов(vScrollbar);
				vScrollbar.alignment.Set(WMComponents.AlignRight);
				AddContent(vScrollbar);
				нов(hScrollbar);
				hScrollbar.alignment.Set(WMComponents.AlignBottom);
				hScrollbar.vertical.Set(ложь);
				AddContent(hScrollbar);
				нов(tv);
				tv.alignment.Set(WMComponents.AlignClient);
				AddContent(tv);
				tv.SetScrollbars(hScrollbar, vScrollbar);
				tv.SetText(text);
				tv.firstLine.Set(0);
			аесли outputMode = modeParsedHtml то
				(* Parse the page *)
				нов(scanner, rc.reader);
				нов(parser, scanner);
				если verbose то ЛогЯдра.пСтроку8("-Parsing "); ЛогЯдра.пСтроку8(rc.url^); ЛогЯдра.пВК_ПС(); всё;
				xmlDoc := parser.Parse();
				если verbose то ЛогЯдра.пСтроку8("-Parsing done."); ЛогЯдра.пВК_ПС(); всё;
				нов(text);
				нов(textWriter, text);
				xmlDoc.Write(textWriter, НУЛЬ,  0);
				textWriter.ПротолкниБуферВПоток;
				(* Show *)
				нов(vScrollbar);
				vScrollbar.alignment.Set(WMComponents.AlignRight);
				AddContent(vScrollbar);
				нов(hScrollbar);
				hScrollbar.alignment.Set(WMComponents.AlignBottom);
				hScrollbar.vertical.Set(ложь);
				AddContent(hScrollbar);
				нов(tv);
				tv.alignment.Set(WMComponents.AlignClient);
				AddContent(tv);
				tv.SetScrollbars(hScrollbar, vScrollbar);
				tv.SetText(text);
				tv.firstLine.Set(0);
			аесли outputMode = modeBbtXml то
				(* Parse the page *)
				нов(scanner, rc.reader);
				нов(parser, scanner);
				если verbose то ЛогЯдра.пСтроку8("--Parsing "); ЛогЯдра.пСтроку8(rc.url^); ЛогЯдра.пВК_ПС(); всё;
				xmlDoc := parser.Parse();
				если verbose то ЛогЯдра.пСтроку8("--Parsing done."); ЛогЯдра.пВК_ПС(); всё;
				(* Transform the document *)
				нов(transformer, xmlDoc, rc.url, width, НУЛЬ, charset, НУЛЬ);
				xmlDoc := transformer.Transform();
				нов(text);
				нов(textWriter, text);
				xmlDoc.Write(textWriter, НУЛЬ, 0);
				textWriter.ПротолкниБуферВПоток;
				(* Show *)
				нов(vScrollbar);
				vScrollbar.alignment.Set(WMComponents.AlignRight);
				AddContent(vScrollbar);
				нов(hScrollbar);
				hScrollbar.alignment.Set(WMComponents.AlignBottom);
				hScrollbar.vertical.Set(ложь);
				AddContent(hScrollbar);
				нов(tv);
				tv.alignment.Set(WMComponents.AlignClient);
				AddContent(tv);
				tv.SetScrollbars(hScrollbar, vScrollbar);
				tv.SetText(text);
				tv.firstLine.Set(0);
			иначе (* outputMode = modeBbtText *)
				(* Parse the page *)
				нов(scanner, rc.reader);
				нов(parser, scanner);
				если verbose то ЛогЯдра.пСтроку8("---Parsing "); ЛогЯдра.пСтроку8(rc.url^); ЛогЯдра.пВК_ПС(); всё;
				xmlDoc := parser.Parse();
				если verbose то ЛогЯдра.пСтроку8("---Parsing done."); ЛогЯдра.пВК_ПС(); всё;
				(* Check for FRAMESET *)
				contents := xmlDoc.GetContents();
				нцПока contents.HasMoreElements() и (framesetElem = НУЛЬ) делай
					content := contents.GetNext();
					если content суть XML.Element то
						framesetElem := GetElement("FRAMESET", content(XML.Element));
					всё;
				кц;
				если framesetElem = НУЛЬ то
					(* Transform the document *)
					нов(transformer, xmlDoc, rc.url, width, loadLink, charset, frameName);
					xmlDoc := transformer.Transform();
					title := transformer.title;
					fillColor.Set(transformer.pageBgColor * 0100H + 0FFH);
					если transformer.bgImage # НУЛЬ то
						нов(bgImage, НУЛЬ, transformer.bgImage);
						AddContent(bgImage);
					всё;
					нов(bbtDecoder);
					bbtDecoder.OpenXML(xmlDoc);
					text := bbtDecoder.GetText();
					(* Show *)
					нов(tv);
					tv.alignment.Set(WMComponents.AlignClient);
					tv.onLinkClicked.Add(loadLink);
					если scrollbars то
						нов(vScrollbar);
						vScrollbar.alignment.Set(WMComponents.AlignRight);
						AddContent(vScrollbar);
						нов(hScrollbar);
						hScrollbar.alignment.Set(WMComponents.AlignBottom);
						hScrollbar.vertical.Set(ложь);
						AddContent(hScrollbar);
						tv.SetScrollbars(hScrollbar, vScrollbar);
					всё;
					AddContent(tv);
					tv.SetText(text);
					tv.firstLine.Set(0);
				иначе
					нов(frameset, framesetElem, rc.url, width, height, loadLink);
					AddContent(frameset);
					contents := xmlDoc.GetContents();
					нцПока contents.HasMoreElements() и (titleElem = НУЛЬ) делай
						content := contents.GetNext();
						если content суть XML.Element то
							titleElem := GetElement("TITLE", content(XML.Element));
						всё;
					кц;
					если titleElem # НУЛЬ то
						title := НУЛЬ;
						contents := titleElem.GetContents();
						нцПока contents.HasMoreElements() и (title = НУЛЬ) делай
							content := contents.GetNext();
							если content суть XML.ArrayChars то
								title := content(XML.ArrayChars).GetStr();
								Strings.TrimWS(title^);
								title := HTMLTransformer.TransformCharEnt(title);
							всё;
						кц;
					всё;
				всё;
			всё;
			rc.Stop();
			rc.Release();
		кон Load;

		проц {перекрыта}Resized*;
		перем
			item : HTMLTransformer.EmbeddedObject;
			width : размерМЗ;
		нач
			Resized^;
			(* ignore first resize, because webPanel is not drawn for the first time yet! *)
			если firstResize то firstResize := ложь; возврат всё;
			если transformer # НУЛЬ то
				width := bounds.GetWidth();
				item := transformer.embeddedObjectsList;
				нцПока item # НУЛЬ делай
					если item.object суть WebBrowserComponents.HR то
						item.object(WebBrowserComponents.HR).ParentTvWidthChanged(width);
					аесли item.object суть HTMLTransformer.Table то
						item.object(HTMLTransformer.Table).ParentTvWidthChanged(width);
					всё;
					item := item.prev;
				кц;
				если tv # НУЛЬ то
					(* Resets the TextView, such that all embedded objects are new aligned! *)
					tv.SetText(text);
				всё;
			всё;
		кон Resized;

	кон HTMLPanel;

	FramesetPanel = окласс (VisualComponent)
	перем
		framesetElem : XML.Element;
		baseAddress : String;
		width : размерМЗ;
		height : размерМЗ;
		loadLink : WMEvents.EventListener;
		frameborderSize : цел32;
		totalFixSizes : цел32;
		nodeIsCol : булево;
		firstFrame : FrameNode;

		проц &New*(framesetElem : XML.Element; baseAddress : String; width : размерМЗ; height : размерМЗ; loadLink : WMEvents.EventListener);
		нач
			Init;
			сам.framesetElem := framesetElem;
			сам.baseAddress := baseAddress;
			сам.width := width;
			сам.height := height;
			сам.loadLink := loadLink;
			takesFocus.Set(ложь);
			alignment.Set(WMComponents.AlignClient);
			BuildFrameList();
			AddFramesToPanel();
		кон New;

		проц BuildFrameList;
		перем
			frameItem : FrameNode;
			framesetItem : FramesetNode;
			url, dummyTitle : String;
			frameSlots : FrameslotNode;
			framesets : FramesetNode;
			slotItem : FrameslotNode;
			framesetPanel : FramesetPanel;
			fWidth, fHeight : размерМЗ;
			lastFrame : FrameNode;
		нач
			ParseFramesetAttr(framesetElem, frameSlots, frameborderSize, totalFixSizes, nodeIsCol);
			ParseFramesetContent(framesetElem, framesets);

			(* add frames and framesets to frame-list *)
			framesetItem := framesets;
			slotItem := frameSlots;
			нцПока (slotItem # НУЛЬ) и (framesetItem # НУЛЬ) делай
				нов(frameItem);
				frameItem.name := framesetItem.name;
				frameItem.scrolling := framesetItem.scrolling;
				frameItem.size := slotItem.size;
				если slotItem.type = typeFix то
					frameItem.relative := ложь;
				иначе
					frameItem.relative := истина;
				всё;
				fWidth := GetFrameWidth(frameItem);
				fHeight := GetFrameHeight(frameItem);
				(* new frame or frameset *)
				если framesetItem.frameset # НУЛЬ то
					нов(framesetPanel, framesetItem.frameset, baseAddress, fWidth, fHeight, LoadLink);
					frameItem.panel := framesetPanel;
				иначе
					если (frameItem.name = НУЛЬ) или (frameItem.name^ = "") то
						frameItem.name := GetNewFrameName();
					всё;
					url := HTMLTransformer.ResolveAddress(baseAddress, framesetItem.src);
					frameItem.panel := GetContent(url, dummyTitle, fWidth, fHeight, frameItem.scrolling, LoadLink, frameItem.name);
				всё;
				frameItem.isLast := ложь;
				если lastFrame # НУЛЬ то
					lastFrame.next := frameItem;
				иначе
					firstFrame := frameItem;
				всё;
				lastFrame := frameItem;

				slotItem := slotItem.next;
				framesetItem := framesetItem.next;
			кц;
			если lastFrame # НУЛЬ то
				lastFrame.isLast := истина;
			всё;

			(* set frame alignment *)
			frameItem := firstFrame;
			нцПока frameItem # НУЛЬ делай
				AlignFrame(frameItem);
				frameItem := frameItem.next;
			кц;
		кон BuildFrameList;

		проц GetFrameWidth(frameItem : FrameNode) : размерМЗ;
		перем
			fWidth : размерМЗ;
		нач
			(* calculate frame-width *)
			если nodeIsCol то
				если frameItem.relative то
					fWidth := округлиВниз((width-totalFixSizes) / 100 * frameItem.size) - frameborderSize;
				иначе
					fWidth := frameItem.size - frameborderSize;
				всё;
			иначе
				fWidth := width;
			всё;
			если fWidth < 1 то fWidth := 1 всё;
			возврат fWidth;
		кон GetFrameWidth;

		проц GetFrameHeight(frameItem : FrameNode) : размерМЗ;
		перем
			fHeight : размерМЗ;
		нач
			(* calculate frame-height *)
			если ~nodeIsCol то
				если frameItem.relative то
					fHeight := округлиВниз((height-totalFixSizes) / 100 * frameItem.size) - frameborderSize;
				иначе
					fHeight := frameItem.size - frameborderSize;
				всё;
			иначе
				fHeight := height;
			всё;
			если fHeight < 1 то fHeight := 1 всё;
			возврат fHeight;
		кон GetFrameHeight;

		проц AlignFrame(frameItem : FrameNode);
		перем
			resizer: WMStandardComponents.Resizer;
		нач
			если ~frameItem.isLast то
				(* add resizer and set its width *)
				нов(resizer);
				frameItem.panel.AddContent(resizer);
				если nodeIsCol то
					resizer.alignment.Set(WMComponents.AlignRight);
					resizer.bounds.SetWidth(frameborderSize);
				иначе
					resizer.alignment.Set(WMComponents.AlignBottom);
					resizer.bounds.SetHeight(frameborderSize);
				всё;
				(* set frame alignment *)
				если nodeIsCol то
					frameItem.panel.alignment.Set(WMComponents.AlignLeft);
				иначе
					frameItem.panel.alignment.Set(WMComponents.AlignTop);
				всё;
			иначе
				(* set frame alignment for last frame*)
				frameItem.panel.alignment.Set(WMComponents.AlignClient);
			всё;
		кон AlignFrame;

		проц Resize;
		перем
			frameItem : FrameNode;
		нач
			frameItem := firstFrame;
			нцПока frameItem # НУЛЬ делай
				если nodeIsCol то
					если frameItem.relative то
						frameItem.panel.bounds.SetWidth(округлиВниз((width-totalFixSizes) / 100 * frameItem.size));
					иначе
						frameItem.panel.bounds.SetWidth(frameItem.size);
					всё;
				иначе
					если frameItem.relative то
						frameItem.panel.bounds.SetHeight(округлиВниз((height-totalFixSizes) / 100 * frameItem.size));
					иначе
						frameItem.panel.bounds.SetHeight(frameItem.size);
					всё;
				всё;
				frameItem := frameItem.next;
			кц;
		кон Resize;

		проц {перекрыта}Resized*;
		нач
			width := bounds.GetWidth();
			height := bounds.GetHeight();
			Resize;
			Resized^;
		кон Resized;

		проц RemoveFramesFromPanel;
		перем
			frameItem : FrameNode;
		нач
			frameItem := firstFrame;
			нцПока frameItem # НУЛЬ делай
				RemoveContent(frameItem.panel);
				frameItem := frameItem.next;
			кц;
		кон RemoveFramesFromPanel;

		проц AddFramesToPanel;
		перем
			frameItem : FrameNode;
		нач
			frameItem := firstFrame;
			нцПока frameItem # НУЛЬ делай
				AddContent(frameItem.panel);
				frameItem := frameItem.next;
			кц;
			Resize();
		кон AddFramesToPanel;

		проц LoadLink(sender, data : динамическиТипизированныйУкль);
		перем
			link, target, 	targetLow : String;
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.LoadLink, sender, data)
			иначе
				DecodeLinkData(data, link, target);
				если ~Strings.StartsWith2("#", link^) то
					targetLow := Strings.LowerCaseInNew(target^);
					если (targetLow^ = "_parent") или (targetLow^ = "_top") или (targetLow^ = "_blank") то
						loadLink(НУЛЬ, data);
					аесли ~FindAndReloadFrame(link, target) то
						loadLink(НУЛЬ, data);
					всё;
				всё;
			всё;
		кон LoadLink;

		проц FindAndReloadFrame(link, target : String) : булево;
		перем
			frameItem : FrameNode;
			framesetPanel : FramesetPanel;
			targetLow : String;
			url, dummyTitle : String;
			fWidth, fHeight : размерМЗ;
		нач
			frameItem := firstFrame;
			нцПока frameItem # НУЛЬ делай
				если frameItem.name = НУЛЬ то
					(* frameItem is a frameset *)
					framesetPanel := frameItem.panel(FramesetPanel);
					если framesetPanel.FindAndReloadFrame(link, target) то
						возврат истина;
					всё;
				иначе
					(* frameItem is a frame *)
					targetLow := Strings.LowerCaseInNew(target^);
					если (target^ = frameItem.name^) или (targetLow^ = "_self") то
						RemoveFramesFromPanel();
						fWidth := GetFrameWidth(frameItem);
						fHeight := GetFrameHeight(frameItem);
						url := HTMLTransformer.ResolveAddress(baseAddress, link);
						frameItem.panel := GetContent(url, dummyTitle, fWidth, fHeight, frameItem.scrolling, LoadLink, frameItem.name);
						AlignFrame(frameItem);
						AddFramesToPanel;
						Reset(сам, НУЛЬ);
						AlignSubComponents();
						Invalidate();
						возврат истина;
					всё;
				всё;
				frameItem := frameItem.next;
			кц;
			возврат ложь;
		кон FindAndReloadFrame;

	кон FramesetPanel;


перем
	frameNameCount : цел32;

проц GetContent(перем url : String; перем title : String; initWidth : размерМЗ; initHeight : размерМЗ; scrollbars : булево; loadLink : WMEvents.EventListener; frameName : String) : VisualComponent;
перем
	rc : WebBrowserComponents.ResourceConnection;
	panel : VisualComponent;
	errorText : WebBrowserComponents.ShortText;
	image : WebBrowserComponents.StretchImagePanel;
	htmlPanel : HTMLPanel;
	textPanel : WebBrowserComponents.TextPanel;
	s : String;
	charset : String;
	charsetPos : размерМЗ;
нач

	rc := WebBrowserComponents.GetResourceConnection(url);
	если rc = НУЛЬ то
		если verbose то ЛогЯдра.пСтроку8("Not found: "); ЛогЯдра.пСтроку8(url^); ЛогЯдра.пВК_ПС; всё;
		s := Strings.ConcatToNew("   Not found: ", url^);
		нов(errorText, s^);
		panel := errorText;
	иначе
		url := rc.url;
		если Strings.StartsWith2("text/html", rc.mimeType^) или (rc.mimeType^ = "") то
			s := Strings.LowerCaseInNew(rc.mimeType^);
			если Strings.Pos("charset", s^) >= 0 то
				charsetPos := Strings.IndexOfByte('=', charsetPos, rc.mimeType^) + 1;
				если (charsetPos >= 0) и (charsetPos < Strings.Length(rc.mimeType^)) то
					charset := Strings.Substring2(charsetPos, rc.mimeType^);
					Strings.TrimWS(charset^);
				всё;
			всё;
			нов(htmlPanel, title, rc, initWidth, initHeight, scrollbars, loadLink, charset, frameName);
			panel := htmlPanel;
		аесли Strings.StartsWith2("text/plain", rc.mimeType^) или Strings.StartsWith2("application/xml", rc.mimeType^) то
			нов(textPanel, rc, НУЛЬ);
			panel := textPanel;
		аесли Strings.StartsWith2("image/", rc.mimeType^) то
			нов(image, rc, НУЛЬ, -1, -1);
			panel := image;
		иначе
			если verbose то ЛогЯдра.пСтроку8("Unknown content type: "); ЛогЯдра.пСтроку8(rc.mimeType^); ЛогЯдра.пВК_ПС; всё;
			s := Strings.ConcatToNew("   Unknown content type: ", rc.mimeType^);
			нов(errorText, s^);
			panel := errorText;
			rc.Stop();
			rc.Release();
		всё;
	всё;
	возврат panel;
кон GetContent;

проц DecodeLinkData*(data : динамическиТипизированныйУкль; перем link : String; перем target : String);
перем
	linkValue : String;
	urlPos, len : размерМЗ;
нач
	linkValue := data(WMTextView.LinkWrapper).link;
	если Strings.StartsWith2("#", linkValue^) то
		link := linkValue;
		target := Strings.NewString("_self");
	иначе
		urlPos := Strings.Pos(";url=", linkValue^);
		target := Strings.Substring(7, urlPos, linkValue^);
		len := Strings.Length(linkValue^);
		если len > (urlPos + 5) то
			link := Strings.Substring(urlPos + 5, len, linkValue^);
		иначе
			link := Strings.NewString("");
		всё;
	всё;
кон DecodeLinkData;

проц GetElement(name : массив из симв8; root : XML.Element) : XML.Element;
перем
	rootName : String;
	contents: XMLObjects.Enumerator;
	content: динамическиТипизированныйУкль;
	retElement: XML.Element;
нач
	если root = НУЛЬ то возврат НУЛЬ; всё;
	rootName := root.GetName();
	если rootName^ = name то возврат root; всё;
	contents := root.GetContents();
	нцПока contents.HasMoreElements() и (retElement = НУЛЬ) делай
		content := contents.GetNext();
		если content суть XML.Element то
			retElement := GetElement(name, content(XML.Element));
		всё;
	кц;
	возврат retElement;
кон GetElement;

проц ParseFramesetAttr(frameset : XML.Element; перем frameSlots : FrameslotNode; перем frameborderSize : цел32; перем fixSizes : цел32; перем nodeIsCol : булево);
перем
	s : String;
	rowsNode : FrameslotNode;
	colsNode : FrameslotNode;
	rowsCount : цел32;
	colsCount : цел32;
	rowsfixSizes : цел32;
	colsfixSizes : цел32;
нач
	rowsCount := 0;
	colsCount := 0;
	s := HTMLTransformer.GetElemAttributeValue(frameset, "rows", ложь);
	если s # НУЛЬ то
		ParseFramesetRowsOrCols(s, rowsNode, rowsCount, rowsfixSizes);
	всё;
	s := HTMLTransformer.GetElemAttributeValue(frameset, "cols", ложь);
	если s # НУЛЬ то
		ParseFramesetRowsOrCols(s, colsNode, colsCount, colsfixSizes);
	всё;
	если (rowsCount = 0) и (colsCount = 0) то
		нов(frameSlots);
		fixSizes := 0;
		nodeIsCol := ложь;
		frameSlots.size := 100;
		frameSlots.type := typePercent;
	аесли rowsCount >= colsCount то
		frameSlots := rowsNode;
		fixSizes := rowsfixSizes;
		nodeIsCol := ложь;
	иначе
		frameSlots := colsNode;
		fixSizes := colsfixSizes;
		nodeIsCol := истина;
	всё;

	(* frameborder-width *)
	frameborderSize := 6;
	s := HTMLTransformer.GetElemAttributeValue(frameset, "frameborder", ложь);
	если s # НУЛЬ то
		Strings.TrimWS(s^);
		Strings.StrToInt(s^, frameborderSize);
	иначе
		s := HTMLTransformer.GetElemAttributeValue(frameset, "border", ложь);
		если s # НУЛЬ то
			Strings.TrimWS(s^);
			Strings.StrToInt(s^, frameborderSize);
		всё;
	всё;

кон ParseFramesetAttr;

проц ParseFramesetRowsOrCols(attrValue : String; перем firstSlot : FrameslotNode; перем nodeCount : цел32; перем fixSizes : цел32);
перем
	start : размерМЗ;
	comma : размерМЗ;
	s : String;
	sizeStr : String;
	size : цел32;
	type : цел32;
	slotItem : FrameslotNode;
	lastSlot : FrameslotNode;
	prevSlot : FrameslotNode;
	percents : цел32;
	parts : цел32;
	factor : вещ32;
	onePart : цел32;
нач
	percents := 0;
	parts := 0;
	fixSizes := 0;
	firstSlot := НУЛЬ;

	start := 0;
	нцДо
		comma := Strings.IndexOfByte(',', start, attrValue^);
		если comma = -1 то
			s := Strings.Substring2(start, attrValue^);
		иначе
			s := Strings.Substring(start, comma, attrValue^);
			start := comma+1;
		всё;
		Strings.TrimWS(s^);
		если Strings.EndsWith("%", s^) то
			sizeStr := Strings.Substring(0, Strings.Length(s^)-1, s^);
			Strings.StrToInt(sizeStr^, size);
			type := typePercent;
		аесли Strings.EndsWith("*", s^) то
			если Strings.Length(s^) = 1 то
				size := 1;
			иначе
				sizeStr := Strings.Substring(0, Strings.Length(s^)-1, s^);
				Strings.TrimWS(sizeStr^);
				Strings.StrToInt(sizeStr^, size);
			всё;
			type := typeParts;
		иначе
			Strings.StrToInt(s^, size);
			type := typeFix;
		всё;
		если size > 0 то
			нов(slotItem);
			slotItem.size := size;
			slotItem.type := type;
			если type = typePercent то
				percents := percents + size;
			аесли type = typeParts то
				parts := parts + size;
			иначе
				fixSizes := fixSizes + size;
			всё;
			если lastSlot # НУЛЬ то
				lastSlot.next := slotItem;
				lastSlot := slotItem;
			иначе
				firstSlot := slotItem;
				lastSlot := slotItem;
			всё;
		всё;
	кцПри comma = -1;

	если (percents > 100) или (parts = 0) то
		если percents > 0 то
			factor := 100 / percents;
		всё;
		onePart := 0;
	иначе
		factor := 1;
		onePart := округлиВниз((100 - percents) / parts);
	всё;

	nodeCount := 0;
	slotItem := firstSlot;
	prevSlot := НУЛЬ;
	нцПока slotItem # НУЛЬ делай
		если slotItem.type = typePercent то
			slotItem.size := округлиВниз(slotItem.size * factor);
		аесли slotItem.type = typeParts то
			slotItem.size := slotItem.size * onePart;
		всё;
		если slotItem.size = 0 то
			если prevSlot # НУЛЬ то
				prevSlot.next := slotItem.next;
			иначе
				(* prevSlot remains NIL *)
				firstSlot := slotItem.next;
			всё;
		иначе
			увел(nodeCount);
			prevSlot := slotItem;
		всё;
		slotItem := slotItem.next;
	кц;

кон ParseFramesetRowsOrCols;

проц ParseFramesetContent(frameset : XML.Element; перем first : FramesetNode);
перем
	last : FramesetNode;
	node : FramesetNode;
	enum : XMLObjects.Enumerator;
	p : динамическиТипизированныйУкль;
	frame : XML.Element;
	name : String;
	s : String;
нач
	first := НУЛЬ;
	last := НУЛЬ;
	enum := frameset.GetContents();
	нцПока enum.HasMoreElements() делай
		p := enum.GetNext();
		если p суть XML.Element то
			frame := p(XML.Element);
			name := frame.GetName();
			если name^ = "FRAMESET" то
				нов(node);
				node.frameset := frame;
				если last # НУЛЬ то
					last.next := node;
					last := node;
				иначе
					first := node;
					last := node;
				всё;
			аесли name^ = "FRAME" то
				s := HTMLTransformer.GetElemAttributeValue(frame, "src", ложь);
				если s # НУЛЬ то
					Strings.TrimWS(s^);
					если s^ # "" то
						нов(node);
						node.src := s;
						s := HTMLTransformer.GetElemAttributeValue(frame, "name", ложь);
						если s # НУЛЬ то
							Strings.TrimWS(s^);
							node.name := s;
						всё;
						node.scrolling := истина;
						s := HTMLTransformer.GetElemAttributeValue(frame, "scrolling", истина);
						если s # НУЛЬ то
							Strings.TrimWS(s^);
							если s^ = "no" то
								node.scrolling := ложь;
							всё;
						всё;
						если last # НУЛЬ то
							last.next := node;
							last := node;
						иначе
							first := node;
							last := node;
						всё;
					всё;
				всё;
			всё;
		всё;
	кц;
кон ParseFramesetContent;

проц GetNewFrameName() : String;
перем
	id : массив 28 из симв8;
	nr : массив 8 из симв8;
нач {единолично}
	id := "BimBrowser-Frame-ID-";
	Strings.IntToStr(frameNameCount, nr);
	Strings.Append(id, nr);
	увел(frameNameCount);
	возврат Strings.NewString(id);
кон GetNewFrameName;

нач
	frameNameCount := 0;
кон WebBrowserPanel.

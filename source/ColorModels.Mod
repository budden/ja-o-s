модуль ColorModels; (** AUTHOR ""; PURPOSE ""; *)
(* ported from Oberon.ColorModels.Mod and Oberon.Colors.Mod
 - use SIGNED32 [0,255] instead of FLOAT32 [0,1] (compatible with WMGraphics.Mod)
*)

(** convert a RedGreenBlue color specification to HueSaturationValue *)
проц RGBToHSV*(r,g,b : цел32; перем h,s,v: вещ32);
перем
	m,rl,gl,bl, rr, gg, bb: вещ32;
нач
	rr := r / 255; gg := g / 255; bb := b / 255;

	если (gg > b) то	(* set v *)
		если (rr > gg) то v:= rr; иначе v:= gg; всё;
	иначе
		если (rr > bb) то v:= rr; иначе v:= bb; всё;
	всё;
	если (gg < bb) то
		если (rr < gg) то m:= rr; иначе m:= gg; всё;
	иначе
		если (rr < bb) то m:= rr; иначе m:= bb; всё;
	всё;
	если (v # 0.0) то s:= (v - m) / v; иначе s:= 0.0; всё;	(* set s *)
	если (s # 0.0) то
		rl:= (v - rr) / (v - m);	(* distance of color from red *)
		gl:= (v - gg) / (v - m);	(* distance of color from green *)
		bl:= (v - bb) / (v - m);	(* distance of color from blue *)
		если (v = rr) то если (m = gg) то h:= 5.0 + bl; иначе h:= 1.0 - gl; всё; всё;
		если (v = gg) то
			если (m = bb) то h:= 1.0 + rl; иначе h:= 3.0 - bl; всё;
		аесли (m = rr) то
			h:= 3.0 + gl;
		иначе
			h:= 5.0 - rl;
		всё;
		h:= h * 60.0;
	иначе
		h:= 0.0;
	всё;
кон RGBToHSV;

(** convert a HueSaturationValue color specification to RedGreenBlue *)
проц HSVToRGB*(h,s,v: вещ32; перем rl,gl,bl: цел32);
перем
	i: цел32;
	f,p1,p2,p3, r, g, b: вещ32;
нач
	если h = 360.0 то h:= 0.0; иначе h:= h / 60.0; всё;	(* convert h to be in [0,6] *)
	i:= округлиВниз(h); f:= h - i;
	p1:= v * (1.0 - s); p2:= v * (1.0 - (s * f)); p3:= v * (1.0 - (s * (1.0 - f)));
	просей i из
	|	0: r:= v; g:= p3; b:= p1;
	|	1: r:= p2; g:= v; b:= p1;
	|	2: r:= p1; g:= v; b:= p3;
	|	3: r:= p1; g:= p2; b:= v;
	|	4: r:= p3; g:= p1; b:= v;
	|	5: r:= v; g:= p1; b:= p2;
	всё;
	rl := округлиВниз ( r*255 ); gl := округлиВниз ( g*255 ); bl := округлиВниз ( b*255 );
кон HSVToRGB;

(** convert a RedGreenBlue color specification to CyanMagentaYellow *)
проц RGBToCMY*(r,g,b: вещ32; перем c,m,y: вещ32);
нач
	c:= 1.0 - r/255; m:= 1.0 - g/255; y:= 1.0 - b/255;
кон RGBToCMY;

(** convert a CyanMagentaYellow color specification to RedGreenBlue *)
проц CMYToRGB*(c,m,y: вещ32; перем r,g,b: цел32);
нач
	r:= округлиВниз ( (1.0 - c)*255 ); g:= округлиВниз ( (1.0 - m)*255 ); b:= округлиВниз ( (1.0 - y)*255 );
кон CMYToRGB;

(** CMYK (Cyan Magenta Yellow blacK) model **)
проц RGBToCMYK* (rl, gl, bl: цел32; перем c, m, y, k: вещ32);
перем r, g, b : вещ32;
нач
	r := rl / 255; g := gl / 255; b := bl / 255;
	c := 1 - r; m := 1 - g; y := 1 - b;
	если r < g то
		если b < r то k := b
		иначе k := r
		всё
	иначе
		если b < g то k := b
		иначе k := g
		всё
	всё;
	c := c - k; m := m - k; y := y - k
кон RGBToCMYK;

проц CMYKToRGB* (c, m, y, k: вещ32; перем r, g, b: вещ32);
нач
	r := 1 - (k + c); g := 1 - (k + m); b := 1 - (k + y)
кон CMYKToRGB;

кон ColorModels.

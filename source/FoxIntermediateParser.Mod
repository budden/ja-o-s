модуль FoxIntermediateParser;
использует
	Strings, Diagnostics, D := Debugging, SyntaxTree := FoxSyntaxTree, Scanner := FoxScanner, Sections := FoxSections,
	IntermediateCode := FoxIntermediateCode, Basic := FoxBasic, Потоки, Files, Global := FoxGlobal;

конст
	IntermediateCodeExtension = "Fil"; (* TODO: move to a better place *)
	Trace=ложь;

тип
	MessageString= массив 256 из симв8;
	Position = Basic.Position;

	(** the intermediate code parser **)
	IntermediateCodeParser* = окласс
	конст
		l0Trace = ложь;
		Strict = истина;

	перем
		diagnostics: Diagnostics.Diagnostics;
		error: булево;
		token: Scanner.Лексема;
		scanner: Scanner.AssemblerScanner;
		system: Global.System;

		проц &Init*(l1diagnostics: Diagnostics.Diagnostics; s: Global.System);
		нач
			утв(s # НУЛЬ); (* a default system object is required in case there is no platform directive *)
			сам.diagnostics := l1diagnostics;
			system := s;
			error := ложь
		кон Init;

		проц Error(pos: Position; конст msg: массив из симв8);
		нач
			error := истина;
			Basic.Error(diagnostics, scanner.source^,pos,msg);

			D.Update;
			если l0Trace то D.TraceBack всё
		кон Error;

		проц NextToken;
		нач error := error или ~scanner.ДочитайЛексему(token)
		кон NextToken;

		проц ThisSymbol(x: Scanner.ВидЛексемы): булево;
		нач
			если ~error и (token.видЛексемы = x) то NextToken; возврат истина иначе возврат ложь всё;
		кон ThisSymbol;

		проц GetIdentifier(перем pos: Position; перем identifier: массив из симв8): булево;
		нач
			pos := token.местоВТексте;
			если token.видЛексемы # Scanner.Идентификатор то возврат ложь
			иначе копируйСтрокуДо0(token.строкаИдентификатора,identifier); NextToken; возврат истина
			всё;
		кон GetIdentifier;

		проц ExpectSymbol(x: Scanner.ВидЛексемы): булево;
		перем
			s: MessageString;
		нач
			если ThisSymbol(x) то возврат истина
			иначе
				s := "expected symbol "; Strings.Append(s,Scanner.symbols[x]); Strings.Append(s," but got "); Strings.Append(s,Scanner.symbols[token.видЛексемы]);
				Error(token.местоВТексте, s);возврат ложь
			всё;
		кон ExpectSymbol;

		проц ThisIdentifier(конст this: массив из симв8): булево;
		нач
			если ~error и (token.видЛексемы = Scanner.Идентификатор) и (this = token.строкаИдентификатора) то NextToken; возврат истина иначе возврат ложь всё;
		кон ThisIdentifier;

		проц ExpectAnyIdentifier(перем pos: Position; перем identifier: массив из симв8): булево;
		нач
			если ~GetIdentifier(pos,identifier)то Error(pos,"identifier expected"); возврат ложь
			иначе возврат истина
			всё;
		кон ExpectAnyIdentifier;

		проц ExpectIntegerWithSign(перем integer: цел32): булево;
		перем
			result, isNegated: булево;
		нач
			isNegated := ThisSymbol(Scanner.ЗнакМинус);
			если ExpectSymbol(Scanner.Число) и (token.numberType = Scanner.Integer) то
				если isNegated то
					integer := -token.integer(цел32)
				иначе
					integer := token.integer(цел32)
				всё;
				result := истина
			иначе
				result := ложь
			всё;
			возврат result
		кон ExpectIntegerWithSign;

		проц ExpectIntegerWithoutSign(перем integer: цел32): булево;
		перем
			result: булево;
		нач
			если ExpectSymbol(Scanner.Число) и (token.numberType = Scanner.Integer) то
				integer := token.integer(цел32);
				result := истина
			иначе
				result := ложь
			всё;
			возврат result
		кон ExpectIntegerWithoutSign;

		проц IgnoreNewLines;
		нач
			нцПока ThisSymbol(Scanner.Ln) делай кц;
		кон IgnoreNewLines;

		(* expect the newline or end-of-text token *)
		проц ExpectLineDelimiter(): булево;
		нач
			если ~error и ((token.видЛексемы = Scanner.Ln) или (token.видЛексемы = Scanner.КонецТекста)) то
				NextToken;
				возврат истина
			иначе
				Error(token.местоВТексте, "end of line/text expected");
				возврат ложь
			всё;
		кон ExpectLineDelimiter;

		(** parse an optional line number **)
		проц ParseLineNumber(expectedLineNumber: Потоки.ТипМестоВПотоке);
		перем
			positionOfLine: Position;
			specifiedLineNumber: Потоки.ТипМестоВПотоке;
			message, tempString: MessageString;
		нач
			если l0Trace то D.String(">>> ParseLineNumber"); D.Ln всё;

			positionOfLine := token.местоВТексте;
			если ThisSymbol(Scanner.Число) то (* note: line numbers are optional *)
				specifiedLineNumber := token.integer;
				если ExpectSymbol(Scanner.Двоеточие) то
					если Strict и (specifiedLineNumber # expectedLineNumber) то
						message := "invalid code line number (";
						Strings.IntToStr(specifiedLineNumber, tempString); Strings.Append(message, tempString);
						Strings.Append(message, " instead of ");
						Strings.IntToStr(expectedLineNumber, tempString); Strings.Append(message, tempString);
						Strings.Append(message, ")");
						Error(positionOfLine, message)
					всё
				всё
			всё
		кон ParseLineNumber;

		(** parse an intermediate code operand **)
		проц ParseOperand(перем operand: IntermediateCode.Operand; sectionList: Sections.SectionList);
		перем
			positionOfOperand, pos: Position; registerNumber, symbolOffset, integer: цел32;
			someHugeint: цел64;
			hasTypeDescriptor, isMemoryOperand, lastWasIdentifier, isNegated: булево;
			someLongreal: вещ64;
			identifier: SyntaxTree.СтрокаИдентификатора;
			type: IntermediateCode.Type;
			sectionOfSymbol: Sections.Section;
			name: Basic.SegmentedName;
			registerClass: IntermediateCode.RegisterClass;
		нач
			если l0Trace то D.String(">>> ParseOperand"); D.Ln всё;

			positionOfOperand := token.местоВТексте;

			(* defaults *)
			hasTypeDescriptor := ложь;
			isMemoryOperand := ложь;

			(* consume optional type description *)
			lastWasIdentifier := GetIdentifier(pos, identifier);
			если lastWasIdentifier и IntermediateCode.DenotesType(identifier, type) то
				hasTypeDescriptor := истина;
				lastWasIdentifier := GetIdentifier(pos, identifier)
			всё;

			(* consume optional memory operand bracket *)
			если ~lastWasIdentifier то
				isMemoryOperand := ThisSymbol(Scanner.ОткрывающаяКвадратнаяСкобка);
				lastWasIdentifier := GetIdentifier(pos, identifier)
			всё;

			если lastWasIdentifier то
				если IntermediateCode.DenotesRegister(identifier, registerClass, registerNumber) то
					(* register *)
					IntermediateCode.InitRegister(operand, type, registerClass, registerNumber);
				иначе
					(* TODO: handle assembly constants *)

					(* symbol name *)
					symbolOffset := 0;

					(* consume optional symbol offset *)
					если ThisSymbol(Scanner.Двоеточие) то
						если ExpectIntegerWithSign(integer) то
							symbolOffset := integer
						иначе
							Error(token.местоВТексте, "invalid symbol offset")
						всё
					всё;

					если l0Trace то D.String(">>> symbol detected"); D.Ln всё;

					Basic.ToSegmentedName(identifier, name);
					IntermediateCode.InitAddress(operand, IntermediateCode.UnsignedIntegerType(system.addressSize), name, 0, symbolOffset)
				всё

			аесли (token.видЛексемы = Scanner.СтрокиЛитералМульти) или (token.видЛексемы = Scanner.СтрокиЛитерал) то
				(* string constant *)
				IntermediateCode.InitString(operand, token.string);
				NextToken

			иначе
				(* immediate values/numbers *)
				isNegated := ThisSymbol(Scanner.ЗнакМинус);
				если ThisSymbol(Scanner.Число) то
					просей token.numberType из
					| Scanner.Integer, Scanner.Hugeint:
						если isNegated то someHugeint := -token.integer иначе someHugeint := token.integer всё;
						если ~hasTypeDescriptor то
							(* if no type description was included: use number type *)
							IntermediateCode.InitNumber(operand, someHugeint);
						аесли type.form = IntermediateCode.Float то
							утв(hasTypeDescriptor);
							IntermediateCode.InitFloatImmediate(operand, type, вещ32(someHugeint))
						иначе
							утв(hasTypeDescriptor и (type.form в IntermediateCode.Integer));
							IntermediateCode.InitImmediate(operand, type, someHugeint)
						всё
					| Scanner.Real, Scanner.Longreal:
						если isNegated то someLongreal := -token.real иначе someLongreal := token.real всё;
						(* if no type description was included: use float type with same amount of bits as address type *)
						если ~hasTypeDescriptor то
							IntermediateCode.InitType(type, IntermediateCode.Float, цел16(system.addressSize))
						всё;
						если type.form в IntermediateCode.Integer то
							Error(positionOfOperand, "floating point immediate value not applicable")
						иначе
							IntermediateCode.InitFloatImmediate(operand, type, someLongreal)
						всё
					иначе СТОП(100)
					всё
				иначе
					Error(positionOfOperand, "invalid operand")
				всё
			всё;

			(* consume optional offset given in system units *)
			если ThisSymbol(Scanner.ЗнакПлюс) то
				если ExpectIntegerWithoutSign(integer) то
					IntermediateCode.SetOffset(operand, integer)
				иначе
					Error(token.местоВТексте, "invalid offset")
				всё
			аесли ThisSymbol(Scanner.ЗнакМинус) то
				если ExpectIntegerWithoutSign(integer) то
					IntermediateCode.SetOffset(operand, -integer)
				иначе
					Error(token.местоВТексте, "invalid offset")
				всё
			всё;

			(* wrap memory operand around current operand if necessary *)
			если isMemoryOperand и ExpectSymbol(Scanner.ЗакрывающаяКвадратнаяСкобка) то
				IntermediateCode.SetType(operand, IntermediateCode.UnsignedIntegerType(system.addressSize)); (* set the type of the inner operand to the platform's address type *)
				если ~hasTypeDescriptor то
					IntermediateCode.InitType(type, IntermediateCode.SignedInteger, цел16(system.addressSize)) (* default: signed integer type of address size *)
				всё;
				IntermediateCode.InitMemory(operand, type, operand, 0) (* TODO: add offset? *)
			всё
		кон ParseOperand;

		(** parse an intermediate code instruction **)
		проц ParseInstruction(перем instruction: IntermediateCode.Instruction; sectionList: Sections.SectionList);
		перем
			opCode: цел8;
			positionOfInstruction, positionOfOperand: Position;
			operandNumber: цел32;
			operand: IntermediateCode.Operand;
			operands: массив 3 из IntermediateCode.Operand;
			operandType: IntermediateCode.Type;
			identifier, message, tempString: SyntaxTree.СтрокаИдентификатора;
		нач
			если l0Trace то D.String(">>> ParseInstruction"); D.Ln всё;

			positionOfInstruction := token.местоВТексте;
			если ExpectAnyIdentifier(positionOfInstruction, identifier) то
				(* TODO: detect labels of the form << labelName: >> *)
				opCode := IntermediateCode.FindMnemonic(identifier);

				если opCode = IntermediateCode.None то
					Error(positionOfInstruction, "unknown mnemonic")
				иначе
					(* consume all operands *)
					IntermediateCode.InitType(operandType, IntermediateCode.SignedInteger, 32); (* defaults *)
					IntermediateCode.InitOperand(operands[0]);
					IntermediateCode.InitOperand(operands[1]);
					IntermediateCode.InitOperand(operands[2]);

					operandNumber := 0;
					если ~ThisSymbol(Scanner.Ln) и ~ThisSymbol(Scanner.КонецТекста) то
						нцДо
							positionOfOperand := token.местоВТексте;
							если operandNumber > 2 то
								Error(positionOfInstruction, "instruction has too many operands")
							иначе
								ParseOperand(operand, sectionList);
								если ~error то
									если Strict и ~IntermediateCode.CheckOperand(operand, opCode, operandNumber, message) то
										Strings.Append(message, " @ operand ");
										Strings.IntToStr(operandNumber + 1, tempString); Strings.Append(message, tempString);
										Error(positionOfOperand, message)
									всё;
									operands[operandNumber] := operand;
									увел(operandNumber)
								всё
							всё
						кцПри error или ~ThisSymbol(Scanner.Запятая);
						если ~error и ExpectLineDelimiter() то всё
					всё;

					если ~error то
						IntermediateCode.InitInstruction(instruction, positionOfInstruction, opCode, operands[0], operands[1], operands[2]);
						если Strict и ~IntermediateCode.CheckInstruction(instruction, message) то
							Error(positionOfInstruction, message)
						всё
					всё
				всё;

			всё
		кон ParseInstruction;

		(** parse the content of an intermediate code section
		note: 'sectionList' is the list where referenced sections are found/to be created
		**)
		проц ParseSectionContent*(l2scanner: Scanner.AssemblerScanner; section: IntermediateCode.Section; sectionList: Sections.SectionList);
		перем
			instruction: IntermediateCode.Instruction;
			lineNumber: Потоки.ТипМестоВПотоке;
		нач
			если l0Trace то D.Ln; D.String(">>> ParseSectionContent"); D.Ln всё;
			сам.scanner := l2scanner;
			IgnoreNewLines;
			lineNumber := 0;
			нцПока ~error и (token.видЛексемы # Scanner.ЗнакПрепинанияТочка) и (token.видЛексемы # Scanner.КонецТекста) делай
				(* consume optional line number *)
				ParseLineNumber(lineNumber);
				если ~error то
					ParseInstruction(instruction, sectionList);
					если ~error то
						если l0Trace то IntermediateCode.DumpInstruction(D.Log, instruction); D.Ln; всё;
						section.Emit(instruction);
						увел(lineNumber)
					всё;
				всё;
				IgnoreNewLines
			кц
		кон ParseSectionContent;

		(** parse a list of section properties **)
		проц ParseSectionProperties(перем section: IntermediateCode.Section);
		перем
			positionOfProperty: Position;  integer: цел32;
		нач
			если l0Trace то D.Ln; D.String(">>> ParseSectionProperties"); D.Ln всё;

			нцПока ~error и (token.видЛексемы # Scanner.КонецТекста) и (token.видЛексемы # Scanner.Ln) делай
				positionOfProperty := token.местоВТексте;

				(* fingerprint *)
				если ThisIdentifier("fingerprint") и ExpectSymbol(Scanner.Equal) то
					если ExpectIntegerWithSign(integer) то
						если (section.fingerprint # 0) и (section.fingerprint # integer) то
							Error(positionOfProperty, "incompatible fingerprint");
						иначе
							section.SetFingerprint(integer);
						всё
					иначе
						Error(positionOfProperty, "invalid fingerprint")
					всё

				(* position *)
				аесли ThisIdentifier("aligned") и ExpectSymbol(Scanner.Equal) то
					если ExpectIntegerWithSign(integer) то
						section.SetPositionOrAlignment(ложь, integer)
					иначе
						Error(positionOfProperty, "invalid alignment")
					всё

				(* fixed position *)
				аесли ThisIdentifier("fixed") и ExpectSymbol(Scanner.Equal) то
					если ExpectIntegerWithSign(integer) то
						section.SetPositionOrAlignment(истина, integer)
					иначе
						Error(positionOfProperty, "invalid fixed postion")
					всё

				(* unit size of the section in bits *)
				аесли ThisIdentifier("unit") и ExpectSymbol(Scanner.Equal) то
					если ExpectIntegerWithSign(integer) то
						section.SetBitsPerUnit(integer) (* overwrite default unit size *)
					иначе
						Error(positionOfProperty, "invalid unit size")
					всё

				(* total size of the section in units *)
				аесли ThisIdentifier("size") и ExpectSymbol(Scanner.Equal) то
					если ExpectIntegerWithSign(integer) то
						(* nothing to do (this property is ignored, since the size is calculated from the actual content) *)
					иначе
						Error(positionOfProperty, "invalid size")
					всё

				иначе
					Error(positionOfProperty, "invalid property")
				всё
			кц
		кон ParseSectionProperties;

		(** parse the content of an intermediate code module **)
		проц ParseModuleContent*(l3scanner: Scanner.AssemblerScanner ; module: Sections.Module (* sectionList: Sections.SectionList; VAR moduleName: SyntaxTree.IdentifierString; VAR backend: Backend.Backend; loader: ModuleLoader*) ): булево;
		перем
			pos, positionOfDirective:Position;
			identifier: Scanner.СтрокаИдентификатора;
			afterModuleDirective, afterImportsDirective, afterFirstSection, isExternalSection: булево;
			sectionType: цел8;
			section: IntermediateCode.Section;
			name: Basic.SegmentedName;
			moduleName: SyntaxTree.СтрокаИдентификатора;
		нач
			если l0Trace то D.Ln; D.String(">>> ParseModuleContent"); D.Ln всё;

			moduleName := "";
			(*NEW(imports, 128);*)

			утв(l3scanner # НУЛЬ);
			сам.scanner := l3scanner;
			NextToken; (* read first token *)

			(* go through directives *)
			afterModuleDirective := ложь;
			afterImportsDirective := ложь;
			afterFirstSection := ложь;
			IgnoreNewLines;
			нцПока ~error и (token.видЛексемы # Scanner.КонецТекста) делай
				positionOfDirective := token.местоВТексте;
				если ExpectSymbol(Scanner.ЗнакПрепинанияТочка) и ExpectAnyIdentifier(pos, identifier) то
					(* 'module' directive *)
					если identifier = "module" то
						если afterModuleDirective то
							Error(positionOfDirective, "multiple module directives");
						аесли ExpectAnyIdentifier(pos, identifier) и ExpectLineDelimiter() то
							moduleName := identifier;
							module.SetModuleName(identifier);
							afterModuleDirective := истина;
						всё

					(* 'platform' directive *)
					аесли identifier = "platform" то
						если ~afterModuleDirective то
							Error(positionOfDirective, "platform directive must be preceeded by module directive")
						аесли ExpectAnyIdentifier(pos, identifier) и ExpectLineDelimiter() то
							module.SetPlatformName(identifier);
							(*! check against used backend *)
						аесли afterFirstSection то
							Error(positionOfDirective, "platform directive not before all sections")
						всё

					(* 'imports' directive *)
					аесли identifier = "imports" то
						если ~afterModuleDirective то
							Error(positionOfDirective, "import directive must be preceeded by module directive")
						аесли afterImportsDirective то
							Error(positionOfDirective, "multiple import directives")
						аесли afterFirstSection то
							Error(positionOfDirective, "import directive not before all sections")
						иначе
							нцДо
								если ExpectAnyIdentifier(positionOfDirective, identifier) то
									module.imports.AddName(identifier);
									(*
									IF ~loader(identifier) THEN Error(positionOfDirective, "could not import") END;
									*)
								всё
							кцПри error или ~ThisSymbol(Scanner.Запятая);
							если ExpectLineDelimiter() то
								afterImportsDirective := истина
							всё
						всё

					(* section *)
					иначе
						(* determine if section is external *)
						если identifier = "external" то
							positionOfDirective := token.местоВТексте;
							если ExpectSymbol(Scanner.ЗнакПрепинанияТочка) и ExpectAnyIdentifier(pos, identifier) то всё;
							isExternalSection := истина
						иначе
							isExternalSection := ложь
						всё;

						если  ~error то
							если identifier = "code" то sectionType := Sections.CodeSection
							аесли identifier = "const" то sectionType := Sections.ConstSection
							аесли identifier = "var" то sectionType := Sections.VarSection
							аесли identifier = "bodycode" то sectionType := Sections.BodyCodeSection
							аесли identifier = "inlinecode" то sectionType := Sections.InlineCodeSection
							аесли identifier = "initcode" то sectionType := Sections.InitCodeSection
							иначе Error(positionOfDirective, "invalid directive or section type")
							всё;

							если ~error и ~afterModuleDirective то
								Error(positionOfDirective, "module directive expected first")
							всё;

							если ~error то
								если ExpectAnyIdentifier(pos, identifier) то
									Basic.ToSegmentedName(identifier, name);
									section := IntermediateCode.NewSection(module.allSections, sectionType, name, НУЛЬ, истина); (* keeps section if already present *)

									(* set default unit size for the platform, which depends on the section type *)
									если (sectionType = Sections.VarSection) или (sectionType = Sections.ConstSection) то
										section.SetBitsPerUnit(system.dataUnit)
									иначе
										section.SetBitsPerUnit(system.codeUnit)
									всё;
									утв(section.bitsPerUnit # Sections.UnknownSize);

									(* consume optional section properties *)
									ParseSectionProperties(section);

									если ~error и ExpectLineDelimiter() то
										ParseSectionContent(l3scanner, section, module.allSections);
										afterFirstSection := истина
									всё
								всё
							всё

						всё
					всё
				всё;
				IgnoreNewLines;
			кц;
			возврат ~error
		кон ParseModuleContent;

		(** parse an entire intermediate code module **)
		проц ParseModule*(l4system: Global.System): Sections.Module;
		перем
			result: Sections.Module;
		нач
			нов(result, НУЛЬ, l4system); (* note: 1. there is no syntax tree module, 2. the system object to be used is not yet known *)
			если ParseModuleContent(scanner, result (* result.allSections, moduleName, backend, loader *)) то
				если l0Trace то
					D.String("++++++++++ PARSED MODULE '"); D.String(result.moduleName); D.String("' ++++++++++"); D.Ln;
					result.Dump(D.Log)
				всё
			иначе
				result := НУЛЬ
			всё
		кон ParseModule;
	кон IntermediateCodeParser;

	проц ParseReader*(reader: Потоки.Чтец; diagnostics: Diagnostics.Diagnostics; module: Sections.Module): булево;
	перем
		assemblerScanner: Scanner.AssemblerScanner;
		intermediateCodeParser: IntermediateCodeParser;
	нач
		assemblerScanner := Scanner.NewAssemblerScanner("",reader,0,diagnostics);
		нов(intermediateCodeParser, diagnostics, module.system);
		возврат intermediateCodeParser.ParseModuleContent(assemblerScanner, module)
	кон ParseReader;

	проц ParseFile*(конст pathName, moduleName: массив из симв8; system: Global.System; diagnostics: Diagnostics.Diagnostics): Sections.Module;
	перем
		filename: Files.FileName;
		assemblerScanner: Scanner.AssemblerScanner;
		intermediateCodeParser: IntermediateCodeParser;
		reader: Потоки.Чтец;
		msg: массив 128 из симв8;
		module: Sections.Module;
	нач
		(* open corresponding intermediate code file *)
		Files.JoinExtension(moduleName, IntermediateCodeExtension, filename);
		если pathName # "" то Files.JoinPath(pathName, filename, filename) всё;
		reader := Basic.GetFileReader(filename);
		если Trace то D.String("FoxIntermediateCodeParser.ParseFile "); D.String(filename); D.Ln всё;
		если reader = НУЛЬ то
			msg := "failed to open ";
			Strings.Append(msg, filename);
			Basic.Error(diagnostics, filename, Basic.invalidPosition, msg);
			возврат НУЛЬ
		иначе
			нов(module, НУЛЬ, system);
			если ParseReader(reader, diagnostics, module) то
				возврат module
			иначе
				возврат НУЛЬ
			всё;
		всё;
	кон ParseFile;

кон FoxIntermediateParser.

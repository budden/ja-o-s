модуль ARMDecoder;

использует НИЗКОУР, Decoder, Потоки, ЛогЯдра;

конст

	objFileSuffix = "Oba";
	none = -1;

	(* argument structure *)
	ArgNone = -2;
	ArgImm = 1;
	ArgReg = 2;
	ArgShift = 3;
	ArgRegImm = 11;
	ArgRegReg = 12;
	ArgRegShift = 13;
	ArgRegMem = 14;
	ArgRegRList = 15;
	ArgRegRegImm = 21;
	ArgRegRegReg = 22;
	ArgRegRegShift = 23;
	ArgRegRegMem = 24;
	ArgRegRegRegReg = 31;
	ArgRegRegRegImm = 32;
	ArgRegRegRegShift = 33;
	ArgCProcRegMem = 41;
	ArgCProcImmRegRegRegImm = 42;

	(* registers *)
	FP = 12; SP = 13; LR = 14; PC = 15; CPSR = 16; SPSR = 17;

	(* status register bits *)
	SRegC = 0; SRegX = 1; SRegS = 2; SRegF = 3;

	(* instructions *)
	opUNDEFINED = -2;
	opADC = 1;
	opADD = 2;
	opAND = 3;
	opASR = 4;
	opB = 5;
	opBIC = 6;
	opBKPT = 7;
	opBL = 8;
	opBLX = 9;
	opBX = 10;
	opCDP = 11;
	opCDP2 = 12;
	opCLZ = 13;
	opCMN = 14;
	opCMP = 15;
	opEOR = 16;
	opLDC = 17;
	opLDC2 = 18;
	opLDM = 19;
	opLDR = 20;
	opLDRB = 21;
	opLDRBT = 22;
	opLDRH = 23;
	opLDRSB = 24;
	opLDRSH = 25;
	opLDRT = 26;
	opLSL = 27;
	opLSR = 28;
	opMCR = 29;
	opMCR2 = 30;
	opMLA = 31;
	opMOV = 32;
	opMRC = 33;
	opMRC2 = 34;
	opMRS = 61;
	opMSR = 35;
	opMUL = 36;
	opMVN = 37;
	opORR = 38;
	opROR = 39;
	opRRX = 40;
	opRSB = 41;
	opRSC = 42;
	opSBC = 43;
	opSMLAL = 44;
	opSMULL = 45;
	opSTC = 46;
	opSTC2 = 47;
	opSTM = 48;
	opSTR = 49;
	opSTRB = 50;
	opSTRBT = 51;
	opSTRH = 52;
	opSTRT = 53;
	opSUB = 54;
	opSWI = 55;
	opSWP = 56;
	opSWPB = 62;
	opTEQ = 57;
	opTST = 58;
	opUMLAL = 59;
	opUMULL = 60;

	(* conditions *)
	EQ = 0;
	NE = 1;
	CSHS = 2;
	CCLO = 3;
	MI = 4;
	PL = 5;
	VS = 6;
	VC = 7;
	HI = 8;
	LS = 9;
	GE = 10;
	LT = 11;
	GT = 12;
	LE = 13;
	AL = 14;
	NV = 15;

	(* argument representations *)
	RepInt = 1;
	RepHex = 2;
	RepRelJmp = 10;

	(* immediate reg operations *)
	Lsl = 0;
	LSR = 1;
	Asr = 2;
	Ror = 3;
	RRX = 4;

	(* memory access addressing modes *)
	AddrModeReg = 1;
	AddrModeRegImm = 2;
	AddrModeRegReg = 3;
	AddrModeRegRegScale = 4;
	AddrModeDA = 5;
	AddrModeIA = 6;
	AddrModeDB = 7;
	AddrModeIB = 8;
	RegUpdateNone = 0;
	RegUpdatePre = 1;
	RegUpdatePost = 2;

тип
	ARMArg = окласс
	кон ARMArg;

	ARMArgImm = окласс (ARMArg)
		перем
			imm, rep : цел32;
		проц &New *(imm, rep : цел32);
		нач
			сам.imm := imm;
			сам.rep := rep
		кон New;
	кон ARMArgImm;

	ARMArgReg = окласс (ARMArg)
		перем
			reg : цел32;
			isCReg : булево; (* coprocessor register? *)
			sregMask : мнвоНаБитахМЗ; (* for CPSR, SPSR registers *)
		проц &New *(reg : цел32);
		нач
			сам.reg := reg;
			isCReg := ложь
		кон New;
	кон ARMArgReg;

	ARMArgRList = окласс (ARMArg)
		перем
			regs : мнвоНаБитахМЗ;
			addrMode : цел32;
		проц &New *(regs : мнвоНаБитахМЗ);
		нач
			сам.regs := regs;
		кон New;
	кон ARMArgRList;

	ARMArgMem = окласс (ARMArg)
		перем
			addrMode, reg, regOffs, regScale, shift, immOffs : цел32;
			width : цел32;
			signed, translation : булево;
			regUpdate : цел32;
		проц &New *(adrMode, reg : цел32);
		нач
			сам.addrMode := adrMode;
			сам.reg := reg;
			width := 4;
			regUpdate := RegUpdateNone;
			signed := ложь;
			translation := ложь
		кон New;
	кон ARMArgMem;

	ARMArgShift = окласс (ARMArg)
		перем
			shiftImmOrReg, operation : цел32;
			reg : булево;
		проц &New *(operation, shiftImmOrReg : цел32; reg : булево);
		нач
			сам.operation := operation;
			сам.shiftImmOrReg := shiftImmOrReg;
			сам.reg := reg
		кон New;
	кон ARMArgShift;

	ARMArgCProc = окласс (ARMArg)
		перем
			cproc : цел32;
		проц &New *(cproc : цел32);
		нач
			сам.cproc := cproc
		кон New;

	кон ARMArgCProc;

	ARMOpcode = окласс (Decoder.Opcode)
	перем
		argStructure : цел32;
		op, cond : цел32;
		ccUpdate : булево;
		arg1, arg2, arg3, arg4, arg5, arg6 : ARMArg;

		проц &{перекрыта}New*(proc : Decoder.ProcedureInfo; stream : Потоки.Писарь);
		нач
			New^(proc, stream);
			instr := -1;
			ccUpdate := ложь;
			argStructure := none
		кон New;

		проц {перекрыта}PrintOpcodeBytes*(w : Потоки.Писарь);
		нач
			w.п16ричное(op, 0)
		кон PrintOpcodeBytes;

		проц {перекрыта}PrintInstruction*(w : Потоки.Писарь);
		перем str : массив 20 из симв8; writeCondition, writeLSMAddrMode : булево;
		нач
			writeCondition := истина; writeLSMAddrMode := ложь;
			просей instr из
				opADC : str := "ADC"
				| opADD : str := "ADD"
				| opAND : str := "AND"
				| opASR : str := "ASR"
				| opB : str := "B"
				| opBIC : str := "BIC"
				| opBKPT : str := "BKPT"
				| opBL : str := "BL"
				| opBLX : str := "BLX"; writeCondition := ложь
				| opBX : str := "BX"
				| opCDP : str := "CDP"
				| opCDP2 : str := "CDP2"; writeCondition := ложь
				| opCLZ : str := "CLZ"
				| opCMN : str := "CMN"
				| opCMP : str := "CMP"
				| opEOR : str := "EOR"
				| opLDC : str := "LDC"
				| opLDC2 : str := "LDC2"; writeCondition := ложь
				| opLDM : str := "LDM"; writeLSMAddrMode := истина
				| opLDR : str := "LDR"
				| opLDRB : str := "LDRB"
				| opLDRBT : str := "LDRBT"
				| opLDRH : str := "LDRH"
				| opLDRSB : str := "LDRSB"
				| opLDRSH : str := "LDRSH"
				| opLDRT : str := "LDRT"
				| opLSL : str := "LSL"
				| opLSR : str := "LSR"
				| opMCR : str := "MCR"
				| opMCR2 : str := "MCR2"; writeCondition := ложь
				| opMLA : str := "MLA"
				| opMOV : str := "MOV"
				| opMRC : str := "MRC"
				| opMRC2 : str := "MRC2"; writeCondition := ложь
				| opMRS : str := "MRS"
				| opMSR : str := "MSR"
				| opMUL : str := "MUL"
				| opMVN : str := "MVN"
				| opORR : str := "ORR"
				| opROR : str := "ROR"
				| opRRX : str := "RRX"
				| opRSB : str := "RSB"
				| opRSC : str := "RSC"
				| opSBC : str := "SBC"
				| opSMLAL : str := "SMLAL"
				| opSMULL : str := "SMULL"
				| opSTC : str := "STC"
				| opSTC2 : str := "STC2"; writeCondition := ложь
				| opSTM : str := "STM"; writeLSMAddrMode := истина
				| opSTR : str := "STR"
				| opSTRB : str := "STRB"
				| opSTRBT : str := "STRBT"
				| opSTRH : str := "STRH"
				| opSTRT : str := "STRT"
				| opSUB : str := "SUB"
				| opSWI : str := "SWI"
				| opSWP : str := "SWP"
				| opSWPB : str := "SWPB"
				| opTEQ : str := "TEQ"
				| opTST : str := "TST"
				| opUMLAL : str := "UMLAL"
				| opUMULL : str := "UMULL"
				| opUNDEFINED : str := "{UNDEFINED}"
			иначе
				ЛогЯдра.пСтроку8("Unknown instr = "); ЛогЯдра.пЦел64(instr, 0); ЛогЯдра.пСтроку8(", op = "); ЛогЯдра.п16ричное(op, -1); ЛогЯдра.пВК_ПС;
				str := "[unknown]"
			всё;
			w.пСтроку8(str);
			если writeCondition то PrintCondition(w) всё;
			если writeLSMAddrMode то PrintLSMAddrMode(w) всё;
			если ccUpdate то w.пСтроку8("  S")
			аесли argStructure = ArgRegMem то
				w.пСтроку8("  ");
				если arg2(ARMArgMem).signed то w.пСимв8('S') всё;
				если arg2(ARMArgMem).width = 1 то w.пСимв8('B')
				аесли arg2(ARMArgMem).width = 2 то w.пСимв8('H')
				всё;
				если arg2(ARMArgMem).translation то w.пСимв8('T') всё;
			всё
		кон PrintInstruction;

		проц PrintCondition (w : Потоки.Писарь);
		перем str : массив 6 из симв8;
		нач
			w.пСимв8(' ');
			просей cond из
				EQ : str := "EQ"
				| NE : str := "NE"
				| CSHS : str := "CS/HS"
				| CCLO : str := "CC/LO"
				| MI : str := "MI"
				| PL : str := "PL"
				| VS : str := "VS"
				| VC : str := "VC"
				| HI : str := "HI"
				| LS : str := "LS"
				| GE : str := "GE"
				| LT : str := "LT"
				| GT : str := "GT"
				| LE : str := "LE"
				| AL : str := ""
				| NV : str := "NV"
			всё;
			w.пСтроку8(str)
		кон PrintCondition;

		проц PrintLSMAddrMode (w : Потоки.Писарь);
		перем str : массив 6 из симв8;
		нач
			утв(argStructure = ArgRegRList);
			w.пСимв8(' ');
			просей arg2(ARMArgRList).addrMode из
				AddrModeIA : str := "IA"
				| AddrModeIB : str := "IB"
				| AddrModeDA : str := "DA"
				| AddrModeDB : str := "DB"
			иначе str := ""
			всё;
			w.пСтроку8(str)
		кон PrintLSMAddrMode;

		проц {перекрыта}PrintArguments*(w : Потоки.Писарь);
		нач
			(* KernelLog.String("*argStructure = "); KernelLog.Int(argStructure, 0); KernelLog.String(", instr = "); KernelLog.Int(instr, 0); KernelLog.Ln; *)
			просей argStructure из
				ArgNone :
				| ArgImm : WriteImm(arg1(ARMArgImm), w)
				| ArgReg : WriteReg(arg1(ARMArgReg), w)
				| ArgRegReg : WriteReg(arg1(ARMArgReg), w); w.пСтроку8(", "); WriteReg(arg2(ARMArgReg), w)
				| ArgRegRList : WriteRegRList(arg1(ARMArgReg), arg2(ARMArgRList), w)
				| ArgRegImm : WriteReg(arg1(ARMArgReg), w); w.пСтроку8(", "); WriteImm(arg2(ARMArgImm), w)
				| ArgRegShift : WriteReg(arg1(ARMArgReg), w); w.пСтроку8(", "); WriteShift(arg2(ARMArgShift), w)
				| ArgRegMem : WriteReg(arg1(ARMArgReg), w); w.пСтроку8(", "); WriteMem(arg2(ARMArgMem), w)
				| ArgRegRegReg : WriteReg(arg1(ARMArgReg), w); w.пСтроку8(", "); WriteReg(arg2(ARMArgReg), w); w.пСтроку8(", "); WriteReg(arg3(ARMArgReg), w)
				| ArgRegRegImm : WriteReg(arg1(ARMArgReg), w); w.пСтроку8(", "); WriteReg(arg2(ARMArgReg), w); w.пСтроку8(", "); WriteImm(arg3(ARMArgImm), w)
				| ArgRegRegShift : WriteReg(arg1(ARMArgReg), w); w.пСтроку8(", "); WriteReg(arg2(ARMArgReg), w); w.пСтроку8(", "); WriteShift(arg3(ARMArgShift), w)
				| ArgRegRegMem : WriteReg(arg1(ARMArgReg), w); w.пСтроку8(", "); WriteReg(arg2(ARMArgReg), w); w.пСтроку8(", "); WriteMem(arg3(ARMArgMem), w)
				| ArgRegRegRegReg : WriteReg(arg1(ARMArgReg), w); w.пСтроку8(", "); WriteReg(arg2(ARMArgReg), w); w.пСтроку8(", "); WriteReg(arg3(ARMArgReg), w); w.пСтроку8(", "); WriteReg(arg4(ARMArgReg), w)
				| ArgRegRegRegImm : WriteReg(arg1(ARMArgReg), w); w.пСтроку8(", "); WriteReg(arg2(ARMArgReg), w); w.пСтроку8(", "); WriteReg(arg3(ARMArgReg), w); w.пСтроку8(", "); WriteImm(arg4(ARMArgImm), w)
				| ArgRegRegRegShift : WriteReg(arg1(ARMArgReg), w); w.пСтроку8(", "); WriteReg(arg2(ARMArgReg), w); w.пСтроку8(", "); WriteReg(arg3(ARMArgReg), w); w.пСтроку8(", "); WriteShift(arg4(ARMArgShift), w)
				| ArgCProcRegMem : WriteCProc(arg1(ARMArgCProc), w); w.пСтроку8(", "); WriteReg(arg2(ARMArgReg), w); w.пСтроку8(", "); WriteMem(arg3(ARMArgMem), w)
				| ArgCProcImmRegRegRegImm : WriteCProc(arg1(ARMArgCProc), w); w.пСтроку8(", "); WriteImm(arg2(ARMArgImm), w); w.пСтроку8(", "); WriteReg(arg3(ARMArgReg), w);  w.пСтроку8(", "); WriteReg(arg4(ARMArgReg), w); w.пСтроку8(", "); WriteReg(arg5(ARMArgReg), w);  w.пСтроку8(", "); WriteImm(arg2(ARMArgImm), w)
			иначе
				ЛогЯдра.пСтроку8("argStructure = "); ЛогЯдра.пЦел64(argStructure, 0); ЛогЯдра.пВК_ПС;
				w.пСтроку8("{argStructure not specified!}")
			всё
		кон PrintArguments;

		проц WriteImm (immArg : ARMArgImm; w :  Потоки.Писарь);
			проц WriteHex;
			перем absImm : цел32;
			нач
				absImm := immArg.imm;
				если immArg.rep = RepRelJmp то
					(* add opcode position and length of full opcode to immediate argument value *)
					увел(absImm, цел32(offset + 4))
				всё;
				Hex(absImm, w)
			кон WriteHex;

		нач
			если immArg.rep = RepInt то
				w.пЦел64(immArg.imm, 0)
			аесли immArg.rep = RepHex то
				WriteHex
			иначе
				w.пЦел64(immArg.imm, 0);
				w.пСтроку8(" (");
				WriteHex;
				w.пСимв8(')')
			всё
		кон WriteImm;

		проц WriteReg (regArg : ARMArgReg; w : Потоки.Писарь);
		нач
			если regArg.isCReg то
				w.пСтроку8("CR"); w.пЦел64(regArg.reg, 0)
			иначе
				WriteRegSymbol(regArg.reg, w);
				если regArg.reg >= CPSR то
					если regArg.sregMask * {SRegC} # {} то w.пСтроку8("c") всё;
					если regArg.sregMask * {SRegX} # {} то w.пСтроку8("x") всё;
					если regArg.sregMask * {SRegS} # {} то w.пСтроку8("s") всё;
					если regArg.sregMask * {SRegF} # {} то w.пСтроку8("f") всё
				всё
			всё
		кон WriteReg;

		проц WriteRegSymbol (reg : цел32; w : Потоки.Писарь);
		нач
			просей reg из
				FP : w.пСтроку8("FP")
				| SP : w.пСтроку8("SP")
				| LR : w.пСтроку8("LR")
				| PC : w.пСтроку8("PC")
				| CPSR : w.пСтроку8("CPSR")
				| SPSR : w.пСтроку8("SPSR")
			иначе
				w.пСимв8('R'); w.пЦел64(reg, 0)
			всё
		кон WriteRegSymbol;

		проц WriteShiftSymbol (op : цел32; w : Потоки.Писарь);
		нач
			просей op из
				Lsl : w.пСтроку8("LSL ")
				| LSR : w.пСтроку8("LSR ")
				| Asr : w.пСтроку8("ASR ")
				| Ror : w.пСтроку8("ROR ")
				| RRX : w.пСтроку8("RRX ")
			всё;
		кон WriteShiftSymbol;


		проц WriteShift (shiftArg : ARMArgShift; w :  Потоки.Писарь);
		нач
			WriteShiftSymbol(shiftArg.operation, w);
			если shiftArg.operation # RRX то
				если shiftArg.reg то
					утв(shiftArg.shiftImmOrReg < CPSR);
					WriteRegSymbol(shiftArg.shiftImmOrReg, w)
				иначе
					w.пСимв8('#');
					Hex(shiftArg.shiftImmOrReg, w)
				всё
			всё
		кон WriteShift;

		проц WriteMem (memArg : ARMArgMem; w : Потоки.Писарь);

			проц WriteEnd;
			нач
				если memArg.regUpdate # RegUpdatePost то
					w.пСимв8("]");
					если memArg.regUpdate = RegUpdatePre то w.пСимв8('!') всё
				всё
			кон WriteEnd;
		нач
			w.пСимв8('[');
			WriteRegSymbol(memArg.reg, w);
			если memArg.regUpdate = RegUpdatePost то w.пСимв8(']') всё;
			просей memArg.addrMode из
				AddrModeReg : w.пСимв8(']')
				| AddrModeRegImm :
					w.пСтроку8(", #");
					если memArg.immOffs < 0 то w.пСимв8('-'); Hex(-memArg.immOffs, w) иначе Hex(memArg.immOffs, w) всё;
					WriteEnd
				| AddrModeRegReg :
					w.пСтроку8(", "); WriteRegSymbol(memArg.regOffs, w); WriteEnd
				| AddrModeRegRegScale : w.пСтроку8(", "); WriteRegSymbol(memArg.regOffs, w); w.пСтроку8(", "); WriteShiftSymbol(memArg.shift, w); w.пСтроку8(" #"); w.пЦел64(memArg.regScale, 0); WriteEnd
			всё
		кон WriteMem;

		проц WriteRegRList (regArg : ARMArgReg; rListArg : ARMArgRList; w : Потоки.Писарь);
		перем i, lastStart : цел32; notFirst : булево;

			проц PrintRegs (start, end : цел32);
			нач
				если start >= 0 то
					если notFirst то w.пСтроку8(", ") всё;
					если start < end то (* print range *)
						WriteRegSymbol(start, w); w.пСтроку8(" - "); WriteRegSymbol(end, w)
					иначе (* print single reg *)
						WriteRegSymbol(start, w)
					всё;
					notFirst := истина
				всё
			кон PrintRegs;

		нач
			WriteRegSymbol(regArg.reg, w);
			если rListArg.addrMode # none то w.пСимв8('!') всё;
			w.пСтроку8(", {");
			lastStart := -1;
			notFirst := ложь;
			нцДля i := 0 до PC делай
				если rListArg.regs * {i} # {} то если lastStart < 0 то lastStart := i всё
				иначе
					PrintRegs(lastStart, i-1); lastStart := -1
				всё;
			кц;
			PrintRegs(lastStart, PC);
			w.пСимв8('}')
		кон WriteRegRList;

		проц WriteCProc (cProcArg : ARMArgCProc; w : Потоки.Писарь);
		нач
			w.пСимв8('p'); w.пЦел64(cProcArg.cproc, 0)
		кон WriteCProc;
	кон ARMOpcode;

	ARMDecoder = окласс (Decoder.Decoder)
	перем
		bit24To27, bit20To23, bit16To19, bit12To15, bit8To11, bit4To7, bit0To3 : цел32;

		проц {перекрыта}NewOpcode*() : Decoder.Opcode;
		перем
			opcode : ARMOpcode;
		нач
			нов(opcode, currentProc, outputStreamWriter);
			возврат opcode
		кон NewOpcode;

		проц {перекрыта}DecodeThis*(opcode : Decoder.Opcode);
		перем
			armOpcode : ARMOpcode;
			opSet : мнвоНаБитахМЗ;
			code, category : цел32;
		нач
			armOpcode := opcode(ARMOpcode);
			armOpcode.op := ReadLInt();
			если bigEndian то SwapBytes(armOpcode.op) всё;
			opSet := мнвоНаБитахМЗ(armOpcode.op);
			(* KernelLog.String("op = "); KernelLog.Hex(armOpcode.op, 0); KernelLog.String(" -> "); *)

			code := armOpcode.op;
			bit0To3 := code остОтДеленияНа 10H; code := code DIV 10H;	(* split instruction *)
			bit4To7 := code остОтДеленияНа 10H; code := code DIV 10H;
			bit8To11 := code остОтДеленияНа 10H; code := code DIV 10H;
			bit12To15 := code остОтДеленияНа 10H; code := code DIV 10H;
			bit16To19 := code остОтДеленияНа 10H; code := code DIV 10H;
			bit20To23 := code остОтДеленияНа 10H; code := code DIV 10H;
			bit24To27 := code остОтДеленияНа 10H; code := code DIV 10H;
			armOpcode.cond := code остОтДеленияНа 10H;

			category := bit24To27 DIV 2;
			если armOpcode.cond = 15 то
				если category = 5 то
					BranchToThumb(armOpcode, armOpcode.op);
					возврат
				аесли ((category = 7) и (bit24To27 = 0EH)) или (category = 6) то (* not undefined *)
				иначе
					armOpcode.instr := opUNDEFINED;
					armOpcode.argStructure := ArgNone;
					возврат
				всё
			всё;
			просей category из
				0 :
					если (opSet * {4, 7}) = {4, 7} то
						MultipliesExtraLS(armOpcode)
					аесли (opSet * {20, 23, 24}) = {24} то Miscellaneous(armOpcode)
					иначе DataProcessing(armOpcode)
					всё
				| 1 :
					если (opSet * {20, 21, 23, 24}) = {21, 24} то
						SRegTransfer(armOpcode)
					аесли (opSet * {20, 21, 23, 24}) = {24} то
						armOpcode.instr := opUNDEFINED;
						armOpcode.argStructure := ArgNone
					иначе DataProcessing(armOpcode)
					всё
				| 2 : LoadStore(armOpcode)
				| 3 :
					если (opSet * {4}) = {4} то
						armOpcode.instr := opUNDEFINED;
						armOpcode.argStructure := ArgNone
					иначе LoadStore(armOpcode)
					всё
				| 4 : LoadStoreMultiple(armOpcode)
				| 5 : Branch(armOpcode)
				| 6 : CoprocLoadStoreDRegTransfer(armOpcode)
				| 7 : ЛогЯдра.пСтроку8("check2"); ЛогЯдра.пВК_ПС;
					если (opSet * {24}) = {24} то SoftwareInterrupt(armOpcode)
					аесли (opSet * {4}) = {4} то CoprocRegTransfer(armOpcode)
					иначе CoprocDataProcessing(armOpcode)
					всё
			всё
		кон DecodeThis;

		проц DecodeShifterOperand(op : цел32; перем argStructure : цел32; перем arg1, arg2 : ARMArg);
		перем
			Rm, shiftImmOrReg, operation, mode : цел32;
			regOp : булево;
			regArg : ARMArgReg;
			immArg : ARMArgImm;
			shiftArg : ARMArgShift;
		нач
			если (мнвоНаБитахМЗ(op) * {25}) = {25} то (* I Bit *)
				argStructure := ArgImm;
				нов(immArg, вращБит(op остОтДеленияНа 100H, -2*bit8To11), RepHex); arg1 := immArg;
			иначе
				Rm := op остОтДеленияНа 10H;
				нов(regArg, Rm); arg1 := regArg;
				argStructure := ArgRegShift;
				mode := bit4To7 остОтДеленияНа 8; (* type of register addressing mode *)
				если mode = 0 то (* Register / Logical shift left by immediate *)
					shiftImmOrReg := (op остОтДеленияНа 1000H) DIV 80H;
					если shiftImmOrReg = 0 то argStructure := ArgReg
					иначе
						нов(shiftArg, Lsl, shiftImmOrReg, ложь); arg2 := shiftArg
					всё
				аесли ((op остОтДеленияНа 1000H) DIV 10H) = 6 то (* rotate right with extend *)
					нов(shiftArg, RRX, 0, ложь); arg2 := shiftArg
				иначе
					просей mode из
						1 : operation := Lsl
						| 2,3 : operation := LSR
						| 4,5 : operation := Asr
						| 6,7 : operation := Ror
					всё;
					если нечётноеЛи¿(mode) то
						regOp := истина;
						shiftImmOrReg := bit8To11
					иначе
						regOp := ложь;
						shiftImmOrReg := (op остОтДеленияНа 1000H) DIV 80H
					всё;
					нов(shiftArg, operation, shiftImmOrReg, regOp); arg2 := shiftArg
				всё
			всё
		кон DecodeShifterOperand;

		проц DataProcessing (opcode : ARMOpcode);
		перем SBit : булево; Rn, Rd, type : цел32;
			regArg : ARMArgReg; argSh1, argSh2 : ARMArg;
			shStruct : цел32;
		нач
			просей (opcode.op остОтДеленияНа 2000000H) DIV 200000H из (* opcode *)
				0 : opcode.instr := opAND; type := 3
				| 1 : opcode.instr := opEOR; type := 3
				| 2 : opcode.instr := opSUB; type := 3
				| 3 : opcode.instr := opRSB; type := 3
				| 4 : opcode.instr := opADD; type := 3
				| 5 : opcode.instr := opADC; type := 3
				| 6 : opcode.instr := opSBC; type := 3
				| 7 : opcode.instr := opRSC; type := 3
				| 8 : opcode.instr := opTST; type := 2
				| 9 : opcode.instr := opTEQ; type := 2
				| 10 : opcode.instr := opCMP; type := 2
				| 11 : opcode.instr := opCMN; type := 2
				| 12 : opcode.instr := opORR; type := 3
				| 13 : opcode.instr := opMOV; type := 1
				| 14 : opcode.instr := opBIC; type := 3
				| 15 : opcode.instr := opMVN; type := 1
			всё;
			DecodeShifterOperand(opcode.op, shStruct, argSh1, argSh2);
			если мнвоНаБитахМЗ(opcode.op) * {20} = {20} то SBit := истина иначе SBit := ложь всё;
			opcode.ccUpdate := SBit и (type # 2);
			Rn := bit16To19;
			Rd := bit12To15;
			если (type = 1) или (type = 2) то
				opcode.argStructure := shStruct + 10;
				если type = 1 то нов(regArg, Rd) иначе нов(regArg, Rn) всё;
				opcode.arg1 := regArg; opcode.arg2 := argSh1; opcode.arg3 := argSh2
			аесли type = 3 то
				opcode.argStructure := shStruct + 20;
				нов(regArg, Rd); opcode.arg1 := regArg;
				нов(regArg, Rn); opcode.arg2 := regArg;
				opcode.arg3 := argSh1; opcode.arg4 := argSh2
			всё
		кон DataProcessing;

		проц Miscellaneous (opcode : ARMOpcode);
		перем regArg : ARMArgReg; immArg : ARMArgImm;
		нач
			просей bit4To7 остОтДеленияНа 2 из
				0 : SRegTransfer(opcode)
				| 1 :
					если bit20To23 = 6 то
						opcode.instr := opCLZ;
						opcode.argStructure := ArgRegReg;
						нов(regArg, bit12To15); opcode.arg1 := regArg;
						нов(regArg, bit0To3); opcode.arg2 := regArg
					аесли логСдвиг(opcode.op, -20) = 0E12H то
						если bit4To7 = 7 то
							opcode.instr := opBKPT;
							нов(immArg, (((opcode.op остОтДеленияНа 100000) DIV 100H) * 10H) + (opcode.op остОтДеленияНа 10H), RepHex);
							opcode.argStructure := ArgImm;
							opcode.arg1 := immArg
						аесли bit4To7 = 1 то
							opcode.instr := opBX;
							нов(regArg, bit0To3); opcode.arg1 := regArg;
							opcode.argStructure := ArgReg
						всё
					всё
			всё
		кон Miscellaneous;

		проц MultipliesExtraLS (opcode : ARMOpcode);
		перем op1: цел32;
			regA, regB, regC, regD : ARMArgReg;  memArg : ARMArgMem;
			argSh1, argSh2 : ARMArg;
			shStruct : цел32;
		нач
			op1 := bit20To23 DIV 2;
			opcode.ccUpdate := (bit20To23 остОтДеленияНа 2) = 1; (* S bit set? *)
			нов(regA, bit16To19);
			нов(regB, bit12To15);
			нов(regC, bit8To11);
			нов(regD, bit0To3);
			если bit24To27 = 1 то (* SWP/SWPB *)
				нов(memArg, AddrModeReg, regA.reg);
				opcode.argStructure := ArgRegRegMem;
				opcode.arg1 := regB; opcode.arg2 := regD; opcode.arg3 := memArg;
				если bit20To23 = 0 то opcode.instr := opSWP иначе opcode.instr := opSWPB всё
			иначе
				opcode.argStructure := ArgRegRegRegReg;
				opcode.arg1 := regB; opcode.arg2 := regA; opcode.arg3 := regD; opcode.arg4 := regC;
				просей op1 из
					0 : opcode.instr := opMUL; opcode.argStructure := ArgRegRegReg; opcode.arg1 := regA; opcode.arg2 := regD; opcode.arg3 := regC
					| 1 : opcode.instr := opMLA; opcode.arg1 := regA; opcode.arg2 := regD; opcode.arg3 := regC; opcode.arg4 := regB
					| 3 : opcode.instr := opRSB; opcode.arg1 := regB; opcode.arg2 := regA; DecodeShifterOperand(opcode.op, shStruct, argSh1, argSh2); opcode.argStructure := shStruct + 20; opcode.arg3 := argSh1; opcode.arg4 := argSh2
					| 4 : opcode.instr := opUMULL
					| 5 : opcode.instr := opUMLAL
					| 6 : opcode.instr := opSMULL
					| 7 : opcode.instr := opSMLAL
				всё
			всё
		кон MultipliesExtraLS;

		проц SRegTransfer (opcode : ARMOpcode);
		перем regArg : ARMArgReg; immArg : ARMArgImm;
		нач
			opcode.argStructure := ArgRegReg;
			если (bit20To23 DIV 4) = 1 то нов(regArg, SPSR) иначе нов(regArg, CPSR) всё;
			если (bit20To23 остОтДеленияНа 4) # 2 то
				opcode.instr := opMRS;
				opcode.arg2 := regArg;
				нов(regArg, bit12To15); opcode.arg1 := regArg
			иначе
				opcode.instr := opMSR;
				regArg.sregMask := мнвоНаБитахМЗ(bit16To19);
				opcode.arg1 := regArg;
				если (bit24To27 DIV 2) = 1 то (* immediate operand *)
					нов(immArg, вращБит(opcode.op остОтДеленияНа 100H, -2*bit8To11), RepHex); (* 8 bit imm rotated by rotate imm *)
					opcode.arg2 := immArg
				иначе нов(regArg, bit0To3); opcode.arg2 := regArg
				всё
			всё
		кон SRegTransfer;

		проц LoadStore (opcode : ARMOpcode);
		перем regArg : ARMArgReg; memArg : ARMArgMem;
			P, L, W, B, U : булево;
			offset12 : цел32;
		нач
			opcode.argStructure := ArgRegMem;
			нов(regArg, bit12To15); opcode.arg1 := regArg;
			P := bit24To27 остОтДеленияНа 2 = 1;
			L := bit20To23 остОтДеленияНа 2 = 1;
			W := (bit20To23 остОтДеленияНа 4) DIV 2 = 1;
			B := (bit20To23 остОтДеленияНа 8) DIV 4 = 1;
			U := bit20To23 DIV 8 = 1;
			offset12 := opcode.op остОтДеленияНа 1000H;
			(* determine memory location and addressing mode *)
			если (bit24To27 DIV 2) = 2 то (* immediate offset/index *)
				если offset12 = 0 то 	нов(memArg, AddrModeReg, bit16To19)
				иначе
					нов(memArg, AddrModeRegImm, bit16To19);
					если U то memArg.immOffs := offset12 иначе memArg.immOffs := -offset12 всё
				всё
			иначе (* register offset/index *)
				если (bit4To7 = 0) и (bit8To11 = 0) то
					нов(memArg, AddrModeRegReg, bit16To19); memArg.regOffs := bit0To3
				иначе
					нов(memArg, AddrModeRegRegScale, bit16To19); memArg.regOffs := bit0To3;
					memArg.shift :=  (bit4To7 остОтДеленияНа 8) DIV 2;
					memArg.regScale := (opcode.op остОтДеленияНа 1000H) DIV 80H
				всё
			всё;
			если L то opcode.instr := opLDR иначе opcode.instr := opSTR всё;
			если  ~P то (* P = 0, post index addressing *)
				если memArg.addrMode # AddrModeReg то memArg.regUpdate := RegUpdatePost всё;
				memArg.immOffs := opcode.op остОтДеленияНа 1000H;
			иначе (* P = 1, offset or pre-indexed addressing *)
				если W то (* base register update *)
					memArg.regUpdate := RegUpdatePre;
				всё
			всё;
			если W то memArg.translation := истина всё;
			если B то memArg.width := 1 всё;
			opcode.arg2 := memArg
		кон LoadStore;

		проц LoadStoreMultiple (opcode : ARMOpcode);
		перем regArg : ARMArgReg; regListArg : ARMArgRList;
			P, U, S, W, L : булево;
		нач
			P := bit24To27 остОтДеленияНа 2 = 1;
			U := bit20To23 DIV 8 = 1;
			S := (bit20To23 остОтДеленияНа 8) DIV 4 = 1;
			W := (bit20To23 остОтДеленияНа 4) DIV 2 = 1;
			L := bit20To23 остОтДеленияНа 2 = 1;
			если L то opcode.instr := opLDM иначе opcode.instr := opSTM всё;
			нов(regArg, bit16To19);
			нов(regListArg, мнвоНаБитахМЗ(opcode.op остОтДеленияНа 10000H));
			если W то
				если P то если U то regListArg.addrMode := AddrModeIB иначе regListArg.addrMode := AddrModeDB всё
				иначе если U то regListArg.addrMode := AddrModeIA иначе regListArg.addrMode := AddrModeDA всё
				всё
			иначе regListArg.addrMode := none всё;
			opcode.argStructure := ArgRegRList;
			opcode.arg1 := regArg;
			opcode.arg2 := regListArg
		кон LoadStoreMultiple;

		проц Branch (opcode : ARMOpcode);
		перем immArg : ARMArgImm;
		нач
			если (мнвоНаБитахМЗ(opcode.op) * {24}) = {24} то opcode.instr := opBL
			иначе opcode.instr := opB
			всё;
			opcode.argStructure := ArgImm;
			нов(immArg, (opcode.op остОтДеленияНа 1000000H), RepRelJmp);
			SignExtension(immArg.imm, 24);
			immArg.imm := (immArg.imm + 1) *4;
			opcode.arg1 := immArg
		кон Branch;

		проц BranchToThumb (opcode : ARMOpcode; op : цел32);
		перем sImmed24, targetAddr : цел32; immArg : ARMArgImm; regArg : ARMArgReg;
		нач
			opcode.instr := opBLX;
			если opcode.cond = 15 то
				sImmed24 := opcode.op остОтДеленияНа 1000000H;
				targetAddr := (bit24To27 остОтДеленияНа 2)*2 + арифмСдвиг(логСдвиг(sImmed24, 8), -6); (* sign extend *)
				нов(immArg, targetAddr, RepRelJmp); opcode.arg1 := immArg;
				opcode.argStructure := ArgImm
			иначе
				нов(regArg, bit0To3); opcode.arg1 := regArg;
				opcode.argStructure := ArgReg
			всё
		кон BranchToThumb;

		проц CoprocLoadStoreDRegTransfer (opcode : ARMOpcode);
		перем cProcArg : ARMArgCProc; memArg : ARMArgMem; regArg : ARMArgReg;
			P, U, W, mul4 : булево;
		нач
			если (bit20To23 остОтДеленияНа 2) = 1 то opcode.instr := opLDC иначе opcode.instr := opSTC всё;
			P := (bit24To27 остОтДеленияНа 2) = 1; (* P bit *)
			U := (bit20To23 DIV 8) = 1; (* U bit *)
			W := ((bit20To23 остОтДеленияНа 4) DIV 2) = 1; (* W bit *)
			mul4 := истина;
			opcode.argStructure := ArgCProcRegMem;
			нов(memArg, AddrModeRegImm, bit16To19);
			если P то
				memArg.regUpdate := RegUpdatePost;
				если ~W то mul4 := ложь всё (* this is the option argument, therefore it is not multiplied by 4, but represented the same way like a post update offset *)
			иначе
				если W то memArg.regUpdate := RegUpdatePre
				иначе memArg.regUpdate := RegUpdateNone
				всё
			всё;
			если U то memArg.immOffs := (opcode.op остОтДеленияНа 100H) иначе memArg.immOffs := - (opcode.op остОтДеленияНа 100H) всё;
			если mul4 то memArg.immOffs := memArg.immOffs * 4 всё;
			нов(cProcArg, bit8To11); opcode.arg1 := cProcArg;
			нов(regArg, bit12To15); regArg.isCReg := истина;
			opcode.arg1 := cProcArg; opcode.arg2 := regArg; opcode.arg3 := memArg
		кон CoprocLoadStoreDRegTransfer;

		проц CoprocDataProcessing (opcode : ARMOpcode);
		нач
			CoprocRegTransfer(opcode);
			если opcode.cond = 15 то opcode.instr := opCDP2 иначе opcode.instr := opCDP всё;
			opcode.arg2(ARMArgImm).imm := bit20To23
		кон CoprocDataProcessing;

		проц CoprocRegTransfer (opcode : ARMOpcode);
		перем immArg : ARMArgImm; regArg : ARMArgReg; cProcArg : ARMArgCProc;
		нач
			если (bit20To23 остОтДеленияНа 2) = 1 то
				если opcode.cond = 15 то opcode.instr := opMRC2 иначе opcode.instr := opMRC всё
			иначе
				если opcode.cond = 15 то opcode.instr := opMCR2 иначе opcode.instr := opMCR всё
			всё;
			opcode.argStructure := ArgCProcImmRegRegRegImm;
			нов(cProcArg, bit8To11); opcode.arg1 := cProcArg;
			нов(immArg, bit20To23 DIV 2, RepInt); opcode.arg2 := immArg; (* opcode-1 *)
			нов(regArg, bit12To15); opcode.arg3 := regArg;
			нов(regArg, bit16To19); regArg.isCReg := истина; opcode.arg4 := regArg;
			нов(regArg, bit0To3); regArg.isCReg := истина; opcode.arg5 := regArg;
			нов(immArg, bit4To7 DIV 2, RepInt); opcode.arg6 := immArg
		кон CoprocRegTransfer;

		проц SoftwareInterrupt (opcode : ARMOpcode);
		перем immArg : ARMArgImm;
		нач
			нов(immArg, opcode.op остОтДеленияНа 1000000H, RepHex);
			opcode.instr := opSWI;
			opcode.argStructure := ArgImm;
			opcode.arg1 := immArg
		кон SoftwareInterrupt;

		проц SignExtension (перем x : цел32; length : цел32);
		нач
			утв((length > 0) и (length <= 32));
			x := логСдвиг(x, 32-length);
			x := арифмСдвиг(x, length-32)
		кон SignExtension;
	кон ARMDecoder;

перем
	bigEndian : булево;

проц SetBigEndian*;
нач
	bigEndian := истина;
кон SetBigEndian;

проц SetLittleEndian*;
нач
	bigEndian := ложь;
кон SetLittleEndian;

проц Hex(n: цел32; w : Потоки.Писарь); (* procedure from PCARMDecoder.Mod *)
	перем i, j: цел16; s, t : массив 10 из симв8;
нач
	i := 0;
	нцДо
		если n остОтДеленияНа 10H < 10 то s[i] := симв8ИзКода(n остОтДеленияНа 10H +кодСимв8("0")) иначе s[i] := симв8ИзКода(n остОтДеленияНа 10H - 10 + кодСимв8("A")) всё;
		n := n DIV 10H остОтДеленияНа 10000000H; увел(i);
	кцПри n = 0;
	j := 0;
	нцПока i>0 делай умень(i); t[j] := s[i]; увел(j) кц; t[j]:="H"; t[j+1] := 0X;
	w.пСтроку8(t)
кон Hex;

проц SwapBytes(перем code: цел32);
тип Opcode = массив 4 из симв8;
перем opcode: Opcode;
	tmp: симв8;
нач
	opcode := НИЗКОУР.подмениТипЗначения(Opcode, code);
	tmp := opcode[0]; opcode[0] := opcode[3]; opcode[3] := tmp;
	tmp := opcode[1]; opcode[1] := opcode[2]; opcode[2] := tmp;
	code := НИЗКОУР.подмениТипЗначения(цел32, opcode);
кон SwapBytes;

проц ARMDecoderFactory (reader : Потоки.Чтец) : Decoder.Decoder;
перем
	armDecoder : ARMDecoder;
нач
	нов(armDecoder, reader);
	возврат armDecoder
кон ARMDecoderFactory;

проц CodeScaleCallback(перем codeSize: размерМЗ);
нач
	codeSize := codeSize * 4
кон CodeScaleCallback;

проц Init*;
нач
	Decoder.RegisterDecoder("Oba", ARMDecoderFactory, CodeScaleCallback);
	ЛогЯдра.пСтроку8("ARMDecoder installed."); ЛогЯдра.пВК_ПС;
кон Init;

нач
	bigEndian := ложь;
кон ARMDecoder.

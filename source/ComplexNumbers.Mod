модуль ComplexNumbers;
(*
	operators for complex numbers
	this module is automatically loaded (FoxSemanticChecker) when operators on complex numbers are encountered
*)

использует Math, MathL;

	операция "+"*(конст left, right: компл32): компл32;
	перем result: компл32;
	нач
		комплRe(result) := комплRe(left) + комплRe(right);
		комплIm(result) := комплIm(left) + комплIm(right);
		возврат result
	кон "+";

	операция "+"*(конст left, right: компл64): компл64;
	перем result: компл64;
	нач
		комплRe(result) := комплRe(left) + комплRe(right);
		комплIm(result) := комплIm(left) + комплIm(right);
		возврат result
	кон "+";

	операция "-"*(конст left, right: компл32): компл32;
	перем result: компл32;
	нач
		комплRe(result) := комплRe(left) - комплRe(right);
		комплIm(result) := комплIm(left) - комплIm(right);
		возврат result
	кон "-";

	операция "-"*(конст left, right: компл64): компл64;
	перем result: компл64;
	нач
		комплRe(result) := комплRe(left) - комплRe(right);
		комплIm(result) := комплIm(left) - комплIm(right);
		возврат result
	кон "-";

	операция "*"*(конст left, right: компл32): компл32;
	перем result: компл32;
	нач
		комплRe(result) := комплRe(left) * комплRe(right) - комплIm(left) * комплIm(right);
		комплIm(result) := комплRe(left) * комплIm(right) + комплIm(left) * комплRe(right);
		возврат result
	кон "*";

	операция "*"*(конст left, right: компл64): компл64;
	перем result: компл64;
	нач
		комплRe(result) := комплRe(left) * комплRe(right) - комплIm(left) * комплIm(right);
		комплIm(result) := комплRe(left) * комплIm(right) + комплIm(left) * комплRe(right);
		возврат result
	кон "*";

	операция "/"*(конст left, right: компл32): компл32;
	перем result: компл32; iDivisor: вещ32;
	нач
		iDivisor := 1.0 / (комплRe(right) * комплRe(right) + комплIm(right) * комплIm(right));
		комплRe(result) := (комплRe(left) * комплRe(right) + комплIm(left) * комплIm(right)) * iDivisor;
		комплIm(result) := (комплIm(left) * комплRe(right) - комплRe(left) * комплIm(right)) * iDivisor;
		возврат result
	кон "/";

	операция "/"*(конст left, right: компл64): компл64;
	перем result: компл64; iDivisor: вещ64;
	нач
		iDivisor := 1.0D0 / (комплRe(right) * комплRe(right) + комплIm(right) * комплIm(right));
		комплRe(result) := (комплRe(left) * комплRe(right) + комплIm(left) * комплIm(right)) * iDivisor;
		комплIm(result) := (комплIm(left) * комплRe(right) - комплRe(left) * комплIm(right)) * iDivisor;
		возврат result
	кон "/";

	операция "матМодуль"*(конст arg: компл32): вещ32;
	нач возврат Math.sqrt(комплRe(arg) * комплRe(arg) + комплIm(arg) * комплIm(arg))
	кон "матМодуль";

	операция "матМодуль"*(конст arg: компл64): вещ64;
	нач возврат MathL.sqrt(комплRe(arg) * комплRe(arg) + комплIm(arg) * комплIm(arg))
	кон "матМодуль";

	операция "~"*(конст left: компл32): компл32;
	нач
		возврат комплRe(left) - комплIm(left) * IMAG
	кон "~";

	операция "~"*(конст left: компл64): компл64;
	нач
		возврат комплRe(left) - комплIm(left) * IMAG
	кон "~";

	операция "<="*(конст x, y: компл32): булево; нач возврат матМодуль(x) <= матМодуль(y); кон "<=";
	операция ">="*(конст x, y: компл32): булево; нач возврат матМодуль(x) >= матМодуль(y); кон ">=";
	операция "<"*(конст x, y: компл32): булево; нач возврат матМодуль(x) < матМодуль(y); кон "<";
	операция ">"*(конст x, y: компл32): булево; нач возврат матМодуль(x) > матМодуль(y); кон ">";

	операция "<="*(конст x, y: компл64): булево; нач возврат матМодуль(x) <= матМодуль(y); кон "<=";
	операция ">="*(конст x, y: компл64): булево; нач возврат матМодуль(x) >= матМодуль(y); кон ">=";
	операция "<"*(конст x, y: компл64): булево; нач возврат матМодуль(x) < матМодуль(y); кон "<";
	операция ">"*(конст x, y: компл64): булево; нач возврат матМодуль(x) > матМодуль(y); кон ">";


кон ComplexNumbers.

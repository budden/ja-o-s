модуль WMXMLTree; (** AUTHOR "TF"; PURPOSE "Simple XML Viewer"; *)

использует
	Потоки, XML, XMLObjects, WMGraphics,
	WMComponents, WMStandardComponents, WMTextView, WMEditors, WMEvents, Strings, TextUtilities, Texts,
	WMTrees, XMLScanner, XMLParser, UTF8Strings;

тип

	Error* = запись
		pos- : Потоки.ТипМестоВПотоке;
		line-, row- : Потоки.ТипМестоВПотоке;
		msg- : массив 128 из симв8;
	кон;

	ErrorList* = укль на массив из Error;

тип

	XMLView* = окласс(WMComponents.VisualComponent)
	перем
		tree : WMTrees.Tree;
		treeView : WMTrees.TreeView;
		toolbar : WMStandardComponents.Panel;
		errorMsg : WMEditors.Editor;
		refresh- : WMStandardComponents.Button;
		onRefresh- : WMEvents.EventSource;
		label- : WMStandardComponents.Label;
		hasErrors :булево;
		highlight : WMTextView.Highlight;

		(** Show error messages in XMLView? Default: FALSE *)
		showErrorMessage* : булево;

		errorList : ErrorList;

		text : Texts.Text;

		editor : WMEditors.Editor;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrXMLView);

			нов(onRefresh, сам, НУЛЬ, НУЛЬ, НУЛЬ);

			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			AddContent(toolbar);

			нов(label); label.alignment.Set(WMComponents.AlignTop);
			label.fillColor.Set(0CCCCCCFFH);
			label.caption.SetAOC("XML Structure (alpha)");
			label.bounds.SetHeight(20);
			сам.AddContent(label);

			нов(refresh); refresh.caption.SetAOC("Refresh"); refresh.alignment.Set(WMComponents.AlignLeft);
			toolbar.AddContent(refresh);
			refresh.onClick.Add(Refresh);

			нов(errorMsg);
			errorMsg.bounds.SetHeight(150); errorMsg.alignment.Set(WMComponents.AlignTop);
			errorMsg.visible.Set(ложь);
			AddContent(errorMsg);

			нов(treeView); treeView.alignment.Set(WMComponents.AlignClient);
			treeView.onClickNode.Add(Click);
			AddContent(treeView);
			tree := treeView.GetTree();
		кон Init;

		проц SetEditor*(e: WMEditors.Editor);
		нач
			если e = editor то возврат всё;
			если (highlight # НУЛЬ) и (editor # НУЛЬ) то
				editor.tv.RemoveHighlight(highlight);
				highlight := НУЛЬ
			всё;
			text := e.text;
			editor := e;
			highlight := editor.tv.CreateHighlight();
			highlight.SetColor(цел32(0DDDD0060H));
			highlight.SetKind(WMTextView.HLOver)
		кон SetEditor;

		проц Click(sender, data : динамическиТипизированныйУкль);
		перем p : динамическиТипизированныйУкль; a, b : размерМЗ;
		нач
			если (data # НУЛЬ) и (data суть WMTrees.TreeNode) то
				tree.Acquire;
				p := tree.GetNodeData(data(WMTrees.TreeNode));
				tree.Release;
				если (p # НУЛЬ) и (p суть XML.Element) то
					если editor # НУЛЬ то
						text.AcquireRead;
						editor.tv.cursor.SetPosition(p(XML.Element).GetPos()(размерМЗ));
						editor.tv.cursor.SetVisible(истина);
						editor.tv.FindCommand((p(XML.Element).GetPos()-1)(размерМЗ), a, b);
						если highlight # НУЛЬ то highlight.SetFromTo(a, b) всё;
						text.ReleaseRead;
					всё
				всё
			всё;
		кон Click;

		проц AddSubNode(node : WMTrees.TreeNode; xml : XML.Element );
		перем en : XMLObjects.Enumerator;
			p : динамическиТипизированныйУкль; s,t,c : Strings.String;
			newNode : WMTrees.TreeNode;
		нач
			нов(newNode);
			tree.AddChildNode(node, newNode);
			tree.SetNodeData(newNode, xml);

			s := xml.GetName();
			t := xml.GetAttributeValue("name");
			если (t#НУЛЬ) то
				нов(c,Strings.Length(s^) + Strings.Length(t^) + 1 + 4);
				c[0] := 0X;
				если (s # НУЛЬ) то
					Strings.Append(c^,s^);
					Strings.Append(c^,': ');
				всё;
				Strings.Append(c^,'"');
				Strings.Append(c^,t^);
				Strings.Append(c^,'"');
			иначе
				c := s;
			всё;

			если c # НУЛЬ то tree.SetNodeCaption(newNode, c) всё;

			en := xml.GetContents();
			нцПока en.HasMoreElements() делай
				p := en.GetNext();
				если p суть XML.Element то
					AddSubNode(newNode, p(XML.Element));
				всё
			кц;
		кон AddSubNode;

		проц SetDocument(xml : XML.Element);
		перем en : XMLObjects.Enumerator;
			p : динамическиТипизированныйУкль;
			node : WMTrees.TreeNode;
		нач
			нов(node);
			tree.Acquire;
			tree.SetRoot(node);
			tree.SetNodeState(node, {WMTrees.NodeAlwaysExpanded});
			tree.SetNodeData(node, xml);

			если xml # НУЛЬ то
				en := xml.GetContents();

				нцПока en.HasMoreElements() делай
					p := en.GetNext();
					если p суть XML.Element то
						AddSubNode(node, p(XML.Element));
					всё
				кц
			всё;
			tree.Release
		кон SetDocument;

		(* Return a copy of the errorList or NIL in case of no errors *)
		проц GetErrorList*() : ErrorList;
		перем result : ErrorList; i : размерМЗ;
		нач
			если errorList # НУЛЬ то
				нов(result, длинаМассива(errorList));
				нцДля i := 0 до длинаМассива(errorList)-1 делай
					result[i] := errorList[i];
				кц;
			всё;
			возврат result;
		кон GetErrorList;

		проц AddErrorToList(pos, line, row : Потоки.ТипМестоВПотоке; конст msg : массив из симв8);
		перем temp : ErrorList; i : размерМЗ;
		нач
			если errorList = НУЛЬ то
				i := 0;
				нов(errorList, 1);
			иначе
				нов(temp, длинаМассива(errorList)+1);
				нцДля i := 0 до длинаМассива(errorList)-1 делай
					temp[i] := errorList[i];
				кц;
				errorList := temp;
			всё;
			errorList[i].pos := pos;
			errorList[i].line := line;
			errorList[i].row := row;
			копируйСтрокуДо0(msg, errorList[i].msg);
		кон AddErrorToList;

		проц Error(pos, line, row: Потоки.ТипМестоВПотоке; конст msg: массив из симв8);
		перем tw : TextUtilities.TextWriter;
		нач
			AddErrorToList(pos, line, row, msg);
			нов(tw, errorMsg.text);
			tw.SetFontStyle({WMGraphics.FontBold});
			tw.пСтроку8(msg); tw.пВК_ПС;
			tw.SetFontStyle({});
			tw.пСтроку8("at pos "); tw.пЦел64(pos, 0); tw.пСтроку8(" (in line "); tw.пЦел64(line, 0); tw.пСтроку8(" row "); tw.пЦел64(row, 0); tw.пСтроку8(")"); tw.пВК_ПС;
			tw.пВК_ПС;
			hasErrors := истина;
			tw.ПротолкниБуферВПоток
		кон Error;

		проц Refresh*(sender, data : динамическиТипизированныйУкль);
		перем r : Потоки.ЧтецИзСтроки;
			scanner : XMLScanner.Scanner;
			parser : XMLParser.Parser;
			doc : XML.Document;
			tr : Texts.TextReader; ch : Texts.Char32; i, p : размерМЗ; resstr : массив 7 из симв8;
			out : Потоки.Писарь;
			ob : Strings.Buffer;
			s : Strings.String;
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.Refresh, sender, data)
			иначе
				errorMsg.text.AcquireWrite;
				errorMsg.text.Delete(0, errorMsg.text.GetLength());
				errorMsg.text.ReleaseWrite;
				errorList := НУЛЬ;
				hasErrors := ложь;

				если text = НУЛЬ то возврат всё;
				text.AcquireRead;
				нов(ob, (text.GetLength() * 3 DIV 2)); (* heuristic to avoid growing in most cases *)
				out := ob.GetWriter();

				нов(tr, text);
				нцДля i := 0 до text.GetLength() - 1 делай
					tr.ReadCh(ch); p := 0;
					если (ch > Texts.Симв32Нулевой) и UTF8Strings.EncodeChar(ch, resstr, p) то out.пСтроку8(resstr) всё
				кц;
				out.ПротолкниБуферВПоток;
				text.ReleaseRead;

				нов(r, ob.GetLength() + 1);
				s := ob.GetString();
				r.ПримиСрезСтроки8ВСтрокуДляЧтения(s^, 0, ob.GetLength());

				hasErrors := ложь;
				нов(scanner, r); scanner.reportError := Error;
				нов(parser, scanner); parser.reportError := Error;
				doc := parser.Parse();
				errorMsg.visible.Set(showErrorMessage и hasErrors);
				если hasErrors то errorMsg.tv.firstLine.Set(0);
					label.caption.SetAOC("XML Structure (ERRORS)");
					label.fillColor.Set(0FF0000FFH);
				иначе
					label.caption.SetAOC("XML Structure");
					label.fillColor.Set(0CCCCCCFFH);
				всё;
				если doc # НУЛЬ то
					SetDocument(doc.GetRoot())
				всё;
				onRefresh.Call(сам);
			всё
		кон Refresh;

	кон XMLView;

перем
	StrXMLView : Strings.String;

нач
	StrXMLView := Strings.NewString("XMLView");
кон WMXMLTree.

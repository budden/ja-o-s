модуль CryptoDSA;	(** AUTHOR "G.F."; PURPOSE "Digital Signature Algorithm DSA"; *)

использует
	B := CryptoBigNumbers, SHA1 := CryptoSHA1, P := CryptoPrimes, U := CryptoUtils, Потоки,
	Base64 := CryptoBase64, BIT;

тип
	Number = B.BigNumber;

	Signature* = окласс
		перем
			r-	: Number;
			s-	: Number;

			проц & Init*( r, s: Number );
			нач
				B.Copy( r, сам.r );
				B.Copy( s, сам.s )
			кон Init;

	кон Signature;

	Key* = окласс
		перем
			name-: массив 128 из симв8;   (** Owner of this key. *)
			private-: булево;
			p-	: Number;
			q-	: Number;
			g-	: Number;
			y-	: Number;
			inv, r : Number;

			проц Sign*( конст digest: массив из симв8;  len: размерМЗ  ): Signature;
			перем  m, xr, ss: Number;  sig: Signature;
			нач
				утв( private );
				если (len > q.len*4 ) или (len > 50) то  СТОП( 102 )  всё;

				B.AssignBin( m, digest, 0, len );
				xr := B.Mul( y, r );
				ss := B.Add( xr, m );	(* s := inv( k ) * (m + x*r) mod q *)
				если B.Cmp( ss, q ) > 0 то  ss := B.Sub( ss, q )  всё;
				ss := B.ModMul( ss, inv, q );
				нов( sig, r, ss );
				возврат sig
			кон Sign;

			проц Verify*(  конст digest: массив из симв8;  dlen: размерМЗ;  sig: Signature ): булево;
			перем
				u, v, w, t1, t2: Number;
			нач
				утв( ~private );
				если B.Cmp( sig.r, q ) >= 0 то  возврат ложь  всё;
				если B.Cmp( sig.s, q ) >= 0 то  возврат ложь  всё;
				(* w = inv( s ) mod q *)
				w := B.ModInverse( sig.s, q );
				(* v = m * w mod q *)
				B.AssignBin( v, digest, 0, dlen );
				v := B.ModMul( v, w, q );
				(* u = r * w mod q *)
				u := B.ModMul( sig.r, w, q );
				(* v = (g^v * y^u mod p) mod q *)
				t1 := B.ModExp( g, v, p );
				t2 := B.ModExp( y, u, p);
				v := B.ModMul( t1, t2, p );
				v := B.Mod( v, q );
				возврат B.Cmp( v, sig.r ) = 0;
			кон Verify;

	кон Key;

перем
	one: Number;	(* constant *)


	проц GenParams*( dsa: Key;  bits: цел16;  конст seed: массив из симв8 );
	перем
		randomseed, pfound: булево;
		i, k, n, count: цел16;
		test, W, r0: Number;
		sbuf, buf, buf2, md: массив 20 из симв8;
		h: SHA1.Hash;
	нач
		если bits < 512 то
			bits := 512;
		иначе
			bits := (bits + 63) DIV 64 * 64;
		всё;
		randomseed := длинаМассива( seed ) < 20;
		если ~randomseed то
			нцДля i := 0 до 19 делай  sbuf[i] := seed[i]  кц;
		всё;
		B.AssignInt( test, 1 );
		test.Shift( bits - 1 );
		нов( h );
		pfound := ложь;
		нц (* find q and p *)
			нцДо (* find q *)
				если randomseed то
					B.RandomBytes( sbuf, 0, 20 );
				всё;
				buf := sbuf;
				buf2 := sbuf;
				(* precompute "SEED + 1" *)
				нцДо
					умень( i );
					buf[i] := симв8ИзКода( кодСимв8( buf[i] ) + 1);
				кцПри (i = 0) или (buf[i] # 0X );
				h.Initialize;
				h.Update( sbuf, 0, 20 );
				h.GetHash( md, 0 );
				h.Initialize;
				h.Update( buf, 0, 20 );
				h.GetHash( buf2, 0 );
				нцДля i := 0 до 19 делай (* md := md xor buf2*)
					md[ i ] := BIT.CXOR( md[ i ], buf2[ i ] );
				кц;
				если кодСимв8( md[0] ) < 128 то
					md[0] := симв8ИзКода( кодСимв8( md[0] ) + 128 );
				всё;
				если ~нечётноеЛи¿( кодСимв8( md[19] ) ) то
					md[19] := симв8ИзКода( кодСимв8( md[19] ) + 1 );
				всё;
				B.AssignBin( dsa.q, md, 0, 20 );
			кцПри P.IsPrime( dsa.q, 50, randomseed );
			count := 0;  n := bits  DIV 160;
			нц (* find p *)
				B.AssignInt( W, 0 );
				(* now 'buf' contains "SEED + offset - 1" *)
				нцДля k := 0 до n  делай
					(* obtain "SEED + offset + k" by incrementing: *)
					i := 20;
					нцДо
						умень( i );
						buf[i] := симв8ИзКода( кодСимв8( buf[i] ) + 1)
					кцПри (i = 0) или (buf[i] # 0X );
					h.Initialize;
					h.Update( buf, 0, 20 );
					h.GetHash( md, 0 );
					B.AssignBin( r0, md, 0, 20 );
					r0.Shift( 160*k );  W := B.Add( W, r0 );
				кц;
				W.Mask( bits - 1);
				W := B.Add( W, test );
				B.Copy( dsa.q, r0 );
				r0.Shift( 1 );
				r0 := B.Mod( W, r0 );
				r0.Dec;
				dsa.p := B.Sub( W, r0 );
				если B.Cmp( dsa.p, test ) >= 0 то
					если P.IsPrime( dsa.p, 50, истина ) то
						pfound := истина;
						прервиЦикл;
					всё;
				всё;
				увел( count );
				если count >= 4096 то  прервиЦикл  всё;
			кц; (* find p *)
			если pfound то  прервиЦикл  всё;
		кц; (* find q and p *)

		B.Copy( dsa.p, test );
		test.Dec;
		r0 := B.Div( test, dsa.q );	(*  r0 := (p-1)/q *)
		B.AssignInt( test, 2 );
		нц (* g := test ^ r0 mod p *)
			dsa.g := B.ModExp( test, r0, dsa.p );
			если B.Cmp( dsa.g, one ) # 0 то  прервиЦикл  всё;
			test.Inc;
		кц;
	кон GenParams;

	проц MakeKeys*( bits: цел16;  конст seed: массив из симв8;  перем pub, priv: Key );
	нач
		нов( priv );
		GenParams( priv, bits, seed );
		нцДо
			priv.y := B.NewRandRange( priv.q )
		кцПри ~priv.y.IsZero( );
		нов( pub );  pub^ := priv^;
		pub.y := B.ModExp( priv.g, priv.y, priv.p );
		priv.private := истина;
		priv.r := B.Mod( pub.y, priv.q );	(* r := (g ^ k mod p) mod q *)
		priv.inv := B.ModInverse( priv.y, priv.q )	(* part of 's := inv( k ) * (m + x*r) mod q' *)
	кон MakeKeys;


	(** returns a new public key with exponent e and modulus m *)
	проц PubKey*( p, q, g, y: Number ): Key;
		перем dsa: Key;
	нач
		нов( dsa );
		dsa.name := "unkown";
		dsa.private := ложь;
		B.Copy( p, dsa.p );
		B.Copy( q, dsa.q );
		B.Copy( g, dsa.g );
		B.Copy( y, dsa.y );
		возврат dsa
	кон PubKey;


	проц LoadPrivateKey*( r: Потоки.Чтец;  конст passwd: массив из симв8 ): Key;
	(* TODO *)
	кон LoadPrivateKey;

	проц StorePrivateKey*( w: Потоки.Писарь;  k: Key;  конст passwd: массив из симв8 );
	(* TODO *)
	кон StorePrivateKey;


	проц StorePublicKey*( w: Потоки.Писарь; k: Key );	(* openssh format *)
	перем buf, encoded: массив 4096 из симв8; pos: размерМЗ;
	нач
		утв( ~k.private );
		w.пСтроку8( "ssh-dss " );
		pos := 0;
		U.PutString( buf, pos, "ssh-dss" );
		U.PutBigNumber( buf, pos, k.p );
		U.PutBigNumber( buf, pos, k.q );
		U.PutBigNumber( buf, pos, k.g );
		U.PutBigNumber( buf, pos, k.y );
		Base64.Encode( buf, pos, encoded );
		w.пСтроку8( encoded );
		w.пСтроку8( " user@Aos" )
	кон StorePublicKey;


	проц LoadPublicKey*( r: Потоки.Чтец ): Key;
	перем buf: массив 4096 из симв8; len: цел32; pos: размерМЗ;
		str: массив 64 из симв8;
		k: Key;
	нач
		нов( k ); k.private := ложь;
		len := Base64.DecodeStream( r, buf );
		pos := 0;
		U.GetString( buf, pos, str );
		утв( str = "ssh-dss" );
		U.GetBigNumber( buf, pos, k.p );
		U.GetBigNumber( buf, pos, k.q );
		U.GetBigNumber( buf, pos, k.g );
		U.GetBigNumber( buf, pos, k.y );
		возврат k
	кон LoadPublicKey;

нач
	B.AssignInt( one, 1 );
кон CryptoDSA.


System.Free CryptoDSA ~

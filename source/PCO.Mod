(* Paco, Copyright 2000 - 2002, Patrik Reali, ETH Zurich *)

модуль PCO; (** AUTHOR "prk / be"; PURPOSE "Parallel Compiler: Intel 386 code patterns"; *)

(*
	Based on the OPO module for OP2 by N. Mannhart
*)

	использует НИЗКОУР, PCM, PCLIR;


	конст

	(** mode and scale factors *)
	noScale* = 0; Scale1* = 0; Scale2* = 1; Scale4* = 2; Scale8* = 3;	(** scale factors *)
	RegReg* 	= 0;	(** register to register *)
	RegMem* 	= 1;	(** register to memory *)
	MemReg* 	= 2;	(** memory to register *)
	ImmReg* 	= 3;	(** immediate to register *)
	ImmMem* 	= 4;	(** immediate to memory *)
	RegSt* 	= 5; 	(** floating point st, reg *)
	StReg* 	= 6; 	(** floating point reg, st *)
	StRegP* 	= 7;	(** floating point ...p reg, st (with pop *)
	MemSt* 	= 8; 	(** floating point st, mem *)
	ForceDisp32*	= 20;	(** modes >= Disp32 have always a disp of 32-bits, don't overlap with Reg, Abs, RegRel, Coc *)
	RegMemA*	= RegMem+ForceDisp32;	(** register to memory, disp is 32-bit *)
	MemAReg*	= MemReg+ForceDisp32;	(** memory to register, disp is 32-bit *)
	ImmAReg*	= ImmReg+ForceDisp32;	(** immediate to memory, disp is 32-bit *)
	ImmMemA*	= ImmMem+ForceDisp32;	(** immediate to memory, disp is 32-bit *)
	MemASt* 	= MemSt+ForceDisp32; 	(** floating point st, mem *)

	(** item modes for Intel i386 (must not overlap item basemodes, > 13 *)
		Reg* = 15; Abs* = 16; RegRel* = 17; Coc* = 18;

	(** aliases used for instructions with one register like not, neg etc. *)
	Regs* 	= RegReg;
	Mem* 	= MemReg;
	Imme* 	= ImmReg;
	MemA*	= MemAReg;
	ImmeA*	= ImmAReg;

	(** i387 *)
	sReal* 	= 0; 	(** short real 32 bit = Bit32 *)
	lReal* 	= 2; 	(** long real 64 bit *)
	eReal* 	= 4;	(** extended real 80 bit, only valid in GenFLD, GenFSTP *)
	dInt* 	= 1;	(* double integer 32 bit *)
	wInt* = 3; 	(** word integer 16 bit *)
	qInt* = 5; 	(** quad integer 64 bit *)
(* !not implemented! and probably never used
	wInt = 1 	(** word integer 16 bit *)
	lInt = 5; 	(** long integer 64 bit *)
*)

	(* code and data length per module *)
	MaxCodeLength* = 2147483647;	(* 2^31 - 1, i.e. the maximum positive SIGNED32 number *)
	InitialCodeLength = 65536;
	(*MaxConstLength*	= 2147483647;	(* Max Const size allowed, limited by Object File Format, see PCBT.Mod*)*)

	(** i386 Register *)
		EAX* = 0; ECX* = 1; EDX* = 2; EBX* = 3; ESP* = 4; EBP* = 5; ESI* = 6; EDI* = 7; (** 32 bit register *)
		AX* = 8; CX* = 9; DX* = 10; BX* = 11; SP* =  12; BP* = 13; SI* = 14; DI* = 15; (** 16 bit register *)
		AL* = 16; CL* = 17; DL* = 18; BL* = 19; AH* = 20; CH* = 21; DH* = 22; BH* = 23; (** 8 bit register *)

	(** register/memory size (8,16, 32 or 64 bit) *)
	Bit8* = AL; Bit16* = AX; Bit32* = EAX (* must be 0 *); Bit64* = Bit32 + 8;


	noDisp* = 0; Disp8* = 1; Disp32* = 2; none = -1;
	noBase* = none; noInx* = none; noImm* = 0;

	BUG = 42;

	(** opcodes used for generating i386 code *)

		(**  GenShiftRot *)
		opROL* = 0; Ror* = 1; RCL* = 2; RCR* = 3; opSHL* = 4; SAL* = 4; opSHR* = 5; SAR*= 7;

		(** GenSHDouble *)
		Left* = 0; Right* = 1;

		(**  GenString, GenRepString, GenRepCmpsScas *)
		CMPS* = 53H; INS* = 36H; LODS* = 56H; MOVS* = 52H; OUTS* = 37H; SCAS* = 57H; STOS* = 55H;

		(** GenJcc *)
		JO* = 0; JNO* = 1; JB* = 2; JC* = 2; JNAE* = 2; JNB* = 3; JNC* = 3; JAE* = 3; JE* = 4; JZ* = 4;
		JNE* = 5; JNZ* = 5; JBE* = 6; JNA* = 6; JNBE* =7; JA* = 7; JS* = 8; JNS* = 9; JP* = 10; JPE* = 10;
		JNP* = 11; JPO* = 11; JL* = 12; JNGE* = 12; JNL* = 13; JGE* = 13; JLE* = 14; JNG* = 14;
		JNLE* = 15; JG* = 15;

		(** GenGroup3 *)
		NOT* = 2; NEG* = 3;

		(** GenTyp1 *)
		ADD* = 0; ADC* = 10H; SUB* = 28H; SBB* = 18H; CMP* = 38H; AND* = 20H; Or* = 8H; XOR* = 30H;

		(** Wait *)
		wWAIT* = 9BH;

		(** GenFop1 *)
		FCOMPP* = 0; FTST* = 1; FLDZ* = 2; FLD1* = 3; FABS* = 4; FCHS* = 5; FSTSW* = 6; FINCSTP* = 7; FDECSTP* = 8;

		(** GenB *)
		BT* = 4; BTR* = 6; BTS* = 5;

		(** general *)
		SAHF* = 9EH; CLD* = 0FCH; STD* = 0FDH; CBW* = 98H; CWD* = 99H (* = CDQ *);
		CLI* = 0FAH; STI* = 0FBH;


	перем
	pc*, dsize*: цел32; 		(* code size, data size *)
	lastImmSize*: цел8;		(* last immediate Size *)
	code*: PCLIR.CodeArray;	(* i386/i387 code area *)
	codeLength : цел32;
	errpos*: цел32;
	CodeErr*: булево;

(* i386 Code Generator *)

   	проц PutByteAt* (pos: цел32; b: цел16);
   	перем c: PCLIR.CodeArray;
   	нач
       	если pos >= codeLength то
          		если codeLength + InitialCodeLength <= MaxCodeLength то
               		codeLength := codeLength + InitialCodeLength;
               		нов(c, codeLength);
               		НИЗКОУР.копируйПамять(адресОт(code[0]), адресОт(c[0]), длинаМассива(code));
               		code := c;
               		code[pos] := симв8ИзКода(b); (*fof*)
           		иначе
               		если ~CodeErr то  PCM.Error(210, PCM.InvalidPosition, ""); CodeErr:= истина  всё;
               		pc:= 0
           		всё
       	иначе
           		code[pos]:= симв8ИзКода (b)
       	всё
   	кон PutByteAt;

	проц PutByte* (b: цел16);
	нач
		PutByteAt(pc, b); увел(pc);
		если pc >= PCM.breakpc то PCM.Error(400, errpos, ""); PCM.breakpc := матМаксимум(цел32) всё; (* << mh 30.8.94 *)
	кон PutByte;

	проц PutWord (w: цел32);
	нач
		PutByteAt(pc, устарПреобразуйКБолееУзкомуЦел( w остОтДеленияНа 100H)); увел(pc);
		PutByteAt(pc, устарПреобразуйКБолееУзкомуЦел((w DIV 100H) остОтДеленияНа 100H)); увел(pc);
		если pc >= PCM.breakpc то PCM.Error(400, errpos, ""); PCM.breakpc := матМаксимум(цел32) всё; (* << mh 30.8.94 *)
	кон PutWord;

  	 проц PutDWordAt* (pos, dw: цел32);
  	 перем c: PCLIR.CodeArray;
  	 нач
      		 если pos >= codeLength - 4 то
          		 если codeLength + InitialCodeLength <= MaxCodeLength то
               		codeLength := codeLength + InitialCodeLength;
               		нов(c, codeLength);
               		НИЗКОУР.копируйПамять(адресОт(code[0]), адресОт(c[0]), длинаМассива(code));
               		code := c;
               		 (*fof>>*)
                		code[pos]:= симв8ИзКода ( dw остОтДеленияНа 100H); увел (pos); (* low byte first *)
               		code[pos]:= симв8ИзКода ( ( dw DIV 100H) остОтДеленияНа 100H); увел (pos);
               		code[pos]:= симв8ИзКода ( ( dw DIV 10000H) остОтДеленияНа 100H); увел (pos);
               		code[pos]:= симв8ИзКода (dw DIV 1000000H)
              		 (*fof<<*)
           		иначе
              		 если ~CodeErr то  PCM.Error(210, PCM.InvalidPosition, ""); CodeErr:= истина  всё;
               		pc:= 0
           		всё
       	иначе
           		code[pos]:= симв8ИзКода ( dw остОтДеленияНа 100H); увел (pos); (* low byte first *)
           		code[pos]:= симв8ИзКода ( ( dw DIV 100H) остОтДеленияНа 100H); увел (pos);
           		code[pos]:= симв8ИзКода ( ( dw DIV 10000H) остОтДеленияНа 100H); увел (pos);
          		code[pos]:= симв8ИзКода (dw DIV 1000000H)
       	всё
  	кон PutDWordAt;

	проц PutDWord* (dw: цел32);
	нач
		PutDWordAt(pc, dw); увел(pc, 4);
		если pc >= PCM.breakpc то PCM.Error(400, errpos, ""); PCM.breakpc := матМаксимум(цел32) всё; (* << mh 30.8.94 *)
	кон PutDWord;

	проц GetDWord* (pos: цел32; перем dw: цел32);
		перем byte: цел16;
	нач
		dw:= кодСимв8 (code[pos]) + устарПреобразуйКБолееШирокомуЦел (кодСимв8 (code[pos+1])) * 100H + устарПреобразуйКБолееШирокомуЦел (кодСимв8 (code[pos+2])) * 10000H;
		byte:= кодСимв8 (code[pos+3]);
		если byte >= 128 то byte:= byte - 256 всё;
		dw:= устарПреобразуйКБолееШирокомуЦел (byte) * 1000000H + dw
	кон GetDWord;

	проц PutReg (reg1, reg2: цел16);
	(* encodes register register addressing mode *)
	нач
		reg1:= reg1 остОтДеленияНа 8; reg2:= reg2 остОтДеленияНа 8;
		PutByte (3 * 40H + reg1 * 8+ reg2)
	кон PutReg;

	проц PutRMInx (reg, base, inx, scale: цел16; disp: цел32;  disp32: булево);
	(* put register memory with index *)
	нач
		reg:= reg остОтДеленияНа 8; inx:= inx остОтДеленияНа 8;
		если base = noBase (*mode = Abs*) то
			PutByte (noDisp * 40H + reg * 8 + 4); (* s-i-b is present *)
			PutByte (scale * 40H + inx * 8+ 5);	(* DS:[d32 + inx*scale] *)
			PutDWord (disp)
		иначе (* RegRel *)
			base:= base остОтДеленияНа 8;
			если (disp = 0) и (base # EBP) то
				PutByte (noDisp * 40H + reg * 8 + 4);	(* s-i-b is present *)
				PutByte (scale * 40H + inx * 8 + base)	(* DS:[base + inx*scale] *)
			аесли (disp <= 127) и (disp >= -128) и ~disp32 то
				PutByte (Disp8 * 40H + reg * 8 + 4); 	(* s-i-b is present + disp8*)
				PutByte (scale * 40H + inx * 8 + base);	(* DS:[base + inx*scale] *)
				PutByte (устарПреобразуйКБолееУзкомуЦел (disp))
			иначе
				PutByte (Disp32 * 40H + reg * 8 + 4);	(* s-i-b is present + disp32*)
				PutByte (scale * 40H + inx * 8 + base);	(* DS:[base + inx*scale] *)
				PutDWord (disp)
			всё
		всё
	кон PutRMInx;

	проц PutRegMem ((*mode: SIGNED8;*) reg, base: цел16; disp: цел32;  disp32: булево);
	(* put register memory *)
	нач
		reg:= reg остОтДеленияНа 8;
		если base = noBase (*mode = Abs*) то
			PutByte (noDisp * 40H + reg * 8 + 5);	(* DS:d32 *)
			PutDWord (disp)
		иначе (* mode = RegRel *)
			base:= base остОтДеленияНа 8;
			если base = ESP то (* 2 bytes address encoding necessary *)
				PutRMInx (reg, base, 4, Scale1, disp, disp32) (* no index register: reg, disp[ESP] *)
			аесли (disp = 0) и (base # EBP) и ~disp32 то (* no displacement *)
				PutByte (noDisp * 40H + reg * 8 + base)	(*  DS:[base] *)
			аесли (disp <= 127) и (disp >= -128) и ~disp32 то
				PutByte (Disp8 * 40H + reg * 8 + base);	(*  DS:[base + d8] *)
				PutByte (устарПреобразуйКБолееУзкомуЦел (disp))
			иначе
				PutByte (Disp32 * 40H + reg * 8 + base);	(*  DS:[base + d32] *)
				PutDWord (disp)
			всё
		всё
	кон PutRegMem;

	проц PutMem (reg, base, inx: цел16; scale: цел8; disp: цел32;  disp32: булево);
	(* put memory *)
	нач
		если inx = noInx то PutRegMem (reg, base, disp, disp32)
		иначе PutRMInx (reg, base, inx, scale, disp, disp32)
		всё
	кон PutMem;

	проц PutDisp (disp: цел32; size: цел16);
	нач
		lastImmSize:= устарПреобразуйКБолееУзкомуЦел (size);
		если size >= Bit8 то PutByte (устарПреобразуйКБолееУзкомуЦел (disp))
		аесли size >= Bit16 то PutWord (disp)
		иначе PutDWord (disp) (* size =  Bit32 *)
		всё
	кон PutDisp;

	проц Prefix* (reg: цел16; перем w: цел8);
	(* put out the Operand Size Prefix if necessary
		w = 0 : 8 bit data w= 1: 16/32 bit data *)
	нач
		если reg в {AX..DI} то
			PutByte (66H); w:= 1 (* Operand Size Prefix *)
		аесли reg в {EAX..EDI} то w:= 1
		иначе w:= 0
		всё
	кон Prefix;

	проц GenMOV* (mode: цел8; reg, base, inx: цел16; scale: цел8; disp, imm: цел32);
		перем w: цел8; r: цел16;
	нач
		Prefix (reg, w);
		просей mode из
			RegReg:
				(* reg = destination, base = source *)
				PutByte (8AH + w); PutReg (reg, base)
		  | RegMem, RegMemA:
				если ( (reg = EAX) или (reg = AX) или (reg = AL) ) и (inx = noInx) и (base = none) то
					PutByte (0A2H + w); PutDWord (disp)
				иначе
					PutByte (88H + w); PutMem (reg, base, inx, scale, disp, mode = RegMemA)
				всё
		  | MemReg, MemAReg:
				если ( (reg = EAX) или (reg = AX) или (reg = AL) ) и (inx = noInx) и (base = none) то
					PutByte (0A0H + w); PutDWord (disp)
				иначе
					PutByte (8AH + w); PutMem (reg, base, inx, scale, disp, mode = MemAReg)
				всё
		  | ImmReg:
				r:= reg; reg:= reg остОтДеленияНа 8;
				если imm = 0 то
					PutByte (30H + 2H + w); PutReg(r, r)	(*use XOR reg, reg*)
				иначе
					PutByte (0B0H + w * 8 + reg); PutDisp (imm, r)
				всё
		  | ImmAReg:
				r:= reg; reg:= reg остОтДеленияНа 8;
				PutByte (0B0H + w * 8 + reg); PutDisp (imm, r)
		  | ImmMem, ImmMemA:
				PutByte (0C6H + w); PutMem (reg, base, inx, scale, disp, mode = ImmMemA); PutDisp (imm, reg)
		иначе СТОП (BUG)
		всё
	кон GenMOV;

	проц GenMOVSX* (mode, s: цел8; reg, base, inx: цел16; scale: цел8; disp: цел32);
		перем w: цел8; (* s = 0: 8 bit; s = 1: 16/32 bit *)
	нач
		Prefix (reg, w);
		PutByte (0FH); PutByte (0BEH + s);
		просей mode из
			RegReg:
				(* reg = destination, base = source *)
				PutReg (reg, base)
		  | MemReg, MemAReg:
				PutMem (reg, base, inx, scale, disp, mode = MemAReg)
		иначе СТОП (BUG)
		всё
	кон GenMOVSX;

	проц GenMOVZX* (mode, s: цел8; reg, base, inx: цел16; scale: цел8; disp: цел32);
		перем w: цел8; (* s = 0: 8 bit; s = 1: 16/32 bit *)
	нач
		Prefix (reg, w);
		PutByte (0FH); PutByte (0B6H+s);
		просей mode из
			RegReg: (* reg = destination, base = source *)
				PutReg (reg, base)
		  | MemReg, MemAReg:
				PutMem (reg, base, inx, scale, disp, mode = MemAReg)
		иначе СТОП (BUG)
		всё
	кон GenMOVZX;

	проц GenIN* (size: цел16);
	нач
		просей size из
		    Bit32: PutByte(0EDH)		(* IN EAX, DX *)
		 | Bit16: PutWord(0ED66H)   (* IN AX, DX *)
		 | Bit8: PutByte(0ECH);	    (* IN AL, DX *)
		 всё
	кон GenIN;

	проц GenOUT* (size: цел16);
	нач
		просей size из
		    Bit32: PutByte(0EFH)		(* OUT DX, EAX *)
		 | Bit16: PutWord(0EF66H)   (* OUT DX, AX *)
		 | Bit8: PutByte(0EEH);	    (* OUT DX, AL *)
		 всё
	кон GenOUT;

	проц GenPUSH* (mode: цел8; reg, base, inx: цел16; scale: цел8; disp, imm: цел32);
		перем w: цел8;
	нач
		если (mode = Imme) и (reg = AX) то  reg := EAX  всё;	(*16bit immediate -> push 32 bit *)
		Prefix (reg, w);
		просей mode из
			Regs:
				reg:= reg остОтДеленияНа 8;
				PutByte (50H + reg)
		  | Mem, MemA: (* push memory *)
				reg:= ESI; PutByte (0FFH); PutMem (reg, base, inx, scale, disp, mode = MemA)
		  | Imme: (* push immediate *)
				если (imm <= 127) и (imm >= -128) то
					PutByte (6AH); PutByte (устарПреобразуйКБолееУзкомуЦел (imm))
				иначе
					PutByte (68H); PutDWord (imm)
				всё
		  | ImmeA: (* push immediate *)
		  		PutByte (68H); PutDWord (imm)
		иначе СТОП (BUG)
		всё
	кон GenPUSH;

	проц GenPOP* (mode: цел8; reg, base, inx: цел16; scale: цел8; disp: цел32);
		перем w: цел8;
	нач
		Prefix (reg, w);
		просей mode из
			Regs:
				reg:= reg остОтДеленияНа 8; PutByte (58H + reg)
		  | Mem, MemA: (* pop memory *)
				reg:= EAX; PutByte (8FH); PutMem (reg, base, inx, scale, disp, mode = MemA)
		иначе СТОП (BUG)
		всё
	кон GenPOP;

	проц GenXCHG* (mode: цел8; reg, base, inx: цел16; scale: цел8; disp: цел32);
		перем w: цел8;
	нач
		Prefix (reg, w);
		просей mode из
			RegReg:
				если (reg = EAX) или (reg = AX) то
					base:= base остОтДеленияНа 8;
					PutByte (90H + base)
				иначе
					PutByte (86H + w); PutReg (reg, base)
				всё
		  | RegMem, RegMemA:
				PutByte (86H + w); PutMem (reg, base, inx, scale, disp, mode = RegMemA)
		иначе СТОП (BUG)
		всё
	кон GenXCHG;

	проц GenLEA* (disp32: булево; reg, base, inx: цел16; scale: цел8; disp: цел32);
	нач
		(* no prefix necessary, addressing mode is always 32 bit *)
		если (reg = base) и (inx = noInx) и (disp = 0) то
			(* skip: LEA reg, 0[reg] has no effect *)
		иначе
			PutByte (8DH); PutMem (reg, base, inx, scale, disp, disp32)
		всё
	кон GenLEA;

	проц GenTyp1* (op, mode: цел8; reg, base, inx: цел16; scale: цел8; disp, imm: цел32);
	(* general code generator procedure for ADD, ADC, AND, XOR, OR, SBB, SUB, CMP *)
		перем w, wImm: цел8;
	нач
		Prefix (reg, w);
		просей mode из
			RegReg: (* reg = destination, base = source *)
				PutByte (op + 2H + w); PutReg (reg, base);
		  | RegMem, RegMemA:
				PutByte (op + w); PutMem (reg, base, inx, scale, disp, mode = RegMemA)
		  | MemReg, MemAReg:
				PutByte (op + 2H + w); PutMem (reg,base, inx, scale, disp, mode = MemAReg)
		  | ImmReg, ImmAReg:
				если (reg = EAX) или (reg = AX) или (reg = AL) то
					PutByte (op + 4H + w); PutDisp (imm, reg)
				иначе
					если reg >= AL то (* 8 bit *)
						PutByte (80H); wImm:= Bit8
					аесли (mode = ImmReg) и (imm <= 127) и (imm >= -128) то (* sign extended *)
						PutByte (83H); wImm:= Bit8
					иначе (* 16/32 bit immediate *)
						PutByte (81H); wImm:= устарПреобразуйКБолееУзкомуЦел (reg) (* 16/32 bit *)
					всё;
					PutReg (op DIV 8, reg); (* op DIV 8 is code for op *)
					PutDisp (imm, wImm)
				всё
		  | ImmMem, ImmMemA:
				если reg >= AL то (* byte ptr *)
					PutByte (80H); wImm:= Bit8
				аесли (imm <= 127) и (imm >= -128) то (* sign extended *)
					PutByte (83H); wImm:= Bit8 (* 16/32 bit *)
				иначе (* 16/32 bit immediate *)
					PutByte (81H); wImm:= устарПреобразуйКБолееУзкомуЦел (reg) (* 16/32 bit *)
				всё;
				PutMem (op DIV 8, base, inx, scale, disp, mode = ImmMemA);
				PutDisp (imm, wImm)
		иначе СТОП (BUG)
		всё
	кон GenTyp1;

	проц GenGroup3* (op, mode: цел8; reg, base, inx: цел16; scale: цел8; disp: цел32);
	(* generic code generator for NEG, NOT *)
		перем w: цел8;
	нач
		Prefix (reg, w); PutByte (0F6H + w);
		если mode = Regs то PutReg (op, reg)
		иначе PutMem (op, base, inx, scale, disp, ложь)
		всё
	кон GenGroup3;

	проц GenINC* (mode: цел8; reg, base, inx: цел16; scale: цел8; disp: цел32);
		перем w: цел8;
	нач
		Prefix (reg, w);
		если mode = ImmReg то
			если w # 0 то
				reg:= reg остОтДеленияНа 8; PutByte (40H + reg)
			иначе
				PutByte (0FEH + w); PutReg (0, reg)
			всё
		иначе
			PutByte (0FEH + w); PutMem (0, base, inx, scale, disp, ложь)
		всё
	кон GenINC;

	проц GenDEC* (mode: цел8; reg, base, inx: цел16; scale: цел8; disp: цел32);
		перем w: цел8;
	нач
		Prefix (reg, w);
		если mode = ImmReg то
			если w # 0 то
				reg:= reg остОтДеленияНа 8; PutByte (48H + reg)
			иначе
				PutByte (0FEH + w); PutReg (1, reg)
			всё
		иначе
			PutByte (0FEH + w); PutMem (1, base, inx, scale, disp, ложь)
		всё
	кон GenDEC;

	проц GenMUL* (disp32: булево; reg, base, inx: цел16; scale: цел8; disp: цел32);
		перем w: цел8;
	нач
		утв(reg остОтДеленияНа 8 = EAX);	(*EAX or AX or AL *)
		Prefix(reg, w);
		PutByte(0F6H + w); PutMem (4, base, inx, scale, disp, disp32)
	кон GenMUL;

	проц GenIMUL* (mode: цел8; shortform: булево; reg, base, inx: цел16; scale: цел8;
									disp, imm: цел32);
		перем w: цел8;
	нач
		Prefix (reg, w);
		просей mode из
			RegReg: (* reg := reg * base *)
				если shortform то
					PutByte (0F6H + w); PutReg (5, base)
				иначе
					PutByte (0FH); PutByte (0AFH); PutReg (reg, base)
				всё
		  | MemReg, MemAReg: (* reg := reg * mem *)
				если shortform то
					PutByte (0F6H + w); PutMem (5, base, inx, scale, disp, mode = MemAReg)
				иначе
					PutByte (0FH); PutByte (0AFH); PutMem (reg, base, inx, scale, disp, mode = MemAReg)
				всё
		  | ImmReg: (* reg := base * imm *)
				если (imm <= 127) и (imm >= -128) то
					PutByte (6BH); PutReg (reg, base); PutByte (устарПреобразуйКБолееУзкомуЦел (imm))
				иначе
					PutByte (69H); PutReg (reg, base); PutDisp (imm, base)
				всё
		  | ImmMem, ImmMemA:
				если (imm <= 127) и (imm >= -128) то
					PutByte (6BH); PutMem (reg, base, inx, scale, disp, mode = ImmMemA); PutByte (устарПреобразуйКБолееУзкомуЦел (imm))
				иначе
					PutByte (69H); PutMem (reg, base, inx, scale, disp, mode = ImmMemA); PutDisp (imm, base)
				всё
		иначе СТОП (BUG)
		всё
	кон GenIMUL;

	проц GenIDIV* (mode: цел8; reg, base, inx: цел16; scale: цел8; disp: цел32);
		перем w: цел8;
	нач
		Prefix (reg, w); PutByte (0F6H + w);
		если mode = RegReg то PutReg (7, reg)
		иначе PutMem (7, base, inx, scale, disp, mode >= ForceDisp32)
		всё
	кон GenIDIV;

	проц GenShiftRot* (op, mode: цел8; reg, base, inx: цел16; scale: цел8; disp, imm: цел32);
	(* Generates code for ROL, ROR, SAL, SAR, SHL and SHR *)
		перем w: цел8;
	нач
		Prefix (reg, w);
		просей mode из
			RegReg: (* reg by  base, base = CL *)
				PutByte (0D2H + w); PutReg (op, reg)
		  | RegMem, RegMemA: (* mem by reg; reg = CL *)
				PutByte (0D2H + w); PutMem (op, base, inx, scale, disp, mode = RegMemA)
		  | ImmReg: (* register by immediate *)
				если imm = 1 то
					PutByte (0D0H + w); PutReg (op, reg)
				иначе
					PutByte (0C0H + w); PutReg (op, reg); PutByte (устарПреобразуйКБолееУзкомуЦел (imm))
				всё
		  | ImmMem, ImmMemA: (* memory by immediate *)
				если imm = 1 то
					PutByte (0D0H + w); PutMem (op, base, inx, scale, disp, mode = ImmMemA)
				иначе
					PutByte (0C0H + w); PutMem (op, base, inx, scale, disp, mode = ImmMemA); PutByte (устарПреобразуйКБолееУзкомуЦел (imm))
				всё
		иначе СТОП (BUG)
		всё
	кон GenShiftRot;

	проц GenSHDouble* (op, mode: цел8; shortform: булево; reg, base, inx: цел16; scale: цел8; disp, imm: цел32);
	перем w : цел8;
	нач
		утв((op = Left) или (op = Right), 200);
		Prefix (reg, w);
		просей mode из
		| RegReg:
				если shortform то
					PutByte(0FH); PutByte(0A5H + op*8); PutReg (reg, base)
				иначе
					PutByte(0FH); PutByte(0A4H + op*8); PutReg (reg, base); PutByte (устарПреобразуйКБолееУзкомуЦел (imm))
				всё
		| RegMem:
				если shortform то
					PutByte(0FH); PutByte(0A5H + op*8); PutMem(reg, base, inx, scale, disp, ложь)
				иначе
					PutByte(0FH); PutByte(0A4H + op*8); PutMem(reg, base, inx, scale, disp, ложь); PutByte (устарПреобразуйКБолееУзкомуЦел (imm))
				всё
		иначе СТОП(BUG)
		всё;
	кон GenSHDouble;

	проц GenString* (op, size: цел16);
		перем w: цел8;
	нач
		Prefix (size, w);
		PutByte (op * 2 + w)
	кон GenString;

	проц GenRepString* (op, size: цел16); (* without REPE CMPS, REPE SCAS *)
		перем w: цел8;
	нач
		Prefix (size, w);
		PutByte (0F3H); PutByte (op * 2 + w)
	кон GenRepString;

	проц GenRepCmpsScas* (op, size: цел16);
	(* REPE CMPS, REPE SCAS *)
		перем w: цел8;
	нач
		Prefix (size, w);
		PutByte (0F3H); PutByte (op * 2 + w)
	кон GenRepCmpsScas;

	проц GenTEST* (mode: цел8; reg, base, inx: цел16; scale: цел8; disp, imm: цел32);
		перем w: цел8;
	нач
		Prefix(reg, w);
		просей mode из
		| RegReg:	(* TEST reg, base *)
				PutByte(84H+w); PutReg(base, reg)
		| RegMem, RegMemA:	(* TEST mem, reg *)
				PutByte(84H+w); PutMem (reg, base, inx, scale, disp, mode = RegMemA)
		| ImmReg:	(* TEST reg, imm *)
				если reg в {EAX, AX, AL} то
					PutByte(0A8H+w); PutDisp(imm, (reg DIV 8) * 8)
				иначе
					PutByte(0F6H+w); PutReg(0, reg);  PutDisp(imm, (reg DIV 8) * 8)
				всё
		| ImmMem, ImmMemA:	(* TEST mem, imm *)
				PutByte(0F6H+w);
				PutMem(0, base, inx, scale, disp, mode = ImmMemA);  PutDisp(imm, (reg DIV 8) * 8)
		иначе СТОП (BUG)
		всё
	кон GenTEST;

	проц GenB* (op: цел8; mode: цел8; reg, base, inx: цел16; scale: цел8; disp, imm: цел32);
		перем w: цел8;
	нач
		Prefix (reg, w); PutByte (0FH);
		просей mode из
			RegReg: (* BT reg, base *)
				PutByte (083H + 8*op); PutReg (base, reg)
		  | RegMem, RegMemA: (* BT mem, reg *)
				PutByte (083H + 8*op); PutMem (reg, base, inx, scale, disp, mode = RegMemA)
		  | ImmReg: (* BT reg, imm *)
				PutByte (0BAH); PutReg (op, reg); PutByte (устарПреобразуйКБолееУзкомуЦел (imm))
		  | ImmMem, ImmMemA: (* BT mem, imm *)
				PutByte (0BAH); PutMem (op, base, inx, scale, disp, mode = ImmMemA); PutByte (устарПреобразуйКБолееУзкомуЦел (imm))
		иначе СТОП (BUG)
		всё
	кон GenB;

	проц GenCALL* (mode: цел8; reg, base, inx: цел16; scale: цел8; disp: цел32);
	(* no intersegment call implemented yet *)
	нач
		если (mode = Imme) или (mode = ImmeA) то  (* direct call within segment *)
			PutByte (0E8H); PutDWord (disp)
		иначе
			PutByte (0FFH);
			если mode = Regs то PutReg (2, reg) (* call reg *)
			иначе PutMem (2, base, inx, scale, disp, истина) (* Memory call, displacement must be 32-bit wide as it will need a fix-up *)
			всё
		всё
	кон GenCALL;

	проц GenJMP* (mode: цел8; reg, base, inx: цел16; scale: цел8; disp: цел32);
	(* no intersegment jmp implemented yet *)
	нач
		если mode =  Imme то (* direct jmp whithin segment *)
			если (disp <= 127) и (disp >= -128) то (* short jump *)
				PutByte (0EBH); PutByte (устарПреобразуйКБолееУзкомуЦел (disp))
			иначе
				PutByte (0E9H); PutDWord (disp)
			всё
		аесли mode =  ImmeA то (* direct jmp whithin segment *)
			PutByte (0E9H); PutDWord (disp)
		иначе
			PutByte (0FFH);
			если mode = Regs то PutReg (4, reg) (* jmp reg *)
			иначе PutMem (4, base, inx, scale, disp, mode >= ForceDisp32) (* jmp memory *)
			всё
		всё
	кон GenJMP;

	проц GenRET* (size: цел32);
	(* no intersegemt return implemented yet *)
	нач
		если size = 0 то PutByte (0C3H)
		аесли size > 0FFFFH то
			(* ret takes only 16bit operands, thus returning more than 10000H bytes must be done by hand*)
			(* POP EBX *)
			(* ADD ESP, size*)
			(* JMP EBX*)
			GenPOP(Regs, EBX, noBase, noInx, noScale, noDisp);
			GenTyp1(ADD, ImmReg, ESP, noBase, noInx, noScale, noDisp, size);
			GenJMP(Regs, EBX, noBase, noInx, noScale, noDisp)
		иначе
			PutByte (0C2H); PutWord (size)
		всё
	кон GenRET;

	проц GenJcc* (op: цел8; disp: цел32);
	(*
		jo, jno, jb/jnae, jnb/jae, je/jz, jne/jnz, jbe/jna, jnbe/ja, js, jns, jp/jpe, jnp/jpo, jl/jnge, jnl/jge, jnle/jg
		disp must lie within the segment!
	*)
	нач
		если (disp <= 127) и (disp >= -128) то (* short jmp *)
			PutByte (70H + op); PutByte (устарПреобразуйКБолееУзкомуЦел (disp))
		иначе (* near jmp, always 32 bit wide *)
			PutByte (0FH); PutByte (80H + op); PutDWord (disp)
		всё
	кон GenJcc;

	проц GenSetcc* (op, mode: цел8; base, inx: цел16; scale: цел8; disp: цел32);
	(*
		seto, setno, setb/setna, setnb, sete/setz, setne/setnz, setnbe/seta, sets, setns, setp/setpe, setnp/setpo,
		setl/setnge, setnl/setge, setle/setng, setnle/setg
		target register/memory is always 8 bit!
	*)
	нач
		PutByte (0FH); PutByte (90H + op);
		если mode = Regs то PutReg (0, base) (* setcc reg *)
		иначе PutMem (0, base, inx, scale, disp, mode >= ForceDisp32)
		всё
	кон GenSetcc;

	проц GenINT* (intNumber: цел16);
	(* int intNumber *)
	нач
		PutByte (0CDH); PutByte (intNumber)
	кон GenINT;

	проц InlineCode* (перем code: массив из симв8; parSize: цел16);
		перем i, n: цел16;
	нач
		n := кодСимв8(code[0]); i := 1;
		нцПока i <= n делай PutByte(кодСимв8(code[i])); увел(i) кц;
	кон InlineCode;

(* floating point encoding *)

	проц GenFLD* (mode, size: цел8; base, inx: цел16; scale: цел8; disp: цел32);
	нач
		просей mode из
			Regs: (* fload st(0), st (i) ; st(i) = base *)
				PutByte (0D9H); PutByte (0C0H + base)
		  | Mem, MemA: (* fload st(0), mem *)
				если size = eReal то
					PutByte (0DBH); PutMem (5, base, inx, scale, disp, mode = MemA)
				аесли size = qInt то
					PutByte (0DFH); PutMem (5, base, inx, scale, disp, mode = MemA)
				иначе
					PutByte (0D9H + size * 2); PutMem (0, base, inx, scale, disp, mode = MemA)
				всё
		иначе СТОП (BUG)
		всё
	кон GenFLD;

	проц GenFST* (mode, size: цел8; base, inx: цел16; scale: цел8; disp: цел32);
	нач
		просей mode из
			Regs: (* fstore st(0), st(i); st(i) = base *)
				PutByte (0DDH); PutByte (0D0H + base)
		  | RegMem, RegMemA: (* fstore mem, st(0) *)
				PutByte (0D9H + size * 2); PutMem (2, base, inx, scale, disp, mode = RegMemA)
		иначе СТОП (BUG)
		всё
	кон GenFST;

	проц GenFSTP* (mode, size: цел8; base, inx: цел16; scale: цел8; disp: цел32);
	нач
		просей mode из
			Regs: (* fstore st(0), st(i); st(i) = base *)
				PutByte (0DDH); PutByte (0D8H + base)
		  | RegMem, RegMemA:
				если size = eReal то
					PutByte (0DBH); PutMem (7, base, inx, scale, disp, mode = RegMemA)
				аесли size = qInt то
					PutByte (0DFH); PutMem (7, base, inx, scale, disp, mode = RegMemA)
				иначе
					PutByte (0D9H + size * 2); PutMem (3, base, inx, scale, disp, mode = RegMemA)
				всё
		иначе СТОП (BUG)
		всё
	кон GenFSTP;

	проц GenFCOM* (mode, size: цел8; base, inx: цел16; scale: цел8; disp: цел32);
	нач
		просей mode из
			Regs: (* fcom st(0), st(i) *)
				PutByte (0D8H); PutByte (0D0H + base)
		  | Mem, MemA: (* fcom st(0), mem *)
				PutByte (0D8H + size * 2); PutMem (2, base, inx, scale, disp, mode = MemA)
		иначе СТОП (BUG)
		всё
	кон GenFCOM;

	проц GenFCOMP* (mode, size: цел8; base, inx: цел16; scale: цел8; disp: цел32);
	нач
		просей mode из
			Regs: (* fcomp st(0), st(i) *)
				PutByte (0D8H); PutByte (0D8H+ base)
		  | Mem, MemA: (* fcomp st(0), mem *)
				PutByte (0D8H + size * 2); PutMem (3, base, inx, scale, disp, mode = MemA)
		иначе СТОП (BUG)
		всё
	кон GenFCOMP;

	проц GenFtyp1* (op, mode, size: цел8; base, inx: цел16; scale: цел8; disp: цел32);
	нач
		просей mode из
			RegSt: (* op st, base and pop not possible*)
				PutByte (0D8H); PutByte (0C0H + устарПреобразуйКБолееШирокомуЦел (op) * 8 + base)
		  | StReg: (* op base, st *)
				PutByte (0DCH); PutByte (0C0H + устарПреобразуйКБолееШирокомуЦел (op) * 8 + base)
		  | StRegP: (* op base, st *)
				PutByte (0DEH); PutByte (0C0H + устарПреобразуйКБолееШирокомуЦел (op) * 8 + base)
		  | MemSt, MemASt:
				PutByte (0D8H + size * 2); PutMem (op, base, inx, scale, disp, mode = MemASt)
		иначе СТОП (BUG)
		всё
	кон GenFtyp1;

	проц GenFADD* (mode, size: цел8; base, inx: цел16; scale: цел8; disp: цел32);
	нач
		GenFtyp1 (0, mode, size, base, inx, scale, disp)
	кон GenFADD;

	проц GenFSUB* (mode, size: цел8; base, inx: цел16; scale: цел8; disp: цел32);
	нач
		если (mode = StReg) или (mode = StRegP) то GenFtyp1 (5, mode, size, base, inx, scale, disp)
		иначе GenFtyp1 (4, mode, size, base, inx, scale, disp)
		всё
	кон GenFSUB;

	проц GenFSUBR* (mode, size: цел8; base, inx: цел16; scale: цел8; disp: цел32);
	нач
		если (mode = StReg) или (mode = StRegP) то GenFtyp1 (4, mode, size, base, inx, scale, disp)
		иначе GenFtyp1 (5, mode, size, base, inx, scale, disp)
		всё
	кон GenFSUBR;

	проц GenFMUL* (mode, size: цел8; base, inx: цел16; scale: цел8; disp: цел32);
	нач
		GenFtyp1 (1, mode, size, base, inx, scale, disp)
	кон GenFMUL;

	проц GenFDIV* (mode, size: цел8; base, inx: цел16; scale: цел8; disp: цел32);
	нач
		если (mode = StReg) или (mode = StRegP) то GenFtyp1 (7, mode, size, base, inx, scale, disp)
		иначе GenFtyp1 (6, mode, size, base, inx, scale, disp)
		всё
	кон GenFDIV;

	проц GenFDIVR* (mode, size: цел8; base, inx: цел16; scale: цел8; disp: цел32);
	нач
		если (mode = StReg) или (mode = StRegP) то GenFtyp1 (6, mode, size, base, inx, scale, disp)
		иначе GenFtyp1 (7, mode, size, base, inx, scale, disp)
		всё
	кон GenFDIVR;

	проц GenFFREE* (freg: цел16);
	нач
		PutByte (0DDH); PutByte (0C0H + freg)
	кон GenFFREE;

	проц GenFop1* (op: цел16);
	нач
		просей op из
			FCOMPP:
				PutByte (0DEH); PutByte (0D9H)
		  | FTST:
				PutByte (0D9H); PutByte (0E4H)
		  | FLDZ:
				PutByte (0D9H); PutByte (0EEH)
		  | FLD1:
				PutByte (0D9H); PutByte (0E8H)
		  | FABS:
				PutByte (0D9H); PutByte (0E1H)
		  | FCHS:
				PutByte (0D9H); PutByte (0E0H)
		  | FSTSW:
				PutByte (0DFH); PutByte (0E0H)
		  | FINCSTP:
				PutByte (0D9H); PutByte (0F7H)
		  | FDECSTP:
				PutByte (0D9H); PutByte (0F6H)
		иначе СТОП (BUG)
		всё
	кон GenFop1;

	проц GenFSTCW* (base, inx: цел16; scale: цел8; disp: цел32);
	нач
		PutByte (0D9H); PutMem (7, base, inx, scale, disp, ложь)
	кон GenFSTCW;

	проц GenFLDCW* (base, inx: цел16; scale: цел8; disp: цел32);
	нач
		PutByte (0D9H); PutMem (5, base, inx, scale, disp, ложь)
	кон GenFLDCW;

нач
	codeLength := InitialCodeLength;
	нов(code, codeLength)
кон PCO.


(*
	15.11.06	ug	code length not limited to 64K, can be extended by chunks of 64K
	04.07.01	prk	intel's ret can only free 2^16 bytes of stack, fixed
	07.05.01	prk	Installable code generators moved to PCLIR; debug function added
	03.05.01	be	Installable code generators
	26.04.01	prk	PCLIR.lea partly removed
*)

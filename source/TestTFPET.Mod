MODULE TestTFPET;

IMPORT
	BimboScannerUCS32, Streams, KernelLog, UCS32, Commands, TFStringPoolUCS32, StringsUCS32, TFTypeSysUCS32, r;
	
PROCEDURE TestBimboScannerInner(CONST iskh : UCS32.PStringJQ; VAR ozhidaliPoluchili : UCS32.PStringJQ) : BOOLEAN;
VAR scanner : BimboScannerUCS32.Scanner; sym : SIZE; i : INTEGER; sw : Streams.StringWriter; r2Ch : ARRAY 10000 OF CHAR; r2, r2LF, ozhidaliLF : UCS32.PStringJQ;
VAR iskhCh : ARRAY 10000 OF CHAR;
CONST limit = 500; 
BEGIN
	UCS32.COPYJQvU8(iskh^, iskhCh);
	scanner := BimboScannerUCS32.InitWithUTF8String(iskhCh);
	NEW(sw, 1000);
	LOOP
		scanner.Get(sym);
		INC(i);
		sw.Int(sym,0);
		IF sym = BimboScannerUCS32.eof THEN
			EXIT
		ELSIF i = limit THEN
			sw.Ln; sw.String("Zashhita ot zaciklivanija");
			EXIT
		ELSIF i MOD 16 = 0 THEN
			sw.Ln
		ELSE
			sw.String(" ") END END;
	sw.Get(r2Ch);
	NEW(r2, 1000);
	UCS32.COPYU8vJQ(r2Ch, r2^);
	NEW(r2LF,LEN(r2)); StringsUCS32.ZameniCRLFnaLF(r2^,r2LF^);
	NEW(ozhidaliLF,LEN(ozhidaliPoluchili)); StringsUCS32.ZameniCRLFnaLF(ozhidaliPoluchili^,ozhidaliLF^);
	IF UCS32.SravniStringJQ(ozhidaliLF^, r2LF^) # UCS32.CmpEqual THEN
		ozhidaliPoluchili := r2;
		RETURN FALSE END;
	RETURN TRUE END TestBimboScannerInner;

PROCEDURE TestBimboScanner*(c : Commands.Context);
VAR iskh, ozhidaliPoluchili, rez : UCS32.PStringJQ; rezCh : ARRAY 10000 OF CHAR; wr : Streams.Writer;
VAR imja : ARRAY 100 OF CHAR; uspekhGetString : BOOLEAN; kodOshibkiGetAString : UNSIGNED8;
BEGIN
	uspekhGetString := c.arg.GetString(imja);
	IF ~uspekhGetString THEN 
		KernelLog.String("TestBimboScanner: GetString неуспешна - не смог прочитать имя теста");
		KernelLog.Ln;
		RETURN END;
	kodOshibkiGetAString := StringsUCS32.GetAString(c.arg, NIL, iskh);
	IF kodOshibkiGetAString # 0 THEN
		KernelLog.String("TestBimboScanner: ошибка при чтении фрагмента исходного текста, код "); 
		KernelLog.Int(kodOshibkiGetAString, 0);
		KernelLog.Ln; 
		RETURN END;
	kodOshibkiGetAString := StringsUCS32.GetAString(c.arg, NIL, ozhidaliPoluchili);
	IF kodOshibkiGetAString # 0 THEN
		KernelLog.String("TestBimboScanner: ошибка при чтении параметра 'ожидаемый результат', код "); 
		KernelLog.Int(kodOshibkiGetAString, 0);
		KernelLog.Ln; 
		RETURN END;
	Streams.OpenWriter(wr, KernelLog.Send);
	NEW(rez, 1000);
	IF ~TestBimboScannerInner(iskh, ozhidaliPoluchili) THEN
		KernelLog.String("TestTFPET.TestBimboScanner "); KernelLog.String(imja); KernelLog.String(" НЕ ПРОШЁЛ"); KernelLog.Ln;
		UCS32.COPYJQvU8(ozhidaliPoluchili^, rezCh);
		KernelLog.String("<"); KernelLog.String(rezCh); KernelLog.String(">"); KernelLog.Ln;
		END END TestBimboScanner;
		
		
		
PROCEDURE EchoGetAString*(c : Commands.Context);
VAR vykh : UCS32.PStringJQ; kodOshibki : UNSIGNED8; kw : Streams.Writer;
BEGIN
	kodOshibki := StringsUCS32.GetAString(c.arg, NIL, vykh);
	IF kodOshibki # 0 THEN
		KernelLog.String("TestTFPET.EchoGetAString: GetAString: код ошибки = "); KernelLog.Int(UNSIGNED16(kodOshibki), 0); KernelLog.Ln;
		RETURN END;
	Streams.OpenWriter( kw, KernelLog.Send );
	kw.StringJQ(vykh^,3); kw.Ln; kw.Update END EchoGetAString; 
	
	
	

PROCEDURE TestStringPool*(c : Commands.Context);
VAR x, y : ARRAY 128 OF UCS32.CharJQ;
	i : SIZE;
	buf : POINTER TO ARRAY OF SIZE;
	s : TFStringPoolUCS32.StringPool;
BEGIN
	NEW(s);
	NEW(buf, 1000000);
	FOR i := 0 TO 1000000 - 1  DO
		UCS32.SIGNED64ToStr(i, x);
(*		Strings.Append(x, " - Huga"); *)
		buf[i] := s.AddString(x);
	END;

	FOR i := 0 TO 1000000 - 1  DO
		UCS32.SIGNED64ToStr(i, x);
(*		Strings.Append(x, " - Huga"); *)
		s.GetString(buf[i], y);
		IF UCS32.SravniStringJQ(x,y) # UCS32.CmpEqual THEN KernelLog.String("Failed"); KernelLog.Ln;  END;
	END;
	KernelLog.String("done"); KernelLog.Ln;
END TestStringPool;

VAR (* m1 : TFTypeSys.Module; *) m2 : TFTypeSysUCS32.Module;
(* TestTFPET.TestReadSymbolFile ~ *)
PROCEDURE TestReadSymbolFile*(c : Commands.Context);
BEGIN
(* m1:=TFTypeSys.ReadSymbolFile("Proba"); *)
m2:=TFTypeSysUCS32.ReadSymbolFile(UCS32.Lit("Proba2")^);
(* KernelLog.String("m1:=========="); KernelLog.Ln;
r.PechKL(m1);
KernelLog.Ln;  *)
KernelLog.String("m2:=========="); KernelLog.Ln;
r.PechKL(m2.scope);
END TestReadSymbolFile;


PROCEDURE TestSravnenijaStrokiSSobojj*(c : Commands.Context);
VAR s1, s2 : UCS32.PStringJQ;
BEGIN
IGNORE StringsUCS32.GetAString(c.arg, NIL, s1);
s2 := StringsUCS32.NewString(s1^);
KernelLog.UCS32StringJQLiteral(s1^);
KernelLog.UCS32StringJQLiteral(s2^);
ASSERT(s1^ = s2^); 
END TestSravnenijaStrokiSSobojj;


PROCEDURE aS*(c : Commands.Context);
VAR s1, s2 : ARRAY 20 OF CHAR;
BEGIN
s1 := "abc";
s2 := s1;
KernelLog.String(s2); KernelLog.Ln;
END aS;
	
	
END TestTFPET.

TestTFPET.TestSravnenijaStrokiSSobojj „Привет, медведь!“ ~ 




TestTFPET.EchoGetAString \"ab
cd"\ ~


System.FreeDownTo TFTypeSysUCS32~


TFAOParser.MakeSymbolFile Proba.Mod ""~


TFAOParser2.MakeSymbolFile Proba2.Mod ""~
TestTFPET.TestReadSymbolFile ~


TestTFPET.TestBimboScanner Дым 'MODULE aaa; "bbbb" (* cccc *)' "71 40 41 39 73 72" ~
TestTFPET.TestBimboScanner Гуля Гуля. \"40 18 72"\
TestTFPET.TestBimboScanner Единица "1 ." "35 18 72"~
TestTFPET.TestBimboScanner Семнадцать "1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 ." 
	\"35 35 35 35 35 35 35 35 35 35 35 35 35 35 35 35\n18 72"\ ~
TestTFPET.TestBimboScanner СтрочкиИСморчки '| mmm , " " .' "42 40 19 35 18 72"~

TestTFPET.TestBimboScanner Даль \"
  PROCEDURE InitReservedChars;VAR  i: SIZE; BEGIN  FOR i := 0 TO LEN(reservedChar)-1 DO    
  IF i <= 2 THEN  (* TAB, CR, ESC ... *)      reservedChar[i] := TRUE;    ELSIF i < UCS32.GranicaASCII 
  THEN CASE CHR(UNSIGNED8(i)) OF | "#", "&", "(", ")", "*", "+", ",", "-", ".", "/", "?": reservedChar[i] := TRUE;  
  | ":", ";", "<", "=", ">": reservedChar[i] := TRUE;    | "[", "]", "^", "{", "|", "}", ppp."\ 
\"74 69 40 41 68 40 20 203 41 64 51 40 34 35 28 40
30 40 22 7 35 27 74 47 40 12 35 26 73 40 31 40
23 34 37 41 45 40 11 40 18 40 74 26 48 40 30 40
30 40 22 22 25 42 35 19 35 19 35 19 35 19 35 19
35 19 35 19 35 19 35 19 35 19 35 20 40 31 40 23
34 37 41 74 42 35 19 35 19 35 19 35 19 35 20 40
31 40 23 34 37 41 42 35 19 35 19 35 19 35 19 35
19 35 19 40 18 72"\ ~


TestTFPET.TestBimboScanner ПростоМодуль "MODULE Proba; END Proba." "71 40 41 43 40 18 72"~
  
  
TestTFPET.EchoGetAString \"CONST limit = 500; BEGIN IGNORE c.arg.GetString(s);   scanner := InitWithUTF8String(s);  
  KernelLog.String("TestTFPET.TestBimboScanner: ");   KernelLog.String(s); KernelLog.Ln;  LOOP    scanner.Get(sym);    
  INC(i);    KernelLog.Int(sym,0); KernelLog.String(" ");    IF sym = eof THEN      KernelLog.Ln;      RETURN END;    
  IF i = limit THEN      KernelLog.String("Защита от зацикливания");      KernelLog.Ln;      RETURN END END END Test;
  
  PROCEDURE InitReservedChars;VAR  i: SIZE; BEGIN  FOR i := 0 TO LEN(reservedChar)-1 DO    
  IF i <= 020H THEN  (* TAB, CR, ESC ... *)      reservedChar[i] := TRUE;    ELSIF i < UCS32.GranicaASCII 
  THEN CASE CHR(UNSIGNED8(i)) OF | "#", "&", "(", ")", "*", "+", ",", "-", ".", "/", "?": reservedChar[i] := TRUE;  
  | ":", ";", "<", "=", ">": reservedChar[i] := TRUE;    | "[", "]", "^", "{", "|", "}", ppp."\ ~        


TestTFPET.EchoGetAString \"\t\9;\3;ю
d\"„“"\~

TestTFPET.TestStringPool ~

WMDebugger.Open BimboScannerUCS32.Mod ~
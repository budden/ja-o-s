(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль LinEqSVD;  (** AUTHOR "ph"; PURPOSE "Solves a linear system of equations using SVD."; *)

использует Nbr := NbrRe, Vec := VecRe, Mtx := MtxRe, LinEq := LinEqRe, Errors := DataErrors, MathRe, Out  := ЛогЯдра ;

тип
	(** on initialization NEW(s,A) the matrix A of size m*n is subjected to the singular value decomposition A= u * w *  v ;     *)
	Solver* = окласс
			перем u-, w-, vt-: Mtx.Matrix;   (**  result matrices; u is size m*n, w is size n*n; note that the algorithm returns the transpose  vT(size n*n) instead of v *)
				threshold*: Nbr.Real;   (** for level of accuracy; default = 1.0D-10*)
				iterations*: цел32;   (** for detection of non-convergence; default = 30*)
				Reciprocal: Vec.Map;
				zero, mag: Nbr.Real;

				проц & Initialize*( перем A: Mtx.Matrix );
				нач
					если A # НУЛЬ то
						u := A.Copy( );  LinEq.NormalizeMatrix( u, mag );  zero := 0;  threshold := 1.0E-10;  iterations := 30;
						Reciprocal := reciprocal;  decompose();  w := w*mag;
					иначе Errors.Error( "A NIL matrix was supplied." )
					всё
				кон Initialize;

				проц decompose;
				перем m, n: цел32;
					rv1: укль на массив из Nbr.Real;
					anorm, scale, c, f, g, h, s, x, y, z: Nbr.Real;  i, its, j, jj, k, l, nm: цел32;  flag: булево;
				нач
					если u = НУЛЬ то Errors.Error(  "A NIL matrix was supplied." );  возврат всё;
					m := u.rows;  n := u.cols;  нов( rv1, n );
					если (w = НУЛЬ ) или (w.rows # n) или (w.cols # n)  то нов( w, 0, n, 0, n ) иначе w.Multiply( zero ) всё;
					если (vt = НУЛЬ ) или (vt.rows # n) или (vt.cols # n)  то нов( vt, 0, n, 0, n ) иначе vt.Multiply( zero ) всё;
					g := 0;  scale := 0;  anorm := 0;
					нцДля i := 0 до n - 1 делай
						l := i + 1;  rv1[i] := scale*g;  g := 0;  s := 0;  scale := 0.0;
						если (i < m) то
							нцДля k := i до m - 1 делай scale := scale + матМодуль( u.Get(k, i) ) кц;
							если (scale # 0.0) то
								нцДля k := i до m - 1 делай u.Set(k, i, u.Get(k, i)/scale);  s := s + u.Get(k, i)*u.Get(k, i);  кц;
								f := u.Get(i, i);
								если f >= 0 то g := -MathRe.Sqrt( s ) иначе g := MathRe.Sqrt( s ) всё;
								h := f*g - s;  u.Set(i, i, f - g);
								нцДля j := l до n - 1 делай
									s := 0.0;
									нцДля k := i до m - 1 делай s := s + u.Get(k, i)*u.Get(k, j) кц;
									f := s/h;
									нцДля k := i до m - 1 делай u.Set(k, j, u.Get(k, j) + f*u.Get(k, i)) кц;
								кц;
								нцДля k := i до m - 1 делай u.Set(k, i , u.Get(k, i)*scale) кц;
							всё
						всё;
						w.Set(i, i, scale*g);  g := 0;  s := 0;  scale := 0.0;
						если (i < m) и (i # (n - 1)) то
							нцДля k := l до n - 1 делай scale := scale + матМодуль( u.Get(i, k) ) кц;
							если (scale # 0.0) то
								нцДля k := l до n - 1 делай u.Set(i, k, u.Get(i, k)/scale);  s := s + u.Get(i, k)*u.Get(i, k);  кц;
								f := u.Get(i, l);
								если f >= 0 то g := -MathRe.Sqrt( s ) иначе g := MathRe.Sqrt( s ) всё;
								h := f*g - s;  u.Set(i, l, f - g);
								нцДля k := l до n - 1 делай rv1[k] := u.Get(i, k)/h кц;
								нцДля j := l до m - 1 делай
									s := 0.0;
									нцДля k := l до n - 1 делай s := s + u.Get(j, k)*u.Get(i, k) кц;
									нцДля k := l до n - 1 делай u.Set(j, k, u.Get(j, k) + s*rv1[k] )кц;
								кц;
								нцДля k := l до n - 1 делай u.Set(i, k,  u.Get(i, k)*scale) кц;
							всё;
						всё;
						anorm := Max( anorm, матМодуль( w.Get(i, i) ) + матМодуль( rv1[i] ) )
					кц;
					нцДля i := n - 1 до 0 шаг -1 делай
						если (i < (n - 1)) то
							если g # 0.0 то
								нцДля j := l до n - 1 делай vt.Set(j, i, u.Get(i, j)/(u.Get(i, l)*g)) кц;
								нцДля j := l до n - 1 делай
									s := 0;
									нцДля k := l до n - 1 делай s := s + u.Get(i, k)*vt.Get(k, j) кц;
									нцДля k := l до n - 1 делай
										если (s # 0.0) то vt.Set(k, j,  vt.Get(k, j) + s*vt.Get(k, i)) всё;
									кц;
								кц;
							всё;
							нцДля j := l до n - 1 делай vt.Set(i, j,  0.0);  vt.Set(j, i,  0.0) кц;
						всё;
						vt.Set(i, i, 1.0);  g := rv1[i];  l := i;
					кц;
					нцДля i := MinI( m, n ) - 1 до 0 шаг -1 делай
						l := i + 1;  g := w.Get(i, i);
						нцДля j := l до n - 1 делай u.Set(i, j, 0.0) кц;
						если (g # 0.0) то
							g := 1.0/g;
							нцДля j := l до n - 1 делай
								s := 0.0;
								нцДля k := l до m - 1 делай s := s + u.Get(k, i)*u.Get(k, j) кц;
								f := s*g/u.Get(i, i);
								нцДля k := i до m - 1 делай
									если (f # 0.0) то u.Set(k, j, u.Get(k, j) + f*u.Get(k, i)) всё;
								кц;
							кц;
							нцДля j := i до m - 1 делай u.Set(j, i,  u.Get(j, i)*g)  кц;
						иначе
							нцДля j := i до m - 1 делай u.Set(j, i,  0.0) кц;
						всё;
						u.Set(i, i,  u.Get(i, i) + 1.0) ;
					кц;
					нцДля k := n - 1 до 0 шаг -1 делай
						its := 0;
						нц
							увел( its );
							если its > iterations то прервиЦикл всё;
							flag := истина;  l := k;
							нц
								nm := l - 1;
								если ((матМодуль( rv1[l] ) + anorm) = anorm) то flag := ложь;  прервиЦикл
								аесли ((матМодуль( w.Get(nm, nm) ) + anorm) = anorm) то прервиЦикл
								всё;
								умень( l );
								если l < 0 то прервиЦикл всё;
							кц;
							если flag то
								c := 0.0;  s := 1.0;  i := l;
								нц
									f := s*rv1[i];  rv1[i] := rv1[i]*c;
									если ((матМодуль( f ) + anorm) = anorm) то прервиЦикл всё;
									g := w.Get(i, i);  h := pythag( f, g );  w.Set(i, i, h);  h := 1.0/h;  c := g*h;  s := -f*h;
									нцДля j := 0 до m - 1 делай
										y := u.Get(j, nm);  z := u.Get(j, i);  u.Set(j, nm, y*c + z*s);  u.Set(j, i,z*c - y*s);
									кц;
									если i = k то прервиЦикл всё;
									увел( i )
								кц;
							всё;
							z := w.Get(k, k);
							если (l = k) то
								если (z < 0.0) то
									w.Set(k, k,-z);
									нцДля j := 0 до n - 1 делай vt.Set(j, k, -vt.Get(j, k)) кц;
								всё;
								прервиЦикл;
							всё;
							если (its = iterations) то
								Errors.Error(  "Singular value decomposition iterations do not converge" );
								u := НУЛЬ;  w := НУЛЬ;  vt := НУЛЬ;  возврат;
							всё;
							x := w.Get(l, l);  nm := k - 1;  y := w.Get(nm, nm);  g := rv1[nm];  h := rv1[k];
							f := ((y - z)*(y + z) + (g - h)*(g + h))/(2.0*h*y);  g := pythag( f, zero + 1 );
							f := ((x - z)*(x + z) + h*((y/(f + sign( f )*матМодуль( g ))) - h))/x;  c := 1.0;  s := 1.0;
							нцДля j := l до nm делай
								i := j + 1;  g := rv1[i];  y := w.Get(i, i);  h := s*g;  g := c*g;  z := pythag( f, h );  rv1[j] := z;  c := f/z;
								s := h/z;  f := x*c + g*s;  g := g*c - x*s;  h := y*s;  y := y*c;
								нцДля jj := 0 до n - 1 делай
									x := vt.Get(jj, j);  z := vt.Get(jj, i);  vt.Set(jj, j, x*c + z*s);  vt.Set(jj, i,  z*c - x*s);
								кц;
								z := pythag( f, h );  w.Set(j, j, z);
								если (z # 0.0) то z := 1.0/z;  c := f*z;  s := h*z;  всё;
								f := c*g + s*y;  x := c*y - s*g;
								нцДля jj := 0 до m - 1 делай
									y := u.Get(jj, j);  z := u.Get(jj, i);  u.Set(jj, j,  y*c + z*s);  u.Set(jj, i,  z*c - y*s);
								кц;
							кц;
							rv1[l] := 0.0;  rv1[k] := f;  w.Set(k, k, x);
						кц
					кц;
					возврат;
				кон decompose;

				проц PseudoInverse*( ): Mtx.Matrix;
				перем p, q, r, psinv: Mtx.Matrix;
				нач
					q := w.Copy( );  q.MapAll( Reciprocal );  p := Mtx.Transpose( u );  r := vt*q;  psinv := r*p;
					(*    psinv := (v*q)* Mtx.Transpose( u );   (*this would be more straightforward, but leads to a runtime trap *)
						*)
					возврат psinv
				кон PseudoInverse;

				проц reciprocal( перем x: Nbr.Real );
				нач
					если матМодуль( x ) < threshold то x := 0 иначе x := 1.0/x всё;
					если матМодуль( x ) < threshold то x := 0 всё;
				кон reciprocal;

			кон Solver;

	(******************************************************************)

	проц pythag( a, b: Nbr.Real ): Nbr.Real;
	перем absa, absb, zero: Nbr.Real;
	нач
		zero := 0;  absa := матМодуль( a );  absb := матМодуль( b );
		если absa > absb то возврат absa*MathRe.Sqrt( 1.0 + absb/absa*absb/absa );
		аесли absb = 0 то возврат zero
		иначе возврат absb*MathRe.Sqrt( 1.0 + absa/absb*absa/absb )
		всё;
	кон pythag;

	проц Max( x, y: Nbr.Real ): Nbr.Real;
	нач
		если x > y то возврат x иначе возврат y всё;
	кон Max;

	проц MinI( i, j: цел32 ): цел32;
	нач
		если i < j то возврат i иначе возврат j;  всё;
	кон MinI;

	проц sign( x: Nbr.Real ): цел32;
	нач
		если x >= 0 то возврат 1 иначе возврат -1 всё;
	кон sign;

	проц Log( m: Mtx.Matrix );
	перем i, j: цел32;
	нач
		нцДля j := 0 до m.rows - 1 делай
			нцДля i := 0 до m.cols - 1 делай  (*Out.LongRealFix( m.Get(j, i), 10, 5 );  *)кц;
			Out.пВК_ПС;
		кц;
		Out.пВК_ПС;
	кон Log;

	проц Test*;
	перем a, U, W, V, VT: Mtx.Matrix;  zero: Nbr.Real;  s: Solver;
	нач
		Out.пВК_ПС;  Out.пСтроку8( "-------Singular Value Decomposition Test-------------" );
		Out.пСтроку8( " Singular Value Decomposition of Matrix A; " );  Out.пВК_ПС;
		Out.пСтроку8( " should yield eigenvalues w (in diagonal matrix) and " );  Out.пВК_ПС;
		Out.пСтроку8( " orthonormal matrices u & vT which, " );  Out.пВК_ПС;
		Out.пСтроку8( " multiplied by itself, should give unit matrices; " );  Out.пВК_ПС;
		Out.пСтроку8( " Finally, PseudoInverse of A is calculated, " );  Out.пВК_ПС;
		Out.пСтроку8( " and Pseudoinverse of Pseudoinverse should lead back to A (least square approximation of..) " );  Out.пВК_ПС;

		нов( a, 0, 4, 0, 5 );

		(*arbitrary matrix fill *)
		zero := 0;   (* hack to initialize matrix easier *)
		a.Set( 0, 0, zero + 1 );  a.Set( 1, 0, zero + 2 );   (* ' type casting', not very elegant *)
		a.Set( 3, 0, zero + 3 );  a.Set( 0, 2, zero + 2 );  a.Set( 2, 1, zero + 3 );  a.Set( 3, 1, zero + 2 );
		a.Set( 3, 4, zero + 4 );

		нов( s, a );   (* at this point, solver is initialized with matrix m and SVD already performed in the background *)

		Out.пСтроку8( "a:" );  Out.пВК_ПС;  Log( a );  Out.пСтроку8( "w:" );  Out.пВК_ПС;  Log( s.w );  Out.пСтроку8( "v,vt,v*vt:" );  Out.пВК_ПС;
		Log( s.vt );  V := Mtx.Transpose( s.vt );  Log( V );  V := s.vt*V;  Log( V );  Out.пСтроку8( "u,ut,u*ut:" );  Out.пВК_ПС;
		Log( s.u );  U := Mtx.Transpose( s.u );  Log( U );  U := s.u*U;  Log( U );  Out.пСтроку8( "u*w*v:" );  Out.пВК_ПС;
		VT := Mtx.Transpose( s.vt );  W := s.w*VT;  U := s.u*W;  Log( U );
		Out.пСтроку8( "a, PseudoInverse(a), PseudoInverse(PseudoInverse(a)):" );  Out.пВК_ПС;  Log( a );
		a := s.PseudoInverse();  Log( a );  s.Initialize( a );  a := s.PseudoInverse();  Log( a );
	кон Test;

кон LinEqSVD.

LinEqSVD.Test

System.Free LinEqSVD ~


set -xeuo pipefail 

SCRIPT_PATH=$(cd $(dirname $0) && pwd);
SCRIPT_PATH=`readlink -f $SCRIPT_PATH`

linux64Dir="$SCRIPT_PATH/Linux64"
testDir="$linux64Dir/work/test"

mkdir -p $testDir
cd $testDir

git clean -x -d -f .
# git checkout -f .

peremesti_diff() {
 Tek=$1
 Pred="${1}.Pred"
 if [ -f $Pred ]; then
  rm $Pred; fi
 if [ -f $Tek ]; then
  mv $Tek $Pred; fi 
}

peremesti_diff Oberon.Compilation.Test.Diff
peremesti_diff Oberon.Execution.Test.Diff

cd $linux64Dir
./a2-testy.sh || echo "a2-testy.sh vernul nenulevojj kod vozvrata. Ehto schitaem normalqnym"

cd $testDir
grep "failed tests" FoxCompilationTest.Log
grep "failed tests" FoxExecutionTest.Log

# tail FoxExecutionTest.Log

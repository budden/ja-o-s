#!/bin/bash

set -e

cd `dirname $0`

if [[ ! -f JA-O-S.iso ]]; then
    echo "Не найден файл JA-O-S.iso, он создаётся с помощью ../source/BuildBios32CD.Tool"
    exit 1
fi

if [[ ! -f нжмд-для-qemu.img ]]; then
    echo "Не найден файл нжмд-для-qemu.img"
    echo "Он создаётся с помощью сотри-и-пересоздай-нжмд-для-qemu.баш"
    exit 1
fi

if [ -z `which qemu-system-x86_64` ]; then
    echo "Установи qemu, например, sudo apt-get install qemu"
    exit 1
fi

set -x

sudo qemu-system-x86_64 -m 1025 -machine pc -serial stdio -net nic,model=pcnet -net user \
 -drive file=нжмд-для-qemu.img,format=raw \
 --enable-kvm \
 -boot d -cdrom /dev/sr0

#  -boot menu=on \
# /dos/down/a2/A2_Rev-6498_serial-trace.iso 

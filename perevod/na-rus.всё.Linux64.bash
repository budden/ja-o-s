#!/bin/bash
set -xe
SCRIPT_PATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
JAOS_ROOT="$SCRIPT_PATH/.."

cd $JAOS_ROOT/perevod

if [ -f na-rus.всё.out.успех ]; then 
  rm -f na-rus.всё.out.успех; fi

cd $JAOS_ROOT/Linux64
./oberon execute ../perevod/na-rus.всё.jaoz

